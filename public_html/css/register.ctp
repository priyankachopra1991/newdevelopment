<div class = "signup-content">
	<div class = "row">                                            
		<div class = "col s10 x12 offset-s1">
			<div class = "card parent-card-container">
				<h2 class = "header">Register</h2>                           
				<div class = "row">
					<div class = "col s6">
						<div class = "card">                                
							<div class = "card-horizontal">
								<div class = "card-image">
									<i class = "material-icons left" data-icon-color = "red">ondemand_video</i>
								</div>
							</div>
							<div class = "card-stacked">
								<div class = "card-content">
									<span class="card-title">On Demand Video</span>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>                          
						</div>
						<div class = "card">                                 
							<div class = "card-horizontal">
								<div class = "card-image">
									<i class = "material-icons left" data-icon-color = "orange">share</i>
								</div>
							</div>
							<div class = "card-stacked">
								<div class = "card-content">
									<span class="card-title">Share Your Ideas</span>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
								</div>
							</div>                          
						</div>
						<div class = "card">                                
							<div class = "card-horizontal">
								<div class = "card-image">
									<i class = "material-icons left" data-icon-color = "blue">group</i>
								</div>
							</div>
							<div class = "card-stacked">
								<div class = "card-content">
									<span class="card-title">Build Audience</span>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
								</div>
							</div>                          
						</div>
					</div>
					<div class = "col s6">
						<div class = "row">
							<form  class = "col s12  signup-form" name ="signUpFrm" id="signUpFrm" method="post" action="<?php echo BASE_URL; ?>/users/register">
							<div id="errorMsgFrm" style="display:none;"></div>							
								<div class = "input-field">
									<i class = "material-icons prefix tiny" data-input-color = "grey">face</i>
									<input type = "text" class = "validate" name="name" id="name">
									<label for = "prefix"> Name</label>		
								</div>                                    <div class = "input-field">
									<i class = "material-icons prefix tiny" data-input-color = "grey">email</i>
									<input type = "email" class = "validate" name="email" id="email" autocomplete="new-email"  />
									<label for="telephone">Email</label>
								</div>                                    <div class = "input-field">
									<i class = "material-icons prefix tiny" data-input-color = "grey">lock_outline</i>
									<input type = "password" class = "validate" name="password" id = "password" autocomplete="new-password" />
									<label for="password">Password</label>
								</div> 
								<div class = "input-field">
									<i class = "material-icons prefix tiny" data-input-color = "grey">perm_identity</i>
									<label for="password">User Type</label>
								<div>
								
								<div class = "input-field radioDiv">
								   <input type="radio" name="usertype" value="CPA" class="space" checked="checked" />
									<span class="space">Content Provider</span>
									<br/>
									<input type="radio" name="usertype" value="PUB"  class="space" />
									<span class="space">Publisher</span>
								</div>
								<!--<div class = "input-field">
									<i class = "material-icons prefix tiny" data-input-color = "grey">perm_identity</i>
									
									<select name="usertype" id="usertype">
										<option value="" disabled selected>User Type</option>
                                        <option value="CPA">Content Provider</option>
                                        <option value="PUB">Publisher</option>
									</select>
									<label for="password"></label>
								</div>-->                                
								<div class = "check-terms">
                                <p>
                                <input required type="checkbox" id="terms"  class="filled-in" name="check"  />
                        <label for="terms">I agree to <a href = "" target="_blank" data-icon-color = "red">terms and conditions</a></label>
                        </p>
                    </div>
								<div class = "input-field submit-form">
									<button class="btn waves-effect waves-light" data-background-color = "red" type="submit" name="submitSignUp" id="submitSignUp">Get Started
									<i class="material-icons right tiny">send</i>
									</button>
								</div>
							</form>
						</div>                                      
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#errorMsgFrm").css("display", "none");
	$("#submitSignUp").click(function(){
		//$("#errorMsgFrm").css("display","block");
		var emailRegex = /^[a-z_A-Z0-9\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	
		var mobileRegex = /^[7-9][0-9]{9}$/;
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var nameRegex1 = /^[a-zA-Z0-9&._-]+.{5,20}$/;
		
		var name = $.trim($('#name').val());
		var email = $.trim($('#email').val());
		var password = $.trim($('#password').val());
		var usertype = $.trim($('#usertype').val());
		var terms = $.trim($('#terms').val());
		if(name ==""){
		    $('#name').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the name.");
			return false;
		}
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		if(email ==""){
		    $('#email').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the email.");
			return false;
		}
		if(!emailRegex.test(email)){
			$('#email').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the valid email.");
			return false;
		}
		if(password ==""){
		    $('#password').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the password.");
			return false;
		}
		
		if(!nameRegex1.test(password))
		{
			$('#password').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Password too short! It must contain atleast 5 characters.");
			return false;
		}
		
		if(usertype ==""){
		    $('#usertype').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please select user type.");
			return false;
		}
		if (!$("#terms").is(":checked")) {
		    $('#terms').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please select term and condition.");
			return false;
		}
		return true;
		
	})
})
</script>

<?php echo $this->element('flashmessage'); ?>