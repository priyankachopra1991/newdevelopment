<?php
App::uses('AppController', 'Controller');
ini_set('error_log','/home/newdevelopment/logs/adminPoints.log');
class ReferralController extends AppController{
	public $name = 'Referral';
	public $uses = array('Userprofile', 'Fcusers', 'Activitylog', 'Activitylogcount');
	public $components = array('Paginator');
	public function beforeFilter() {
		parent::beforeFilter();
	}
		
	public function countreport(){
		
		if(!empty($_GET['dataFor'])){
			$dataFor = $_GET['dataFor'];
		} else {
			$dataFor = "registerUser";
		}
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate']. " 00:00:00";
		} else {
			$startDate = date('Y-m-d'). " 00:00:00";
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate']. " 23:59:59";
		} else {
			$endDate = date('Y-m-d'). " 23:59:59";
		}
		
		if($dataFor == "registerUser"){//fetch register user data
		    
			$registerData = $this->Activitylog->query("select count(id) as countRec from user_activity_log where action_type='register' and created>='".$startDate."' and created<='".$endDate."'");
			$this->set(compact('registerData','registerData'));
			
		}
		
		if($dataFor == "view"){//fetch user view user data
		    
			$userViewData = $this->Activitylog->query("select msisdn, count(id) as countRec from user_activity_log where msisdn !='' and msisdn !='NA' and action_type='view' and created>='".$startDate."' and created<='".$endDate."' group by msisdn order by count(id) desc limit 0,100");
			
			$this->set(compact('userViewData','userViewData'));
			
		}
		
		if($dataFor == "referral"){//fetch user referal data...
		    
			$referralData = $this->Activitylog->query("select t1.msisdn as msisdn, count(t2.referredBy) as referredBy from user_activity_log as t1 join user_activity_log as t2 on t2.referredBy = t1.referralId where t1.msisdn !='' and t1.msisdn !='NA' and t1.action_type='register' and t2.msisdn !='' and t2.msisdn !='NA' and t2.action_type='register' and t2.referredBy !='' and t2.referredBy !='NA' and t2.created>='".$startDate."' and t2.created<='".$endDate."' group by t2.referredBy order by count(t2.referredBy) desc limit 0,100");
			$this->set(compact('referralData','referralData'));
			
		}
		
		if($dataFor == "subscriber"){//fetch user subscriber data...
		    //paging section start here...
			$pageNum=1;
			$recPerPage = RECORDPERPAGE;
		    $recPerPage = 1;
			if(isset($this->request->query['page']) && $this->request->query['page']!=0)
			{
				$pageNum = $this->request->query['page'];
			}
			$offSet = ($pageNum - 1) * $recPerPage;
			//paging section end here...
		
			//$subscriberData = $this->Userprofile->query("select t1.msisdn as msisdn, t1.id as user_id  from user_profile as t1 join ch_subscribed_user_details as t2 on t2.app_user_id=t1.id where t1.msisdn !='' and t1.msisdn !='NA' and t1.status=1 and t1.project='firstcut' and t1.operating_system='android' and t2.app_user_id !='' and t2.app_user_id !='NA' and t2.status=1 and t2.created>='".$startDate."' and t2.created<='".$endDate."' group by t1.msisdn order by t2 .id desc limit $offSet, $recPerPage");
			
			$subscriberDataRecs = $this->Activitylog->query("select msisdn, user_id from user_activity_log where subscribe_status=1 and action_type='subscriber' and created>='".$startDate."' and created<='".$endDate."' order by id desc limit $offSet, $recPerPage");
			$subscriberData = array();
			$i = 0;
			foreach($subscriberDataRecs as $subscriberDataRec){
				$referralCount = 0;
				$userVideoViewCount = 0;
				$getReferralIdRec = $this->getUserReferalId($subscriberDataRec["user_activity_log"]["user_id"]);
				if(!empty($getReferralIdRec) && $getReferralIdRec[0]["user_activity_log"]["referralId"] !="" && $getReferralIdRec[0]["user_activity_log"]["referralId"] !="NA"){
					$getReferralCount = $this->getSubscriberUserReferalCount($getReferralIdRec[0]["user_activity_log"]["referralId"], $startDate, $endDate);
					$referralCount = $getReferralCount[0][0]["countRec"];
				} 
				
				$videoViewCount = $this->getSubscriberVideoViewCount($subscriberDataRec["user_activity_log"]["user_id"], $startDate, $endDate);
				if(!empty($videoViewCount)){
					$userVideoViewCount = $videoViewCount[0][0]["countRec"];
				}
				$subscriberData[$i]["msisdn"] = $subscriberDataRec["user_activity_log"]["msisdn"];
				$subscriberData[$i]["referralCount"] = $referralCount;
				$subscriberData[$i]["videoViewCount"] = $userVideoViewCount;
				$i++;
			}
			
			$subscriberCountData = $this->Userprofile->query("select count(msisdn) as countRec from user_activity_log where subscribe_status=1 and action_type='subscriber' and created>='".$startDate."' and created<='".$endDate."'");
			
			$totalRec = $subscriberCountData[0][0]["countRec"];
			$numOfPage = ceil($totalRec / $recPerPage);
		
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
			$this->set(compact('subscriberData','subscriberData'));
			//echo "<pre>";
			//print_r($subscriberData);
			//die;
		}
		
	}
	public function givenPointByAdmin(){
		if($this->request->isPost())
		{
			$data = $this->request->data;
			$dataFor = $data["dataFor"];
			$startDate = $data["startDate"];
			$endDate = $data["endDate"];
			$startDateTime = $data["startDate"]." 00:00:00";
			$endDateTime = $data["endDate"]." 23:59:59";
			$noOfUser = $data["no_of_user"];
			$pointsPerUser = $data["points_per_user"];
			$msisdn = array();
		if($dataFor == "referral"){
			$pointData = $this->Activitylog->query("select t1.msisdn as msisdn from user_activity_log as t1 join user_activity_log as t2 on t2.referredBy = t1.referralId where t1.msisdn !='' and t1.msisdn !='NA' and t1.action_type='register' and t1.point_status=0 and t2.msisdn !='' and t2.msisdn !='NA' and t2.action_type='register' and t2.referredBy !='' and t2.referredBy !='NA' and t2.created>='".$startDateTime."' and t2.created<='".$endDateTime."' group by t2.referredBy order by rand() limit 0, $noOfUser");
			$i = 0; 
			foreach($pointData as $pointDataRec){
				$msisdn[$i]["msisdn"] = $pointDataRec["t1"]["msisdn"];
				$i++;
			}
		}
		
		if($dataFor == "view")
			
			$pointData = $this->Activitylog->query("select msisdn from user_activity_log where msisdn !='' and msisdn !='NA' and point_status=0 and action_type='view' and created>='".$startDate."' and created<='".$endDate."' group by msisdn order by rand() limit 0, $noOfUser");
			$i = 0; 
			foreach($pointData as $pointDataRec){
				$msisdn[$i]["msisdn"] = $pointDataRec["user_activity_log"]["msisdn"];
				$i++;
			}
		}
		
		if(!empty($msisdn)){
			
			$msisdnJson = array();
			$msisdnJsonArr = array();
			foreach($msisdn as $msisdnRec){
			
				$msisdnJson[] = array("msisdn"=>$msisdnRec["msisdn"]);
				$msisdnJsonArr[] = $msisdnRec["msisdn"];
			}
			//$output = json_encode($msisdnJson);
            
			$actionType = 28;
			$appNameId = appNameId;
			//$action = "install";
			//$point = getV3mobiRewards($action, $appNameId);
			$point = $pointsPerUser;
			$note = 'Firstcut, Firstcut Admin Rewards,'.date( 'Y-m-d H:i:s').','.$point;
			error_log("REFERRAL | GIVEN ADMIN REWARDS POINT | actionType=".$actionType. " | appNameId=".$appNameId." | point=".$point. " | note=".$note."\n");
			$url = appReferalUrl.'get_rewardsadmin.php';
			$ch = curl_init($url);
			$json_data = array("msisdn"=>$msisdnJson, 'actionType'=>$actionType, 'appname'=>$appNameId, 'point'=>$point,'note'=>$note);
			$jsonDataEncoded = json_encode($json_data);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			$result = curl_exec($ch);
			 $response   = json_decode($result);
			error_log("REFERRAL | [GET GIVEN ADMIN REWARDS POINT] ".print_r($response,1)."\n");
			curl_close($ch);
			$implodeMsisdn = @implode(",", $msisdnJsonArr);
			
			$updateRec = $this->Activitylog->query("update user_activity_log set point_status=1, points='".$point."', modified='".date( 'Y-m-d H:i:s')."' where msisdn in ($implodeMsisdn)");
			$this->Session->setFlash('Points Updated Successfully.');$this->redirect('countreport?dataFor='.$dataFor.'&startDate='.$startDate.'&endDate='.$endDate.'');
		}
		
	}
	public function detailcountreport(){
		$startDate = $_GET['startDate']. " 00:00:00";
		$endDate = $_GET['endDate']. " 23:59:59";
		$userDetailArr = array();
		$userViewData = $this->Fcusers->query("select count(id) as countRec from  users_view where user_id='".$_GET["user_id"]."' and created>='".$startDate."' and created<='".$endDate."'");
		
		$referralId = $this->getReferalId($_GET["user_id"]);
		$registerData = 0;
		if(!empty($referralId)){
			$registerDataRec = $this->Userprofile->query("select count(id) as countRec from user_profile where referredBy='".$referralId[0]["user_profile"]["referralId"]."' and status=1 and project='firstcut' and operating_system='android' and created>='".$startDate."' and created<='".$endDate."'");
			$registerData = $registerDataRec[0][0]["countRec"];
			$registerMsisdn = $referralId[0]["user_profile"]["msisdn"];
			
		}
		
		$userDetailArr["userView"] = $userViewData[0][0]["countRec"];
		$userDetailArr["registerUser"] = $registerData;	
		$userDetailArr["msisdn"] = $registerMsisdn;	
		$this->set(compact('userDetailArr','userDetailArr'));
       
	}
	public function getReferalId($userId){
		
		$referralId = $this->Userprofile->query("select msisdn, referralId from user_profile where id='".$userId."'");
		return $referralId;
		
	}
	
	public function getUserReferalId($userId){
		
		$referralId = $this->Activitylog->query("select referralId from user_activity_log where user_id='".$userId."' and action_type='register'");
		return $referralId;
		
	}
	
	public function getSubscriberUserReferalCount($referralId, $startDate, $endDate){
		
		$referralCount = $this->Activitylog->query("select count(id) as countRec from user_activity_log where msisdn !='' and msisdn !='NA' and  referredBy='".$referralId."' and action_type='register' and created>='".$startDate."' and created<='".$endDate."'");
		return $referralCount;
		
	}
	
	public function getSubscriberVideoViewCount($userId, $startDate, $endDate){
		$videoViewCount = $this->Activitylog->query("select count(id) as countRec from user_activity_log where user_id='".$userId."'  and action_type='view' and created>='".$startDate."' and created<='".$endDate."'");
		return $videoViewCount;
		
	}
	public function notification()
	{
	   if($this->request->isPost())
	   {
		   $sql = array(); 
           $data = $this->request->data;	
		   $time = time();
		   if(!empty($data["image"]["name"]))
			{
				$file1 = $data['image']['name'];
				$FileType = pathinfo($file1, PATHINFO_EXTENSION);
				$filename = CHANNEL_PUBLICHTML_PATH ."notification_".$time.".".$FileType;
				if (move_uploaded_file($data['image']['tmp_name'],$filename))
                {
					$files_arr = explode("\n",$filename);
					foreach($files_arr as $file1)
					{
						if(!empty($file1))
					    {
							$s3 = new S3(ACCESS_KEY,SECRET_KEY);
							$bucketname='contentsimages';
							$file1_arr =explode("/",$file1);
                                $filename1 = end($file1_arr);
                                $main_arr = array_splice($file1_arr,5);
                                $bucket_file = implode("/",$main_arr);
                                $type = $data['image']['type'];
                                $header = array('Content-Type' =>$type);
								$s3->putObjectFile($file1,$bucketname,$bucket_file,S3::ACL_PUBLIC_READ,array(),$header);
                                if($s3->putObjectFile($file1,$bucketname,$bucket_file,S3::ACL_PUBLIC_READ,array(),$header))
								{
								   $image_link = 'https://s3-ap-southeast-1.amazonaws.com/contentsimages/'.$bucket_file;
                                }
								
						}
					}
				}
			}
			$url = "https://fcm.googleapis.com/fcm/send";
			$ch = curl_init($url);
			
			$getuserprofiledetail = $this->Activitylog->query("SELECT id,fcm_token from user_activity_log where point_status='0' and msisdn = '7838450901' group by msisdn");
			for($n=0;$n<count($getuserprofiledetail);$n++)
			{
				$json_data = array("data"=>array("message"=>$data["message"],"image_link"=>$image_link,"to"=>$getfinaluser[$n]['user_activity_log']['fcm_token']));
				$jsonDataEncoded = json_encode($json_data);
				curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: key=AAAAzGumWow:APA91bEeh0yvPFRo9SOj-oHdwCle2dVWAttq_O4f9ao70_Rnkv3IueOveWAbWHBS4fagJVgtfTbc80cZUgdthKnmljV_v8oS11gQE8tXEhyLjR8D_Pi9Hf80XtfD8fsP7S3xaK47rFlm'));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$result = curl_exec($ch);
				curl_close($ch);
				$resultData = json_decode($result);
				$getuserprofiledetail = $this->Activitylog->query("update user_activity_log set notification_status = '1' where id = '".$getfinaluser[$n]['user_activity_log']['id']."'");
			    
			}
			$this->Session->setFlash('Notification send successfully.');
			$this->redirect('notification');
	   }
	}
	public function countreportdata(){
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate']. " 00:00:00";
		} else {
			$startDate = date('Y-m-d'). " 00:00:00";
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate']. " 23:59:59";
		} else {
			$endDate = date('Y-m-d'). " 23:59:59";
		}
		
		$queryString = "";
		if(isset($_GET['subscriber']) && $_GET['subscriber'] == 1){
			$queryString .= " and t1.subscribe_status=1";
		}
		if(isset($_GET['view']) && $_GET['view'] == 1 && isset($_GET['referral']) && $_GET['referral'] == 1){
			$queryString .= " order by t1.view_count desc, t1.referral_count desc";
		} else if(isset($_GET['referral']) && $_GET['referral'] == 1){
			$queryString .= " order by t1.referral_count desc";
		} else if(isset($_GET['view']) && $_GET['view'] == 1){
			$queryString .= " order by t1.view_count desc";
		} else {
			$queryString .= " order by t1.view_count desc, t1.referral_count desc";
		}
		
		//echo "select t1.id, t1.msisdn, t1.view_count, t1.referral_count, t1.subscribe_status from user_activity_count_log as t1 LEFT JOIN user_activity_count_log t2 on (t1.id = t2.id and t1.id > t2.id) where t1.msisdn != '' and t1.msisdn != 'NA' and t1.created>='".$startDate."' and t1.created<='".$endDate."' group by t1.msisdn $queryString limit 0,100";
		//die;
		
		//$userData = $this->Activitylogcount->query("select t1.id, t1.msisdn, t1.view_count, t1.referral_count, t1.subscribe_status from user_activity_count_log as t1 LEFT JOIN user_activity_count_log t2 on (t1.id = t2.id and t1.id > t2.id) where t1.msisdn != '' and t1.msisdn != 'NA' and t1.created>='".$startDate."' and t1.created<='".$endDate."' group by t1.msisdn $queryString limit 0,100");
		$userData = $this->Activitylogcount->query("SELECT t1.id, t1.user_id, t1.msisdn, t1.view_count, t1.referral_count, t1.subscribe_status FROM user_activity_count_log t1 LEFT JOIN user_activity_count_log t2 ON (t1.user_id = t2.user_id AND t1.id < t2.id) WHERE t2.id IS NULL and t1.msisdn != '' and t1.msisdn != 'NA' and t1.created>='".$startDate."' and t1.created<='".$endDate."'  $queryString limit 0,100");
		//echo "<pre>";
		//print_r($userData);
		//die;
		$this->set(compact('userData', $userData));
	}
	
	public function givenUserPointByAdmin(){
		if($this->request->isPost())
		{
			$data = $this->request->data;
			$subscriber = $data["subscriber"];
			$view = $data["view"];
			$referral = $data["referral"];
			$register = $data["register"];
			$startDate = $data["startDate"];
			$endDate = $data["endDate"];
			$startDateTime = $data["startDate"]." 00:00:00";
			$endDateTime = $data["endDate"]." 23:59:59";
			$noOfUser = $data["no_of_user"];
			$pointsPerUser = $data["points_per_user"];
			$msisdn = array();
			$urlConcat = "";
			if(!empty($_GET['startDate'])){
				$startDate = $_GET['startDate']. " 00:00:00";
			} else {
				$startDate = date('Y-m-d'). " 00:00:00";
			}
			
			if(!empty($_GET['endDate'])){
				$endDate = $_GET['endDate']. " 23:59:59";
			} else {
				$endDate = date('Y-m-d'). " 23:59:59";
			}
			
			$queryString = "";
			if(isset($subscriber) && $subscriber == 1){
				$queryString .= " and t1.subscribe_status=1";
				$urlConcat .="&subscriber=1";
			}
			if(isset($view) && $view == 1 && isset($referral) && $referral == 1){
				$queryString .= " order by t1.view_count desc, t1.referral_count desc";
				$urlConcat .="&view=1&referral=1";
			} else if(isset($referral) && $referral == 1){
				$queryString .= " order by t1.referral_count desc";
			} else if(isset($view) && $view == 1){
				$queryString .= " order by t1.view_count desc";
				$urlConcat .="&view=1";
			} else {
				$queryString .= " order by t1.view_count desc, t1.referral_count desc";
			}
			
			if(isset($register) && $register == 1){
				$urlConcat .="&register=1";
			}
			
			$userData = $this->Activitylogcount->query("SELECT t1.msisdn FROM user_activity_count_log t1 LEFT JOIN user_activity_count_log t2 ON (t1.user_id = t2.user_id AND t1.id < t2.id) WHERE t2.id IS NULL and t1.msisdn != '' and t1.msisdn != 'NA' and t1.created>='".$startDate."' and t1.created<='".$endDate."'  $queryString limit 0,$noOfUser");
			
			$i = 0; 
			foreach($userData as $userDataRec){
				$msisdn[$i]["msisdn"] = $userDataRec["t1"]["msisdn"];
				$i++;
			}
			
			if(!empty($msisdn)){
				
				$msisdnJson = array();
				$msisdnJsonArr = array();
				foreach($msisdn as $msisdnRec){
				
					$msisdnJson[] = array("msisdn"=>$msisdnRec["msisdn"]);
					$msisdnJsonArr[] = $msisdnRec["msisdn"];
				}
				//$output = json_encode($msisdnJson);
				
				$actionType = 28;
				$appNameId = appNameId;
				//$action = "install";
				//$point = getV3mobiRewards($action, $appNameId);
				$point = $pointsPerUser;
				$note = 'Firstcut, Firstcut Admin Rewards,'.date( 'Y-m-d H:i:s').','.$point;
				error_log("REFERRAL | GIVEN ADMIN REWARDS POINT | actionType=".$actionType. " | appNameId=".$appNameId." | point=".$point. " | note=".$note."\n");
				$url = appReferalUrl.'get_rewardsadmin.php';
				$ch = curl_init($url);
				$json_data = array("msisdn"=>$msisdnJson, 'actionType'=>$actionType, 'appname'=>$appNameId, 'point'=>$point,'note'=>$note);
				$jsonDataEncoded = json_encode($json_data);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				$result = curl_exec($ch);
				 $response   = json_decode($result);
				error_log("REFERRAL | [GET GIVEN ADMIN REWARDS POINT] ".print_r($response,1)."\n");
				curl_close($ch);
				$implodeMsisdn = @implode(",", $msisdnJsonArr);
				
				$updateRec = $this->Activitylog->query("update user_activity_count_log set point_status=1, points='".$point."', modified='".date( 'Y-m-d H:i:s')."' where msisdn in ($implodeMsisdn)");
				$this->Session->setFlash('Points Updated Successfully.');$this->redirect('countreportdata?startDate='.$startDate.'&endDate='.$endDate.'$urlConcat');
			}
		}
	}
}
?>