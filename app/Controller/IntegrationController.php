<?php
App::uses('HttpSocket', 'Network/Http');
ini_set('memory_limit', '4024M');
ini_set('max_execution_time', 1000);
ini_set('error_log',BASE_LOG_PATH.'youtube/youtubeinsert.log');


class IntegrationController extends AppController
{
	public $helpers = array('Js');
	public $uses = array('Appnamelist', 'Fcappcontent', 'Fcappcategories', 'Country', 'User', 'Fcyoutubechannelcontent');

	public function youtubeadd()
	{
		$country = $this->Country->query("select id, country_name, country_code from cm_country_code where status=1");
		$this->set('country', $country);
		if($this->request->isPost())
		{
		    $data = $this->request->data;
			$qrycount = $this->Fcyoutubechannelcontent->find('first',array('conditions'=>array('Fcyoutubechannelcontent.youtube_channel_id'=>$data["youtube_channel_id"])));
		    if(count($qrycount) >= 1){//check if channel data all ready exist...
			
				$this->Session->setFlash('This channel content have already added.');
				$this->redirect('youtubeadd');  
			} else {
			
				if(isset($data["allContent"]) || !empty($data["allContent"])){
					/*$url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId='.$data["youtube_channel_id"].'&order=date&type=video&key='.YOUTUBEAPIKEY.'&maxResults=50';
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLINFO_HEADER_OUT, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$result     = curl_exec($ch);
					$responses   = json_decode($result,true);
					curl_close($ch);*/
					$youtube_channel_id = $data["youtube_channel_id"];
					
					$youtubeContents = $this->youtubeDataHit($youtube_channel_id);
					//echo "<pre>";
					//print_r($youtubeContents);
					//die;
					if(!empty($youtubeContents)){
						
						$sql = array();
						$currDate = date('Y-m-d H:i:s');
						//$i =0;
						foreach($youtubeContents as $key=>$val){
							
							foreach($val as $keyRec=>$response){
								$sql['category_id'] = $data["subcategory_id"];
								$sql['name'] = addslashes($response['snippet']['title']);
								$sql['description'] = addslashes($response['snippet']['description']);
								$sql['image_url'] = YOUTUBEIMAGEURL. $response['id']['videoId'] . YOUTUBEIMAGEURLBACK;
								$sql['video_url'] = YOUTUBEVIDEOURL. $response['id']['videoId'];
								$sql['video_id'] = $response['id']['videoId'];
								$sql['channel_id'] = $data["channel_id"];
								$sql['country_code'] = $data["country_code"];
								$sql['duration'] = $this->getDuration($response['id']['videoId']);
								$sql['youtube_channel_name'] = addslashes($response['snippet']['channelTitle']);
								$sql['youtube_channel_id'] = $data["youtube_channel_id"];
								$sql['created'] = $currDate;
								$sql['modified'] = $currDate;
								$sql['user_id'] = $this->Session->read('User.id');
								//echo $sql;
								//echo "<br/>";
								$this->Fcyoutubechannelcontent->create();			
								$this->Fcyoutubechannelcontent->save($sql);
								//$i++;
							}
							
							
						}
						//echo $i;
						//die;
						
						//echo'<pre>';print_r($dataset);echo'</pre>';
						//die;
					    $this->Session->setFlash('Content added successfully.');
						$this->redirect('youtubeadd');
					} else {//display error message...
						$this->Session->setFlash('This channel have no content.');
						$this->redirect('youtubeadd');  
					}
				
				} else {//redirect to next page...
					//$this->Session->setFlash('This channel have no content.');
					$youtube_channel_id = $data["youtube_channel_id"];
			        $responses = $this->youtubeDataHit($youtube_channel_id);
					if(!empty($responses)){
						$this->redirect('youtubeaddrecord/?youtube_channel_id='.$data["youtube_channel_id"].'&country_code='.$data["country_code"].'&subcategory_id='.$data["subcategory_id"].'&channel_id='.$data["channel_id"]); 
					} else {
						$this->Session->setFlash('Invalid Channel Id.');
						$this->redirect('youtubeadd');  
					}
				}
			}
			
		}
	}	 
	
	public function youtubeaddrecord(){
	
		if(!empty($_GET["youtube_channel_id"])){
			/*$url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId='.$_GET["youtube_channel_id"].'&order=date&type=video&key='.YOUTUBEAPIKEY.'&maxResults=50';
					
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$result     = curl_exec($ch);
			$responses   = json_decode($result,true);
			*/
			$youtube_channel_id = $_GET["youtube_channel_id"];
			$responses = $this->youtubeDataHit($youtube_channel_id);
			$this->set('youtubeContents', $responses);
			
		} else {
			$this->Session->setFlash('Somthing went wrong with url.');
			$this->redirect('youtubeadd'); 
		}
		
		if($this->request->isPost())
		{
			if(empty($_POST["video_id"])){
				$this->Session->setFlash('Select atleast one content.');
				$this->redirect($_SERVER['HTTP_REFERER']);
			} else {
			  	for($i=0; $i < count($_POST["video_id"]); $i++){
					$sql = array();
					$currDate = date('Y-m-d H:i:s');
					if(isset($_POST["video_id"][$i])){
					    
						$sql['category_id'] =  $_POST["subcategory_id"]["".$_POST["video_id"][$i].""];
						$sql['name'] = addslashes($_POST["name"]["".$_POST["video_id"][$i].""]);
						$sql['description'] = addslashes($_POST["description"]["".$_POST["video_id"][$i].""]);
						$sql['image_url'] = YOUTUBEIMAGEURL. $_POST["video_id"][$i] . YOUTUBEIMAGEURLBACK;
						$sql['video_url'] = YOUTUBEVIDEOURL. $_POST["video_id"][$i];
						$sql['video_id'] = $_POST["video_id"][$i];
						$sql['channel_id'] = $_POST["channel_id"]["".$_POST["video_id"][$i].""];
						$sql['country_code'] =  $_POST["country_code"]["".$_POST["video_id"][$i].""];
						$sql['duration'] = $this->getDuration($_POST["video_id"][$i]);
						$sql['youtube_channel_name'] = addslashes($_POST["youtube_channel_name"]["".$_POST["video_id"][$i].""]);
						$sql['youtube_channel_id'] = $_POST["youtube_channel_id"]["".$_POST["video_id"][$i].""];
						$sql['created'] = $currDate;
						$sql['modified'] = $currDate;
						$sql['user_id'] = $this->Session->read('User.id');
						
						$this->Fcyoutubechannelcontent->create();			
						$this->Fcyoutubechannelcontent->save($sql);
					}
				}
				$this->Session->setFlash('Content added successfully.');
			    $this->redirect('youtubeadd');
			}
		}
		
	}
	
	public function getchannelid(){
	}
	
	public function youtubeview(){
		
	   $pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		
		$searchCond = array();
		$channel = array();
		$cntChannel = array();
		
		$numOfPage = 0;
	   
		if(isset($this->request->query['searchForm']))	
		{ 			
			if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='youtube_channel_id') && (!empty($this->request->query['searchStr']))))
			{
				
				$searchCond[]='and youtube_channel_id like "%'.$this->request->query['searchStr'].'%"' ;
			}
			if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='youtube_channel_name') && (!empty($this->request->query['searchStr']))))
			{
				$searchCond[]='and youtube_channel_name="'.$this->request->query['searchStr'].'"' ;
			}
		}	  
		
		//Find all user id...
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$searchCondStr=implode(" ",$searchCond);
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
			$searchCondStr .=" and child.status = '".$_GET["filterBy"]."'" ;
		}
		$orderBy = "";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by id ".$_GET["sortBy"]."" ;			
		}
		
		$channel = $this->Fcyoutubechannelcontent->query("select youtube_channel_id, youtube_channel_name, created from youtube_channel_content where 1=1 $searchCondStr group by youtube_channel_id $orderBy limit  $offSet, $recPerPage");
			
		$cntChannel = $this->Fcyoutubechannelcontent->query("select count(id) as countRec from youtube_channel_content where 1=1 $searchCondStr group by youtube_channel_id");
		$num = count($cntChannel);
		$numOfPage=ceil($num/$recPerPage);
		$this->set(compact('channel','channel'));
			
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	
	}
	
	public function youtubeDelete($youtube_channel_id){
		$this->autoRender = false;
		$this->layout = false;
		$currDate = date('Y-m-d H:i:s');
		if(!empty($youtube_channel_id)){
			$this->Fcyoutubechannelcontent->query("update youtube_channel_content set delete_content=1, modified='".$currDate."' where youtube_channel_id='".$youtube_channel_id."'");
			$this->Fcappcontent->query("update cm_contents set status ='D', modified='".$currDate."' WHERE youtube_channel_id='".$youtube_channel_id."'");
			$this->Session->setFlash('Record deleted successfully.');
			$this->redirect('youtubeview');
		}
		$this->redirect('youtubeview');
	}
	
	public function youtubecontentview(){
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		
		$searchCond = array();
		$channel = array();
		$cntChannel = array();
		$num = 0;
		$NumofPage = 0;
	   
		
		if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='youtube_channel_id') && (!empty($this->request->query['searchStr']))))
		{
			
			$searchCond[]='and youtube_channel_id like "%'.$this->request->query['searchStr'].'%"' ;
		}
		if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='youtube_channel_name') && (!empty($this->request->query['searchStr']))))
		{
			$searchCond[]='and youtube_channel_name="'.$this->request->query['searchStr'].'"' ;
		}
		if((!empty($this->request->query['video_id']) && (!empty($this->request->query['video_id']))))
		{
			$searchCond[]='and video_id="'.$this->request->query['video_id'].'"' ;
		}
			  
		
		//Find all user id...
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$searchCondStr=implode(" ",$searchCond);
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
			$searchCondStr .=" and status = '".$_GET["filterBy"]."'" ;
		}
		$orderBy = "";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by id ".$_GET["sortBy"]."" ;			
		}
		
		//echo "select name, youtube_channel_id, youtube_channel_name, created from youtube_channel_content where 1=1 $searchCondStr";
		//die;
		//echo "select name, youtube_channel_id, youtube_channel_name, created from youtube_channel_content where 1=1 $searchCondStr $orderBy limit  $offSet, $recPerPage";
		//die;
		
		$channel = $this->Fcyoutubechannelcontent->query("select name, youtube_channel_id, youtube_channel_name, created from youtube_channel_content where 1=1 $searchCondStr $orderBy limit  $offSet, $recPerPage");
		
		$cntChannel = $this->Fcyoutubechannelcontent->query("select count(id) as countRec from youtube_channel_content where 1=1 $searchCondStr");
		//$num = count($cntChannel);
		$num = $cntChannel[0][0]["countRec"];
		
		$numOfPage=ceil($num/$recPerPage);
		$this->set(compact('channel','channel'));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	}
	
	public function youtubeDataHit($youtube_channel_id, $nextPageToken = NULL, $dataToSave = array()){
	    $url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId='.$youtube_channel_id.'&order=date&type=video&key='.YOUTUBEAPIKEY.'&maxResults=50&pageToken='.$nextPageToken.'';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$result     = curl_exec($ch);
		$responses   = json_decode($result,true);
		curl_close($ch);
		//echo "<pre>";
		//print_r($responses);
		//die;
		$nextPageToken = @$responses["nextPageToken"];
		error_log("Next Page Token Id=".$nextPageToken);
		if(empty($nextPageToken) && !empty($responses["items"])){
			$dataToSave[] = $responses["items"];
		} else{
			while(isset($nextPageToken))
			{
			
				$dataToSave[] = $responses["items"];
				return $dataToSave = $this->youtubeDataHit($youtube_channel_id, $nextPageToken, $dataToSave);
			}
		}
		//echo "<pre>";
		//print_r($dataToSave);
		//die;
		return $dataToSave;
	}
	
	public function getDuration($vidID){
	   $vidkey = $vidID ;
	   $apikey = "AIzaSyCz2PEZszqb9WgonuYmFwlMOr0Rdp0prSk";
	   $dur = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$vidkey&key=$apikey");
	   $VidDuration =json_decode($dur, true);
	   foreach ($VidDuration['items'] as $vidTime)
	   {
		   $VidDuration= $vidTime['contentDetails']['duration'];
	   }
	   preg_match_all('/(\d+)/',$VidDuration,$parts);
	   /*$hours = intval(floor($parts[0][0]/60) * 60 * 60);
	   $minutes = intval($parts[0][0]%60 * 60);
	   $seconds = @intval($parts[0][1]);*/
	   
	   $hours = 0;
	   $minutes = 0;
	   $seconds = 0;
	   if(count($parts[0]) ==3){
		   $hours = intval(floor($parts[0][0])*60*60);
		   $minutes = intval(floor($parts[0][1])*60);
		   $seconds = @intval(floor($parts[0][2]));
	   } else if(count($parts[0]) ==2){
		   $minutes = intval(floor($parts[0][0])*60);
		   $seconds = @intval(floor($parts[0][1]));
	   } else {
		   $seconds = @intval(floor($parts[0][0]))*60;
	   }
	   
	   $totalSec = $hours + $minutes + $seconds; //this is the example in seconds
	   return $totalSec;
	}

	public function getImages($youtubeId){
		$url = 'http://img.youtube.com/vi/' . $youtubeId . '/0.jpg';
		$img = dirname(__FILE__) . '/downloadImages/youtubeThumbnail_'  . $youtubeId . '.jpg';
		file_put_contents($img, file_get_contents($url));
	}
	public function youtubeaddexcel()
	{
		$country = $this->Country->query("select id, country_name, country_code from cm_country_code where status=1");
		$this->set('country', $country);
		if($this->request->isPost())
		{
		    $data = $this->request->data;
			$youtube_channel_id = $data["youtube_channel_id"];
			$category_id = $data["subcategory_id"];
			$channel_id = $data["channel_id"];
			$country_code = $data["country_code"];
			$user_id = $this->Session->read('User.id');
			$channelname = $this->Fcyoutubechannelcontent->query("select c_name from ch_channel where id = '".$channel_id."' and user_id = '".$user_id."' and country_code = '".$country_code."' and  status=1");
		    
			//echo "<pre>";
			//print_r($_FILES);
			//die;
			$youtube_channel_name = $channelname[0]['ch_channel']['c_name'];
			
			$filename = $_FILES['csvfile']['name'];

			
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			$target_file=DOM_PUBLICHTML_PATH."utubeexcel/".date('YmdHis')."_$filename";
			if($imageFileType=='csv')
			{
				if (move_uploaded_file($_FILES['csvfile']['tmp_name'],$target_file)) 
				{
					$txtfile_n = explode("/",$target_file); 
					
					$txtfile  = explode(".",$target_file);
					$txtfile_name = $txtfile[0];
					$myfile = fopen($txtfile_name.'.txt', "w") or die("Unable to open file!");
					$file = $_FILES['csvfile']['tmp_name'];
					$handle = fopen($target_file,"r"); 
					$data = fgetcsv($handle,1000,",","'");
					$c=count($data);
					if($c == 1) 
				    {
						$this->Fcyoutubechannelcontent->query("INSERT into utube_videoids_excel set youtube_channel_id='".$youtube_channel_id."',youtube_channel_name='".$youtube_channel_name."',user_id='".$user_id."',category_id='".$category_id."',excelname ='".$txtfile_n[5]."',channel_id ='".$channel_id."',status ='0'");
						
						while($data = fgetcsv($handle,1000,",","'")) 
					    {
							if ($data[0]) 
						    {
								$this->Fcyoutubechannelcontent->query("INSERT into utube_videoids set youtube_channel_id='".$youtube_channel_id."',youtube_channel_name='".$youtube_channel_name."',user_id='".$user_id."',category_id='".$category_id."',video_id ='".$data[0]."',channel_id ='".$channel_id."',status ='1'");
								
								$this->Fcyoutubechannelcontent->query("update utube_videoids_excel set status ='1' where excelname = '".$txtfile_n[5]."'");
								
							}
							
						}
					}
					
					
				  	$this->Session->setFlash('CSV uploaded successfully.');
					$this->redirect('youtubeaddexcel');	
					
				}
			}
			else 
			{
				$this->Session->setFlash('Unacceptable file type: upload only csv');
				$this->redirect('youtubeaddexcel'); 	  
			}
		}
	}
}


?>
