<?php
App::uses('AppController', 'Controller');
class MetatagsController extends AppController{
	public $name = 'Metatags';
	public $uses = array('Metatags', 'Fcappcategories');
	
	public function view($page=0)
    {
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$searchCond = array();
		$casts = array();
		$cntCasts = array();
		$num = 0;
		$NumofPage = 0;
		$searchCond[] = "1=1";
		if(isset($this->request->query['searchForm']))	
		{
			if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='title') && (!empty($this->request->query['searchStr']))))
			{
				
				$searchCond[]=' title like "%'.$this->request->query['searchStr'].'%"' ;
			}
			if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='category_id') && (!empty($this->request->query['searchStr']))))
			{
				$searchCond[]=' category_id="'.$this->request->query['searchStr'].'"' ;
			}
		}
		
		$searchCondStr = implode(" and ",$searchCond);
		
		$metatags = $this->Metatags->query("select * from page_meta_management where $searchCondStr order by id desc");
		$cntMetatags = $this->Metatags->query("select count(id) as countRec from page_meta_management where $searchCondStr");
		
		
		$num = @$cntMetatags[0][0]["countRec"];
		$NumofPage=ceil($num/$RowofPerpage);
		$this->set(compact('metatags','metatags'));
			
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
	}
	
	public function add()
    {
	
		if($this->request->isPost())
		{   $sql = array(); 
			$data = $this->request->data;
			$currDate = date('Y-m-d H:i:s');
			$sql["category_id"] = $data["category_id"];
			$sql["title"] = $data["title"];
			$sql["name"] = $data["name"];
			$sql["content"] = $data["content"];
			$sql["type"] = $data["type"];
			$sql["created"] = $currDate;
			$sql["modified"] = $currDate;
			
			$this->Metatags->save($sql);
			$lastInsertId =  $this->Metatags->id;
			$this->Session->setFlash('Metatag added successfully.');
			$this->redirect('view');
			
		} 
	}
	
	public function edit($id=null)
    {
		$metatags = $this->Metatags->query("select * from page_meta_management where id='".$id."'");
		$this->set('metatags', $metatags);
		$data = $this->request->data;
		if(!empty($data)){
			$currDate = date('Y-m-d H:i:s');
			$sql = "update page_meta_management set ";
			$sql = $sql . " category_id = '" .  $data["category_id"] . "'";
			$sql = $sql . ", title = '" . $data["title"] . "'";
			$sql = $sql . ", name = '" . $data["name"] . "'";
			$sql = $sql . ", content = '" . $data["content"] . "'";
			$sql = $sql . ", type = '" . $data["type"] . "'";
			$sql = $sql . ", modified = '" . $currDate . "'";
			$sql = $sql . " where id = '". $data["id"] ."'";
			
			$update = $this->Metatags->query($sql);
			
			$this->Session->setFlash('Metatag edit successfully.');
			$this->redirect('view');
		}
	}
	
	public function adddefault()
    {
		$categories = $this->Fcappcategories->query("select category_id from cm_categories where status='A' and project='firstcut'");
		$this->set('categories', $categories);
		if($this->request->isPost())
		{   $sql = array(); 
			$sql1 = array(); 
			$sql2 = array(); 
			$data = $this->request->data;
			$currDate = date('Y-m-d H:i:s');
			
			if(isset($data["title_content"]) && !empty($data["title_content"])){
			$this->Metatags->create();
				$sql["category_id"] = $data["category_id"];
				$sql["title"] = $data["title"];
				$sql["name"] = 'title';
				$sql["content"] = $data["title_content"];
				$sql["type"] = $data["type"];
				$sql["created"] = $currDate;
				$sql["modified"] = $currDate;
				$this->Metatags->save($sql);
			}
			
			if(isset($data["keywords_content"]) && !empty($data["keywords_content"])){
			$this->Metatags->create();
				$sql1["category_id"] = $data["category_id"];
				$sql1["title"] = $data["title"];
				$sql1["name"] = 'keywords';
				$sql1["content"] = $data["keywords_content"];
				$sql1["type"] = $data["type"];
				$sql1["created"] = $currDate;
				$sql1["modified"] = $currDate;
				$this->Metatags->save($sql1);
			}
			
			if(isset($data["description_name"]) && !empty($data["description_name"])){
			$this->Metatags->create();
				$sql2["category_id"] = $data["category_id"];
				$sql2["title"] = $data["title"];
				$sql2["name"] = 'description';
				$sql2["content"] = $data["description_name"];
				$sql2["type"] = $data["type"];
				$sql2["created"] = $currDate;
				$sql2["modified"] = $currDate;
				$this->Metatags->save($sql2);
			}
			
			$this->Session->setFlash('Metatag added successfully.');
			$this->redirect('viewdefault');
			
		} 
	}
	
	public function viewdefault($page=0)
    {
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$searchCond = array();
		$casts = array();
		$cntCasts = array();
		$num = 0;
		$NumofPage = 0;
		$searchCond[] = "type = 'defaults'";
		if(isset($this->request->query['searchForm']))	
		{
			if((!empty($this->request->query['searchIn']) && ($this->request->query['searchIn'] =='category_id') && (!empty($this->request->query['searchStr']))))
			{
				$searchCond[]=' category_id="'.$this->request->query['searchStr'].'"' ;
			}
		}
		
		$searchCondStr = implode(" and ",$searchCond);
		
		$metatags = $this->Metatags->query("select * from page_meta_management where $searchCondStr group by category_id order by id desc");
		$cntMetatags = $this->Metatags->query("select count(id) as countRec from page_meta_management where $searchCondStr group by category_id");
		
		
		$num = @$cntMetatags[0][0]["countRec"];
		$NumofPage=ceil($num/$RowofPerpage);
		$this->set(compact('metatags','metatags'));
			
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
	}
	
	public function editdefault($id=null)
    {
		
		$metatags = $this->Metatags->query("select * from page_meta_management where category_id='".$id."' and type='defaults'");
		$this->set('metatags', $metatags);
		if($this->request->isPost())
		{   
			$data = $this->request->data;
			$deleteRec = $this->Metatags->query("delete from page_meta_management where category_id = '".$data["category_id"]."'");
			
			//$this->Metatags->deleteAll(array('Metatags.category_id'=>$id));
			$sql = array(); 
			$sql1 = array(); 
			$sql2 = array(); 
			
			$currDate = date('Y-m-d H:i:s');
			
			if(isset($data["title_content"]) && !empty($data["title_content"])){
			$this->Metatags->create();
				$sql["category_id"] = $data["category_id"];
				$sql["title"] = $data["title"];
				$sql["name"] = 'title';
				$sql["content"] = $data["title_content"];
				$sql["type"] = $data["type"];
				$sql["created"] = $currDate;
				$sql["modified"] = $currDate;
				$this->Metatags->save($sql);
			}
			
			if(isset($data["keywords_content"]) && !empty($data["keywords_content"])){
			$this->Metatags->create();
				$sql1["category_id"] = $data["category_id"];
				$sql1["title"] = $data["title"];
				$sql1["name"] = 'keywords';
				$sql1["content"] = $data["keywords_content"];
				$sql1["type"] = $data["type"];
				$sql1["created"] = $currDate;
				$sql1["modified"] = $currDate;
				$this->Metatags->save($sql1);
			}
			
			if(isset($data["description_name"]) && !empty($data["description_name"])){
			$this->Metatags->create();
				$sql2["category_id"] = $data["category_id"];
				$sql2["title"] = $data["title"];
				$sql2["name"] = 'description';
				$sql2["content"] = $data["description_name"];
				$sql2["type"] = $data["type"];
				$sql2["created"] = $currDate;
				$sql2["modified"] = $currDate;
				$this->Metatags->save($sql2);
			}
			
			$this->Session->setFlash('Metatag edit successfully.');
			$this->redirect('viewdefault');
			
		}
	}
	public function add_meta()
	{
	    $categories = $this->Fcappcategories->query("select category_id from cm_categories where status='A' and project='firstcut'");
		$this->set('categories', $categories);
		if($this->request->isPost())
		{
            $sql = array();
			$data = $this->request->data;
			$currDate = date('Y-m-d H:i:s');
			$sql["category_id"] = $data["category_id"];
			$sql["title"] = $data["title"];
			$sql["name"] = $data["name"];
			$sql["content"] = $data["content"];
			$sql["type"] = $data["type"];
            $sql["created"]	= $currDate;
			$sql["modified"] = $currDate;
			$this->Metatags->save($sql);
			$this->Session->setFlash('Metatag added successfully.');
			//$this->redirect('viewdefault');
			
		}
	}
		public function social_meta_tags()
	    {
		    $metatags = $this->Metatags->query("select title,category_id,count(category_id) as nooftype from page_meta_management group by category_id order by id desc");
		    $this->set('metatags', $metatags);
	    }
		public function social_meta_tags_edit($id=null)
	{
		
		$nooftype = $this->Metatags->query("select id,category_id,name,content,type from page_meta_management  where category_id='".$id."'");
		$this->set('nooftype', $nooftype);
		if($this->request->isPost())
		{
			$sql = array();
			$data = $this->request->data;
			
			$countRec = count($data['id']);
			//echo "<pre>";
			//print_r($data);
			//echo "<br>";
			//echo $countRec;
			//die;
			for($j=0;$j<$countRec;$j++)
			{
				//$sql['id'] = $data['id'][$j];
				//$sql['category_id'] = $data['category_id'][$j];
				//$sql['name'] = $data['name'][$j];
				//$sql['content'] = $data['content'][$j];
				//$sql['type'] = $data['type'][$j];
				$sql = "update page_meta_management set ";
				$sql = $sql . " name = '" .  $data["name"][$j] . "'";
				$sql = $sql . " ,content = '" .  $data["content"][$j] . "'";
			    $sql = $sql . " ,type = '" .  $data["type"][$j] . "'";
				$sql = $sql . " where id = '". $data["id"][$j] ."'";
				$update = $this->Metatags->query($sql);
			    	
			}
			$this->Session->setFlash('Details edited successfully.');
			//echo "<pre>";
			//print_r($sql);
			//die;
			
			
		}
	}
	
	
	
}

?>