<?php
App::uses('Component', 'Controller');
class UserInfoFromHeadersComponent extends Component{
	public $request;
        public $response;
        public $_methods;
        public $_Collection;
        public $Session;
	public $settings = array(
             //   'model' => 'Content',
		'msisdn'=>array(
			'X-MDN',
			'x-mdn',
			'HTTP_X_MDN',
			'x-up-calling-line-id',
			'X-UP-CALLING-LINE-ID',
			'x-msisnd',
			'X-MSISDN',
			'x-msisdn',
			'X-Msisdn',
			'x-up-calling-line-id'
		)
        );
	public $msisdn = null;
	//public $msisdn = '9599306217';
	public $clientIp = 'NA';
	public $userAgent = 'NA';
	public $isValid = false;
	public function __construct(ComponentCollection $collection, $settings = array()){
                $this->_Collection = $collection;
                $this->settings = Hash::merge($this->settings, $settings);
        }
	public function initialize(Controller $controller){
                $this->request = $controller->request;
                $this->response = $controller->response;
                $this->_methods = $controller->methods;
                $this->Session = $controller->Session;
        }
	public function headerInfo(){
		CakeLog::info('headers ['.print_r(getallheaders(),true).']','debug');
		$this->clientIp = $this->request->clientIp($safe = true);
		$this->userAgent = $this->request->header('User-Agent');
		echo $this->getMsisdn();
		
		if(!isset($this->msisdn)){
			$this->msisdn = $this->Session->read('User.msisdn');
		}
		$this->validateMsisdn();
		CakeLog::info('msisdn ['.$this->msisdn.'] clientIp ['.$this->clientIp.']','debug');
		CakeLog::info('msisdn ['.$this->msisdn.'] userAgent ['.$this->userAgent.']','debug');
	}
	public function getMsisdn(){
		foreach($this->settings['msisdn'] as $key => $value){
			 $umsisdn = $this->request->header($value); 
			if(!empty($umsisdn)){
				$this->msisdn = $umsisdn;
				break;
			}
		}
        }
	public function validateMsisdn($msisdn = null){
		if(!empty($msisdn)){$this->msisdn = $msisdn;}
	   $this->isValid   = preg_match('/^[0-9]{10,12}$/', $this->msisdn);
       CakeLog::info('msisdn valid response['.$this->isValid.']','debug');

	}
}
?>
