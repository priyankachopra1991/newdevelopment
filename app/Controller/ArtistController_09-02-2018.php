<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'S3.php');
App::import('Vendor', 'sdk/sdk.class.php');
App::import('Vendor', 'AmazonS3.php');

class ArtistController extends AppController{
	public $name = 'Artist';
	public $uses = array('Artist', 'Country', 'User', 'Content', 'Cast', 'Artistinfo', 'Contentnotification');
	public $components = array('Paginator');
	public function beforeFilter() {
	parent::beforeFilter();
	
	}
	
	public function viewartist($page=0)
	{
		
		$country = $this->Country->find('all',array('conditions'=>array('Country.status'=>'1')));
		$this->set('country', $country);
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		$orderBy = "";
		//$countryCode = "IN";
		$countryCode = "";
		$searchCondStr = " status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && $_GET['filterBy'] != ""){
			$searchCondStr .=" and status = '".$_GET["filterBy"]."'" ;
		}
		
		$orderBy = "";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by id ".$_GET["sortBy"]."" ;			
		}
		$artists = $this->Artist->query("select * from ar_artist where $searchCondStr $orderBy limit $offSet, $recPerPage");
		//debug($artists);	
		foreach($artists as $key=>$val){
			$artistVideoCount = $this->contentDetailArtist($artists[$key]["ar_artist"]["id"], $countryCode);
			$artists[$key]["ar_artist"]["videoCount"] = $artistVideoCount[0][0]["videoCount"];
			$artists[$key]["ar_artist"]["videoViewCount"] = $artistVideoCount[0][0]["videoViewCount"];
			
		}
		
		$artistCount = $this->Artist->query("select count(id) as countRec from ar_artist where $searchCondStr");
		
		$totalRec = $artistCount[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		
		$this->set(compact('artists', $artists));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
		
	}
	
	public function addartist(){
		$casts = $this->Cast->find('all');
		$this->set('casts', $casts);
		$country = $this->Country->find('all',array('conditions'=>array('Country.status'=>'1')));
		$this->set('country', $country);
		if($this->request->isPost())
		{	
			$data = $this->request->data;
			
			if(!empty($data["facebookId"])){
				$checkArtistSocialInfo = $this->Artistinfo->query("SELECT id FROM `ar_artists_socialInfo` where facaebookId='".$data["facebookId"]."' ");
				
				if(count($checkArtistSocialInfo) > 0 ){
					$this->Session->setFlash('This facebook id is used by another artist.');
					$this->redirect('addartist');
				}
	
			} else {
				$sql = array();
				
				$sql["name"] = $data["name"];
				$sql["decription"] = $data["decription"];
				$explodeRole = explode("~", $data["roleId"]);
				$sql["roleId"] = $explodeRole[0];
				$sql["role"] = $explodeRole[1];
				$sql["country_code"] = $data["country_code"];
				$sql["tagline"] = $data["tagline"];
				$sql["dob"] = $data["dob"];
				$sql["status"] = 0;
				$sql["user_id"] = $this->Session->read('User.id');;
				
				$sql["created"] = CURRDATE;
				$sql["modified"] = CURRDATE;
				$time = time();
				if(!empty($data["coverImg"]["name"]))
				{//artist cover image upload...
					$file1 = $data['coverImg']; 
					$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
					/*if(file_exists(UPLOADPATH . 'artist/'.$data['hiddenimage1'])){
						
						unlink(UPLOADPATH . 'artist/'.$data['hiddenimage1']);	
					}*/
					
					move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'artist/' . "c1_".$time.".".$imageFileType1);
					$coverImg = "c1_".$time.".".$imageFileType1;
					$sql["coverImg"] = $coverImg;
					$coverImg = UPLOADPATH . 'artist/'. $coverImg;
					chmod($coverImg, 0777);
					
					if(file_exists($coverImg)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $coverImg);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						
						unlink($coverImg);
						 
					}
				}
				
				if(!empty($data["userImg"]["name"]))
				{//artist cover image upload...
					$file2 = $data['userImg']; 
					$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
					/*if(file_exists(UPLOADPATH . 'artist/'.$data['hiddenimage2'])){
						
						unlink(UPLOADPATH . 'artist/'.$data['hiddenimage2']);	
					}*/
					
					move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'artist/' . "c2_".$time.".".$imageFileType2);
					$userImg = "c2_".$time.".".$imageFileType2;
					$sql["userImg"] = $userImg;
					$userImg = UPLOADPATH . 'artist/'. $userImg;
					chmod($userImg, 0777);
					
					if(file_exists($userImg)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $userImg);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						
						unlink($userImg);
						 
					}
				}
				
				$this->Artist->save($sql);
				$lastInsertId =  $this->Artist->id; 
				$sql1 = array();
				$sql1["artistId"] = $lastInsertId;
				$sql1["facaebookId"] = $data["facaebookId"];
				$sql1["facebookUrl"]="http://www.facebook.com/{CLICK_ID}";
				$sql1["facebookPageId"] = $data["facebookPageId"];
				$sql1["googleId"] = $data["googleId"];
				$sql1["tweeterId"] = $data["tweeterId"];
				$sql1["tweeterUrl"]="https://twitter.com/https://twitter.com/{CLICK_ID}";
				$sql1["user_id"] = $this->Session->read('User.id');
				$sql1["created"] = CURRDATE;
					
				$this->Artistinfo->save($sql1);
				
				$this->Session->setFlash('Artist updated successfully.');
				$this->redirect('viewartist');
			}
				
		}
	}
	
	public function editartist($id=null){
		$searchCondStr = "id ='".$id."'";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		$artists = $this->Artist->query("select * from ar_artist where $searchCondStr");
		if(count($artists) <= 0 ){
			$this->Session->setFlash('You have no permission to edit this artist.');
			$this->redirect('viewartist');
		} else {
			
			$this->set(compact('artists', $artists));
			$artistInfo = $this->Artistinfo->query("select * from ar_artists_socialInfo where artistId='".$id."'");
			$this->set(compact('artistInfo', $artistInfo));
			$casts = $this->Cast->find('all');
			$this->set('casts', $casts);
			$country = $this->Country->find('all',array('conditions'=>array('Country.status'=>'1')));
			$this->set('country', $country);
			
			//edit artist section start here...
			if($this->request->isPost())
			{	
				$data = $this->request->data;
				
				if(!empty($data["artistInfoId"]) && !empty($data["facebookId"])){
					$checkArtistSocialInfo = $this->Artistinfo->query("SELECT id FROM `ar_artists_socialInfo` where facaebookId='".$data["facebookId"]."' and artistId !='".$id."'");
					
					if(count($checkArtistSocialInfo) > 0 ){
						$this->Session->setFlash('This facebook id is used by another artist.');
						$this->redirect('editartist/'.$id);
					}
		
				} else {
					$sql1 = array();
					$sql1["id"] = $data["artistInfoId"];
					$sql1["facaebookId"] = $data["facaebookId"];
					$sql1["facebookUrl"]="http://www.facebook.com/{CLICK_ID}";
					$sql1["facebookPageId"] = $data["facebookPageId"];
					$sql1["googleId"] = $data["googleId"];
					$sql1["tweeterId"] = $data["tweeterId"];
					   $sql1["tweeterUrl"]="https://twitter.com/https://twitter.com/{CLICK_ID}";
					if(empty($data["artistInfoId"])){
						$sql1["user_id"] = $this->Session->read('User.id');
						$sql1["created"] = CURRDATE;
						
					}
					$this->Artistinfo->save($sql1);
					
					$sql = array();
					$sql["id"] = $data["id"];
					$sql["name"] = $data["name"];
					$sql["decription"] = $data["decription"];
					$explodeRole = explode("~", $data["roleId"]);
					$sql["roleId"] = $explodeRole[0];
					$sql["role"] = $explodeRole[1];
					$sql["country_code"] = $data["country_code"];
					$sql["tagline"] = $data["tagline"];
					$sql["dob"] = $data["dob"];
					if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO"))
					{
						$sql["status"] = $data["status"];
						$sql["approved_by"] = $this->Session->read('User.id');
						//insert data in user notification...
						if($data["status"] != $data["oldstatus"]){
							$sql1 = array();
							$sql1["approved"] = $data["status"];
							$sql1["created"] = CURRDATE;
							$sql1["approved_by"] = $this->Session->read('User.id');
							$sql1["c_type"] = 3;
							$sql1["c_name"] = $data["name"];
							$sql1["c_id"] = $data["id"];
							$sql1["user_id"] = $data["user_id"];
							$this->Contentnotification->save($sql1);

						}
						
						
					}
					//$sql["created"] = CURRDATE;
					$sql["modified"] = CURRDATE;
					$time = time();
					if(!empty($data["coverImg"]["name"]))
					{//artist cover image upload...
						$file1 = $data['coverImg']; 
						$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
						if(file_exists(UPLOADPATH . 'artist/'.$data['hiddenimage1'])){
							
							unlink(UPLOADPATH . 'artist/'.$data['hiddenimage1']);	
						}
						
						move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'artist/' . "c1_".$time.".".$imageFileType1);
						$coverImg = "c1_".$time.".".$imageFileType1;
						$sql["coverImg"] = $coverImg;
						$coverImg = UPLOADPATH . 'artist/'. $coverImg;
						chmod($coverImg, 0777);
						
						if(file_exists($coverImg)){//banner move code start here...
								//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
							$files_arr = explode("\n", $coverImg);
							$s3 = new S3(ACCESS_KEY,SECRET_KEY);
							
							foreach($files_arr as $file1){
								if(!empty($file1)){
								
									$bucketname = FIRSTPOSTERBUCKET;
									$file1_arr = explode("/",$file1);
									$filename1 = end($file1_arr);
									$main_arr = array_splice($file1_arr, 6);
									$bucket_file = implode("/",$main_arr); 
									$image_type = image_type_to_mime_type(exif_imagetype($file1));
									$header = array('Content-Type' =>$image_type);
									$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
									
									//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
									if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
										//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
									}
								}
							}
							
							unlink($coverImg);
							 
						}
					}
					
					if(!empty($data["userImg"]["name"]))
					{//artist cover image upload...
						$file2 = $data['userImg']; 
						$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
						if(file_exists(UPLOADPATH . 'artist/'.$data['hiddenimage2'])){
							
							unlink(UPLOADPATH . 'artist/'.$data['hiddenimage2']);	
						}
						
						move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'artist/' . "c2_".$time.".".$imageFileType2);
						$userImg = "c2_".$time.".".$imageFileType2;
						$sql["userImg"] = $userImg;
						$userImg = UPLOADPATH . 'artist/'. $userImg;
						chmod($userImg, 0777);
						
						if(file_exists($userImg)){//banner move code start here...
								//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
							$files_arr = explode("\n", $userImg);
							$s3 = new S3(ACCESS_KEY,SECRET_KEY);
							
							foreach($files_arr as $file1){
								if(!empty($file1)){
								
									$bucketname = FIRSTPOSTERBUCKET;
									$file1_arr = explode("/",$file1);
									$filename1 = end($file1_arr);
									$main_arr = array_splice($file1_arr, 6);
									$bucket_file = implode("/",$main_arr); 
									$image_type = image_type_to_mime_type(exif_imagetype($file1));
									$header = array('Content-Type' =>$image_type);
									$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
									
									//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
									if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
										//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
									}
								}
							}
							
							unlink($userImg);
							 
						}
					}
					
					$this->Artist->save($sql);
					$this->Session->setFlash('Artist updated successfully.');
					$this->redirect('viewartist');
				}
				
			}
		}
		
	}
	
	public function detailartist($id=null){
		$searchCondStr = "id='".$id."' and status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		$artists = $this->Artist->query("select * from ar_artist where $searchCondStr");
		if(count($artists) <= 0 ){
			$this->Session->setFlash('You have no permission to view this artist.');
			$this->redirect('viewartist');
		} else {
			$this->set(compact('artists', $artists));
		}
	}
	
	public function deleteartist($id=null){
		$searchCondStr = "id='".$id."' and status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		$artists = $this->Artist->query("select id from ar_artist where $searchCondStr");
		if(count($artists) <= 0 ){
			$this->Session->setFlash('You have no permission to delete this artist.');
			$this->redirect('viewartist');
		} else {
			$artistUpdate = $this->Artist->query("update ar_artist set status='4', modified='".CURRDATE."' where id='".$id."'");
			$this->Session->setFlash('Artist deleted successfully.');
			$this->redirect('viewartist');
		}
	}
	
	public function addcontentartist(){
		$searchCondStr = "status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$artists = $this->Artist->query("select id, name from ar_artist where $searchCondStr");
		$this->set(compact('artists', $artists));
		
	}
	
	public function contentDetailArtist($artistId, $countryCode=null){
		$countryCond = "";
		if(!empty($countryCode)){
			$countryCond = "and t2.country_code='".$countryCode."'";
		}
		
		$contentCount = $this->Content->query("select count(t2.content_id) as videoCount, sum(t2.view) as videoViewCount from ar_artist_contents as t1 inner join cm_contents as t2 on t2.content_id = t1.content_id inner join cm_content_lang as t3 on t3.content_id = t2.content_id where t2.status='A' and t2.approved=1 $countryCond and t1.artist_id = '".$artistId."'
		and t2.category_id in (select category_id from cm_categories where status='A' and project='firstcut')");
		return $contentCount;
	}
	
}
?>
