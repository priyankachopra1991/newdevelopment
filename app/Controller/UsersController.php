<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController{
	public $name = 'Users';
	public $uses = array('User', 'Menu', 'Contentusedreport', 'Fcappcontent', 'Contentnotification');
	public $components = array('Paginator');
	public function beforeFilter() {
	parent::beforeFilter();
	
	}
	public function login()
    { 
	    $this->layout = 'login';
		$data = $this->request->data;
		
		if(!empty($data["email"]))
		{
			$qrycount = $this->User->find('first', array('conditions' => array('email' => trim($data["email"]), 'password' => md5(trim($data["password"])))));
			
			if(count($qrycount) <= 0)
			{
				$this->Session->setFlash('Email or password is not matching. Please check and try again.');
				$this->redirect('login'); 
			} else if($qrycount["User"]["status"] == "B") {
				$this->Session->setFlash(' Your account under verification process! Please try again.');
				$this->redirect('login'); 
			} else if($qrycount["User"]["status"] == "C") {
				$this->Session->setFlash('Your account is rejected. Please contact to administrator.');
				$this->redirect('login');
			} else if($qrycount["User"]["status"] == "D") {
				$this->Session->setFlash('Please verify your email for login.');
				$this->redirect('login');
			} else if($qrycount["User"]["status"] == "I") {
				$this->Session->setFlash('Deactivate by the Admin User.');
				$this->redirect('login');
			} else {	
				foreach($qrycount as $row)
				{
					$this->Session->write('User.id', $row['id']);
					$this->Session->write('User.name', $row['name']);
					$this->Session->write('User.email', $row['email']);
					$this->Session->write('User.user_id', $row['user_id']);
					$this->Session->write('User.user_type', $row['user_type']);
					$this->Session->write('User.phone_number', $row['phone_number']);
					$this->Session->write('User.status', $row['status']);
					$this->Session->write('User.company', $row['company']);
					$this->Session->write('User.created', $row['created']);
					$this->Session->write('User.content_deliver_type', $row['content_deliver_type']);
					$this->Session->write('User.menu_id', $row['menu_id']);
					
					
					$this->redirect('dashboard');
					
				}
			}
		}
	}
	
	public function logout()
    {
		$this->Session->destroy();
		$this->Cookie->delete('userView');
		$this->redirect('login');
	}
	
	public function register()
    { 
		$this->layout = 'signup';
		$data = $this->request->data;
		if(!empty($data["email"]))
		{
			$qrycount = $this->User->find('first',array('conditions'=>array('User.email'=>$data['email'])));
			if(count($qrycount)>0)
		    {
				$this->Session->setFlash('This email is already register.');
				$this->redirect('register'); 
			} else {
				$sql = array();
				$sql["name"] = trim($data["name"]);
				$sql["email"] = trim($data["email"]);
				$sql["password"] = md5(trim($data["password"]));
				$sql["user_type"] = trim($data["usertype"]);
				$sql["status"] = 'D';
				$this->User->save($sql);
				$lastInsertId =  $this->User->id;
				
				//email section start here...
				$mailContent ="Hello ".trim($data["name"])."". ",<br />";
				$mailContent .="You have successfully registered with us. Please verify your email id to activate your profile.<br/>";
				$mailContent .="Email: = ".trim($data["email"])."<br/>";
				$mailContent .="Password: = ".trim($data["password"])."<br/>";
				$mailContent .="Please click on the link below to verify your email.<br/>";
				$emailUrl = BASE_URL.'/users/emailverify/'.base64_encode($lastInsertId);
				//$mailContent .="<a href=".BASE_URL.'/users/emailverify/'.base64_encode($lastInsertId).">".BASE_URL.'/pages/emailverify/'.base64_encode($lastInsertId)."</a><br/>";
				
				//$mailContent .='<a href=\".$emailUrl."\>Click Here</a>';
				$mailContent .='<a href ="'.$emailUrl.'">Click Here To Verify Your Email</a>';
				$mailContent .="<br/>Thanks<br/>";
				$mailContent = nl2br($mailContent);
				$emailFrom = ADMIN_EMAIL;
				$nmFrom = ADMIN_NAME;
				$emailTo = trim($data["email"]);
				$nmTo = trim($data["name"]);
				$subject = "Signup welcome email";
		
		
		        $this->User->_mail($emailFrom, $nmFrom, $emailTo, $nmTo, $subject . " - " . SITE_NAME, trim($mailContent));
				$this->Session->setFlash('Thankyou for registering with us. Please verify your email to continue. Check in spam box also.');
				$this->redirect('register'); 
			}
		} 
	}
	
	public function emailverify($id=null){
		if(!empty($userid)){
			$newUserId = base64_decode($id);
			$qrycount = $this->User->find('first',array('conditions'=>array('User.id'=>$newUserId)));
			if(count($qrycount) <= 0){
				$this->Session->setFlash('Something went wrong with url.');
				$this->redirect('login'); 
			} else {
				$this->User->updateAll(array("status"=>"'B'"),array("user_id"=>$id));
				
				//mail section start here...
				
				$mailContent ="Hello ".ADMIN_NAME.",<br />";
				$mailContent .="A new email verify by user. Please see the below details for user.<br/>";
				$mailContent .="Id: = ".$qrycount["User"]["id"]."<br/>";
				$mailContent .="Name: = ".$qrycount["User"]["name"]."<br/>";
				$mailContent .="User Id: = ".$qrycount["User"]["user_id"]."<br/>";
				$mailContent .="Email: = ".$qrycount["User"]["email"]."<br/>";
				$mailContent .="Company: = ".$qrycount["User"]["company"]."<br/>";
				$mailContent .="Mobile Number: = ".$qrycount["User"]["phone_number"]."<br/>";
				if($qrycount["User"]["user_type"] == "CPA"){
					$userType = "Content Provider"; 
				} else {
					$userType = "Publisher"; 
				}
				$mailContent .="User Type: = ".$userType."<br/>";
				$mailContent .="Thanks<br/>";
				
				$mailContent = nl2br($mailContent);
				$emailFrom = $qrycount["User"]["email"];
				$nmFrom = $qrycount["User"]["name"];
				$emailTo = ADMIN_EMAIL;
				$nmTo = ADMIN_NAME;
				$subject = "Email Verification";
		
		
				$this->User->_mail($emailFrom, $nmFrom, $emailTo, $nmTo, $subject . " - " . SITE_NAME, trim($mailContent));
				
				$this->Session->setFlash('Your email is verified successfully and account is under review. You will be notified on your email once your account is reviewed by Admin.');
			    $this->redirect('login');
			}
		} else {
			$this->Session->setFlash('Something went wrong.');
			$this->redirect('login'); 
		}
	}
	
	public function dashboard()
    { 
	
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			//Find all user id...
			$searchCond = "";
			$searchCondUser = "";
			$userIdCheck = "";
			if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				$arr = array();
				foreach($userid as $key=>$val){
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond .= " and FIND_IN_SET (cp_id, '$finalUserArr')";
				$userIdCheck .= " and FIND_IN_SET (cp_id, '$finalUserArr')";
				$searchCondUser .= " and FIND_IN_SET (t2.user_id, '$finalUserArr')";
			}
			
			//bulk revenue...
			
			$last7days = date('Y-m-d', strtotime("-7 day"));
			$last30days = date('Y-m-d', strtotime("-30 day"));
			$endDate = date('Y-m-d', strtotime("-1 day"));
			//last 7 days revenue...
			
			$contentData7 = $this->Contentusedreport->query("select  count(t1.id) as countRec from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where t1.created >= '".$last7days." 00:00:00' and t1.created <= '".$endDate." 23:59:59' $userIdCheck");
			
			$this->set('contentData7', $contentData7);
			//last 30 days revenue...
			$contentData30 = $this->Contentusedreport->query("select  count(t1.id) as countRec from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where t1.created >= '".$last30days." 00:00:00' and t1.created <= '".$endDate." 23:59:59' $userIdCheck");
			$this->set('contentData30', $contentData30);
			
			//this year revenue...
			
			$contentDataYear = $this->Contentusedreport->query("select  count(t1.id) as countRec from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where year(t1.created)='".date('Y')."'  and date(t1.created)<='".$endDate."' $userIdCheck");
			$this->set('contentDataYear', $contentDataYear);
			
			//total views...
			$date24 = date("Y-m-d H:i:s", strtotime('-24 hours', time()));
			//$contentView = $this->Fcappcontent->query("select count(t1.id) as countRec from users_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where t2.status='A' and t2.approved='1' $searchCondUser and t2.category_id in (select category_id from cm_categories where status='A' and project='firstcut') and t1.created>='".$date24."'");
			
			$contentView = $this->Fcappcontent->query("select count(id) as countRec from user_cp_content_view where created>='".$date24."' $userIdCheck");
			
			$this->set('contentView', $contentView);
			
			//month wise revenue detail...
			
			$userMonthWiseData = $this->Contentusedreport->query("select  count(id) as countRec, month(created) as month from user_cp_content_view  where year(created)='".date('Y')."' and date(created)<='".$endDate."' $userIdCheck group by month(created)");
			$this->set('userMonthWiseData', $userMonthWiseData);
			
			//today revenue detail...
			$dateVal = date("Y-m-d");
			$userHourWiseData = $this->Contentusedreport->query("select  count(id) as countRec, hour(created) as hour from user_cp_content_view where year(created)='".date('Y')."' and date(created)<='".$endDate."' $userIdCheck group by hour(created)");
			
			$this->set('userHourWiseData', $userHourWiseData);
			
			//today revenue detail...
			$dateVal = date("Y-m-d");
			
			$userHourWiseData = $this->Contentusedreport->query("select  count(id) as countRec, hour(created) as hour from user_cp_content_view where created>='".$dateVal." 00:00:00' and created<='".$dateVal." 23:59:59' group by HOUR(created)");
			
			$this->set('userHourWiseData', $userHourWiseData);
			//previous day data...
			$prevDateVal = date("Y-m-d", strtotime('-1 day'));
			
			$userPrevHourWiseData = $this->Contentusedreport->query("select  count(id) as countRec, hour(created) as hour from user_cp_content_view where created>='".$prevDateVal." 00:00:00' and created<='".$prevDateVal." 23:59:59' $userIdCheck group by HOUR(created)");
			$this->set('userPrevHourWiseData', $userPrevHourWiseData);
			
			//previous day data...
			$prev28DateVal = date("Y-m-d", strtotime('-28 day'));
			
			$userPrev28DaysWiseData = $this->Contentusedreport->query("select  count(id) as countRec, date(created) as created from user_cp_content_view where created>='".$prev28DateVal." 00:00:00' and created<='".$prevDateVal." 23:59:59' $userIdCheck group by date(created)");
			
			$this->set('userPrev28DaysWiseData', $userPrev28DaysWiseData);
			
			$prev7DateVal = date("Y-m-d", strtotime('-7 day'));
			
			$userPrev7DaysWiseData = $this->Contentusedreport->query("select  count(id) as countRec, date(created) as created from user_cp_content_view where created>='".$prev7DateVal." 00:00:00' and created<='".$prevDateVal." 23:59:59' $userIdCheck group by date(created)");
			
			$this->set('userPrev7DaysWiseData', $userPrev7DaysWiseData);
		}
	}
	
	public function myprofile($id=null)
    { 
		if(empty($id) || $id != $this->Session->read('User.id')){
			$this->Session->setFlash('Something went wrong.');
			$this->redirect('dashboard'); 
		} else {
			$user = $this->User->find('first', array('conditions'=>array('User.id'=>$id)));
			$this->set('user', $user);
		}

		$data = $this->request->data;
		
		if(!empty($data["email"]))
		{
			$sql = array();
			$sql["name"] = trim($data["name"]);
			$sql["phone_number"] = trim($data["phone_number"]);
			$sql["company"] = trim($data["company"]);
			$sql["id"] = $id;
			$this->User->save($sql);
			$this->Session->write('User.name', trim($data["name"]));
			$this->Session->write('User.phone_number', trim($data["phone_number"]));
			$this->Session->write('User.company', trim($data["company"]));
			$this->Session->setFlash('Profile updated successfully..');
			$this->redirect('myprofile/'.$id);
		}
	}
	
	public function adduser()
    {
		
		//$qryMenu = $this->Menu->find('all', array('fields' => array('Menu.menu_id', 'Menu.menu_name'),'conditions'=>array('Menu.parent_menu_id'=>0, 'Menu.status'=>'A', 'FIND_IN_SET(\''. $this->Session->read('User.user_type') .'\', Menu.user_type)'), 'order'=>array('Menu.display_order ASC')));
		$qryMenu = $this->Menu->find('all', array('fields' => array('Menu.menu_id', 'Menu.menu_name'),'conditions'=>array('Menu.parent_menu_id'=>0, 'Menu.status'=>'A'), 'order'=>array('Menu.display_order ASC')));
		$this->set('qryMenuRecs', $qryMenu);
		//echo "<pre>";
		//print_r($qryMenu);
		//die;
		//die;
		$data = $this->request->data;
		if(!empty($data["email"]))
		{
			$qrycount = $this->User->find('first',array('conditions'=>array('User.email'=>$data['email'])));
			if(count($qrycount)>0)
		    {
				$this->Session->setFlash('This email is already registered.');
				$this->redirect('adduser'); 
			} else {
				if($this->Session->read('User.user_type') == 'CPA'){
					$data["usertype"] = 'CPU';
				} else {
					$data["usertype"] = 'V3MO';
				}
				
				$impMenuId = implode(",", $data["menuIdArr"]);
				$randPassword = $this->User->randPassword(5);
				$sql = array();
				$sql["name"] = trim($data["fname"])." ".trim($data["lname"]);
				$sql["email"] = trim($data["email"]);
				$sql["password"] = md5(trim($randPassword));
				$sql["user_type"] = trim($data["usertype"]);
				$sql["phone_number"] = trim($data["mobile_number"]);
				$sql["parent_id"] = $this->Session->read('User.id');
				$sql["status"] = 'A';
				$sql["menu_id"] = $impMenuId;
				$this->User->save($sql);
				$lastInsertId =  $this->User->id;
				
				//email section start here...
				$mailContent ="Hello ".trim($data["name"])."". ",<br />";
				$mailContent .="You have successfully registered with us. Please use below credential for login.<br/>";
				$mailContent .="Email: = ".trim($data["email"])."<br/>";
				$mailContent .="Password: = ".$randPassword."<br/>";
				
				$mailContent .="Thanks<br/>";
				$mailContent = nl2br($mailContent);
				$emailFrom = ADMIN_EMAIL;
				$nmFrom = ADMIN_NAME;
				$emailTo = trim($data["email"]);
				$nmTo = trim($data["name"]);
				$subject = "Signup welcome email";
		
				$this->User->_mail($emailFrom, $nmFrom, $emailTo, $nmTo, $subject . " - " . SITE_NAME, trim($mailContent));
				$this->Session->setFlash('Thankyou for registering with us. Please check register email for credential.');
				$this->redirect('adduser'); 
			}
		}
		
	}
	public function viewuser($page=0){
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		//find all user data...
		$searchCondStr = "id != '".$this->Session->read('User.id')."'";
		if($this->Session->read('User.user_type') == 'CPA' || $this->Session->read('User.user_type') == 'CPU'){//check condition for user...
		   $searchCondStr .= " and user_type='CPU'";
		   $searchCondStr .= " and parent_id='".$this->Session->read('User.id')."'";
		}
		
		//data by search...
		
		if(isset($_GET['searchBy']) && !empty($_GET['searchBy'])){
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "name")){
				$searchCondStr .=" and name like '%".$_GET["searchString"]."%'" ;
			}
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "email")){
				$searchCondStr .=" and email like '%".$_GET["searchString"]."%'" ;
			}
		}
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
			$searchCondStr .=" and status = '".$_GET["filterBy"]."'" ;
		}
		$orderBy = "";
		$orderBy = "order by id desc";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by id ".$_GET["sortBy"]."" ;			
		}
		
		
		$users = $this->User->query("SELECT * from cm_users as User where $searchCondStr $orderBy limit $offSet, $recPerPage");
		$cntUser = $this->User->query("SELECT count(*) as countRec from cm_users where $searchCondStr");
		
		$totalRec = $cntUser[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		
		$this->set(compact('users', $users));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
		
	}
	
	public function edituser($id=null){
		//$qryMenu = $this->Menu->find('all', array('fields' => array('Menu.menu_id', 'Menu.menu_name'),'conditions'=>array('Menu.parent_menu_id'=>0, 'Menu.status'=>'A', 'FIND_IN_SET(\''. $this->Session->read('User.user_type') .'\', Menu.user_type)'), 'order'=>array('Menu.display_order ASC')));
		$qryMenu = $this->Menu->find('all', array('fields' => array('Menu.menu_id', 'Menu.menu_name'),'conditions'=>array('Menu.parent_menu_id'=>0, 'Menu.status'=>'A'), 'order'=>array('Menu.display_order ASC')));
		
		$this->set('qryMenuRecs', $qryMenu);
		
		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
		
		$this->set('user', $user);
		
		$data = $this->request->data;
		if(!empty($data["email"]))
		{
			$qrycount = $this->User->find('first',array('conditions'=>array('User.id'=>$id, 'User.email'=>$data['email'])));
			 
			if(count($qrycount)<= 0)
		    {
				$this->Session->setFlash('You have no permission to edit this user.');
				$this->redirect('viewuser'); 
			} else {
				$impMenuId = implode(",", $data["menuIdArr"]);
				$sql = array();
				$sql["name"] = trim($data["name"]);
				$sql["phone_number"] = trim($data["mobile_number"]);
				$sql["company"] = trim($data["company"]);
				$sql["status"] = $data["status"];
				$sql["menu_id"] = $impMenuId;
				$sql["id"] = $id;
				$this->User->save($sql);
				if($data["status"] =='A')
				{
					//$password = $data["password"];
					//$pwdm = md5($password);
					$mailContent ="Hello ".$data["name"]."". ",<br />";
					$mailContent .="Your profile has been successfully approved by admin. Now you can login with valid credentials.<br/>";
					$mailContent .="<a href=".BASE_URL.">".BASE_URL."</a><br/>";
					//$mailContent .="User Id: = ".$data["user_id"]."<br/>";
				    //$mailContent .="Password: = ".$pwdm."<br/>";
					$mailContent .="Thanks<br/>";
					$mailContent = nl2br($mailContent);
					$emailFrom = ADMIN_EMAIL;
					$nmFrom = ADMIN_NAME;
					$emailTo = trim($data["email"]);
					$nmTo = $data["name"];
					$subject = "Approved Profile";
			
			
					$this->User->_mail($emailFrom, $nmFrom, $emailTo, $nmTo, $subject . " - " . SITE_NAME, trim($mailContent));
				}
				if($data["status"] =='C')
				{
					$mailContent ="Hello ".$data["name"]."". ",<br />";
					$mailContent .="Your profile is rejected by admin.<br/>";
					$mailContent .= "Reason for rejection -" . $data["remark"] . "<br/>";
					$mailContent .="Thanks<br/>";
					$mailContent = nl2br($mailContent);
					$emailFrom = ADMIN_EMAIL;
					$nmFrom = ADMIN_NAME;
					$emailTo = trim($data["email"]);
					$nmTo = $data["name"];
					$subject = "Rejected Profile";
			
					$this->User->_mail($emailFrom, $nmFrom, $emailTo, $nmTo, $subject . " - " . SITE_NAME, trim($mailContent));
				}
				
				
				$this->Session->setFlash('User updated successfully.');
				$this->redirect('viewuser');
			}
		}
		
	}
	
	public function changepassword()
	{
		$data = $this->request->data;
		
		
		if(!empty($data["email"]))
		{
			if(trim($data["email"]) != $this->Session->read('User.email')){
				$this->Session->setFlash('You cannot change password for this user.');
				$this->redirect('changepassword');
			} 
			else 
			{
				
				$user = $this->User->find('first',array('conditions'=>array('User.email'=>trim($data["email"]), 'User.password'=>md5($data["oldpassword"]))));
				
				if(count($user)<= 0)
				{
					$this->Session->setFlash('Old password not match with database.');
					$this->redirect('changepassword'); 
				} else {
					$sql = array();
					$sql["password"] = md5($data["newpassword"]);
					$sql["id"] = $this->Session->read('User.id');
					$this->User->save($sql);
					$this->Session->setFlash('Password updated successfully.');
					$this->redirect('changepassword');
				}
			}
		}
		
	}
	
		public function sidebar()
    { 
	    $this->autoRender = false ;
		$userType = $this->Session->read('User.user_type');
		$menuId = $this->Session->read('User.menu_id');
		$condM = "";
		if(!empty($menuId) && $menuId !=""){
			$condM = "and menu_id in ($menuId)";
		}
		
		
		$qry = $this->Menu->query("select * from cm_admin_menus where parent_menu_id=0 and status ='A' and FIND_IN_SET('$userType', user_type) $condM order by display_order asc");
		
	
		$allNavigation = array();
		foreach( $qry as $key=>$val){
			$allNavigation[]["parent"] = $val['cm_admin_menus'];
			$qry1 = $this->Menu->query("select * from cm_admin_menus where status ='A' and FIND_IN_SET('$userType', user_type) and parent_menu_id='".$allNavigation[$key]['parent']['menu_id']."' order by display_order asc");
			if(!empty($qry1)){
				foreach($qry1 as $key1=>$val1){
					$allNavigation[$key]["firstchild"][$key1] = $val1['cm_admin_menus'];
					$qry2 = $this->Menu->query("select * from cm_admin_menus where status ='A' and FIND_IN_SET('$userType', user_type) and parent_menu_id='".$allNavigation[$key]["firstchild"][$key1]["menu_id"]."' order by display_order asc");
					
					if(!empty($qry2)){
						foreach($qry2 as $key2=>$val2){
						$allNavigation[$key]["firstchild"][$key1]["secondchild"][$key2] = $val2['cm_admin_menus'];
							
						}
					}	
					
				}
			}
		}
		return $allNavigation;
		//$this->response->body(json_encode($allNavigation));
        //return $this->response;
	}
}
?>