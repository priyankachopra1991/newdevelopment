<?php
App::uses('HttpSocket', 'Network/Http');
class NotificationController extends AppController
{
	public $name = 'Notification';
	public $uses = array('Appnamelist','Fcappcontent','FcappBulkupload','FcappContentPrice','Fcappcategories','Channel','Country', 'User', 'Language', 'ContentNotification','Notification','Fccgimage');
	public $components = array('Paginator');
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function sendnotification()
	{
		if($this->request->isPost())
		 {
			$data = $this->request->data;
			//echo $data['deep_link'];
			//die;
			$sql = array();
			$time = time();
			if(!empty($data["image_link"]["name"]))
			{
				$file1 = $data['image_link']['name'];
				$FileType = pathinfo($file1, PATHINFO_EXTENSION);
				$filename = DOM_PUBLICHTML_PATH ."contents/advert_".$time.".".$FileType;
				if (move_uploaded_file($data['image_link']['tmp_name'],$filename))
                {
					$files_arr = explode("\n",$filename);
					foreach($files_arr as $file1)
					{
						if(!empty($file1))
					    {
							$file1_arr =explode("/",$file1);
							//echo "<pre>";
					        //print_r($file1_arr);
					        //die;
					        $image_link = "http://162.144.82.177/~cmsadmin/contents/".$file1_arr[5];
					        
						}
					}
					
					/*foreach($files_arr as $file1)
					{
						if(!empty($file1))
					    {
							$s3 = new S3(ACCESS_KEY,SECRET_KEY);
							$bucketname='firstcut';
							$file1_arr =explode("/",$file1);
                                $filename1 = end($file1_arr);
                                $main_arr = array_splice($file1_arr,5);
                                $bucket_file = implode("/",$main_arr);
                                $type = $data['image_link']['type'];
                                $header = array('Content-Type' =>$type);
								$s3->putObjectFile($file1,$bucketname,$bucket_file,S3::ACL_PUBLIC_READ,array(),$header);
                                if($s3->putObjectFile($file1,$bucketname,$bucket_file,S3::ACL_PUBLIC_READ,array(),$header))
								{
								   $image_link = 'https://s3-ap-southeast-1.amazonaws.com/firstcut/'.$bucket_file;
                                }
								
						}
					}*/
				}
				
			}
			
			$url = "https://fcm.googleapis.com/fcm/send";
			$ch = curl_init($url);
			
			$json_data = array("data"=>array("title"=>$data["title"],"message"=>$data["message"],"image_link"=>$image_link,"tag"=>$data["tags"],"deep_link"=>$data["deep_link"]),"priority"=>"high","to"=>$data["topic"]);
			
			//$json_data = array("data"=>array("title"=>$data["title"],"message"=>$data["message"],"image_link"=>$image_link,"deep_link"=>$data["deep_link"]),"tag"=>$data["tags"]),"priority"=>"high","to"=>$data["topic"]);
	
	        //ini_set('error_log', BASE_LOG_PATH.'/notification.log');
			$jsonDataEncoded = json_encode($json_data);
			
			//error_log("PUSH NOTIFICATION INPUT=".$jsonDataEncoded."");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			if($data["app"] == "Onlyearn")
			{
				//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: key=AIzaSyDxLBJ4HTiIO0eBxxeOZbmb0tk1eWPXVR0'));
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: key=AAAAzGumWow:APA91bEeh0yvPFRo9SOj-oHdwCle2dVWAttq_O4f9ao70_Rnkv3IueOveWAbWHBS4fagJVgtfTbc80cZUgdthKnmljV_v8oS11gQE8tXEhyLjR8D_Pi9Hf80XtfD8fsP7S3xaK47rFlm'));
				
			} 
			else if($data["app"] == "Firstcut"){
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: key=AIzaSyBVepJLxRsXgzZY9z6KJdwzyBcvggt8BGk'));
			} else if($data["app"] == "Meretoons"){
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: key=AIzaSyBCuQP_Uog3os19Z9bY1IQ4MPi7xMd5H0U'));
			}
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($ch);
			curl_close($ch);
			
			//error_log("PUSH NOTIFICATION Result=".$result."");
			$resultData = json_decode($result);
			
			$sql = array();
			$sql["topic"] = $data["topic"];
			$sql["app"] = $data["app"];
			$sql["title"] = $data["title"];
			$sql["image_link"] = $image_link;
			$sql["deep_link"] =  $data["deep_link"];
			$sql["tags"] = $data["tags"];
			$sql["message"] = $data["message"];
			$sql["created"] = date('Y-m-d H:i:s');
			$sql["messageid"] = $resultData->message_id;
			
				
			$this->Notification->save($sql);
			
			$this->Session->setFlash('Notification send sucessfully.');
			$this->redirect('sendnotification');
		 }
	}
//=======================VIEW NOTIFIVATION @ VIJAY =============================================
	public function viewnotification()
	{
		
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		//echo $recPerPage;die;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		$downloadsRow = $this->Notification->query("SELECT count(*) as countRec FROM pushnotification_data");
		
	    $value = $this->Notification->query("select * from pushnotification_data order by id desc limit $offSet, $recPerPage");
		
	    //$this->set('value', $value);
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			//echo $numOfPage;die;
			$this->set('title', 'View Notification');
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
	}
//===========================END OF FUNCTION====================================================
}
?>