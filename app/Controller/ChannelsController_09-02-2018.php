<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'S3.php');
App::import('Vendor', 'sdk/sdk.class.php');
App::import('Vendor', 'AmazonS3.php');
App::import('Component', 'Common');

class ChannelsController extends AppController{
	public $name = 'Channels';
	public $uses = array('Channel', 'User', 'Content', 'Contentlanguage', 'Series', 'Subscriber', 'Categories', 'Country', 'Language', 'Contentnotification');
	public $components = array('Paginator');
	public function beforeFilter() {
	parent::beforeFilter();
	
	}
	
	public function viewchannel($page=0){
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		$orderBy = "";
		//$countryCode = "IN";
		$countryCode = "";
		$searchCondStr = " status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && $_GET['filterBy'] != ""){
			$searchCondStr .=" and status = '".$_GET["filterBy"]."'" ;
		}
		
		$orderBy = "order by id desc";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by id ".$_GET["sortBy"]."" ;			
		}
		
		$channels = $this->Channel->query("select id, c_name, c_banner, status from ch_channel where $searchCondStr $orderBy limit $offSet, $recPerPage");
		foreach($channels as $key=>$val){
			
			$channelVideoCount = $this->channelVideoCount($channels[$key]["ch_channel"]["id"], $countryCode);
			
			$channels[$key]["ch_channel"]["videoCount"] = $channelVideoCount[0][0]["videoCount"];
			$channels[$key]["ch_channel"]["videoViewCount"] = $channelVideoCount[0][0]["videoViewCount"];
			$seriesCount = $this->channelSeriesCount($channels[$key]["ch_channel"]["id"]);
			$channels[$key]["ch_channel"]["seriesCount"] = $seriesCount[0][0]["seriesCount"];
			$subscriberCount = $this->channelSubscriberCount($channels[$key]["ch_channel"]["id"]);
			$channels[$key]["ch_channel"]["subscriberCount"] = $subscriberCount[0][0]["subscriberCount"];
			
		}	
		
		$channelsCount = $this->Channel->query("select count(id) as countRec from ch_channel where $searchCondStr");
		
		$totalRec = $channelsCount[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		
		$this->set(compact('channels', $channels));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
		
		
	}
	
	public function addchannel(){
		$country = $this->Country->find('all',array('conditions'=>array('Country.status'=>'1')));
		$this->set('country', $country);
		if($this->request->isPost())
		{  
			$data = $this->request->data;
			//echo "<pre>";
			//print_r($data);
			//die;
			$sql = array();
			//$sql["id"] = $data["id"];
			$sql["c_name"] = $data["c_name"];
			$sql["c_description"] = $data["c_description"];
			$sql["country_code"] = $data["country_code"];			
			$sql["c_lang"] = $data["c_lang"];
			$sql["c_legaldoc_type"] = $data["c_legaldoc_type"];
			$sql["facebook_connect_url"] = $data["facebook_connect_url"];
			$sql["youtube_connect_url"] = $data["youtube_connect_url"];
			$sql["user_id"] = $this->Session->read('User.id');
			$sql["status"] = 0;
			/*if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO"))
			{
				$sql["status"] = $data["status"];
				$sql["approved_by"] = $this->Session->read('User.id');
			}*/
			$sql["created"] = CURRDATE;
			$sql["modified"] = CURRDATE;
			$time = time();
			
			if(!empty($data["c_banner"]["name"]))
			{//channel banner image upload...
				$file1 = $data['c_banner']; 
				$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
				/*if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage1'])){
					//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
					//die;
					unlink(UPLOADPATH . 'channel/'.$data['hiddenimage1']);	
				}*/
				
				move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'channel/' . "c_b_".$time.".".$imageFileType1);
				$ch_banner = "c_b_".$time.".".$imageFileType1;
				$sql["c_banner"] = $ch_banner;
				$bannerImage = UPLOADPATH . 'channel/'. $ch_banner;
				chmod($bannerImage, 0777);
				
				if(file_exists($bannerImage)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
					$files_arr = explode("\n", $bannerImage);
					$s3 = new S3(ACCESS_KEY,SECRET_KEY);
					
					foreach($files_arr as $file1){
						if(!empty($file1)){
						
							$bucketname = FIRSTPOSTERBUCKET;
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
							$main_arr = array_splice($file1_arr, 6);
							$bucket_file = implode("/",$main_arr); 
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);
							$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
							
							//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
							if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
								//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
							}
						}
					}
					
					unlink($bannerImage);
					 
				}
			}
			
			if(!empty($data["c_icon"]["name"]))
			{//channel icon image upload...
				$file2 = $data['c_icon']; 
				$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
				/*if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage2'])){
					//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
					//die;
					unlink(UPLOADPATH . 'channel/'.$data['hiddenimage2']);	
				}*/
				
				$ch_icon = "c_i_".$time.".".$imageFileType2;
				move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'channel/' . $ch_icon);
				$sql["c_icon"] = $ch_icon;
				$channelImage = UPLOADPATH . 'channel/'. $ch_icon;
				chmod($channelImage, 0777);
				
				if(file_exists($channelImage)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
					$files_arr = explode("\n", $channelImage);
					$s3 = new S3(ACCESS_KEY,SECRET_KEY);
					
					foreach($files_arr as $file1){
						if(!empty($file1)){
						
							$bucketname = FIRSTPOSTERBUCKET;
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
							$main_arr = array_splice($file1_arr, 6);
							$bucket_file = implode("/",$main_arr); 
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);
							$cache = array('CacheControl' =>'public, max-age=31536000');
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
							
							//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
							if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
								//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
							}
						}
					}
					 
				}
				unlink($channelImage);
			}
			
			if(!empty($data["c_legaldoc"]["name"]))
			{//channel icon image upload...
				$file3 = $data['c_legaldoc']; 
				$imageFileType3 = pathinfo($file3['name'], PATHINFO_EXTENSION);
				/*if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage3'])){
					//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
					//die;
					unlink(UPLOADPATH . 'channel/'.$data['hiddenimage3']);	
				}*/
				
				$ch_doc = "c_l_".$time.".".$imageFileType3;
				move_uploaded_file($file3['tmp_name'], UPLOADPATH . 'channel/' . $ch_doc);
				$sql["c_legaldoc"] = $ch_doc;
				$channelDoc = UPLOADPATH . 'channel/'. $ch_doc;
				chmod($channelDoc, 0777);
				
				if(file_exists($channelDoc)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
					$files_arr = explode("\n", $channelDoc);
					$s3 = new S3(ACCESS_KEY,SECRET_KEY);
					
					foreach($files_arr as $file1){
						if(!empty($file1)){
						
							$bucketname = FIRSTPOSTERBUCKET;
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
							$main_arr = array_splice($file1_arr, 6);
							$bucket_file = implode("/",$main_arr); 
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);
							$cache = array('CacheControl' =>'public, max-age=31536000');
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
							
							//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
							if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
								//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
							}
						}
					}
					 
				}
				unlink($channelDoc);
			}
			
			$this->Channel->save($sql);
			$this->Session->setFlash('Channel Added Successfully.');
			$this->redirect('viewchannel');
		}
	}
	
	public function editchannel($id=null){
		$channel = $this->Channel->find('first',array('conditions'=>array('Channel.id'=>$id)));
		
		$this->set('channel', $channel);
		$user = $this->User->find('first', array('fields' => array('User.name'), 'conditions'=>array('User.id'=>$channel["Channel"]["user_id"])));
		$this->set('user', $user);
		$country = $this->Country->find('all',array('conditions'=>array('Country.status'=>'1')));
		$this->set('country', $country);
		
		
		if($this->request->isPost())
		{    
	        $searchCondStr = "id='".$id."'";
			//Find all user id...
			if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

				$finalUserArr = implode(",", $finalUser);
				$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
			}
			$channels = $this->Channel->query("select id from ch_channel where $searchCondStr");
		
			if(count($channels) <= 0 ){
				$this->Session->setFlash('You have no permission to delete this channel.');
				$this->redirect('viewchannel');
			} else {
				$data = $this->request->data;
				//echo "<pre>";
				//print_r($data);
				//die;
				$sql = array();
				$sql["id"] = $data["id"];
				$sql["c_name"] = $data["c_name"];
				$sql["c_description"] = $data["c_description"];
				$sql["country_code"] = $data["country_code"];			
				$sql["c_lang"] = $data["c_lang"];
				$sql["c_legaldoc_type"] = $data["c_legaldoc_type"];
				$sql["facebook_connect_url"] = $data["facebook_connect_url"];
				$sql["youtube_connect_url"] = $data["youtube_connect_url"];
				
				if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO"))
				{
					$sql["status"] = $data["status"];
					$sql["approved_by"] = $this->Session->read('User.id');
					
					//insert data in user notification...
					if($data["status"] != $data["oldstatus"]){
						$sql1 = array();
						$sql1["approved"] = $data["status"];
						$sql1["created"] = CURRDATE;
						$sql1["approved_by"] = $this->Session->read('User.id');
						$sql1["c_type"] = 2;
						$sql1["c_name"] = $data["c_name"];
						$sql1["c_id"] = $data["id"];
						$sql1["user_id"] = $data["user_id"];
						$this->Contentnotification->save($sql1);
						
					}
				}
				$sql["modified"] = CURRDATE;
				$time = time();
				
				if(!empty($data["c_banner"]["name"]))
				{//channel banner image upload...
					$file1 = $data['c_banner']; 
					$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
					if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage1'])){
						//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
						//die;
						unlink(UPLOADPATH . 'channel/'.$data['hiddenimage1']);	
					}
					
					move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'channel/' . "c_b_".$time.".".$imageFileType1);
					$ch_banner = "c_b_".$time.".".$imageFileType1;
					$sql["c_banner"] = $ch_banner;
					$bannerImage = UPLOADPATH . 'channel/'. $ch_banner;
					chmod($bannerImage, 0777);
					
					if(file_exists($bannerImage)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $bannerImage);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$cache = array('CacheControl' =>'public, max-age=31536000');
								$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						
						unlink($bannerImage);
						 
					}
				}
				
				if(!empty($data["c_icon"]["name"]))
				{//channel icon image upload...
					$file2 = $data['c_icon']; 
					$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
					if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage2'])){
						//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
						//die;
						unlink(UPLOADPATH . 'channel/'.$data['hiddenimage2']);	
					}
					
					$ch_icon = "c_i_".$time.".".$imageFileType2;
					move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'channel/' . $ch_icon);
					$sql["c_icon"] = $ch_icon;
					$channelImage = UPLOADPATH . 'channel/'. $ch_icon;
					chmod($channelImage, 0777);
					
					if(file_exists($channelImage)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $channelImage);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$cache = array('CacheControl' =>'public, max-age=31536000');
								$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						 
					}
					unlink($channelImage);
				}
				
				if(!empty($data["c_legaldoc"]["name"]))
				{//channel icon image upload...
					$file3 = $data['c_legaldoc']; 
					$imageFileType3 = pathinfo($file3['name'], PATHINFO_EXTENSION);
					if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage3'])){
						//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
						//die;
						unlink(UPLOADPATH . 'channel/'.$data['hiddenimage3']);	
					}
					
					$ch_doc = "c_l_".$time.".".$imageFileType3;
					move_uploaded_file($file3['tmp_name'], UPLOADPATH . 'channel/' . $ch_doc);
					$sql["c_legaldoc"] = $ch_doc;
					$channelDoc = UPLOADPATH . 'channel/'. $ch_doc;
					chmod($channelDoc, 0777);
					
					if(file_exists($channelDoc)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $channelDoc);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$cache = array('CacheControl' =>'public, max-age=31536000');
								$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						 
					}
					unlink($channelDoc);
				}
				
				$this->Channel->save($sql);
				$this->Session->setFlash('Channel Updated Successfully.');
				$this->redirect('viewchannel');
			}
		}
	
		
	}
	
	public function detailchannel($id=null){
		$searchCondStr= "id = '".$id."' and status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$channels = $this->Channel->query("select id, c_name, c_description, c_banner, status, created from ch_channel where $searchCondStr");
		
		if(count($channels) <= 0 ){
			$this->Session->setFlash('You have no permission to view this channel.');
			$this->redirect('viewchannel');
		} else {
			$this->set(compact('channels', $channels));
			$categories = $this->Categories->query("select t1.category_id, t1.name from cm_categories as t1 inner join cm_contents as t2 on t2.category_id=t1.category_id inner join cm_content_lang as t3 on t3.content_id=t2.content_id where t1.status='A' and t1.project='firstcut' and t2.status='A' and t2.approved='1' and t2.channel_id='".$channels[0]["ch_channel"]["id"]."' group by t1.category_id");
			$categoryList = "";
			$i = 0;
			foreach($categories as $category){
				if($i> 0){
					$categoryList .= ", ";
				}
				$categoryList .= $category["t1"]["name"];
				$i++;
			}
			
			$this->set(compact('categoryList', $categoryList));
			
		}
		
	}
	
	public function deletechannel($id=null){
		$searchCondStr = "id='".$id."'";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		$channels = $this->Channel->query("select id from ch_channel where $searchCondStr");
	
		if(count($channels) <= 0 ){
			$this->Session->setFlash('You have no permission to delete this channel.');
			$this->redirect('viewchannel');
		} else {
			$channelUpdate = $this->Channel->query("update ch_channel set status=4, modified='".CURRDATE."' where id='".$id."'");
			$contentUpdate = $this->Content->query("update cm_contents set status='C', modified='".CURRDATE."' where channel_id='".$id."'");
			$seriesUpdate = $this->Series->query("update ch_series set status='4', modified='".CURRDATE."' where channel_id='".$id."'");
			$this->Session->setFlash('Channel deleted succesfully.');
			$this->redirect('viewchannel');
		}
	}
	
	public function channelVideoCount($channelId=null, $countryCode=null){
		$countryCond = "";
		if(!empty($countryCode)){
			$countryCond = "and t1.country_code='".$countryCode."'";
		}
		
		//$contentCount = $this->Content->query("select count(t1.content_id) as videoCount, sum(t1.view) as videoViewCount from cm_contents as t1 inner join cm_content_lang as t2 on t2.content_id = t1.content_id where t1.status='A' and t1.approved='1' $countryCond  and channel_id = '".$channelId."'	and category_id in (select category_id from cm_categories where status='A' and project='firstcut')");
		
		$contentCount = $this->Content->query("select count(content_id) as videoCount, sum(view) as videoViewCount from cm_contents where status='A' and approved='1' $countryCond and channel_id = '".$channelId."'");
		return $contentCount;	
	}
	
	public function channelSeriesCount($channelId=null){
		$seriesCount = $this->Series->query("select count(id) as seriesCount from ch_series where channel_id='".$channelId."' and status='1'");
		return $seriesCount;
	}
	
	public function channelSubscriberCount($channelId=null){
		$subscriberCount = $this->Subscriber->query("select count(id) as subscriberCount from ch_channel_subscriber where channel_id='".$channelId."' and status='1'");
		return $subscriberCount;
	}
	
}
?>	