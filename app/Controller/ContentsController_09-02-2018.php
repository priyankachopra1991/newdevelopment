<?php
App::uses('HttpSocket', 'Network/Http');
//chmod("/home/cmsadmin", 0755);
//chmod("/home/cmsadmin/public_html", 0755);
//chmod("/home/cmsadmin/public_html/bulkupload", 0777);
//chmod("/home/cmsadmin/public_html/tmpcontents", 0777);
//chmod("/home/cmsadmin/public_html/contents", 0777);
ini_set('memory_limit', '4024M');
ini_set('max_execution_time', 3000);
ini_set('post_max_size', '500M');
ini_set('upload_max_filesize', '500M');
App::import('Component', 'Common');

class ContentsController extends AppController
{	public $name='Contents';
	public $helpers = array('Js');
	public $uses = array('Content','Category','ContentPrice','Bulkupload', 'User', 'Project');
	
	public function bulk_upload()
	{
	
		$post_max_size = ini_get('post_max_size');
 		$upload_max_filesize = ini_get('upload_max_filesize');
 
		
		$rec = $this->Category->find('all',array('conditions'=>array('Category.parent_id'=>1)));
		$this->set('category', $rec);
	
	
		if ($this->request->is('post')) 
		{	
			$filename =$_FILES['csvfile']['name'];
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			$target_file=PUBLICHTML_PATH."bulkupload/".date('YmdHis')."_$filename";
		    
			if($imageFileType=='csv')
			{
				if (move_uploaded_file($_FILES['csvfile']['tmp_name'],$target_file)) {
					
					$txtfile_n=explode(".",$target_file);
					$txtfile_name=$txtfile_n[0];
					
					$myfile = fopen($txtfile_name.'.txt', "w") or die("Unable to open file!");
					
					
				}				
				
				$subcategory=$this->data['subcategory_id']; 
			
				$contenttype=$this->data['contenttype']; 
				$file=$_FILES['csvfile']['tmp_name'];
								
				
				$handle = fopen($target_file,"r"); 
				$i=0;
				$data = fgetcsv($handle,1000,",","'");
							
				$c=count($data);
			    
				if($c==10) 
				{
					$p_count=0;  
					$r_count=1;
					while($data = fgetcsv($handle,1000,",","'")) 
					{   $b = "";
						if ($data[0]) 
						{
							$content_name=$data[0];	
							$content_video=trim($data[1]); 
							$content_poster= trim($data[2]);				
                            $poster = trim($data[2]);								
							$content_description= $data[3];$content_price= $data[4];
							$vodafone_price=$data[5];
							
							$txt_msg = "\nContent ".$r_count." ($content_name): ";
							$this->request->data['Content']['director_name']=$data[6];
							$this->request->data['Content']['production_house']=$data[7];
							$var = $data[8];
							$date = str_replace('/', '-', $var);
							 $date=date('Y-m-d', strtotime($date)); 
							$this->request->data['Content']['release_date']=$date;
							$var1 = $data[9];
							$date1 = str_replace('/', '-', $var1);
							$date1=date('Y-m-d', strtotime($date1)); 
							$this->request->data['Content']['expiry_date']=$date1;
							
							if(!empty($content_video))
							{
								$command = ("/usr/bin/ffmpeg  -i ".$content_video." -vstats 2>&1");  
     							$output = shell_exec($command);
								$regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    							if (preg_match($regex_sizes, $output, $regs)) {
									$codec = $regs [1] ? $regs [1] : null;
         						    $width = $regs [3] ? $regs [3] : null; 
         						    $height = $regs [4] ? $regs [4] : null;
								  
								}
	 							$search='/Duration: (.*?),/';
								preg_match($search, $output, $matches);
							    $explode = explode(':', $matches[1]);
							
								$duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								$duration=round($duration);
								$this->request->data['Content']['duration']=$duration;
							}
							
							$txt_msg = "\nContent ".$r_count." ($content_name): ";
							$txt="";
							//fwrite($myfile, $txt_msg);
							if(empty($content_name)){
								$txt .="Name is empty";
								$this->Session->setFlash('Name is empty.');
					            $this->redirect('bulk_upload');
							}
							if((!empty($content_video)) && (!file_exists($content_video)) && ($contenttype !="W")){	
								$txt .="Video is not exist";
								$this->Session->setFlash('Video is not exist.');
					            $this->redirect('bulk_upload');
							}
							if(empty($content_poster)){
								$txt .="Poster is empty";
								$this->Session->setFlash('Poster is empty.');
					            $this->redirect('bulk_upload');
							}
							else if(!file_exists($content_poster)){	
								$txt .="Poster is not exist";
								$this->Session->setFlash('Poster is not exist.');
					            $this->redirect('bulk_upload');
							}
							if(empty($content_description)){
								$txt .="Description is empty";
								$this->Session->setFlash('Description is empty.');
					            $this->redirect('bulk_upload');
							}
							if(empty($content_price)){
								$txt .="Price is empty";
								$this->Session->setFlash('Price is empty.');
					            $this->redirect('bulk_upload');
							}

							if(empty($txt))
							{
								$this->request->data['Content']['name']=$content_name;
								$this->request->data['Content']['price']=$content_price;
								$this->request->data['Content']['description']=$content_description;
								$this->request->data['Content']['category_id']=$subcategory;
								$this->request->data['Content']['content_type']=$contenttype;
								
								$this->request->data['Content']['user_id']=$this->Session->read('User.id');
								$this->request->data['Content']['ip']=$_SERVER['REMOTE_ADDR'];
								$poster=explode('/',$poster);
								$poster=end($poster);
								$poster=explode('.',$poster);
								$pos=$poster['1'];
								
								$b = time().rand(11,99);
								
								if($pos == "jpg" or $pos=="png" or $pos=="jpeg" or $pos=="gif") 
								{	
									
									rename($content_poster, PUBLICHTML_PATH."contents/".$b.".$pos");
									$c_image1=$b."_200x200"; 
		   						    $c_image2=$b."_400x200"; 
									
									$output=exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$pos  ".PUBLICHTML_PATH."contents/".$b.".png 2>&1", $array);
									exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$pos -resize 200x200  ".PUBLICHTML_PATH."contents/".$c_image1.".png 2>&1", $array);
									exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$pos -resize 400x200 ".PUBLICHTML_PATH."contents/".$c_image2.".png 2>&1", $array);
									$this->request->data['Content']['poster']=$b.".$pos";
								} else {
									$this->Session->setFlash('Content poster must be jpeg or jpg or png or gif format.');
										$this->redirect('bulk_upload'); 
								}
								
								if(!empty($content_video))
								{  
									$ex=explode("/",$content_video);
									//$output=$ex[5];
									$output=end($ex);
									$n=explode(".",$output);
									$f=$n[1]; 
									if($f=='mp4' or $f=='mp3' or $f=='3gp')
									{	
										$m=$b.".mp4";
										$m1=$b."_480.mp4";
										$m2=$b."_360.mp4";
										$m3=$b."_240.mp4";
																			
										$output= shell_exec('nohup sh "'.PUBLICHTML_SH_PATH.'"testv.sh "'.$content_video.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'" 1>/dev/null 2>/dev/null &'); 
									  	
										$this->request->data['Content']['content']=$m;		
									}else{
										$this->Session->setFlash('Content must be of video type.');
										$this->redirect('bulk_upload');  
									}
									
								}								
								$txt="Sucessfully inserted \n";
								//fwrite($myfile, $txt_msg2);
								
							 
								if($contenttype !="W"){
									$content1=$this->request->data['Content']['content'];
								} else {
									$content1="";
									
								}
								 
                           // print_r($this->request->data);
							//echo "<br/>";
							$this->Content->create();
							$this->Content->save($this->request->data);
							$last_id=$this->Content->getLastInsertID();
							
							if($last_id)
							{
								//insert vodafone price...
								
								
								$sql1 = "insert into  cm_content_prices set ";
								$sql1 = $sql1 . " content_id = '" . $last_id . "'";
								$sql1 = $sql1 . ", price = '" . $vodafone_price. "'";
								$sql1 = $sql1 . ", billing_partner = 'vf'";
								$this->ContentPrice->query($sql1);
								
								//insert airtel price...
											
								$sql2 = "insert into  cm_content_prices set ";
								$sql2 = $sql2 . " content_id = '" . $last_id . "'";
								$sql2 = $sql2 . ", price = '" . $content_price. "'";
								$sql2 = $sql2 . ", billing_partner = 'at'";
								
								$this->ContentPrice->query($sql2);
								
								//insert idea price...
								$sql3 = "insert into  cm_content_prices set ";
								$sql3 = $sql3 . " content_id = '" . $last_id . "'";
								$sql3 = $sql3 . ", price = '" . $content_price. "'";
								$sql3 = $sql3 . ", billing_partner = 'ia'";
								$this->ContentPrice->query($sql3);
								
								//insert tata price...
								$sql4 = "insert into  cm_content_prices set ";
								$sql4 = $sql4 . " content_id = '" . $last_id . "'";
								$sql4 = $sql4 . ", price = '" . $content_price. "'";
								$sql4 = $sql4 . ", billing_partner = 'tt'";
								
								$this->ContentPrice->query($sql4);
							}
							
							
							/*if($last_id)
							{
									
									$this->request->data['ContentPrice']['content_id']=$last_id;
									$this->request->data['ContentPrice']['billing_partner']="tt";
									$this->request->data['ContentPrice']['price']=$content_price;    
								
														
									$this->ContentPrice->create();			
									$this->ContentPrice->save($this->request->data);
							
									
									if(empty($vodafone_price)){
										$vodafone_price=0;
										
									} else {
										$vodafone_price=$vodafone_price;
									}	
									$this->request->data['ContentPrice']['content_id']=$last_id;
									$this->request->data['ContentPrice']['billing_partner']="vf";
									$this->request->data['ContentPrice']['price']=$vodafone_price;    
									$this->ContentPrice->create();			
									$this->ContentPrice->save($this->request->data);
									
									$this->request->data['ContentPrice']['content_id']=$last_id;
									$this->request->data['ContentPrice']['billing_partner']="at";
									$this->request->data['ContentPrice']['price']=$content_price;    
								
														
									$this->ContentPrice->create();			
									$this->ContentPrice->save($this->request->data);
									
									$this->request->data['ContentPrice']['content_id']=$last_id;
									$this->request->data['ContentPrice']['billing_partner']="ia";
									$this->request->data['ContentPrice']['price']=$content_price;    
								
														
									$this->ContentPrice->create();			
									$this->ContentPrice->save($this->request->data);
									
												
								}*/							
							}
							 
						} 
						
						$msg=$txt_msg.$txt;
						fwrite($myfile, $msg);

						$r_count ++;
						++$p_count; 	
							
					}
				
					$id=$this->Session->read('User.id');
					$target_file1=explode('/',$target_file);
					$csvfile=$target_file1[5];				
					$txtfile_name1 =explode('/',$txtfile_name);
					$txtfile=$txtfile_name1[5];	
					
				
					$sql5 = "INSERT into cm_bulk_uploads set csv_file='".$csvfile."',error_file='".$txtfile.'.txt'."',user_id='".$id."'";
					$this->Bulkupload->query($sql5);
				
				  	$this->Session->setFlash('CSV uploaded successfully.');
					$this->redirect('not_approved_content');	
					fclose($myfile);
					
				} else {
					$this->Session->setFlash('Mismatch columns: must be 10 columns.');
					$this->redirect('bulk_upload'); 				  
				}
			} else {
				$this->Session->setFlash('Unacceptable file type: upload only csv');
				$this->redirect('bulk_upload'); 	  
			}
		}
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		
		$this->set('categoryList', $categoryList);
		
	}	
	
public function updateContent_order(){
	
	$this->autoRender = false;
	if($this->request->isPost())
	{
		$this->loadModel('Content');
		$value=$this->request->data;
		$this->Content->query("UPDATE `cm_contents` SET `display_order` = '".$value['orderid']."' WHERE `content_id`='". $value['o_id']."'");	
	$this->render('../Elements/Content/view');  
	
	}
	
}	

	
	
	
	public function bulk_upload_list()
	{
		$this->loadModel('Bulkupload');
		$searchCond=array();
		if($this->Session->read('user_type')=='CP'){
			$cp_users_cond='';
			if($this->Session->read('User.cp_users'))
			{ 
				$cp_users=implode(',',$this->Session->read('User.cp_users') );
				$cp_users_cond=" OR Bulkupload.user_id in($cp_users)";
			}
			$searchCond[]="(Bulkupload.user_id = '".$this->Session->read('User.user_id')."' $cp_users_cond)";
		}else if($this->Session->read('user_type')=='CPU'){
			$searchCond[]="Bulkupload.user_id = '".$this->Session->read('User.user_id')."'";		
		}
		$uploadall=$this->Bulkupload->find('all',array('conditions'=>array($searchCond), 'order' => array('Bulkupload.bulk_id DESC'),'limit' => 10));
		$this->set('uploadall',$uploadall);
		
	}
	
	public function add() 
	{	
		$this->loadModel('Category');
		$this->loadModel('ContentPrice');
		$this->loadModel('Language');
		if($this->request->is('post')){	
			$value = $this->request->data;
			/*print_r($value);
			echo "<pre>";
			print_r($this->request->data);
			die;*/
			
			
			//Check image and video type before insert record...
			//image type checking...
			
			if(!empty($this->data['poster']['name']))
			{ 
		
				$filename = $this->data['poster'];
				
				$imageFileType = pathinfo($filename['name'], PATHINFO_EXTENSION);
				if(($imageFileType != "jpg") && ($imageFileType !="png") && ($imageFileType !="jpeg") && ($imageFileType !="gif")){
					$this->Session->setFlash('Unacceptable file type for poster.');
					$this->redirect('add');
				}	
			} 
			
			//video type checking...
			if(!empty($this->data['content']['name'])){
				$filename1 = $this->data['content']; 
				$videoFileType = pathinfo($filename1['name'], PATHINFO_EXTENSION);
				$size=$this->data['content'];
				$videoSize=$size['size'];
				if($videoSize > 0){
					
					if(($videoFileType != "mp4") && ($videoFileType != "mp3") && ($videoFileType != "3gp")){
						$this->Session->setFlash(
						'Unacceptable file type for video.');
						$this->redirect('add');
						
					}
					
				} else{
					$this->Session->setFlash(
						'Unacceptable file for video.');
					$this->redirect('add');
				}
				
			}	
			//mannage images... 
			/*if(!empty($this->data['poster']['name']))
			{ 
				move_uploaded_file($filename['tmp_name'], PUBLICHTML_PATH . 'contents/' . time()."_".$filename['name']);
						
				$this->request->data['poster'] = time()."_".$filename['name'];
				$targetFile = PUBLICHTML_PATH . 'contents/' . time()."_".$filename['name'];
				$imgsize = getimagesize($targetFile);
				$targetFileConvertedThumb =  str_replace('//','/',PUBLICHTML_PATH . 'contents/') . "thumb_1_" .$this->request->data['poster'];
				$targetFileConvertedThumb =  str_replace('//','/',PUBLICHTML_PATH . 'contents/') . "thumb_2_" .$this->request->data['poster'];
				$src_w = $imgsize[0];
				$src_h = $imgsize[1];
				
					parent::createThumbnail($targetFile, 200, 200, 0, $targetFileConvertedThumb);
					parent::createThumbnail($targetFile, 400, 200, 0, $targetFileConvertedThumb);
				
			}*/
			
			$b = time().rand(11,99);
			$newfilename=$b.'.'.$imageFileType;
			$dest=PUBLICHTML_PATH.'contents/';
			$filename=PUBLICHTML_PATH."contents/".$this->data['poster']['name']."";
			if(!empty($this->data['poster']['name']))
			{
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			if (move_uploaded_file($this->data['poster']['tmp_name'],$dest.$newfilename))
				{
				
				$newfilename=$b.'.'.$imageFileType;
				$dest=PUBLICHTML_PATH.'contents/';
				$c_image1=$b."_200x200"; 
				$c_image2=$b."_400x200"; 
				//exec("/usr/bin/convert /home/cmsadmin/public_html/tmpcontents/".$b.".$pos  /home/cmsadmin/public_html/tmpcontents/".$b.".png 2>&1", $array); die;
				$output=exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$imageFileType  ".PUBLICHTML_PATH."contents/".$b.".png 2>&1", $array);
				exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$imageFileType -resize 200x200  ".PUBLICHTML_PATH."contents/".$c_image1.".png 2>&1", $array);
				exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$imageFileType -resize 400x200 ".PUBLICHTML_PATH."contents/".$c_image2.".png 2>&1", $array);								
				$poster=$b.'.png';
				//$poster=$newfilename;
				$this->request->data['Content']['poster']=$newfilename; 
				}
			}
			
			
			//mannage videos...
			if(!empty($this->data['content']['name']))
			{
				$this->data['content']['name'];
			    $size=$this->data['content'];
			    $vsize=$size['size'];
			    $this->request->data['Content']['size']=$vsize;
				if($vsize!=0)
			    {
					$filename1 = PUBLICHTML_PATH."contents/".$this->data['content']['name'];
				    
					$file=explode('/',$filename1);
					$file=$file[1];
					$file=explode('.',$file);
					$file=$file[0];
					//$m=$time."_VIDEO.mp4";
					
					$m=$b.".mp4";
					$m1=$b."_480.mp4";
					$m2=$b."_360.mp4";
					$m3=$b."_240.mp4";	
					
					//if (move_uploaded_file($this->data['content']['tmp_name'],$filename1))
					//{
						CakeLog::info('PATH ['.$filename1.']','debug');
						$stat = stat($filename1);
						$command = ("/usr/bin/ffmpeg  -i ".$filename1." -vstats 2>&1");  
     					$output = shell_exec($command);
						$regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
						if (preg_match($regex_sizes, $output, $regs)) {
        					$codec = $regs [1] ? $regs [1] : null;
         					$width = $regs [3] ? $regs [3] : null; 
         					$height = $regs [4] ? $regs [4] : null;
								  
						}
						
						$search='/Duration: (.*?),/';
						preg_match($search, $output, $matches);
						$explode = explode(':', $matches[1]);
						$duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
						$duration=round($duration); 
						$this->request->data['Content']['duration']=$duration;
						$output= shell_exec('nohup sh "'.PUBLICHTML_SH_PATH.'"testv.sh "'.$filename1.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'" 1>/dev/null 2>/dev/null &'); 
												
						$this->request->data['Content']['content']=$m;
						
					/*} else {
						$this->Session->setFlash('There was a problem uploading file. Please try again.');
						$this->redirect('add');
					}*/
					
					   
				}
			} 
			
			//echo "<br/>";
			$this->Content->create();
			//echo "<pre>";
			//print_r($this->request->data);
			//die;
			$sql = array();
			$sql["category_id"] = $this->request->data["Content"]["subcategory_id"];
			$sql["name"] = $this->request->data["name"];
			$sql["price"] = $this->request->data["price"];
			if(!empty($this->request->data["content"]["name"])){
				$sql["content"] = $this->request->data['Content']['content'];
			} else {
				
				$sql["content"] = "";
			}
			
			$sql["poster"] = $this->request->data["Content"]["poster"];
			$sql["content_type"] = $this->request->data["Content"]["content_type"];
			$sql["description"] = $this->request->data["Content"]["description"];
			
			$sql["language_code"] = $this->request->data["Content"]["language_code"];
			$sql["status"] = 'D';
			$sql["ip"] = $_SERVER['REMOTE_ADDR'];
			$sql["user_id"] = $this->Session->read('User.id');
			
			$sql["production_house"] = $this->request->data["Content"]["production_house"];
			$sql["director_name"] = $this->request->data["Content"]["director_name"];
			$sql["release_date"] = $this->request->data["Content"]["release_date"];
			$sql["expiry_date"] = $this->request->data["Content"]["expiry_date"];
			$sql["display_order"] = $this->request->data["display_order"];
			$sql["created"] = date('Y-m-d H:i:s');
			$sql["modified"] = date('Y-m-d H:i:s');
			
			$this->Content->save($sql);	
			$contentId=$this->Content->getLastInsertID();
			
			$this->ContentPrice->create();
			
			//insert vodafone price...
					
			$sql1 = "insert into  cm_content_prices set ";
		    $sql1 = $sql1 . " content_id = '" . $contentId . "'";
		    $sql1 = $sql1 . ", price = '" . $this->request->data["Content"]["vodafoneprice"]. "'";
			$sql1 = $sql1 . ", billing_partner = 'vf'";
			$this->ContentPrice->query($sql1);
			
			//insert airtel price...
						
			$sql2 = "insert into  cm_content_prices set ";
		    $sql2 = $sql2 . " content_id = '" . $contentId . "'";
		    $sql2 = $sql2 . ", price = '" . $this->request->data["price"]. "'";
			$sql2 = $sql2 . ", billing_partner = 'at'";
			
			$this->ContentPrice->query($sql2);
			
			//insert idea price...
			$sql3 = "insert into  cm_content_prices set ";
		    $sql3 = $sql3 . " content_id = '" . $contentId . "'";
		    $sql3 = $sql3 . ", price = '" . $this->request->data["price"]. "'";
			$sql3 = $sql3 . ", billing_partner = 'ia'";
			$this->ContentPrice->query($sql3);
			
			//insert tata price...
			$sql4 = "insert into  cm_content_prices set ";
		    $sql4 = $sql4 . " content_id = '" . $contentId . "'";
		    $sql4 = $sql4 . ", price = '" . $this->request->data["price"]. "'";
			$sql4 = $sql4 . ", billing_partner = 'tt'";
			
			$this->ContentPrice->query($sql4);
			//unlink($filename1);
			
			$this->Session->setFlash('Content added successfully.');
			$this->redirect('not_approved_content');
			
		}	
			
		///////////////////////////////////////////
			
		
		$lang=$this->Language->find('all');
		$this->set('lang',$lang);
		
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		
		$this->set('categoryList', $categoryList);
		
		
		
	}
	
	
	
	
	public function view($page=0) {
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		$searchCategories=array();
		$searchSubCategories=array();
		if($this->request->is('post')){	
		$user_id=$this->Session->read('User.id');
			$value=$this->data;
			if(isset($value['status']))
			{
				if($value['status']=="1"){
					 foreach($value['check'] as $checkone)
					{
						
						$this->Content->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						 
				 	}	
					$this->Session->setFlash('Content Update Successfully.');					
				}
				
				if($value['status']=="2"){
					 foreach($value['check'] as $checkone)
					{
						
						$this->Content->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						  
				 	}	
						$this->Session->setFlash('Content Update Successfully.');					
				}
				if($value['status']=="A"){
					foreach($value['check'] as $checkone)
					{
						$this->Content->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						  
					}
					$this->Session->setFlash('Content Update Successfully.');					
				}
				if($value['status']=="D"){
					foreach($value['check'] as $checkone)
				 	{
					 	$this->Content->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						
				 	}	
					$this->Session->setFlash('Content Update Successfully');					
				}
			}
		}
		
		
		if($this->request->is('get')){	
			$value=$this->data;
			
			$this->loadModel('Category');
			
		  if(isset($this->request->query['searchForm']))	
		  { 			
			  if(!empty($this->request->query['project']))
			  {
				  $searchCond[]="Subcategory.project='".$this->request->query['project']."'";
				   //Get parent category if project selected...
				  $searchCategories = $this->Category->find('all',array('conditions' => array('Category.project' =>$this->request->query['project'],'Category.parent_id' =>1)));
			  }
			  
			  if(!empty($this->request->query['category_id']))
			  {
				  $searchCond[]="Category.category_id='".$this->request->query['category_id']."'";
				  //Get sub category if project selected...
				  $searchSubCategories = $this->Category->find('all',array('conditions' => array('Category.parent_id' =>$this->request->query['category_id'])));
				 
			  }
			  
			   if(!empty($this->request->query['subcategory_id']))
			  {
				  $searchCond[]="Content.category_id='".$this->request->query['subcategory_id']."'";
				  
				  
			  }
			  
			  if(!empty($this->request->query['searchStr'])){
				  if($this->request->query['searchIn']=="ct_id"){
					  $searchCond[]="Content.content_id='".$this->request->query['searchStr']."'";
				  }
				  if($this->request->query['searchIn']=="name"){
					  $name=str_replace('+', ' ', $this->request->query['searchStr']);
  
					  $searchCond[]="Content.name like '%".$name."%'";
				  }
				  if($this->request->query['searchIn']=="ct_price"){
					  $searchCond[]="Content.price='".$this->request->query['searchStr']."'";
				  } 
				  	if($this->request->query['searchIn']=="keyword"){
					$searchCond[]="Content.name like '%".$this->request->query['searchStr']."%' OR Content.description like '%".$this->request->query['searchStr']."%'";
				  } 
				  
			  } 
		  } 
		
		}		
		
		//Find all user id...
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="FIND_IN_SET   (Content.user_id, '$finalUserArr')";
		}
		
		$searchCond[]="Content.approved  in(0,1,2)";
		
				
		$searchCondStr=implode(" AND ",$searchCond);
		//print_r($searchCondStr);die;
		$query="SELECT `Content`.`content_id`, `Content`.`category_id`, `Content`.`name`, `Content`.`price`, `Content`.`description`, `Content`.`content`, `Content`.`poster`, `Content`.`content_type`, `Content`.`language_code`, `Content`.`status`, `Content`.`display_order`,`Content`.`created`, `Content`.`modified`, `Content`.`ip`, `Content`.`user_id`, `Content`.`approved` , Category.name, Category.category_id, Subcategory.project, Subcategory.name, Subcategory.category_id, Users.user_id as username 
		FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as  Subcategory, cm_users as Users
		WHERE Content.category_id=Subcategory.category_id 
		AND Subcategory.parent_id = Category.category_id 
		AND Users.id=`Content`.`user_id`
		AND $searchCondStr  
		order by content_id desc
		limit $offset,$RowofPerpage";
		//echo 'query: '.$query; 
		$contents = $this->Content->query($query);
	
		$downloadsRow=$this->Content->query("SELECT count(Content.content_id) as count FROM `cm_contents` as Content, cm_categories Category, cm_categories Subcategory WHERE Content.category_id=Subcategory.category_id AND Subcategory.parent_id = Category.category_id AND $searchCondStr ");
		
		$Act=$this->Content->query("SELECT count(Content.content_id) as count FROM `cm_contents` as Content, cm_categories Category, cm_categories Subcategory WHERE Content.category_id=Subcategory.category_id AND Subcategory.parent_id = Category.category_id AND Content.status ='A' AND $searchCondStr ");
		
		$InAct=$this->Content->query("SELECT count(Content.content_id) as count FROM `cm_contents` as Content, cm_categories Category, cm_categories Subcategory WHERE Content.category_id=Subcategory.category_id AND Subcategory.parent_id = Category.category_id AND Content.status ='D' AND $searchCondStr ");
		
		$Act = $Act[0][0]['count'];
		$InAct = $InAct[0][0]['count'];
		
		$num = $downloadsRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		
		$this->set('searchCategories',$searchCategories);
		$this->set('searchSubCategories',$searchSubCategories);
		
		$this->set(compact('contents', $contents));	
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		$this->set('Act', $Act);
		$this->set('InAct', $InAct);
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		$this->set('categoryList', $categoryList);
	}
	
	public function edit($content_id=null) {
		$this->loadModel('Category');
		$this->loadModel('ContentPrice');
		$this->loadModel('Language');
		$this->loadModel('User');
		if($this->request->is('post')) 
		{	
			$value = $this->data;
			//Check image and video type at time of edit...
			
			if(!empty($value['Content']['poster']['name']))
			{
				
				$filename=PUBLICHTML_PATH."contents/".$value['Content']['poster']['name']."";
				//echo $filename;die;
				$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
				
				if(($imageFileType != "jpg") && ($imageFileType!="png") && ($imageFileType!="jpeg") && ($imageFileType!="gif")){
					
					$this->Session->setFlash('Unacceptable file type for poster.');
					$this->redirect('edit/'.$content_id);
				}
			}
			
			if(!empty($this->data['Content']['content']['name'])){
				$filename = PUBLICHTML_PATH."contents/".$this->data['Content']['content']['name'];
				$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
				if(($imageFileType != "mp4") && ($imageFileType != "mp3") && ($imageFileType != "3gp")) 
				{	
					$this->Session->setFlash('Unacceptable file type for video.');
					$this->redirect('edit/'.$content_id);
				}
			}
			
			
			//echo"<pre>";print_r($value);die;
		
			
			
			$filename=PUBLICHTML_PATH."contents/".$value['Content']['poster']['name']."";
			
			if(!empty($this->data['Content']['poster']['name']))
			{
				$pos=$this->Content->find('all',array('fields' => array('poster'),'conditions' => array('Content.content_id' => $content_id)));
				$pos=$pos['0']['Content']['poster'];
				$image1=explode(".",$pos);
		        $c_image=$image1['0'];
				$pos1=$c_image.".jpg"; 
				$pos2=$c_image."_200x200.png";
				$pos3=$c_image."_400x200.png";
				$file_exists=file_exists(PUBLICHTML_PATH.'contents/'.$pos.'');
				$file_exists1=file_exists(PUBLICHTML_PATH.'contents/'.$pos1.''); 
				$file_exists2=file_exists(PUBLICHTML_PATH.'contents/'.$pos2.''); 
				$file_exists3=file_exists(PUBLICHTML_PATH.'contents/'.$pos3.'');  
				if($file_exists==1){unlink(PUBLICHTML_PATH.'contents/'.$pos.''); }
				if($file_exists1==1){ unlink(PUBLICHTML_PATH.'contents/'.$pos1.''); }
				if($file_exists2==1){ unlink(PUBLICHTML_PATH.'contents/'.$pos2.''); }
				if($file_exists3==1){ unlink(PUBLICHTML_PATH.'contents/'.$pos3.''); }
				$filename = PUBLICHTML_PATH."contents/".$this->data['Content']['poster']['name'];
				//echo $filename;die;
				$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
				if($imageFileType == "jpg" or $imageFileType=="png" or $imageFileType="jpeg" or $imageFileType="gif") 
				{
					$b = time().rand(11,99);
					$newfilename=$b.'.'.$imageFileType;
					$dest=PUBLICHTML_PATH.'contents/';
					//$newfilename=time().'_POSTER.'.$imageFileType;
					if (move_uploaded_file($this->data['Content']['poster']['tmp_name'],$dest.$newfilename))
					{
						$c_image1=$b."_200x200"; 
		   				$c_image2=$b."_400x200"; 
						//exec("/usr/bin/convert /home/cmsadmin/public_html/tmpcontents/".$b.".$pos  /home/cmsadmin/public_html/tmpcontents/".$b.".png 2>&1", $array); die;
						$output=exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$imageFileType  ".PUBLICHTML_PATH."contents/".$b.".png 2>&1", $array);
						exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$imageFileType -resize 200x200  ".PUBLICHTML_PATH."contents/".$c_image1.".png 2>&1", $array);
						exec("/usr/bin/convert ".PUBLICHTML_PATH."contents/".$b.".$imageFileType -resize 400x200 ".PUBLICHTML_PATH."contents/".$c_image2.".png 2>&1", $array);								
						$poster=$b.'.png';
						//$poster=$newfilename;
					} else {
						$this->Session->setFlash('There was a problem uploading file. Please try again.');
						$this->redirect('edit/'.$content_id);
					}	
				}else {
					$this->Session->setFlash('Unacceptable file type for poster.');
					$this->redirect('edit/'.$content_id);
				}  
			}else if(isset($value['thumb_poster']) && !empty($value['thumb_poster'])){
				$poster=$value['thumb_poster'];
			}else{
				$poster=$value['Content']['old_poster'];
			}
			//Edit video code start here...
			if(!empty($this->data['Content']['content']['name'])){
			$this->data['Content']['content']['name'];
			 $size=$this->data['Content']['content'];
			 $vsize=$size['size'];
			 $this->request->data['Content']['size']=$vsize;
			
			if($vsize!=0)
			{	
				 $filename = PUBLICHTML_PATH."contents/".$this->data['Content']['content']['name'];
				 $imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
				if($imageFileType == "mp4" or $imageFileType == "mp3" or $imageFileType == "3gp" ) 
				{					 
					$file=explode('/',$filename);
					$file=$file[1];
					$file=explode('.',$file);
					$file=$file[0];
					//$m=$time."_VIDEO.mp4";
					$b = time().rand(11,99);
					$m=$b.".mp4";
				    $m1=$b."_480.mp4";
					$m2=$b."_360.mp4";
					$m3=$b."_240.mp4";	
					   
					if (move_uploaded_file($this->data['Content']['content']['tmp_name'],$filename))
					{
						CakeLog::info('PATH ['.$filename.']','debug');
						//chown($filename ,"cmsadmin");
						$stat = stat($filename);
						// echo ($filename);
						         $command = ("/usr/bin/ffmpeg  -i ".$filename." -vstats 2>&1");  
     							 $output = shell_exec($command); 
								// print_r($output);  
								$regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    							 if (preg_match($regex_sizes, $output, $regs)) {
        						   $codec = $regs [1] ? $regs [1] : null;
         						   $width = $regs [3] ? $regs [3] : null; 
         							$height = $regs [4] ? $regs [4] : null;
								  
								 }
								 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration); 
								 $this->request->data['Content']['duration']=$duration;
								
						//print_r(posix_getpwuid($stat['uid']));die;
						//$command = '/usr/bin/ffmpeg -i "'.$filename.'" -vcodec h263 -s qcif -r 15 -b 100k -acodec libfaac -ac 1 -ar 32000 -ab 64k "'.PUBLICHTML_PATH.'contents/'.$m.'"';
			//$command= exec("/usr/bin/ffmpeg -i ".$filename." -b 150k -s 320x240 -r 15 -ac 1 -ar 8000 -ab 24k ".PUBLICHTML_PATH."contents/".$m."  2>&1", $o, $v);
					//shell_exec($command); 
					
					$output= shell_exec('nohup sh "'.PUBLICHTML_SH_PATH.'"testv.sh "'.$filename.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'" 1>/dev/null 2>/dev/null &'); 
							
												
						$this->request->data['Content']['content']=$m;	
						
						    $oldFile = $this->request->data['Content']['old_content'];
							
							$vpos1=$oldFile.".mp4";
							$vpos2=$oldFile."_480.mp4";
							$vpos3=$oldFile."_360.mp4";
							$vpos4=$oldFile."_240.mp4";
							$vidfile_exists1=file_exists(PUBLICHTML_PATH.'contents/'.$vpos1.'');
							$vidfile_exists2=file_exists(PUBLICHTML_PATH.'contents/'.$vpos2.''); 
							$vidfile_exists3=file_exists(PUBLICHTML_PATH.'contents/'.$vpos3.''); 
							$vidfile_exists4=file_exists(PUBLICHTML_PATH.'contents/'.$vpos4.'');  
							if($vidfile_exists1==1){unlink(PUBLICHTML_PATH.'contents/'.$vpos1.''); }
							if($vidfile_exists2==1){ unlink(PUBLICHTML_PATH.'contents/'.$vpos2.''); }
							if($vidfile_exists3==1){ unlink(PUBLICHTML_PATH.'contents/'.$vpos3.''); }
							if($vidfile_exists4==1){ unlink(PUBLICHTML_PATH.'contents/'.$vpos4.''); }
						
						
						
					} else{
						$this->Session->setFlash('There was a problem uploading file. Please try again.');
						$this->redirect('edit/'.$content_id);
					}	
				}else {
					$this->Session->setFlash('Unacceptable file type for video');
					$this->redirect('edit/'.$content_id);
				}	
			} else {
				if(isset($this->data['Content']['old_content'])){
					$this->request->data['Content']['content']=$this->data['Content']['old_content'];
				} else {
					$this->request->data['Content']['content']="";
				}
			}
			} else {
				if(isset($this->data['Content']['old_content'])){
					$this->request->data['Content']['content']=$this->data['Content']['old_content'];
				} else {
					$this->request->data['Content']['content']="";
				}
			}
			//Edit video code start here...
			
			
			
			//if(!empty($value['status']))
			//$this->request->data['Content']['user_id']=$this->Session->read('User.user_id');
			if(!isset($value['approved'])) {
				$value['approved'] = 0;
				$approvedby = "";
				
			}  else {
				if($value['approved'] > 0){
					$approvedby = $this->Session->read('User.id');
				} else {
					$approvedby = "";
				}	
			}			
			$this->Content->updateAll(
					array('Content.name' =>"'".addslashes($value['name'])."'",'Content.poster'=>"'".$poster."'",'Content.content'=>"'".$this->request->data['Content']['content']."'",'Content.price'=>"'".$value['price']."'",'Content.category_id' =>"'".$value['subcategory_id']."'",'Content.display_order' =>"'".$value['display_order']."'",'Content.tags'=>"'".$this->request->data['tags']."'",
					'Content.description' =>"'".addslashes($value['description'])."'",'Content.language_code' =>"'".$value['language_code']."'",'Content.release_date' =>"'".addslashes($value['release_date'])."'",'Content.expiry_date' =>"'".addslashes($value['expiry_date'])."'",'Content.status' =>"'".$value['status']."'",'Content.approved' =>"'".$value['approved']."'",'Content.approved_by' =>"'".$approvedby."'",'Content.director_name' =>"'".addslashes($value['director_name'])."'",'Content.production_house' =>"'".addslashes($value['production_house'])."'",'Content.remark' =>"'".addslashes($value['remark'])."'"),
					array('Content.content_id' =>$value['content_id'])
			);
			
			
			/*if(!empty($value['remark']))
			$this->Content->updateAll(
					array('Content.name' =>"'".addslashes($value['name'])."'",'Content.poster'=>"'".$poster."'",'Content.price'=>"'".$value['price']."'",'Content.category_id' =>"'".$value['subcategory_id']."'",
					'Content.description' =>"'".addslashes($value['description'])."'",'Content.language_code' =>"'".$value['language_code']."'",'Content.status' =>"'".$value['status']."'",'Content.approved' =>"'".$value['approved']."'",'Content.approved_by' =>"'".$this->Session->read('User.id')."'"),
					array('Content.content_id' =>$value['content_id'])
			);*/
			
			//echo '<pre>';print_r($this->data); die;
				//-------- tata price ---------
				
				$this->ContentPrice->query("update cm_content_prices set price='".$value['price']."' where billing_partner='tt' and  content_id='".$value['content_id']."' ");
			
			
			//-------- airtel price ---------			
				$this->ContentPrice->query("update cm_content_prices set price='".$value['price']."' where billing_partner='at' and content_id='".$value['content_id']."'");
				
				//-------- aircel price ---------			
				$this->ContentPrice->query("update cm_content_prices set price='".$value['price']."' where billing_partner='ia' and content_id='".$value['content_id']."' ");
			
				
			//-------- vodafone price ---------		
				
				$this->ContentPrice->query("update cm_content_prices set price='".$value['vodafoneprice']."' where billing_partner='vf' and content_id='".$value['content_id']."' ");
			
			
			$this->Session->setFlash('Content edit successfully');
			if($value['approved'] ==1){
				$this->redirect('approved_contents');
			} if($value['approved'] ==2){
				$this->redirect('rejected_content_list');
			} else {
				$this->redirect('not_approved_content');
			}
			
		
		}		
		//if(empty($content_id))
		//	$this->redirect('view');
			
		/* $query="SELECT Content.*, cp1.price as tata_price, cp2.price as mtnl_price ,cp1.content_price_id as tata_price_id ,cp2.content_price_id as mtnl_price_id
				FROM cm_contents Content LEFT JOIN cm_content_prices cp1
				ON Content.content_id = (cp1.content_id)
				LEFT JOIN cm_content_prices cp2 ON Content.content_id=cp2.content_id			
				WHERE Content.content_id=$content_id and cp1.billing_partner='tt' and cp2.billing_partner='mt'"; */	
				
		$query="SELECT Content.*, Category.category_id, Subcategory.category_id, Subcategory.project, Subcategory.parent_id, Users.user_id as username FROM cm_contents Content, cm_categories Category, cm_categories Subcategory , cm_users as Users
				WHERE Content.category_id=Subcategory.category_id
				AND Subcategory.parent_id=Category.category_id
				AND Users.id=`Content`.`user_id`
				AND Content.content_id=$content_id ";
		$content = $this->Content->query($query);	
		//echo "<pre>";print_r($content);die;
		$tata_price_arr=$this->ContentPrice->find('first',array('conditions'=>array('content_id'=>$content_id,'billing_partner'=>'tt')));
		$mtnl_price_arr=$this->ContentPrice->find('first',array('conditions'=>array('content_id'=>$content_id,'billing_partner'=>'mt')));
		$vodafone_price_arr=$this->ContentPrice->find('first',array('conditions'=>array('content_id'=>$content_id,'billing_partner'=>'vf')));
		$aircel_price_arr=$this->ContentPrice->find('first',array('conditions'=>array('content_id'=>$content_id,'billing_partner'=>'ac')));
		
		$content=$content[0];
		$langs=$this->Language->find('all');
		$parent_id=$content['Subcategory']['parent_id'];
		$categories=$this->Category->find('all',array('conditions'=>array('Category.project'=>$content['Subcategory']['project'],'Category.parent_id'=>1)));
		$sub_categories=$this->Category->find('all',array('conditions'=>array('Category.parent_id'=>$parent_id)));
		$this->set(compact('categories','sub_categories','category','content','langs','tata_price_arr','mtnl_price_arr','vodafone_price_arr','aircel_price_arr','content'));
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		
		$this->set('project', $project);
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		$this->set('categoryList', $categoryList);
		
	}
    public function get_sub_category($category_id=null)
    { 
		$this->loadModel('Category');
		if($this->request->is('ajax'))
		{
			$this->autoRender = false;
			$this->layout = 'ajax';
			$category_id = $_POST['category_id'];
			 $cat = $this->Category->find('all',array(
			  'conditions' => array('Category.parent_id' => $category_id,'Category.status'=>'A'),
			  'recursive' => -1
			));			
			$this->set(compact('cat'));
			$this->render('../Elements/Categories/get_sub_category');  
		}
    }
	public function not_approved_content($page=0)
	{
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$userId=$this->Session->read('User.id');
		$searchCategories=array();
		$searchSubCategories=array();
		if($this->request->is('post'))
		{
			$check = $_POST['check'];
			if($check){
				foreach($check as $checkone)
				{
				    $this->Content->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
					$this->Session->setFlash('Content Approved Successfully');
				}	
			}
		}
		
		if($this->request->is('get')){	
			$this->loadModel('Category');
			
		    if(isset($this->request->query['searchForm']))	
		    { 			
			  if(!empty($this->request->query['project']))
			  {
				  $searchCond[]="Subcategory.project='".$this->request->query['project']."'";
				  //Get parent category if project selected...
				  $searchCategories = $this->Category->find('all',array('conditions' => array('Category.project' =>$this->request->query['project'],'Category.parent_id' =>1)));
				  
			  }
			  
			  if(!empty($this->request->query['category_id']))
			  {
				  $searchCond[]="Category.category_id='".$this->request->query['category_id']."'";
				  //Get sub category if project selected...
				  $searchSubCategories = $this->Category->find('all',array('conditions' => array('Category.parent_id' =>$this->request->query['category_id'])));
			  }
			  
			   if(!empty($this->request->query['subcategory_id']))
			  {
				  $searchCond[]="Content.category_id='".$this->request->query['subcategory_id']."'";
				  
			  }
			  
			  if(!empty($this->request->query['searchStr'])){
				  if($this->request->query['searchIn']=="ct_id"){
					  $searchCond[]="Content.content_id='".$this->request->query['searchStr']."'";
				  }
				  if($this->request->query['searchIn']=="name"){
					  $name=str_replace('+','', $this->request->query['searchStr']);
  
					  $searchCond[]="Content.name like '%".$name."%'";
				  }
				  if($this->request->query['searchIn']=="ct_price"){
					  $searchCond[]="Content.price='".$this->request->query['searchStr']."'";
				  } 
				  if($this->request->query['searchIn']=="keyword"){
					  $searchCond[]="Content.name like '%".$this->request->query['searchStr']."%' OR Content.description like '%".$this->request->query['searchStr']."%'";
				  } 
				  
			  } 
			  
			  
		  } 
		
		}
			
		$searchCond[]="Content.approved = 0";	
		//Find all user id...
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="FIND_IN_SET (Content.user_id, '$finalUserArr')";
		}
		
		$searchCondStr=implode(" AND ",$searchCond);
		
		
	       $query="SELECT `Content`.`content_id`, `Content`.`category_id`, `Content`.`name`,`Content`.`content`, `Content`.`price`, `Content`.`description`, `Content`.`content`, `Content`.`poster`, `Content`.`content_type`, `Content`.`language_code`, `Content`.`status`, `Content`.`created`, `Content`.`modified`, `Content`.`ip`, `Content`.`user_id`, `Content`.`approved` , Category.name, Category.category_id, Subcategory.project, Subcategory.name, Subcategory.category_id, Users.user_id as username
			FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as Subcategory, cm_users as Users 
			WHERE Content.category_id=Subcategory.category_id 
			AND Subcategory.parent_id = Category.category_id 
			AND Users.id=`Content`.`user_id`
			AND $searchCondStr  
			order by content_id desc 
			limit $offset,$RowofPerpage";
			
	
		$contents = $this->Content->query($query);	
		
		$downloadsRow=$this->Content->query("SELECT count(Content.content_id) as count FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as Subcategory, cm_users as Users WHERE Content.category_id=Subcategory.category_id 
		AND Subcategory.parent_id = Category.category_id 
		AND Users.id=`Content`.`user_id`
		AND $searchCondStr");
	
		$num = $downloadsRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		
		$this->set('searchCategories',$searchCategories);
		$this->set('searchSubCategories',$searchSubCategories);
		
						
		$this->set(compact('contents', $contents));	
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		
		$this->set('categoryList', $categoryList);
		
	}
	

	public function rejected_content_list($page=0) 
	{
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$searchCategories=array();
		$searchSubCategories=array();
		if($this->request->is('post'))
		{
			$check = $_POST['check'];
			if($check){
				 foreach($check as $checkone)
				 {
					 $this->Content->query("UPDATE cm_contents SET approved=1 WHERE content_id='".$checkone."'");
					 $this->Session->setFlash('Content Approved Successfully');
				 }	
			}
		}
		
		if($this->request->is('get')){	
			$this->loadModel('Category');
			
		  if(isset($this->request->query['searchForm']))	
		  { 			
			  if(!empty($this->request->query['project']))
			  {
				  $searchCond[]="Subcategory.project='".$this->request->query['project']."'";
				  //find get category if project selected...
				  $searchCategories = $this->Category->find('all',array('conditions' => array('Category.project' =>$this->request->query['project'],'Category.parent_id' =>1) ));
			  }
			  
			  if(!empty($this->request->query['category_id']))
			  {
				  $searchCond[]="Category.category_id='".$this->request->query['category_id']."'";
				  //find sub category if project selected...
				  $searchSubCategories = $this->Category->find('all',array('conditions' => array('Category.parent_id' =>$this->request->query['category_id']) ));
			  }
			  
			   if(!empty($this->request->query['subcategory_id']))
			  {
				  $searchCond[]="Content.category_id='".$this->request->query['subcategory_id']."'";
				  
			  }
			  
			  if(!empty($this->request->query['searchStr'])){
				  if($this->request->query['searchIn']=="ct_id"){
					  $searchCond[]="Content.content_id='".$this->request->query['searchStr']."'";
				  }
				  if($this->request->query['searchIn']=="name"){
					  $name=str_replace('+','', $this->request->query['searchStr']);
  
					  $searchCond[]="Content.name like '%".$name."%'";
				  }
				  if($this->request->query['searchIn']=="ct_price"){
					  $searchCond[]="Content.price='".$this->request->query['searchStr']."'";
				  } 
				  if($this->request->query['searchIn']=="keyword"){
					  $searchCond[]="Content.name like '%".$this->request->query['searchStr']."%' OR Content.description like '%".$this->request->query['searchStr']."%'";
				  } 
				  
			    } 
		    } 
		
		}
			
		$searchCond[]="Content.approved = 2";	
		//Find all user id...
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="FIND_IN_SET (Content.user_id, '$finalUserArr')";

		}
		
		$searchCondStr=implode(" AND ",$searchCond);
		
		//print_r($searchCondStr);die;
		$query="SELECT `Content`.`content_id`, `Content`.`category_id`, `Content`.`name`,`Content`.`content`, `Content`.`price`, `Content`.`description`, `Content`.`content`, `Content`.`poster`, `Content`.`content_type`, `Content`.`language_code`, `Content`.`status`, `Content`.`created`, `Content`.`modified`, `Content`.`ip`, `Content`.`user_id`, `Content`.`approved` ,`Content`.`approved_by`, Category.name, Category.category_id, Subcategory.project, Subcategory.name, Subcategory.category_id, Users.user_id as username
			FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as Subcategory, cm_users as Users
			WHERE Content.category_id=Subcategory.category_id 
			
			AND Subcategory.parent_id = Category.category_id 
			AND Users.id=`Content`.`approved_by`
			AND $searchCondStr  
			order by content_id desc 
			limit $offset,$RowofPerpage";
	
		$contents=$this->Content->query($query);	
		
		$downloadsRow=$this->Content->query("SELECT count(Content.content_id) as count FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as Subcategory, cm_users as Users	WHERE Content.category_id=Subcategory.category_id 
		AND Subcategory.parent_id = Category.category_id 
		AND Users.id=`Content`.`approved_by`
		AND $searchCondStr");
	
		$num = $downloadsRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		
		$this->set('searchCategories', $searchCategories);
		$this->set('searchSubCategories', $searchSubCategories);
		$this->set('contents', $contents);
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		
		$this->set('project', $project);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);	
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		
		$this->set('categoryList', $categoryList);

	}
	

 
	 public function approved_contents($page=0)
	 {
		 
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$searchCategories=array();
		$searchSubCategories=array();
		if($this->request->is('post'))
		{
			$check = $_POST['check'];
			if($check){
				 foreach($check as $checkone)
				 {
					 $this->Content->query("UPDATE cm_contents SET approved=1 WHERE content_id='".$checkone."'");
					 $this->Session->setFlash('Content Approved Successfully');
				 }	
			}
		}
		
		if($this->request->is('get')){	
			$this->loadModel('Category');
			
		  if(isset($this->request->query['searchForm']))	
		  { 			
			  if(!empty($this->request->query['project']))
			  {
				  $searchCond[]="Subcategory.project='".$this->request->query['project']."'";
				  //find get category if project selected...
				  $searchCategories = $this->Category->find('all',array('conditions' => array('Category.project' =>$this->request->query['project'],'Category.parent_id' =>1) ));
			  }
			  
			  if(!empty($this->request->query['category_id']))
			  {
				  $searchCond[]="Category.category_id='".$this->request->query['category_id']."'";
				  //find sub category if project selected...
				  $searchSubCategories = $this->Category->find('all',array('conditions' => array('Category.parent_id' =>$this->request->query['category_id']) ));
			  }
			  
			   if(!empty($this->request->query['subcategory_id']))
			  {
				  $searchCond[]="Content.category_id='".$this->request->query['subcategory_id']."'";
				  
			  }
			  
			  if(!empty($this->request->query['searchStr'])){
				  if($this->request->query['searchIn']=="ct_id"){
					  $searchCond[]="Content.content_id='".$this->request->query['searchStr']."'";
				  }
				  if($this->request->query['searchIn']=="name"){
					  $name=str_replace('+','', $this->request->query['searchStr']);
  
					  $searchCond[]="Content.name like '%".$name."%'";
				  }
				  if($this->request->query['searchIn']=="ct_price"){
					  $searchCond[]="Content.price='".$this->request->query['searchStr']."'";
				  } 
				  if($this->request->query['searchIn']=="keyword"){
					  $searchCond[]="Content.name like '%".$this->request->query['searchStr']."%' OR Content.description like '%".$this->request->query['searchStr']."%'";
				  } 
				  
			    } 
		    } 
		
		}
			
		$searchCond[]="Content.approved = 1";	
		//Find all user id...
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="FIND_IN_SET (Content.user_id, '$finalUserArr')";

		}
		
		$searchCondStr=implode(" AND ",$searchCond);
		
		//print_r($searchCondStr);die;
		$query="SELECT `Content`.`content_id`, `Content`.`category_id`, `Content`.`name`,`Content`.`content`, `Content`.`price`, `Content`.`description`, `Content`.`content`, `Content`.`poster`, `Content`.`content_type`, `Content`.`language_code`, `Content`.`status`, `Content`.`created`, `Content`.`modified`, `Content`.`ip`, `Content`.`user_id`, `Content`.`approved` ,`Content`.`approved_by`, Category.name, Category.category_id, Subcategory.project, Subcategory.name, Subcategory.category_id, Users.user_id as username
			FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as Subcategory, cm_users as Users
			WHERE Content.category_id=Subcategory.category_id 
			
			AND Subcategory.parent_id = Category.category_id 
			AND Users.id=Content.approved_by
			AND $searchCondStr  
			order by content_id desc 
			limit $offset,$RowofPerpage";
			
			
	
		$contents=$this->Content->query($query);	
		
		
		$downloadsRow=$this->Content->query("SELECT count(Content.content_id) as count FROM `cm_contents` AS `Content`, cm_categories as Category, cm_categories as Subcategory, cm_users as Users WHERE Content.category_id=Subcategory.category_id 
		AND Subcategory.parent_id = Category.category_id 
		AND Users.id=`Content`.`approved_by`
		AND $searchCondStr ");
	
		$num = $downloadsRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		
		$this->set('searchCategories', $searchCategories);
		$this->set('searchSubCategories', $searchSubCategories);
		$this->set('contents', $contents);
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		
		$this->set('project', $project);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		
		$categoryList = $this->Category->query("select category_id, name, project from cm_categories as Category where status='A' and parent_id=0 order by display_order");
		$this->set('categoryList', $categoryList);
		
	 }
	 
	 

 
    public function publishercontentview($page=0){
		
		
		//$this->layout = 'publisher';
		$this->loadModel('Category');
	
		$searchCategories=array();
		$searchSubCategories=array();
		$subCategory1 ="";
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$topCategories = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id='0' order by display_order");
		$this->set('topCategory', $topCategories);
		if(isset($this->request->query['searchForm']))	
	    {
			if(!empty($this->request->query['category_id']))
			{
				$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id ='".$this->request->query['category_id']."' order by display_order");
				$subCatArray = array();
				$subCatNameArray = array();
				foreach($subCategory as $key=>$val){
					
					$subCatArray[] = $val["Category"]["category_id"];
					
				}
				
				$subCategory1 = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id ='".$this->request->query['category_id']."' order by display_order");
			}
			
			if(!empty($this->request->query['subcategory_id']))
			{
				$subCatArray = array();
				$subCatArray[] = $this->request->query['subcategory_id'];
				
			}
			if((empty($this->request->query['category_id'])) && (empty($this->request->query['subcategory_id'])))
			{
				$parCatArray = array();
				foreach($topCategories as $key=>$val){
					
					$parCatArray[] = $val["Category"]["category_id"];
				}
				$finalParCatArr = implode(",", $parCatArray);
				
				$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id in($finalParCatArr) order by display_order");
				$subCatArray = array();
				foreach($subCategory as $key=>$val){
					
					$subCatArray[] = $val["Category"]["category_id"];
				}
			}
			
			if(!empty($this->request->query['content_name']))
			{
				$searchCond[] = " t1.name like '%".$this->request->query['content_name']."%'";
			}
			
	    } else {
			$parCatArray = array();
			foreach($topCategories as $key=>$val){
				
				$parCatArray[] = $val["Category"]["category_id"];
			}
			$finalParCatArr = implode(",", $parCatArray);
			
			$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id in($finalParCatArr) order by display_order");
			$subCatArray = array();
			foreach($subCategory as $key=>$val){
				
				$subCatArray[] = $val["Category"]["category_id"];
			}
			
		}
		
		
		$finalSubCatArr = implode(",", $subCatArray);
		
		$searchCond[] ="FIND_IN_SET(t1.category_id, '$finalSubCatArr')";
		
		
		$searchCond[]="1 = 1";
		$searchCond[]="t1.approved = 1";
		$searchCondStr=implode(" AND ",$searchCond);
		
		//echo $query = "select * from cm_contents where status='A' and $searchCondStr order by date(modified) desc limit $offset,$RowofPerpage";
		$query = "select t1.*, t2.name as category_name from cm_contents as t1 inner join cm_categories as t2 on t2.category_id=t1.category_id where t1.status='A' and $searchCondStr order by date(t1.created) desc limit $offset,$RowofPerpage";
		$contents = $this->Content->query($query);
		//print_r($contents);
		//die;
		
		$countRow=$this->Content->query("SELECT count(content_id) as count from cm_contents as t1 where status='A' and $searchCondStr order by date(modified) desc");
	
		$num = $countRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		$this->set('searchSubCategories', $subCategory1);
		$this->set('contents', $contents);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
	
	}
	
	public function publisherpurchaseview($page=0){
		$this->layout = 'publisher';
		$this->loadModel('Category');
		$this->loadModel('CmPublisherContent');
		$this->loadModel('PublisherOrderLine');
		$this->loadModel('PublisherOrder');
		
		$searchCategories=array();
		$searchSubCategories=array();
		$subCategory1 ="";
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$topCategories = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id='0' order by display_order");
		$this->set('topCategory', $topCategories);
		if(isset($this->request->query['searchForm']))	
	    {
			if(!empty($this->request->query['category_id']))
			{
				$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id ='".$this->request->query['category_id']."' order by display_order");
				$subCatArray = array();
				$subCatNameArray = array();
				foreach($subCategory as $key=>$val){
					
					$subCatArray[] = $val["Category"]["category_id"];
					
				}
				
				$subCategory1 = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id ='".$this->request->query['category_id']."' order by display_order");
			}
			
			if(!empty($this->request->query['subcategory_id']))
			{
				$subCatArray = array();
				$subCatArray[] = $this->request->query['subcategory_id'];
				
			}
			if((empty($this->request->query['category_id'])) && (empty($this->request->query['subcategory_id'])))
			{
				$parCatArray = array();
				foreach($topCategories as $key=>$val){
					
					$parCatArray[] = $val["Category"]["category_id"];
				}
				$finalParCatArr = implode(",", $parCatArray);
				
				$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id in($finalParCatArr) order by display_order");
				$subCatArray = array();
				foreach($subCategory as $key=>$val){
					
					$subCatArray[] = $val["Category"]["category_id"];
				}
			}
			
			if(!empty($this->request->query['content_name']))
			{
				$searchCond[] = " t1.name like '%".$this->request->query['content_name']."%'";
			}
			
	    } else {
			$parCatArray = array();
			foreach($topCategories as $key=>$val){
				
				$parCatArray[] = $val["Category"]["category_id"];
			}
			$finalParCatArr = implode(",", $parCatArray);
			
			$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id in($finalParCatArr) order by display_order");
			$subCatArray = array();
			foreach($subCategory as $key=>$val){
				
				$subCatArray[] = $val["Category"]["category_id"];
			}
			
		}
		
		
		$finalSubCatArr = implode(",", $subCatArray);
		
		$searchCond[] ="FIND_IN_SET(t1.category_id, '$finalSubCatArr')";
		
		
		$searchCond[]="1 = 1";
		$searchCond[]="t1.approved = 1";
		$searchCondStr=implode(" AND ",$searchCond);
		
		//echo $query = "select * from cm_contents where status='A' and $searchCondStr order by date(modified) desc limit $offset,$RowofPerpage";
		$query = "select t1.*, t2.name as category_name from cm_contents as t1 inner join cm_categories as t2 on t2.category_id=t1.category_id where t1.status='A' and $searchCondStr order by date(t1.modified) desc limit $offset,$RowofPerpage";
		$contents = $this->Content->query($query);
		//print_r($contents);
		//die;
		
		$countRow=$this->Content->query("SELECT count(content_id) as count from cm_contents as t1 where status='A' and $searchCondStr order by date(modified) desc");
	
		$num = $countRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		$this->set('searchSubCategories', $subCategory1);
		$this->set('contents', $contents);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		
		//to check for unable to select content by publisher...
		
		$allreadySelectedContent = $this->PublisherOrder->query("select t1.content_id as content_id from publisher_order_line as t1 left join publisher_order as t2 on t1.order_id=t2.id where t2.publisher_id='".$this->Session->read('User.id')."'");
		
		
		$finalContentrec = array();
		if(!empty($allreadySelectedContent)){
			foreach($allreadySelectedContent as $allreadySelectedContents){
				$finalContentrec[] = $allreadySelectedContents["t1"]["content_id"];
			}	
		}		
		$this->set('finalcontentArray', $finalContentrec);
		
		//insert publisher data in publisher_order and publisher_order_line_table...
		//print_r($finalContentrec);
		//die;
		
		$allpublisherContent = "";
		$allSubcat = "";
		$allCat = "";
		
		if (isset($_POST["purchaseSubmit"])) 
		{	
			$sub_category_id = "";
			$category_id = "";
	
			//check data from category and sub category basis...
			$allpublisherContent = "";
			$allpublisherContent = $this->PublisherOrder->query("select count(id) as count from publisher_order where publisher_id='".$this->Session->read('User.id')."' and selection_type=0");
			
			if($allpublisherContent[0][0]["count"] >=1)
			{
				$this->Session->setFlash('You have allready added all content.');
				$this->redirect('publisherpurchaseview');		
				
			} else if((@$_GET["subcategory_id"] !="") && ($_POST["selectpage"] == "all"))
			{
				
				$allSubcat = $this->PublisherOrder->query("select count(id) as count from publisher_order where publisher_id='".$this->Session->read('User.id')."'  and selection_type=2 and sub_category_id='".$_GET["subcategory_id"]."'");
				
				if($allSubcat[0][0]["count"] >=1){
					$this->Session->setFlash('You have allready added all content of selected category.');
					$this->redirect('publisherpurchaseview');
					
				} else {
				
					$sub_category_id = $_GET["subcategory_id"];
					$category_id = "";
					
					$contentRecChk = $this->Content->query("select content_id from cm_contents where status='A' and category_id='".$_GET["subcategory_id"]."'");
					
					
				}
			} else if((@$_GET["category_id"] > 0) && ($_POST["selectpage"] == "all") && (@$_GET["subcategory_id"] =="")){
				$allCat = $this->PublisherOrder->query("select count(id) as count from publisher_order where publisher_id='".$this->Session->read('User.id')."'  and selection_type=1 and category_id='".$_GET["category_id"]."'");
				if($allCat[0][0]["count"]){
					$this->Session->setFlash('You have allready added all content of selected category.');
					$this->redirect('publisherpurchaseview');
						
				} else {
					$category_id = $_GET["category_id"];
					$subcategory_id = "";
					$contentRecChk = $this->Content->query("SELECT cm_contents.content_id FROM `cm_contents` as cm_contents inner join cm_categories as t2 on t2.category_id=cm_contents.category_id and t2.parent_id='".$_GET["category_id"]."' and cm_contents.status='A' and cm_contents.status='A'");
					
				}
				
			} else if(isset($_GET["category_id"]) && (@$_GET["category_id"] == 0) && ($_POST["selectpage"] == "all") && (@$_GET["subcategory_id"] =="")){
				
				$category_id = $_GET["category_id"];
				$subcategory_id = "";
				$contentRecChk = $this->Content->query("select content_id from cm_contents where status='A'");
			 
			} else if(($_POST["selectpage"] == "all") && (@$_GET["category_id"] == "") && (@$_GET["subcategory_id"] =="")){
				$category_id = 0;
				$subcategory_id = "";
				$contentRecChk = $this->Content->query("select content_id from cm_contents where status='A'");
			 
			}
			
			if((!empty($contentRecChk)) ){
				
				$insertContentIds = array();
				$finalInsertContentIds = array();
				foreach($contentRecChk as $contentRecChks){
					$insertContentIds[] = $contentRecChks["cm_contents"]["content_id"];
				}
				$finalInsertContentIds = array_diff($insertContentIds, $finalContentrec);
				if(empty($finalInsertContentIds)){
					$this->Session->setFlash('You have allready added all content of selected matching.');
					$this->redirect('publisherpurchaseview');
				} 
				
				
			} else if($_POST["selectpage"] != "all"){
				if(empty($_POST["contentId"])){
					$this->Session->setFlash('You have not selected any content.');
					$this->redirect('publisherpurchaseview');
					
				} else {
					//$contentId = implode(",", $_POST["contentId"]);
					$finalInsertContentIds = $_POST["contentId"];
					
				}
			} 
			
			    $date = date('Y-m-d H:i:s');
				$sql = "insert into publisher_order set ";
				$sql = $sql . "publisher_id = '" . $this->Session->read('User.id') . "'";
				if($sub_category_id != ""){
					$sql = $sql . ",selection_type = 2";
					$sql = $sql . ",sub_category_id = '" . $sub_category_id . "'";
				}else if($category_id != "" && $sub_category_id == "" ){
					if($category_id != 0){
						$sql = $sql . ",selection_type = 1";
						$sql = $sql . ",category_id = '" . $category_id . "'";
					} else {
						$sql = $sql . ",selection_type = 0";
						
					}
				} else {
					$sql = $sql . ",selection_type = 3";
					
				}
				
				$sql = $sql . ",created = '" . $date . "'";
				//echo $sql;
				//die;
				$insertRec = $this->PublisherOrder->query($sql);
				$result = $this->PublisherOrder->query("SELECT LAST_INSERT_ID() as insertID");
				
				$orderId = $result[0][0]["insertID"];
				//print_r($finalInsertContentIds);
				//die;
				for($i=0; $i< count($finalInsertContentIds); $i++){
					if(isset($finalInsertContentIds[$i])){
						$sql1 = "insert into publisher_order_line set ";
						$sql1 = $sql1 . "order_id = '" . $orderId . "'";
						$sql1 = $sql1 . ",content_id = '" . $finalInsertContentIds[$i] . "'";
						$insertOLineRec = $this->PublisherOrderLine->query($sql1);
					}
				}
				
				
				$this->Session->setFlash('Content added successfully.');
				$this->redirect('publisherpurchaseview');
			
			
		}
		
	
	}
	
	public function purchasecontentview($page=0){
			
		$this->layout = 'publisher';
		$this->loadModel('Category');
		$this->loadModel('Content');
		$this->loadModel('CmPublisherContent');
		$this->loadModel('PublisherOrderLine');
		$this->loadModel('PublisherOrder');
		
		$searchCategories=array();
		$searchSubCategories=array();
		$countRow =array();
		$contents =array();
		$finalpurchasedDate =array();
		
		$subCategory1 ="";
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		/*$topCategories = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id='0' order by display_order");
		$this->set('topCategory', $topCategories);
				
		$publisherRec = $this->CmPublisherContent->query("select * from cm_publisher_contents where publisher_id='".$this->Session->read('User.id')."' order by selection_type desc");
		//print_r($publisherRec);
		//die;
		
		$subCatArr = array();
		foreach($publisherRec as $publisherRec1){
			if($publisherRec1["cm_publisher_contents"]["selection_type"] == 1){
				$subCat = $this->Category->query("select category_id from cm_categories where parent_id='".$publisherRec1["cm_publisher_contents"]["category_id"]."'");
				
				foreach($subCat as $subCatR){
					$subCatArr[] = $subCatR["cm_categories"]["category_id"];
				}
			}
		}
	    $contentIds = array();
		$k=0;
		$z=0;
		$contentImp = "";
		$subCatArr1 = array();
		foreach($publisherRec as $publisherRec){
			
			if($publisherRec["cm_publisher_contents"]["selection_type"] == 3){
				$contentRec = explode(",", $publisherRec["cm_publisher_contents"]["content_id"]);
								
				for($i=0; $i<count($contentRec); $i++){
					$contentIds[$k]["content_id"] = $contentRec[$i];
					$contentIds[$k]["purchasedDate"] = $publisherRec["cm_publisher_contents"]["created"];
					$contIds[$k] = $contentRec[$i];
					$k++;
				}
				
			} else if($publisherRec["cm_publisher_contents"]["selection_type"] == 2){
				if(!empty($contIds)){
					$finalcontIds = implode(",", $contIds);
					$strCond = " and not FIND_IN_SET (content_id, '$finalcontIds')";
				} else {
					$strCond = "";
				}
								
				$contents = $this->Content->query("select content_id from cm_contents where category_id='".$publisherRec["cm_publisher_contents"]["sub_category_id"]."' and status='A' $strCond");
				$subCatIds[$k] = $publisherRec["cm_publisher_contents"]["sub_category_id"];
				$contentArr = array();
				$y=0;
				foreach($contents as $contents){
					$contentArr[$y]["content_id"] = $contents["cm_contents"]["content_id"];
					$contentArr[$y]["purchasedDate"] = $publisherRec["cm_publisher_contents"]["created"];
					$contIds[] = $contents["cm_contents"]["content_id"];
				
					$y++;
				}
				$contentIds = array_merge($contentIds, $contentArr);
			} else if($publisherRec["cm_publisher_contents"]["selection_type"] == 1){
				if(!empty($subCatIds)){
					$finalsubCatIds = implode(",", $subCatIds);
					$strCatCond = " and not FIND_IN_SET (category_id, '$finalsubCatIds')";
					
				} else {
					$strCatCond = "";
				}
				$subCat = $this->Category->query("select category_id from cm_categories where parent_id='".$publisherRec["cm_publisher_contents"]["category_id"]."' $strCatCond");

				foreach ($subCatIds as $eachSubcatIds){
					if (($key = array_search($eachSubcatIds, $subCatArr)) !== false) {
						unset($subCatArr[$key]);
					}
				}
				
				if(!empty($subCatArr)){
					$finalsubCatArr = implode(",", $subCatArr);
					$strCatCond = " and FIND_IN_SET (category_id, '$finalsubCatArr')";
				} else {
					$strCatCond = "";
				}
				
				if(!empty($contIds)){
					$finalcontIds = implode(",", $contIds);
					$strCond = " and not FIND_IN_SET (content_id, '$finalcontIds')";
				} else {
					$strCond = "";
				}
				if($z == 0){
					
					$contents1 = $this->Content->query("select content_id from cm_contents where status='A' $strCatCond $strCond");
					
					$contentArr1 = array();
					$t=0;
					foreach($contents1 as $contents1){
						$contentArr1[$t]["content_id"] = $contents1["cm_contents"]["content_id"];
						$contentArr1[$t]["purchasedDate"] = $publisherRec["cm_publisher_contents"]["created"];
						$contIds[] = $contents1["cm_contents"]["content_id"];
					
						$t++;
					}
					$contentIds = array_merge($contentIds, $contentArr1);
				}
				$z++;
			} else if($publisherRec["cm_publisher_contents"]["selection_type"] == 0){
				if(!empty($contIds)){
					$finalcontIds = implode(",", $contIds);
					$strCond = " and not FIND_IN_SET (content_id, '$finalcontIds')";
				} else {
					$strCond = "";
				}
				
				$contents2 = $this->Content->query("select content_id from cm_contents where status='A' $strCond");
				
				$contentArr2 = array();
				$r=0;
				foreach($contents2 as $contents2){
					$contentArr2[$r]["content_id"] = $contents2["cm_contents"]["content_id"];
					$contentArr2[$r]["purchasedDate"] = $publisherRec["cm_publisher_contents"]["created"];
					$contIds[] = $contents2["cm_contents"]["content_id"];
				
					$r++;
				}
				
				$contentIds = array_merge($contentIds, $contentArr2);
				
			}
			$k++;
			
		}
		//echo "<pre>";
		//print_r($contentIds);
		//die;
		if(!empty($contentIds)){
			$finalContentrec = array();
			$finalpurchasedDate = array();
			
			//$contentIds = sort($contentIds);
			
			foreach($contentIds as $newcontentIds){
				$finalContentrec[] = $newcontentIds["content_id"];
				$finalpurchasedDate[$newcontentIds["content_id"]] = $newcontentIds["purchasedDate"];
				
			}
			if(!empty($finalContentrec)){
			$finalcontIdsRec = implode("','", $finalContentrec);
				$finalstrCond = " where content_id in ('$finalcontIdsRec')";
			} else {
				$finalstrCond = "";
			}
			//echo "select * from cm_contents $finalstrCond";
			$contentsFinal = $this->Content->query("select * from cm_contents as t1 $finalstrCond  limit $offset,$RowofPerpage");
			
			$countRow=$this->Content->query("SELECT count(content_id) as count from cm_contents $finalstrCond");
			
		}
		
	
		$this->set('finalpurchasedDate', $finalpurchasedDate);*/
		if(isset($this->request->query['searchForm'])){
			if(!empty($this->request->query['content_name']))
			{
				$searchCond = " and t1.name like '%".$this->request->query['content_name']."%'";
			}
		}
		
		$contentsFinal = $this->PublisherOrder->query("select t1.*, t3.created as purchaseDate from cm_contents as t1 inner join publisher_order_line as t2 on t2.content_id=t1.content_id inner join publisher_order as t3 on t2.order_id=t3.id where t3.publisher_id='".$this->Session->read('User.id')."' $searchCond order by t3.created desc limit $offset,$RowofPerpage");
		
		
		$countRow=$this->Content->query("select count(t1.content_id) as count from cm_contents as t1 inner join publisher_order_line as t2 on t2.content_id=t1.content_id inner join publisher_order as t3 on t2.order_id=t3.id where t3.publisher_id='".$this->Session->read('User.id')."' $searchCond");

		
		$this->set('contents', $contentsFinal);
		
		$num = $countRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		
		
	}
	
	/*public function publisherpurchaseview($page=0){
		
		$this->layout = 'publisher';
		$this->loadModel('Category');
		$this->loadModel('CmPublisherContent');
		
		$searchCategories=array();
		$searchSubCategories=array();
		$subCategory1 ="";
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		$topCategories = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id='0' order by display_order");
		$this->set('topCategory', $topCategories);
		if(isset($this->request->query['searchForm']))	
	    {
			if(!empty($this->request->query['category_id']))
			{
				$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id ='".$this->request->query['category_id']."' order by display_order");
				$subCatArray = array();
				$subCatNameArray = array();
				foreach($subCategory as $key=>$val){
					
					$subCatArray[] = $val["Category"]["category_id"];
					
				}
				
				$subCategory1 = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id ='".$this->request->query['category_id']."' order by display_order");
			}
			
			if(!empty($this->request->query['subcategory_id']))
			{
				$subCatArray = array();
				$subCatArray[] = $this->request->query['subcategory_id'];
				
			}
			if((empty($this->request->query['category_id'])) && (empty($this->request->query['subcategory_id'])))
			{
				$parCatArray = array();
				foreach($topCategories as $key=>$val){
					
					$parCatArray[] = $val["Category"]["category_id"];
				}
				$finalParCatArr = implode(",", $parCatArray);
				
				$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id in($finalParCatArr) order by display_order");
				$subCatArray = array();
				foreach($subCategory as $key=>$val){
					
					$subCatArray[] = $val["Category"]["category_id"];
				}
			}
			
			if(!empty($this->request->query['content_name']))
			{
				$searchCond[] = " t1.name like '%".$this->request->query['content_name']."%'";
			}
			
	    } else {
			$parCatArray = array();
			foreach($topCategories as $key=>$val){
				
				$parCatArray[] = $val["Category"]["category_id"];
			}
			$finalParCatArr = implode(",", $parCatArray);
			
			$subCategory = $this->Category->query("select category_id, name from cm_categories as Category where status='A' and parent_id in($finalParCatArr) order by display_order");
			$subCatArray = array();
			foreach($subCategory as $key=>$val){
				
				$subCatArray[] = $val["Category"]["category_id"];
			}
			
		}
		
		
		$finalSubCatArr = implode(",", $subCatArray);
		
		$searchCond[] ="FIND_IN_SET(t1.category_id, '$finalSubCatArr')";
		
		
		$searchCond[]="1 = 1";
		$searchCond[]="t1.approved = 1";
		$searchCondStr=implode(" AND ",$searchCond);
		
		//echo $query = "select * from cm_contents where status='A' and $searchCondStr order by date(modified) desc limit $offset,$RowofPerpage";
		$query = "select t1.*, t2.name as category_name from cm_contents as t1 inner join cm_categories as t2 on t2.category_id=t1.category_id where t1.status='A' and $searchCondStr order by date(t1.modified) desc limit $offset,$RowofPerpage";
		$contents = $this->Content->query($query);
		//print_r($contents);
		//die;
		
		$countRow=$this->Content->query("SELECT count(content_id) as count from cm_contents as t1 where status='A' and $searchCondStr order by date(modified) desc");
	
		$num = $countRow[0][0]['count'];
		$NumofPage=ceil($num/$RowofPerpage);
		
		$this->set('searchSubCategories', $subCategory1);
		$this->set('contents', $contents);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		
		/*$publisherContent = $this->CmPublisherContent->query("select * from cm_publisher_contents where publisher_id = '".$this->Session->read('User.id')."'");
		
		
		$contentIdRec = array();
		$finalcontentArray = array();
		foreach( $publisherContent as $key=>$val){
			
			$publisherContents = explode(",", $val["cm_publisher_contents"]["content_id"]);
			
			$contentIdRec = array_merge($contentIdRec, $publisherContents); 
			
		}
		$finalcontentArray = array_unique($contentIdRec);
		//print_r($finalcontentArray);
		$this->set('finalcontentArray', $finalcontentArray);
		//die;
		
	}*/
	
	public function purchasecontentviewDownload(){
		
		$this->layout = null;
	    $this->autoLayout = false;
		$this->loadModel('Category');
		$this->loadModel('Content');
		$this->loadModel('CmPublisherContent');
		$this->loadModel('PublisherOrderLine');
		$this->loadModel('PublisherOrder');
		$date = date('Y-m-d');
		
		$filename = "purchase_content_".$date.".csv";
		$csv_file = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		$results = $this->PublisherOrder->query("select t2.content_id as contentId, t1.name as contentName, t3.created as purchaseDate, t4.name as sabCatName, t5.name as catName from cm_contents as t1 inner join publisher_order_line as t2 on t2.content_id=t1.content_id inner join publisher_order as t3 on t2.order_id=t3.id inner join cm_categories as t4 on t4.category_id=t1.category_id  join cm_categories as t5 on t5.category_id=t4.parent_id where t3.publisher_id='".$this->Session->read('User.id')."' order by t3.created desc");	
		$header_row = array("Content Name", "Purchase Date", "Category Name", "Sub Category Name", "Url");
		
		fputcsv($csv_file,$header_row,',','"');
		foreach($results as $result=>$obj){
			$randVal = $this->Content->fRand(5);
			$publisherid = base64_encode($this->Session->read('User.id'));
		    $contentid = base64_encode($obj['t2']['contentId']);
			$url = "http://vuhu.in/videohit.php?param=".$randVal.'$'.$publisherid."$".$contentid."";
			
			$row = array(
				$obj['t1']['contentName'],	
				$obj['t3']['purchaseDate'],
				$obj['t5']['catName'],
				$obj['t4']['sabCatName'],
				$url
				);
							
			fputcsv($csv_file,$row,',','"');
        }
		fclose($csv_file);die;
		
	}
	
}

?>
