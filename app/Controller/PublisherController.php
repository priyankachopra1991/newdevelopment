<?php
App::uses('HttpSocket', 'Network/Http');
class PublisherController extends AppController
{
	public $name = 'Publisher';
	public $uses = array('Appnamelist','Fcappcontent','FcappBulkupload','FcappContentPrice','Fcappcategories','Channel','Country', 'User', 'Language');
	public $components = array('Paginator');
	public function beforeFilter() {
		parent::beforeFilter();
	}	
	public function publishercontentview($page=0){
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
	    $country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country); 	
		$data=$this->request->query;
		if($this->request->is('post'))
		{
			$value=$this->data;
			$user_id=$this->Session->read('User.user_id');
			if(isset($value['statusn']))
			    {
					if($value['statusn']=="2")
					{
					    foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
					    $this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="1")
					{
						foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="T")
					{
						foreach($value['check'] as $checkone)
					    {
							$modified = date('YmdHis');
						    $this->Fcappcontent->query("UPDATE cm_contents SET trend = 'T',modified = '".$modified."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="0")
					   {
						   foreach($value['check'] as $checkone)
					       {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 0 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="A")
					{
						foreach($value['check'] as $checkone)
					    {
						 $this->Fcappcontent->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');
						$this->redirect('approvedcontent');						
					}
					if($value['statusn']=="D")
					{
						foreach($value['check'] as $checkone)
						{
							$this->Fcappcontent->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');		
						$this->redirect('approvedcontent');							
					}
				
				}
			}
			
			
			if(!empty($data['status']))
			{
				if($data['status']=='A')
				{
					$status= "t1.status='".$data['status']."'";
				}
				else if($data['status']=='D')
				{
					$status="t1.status='".$data['status']."'";
				}
				else if($data['status']=='1')
				{
					$status="t1.approved='".$data['status']."'";
				}
				else if($data['status']=='2')
				{
					$status="t1.approved='".$data['status']."'"; 
				}
			}
			  
			if(!empty($data['country_code']) && !empty($data['status']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and   $status and   t1.NAME like '%".$data['name']."%'";
			} 
			else if(!empty($data['country_code']) && !empty($data['status']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  $status ";
			}
			else if(!empty($data['status']) && !empty($data['name']))
			{
				$strCond = " $status and  t1.NAME like '%".$data['name']."%'";	
			}
			else if(!empty($data['country_code']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['name']))
			{
				$strCond = "t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['status']))
			{
				$strCond = "$status";
			}
			else if(!empty($data['country_code']))
			{	
				$strCond="t1.country_code='".$data['country_code']."'";
			}
			else if(!empty($data['subcategory_id']))
			{	
				$strCond="t1.category_id='".$data['subcategory_id']."'";
			}
			else if(!empty($data['channel_id']))
			{	
				$strCond="t1.channel_id='".$data['channel_id']."'";
			}
			else
			{
				$strCond="(t1.status = 'A' OR t1.status = 'D') ";
			}
			$channelCond = "";
			if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
			{
				
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond[]="FIND_IN_SET (t1.user_id, '$finalUserArr')";
				
				$channelCond = "and FIND_IN_SET (user_id, '$finalUserArr')"; 
			}
			
			$channelList = $this->Channel->query("select id, c_name from ch_channel where status < '3' $channelCond");
				$this->set('channelList', $channelList);
			//data by filter...
			
			if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
				$searchCond[] = " t1.status = '".$_GET["filterBy"]."'" ;
			}
			
			if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
				$orderBy = " order by t1.content_id ".$_GET["sortBy"]."" ;
			}  else {
				$orderBy = " order by t1.content_id desc" ;
			}
			
			$searchCond[]="t1.approved= 1";
			$searchCondStr=implode(" AND ",$searchCond);
			
			$value = $this->Fcappcontent->query("SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage");
			
			$downloadsRow = $this->Fcappcontent->query("SELECT count(t1.content_id) as countRec FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where  $strCond and $searchCondStr  and t2.project='firstcut'");
			
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
	}
	
	public function publisherpurchaseview($page=0){
		
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
	    $country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country); 	
		$data=$this->request->query;
		if($this->request->is('post'))
		{
			$value=$this->data;
			$user_id=$this->Session->read('User.user_id');
			if(isset($value['statusn']))
			    {
					if($value['statusn']=="2")
					{
					    foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
					    $this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="1")
					{
						foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="T")
					{
						foreach($value['check'] as $checkone)
					    {
							$modified = date('YmdHis');
						    $this->Fcappcontent->query("UPDATE cm_contents SET trend = 'T',modified = '".$modified."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="0")
					   {
						   foreach($value['check'] as $checkone)
					       {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 0 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="A")
					{
						foreach($value['check'] as $checkone)
					    {
						 $this->Fcappcontent->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');
						$this->redirect('approvedcontent');						
					}
					if($value['statusn']=="D")
					{
						foreach($value['check'] as $checkone)
						{
							$this->Fcappcontent->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');		
						$this->redirect('approvedcontent');							
					}
				
				}
			}
			
			
			if(!empty($data['status']))
			{
				if($data['status']=='A')
				{
					$status= "t1.status='".$data['status']."'";
				}
				else if($data['status']=='D')
				{
					$status="t1.status='".$data['status']."'";
				}
				else if($data['status']=='1')
				{
					$status="t1.approved='".$data['status']."'";
				}
				else if($data['status']=='2')
				{
					$status="t1.approved='".$data['status']."'"; 
				}
			}
			  
			if(!empty($data['country_code']) && !empty($data['status']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and   $status and   t1.NAME like '%".$data['name']."%'";
			} 
			else if(!empty($data['country_code']) && !empty($data['status']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  $status ";
			}
			else if(!empty($data['status']) && !empty($data['name']))
			{
				$strCond = " $status and  t1.NAME like '%".$data['name']."%'";	
			}
			else if(!empty($data['country_code']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['name']))
			{
				$strCond = "t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['status']))
			{
				$strCond = "$status";
			}
			else if(!empty($data['country_code']))
			{	
				$strCond="t1.country_code='".$data['country_code']."'";
			}
			else if(!empty($data['subcategory_id']))
			{	
				$strCond="t1.category_id='".$data['subcategory_id']."'";
			}
			else if(!empty($data['channel_id']))
			{	
				$strCond="t1.channel_id='".$data['channel_id']."'";
			}
			else
			{
				$strCond="(t1.status = 'A' OR t1.status = 'D') ";
			}
			$channelCond = "";
			if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
			{
				
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond[]="FIND_IN_SET (t1.user_id, '$finalUserArr')";
				
				$channelCond = "and FIND_IN_SET (user_id, '$finalUserArr')"; 
			}
			
			$channelList = $this->Channel->query("select id, c_name from ch_channel where status < '3' $channelCond");
				$this->set('channelList', $channelList);
			//data by filter...
			
			if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
				$searchCond[] = " t1.status = '".$_GET["filterBy"]."'" ;
			}
			
			if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
				$orderBy = " order by t1.content_id ".$_GET["sortBy"]."" ;
			}  else {
				$orderBy = " order by t1.content_id desc" ;
			}
			
			$searchCond[]="t1.approved= 1";
			$searchCondStr=implode(" AND ",$searchCond);
			
			$value = $this->Fcappcontent->query("SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage");
			
			$downloadsRow = $this->Fcappcontent->query("SELECT count(t1.content_id) as countRec FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where  $strCond and $searchCondStr  and t2.project='firstcut'");
			
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
		
		//to check for unable to select content by publisher...
		
		/*$allreadySelectedContent = $this->PublisherOrder->query("select t1.content_id as content_id from publisher_order_line as t1 left join publisher_order as t2 on t1.order_id=t2.id where t2.publisher_id='".$this->Session->read('User.id')."'");
		
		
		$finalContentrec = array();
		if(!empty($allreadySelectedContent)){
			foreach($allreadySelectedContent as $allreadySelectedContents){
				$finalContentrec[] = $allreadySelectedContents["t1"]["content_id"];
			}	
		}		
		$this->set('finalcontentArray', $finalContentrec);
		
		//insert publisher data in publisher_order and publisher_order_line_table...
		//print_r($finalContentrec);
		//die;
		
		$allpublisherContent = "";
		$allSubcat = "";
		$allCat = "";
		
		if (isset($_POST["purchaseSubmit"])) 
		{	
			$sub_category_id = "";
			$category_id = "";
	
			//check data from category and sub category basis...
			$allpublisherContent = "";
			$allpublisherContent = $this->PublisherOrder->query("select count(id) as count from publisher_order where publisher_id='".$this->Session->read('User.id')."' and selection_type=0");
			
			if($allpublisherContent[0][0]["count"] >=1)
			{
				$this->Session->setFlash('You have allready added all content.');
				$this->redirect('publisherpurchaseview');		
				
			} else if((@$_GET["subcategory_id"] !="") && ($_POST["selectpage"] == "all"))
			{
				
				$allSubcat = $this->PublisherOrder->query("select count(id) as count from publisher_order where publisher_id='".$this->Session->read('User.id')."'  and selection_type=2 and sub_category_id='".$_GET["subcategory_id"]."'");
				
				if($allSubcat[0][0]["count"] >=1){
					$this->Session->setFlash('You have allready added all content of selected category.');
					$this->redirect('publisherpurchaseview');
					
				} else {
				
					$sub_category_id = $_GET["subcategory_id"];
					$category_id = "";
					
					$contentRecChk = $this->Content->query("select content_id from cm_contents where status='A' and category_id='".$_GET["subcategory_id"]."'");
					
					
				}
			} else if((@$_GET["category_id"] > 0) && ($_POST["selectpage"] == "all") && (@$_GET["subcategory_id"] =="")){
				$allCat = $this->PublisherOrder->query("select count(id) as count from publisher_order where publisher_id='".$this->Session->read('User.id')."'  and selection_type=1 and category_id='".$_GET["category_id"]."'");
				if($allCat[0][0]["count"]){
					$this->Session->setFlash('You have allready added all content of selected category.');
					$this->redirect('publisherpurchaseview');
						
				} else {
					$category_id = $_GET["category_id"];
					$subcategory_id = "";
					$contentRecChk = $this->Content->query("SELECT cm_contents.content_id FROM `cm_contents` as cm_contents inner join cm_categories as t2 on t2.category_id=cm_contents.category_id and t2.parent_id='".$_GET["category_id"]."' and cm_contents.status='A' and cm_contents.status='A'");
					
				}
				
			} else if(isset($_GET["category_id"]) && (@$_GET["category_id"] == 0) && ($_POST["selectpage"] == "all") && (@$_GET["subcategory_id"] =="")){
				
				$category_id = $_GET["category_id"];
				$subcategory_id = "";
				$contentRecChk = $this->Content->query("select content_id from cm_contents where status='A'");
			 
			} else if(($_POST["selectpage"] == "all") && (@$_GET["category_id"] == "") && (@$_GET["subcategory_id"] =="")){
				$category_id = 0;
				$subcategory_id = "";
				$contentRecChk = $this->Content->query("select content_id from cm_contents where status='A'");
			 
			}
			
			if((!empty($contentRecChk)) ){
				
				$insertContentIds = array();
				$finalInsertContentIds = array();
				foreach($contentRecChk as $contentRecChks){
					$insertContentIds[] = $contentRecChks["cm_contents"]["content_id"];
				}
				$finalInsertContentIds = array_diff($insertContentIds, $finalContentrec);
				if(empty($finalInsertContentIds)){
					$this->Session->setFlash('You have allready added all content of selected matching.');
					$this->redirect('publisherpurchaseview');
				} 
				
				
			} else if($_POST["selectpage"] != "all"){
				if(empty($_POST["contentId"])){
					$this->Session->setFlash('You have not selected any content.');
					$this->redirect('publisherpurchaseview');
					
				} else {
					//$contentId = implode(",", $_POST["contentId"]);
					$finalInsertContentIds = $_POST["contentId"];
					
				}
			} 
			
			    $date = date('Y-m-d H:i:s');
				$sql = "insert into publisher_order set ";
				$sql = $sql . "publisher_id = '" . $this->Session->read('User.id') . "'";
				if($sub_category_id != ""){
					$sql = $sql . ",selection_type = 2";
					$sql = $sql . ",sub_category_id = '" . $sub_category_id . "'";
				}else if($category_id != "" && $sub_category_id == "" ){
					if($category_id != 0){
						$sql = $sql . ",selection_type = 1";
						$sql = $sql . ",category_id = '" . $category_id . "'";
					} else {
						$sql = $sql . ",selection_type = 0";
						
					}
				} else {
					$sql = $sql . ",selection_type = 3";
					
				}
				
				$sql = $sql . ",created = '" . $date . "'";
				//echo $sql;
				//die;
				$insertRec = $this->PublisherOrder->query($sql);
				$result = $this->PublisherOrder->query("SELECT LAST_INSERT_ID() as insertID");
				
				$orderId = $result[0][0]["insertID"];
				//print_r($finalInsertContentIds);
				//die;
				for($i=0; $i< count($finalInsertContentIds); $i++){
					if(isset($finalInsertContentIds[$i])){
						$sql1 = "insert into publisher_order_line set ";
						$sql1 = $sql1 . "order_id = '" . $orderId . "'";
						$sql1 = $sql1 . ",content_id = '" . $finalInsertContentIds[$i] . "'";
						$insertOLineRec = $this->PublisherOrderLine->query($sql1);
					}
				}
				
				
				$this->Session->setFlash('Content added successfully.');
				$this->redirect('publisherpurchaseview');
			*/
			
		//}
	}
}
?>	