<?php
App::uses('AppController', 'Controller');

class PromotionController extends AppController{
	public $name = 'Promotion';
	public $uses = array('Promotion');
	public $components = array('Paginator');
	public function beforeFilter() {
	parent::beforeFilter();
	
	}
	
	public function add(){
		
		if($this->request->isPost()){
			$data = $this->request->data;
			$sql=array();
			$sql["name"] = $data["name"];
			$sql["link"] = $data["url_link"];	
			$sql["status"] = $data["status"];
			$sql["type"] = $data["type"];
			$sql["display_order"] = $data["display_order"];
			if(!empty($data["imgfile"]["name"]))
			{
				$file1 = $data["imgfile"];
				$exten = pathinfo($file1["name"], PATHINFO_EXTENSION);
				move_uploaded_file($file1["tmp_name"],UPLOADPATH . 'promo_imgs/' . "promotional_".time().".".$exten);
				move_uploaded_file($file1["tmp_name"],UPLOADPATH . 'promo_imgs/' . "promotional_".time().".".$exten);
				$promo_img = "promotional_".time().".".$exten;
				$sql['image']=$promo_img;
				$promo_img = UPLOADPATH . 'promo_imgs/'. $promo_img;
				//send image to server...
				$resConnection = ssh2_connect(FCIMAGESERVER,  FCIMAGESERVERPORT);
				if(ssh2_auth_password($resConnection, FCIMAGESERVERUSERNAME, FCIMAGESERVERPASSWORD)){
					$resSFTP = ssh2_sftp($resConnection);
					if(isset($promo_img) && !empty($promo_img) && file_exists($promo_img)){//cover image code on new server start here...
						$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$sql['image'], 'w');
						$srcFile = fopen($promo_img, 'r');
						$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
						fclose($resFile);
						fclose($srcFile);
						unlink($coverImg);
					}
					ssh2_exec($resConnection, 'exit');
					unset($resConnection);
				}
				
			}
			$this->Promotion->save($sql);
			$this->Session->setFlash('Promotional banner added successfully.');
			$this->redirect('view');
		} 	
		
	}

	public function view($page=0){
		
		$page=1;
		$recode_per_page=10;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$page = $this->request->query['page'];
		}
		 $this_page_rec=($page-1)*$recode_per_page;
		
		$orderBy = "order by id asc";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = "order by id ".$_GET["sortBy"]."" ;			
		}
		
		$searchCond = array();
		
		if(isset($_GET['searchBy']) && !empty($_GET['searchBy'])){
			 
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "name")){
				
				$searchCond[]='and name like "%'.$_GET["searchString"].'%"' ;
			}
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "u_id")){
				$searchCond[]='and id="'.$_GET["searchString"].'"' ;
			}
		}
		$searchCondStr = @implode(" ",$searchCond);	
		$filter = "1";
		
		if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
			$filter = "status=".$_GET["filterBy"]."";			
		}
		
		$total_row=$this->Promotion->query("select count(id)as recode from cm_promotional_content");
		
		$userdata=$this->Promotion->query("select *from cm_promotional_content where $filter $searchCondStr $orderBy limit $this_page_rec,$recode_per_page");
		
		$total_num=@$total_row[0][0]["recode"];
		
		
		//$num_of_pages=ceil($total_num/$recode_per_page)
		$this->set("total_num",$total_num);
		$this->set("userdata",$userdata);
	} 
	
	
	public function edit($id=null){
		$proData = $this->Promotion->find('first',array('conditions'=>array('Promotion.id'=>$id)));
		$this->set('proData', $proData);
		
		if($this->request->isPost())
		{ 
			$data = $this->request->data;
			$sql = array();
			$sql["id"] = $data["id"];
			$sql["name"] = $data["name"];
			$sql["link"] = $data["url_link"];	
			$sql["status"] = $data["status"];
			$sql["type"] = $data["type"];
			$sql["display_order"] = $data["display_order"];
			
			if(!empty($data["imgfile"]["name"]))
			{
				$file1 = $data["imgfile"];
				$exten = pathinfo($file1["name"], PATHINFO_EXTENSION);
				move_uploaded_file($file1["tmp_name"],UPLOADPATH . 'promo_imgs/' . "promotional_".time().".".$exten);
				move_uploaded_file($file1["tmp_name"],UPLOADPATH . 'promo_imgs/' . "promotional_".time().".".$exten);
				$promo_img = "promotional_".time().".".$exten;
				$sql['image']=$promo_img;
				$promo_img = UPLOADPATH . 'promo_imgs/'. $promo_img;
				//send image to server...
				$resConnection = ssh2_connect(FCIMAGESERVER,  FCIMAGESERVERPORT);
				if(ssh2_auth_password($resConnection, FCIMAGESERVERUSERNAME, FCIMAGESERVERPASSWORD)){
					$resSFTP = ssh2_sftp($resConnection);
					if(isset($promo_img) && !empty($promo_img) && file_exists($promo_img)){//cover image code on new server start here...
						$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$sql['image'], 'w');
						$srcFile = fopen($promo_img, 'r');
						$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
						fclose($resFile);
						fclose($srcFile);
						unlink($coverImg);
					}
					ssh2_exec($resConnection, 'exit');
					unset($resConnection);
				}
				
			}
			$sql["modified"] = CURRDATE;
			$this->Promotion->save($sql);
			$this->Session->setFlash('Promotional banner updated successfully.');
			$this->redirect('view');

		}
		
		
	}	
	
	
}
?>
