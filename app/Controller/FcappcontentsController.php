<?php
App::uses('HttpSocket', 'Network/Http');
class FcappcontentsController extends AppController
{
	public $name = 'Fcappcontents';
	public $uses = array('Appnamelist','Fcappcontent','FcappBulkupload','FcappContentPrice','Fcappcategories','Channel','Country', 'User', 'Language', 'ContentNotification','Fccgimage');
	public $components = array('Paginator');
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function viewcontent(){
		
		
	
		//session_start();
		//$_SESSION['page_ref_url']=$_SERVER['HTTP_REFERER'];
		//echo $_SESSION['page_ref_url']; die;
		
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
	    $country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country); 	
		$data=$this->request->query;
		
		if($this->request->is('post'))
		{
			$value=$this->data;
			$user_id=$this->Session->read('User.user_id');
			if(isset($value['statusn']))
			    {
					
					if($value['statusn']=="2")
					{   
					    foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
					    $this->Session->setFlash('Content Updated Successfully.');	
						//$this->redirect('viewcontent');
						$this->redirect($_SERVER['HTTP_REFERER']);
					}
					if($value['statusn']=="1")
					{
						foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						//$this->redirect('viewcontent');
						$this->redirect($_SERVER['HTTP_REFERER']);
					}
					if($value['statusn']=="T")
					{
						foreach($value['check'] as $checkone)
					    {
							$modified = date('YmdHis');
						    $this->Fcappcontent->query("UPDATE cm_contents SET trend = 'T',modified = '".$modified."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						//$this->redirect('viewcontent');
						$this->redirect($_SERVER['HTTP_REFERER']);
					}
					if($value['statusn']=="0")
					   {
						   foreach($value['check'] as $checkone)
					       {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 0 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						//$this->redirect('viewcontent');
						$this->redirect($_SERVER['HTTP_REFERER']);
					}
					if($value['statusn']=="A")
					{
						foreach($value['check'] as $checkone)
					    {
						 $this->Fcappcontent->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');
						//$this->redirect('viewcontent');						
						$this->redirect($_SERVER['HTTP_REFERER']);						
					}
					if($value['statusn']=="D")
					{
						foreach($value['check'] as $checkone)
						{
							$this->Fcappcontent->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');		
						//$this->redirect('viewcontent');							
						$this->redirect($_SERVER['HTTP_REFERER']);							
					}
				
				}
			}
			
			
			if(isset($data['status']) && $data['status'] !="")
			{
				
				if($data['status']=='A')
				{
					$status= "t1.status='".$data['status']."'";
				}
				else if($data['status']=='D')
				{
					$status="t1.status='".$data['status']."'";
				}
				else if($data['status']=='1')
				{
					$status="t1.approved='".$data['status']."'";
				}
				else if($data['status']=='2')
				{
					$status="t1.approved='".$data['status']."'"; 
				} 
				else if($data['status']=='0')
				{
					
					$status="t1.approved='".$data['status']."'"; 
				}
				
			} else {
					$status = "(t1.approved='2' OR t1.approved='1' OR t1.approved='0' OR t1.status = 'A' OR t1.status = 'D')";
			}
			$strCond = "";
			$strCond .= $status;
			//echo $data['fcut_content_type'];die;
			
 			if(!empty($data['name']))
			{
				$strCond .= " and t1.NAME like '%".$data['name']."%'";
			} 
			else if(!empty($data['country_code']))
			{
				$strCond .= "and t1.country_code='".$data['country_code']."'";
			}
			
			else if(!empty($data['subcategory_id']))
			{	
				$strCond .= " and t1.category_id='".$data['subcategory_id']."'";
			}
			else if(!empty($data['channel_id']))
			{	
				$strCond .=" and t1.channel_id='".$data['channel_id']."'";
			}
			
			if($data['fcut_content_type'] == 'utube')
			{
				$strCond .= " and t1.fcut_content_type = '".$data['fcut_content_type']."' ";
			}else if($data['fcut_content_type'] == 'inhouse'){
				$strCond .= " and t1.fcut_content_type = '".$data['fcut_content_type']."' and t1.video_id IS NOT NULL";
			} 
			
			$channelCond = "";
			if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
			{
				
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				
				
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond[]="FIND_IN_SET (t1.user_id, '$finalUserArr')";
				
				$channelCond = "and FIND_IN_SET (user_id, '$finalUserArr')"; 
			}
			
			$channelList = $this->Channel->query("select id, c_name from ch_channel where status < '3' $channelCond");
				$this->set('channelList', $channelList);
			//data by filter...
			
			if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
				$searchCond[] = " t1.status = '".$_GET["filterBy"]."'" ;
			}
			
			if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
				$orderBy = " order by t1.content_id ".$_GET["sortBy"]."" ;
			}  else {
				$orderBy = " order by t1.content_id desc" ;
			}
			
			$searchCond[]="t1.approved!= 3";
			$searchCondStr=implode(" AND ",$searchCond);
			//echo "SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage";
			//die;
			$value = $this->Fcappcontent->query("SELECT t1.content_id,t1.fcut_content_type,t1.video_id,t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type,t1.premium_type, t1.view, t1.user_id FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage");
			
			//echo "SELECT t1.content_id,t1.fcut_content_type,t1.video_id,t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type,t1.premium_type, t1.view, t1.user_id FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage";die;
			
			$downloadsRow = $this->Fcappcontent->query("SELECT count(t1.content_id) as countRec FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where  $strCond and $searchCondStr  and t2.project='firstcut'");
			
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
	}
	
	public function approvedcontent(){
		
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
	    $country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country); 	
		$data=$this->request->query;
		if($this->request->is('post'))
		{
			$value=$this->data;
			$user_id=$this->Session->read('User.user_id');
			if(isset($value['statusn']))
			    {
					if($value['statusn']=="2")
					{
					    foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
					    $this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="1")
					{
						foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="T")
					{
						foreach($value['check'] as $checkone)
					    {
							$modified = date('YmdHis');
						    $this->Fcappcontent->query("UPDATE cm_contents SET trend = 'T',modified = '".$modified."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="0")
					   {
						   foreach($value['check'] as $checkone)
					       {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 0 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="A")
					{
						foreach($value['check'] as $checkone)
					    {
						 $this->Fcappcontent->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');
						$this->redirect('approvedcontent');						
					}
					if($value['statusn']=="D")
					{
						foreach($value['check'] as $checkone)
						{
							$this->Fcappcontent->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');		
						$this->redirect('approvedcontent');							
					}
				
				}
			}
			
			
			if(!empty($data['status']))
			{
				if($data['status']=='A')
				{
					$status= "t1.status='".$data['status']."'";
				}
				else if($data['status']=='D')
				{
					$status="t1.status='".$data['status']."'";
				}
				else if($data['status']=='1')
				{
					$status="t1.approved='".$data['status']."'";
				}
				else if($data['status']=='2')
				{
					$status="t1.approved='".$data['status']."'"; 
				}
			}
			  
			if(!empty($data['country_code']) && !empty($data['status']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and   $status and   t1.NAME like '%".$data['name']."%'";
			} 
			else if(!empty($data['country_code']) && !empty($data['status']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  $status ";
			}
			else if(!empty($data['status']) && !empty($data['name']))
			{
				$strCond = " $status and  t1.NAME like '%".$data['name']."%'";	
			}
			else if(!empty($data['country_code']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['name']))
			{
				$strCond = "t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['status']))
			{
				$strCond = "$status";
			}
			else if(!empty($data['country_code']))
			{	
				$strCond="t1.country_code='".$data['country_code']."'";
			}
			else if(!empty($data['subcategory_id']))
			{	
				$strCond="t1.category_id='".$data['subcategory_id']."'";
			}
			else if(!empty($data['channel_id']))
			{	
				$strCond="t1.channel_id='".$data['channel_id']."'";
			}
			else
			{
				$strCond="(t1.status = 'A' OR t1.status = 'D') ";
			}
			$channelCond = "";
			if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
			{
				
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond[]="FIND_IN_SET (t1.user_id, '$finalUserArr')";
				
				$channelCond = "and FIND_IN_SET (user_id, '$finalUserArr')"; 
			}
			
			$channelList = $this->Channel->query("select id, c_name from ch_channel where status < '3' $channelCond");
				$this->set('channelList', $channelList);
			//data by filter...
			
			if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
				$searchCond[] = " t1.status = '".$_GET["filterBy"]."'" ;
			}
			
			if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
				$orderBy = " order by t1.content_id ".$_GET["sortBy"]."" ;
			}  else {
				$orderBy = " order by t1.content_id desc" ;
			}
			
			$searchCond[]="t1.approved= 1";
			$searchCondStr=implode(" AND ",$searchCond);
			
			$value = $this->Fcappcontent->query("SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage");
			
			$downloadsRow = $this->Fcappcontent->query("SELECT count(t1.content_id) as countRec FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where  $strCond and $searchCondStr  and t2.project='firstcut'");
			
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
	}
	
	public function rejectedcontent(){
		
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
	    $country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country); 	
		$data=$this->request->query;
		if($this->request->is('post'))
		{
			$value=$this->data;
			$user_id=$this->Session->read('User.user_id');
			if(isset($value['statusn']))
			    {
					if($value['statusn']=="2")
					{
					    foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
					    $this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="1")
					{
						foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="T")
					{
						foreach($value['check'] as $checkone)
					    {
							$modified = date('YmdHis');
						    $this->Fcappcontent->query("UPDATE cm_contents SET trend = 'T',modified = '".$modified."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="0")
					   {
						   foreach($value['check'] as $checkone)
					       {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 0 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('approvedcontent');
					}
					if($value['statusn']=="A")
					{
						foreach($value['check'] as $checkone)
					    {
						 $this->Fcappcontent->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');
						$this->redirect('approvedcontent');						
					}
					if($value['statusn']=="D")
					{
						foreach($value['check'] as $checkone)
						{
							$this->Fcappcontent->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');		
						$this->redirect('approvedcontent');							
					}
				
				}
			}
			
			
			if(!empty($data['status']))
			{
				if($data['status']=='A')
				{
					$status= "t1.status='".$data['status']."'";
				}
				else if($data['status']=='D')
				{
					$status="t1.status='".$data['status']."'";
				}
				else if($data['status']=='1')
				{
					$status="t1.approved='".$data['status']."'";
				}
				else if($data['status']=='2')
				{
					$status="t1.approved='".$data['status']."'"; 
				}
			}
			  
			if(!empty($data['country_code']) && !empty($data['status']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and   $status and   t1.NAME like '%".$data['name']."%'";
			} 
			else if(!empty($data['country_code']) && !empty($data['status']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  $status ";
			}
			else if(!empty($data['status']) && !empty($data['name']))
			{
				$strCond = " $status and  t1.NAME like '%".$data['name']."%'";	
			}
			else if(!empty($data['country_code']) && !empty($data['name']))
			{
				$strCond = "t1.country_code='".$data['country_code']."' and  t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['name']))
			{
				$strCond = "t1.NAME like '%".$data['name']."%'";
			}
			else if(!empty($data['status']))
			{
				$strCond = "$status";
			}
			else if(!empty($data['country_code']))
			{	
				$strCond="t1.country_code='".$data['country_code']."'";
			}
			else if(!empty($data['subcategory_id']))
			{	
				$strCond="t1.category_id='".$data['subcategory_id']."'";
			}
			else if(!empty($data['channel_id']))
			{	
				$strCond="t1.channel_id='".$data['channel_id']."'";
			}
			else
			{
				$strCond="(t1.status = 'A' OR t1.status = 'D') ";
			}
			$channelCond = "";
			if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
			{
				
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond[]="FIND_IN_SET (t1.user_id, '$finalUserArr')";
				
				$channelCond = "and FIND_IN_SET (user_id, '$finalUserArr')"; 
			}
			
			$channelList = $this->Channel->query("select id, c_name from ch_channel where status < '3' $channelCond");
				$this->set('channelList', $channelList);
			//data by filter...
			
			if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
				$searchCond[] = " t1.status = '".$_GET["filterBy"]."'" ;
			}
			
			if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
				$orderBy = " order by t1.content_id ".$_GET["sortBy"]."" ;
			}  else {
				$orderBy = " order by t1.content_id desc" ;
			}
			
			$searchCond[]="t1.approved= 2";
			$searchCondStr=implode(" AND ",$searchCond);
			
			$value = $this->Fcappcontent->query("SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage");
			
			$downloadsRow = $this->Fcappcontent->query("SELECT count(t1.content_id) as countRec FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where  $strCond and $searchCondStr  and t2.project='firstcut'");
			
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
	}
	
	public function detailcontent($id=null){
		
		$this->Session->write('Content.contentRedirect',  $_SERVER['HTTP_REFERER']);
		
		$searchCondStr = " t1.content_id='".$id."' and t1.approved!= 3";
		$searchCond[] =" t1.content_id='".$id."'";
		$searchCond[] =" t1.approved!= 3";
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCond[] = " FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$searchCondStr=implode(" AND ",$searchCond);
		
		$contentRec = $this->Fcappcontent->query("SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view, t1.channel_id FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $searchCondStr and t2.project='firstcut'");
		if($contentRec <= 0){
			$this->Session->setFlash('You have no permission to view this content.');
			$this->redirect($_SERVER['HTTP_REFERER']);
		} else {
			$channel = $this->Channel->find('first', array('fields' => array('Channel.c_name'), 'conditions'=>array('Channel.id'=>$contentRec[0]["t1"]["channel_id"])));
			$this->set(compact('channel', $channel));
			$this->set(compact('contentRec', $contentRec));
		}
	}
	
	public function deletecontent($id=null){
		$searchCond[] = " content_id ='".$id."'";
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCond[] = " FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$searchCondStr=implode(" AND ",$searchCond);
		$contentCount = $this->Fcappcontent->query("select content_id from cm_contents where $searchCondStr");
		if($contentCount <= 0){
			$this->Session->setFlash('You have no permission to delete this content.');
			$this->redirect('viewcontent');
		} else {
			$this->Fcappcontent->query("update cm_contents set approved = '3' where content_id = '".$id."'");
			$this->Session->setFlash('Record deleted successfully.');
			$this->redirect('viewcontent');
		}
	}
	
	
	
	public function bulkupload(){
		
		$country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country);
		
		$sql = array();	
		if ($this->request->is('post')) 
		{	
			//$this->Session->write('Appname.slug', $this->data['app_slug']);
			$filename =$_FILES['csvfile']['name'];
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			$target_file = DOM_PUBLICHTML_PATH."bulkupload/".date('YmdHis')."_$filename";
		
			if($imageFileType=='csv')
			{
				if (move_uploaded_file($_FILES['csvfile']['tmp_name'],$target_file)) {
					$txtfile_n=explode(".",$target_file);
					$txtfile_name=$txtfile_n[0];
					$myfile = fopen($txtfile_name.'.txt', "w") or die("Unable to open file!");
				}


				$country_code=$this->data['country_code'];
                $subcategory=$this->data['subcategory_id']; 
				$channel_id=$this->data['channel_id']; 
				$file=$_FILES['csvfile']['tmp_name'];
				$handle = fopen($target_file,"r"); 
				$i=0;
				$data = fgetcsv($handle,1000,",","'");
				
				$c=count($data);
				if($c==16) 
				{
					$p_count=0;  
					$r_count=1;
					while($data = fgetcsv($handle,1000,",","'")) 
					{ 
					
					
						if ($data[0]) 
						{
							$content_name=$data[0];	
							$content_video=trim($data['1']); 
							$trialcontent=trim($data['2']);
							$content_poster= $poster=trim($data['3']);								
							$content_description= $data['4'];
                            $premium_type=$data['5'];							
							$content_price= $data['6'];
							$publisher=$data['7'];
							$language=$data['8'];
						
							$txt_msg = "\nContent ".$r_count." ($content_name): ";
							if($publisher=='mini')
							{
								$sql['director_name']=$data['9'];
								$sql['production_house']=$data['10'];
								$var = $data['11'];
								$date = str_replace('/', '-', $var);
								$date=date('Y-m-d', strtotime($date)); 
								$sql['release_date']=$date;
							}
							
							if(!empty($content_video))
							 {
								 $command = ("/usr/bin/ffmpeg  -i ".$content_video." -vstats 2>&1");  
     							 $output = shell_exec($command); 								 
								 
     							 $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    							if (preg_match($regex_sizes, $output, $regs)) {
        						  $codec = $regs [1] ? $regs [1] : null;
         						  $width = $regs [3] ? $regs [3] : null; 
         						  $height = $regs [4] ? $regs [4] : null;
								  
								}
	 							 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 /*echo 'Hour: ' . $explode[0];
								 echo 'Minute: ' . $explode[1];
								 echo 'Seconds: ' . $explode[2];*/
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration);
								 $sql['duration']=$duration;
							 }


                            if(!empty($trialcontent))
							 {
								 $command = ("/usr/bin/ffmpeg  -i ".$trialcontent." -vstats 2>&1");  
     							 $output = shell_exec($command); 								 
								 
     							 $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    							if (preg_match($regex_sizes, $output, $regs)) {
        						  $codec = $regs [1] ? $regs [1] : null;
         						  $width = $regs [3] ? $regs [3] : null; 
         						  $height = $regs [4] ? $regs [4] : null;
								  
							 }
	 							 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 /*echo 'Hour: ' . $explode[0];
								 echo 'Minute: ' . $explode[1];
								 echo 'Seconds: ' . $explode[2];*/
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration);
								 $sql['duration']=$duration;
							 }
							
							if($part==1)
							{
								$part=1;
								unset($_SESSION['resolution']);
							}
							else
							{
								$last_insert_id=$this->Session->read('last_id');
								$part=$last_insert_id."-".$p_count;
							}
							 /*if($width>=720  and !isset($_SESSION['resolution']))
							 {*/
//echo '<pre>'; print_r($data); die;
							$txt_msg = "\nContent ".$r_count." ($content_name): ";
							$txt="";
							//fwrite($myfile, $txt_msg);
							if(empty($content_name)){
								$txt .="Name is empty, ";
							}
							if(empty($content_video)) {	
								$txt .="Video is not exist, ";
							}

							/*if(empty($trialcontent))
							{

                              $txt .=" Trial video is not exist, ";

							}*/

							if(empty($content_poster)){
								$txt .="Poster is empty, ";
							}
							else if(!file_exists($content_poster)){	
								$txt .="Poster is not exist, ";
							}
							if(empty($content_description)){
								$txt .="Description is empty, ";
							}
							if(empty($content_price)){
								$txt .="Price is empty ,";
							}
							
							//echo 'content_poster: '.$content_poster.'y<br>'.$content_video.' content_video.:: ';echo $txt; die;
							if(empty($txt))
							{
								$sql['channel_id'] = $channel_id;
								$sql['name']=$content_name;
								$sql['price']=$content_price;
								$sql['description']=$content_description;
								$sql['category_id']=$subcategory;
								$sql['country_code']=$country_code;
								$sql['project']=$project;
								$sql['user_id']=$this->Session->read('User.id');
								$sql['ip']=$_SERVER['REMOTE_ADDR'];
								$sql['part']=$part;
								$sql['language_code']=$language;
								$poster=explode('/',$poster);
								//$poster=$poster[5];
								$poster=end($poster);
								$poster=explode('.',$poster);
								$pos=$poster['1'];
								$b = time().rand(11,99);
					        	$sql['poster']=$b.".png";	
								
								//$content_video1=DOM_PUBLICHTML_PATH."contents/".$m;
								//echo $content_poster;
								//echo "<br/>";
								//echo DOM_PUBLICHTML_PATH."contents/".$b.".$pos";
								//die;
								rename($content_poster, DOM_PUBLICHTML_PATH."contents/".$b.".$pos");
								$c_image1=$b."_200x200"; 
		   						$c_image2=$b."_400x200"; 
								//exec("/usr/bin/convert /home/cmsadmin/public_html/tmpcontents/".$b.".$pos  /home/cmsadmin/public_html/tmpcontents/".$b.".png 2>&1", $array); die;
							   
								
								$output=exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$pos  ".DOM_PUBLICHTML_PATH."contents/".$b.".png 2>&1", $array);
								exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$pos -resize 200x200  ".DOM_PUBLICHTML_PATH."contents/".$c_image1.".png 2>&1", $array);
		 						exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$pos -resize 400x200 ".DOM_PUBLICHTML_PATH."contents/".$c_image2.".png 2>&1", $array);
							
								if(!empty($content_video) && ($this->data['app_slug'] !='bhojpurinama'))
								{  
									$ex=explode("/",$content_video);
									//$output=$ex[5];
									$output=end($ex);
									$n=explode(".",$output);
									$f=$n[1]; 
									if($f=='mp4' or $f=='mp3' or $f=='3gp')
									{	
										
										$m=$b.".mp4";
										$m1=$b."_480.mp4";
										$m2=$b."_360.mp4";
										//$m3=$b."_240.mp4";
										$m3=$b."_176.mp4";
										
										$mn1=$b."new_480.mp4";
										$mn2=$b."new_360.mp4";
										//$mn3=$b."new_240.mp4";
										$mn3=$b."new_176.mp4";
																			
					//$output= shell_exec('sh /home/cmsadmin/app/convert1.sh "'.$content_video.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'"'); 
						
					CakeLog::info('nohup sh '.PUBLICHTML_SH_PATH.'testdv.sh "'.$content_video.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'" 1>/dev/null 2>/dev/null &','debug');

					CakeLog::info('nohup sh '.PUBLICHTML_SH_PATH.'sizetest.sh "'.$content_video.'" "'.$m.'" "'.$mn1.'" "'.$mn2.'" "'.$mn3.'" "'.$height.'" 1>/dev/null 2>/dev/null &','debug');

					$output= shell_exec('nohup sh '.PUBLICHTML_SH_PATH.'testdv.sh "'.$content_video.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'" 1>/dev/null 2>/dev/null &'); 

					//$output_new= shell_exec('nohup sh /home/cmsadmin/app/sizetest.sh "'.$content_video.'" "'.$m.'" "'.$mn1.'" "'.$mn2.'" "'.$mn3.'" "'.$height.'" 1>/dev/null 2>/dev/null &'); 

					/*  $output=exec("/usr/bin/ffmpeg -i ".$content_video." -b 255k ".DOM_PUBLICHTML_PATH."tmpcontents/".$m."");   
					$output1=exec("/usr/bin/ffmpeg -i ".$content_video." -b 255k -s 720x480 ".DOM_PUBLICHTML_PATH."tmpcontents/".$m1." 2>&1", $o, $v);										
										
                    $output2= exec("/usr/bin/ffmpeg -i ".$content_video." -b 150k -s 320x240 -r 15 -ac 1 -ar 8000 -ab 24k ".DOM_PUBLICHTML_PATH."tmpcontents/".$m2."  2>&1", $o, $v);*/
					//rename($content_video,DOM_PUBLICHTML_PATH."contents/".$m);	
										$sql['content']=$m;		
									}else{
										$this->Session->setFlash('Content must be of video type');
										$this->redirect('bulkupload');  
									}
								} else {
									$sql['content'] = $content_video;
								}
                               

                             // trial vodeo upload
                               
                               if(!empty($trialcontent))
								{  
									$ex1=explode("/",$trialcontent);
									//$output=$ex[5];
									$output1=end($ex1);
									$n1=explode(".",$output1);
									$f1=$n1[1]; 
									if($f1=='mp4' or $f1=='mp3' or $f1=='3gp')
									{	
										
										$m=$b.".mp4";
										$m1=$b."_480.mp4";
										$m2=$b."_360.mp4";
										//$m3=$b."_240.mp4";
										$m3=$b."_176.mp4";
																			
					//$output= shell_exec('sh /home/cmsadmin/app/convert1.sh "'.$content_video.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'"'); 
							
					      $output= shell_exec('nohup sh '.PUBLICHTML_SH_PATH.'testdv.sh "'.$trialcontent.'" "'.$m.'" "'.$m1.'" "'.$m2.'" "'.$m3.'" "'.$height.'" 1>/dev/null 2>/dev/null &'); 
									 /*  $output=exec("/usr/bin/ffmpeg -i ".$content_video." -b 255k ".DOM_PUBLICHTML_PATH."tmpcontents/".$m."");   
										$output1=exec("/usr/bin/ffmpeg -i ".$content_video." -b 255k -s 720x480 ".DOM_PUBLICHTML_PATH."tmpcontents/".$m1." 2>&1", $o, $v);										
										
                                       $output2= exec("/usr/bin/ffmpeg -i ".$content_video." -b 150k -s 320x240 -r 15 -ac 1 -ar 8000 -ab 24k ".DOM_PUBLICHTML_PATH."tmpcontents/".$m2."  2>&1", $o, $v);*/
 
										//rename($content_video,DOM_PUBLICHTML_PATH."contents/".$m);	
										$sql['trial_content']=$m;	

									  }else{

										$this->Session->setFlash('content must be of video type');
										$this->redirect('bulkupload');  
									}
								} else {
									$sql['trial_content'] = $trialcontent;
								}
								$sql['video_conversion'] = 1;
							   $txt="Sucessfully inserted \n";
									   						
								$this->Fcappcontent->create();			
								$this->Fcappcontent->save($sql);
								$last_id=$this->Fcappcontent->getLastInsertID();
								if($last_id)
								{
								    $arrTata = array();
									if(empty($tata_price)) $tata_price=0;
									$arrTata['content_id']=$last_id;
									$arrTata['billing_partner']="tt";
									$arrTata['price']=$tata_price;    		
														
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrTata);
							         
									$arrmtnl = array();
									if(empty($mtnl_price)) $mtnl_price=0;
									$arrmtnl['content_id']=$last_id;
									$arrmtnl['billing_partner']="mt";
									$arrmtnl['price']=$mtnl_price;    
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrmtnl);						
							        $arrvf = array();
									if(empty($vodafone_price)) $vodafone_price=0;
									$arrvf['content_id']=$last_id;
									$arrvf['billing_partner']="vf";
									$arrvf['price']=$vodafone_price;    
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrvf);						
							        
									$arrAircell = array();
									if(empty($aircel_price)) $aircel_price=0;
									$arrAircell['content_id']=$last_id;
									$arrAircell['billing_partner']="ac";
									$arrAircell['price']=$aircel_price;    
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrAircell);						
								}							
							} else {
								$this->Session->setFlash("".$txt."");
								$this->redirect('bulkupload');	
							}
							 
						} 
						if($part==1)
						 {
							 $_SESSION['last_id']=$last_id;
							 $p_count=0;   
						 } 
						$msg=$txt_msg.$txt;
						fwrite($myfile, $msg);
						$r_count ++;
						++$p_count; 						
					}
					$id=$this->Session->read('User.user_id');
					$target_file1=explode('/',$target_file);
					$csvfile=$target_file1[5];				
					
					$txtfile_name1 =explode('/',$txtfile_name);
					$txtfile=$txtfile_name1[5];				
					
					$this->FcappBulkupload->query("INSERT into cm_bulk_uploads set csv_file='".$csvfile."',error_file='".$txtfile.'.txt'."',user_id='".$id."'"); 
				  	$this->Session->setFlash('CSV uploaded successfully.');
					$this->redirect('bulkupload');	
					fclose($myfile);
					
				} else {
					$this->Session->setFlash('Mismatch columns: must be 12 columns.');
					$this->redirect('bulkupload'); 				  
				}
			} else {
				$this->Session->setFlash('Unacceptable file type: upload only csv');
				$this->redirect('bulkupload'); 	  
			}
		}
		
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCondUser="FIND_IN_SET (user_id, '$finalUserArr')";
		
		} else {
			$searchCondUser="1=1";
		}
		$channelList = $this->Channel->query("select id, c_name from ch_channel where $searchCondUser");
		
		$this->set('channelList', $channelList);
		
	}
	
	public function editcontent($content_id=null){
		
		if($this->request->is('post')) 
		{	
			$value = $this->data;
			//echo"<pre>";
			//print_r($value);
			//die;
			
			$created = date('Y-m-d H:i:s');
			
			// Notification entry in table 
			if(($value['old_status']!= $value['status']) && ($value['old_approved']!= $value['approved']))
			{	
				$this->Fcappcontent->query("insert into cm_content_notification  set user_id ='".$this->Session->read('User.id')."',c_id='".$content_id."',c_name ='".$value['name']."',c_type = '1',approved='".$value['approved']."',status='".$value['status']."',created='".$created."',readtime='".$created."',approved_by='".$this->Session->read('User.id')."'");
			}
			else if($value['old_status']!= $value['status'])
			{
				
				$this->Fcappcontent->query("insert into cm_content_notification  set user_id ='".$this->Session->read('User.id')."',c_id='".$content_id."',c_name ='".$value['name']."',c_type = '1',approved='".$value['approved']."',status='".$value['status']."',created='".$created."',readtime='".$created."',approved_by='".$this->Session->read('User.id')."'");
			}
			else if($value['old_approved']!= $value['approved'])
			{
				
				$this->Fcappcontent->query("insert into cm_content_notification  set user_id ='".$this->Session->read('User.id')."',c_id='".$content_id."',c_name ='".$value['name']."',c_type = '1',approved='".$value['approved']."',status='".$value['status']."',created='".$created."',readtime='".$created."',approved_by='".$this->Session->read('User.id')."'");
			}
			else
			{
				
			}
			
			//-------- tata price ---------
			if($value['tata_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['tataprice']."' where  content_price_id='".$value['tata_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='tt', content_id='".$value['content_id']."', price='".$value['tataprice']."' ");
			}
			
			//-------- mtnl price ---------			
			if($value['mtnl_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['mtnlprice']."' where  content_price_id='".$value['mtnl_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='mt', content_id='".$value['content_id']."', price='".$value['mtnlprice']."' ");
			}
				
				
				
			//-------- vodafone price ---------		
			if($value['vodafone_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['vodafoneprice']."' where  content_price_id='".$value['vodafone_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='vf', content_id='".$value['content_id']."', price='".$value['vodafoneprice']."' ");
			}
			
			//-------- aircel price ---------	
			if($value['aircel_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['aircelprice']."' where  content_price_id='".$value['aircel_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='ac', content_id='".$value['content_id']."', price='".$value['aircelprice']."' ");
			}
			
			if(!empty($this->data['content']['name']))
			{
				
			        if(is_uploaded_file($_FILES['data']['tmp_name']['content']))
					{
						$sourcePath1 = $_FILES['data']['tmp_name']['content'];
						$content = $_FILES['data']['name']['content'];
						$b = time().rand(11,99);
						$imageFileType = pathinfo($content,PATHINFO_EXTENSION);
						$newfilename= $b.'.'.$imageFileType; 
						$targetPath = DOM_PUBLICHTML_PATH."contents/".$newfilename;
						if(move_uploaded_file($sourcePath1,$targetPath))
						{
							//$sql['content']= $newfilename;
							$contentimage = $newfilename;
						}
					}
					$contentimage1 = DOM_PUBLICHTML_PATH."contents/".$contentimage;
					if(file_exists($contentimage1))
				    {
					$files_arr = explode("\n", $contentimage1);
				    $s3 = new S3(ACCESS_KEY,SECRET_KEY);
					foreach($files_arr as $file1)
				    {
						if(!empty($file1))
						{
							$bucketname = "firstcut";
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
						    $main_arr = array_splice($file1_arr, 5);
						    $bucket_file = implode("/",$main_arr);
							$image_type = "video/mp4";
							$header = array('Content-Type' =>$image_type);
                            $cache = array('CacheControl' =>'public, max-age=31536000');

$command = ("/usr/bin/ffmpeg  -i ".$file1." -vstats 2>&1");  
     					    $output = shell_exec($command); 
							$regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
							
							if (preg_match($regex_sizes, $output, $regs)) {
        						  $codec = $regs [1] ? $regs [1] : null;
         						  $width = $regs [3] ? $regs [3] : null; 
         						  $height = $regs [4] ? $regs [4] : null;
								  
								}
	 							 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration);							
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
						
						if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header))
						{
							//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
						}
							
						}
					}
				}
			}
			else
			{
				$contentimage = $value['old_content'];
			}
			if(!empty($this->data['trialcontent']['name']))
			{
				
			       if(is_uploaded_file($_FILES['data']['tmp_name']['trialcontent']))
					{
						$sourcePath2 = $_FILES['data']['tmp_name']['trialcontent'];
						$trialcontent = $_FILES['data']['name']['trialcontent'];
						$imageFileType = pathinfo($trialcontent,PATHINFO_EXTENSION);
						$b = time().rand(11,99);
						$newfilename= $b.'.'.$imageFileType;
						$targetPath2 = DOM_PUBLICHTML_PATH."contents/".$newfilename;

						if(move_uploaded_file($sourcePath2,$targetPath2))
						{
							
							//$sql['trial_content']= $newfilename;
							$trialcont = $newfilename;
						}
						
					}
					$trialcont1 = DOM_PUBLICHTML_PATH."contents/".$trialcont;
				if(file_exists($trialcont1))
				{
					$files_arr = explode("\n", $trialcont1);
				    $s3 = new S3(ACCESS_KEY,SECRET_KEY);
					foreach($files_arr as $file1)
				    {
						if(!empty($file1))
						{
							$bucketname = "firstcut";
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
						    $main_arr = array_splice($file1_arr, 5);
						    $bucket_file = implode("/",$main_arr);
							$image_type = "video/mp4";
							$header = array('Content-Type' =>$image_type);	
							$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
						
						//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
						if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header))
						{
							//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
						}
							
						}
					}
				}
			}
			else
			{
				$trialcont = $value['old_trialcontent'];
			}
			
			$filename=DOM_PUBLICHTML_PATH."contents/".$value['poster']['name']."";
			
			if(!empty($this->data['poster']['name']))
			{
				
				$pos=$this->Fcappcontent->query("select poster from cm_contents as Content where content_id='".$content_id."'");
				$pos=$pos['Content']['poster'];
				$image1=explode(".",$pos);
				$c_image=$image1['0'];
				$pos1=$c_image.".jpg"; 
				$pos2=$c_image."_200x200.png";
				$pos3=$c_image."_400x200.png";
				$file_exists=file_exists(DOM_PUBLICHTML_PATH.'contents/'.$pos.'');
				$file_exists1=file_exists(DOM_PUBLICHTML_PATH.'contents/'.$pos1.''); 
				$file_exists2=file_exists(DOM_PUBLICHTML_PATH.'contents/'.$pos2.''); 
				$file_exists3=file_exists(DOM_PUBLICHTML_PATH.'contents/'.$pos3.'');  
				if($file_exists==1){@unlink(DOM_PUBLICHTML_PATH.'contents/'.$pos.''); }
				if($file_exists1==1){@unlink(DOM_PUBLICHTML_PATH.'contents/'.$pos1.''); }
				if($file_exists2==1){@unlink(DOM_PUBLICHTML_PATH.'contents/'.$pos2.''); }
				if($file_exists3==1){@unlink(DOM_PUBLICHTML_PATH.'contents/'.$pos3.''); }
				$filename = DOM_PUBLICHTML_PATH."contents/".$this->data['poster']['name'];
				//echo $filename;die;
				$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
				if(strtolower($imageFileType) == "jpg" or strtolower($imageFileType) == "png" or strtolower($imageFileType) == "jpeg" or strtolower($imageFileType) == "gif") 
				{
				
					$b = time().rand(11,99);
					$newfilename=$b.'.'.$imageFileType;
					$dest=DOM_PUBLICHTML_PATH.'contents/';
					//$newfilename=time().'_POSTER.'.$imageFileType;
					if (move_uploaded_file($this->data['poster']['tmp_name'],$dest.$newfilename))
					{
					
						$c_image1=$b."_200x200"; 
						$c_image2=$b."_400x200"; 
						//exec("/usr/bin/convert /home/cmsadmin/public_html/tmpcontents/".$b.".$pos  /home/cmsadmin/public_html/tmpcontents/".$b.".png 2>&1", $array); die;
					
						
						$output=exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$imageFileType  ".DOM_PUBLICHTML_PATH."contents/".$b.".png 2>&1", $array);
						exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$imageFileType -resize 200x200  ".DOM_PUBLICHTML_PATH."contents/".$c_image1.".png 2>&1", $array);
						exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$imageFileType -resize 400x200 ".DOM_PUBLICHTML_PATH."contents/".$c_image2.".png 2>&1", $array);								
						$poster=$b.'.png';
						
						
						//$poster=$newfilename;
					} else {
						$this->Session->setFlash('There was a problem uploading file. Please try again.');
						$this->redirect('edit');
					}	
				}else {
					$this->Session->setFlash('Unacceptable file type for poster');
					$this->redirect('edit');
				}  
			}else if(isset($poster)){
				$poster=$poster;
				$posterimage1 = $dest.$poster;
				
				//connect to image server
				$resConnection = ssh2_connect(FCIMAGESERVER,  FCIMAGESERVERPORT);
				if(ssh2_auth_password($resConnection, FCIMAGESERVERUSERNAME, FCIMAGESERVERPASSWORD)){
					$resSFTP = ssh2_sftp($resConnection);
					if(isset($posterimage1) && !empty($posterimage1) && file_exists($posterimage1)){//cover image code on new server start here...
						$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$poster, 'w');
						$srcFile = fopen($posterimage1, 'r');
						$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
						fclose($resFile);
						fclose($srcFile);
						//unlink($coverImg);
					}
					
					ssh2_exec($resConnection, 'exit');
					unset($resConnection);
				}
			}else{
				$poster=$value['old_poster'];
			}
			if(!isset($value['approved'])) 
			{
				$value['approved'] = 0;
				
			}
			
			if($value['approved'] ==2)
			{
				$remarkVal = $value['remark'];
			} else
			{
				$remarkVal = '';
			}
			
			$modifiedDate = date('Y-m-d H:i:s');
			
			if(($this->Session->read('User.user_type') == "SEO"))
			{
				$seoFlag = '1';
			} else {
				$seoFlag = '0';
			}
			
			$approvedBy = "";
			if($value["approved"] &&  $value["approved1"] != $value["approved"]){
			   if($this->Session->read('User.user_type') == "V3MO" || $this->Session->read('User.user_type') == "V3MOA"){
				   $approvedBy = $this->Session->read('User.id');
			   } else {
				   $approvedBy = "";
			   }
			}
					
			$modifiedDate = date('Y-m-d H:i:s');
			$this->Fcappcontent->updateAll(
					array('name' =>"'".addslashes($value['name'])."'",'poster'=>"'".$poster."'",'price'=>"'".$value['price']."'",'category_id' =>"'".$value['subcategory_id']."'",'display_order' =>"'".$value['display_order']."'",'trial_content'=>"'".$trialcont."'",'content'=>"'".$contentimage."'",'tags'=>"'".addslashes($value['tags'])."'",
					'description' =>"'".addslashes($value['description'])."'",'language_code' =>"'".$value['language_code']."'",'content_type' =>"'".$value['content_type']."'",'status' =>"'".$value['status']."'",'director_name' =>"'".addslashes($value['director_name'])."'",'production_house' =>"'".addslashes($value['production_house'])."'",'release_date' =>"'".addslashes($value['release_date'])."'",'expiry_date' =>"'".addslashes($value['expiry_date'])."'",'convert_status' =>"'0'",'langFlag' =>"'0'",'vSizeFlag' =>"'0'",'vdFlag' =>"'0'",'approved' =>"'1'",'approved_by' =>"'".$approvedBy."'",'remark' =>"'".$remarkVal."'",'modified' =>"'".$modifiedDate."'",'duration' =>"'".$value['duration']."'",'channel_id' =>"'".$value['channel_id']."'",'meta_title' =>"'".addslashes($value['meta_title'])."'",'meta_key' =>"'".addslashes($value['meta_key'])."'",'meta_description' =>"'".addslashes($value['meta_description'])."'",'premium_type' =>"'".$value['premium_type']."'",'channel_id' =>"'".$value['channel_id']."'",'seoFlag' =>"'".$seoFlag."'"),
					array('content_id' =>$value['content_id'])
			);
		
			$redirectUrl = $this->Session->read('Content.contentRedirect');
			$this->Session->setFlash('Content edit successfully');
			
			if(1==1){
				$this->redirect($redirectUrl);
			}
			else{
				$this->redirect('viewcontent');
			}
			
		}
		
		$query="SELECT Content.*, Category.category_id, Subcategory.category_id, Subcategory.project, Subcategory.parent_id
				FROM cm_contents Content, cm_categories Category, cm_categories Subcategory 
				WHERE Content.category_id=Subcategory.category_id
				
				AND Content.content_id=$content_id ";
		$content = $this->Fcappcontent->query($query);	
		//echo "<pre>";print_r($content);die;
		$tata_price_arr = $this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='tt' and content_id='".$content_id."'");
		$mtnl_price_arr=$this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='mt' and content_id='".$content_id."'");
		$vodafone_price_arr=$this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='vf' and content_id='".$content_id."'");
		$aircel_price_arr=$this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='ac' and content_id='".$content_id."'");
		
		$content=$content[0];
		
		$parent_id=$content['Subcategory']['parent_id'];
		
		$subCatList = $this->Fcappcategories->query("select * from cm_categories where category_id='".$content['Content']['category_id']."'");
		$this->set('subCatList', $subCatList);	
	
		$this->set(compact('categories','sub_categories','category','content','langs','tata_price_arr','mtnl_price_arr','vodafone_price_arr','aircel_price_arr','content','parent_id'));
		
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCondUser="FIND_IN_SET (user_id, '$finalUserArr')";

		} 
		else 
		{
			$searchCondUser="1=1";
		}
		
		$channelList = $this->Channel->query("select id, c_name from ch_channel where $searchCondUser");
	
		$this->set('channelList', $channelList);
		
		$country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country);
		
		
	}
	
	public function direct_upload_to_bucket_video(){
		$access_key         = ACCESS_KEY; //Access Key
         $secret_key         = SECRET_KEY; //Secret Key
         $my_bucket          = FIRSTBUCKET; //bucket name
         $region             = "ap-southeast-1"; //bucket region
		 $success_redirect   = BASE_URL.'/fcappcontents/direct_upload_to_bucket_video'; //URL to which the client is redirected upon success (currently self) 
         $allowd_file_size   = "5368706371"; //5 GB allowed Size
		 $short_date 		= gmdate('Ymd'); //short date
		 $iso_date 			= gmdate("Ymd\THis\Z"); //iso format date
         $expiration_date 	= gmdate('Y-m-d\TG:i:s\Z', strtotime('+5 hours')); //policy expiration 5 hour from now
		 $policy = utf8_encode(json_encode(array(
					'expiration' => $expiration_date,  
					'conditions' => array(
						array('acl' => 'public-read'),  
						array('bucket' => $my_bucket), 
						array('success_action_redirect' => $success_redirect),
						array('starts-with', '$key', ''),
						array('content-length-range', '1', $allowd_file_size), 
						array('x-amz-credential' => $access_key.'/'.$short_date.'/'.$region.'/s3/aws4_request'),
						array('x-amz-algorithm' => 'AWS4-HMAC-SHA256'),
						array('X-amz-date' => $iso_date)
						)))); 
						 $kDate = hash_hmac('sha256', $short_date, 'AWS4' . $secret_key, true);
         $kRegion = hash_hmac('sha256', $region, $kDate, true);
         $kService = hash_hmac('sha256', "s3", $kRegion, true);
         $kSigning = hash_hmac('sha256', "aws4_request", $kService, true);
         $signature = hash_hmac('sha256', base64_encode($policy), $kSigning);
		 $this->set('access_key', $access_key);
		 $this->set('short_date', $short_date);
		 $this->set('region', $region);
		 $this->set('iso_date', $iso_date);
		 $this->set('policy', $policy);
		 $this->set('signature', $signature);
		 $this->set('success_redirect', $success_redirect);
		 if(isset($_GET["key"]))
		 {
			$this->Session->setFlash('File uploaded successfully.');	
			$this->redirect('direct_upload_to_bucket_video');
		 }
		
	}
	
	public function direct_upload_to_bucket()
	{
	     $access_key         = ACCESS_KEY; //Access Key
         $secret_key         = SECRET_KEY; //Secret Key
         $my_bucket          = FIRSTPOSTERBUCKET; //bucket name
         $region             = "ap-southeast-1"; //bucket region
         $success_redirect   = BASE_URL.'/fcappcontents/direct_upload_to_bucket'; //URL to which the client is redirected upon success (currently self) 
         $allowd_file_size   = "5368706371"; //5 GB allowed Size
		 //dates
         $short_date 		= gmdate('Ymd'); //short date
		 $iso_date 			= gmdate("Ymd\THis\Z"); //iso format date
         $expiration_date 	= gmdate('Y-m-d\TG:i:s\Z', strtotime('+5 hours')); //policy expiration 5 hour from now
		 $policy = utf8_encode(json_encode(array(
					'expiration' => $expiration_date,  
					'conditions' => array(
						array('acl' => 'public-read'),  
						array('bucket' => $my_bucket), 
						array('success_action_redirect' => $success_redirect),
						array('starts-with', '$key', ''),
						array('content-length-range', '1', $allowd_file_size), 
						array('x-amz-credential' => $access_key.'/'.$short_date.'/'.$region.'/s3/aws4_request'),
						array('x-amz-algorithm' => 'AWS4-HMAC-SHA256'),
						array('X-amz-date' => $iso_date)
						)))); 
						
		 $kDate = hash_hmac('sha256', $short_date, 'AWS4' . $secret_key, true);
         $kRegion = hash_hmac('sha256', $region, $kDate, true);
         $kService = hash_hmac('sha256', "s3", $kRegion, true);
         $kSigning = hash_hmac('sha256', "aws4_request", $kService, true);
         $signature = hash_hmac('sha256', base64_encode($policy), $kSigning);
		 $this->set('access_key', $access_key);
		 $this->set('short_date', $short_date);
		 $this->set('region', $region);
		 $this->set('iso_date', $iso_date);
		 $this->set('policy', $policy);
		 $this->set('signature', $signature);
		 $this->set('success_redirect', $success_redirect);
		
		 if(isset($_GET["key"]))
		 {
		 
		 	$this->Session->setFlash('file uploaded successfully.');	
			$this->redirect('direct_upload_to_bucket');
		 }
		 
	
	}
	
	public function bulk_upload_without_content(){
			
		$country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country);

		$sql = array();	
		if ($this->request->is('post')) 
		{	
			$filename =$_FILES['csvfile']['name'];
			$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
			$target_file=DOM_PUBLICHTML_PATH."bulkupload/".date('YmdHis')."_$filename";
		
			if($imageFileType=='csv')
			{
				if (move_uploaded_file($_FILES['csvfile']['tmp_name'],$target_file)) 
				{
					$txtfile_n=explode(".",$target_file);
					$txtfile_name=$txtfile_n[0];
					$myfile = fopen($txtfile_name.'.txt', "w") or die("Unable to open file!");
				}				
				
				$channel_id = $this->data['channel_id'];
				$country_code=$this->data['country_code'];
				$subcategory=$this->data['subcategory_id']; 
				$contenttype=$this->data['contenttype']; 
				$file=$_FILES['csvfile']['tmp_name'];
				$handle = fopen($target_file,"r"); 
				$i=0;
				$data = fgetcsv($handle,1000,",","'");
								
				$c=count($data);
				
				if($c==16) 
				{
				
				
					$p_count=0;  
					$r_count=1;
					while($data = fgetcsv($handle,1000,",","'")) 
					{ 
					
					
						if ($data[0]) 
						{
							$content_name=$data[0];	
							$content_video=trim($data['1']); 
							$trialcontent=trim($data['2']);
							$content_poster= $poster=trim($data['3']);					
							$content_description= $data['4'];					
							$content_price= $data['5'];
							$tata_price=$data['6'];
							$mtnl_price=$data['7'];
							$vodafone_price=$data['8'];
							$aircel_price=$data['9'];
							$part=$data['10'];
							$publisher=$data['11'];
							$language=$data['15'];
						
							$txt_msg = "\nContent ".$r_count." ($content_name): ";
							if($publisher=='mini')
							{
								$sql['director_name']=$data['12'];
								$sql['production_house']=$data['13'];
								$var = $data['14'];
								$date = str_replace('/', '-', $var);
								$date=date('Y-m-d', strtotime($date)); 
								$sql['release_date']=$date;
							}
							
							if(!empty($content_video))
							 {
								 $command = ("/usr/bin/ffmpeg  -i ".$content_video." -vstats 2>&1");  
     							 $output = shell_exec($command); 								 
								 
     							 $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    							if (preg_match($regex_sizes, $output, $regs)) {
        						  $codec = $regs [1] ? $regs [1] : null;
         						  $width = $regs [3] ? $regs [3] : null; 
         						  $height = $regs [4] ? $regs [4] : null;
								  
								}
	 							 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 /*echo 'Hour: ' . $explode[0];
								 echo 'Minute: ' . $explode[1];
								 echo 'Seconds: ' . $explode[2];*/
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration);
								 $sql['duration']=$duration;
							 }
							 
							if(!empty($trialcontent))
							 {
								 $command = ("/usr/bin/ffmpeg  -i ".$trialcontent." -vstats 2>&1");  
     							 $output = shell_exec($command); 								 
								 
     							 $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    							if (preg_match($regex_sizes, $output, $regs)) {
        						  $codec = $regs [1] ? $regs [1] : null;
         						  $width = $regs [3] ? $regs [3] : null; 
         						  $height = $regs [4] ? $regs [4] : null;
								  
							 }
	 							 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 /*echo 'Hour: ' . $explode[0];
								 echo 'Minute: ' . $explode[1];
								 echo 'Seconds: ' . $explode[2];*/
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration);
								 $sql['duration']=$duration;
							 }

                                                         
						if($part==1)
							{
								$part=1;
								unset($_SESSION['resolution']);
							}
							else
							{
								$last_insert_id=$this->Session->read('last_id');
								$part=$last_insert_id."-".$p_count;
							}
							 /*if($width>=720  and !isset($_SESSION['resolution']))
							 {*/
//echo '<pre>'; print_r($data); die;
							$txt_msg = "\nContent ".$r_count." ($content_name): ";
							$txt="";
							//fwrite($myfile, $txt_msg);
							if(empty($content_name)){
								$txt .="Name is empty, ";
							}
							/*if((!empty($content_video)) and (!file_exists($content_video))){	
								$txt .="Video is not exist, ";
							}*/

							if((!empty($trialcontent)) and (!file_exists($trialcontent)))
							{

                              $txt .=" Trial video is not exist, ";

							}
							if(empty($content_poster)){
								$txt .="Poster is empty, ";
							}
							else if(!file_exists($content_poster)){	
								$txt .="Poster is not exist, ";
							}
							if(empty($content_description)){
								$txt .="Description is empty, ";
							}
							if(empty($content_price)){
								$txt .="Price is empty ,";
							}
                            
							//echo 'content_poster: '.$content_poster.'y<br>'.$content_video.' content_video.:: ';echo $txt; die;
							if(empty($txt))
							{
								$sql['channel_id']=$channel_id;
								$sql['name']=$content_name;
								$sql['price']=$content_price;
								$sql['description']=$content_description;
								$sql['category_id']=$subcategory;
								$sql['content_type']=$contenttype;
								$sql['country_code']=$country_code;
								$sql['project']=$project;
								$sql['user_id']=$this->Session->read('User.id');
								$sql['ip']=$_SERVER['REMOTE_ADDR'];
								$sql['part']=$part;
								$sql['language_code']=$language;
								$poster=explode('/',$poster);
								//$poster=$poster[5];
								$poster=end($poster);
								$poster=explode('.',$poster);
								$pos=$poster['1'];
								$b = time().rand(11,99);
					        	$sql['poster']=$b.".png";	
								
								//$content_video1=DOM_PUBLICHTML_PATH."contents/".$m;
								rename($content_poster, DOM_PUBLICHTML_PATH."contents/".$b.".$pos");
								$c_image1=$b."_200x200"; 
		   						$c_image2=$b."_400x200"; 
								//exec("/usr/bin/convert /home/cmsadmin/public_html/tmpcontents/".$b.".$pos  /home/cmsadmin/public_html/tmpcontents/".$b.".png 2>&1", $array); die;
							   
								
								$output=exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$pos  ".DOM_PUBLICHTML_PATH."contents/".$b.".png 2>&1", $array);
								exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$pos -resize 200x200  ".DOM_PUBLICHTML_PATH."contents/".$c_image1.".png 2>&1", $array);
		 						exec("/usr/bin/convert ".DOM_PUBLICHTML_PATH."contents/".$b.".$pos -resize 400x200 ".DOM_PUBLICHTML_PATH."contents/".$c_image2.".png 2>&1", $array);
								//$content_poster1=DOM_PUBLICHTML_PATH."contents/".$b.".$pos";
								
								$sql['content'] = $content_video;

								$sql['trial_content']=$trialcontent;	
                               
													
								$txt="Sucessfully inserted \n";
								//fwrite($myfile, $txt_msg2);
								
	 
	 
						   /*$project1=$this->request->data['Content']['project'];
							$content1=$this->request->data['Content']['content'];
							  if($project1=='mini' || $project1=='mastram' || $project1=='miniwap' || $project1=='miniwappremium' || $project1=='watchearn')
							  {
					$this->Bulkupload->query("insert into catByData (project,content,ADDTIME) value('".$project1."','".$content1."','".date('Y-m-d H:i:s')."')");
							  }*/
              
	                          
								
								$this->Fcappcontent->create();			
								$this->Fcappcontent->save($sql);
								$last_id=$this->Fcappcontent->getLastInsertID();
								if($last_id)
								{
								   $arrTata = array();
									if(empty($tata_price)) $tata_price=0;
									$arrTata['content_id']=$last_id;
									$arrTata['billing_partner']="tt";
									$arrTata['price']=$tata_price;    
								
														
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrTata);
							         
									$arrmtnl = array();
									if(empty($mtnl_price)) $mtnl_price=0;
									$arrmtnl['content_id']=$last_id;
									$arrmtnl['billing_partner']="mt";
									$arrmtnl['price']=$mtnl_price;    
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrmtnl);						
							        $arrvf = array();
									if(empty($vodafone_price)) $vodafone_price=0;
									$arrvf['content_id']=$last_id;
									$arrvf['billing_partner']="vf";
									$arrvf['price']=$vodafone_price;    
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrvf);						
							        
									$arrAircell = array();
									if(empty($aircel_price)) $aircel_price=0;
									$arrAircell['content_id']=$last_id;
									$arrAircell['billing_partner']="ac";
									$arrAircell['price']=$aircel_price;    
									$this->FcappContentPrice->create();			
									$this->FcappContentPrice->save($arrAircell);						
								}							
							} else {
								$this->Session->setFlash("".$txt."");
								$this->redirect('bulk_upload_without_content');	
							}
							 /*}else{
								  	$txt='not inserted';
							 		$last_id=1;
							 		$_SESSION['resolution']=$last_id;
								 }*/
						} 
						if($part==1)
						 {
							 $_SESSION['last_id']=$last_id;
							 $p_count=0;   
						 } 
						$msg=$txt_msg.$txt;
						fwrite($myfile, $msg);
						$r_count ++;

						++$p_count; 						
					}
					$id=$this->Session->read('User.user_id');
					$target_file1=explode('/',$target_file);
					$csvfile=$target_file1[5];				
					
					$txtfile_name1 =explode('/',$txtfile_name);
					$txtfile=$txtfile_name1[5];				
					
					$this->FcappBulkupload->query("INSERT into cm_bulk_uploads set csv_file='".$csvfile."',error_file='".$txtfile.'.txt'."',user_id='".$id."'"); 
				  	$this->Session->setFlash('CSV uploaded successfully.');
					$this->redirect('bulk_upload_without_content');	
					fclose($myfile);
					
				} else {

					$this->Session->setFlash('Mismatch columns: must be 15 columns.');
					$this->redirect('bulk_upload_without_content'); 				  
				}
			} else {
				$this->Session->setFlash('Unacceptable file type: upload only csv');
				$this->redirect('bulk_upload_without_content'); 	  
			}
		}
		
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCondUser="FIND_IN_SET (user_id, '$finalUserArr')";
		
		}  else {
			$searchCondUser="1=1";
		}
		
		$channelList = $this->Channel->query("select id, c_name from ch_channel where $searchCondUser");
		$this->set('channelList', $channelList);
	}
	
	public function addyoutube(){

		$country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
        $this->set('country', $country);
		$sql = array(); 
		if($this->request->is('post')){	
		
			$value = $this->request->data;
			
			$this->Session->write('Appname.slug', $value["app_slug"]);
		
			$time=$this->request->data['time'];
			$b = time().rand(11,99);
			//echo '<pre>';print_r($this->data);die;
			
			// $content_video;  
			if(isset($value["content"])){
			 $content_video = $value["content"];
			 $content_video1 = explode("?", $content_video);
			 $content_video_id=$content_video1['1'];
			 $vidID = explode("=", $content_video_id); 
			 $vidID=$vidID['1'];
			 $content_poster="https://img.youtube.com/vi/$vidID/default.jpg";
			 $duration=$this->getDurationInSeconds($vidID);
			 }
			 
			$sql['channel_id']=$this->request->data['channel_id'];
			$sql['name']=$this->request->data['name'];
			$sql['user_id']=$this->Session->read('User.id');
			$sql['category_id']=$this->request->data['subcategory_id'];
			$sql['language_code']=$this->request->data['language_code'];
			$sql['ip']=$_SERVER['REMOTE_ADDR'];
			$sql['price']=$this->request->data['price'];
			$sql['description']=$this->request->data['description'];
			$sql['content_type']=$this->request->data['content_type'];
			$sql['tags']=$this->request->data['tags'];
			$sql['display_order']=$this->request->data['display_order'];
			$sql['release_date']=$this->request->data['release_date'];
			$sql['expiry_date']=$this->request->data['expiry_date'];
			$sql['content']= $content_video;
			$sql['poster']= $content_poster;
			$sql['duration']=$duration;
		    $sql['fcut_content_type']='utube';
			$sql['premium_type']='free';
			$sql['meta_title']=$this->request->data['meta_title'];
			$sql['meta_key']=$this->request->data['meta_key'];
			$sql['meta_description']=$this->request->data['meta_description'];
			
			
			/*echo "<pre>";
			print_r($sql);
			die;*/
			
			$this->Fcappcontent->save($sql);
			//$this->Session->setFlash('Content Added Successfully');
			$id=$this->Fcappcontent->getLastInsertID();
			/* if($this->data['Content']['tataprice']=="0" OR $this->data['Content']['tataprice']>"0")
			{ */
			$tataPrice = array();
				if(empty($this->data['tataprice']) && $this->data['tataprice']!=0) $this->data['tataprice']=0;
				$tataPrice['content_id']=$id;
				$tataPrice['billing_partner']='tt';
				$tataPrice['price']=$this->data['tataprice'];
				$this->FcappContentPrice->save($tataPrice);
			/* }
			if($this->data['Content']['mtnlprice']=="0" OR $this->data['Content']['mtnlprice'] >"0" )
			{ */
			$mtnlPrice = array();
				if(empty($this->data['mtnlprice']) && $this->data['mtnlprice']!=0) $this->data['mtnlprice']=0;
				$mtnlPrice['content_id']=$id;
				$mtnlPrice['billing_partner']='mt';
				$mtnlPrice['price']=$this->data['mtnlprice'];
				$this->FcappContentPrice->create();
				$this->FcappContentPrice->save($mtnlPrice);
			/* }
			if($this->data['Content']['vodafoneprice']=="0" OR $this->data['Content']['vodafoneprice']>"0")
			{ */
			$vfPrice = array();
				if(empty($this->data['vodafoneprice']) && $this->data['vodafoneprice']!=0) $this->data['vodafoneprice']=0;
				$vfPrice['content_id']=$id;
				$vfPrice['billing_partner']='vf';
				$vfPrice['price']=$this->data['vodafoneprice'];
				$this->FcappContentPrice->create();
				$this->FcappContentPrice->save($vfPrice);
			/* }
			if($this->data['Content']['aircelprice']=="0" OR $this->data['Content']['aircelprice']>"0")
			{ */
			$aircelPrice = array();
				if(empty($this->data['aircelprice']) && $this->data['aircelprice']!=0) $this->data['aircelprice']=0;
				$aircelPrice['content_id']=$id;
				$aircelPrice['billing_partner']='ac';
				$aircelPrice['price']=$this->data['aircelprice'];
				$this->FcappContentPrice->create();
				$this->FcappContentPrice->save($aircelPrice);
			//}
			$this->Session->setFlash('Content Added Successfully');
			$this->redirect('addyoutube');
		}
		//$this->Session->setFlash('Content Added Successfully');
		
		$lang=$this->Language->find('all');
		$this->set('lang',$lang);
		
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCondUser="FIND_IN_SET (user_id, '$finalUserArr')";
		
		} else {
			$searchCondUser="1=1";
		}
		$channelList = $this->Channel->query("select id, c_name from ch_channel where $searchCondUser");
		
		$this->set('channelList', $channelList);
		
	}
	
	public function edityoutubecontent($id=null){
		$country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
        $this->set('country', $country);
	
		if($this->request->is('post')) 
		{	
			$value = $this->data;
			//echo"<pre>";print_r($value);die;
			
			$created = date('Y-m-d H:i:s');
			
			// Notification entry in table 
			if(($value['old_status']!= $value['status']) && ($value['old_approved']!= $value['approved']))
			{	
				$this->Fcappcontent->query("insert into cm_content_notification  set user_id ='".$this->Session->read('User.id')."',c_id='".$content_id."',c_name ='".$value['name']."',c_type = '1',approved='".$value['approved']."',status='".$value['status']."',created='".$created."',readtime='".$created."',approved_by='".$this->Session->read('User.id')."'");
			}
			else if($value['old_status']!= $value['status'])
			{
				
				$this->Fcappcontent->query("insert into cm_content_notification  set user_id ='".$this->Session->read('User.id')."',c_id='".$content_id."',c_name ='".$value['name']."',c_type = '1',approved='".$value['approved']."',status='".$value['status']."',created='".$created."',readtime='".$created."',approved_by='".$this->Session->read('User.id')."'");
			}
			else if($value['old_approved']!= $value['approved'])
			{
				
				$this->Fcappcontent->query("insert into cm_content_notification  set user_id ='".$this->Session->read('User.id')."',c_id='".$content_id."',c_name ='".$value['name']."',c_type = '1',approved='".$value['approved']."',status='".$value['status']."',created='".$created."',readtime='".$created."',approved_by='".$this->Session->read('User.id')."'");
			}
			else
			{
				
			}
			
			//-------- tata price ---------
			if($value['tata_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['tataprice']."' where  content_price_id='".$value['tata_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='tt', content_id='".$value['content_id']."', price='".$value['tataprice']."' ");
			}
			
			//-------- mtnl price ---------			
			if($value['mtnl_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['mtnlprice']."' where  content_price_id='".$value['mtnl_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='mt', content_id='".$value['content_id']."', price='".$value['mtnlprice']."' ");
			}
				
			//-------- vodafone price ---------		
			if($value['vodafone_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['vodafoneprice']."' where  content_price_id='".$value['vodafone_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='vf', content_id='".$value['content_id']."', price='".$value['vodafoneprice']."' ");
			}
			
			//-------- aircel price ---------	
			if($value['aircel_price_id']!=''){ 
				$this->FcappContentPrice->query("update cm_content_prices set price='".$value['aircelprice']."' where  content_price_id='".$value['aircel_price_id']."'");
			}else{			
				$this->FcappContentPrice->query("Insert into cm_content_prices set billing_partner='ac', content_id='".$value['content_id']."', price='".$value['aircelprice']."' ");
			}
			
			if(!isset($value['approved'])) {
				$value['approved'] = 0;
				
			}
			
			if($value['approved'] ==2){
				$remarkVal = $value['remark'];
			} else{
				$remarkVal = '';
			}
			
			if(isset($value["content"])){
			 $content_video = $value["content"];
			 $content_video1 = explode("?", $content_video);
			 $content_video_id=$content_video1['1'];
			 $vidID = explode("=", $content_video_id); 
			 $vidID=$vidID['1'];
			 $content_poster="https://img.youtube.com/vi/$vidID/default.jpg";
			 $duration=$this->getDurationInSeconds($vidID);
			 }
			 
			 if(($this->Session->read('User.user_type') == "SEO")){
				$seoFlag = '1';
			} else {
				$seoFlag = '0';
			}
			$approvedBy = "";
			if($value["approved"] &&  $value["approved1"] != $value["approved"]){
               if($this->Session->read('User.user_type') == "V3MO" || $this->Session->read('User.user_type') == "V3MOA"){
				   $approvedBy = $this->Session->read('User.id');
			   } else {
				   $approvedBy = "";
			   }
			}
			
					
			$this->Fcappcontent->updateAll(
					array('name' =>"'".addslashes($value['name'])."'",'poster'=>"'".$content_poster."'",'content'=>"'".$content_video."'",'price'=>"'".$value['price']."'",'category_id' =>"'".$value['subcategory_id']."'",'display_order' =>"'".$value['display_order']."'",
					'description' =>"'".addslashes($value['description'])."'",'tags' =>"'".$value['tag']."'",'language_code' =>"'".$value['language_code']."'",'status' =>"'".$value['status']."'",'director_name' =>"'".addslashes($value['director_name'])."'",'production_house' =>"'".addslashes($value['production_house'])."'",'release_date' =>"'".addslashes($value['release_date'])."'",'expiry_date' =>"'".addslashes($value['expiry_date'])."'",'approved' =>"'".$value['approved']."'",'approved_by' =>"'".$approvedBy."'",'remark' =>"'".$remarkVal."'",'duration' =>"'".$duration."'",'meta_title' =>"'".addslashes($value['meta_title'])."'",'meta_key' =>"'".addslashes($value['meta_key'])."'",'meta_description' =>"'".addslashes($value['meta_description'])."'",'premium_type' =>"'".$value['premium_type']."'",'channel_id' =>"'".$value['channel_id']."'",'seoFlag' =>"'".$seoFlag."'"),
					array('content_id' =>$value['content_id'])
			);
			
			$this->Session->setFlash('Content edit successfully');
			$this->redirect('viewcontent');
		
		}		
		
		$content_id = $id;
		$query="SELECT Content.*, Category.category_id, Subcategory.category_id, Subcategory.project, Subcategory.parent_id
				FROM cm_contents Content, cm_categories Category, cm_categories Subcategory 
				WHERE Content.category_id=Subcategory.category_id
				AND Content.content_id=$content_id ";
		$content = $this->Fcappcontent->query($query);	
		//echo "<pre>";print_r($content);die;
		$tata_price_arr = $this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='tt' and content_id='".$content_id."'");
		$mtnl_price_arr=$this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='mt' and content_id='".$content_id."'");
		$vodafone_price_arr=$this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='vf' and content_id='".$content_id."'");
		$aircel_price_arr=$this->FcappContentPrice->query("select * from cm_content_prices as DomainContentPrice where billing_partner='ac' and content_id='".$content_id."'");
		
		$content=$content[0];
		$langs=$this->Language->find('all');
		$parent_id=$content['Subcategory']['parent_id'];
		
		$subCatList = $this->Fcappcategories->query("select * from cm_categories where category_id='".$content['Content']['category_id']."'");
		$this->set('subCatList', $subCatList);	
	
		$this->set(compact('categories','sub_categories','category','content','langs','tata_price_arr','mtnl_price_arr','vodafone_price_arr','aircel_price_arr','content'));
		
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCondUser="FIND_IN_SET (user_id, '$finalUserArr')";
		
		} else {
			$searchCondUser="1=1";
		}
		$channelList = $this->Channel->query("select id, c_name from ch_channel where $searchCondUser");
		
		$this->set('channelList', $channelList);
	}
	
    function getDurationInSeconds($talkId){
		$vidkey = $talkId ;
		$apikey = "AIzaSyCz2PEZszqb9WgonuYmFwlMOr0Rdp0prSk";
		$dur = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$vidkey&key=$apikey");
		$VidDuration =json_decode($dur, true);
		foreach ($VidDuration['items'] as $vidTime)
		{
		   $VidDuration= $vidTime['contentDetails']['duration'];
		}
		@preg_match_all('/(\d+)/',$VidDuration,$parts);
		$hours = intval(floor($parts[0][0]/60)%60 * 60);
		$minutes = intval($parts[0][0]%60 * 60);
		$seconds = intval($parts[0][1]);
		$totalSec = $hours + $minutes + $seconds; //this is the example in seconds
		return $totalSec;
		die;
	} 
	public function view_content_category(){
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
	    $country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
		$this->set('country', $country); 	
		$data=$this->request->query;
		
		if($this->request->is('post'))
		{
			$value=$this->data;
			$user_id=$this->Session->read('User.user_id');
			if(isset($value['statusn']))
			    {
					
					if($value['statusn']=="2")
					{   
					    foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 2 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
					    $this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('viewcontent');
					}
					if($value['statusn']=="1")
					{
						foreach($value['check'] as $checkone)
					    {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 1 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('viewcontent');
					}
					if($value['statusn']=="T")
					{
						foreach($value['check'] as $checkone)
					    {
							$modified = date('YmdHis');
						    $this->Fcappcontent->query("UPDATE cm_contents SET trend = 'T',modified = '".$modified."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('viewcontent');
					}
					if($value['statusn']=="0")
					   {
						   foreach($value['check'] as $checkone)
					       {
							$this->Fcappcontent->query("UPDATE cm_contents SET approved = 0 , approved_by = '".$user_id."' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');	
						$this->redirect('viewcontent');
					}
					if($value['statusn']=="A")
					{
						foreach($value['check'] as $checkone)
					    {
						 $this->Fcappcontent->query("UPDATE cm_contents SET status = 'A' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');
						$this->redirect('viewcontent');						
					}
					if($value['statusn']=="D")
					{
						foreach($value['check'] as $checkone)
						{
							$this->Fcappcontent->query("UPDATE cm_contents SET status = 'D' WHERE content_id='".$checkone."'");
						}	
						$this->Session->setFlash('Content Updated Successfully.');		
						$this->redirect('viewcontent');							
					}
				
				}
			}
			
			
			if(isset($data['status']) && $data['status'] !="")
			{
				
				if($data['status']=='A')
				{
					$status= "t1.status='".$data['status']."'";
				}
				else if($data['status']=='D')
				{
					$status="t1.status='".$data['status']."'";
				}
				else if($data['status']=='1')
				{
					$status="t1.approved='".$data['status']."'";
				}
				else if($data['status']=='2')
				{
					$status="t1.approved='".$data['status']."'"; 
				} 
				else if($data['status']=='0')
				{
					
					$status="t1.approved='".$data['status']."'"; 
				}
				
			} else {
					$status = "(t1.approved='2' OR t1.approved='1' OR t1.approved='0' OR t1.status = 'A' OR t1.status = 'D')";
			}
			$strCond = "";
			$strCond .= $status;
 			if(!empty($data['name']))
			{
				$strCond .= " and t1.NAME like '%".$data['name']."%'";
			} 
			else if(!empty($data['country_code']))
			{
				$strCond .= "and t1.country_code='".$data['country_code']."'";
			}
			
			else if(!empty($data['subcategory_id']))
			{	
				$strCond .= " and t1.category_id='".$data['subcategory_id']."'";
			}
			else if(!empty($data['channel_id']))
			{	
				$strCond .=" and t1.channel_id='".$data['channel_id']."'";
			}
			
			$channelCond = "";
			if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
			{
				
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
				
				
				$arr = array();
				foreach($userid as $key=>$val){
					
					$arr[] = $val["cm_users"]["id"];
				}
				$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
				$finalUserArr = implode(",", $finalUser);
				$searchCond[]="FIND_IN_SET (t1.user_id, '$finalUserArr')";
				
				$channelCond = "and FIND_IN_SET (user_id, '$finalUserArr')"; 
			}
			
			$channelList = $this->Channel->query("select id, c_name from ch_channel where status < '3' $channelCond");
				$this->set('channelList', $channelList);
			//data by filter...
			
			if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
				$searchCond[] = " t1.status = '".$_GET["filterBy"]."'" ;
			}
			
			if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
				$orderBy = " order by t1.content_id ".$_GET["sortBy"]."" ;
			}  else {
				$orderBy = " order by t1.content_id desc" ;
			}
			
			$searchCond[]="t1.approved!= 3";
			$searchCondStr=implode(" AND ",$searchCond);
			//echo "SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage";
			//die;
			$value = $this->Fcappcontent->query("SELECT t1.content_id, t2.name,t1.name,t1.content,t1.poster,t1.country_code,t1.description,t1.tags,t1.status,t1.created,t1.approved,t1.fcut_content_type, t1.view, t1.user_id,t1.sequence,t1.cp_sequence FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where $strCond and $searchCondStr and t2.project='firstcut' $orderBy limit $offSet, $recPerPage");
			
			$downloadsRow = $this->Fcappcontent->query("SELECT count(t1.content_id) as countRec FROM cm_contents AS t1 INNER JOIN cm_categories AS t2 ON t1.category_id = t2.category_id where  $strCond and $searchCondStr  and t2.project='firstcut'");
			
			$totalRec = $downloadsRow[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
			
			$this->set(compact('value', $value));
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
	}
	public function sequencechange() 
	{
		$data = $this->request->data;
	    if(!empty($data))
		{
			$current_date=date('Y-m-d H:i:s');
			
			$this->Fcappcontent->query("UPDATE cm_contents SET sequence = '".$data['c_id']."', modified='".$current_date."' WHERE content_id='".$data['content_id']."'");
		echo "1";
	    exit;
		}
		
	}
	public function sequencechange1() 
	{
		$data = $this->request->data;
	    if(!empty($data))
		{
			$current_date=date('Y-m-d H:i:s');
			
			$this->Fcappcontent->query("UPDATE cm_contents SET cp_sequence = '".$data['c_id']."', modified='".$current_date."' WHERE content_id='".$data['content_id']."'");
		echo "1";
	    exit;
		}
		
	}
	public function direct_cg_image_bucket()
	{
		if($this->request->isPost())
		{   
		   $sql = array(); 
           $data = $this->request->data;
		   $sql["fcimage"] = $data["fcimage"];
		   $sql["user_id"] = $this->Session->read('User.id');
           $time = time();
		   if(!empty($data["fcimage"]["name"]))
			{
				//fcimage image upload...
				$file1 = $data['fcimage']; 
				$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType1)!= "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif"))
				{
					$this->Session->setFlash('Unacceptable file type for channel banner.');
					$this->redirect('direct_cg_image_bucket');
					
				} 
				else 
				{
					move_uploaded_file($file1['tmp_name'], CHANNEL_PUBLICHTML_PATH . 'cg_fc_image/' . "fcimage_".$time.".".$imageFileType1);
					$sql["fcimage"] = "fcimage_".$time.".".$imageFileType1;
				}
			}
			else
			{
				$sql["fcimage"] = "v3mobi.jpg";
			}
			
			if(!empty($data["cgimage"]["name"]))
			{
				$file2 = $data['cgimage']; 
				$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType2) != "jpg") && (strtolower($imageFileType2) !="png") && (strtolower($imageFileType2) !="jpeg") && (strtolower($imageFileType2) !="gif"))
				{
					$this->Session->setFlash('Unacceptable file type for channel icon.');
					$this->redirect('direct_cg_image_bucket');
					
				} else 
				{
					move_uploaded_file($file2['tmp_name'], CHANNEL_PUBLICHTML_PATH . 'cg_fc_image/' . "cgimage_".$time.".".$imageFileType2);
					$sql["cgimage"] = "cgimage_".$time.".".$imageFileType2;
				}
			}
			else
			{
				$sql["cgimage"] = "profile_pic.png";
			}
			
			$fcimage = CHANNEL_PUBLICHTML_PATH . 'cg_fc_image/'. $sql["fcimage"];
			$cgimage = CHANNEL_PUBLICHTML_PATH . 'cg_fc_image/'. $sql["cgimage"];
			if(file_exists($fcimage))
			{	
				$files_arr = explode("\n", $fcimage);
				$s3 = new S3(ACCESS_KEY,SECRET_KEY);
				
				foreach($files_arr as $file1)
				{
					if(!empty($file1)){
					
					    $bucketname = 'contentsimages';
						$file1_arr = explode("/",$file1);
						$filename1 = end($file1_arr);
						$main_arr = array_splice($file1_arr, 5);
						$bucket_file = implode("/",$main_arr); 
						$image_type = image_type_to_mime_type(exif_imagetype($file1));
						$header = array('Content-Type' =>$image_type);
						$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
						if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header))
						{
							
						}
					}
				}
				 
			}
			
			if(file_exists($cgimage))
			{
				
				$files_arr = explode("\n", $cgimage);
				$s3 = new S3(ACCESS_KEY,SECRET_KEY);
				foreach($files_arr as $file1){
					if(!empty($file1)){
					    $bucketname = 'contentsimages';
						$file1_arr = explode("/",$file1);
						$filename1 = end($file1_arr);
						$main_arr = array_splice($file1_arr, 5);
						$bucket_file = implode("/",$main_arr); 
						$image_type = image_type_to_mime_type(exif_imagetype($file1));
						$header = array('Content-Type' =>$image_type);
						if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header))
						{
							
						}
					}
				}
				 
			}
			
			//image move to bucket code end here...
			$sql["name"]= $data["name"];
			
			$this->Fccgimage->save($sql);
			$lastInsertId =  $this->Fccgimage->id;
			$this->Session->setFlash('Images added successfully.');
			$this->redirect('direct_cg_image_bucket');
		}
	}
public function addcontent()
	{
		$country = $this->Country->query("select id,country_name,country_code from cm_country_code where status=1");
        $this->set('country', $country);
		
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
		{
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val)
			{
				
				$arr[] = $val["cm_users"]["id"];
			}
			
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCondUser="FIND_IN_SET (user_id, '$finalUserArr')";
		} 
		else 
		{
			$searchCondUser="1=1";
		}
		
		
		$channelList = $this->Channel->query("select id, c_name from ch_channel where $searchCondUser ");
		$this->set('channelList', $channelList);
		
		$lang=$this->Language->find('all');
		$this->set('lang',$lang);
		if($this->request->is('post'))
		{
			$data = $this->data;
			//echo "<pre>";
			//print_r($data);
			//die;
			$countRec = count($data['content']); 
			$sql=array();
			for($i = 0; $i < $countRec; $i++)
		    {
				if(!empty($_FILES['data']['name']))
				{
					if(is_uploaded_file($_FILES['data']['tmp_name']['content'][$i]))
					{
						$sourcePath1 = $_FILES['data']['tmp_name']['content'][$i];
						$content = $_FILES['data']['name']['content'][$i];
						$b = time().rand(11,99);
						$imageFileType = pathinfo($content,PATHINFO_EXTENSION);
						if($imageFileType != 'mp4')
						{
							
							$this->Session->setFlash('Only mp4 format allowed');
							$this->redirect('addcontent');
						}else
						{
						$newfilename= $b.'.'.$imageFileType; 
						$targetPath = DOM_PUBLICHTML_PATH."contents/".$newfilename;
						if(move_uploaded_file($sourcePath1,$targetPath))
						{
							$sql['content']= $newfilename;
							$contentimage = $newfilename;
						}
						}
					}
					if(is_uploaded_file($_FILES['data']['tmp_name']['trialcontent'][$i]))
					{
						$sourcePath2 = $_FILES['data']['tmp_name']['trialcontent'][$i];
						$trialcontent = $_FILES['data']['name']['trialcontent'][$i];
						$imageFileType = pathinfo($trialcontent,PATHINFO_EXTENSION);
						$b = time().rand(11,99);
						$newfilename= $b.'.'.$imageFileType;
						$targetPath2 = DOM_PUBLICHTML_PATH."contents/".$newfilename;

						if(move_uploaded_file($sourcePath2,$targetPath2))
						{
							
							$sql['trial_content']= $newfilename;
							$trialcont = $newfilename;
						}
						
					}
					else
					{
						$sql['trial_content']= "";
					    $trialcont = "";
						
					}
					if(is_uploaded_file($_FILES['data']['tmp_name']['poster'][$i]))
					{
						$sourcePath3 = $_FILES['data']['tmp_name']['poster'][$i];
						$poster = $_FILES['data']['name']['poster'][$i];
						$imageFileType = pathinfo($poster,PATHINFO_EXTENSION);
						$b = time().rand(11,99);
						$newfilename= $b.'.'.$imageFileType;
						$targetPath3 = DOM_PUBLICHTML_PATH."contents/".$newfilename;
						
						if(move_uploaded_file($sourcePath3,$targetPath3))
						{
							$sql['poster']= $newfilename;
							$posterimage = $newfilename;
							
						}
						
					}
				
						
				}
				//image move to bucket code start here...
				$contentimage1 = DOM_PUBLICHTML_PATH."contents/".$contentimage;
				$trialcont1 = DOM_PUBLICHTML_PATH."contents/".$trialcont;
				$posterimage1 = DOM_PUBLICHTML_PATH."contents/".$posterimage;
				if(file_exists($contentimage1))
				{
					$files_arr = explode("\n", $contentimage1);
				    $s3 = new S3(ACCESS_KEY,SECRET_KEY);
					foreach($files_arr as $file1)
				    {
						if(!empty($file1))
						{
							$bucketname = "firstcut";
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
						    $main_arr = array_splice($file1_arr, 5);
						    $bucket_file = implode("/",$main_arr);
							$image_type = "video/mp4";
							$header = array('Content-Type' =>$image_type);
                            $cache = array('CacheControl' =>'public, max-age=31536000');

$command = ("/usr/bin/ffmpeg  -i ".$file1." -vstats 2>&1");  
     					    $output = shell_exec($command); 
							$regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
							
							if (preg_match($regex_sizes, $output, $regs)) {
        						  $codec = $regs [1] ? $regs [1] : null;
         						  $width = $regs [3] ? $regs [3] : null; 
         						  $height = $regs [4] ? $regs [4] : null;
								  
								}
	 							 $search='/Duration: (.*?),/';
								 preg_match($search, $output, $matches);
							     $explode = explode(':', $matches[1]);
								 /*echo 'Hour: ' . $explode[0];
								 echo 'Minute: ' . $explode[1];
								 echo 'Seconds: ' . $explode[2];*/
								 $duration=($explode[0]*3600) + ($explode[1]*60) + $explode[2];
								 $duration=round($duration);							
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
						
						if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header))
						{
							//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
						}
							
						}
					}
				}
				if(file_exists($trialcont1))
				{
					$files_arr = explode("\n", $trialcont1);
				    $s3 = new S3(ACCESS_KEY,SECRET_KEY);
					foreach($files_arr as $file1)
				    {
						if(!empty($file1))
						{
							$bucketname = "firstcut";
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
						    $main_arr = array_splice($file1_arr, 5);
						    $bucket_file = implode("/",$main_arr);
							$image_type = "video/mp4";
							$header = array('Content-Type' =>$image_type);	
							$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
						
						//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
						if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header))
						{
							//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
						}
							
						}
					}
				}
				else
				{
					$sql['trial_content']= "";
					//$trialcont = "";
				}
				if(file_exists($posterimage1))
				{
					$files_arr = explode("\n", $posterimage1);
				    $s3 = new S3(ACCESS_KEY,SECRET_KEY);
					foreach($files_arr as $file1)
				    {
						if(!empty($file1))
						{
							$bucketname = "contentsimages";
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
						    $main_arr = array_splice($file1_arr, 5);
						    $bucket_file = implode("/",$main_arr);
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);	
							$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
						
						//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
						if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header))
						{
							//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
						}
							
						}
					}
				}
			$sql['app_slug'] = $this->request->data['app_slug'][$i];
			$sql['country_code'] = $this->request->data['country_code'][$i];
			$sql['channel_id'] = $this->request->data['channel_id'][$i];
			$sql['category_id'] = $this->request->data['subcategory_id'][$i];
			$sql['country_code'] = $this->request->data['country_code'][$i];
			$sql['name']   = $this->request->data['name'][$i];
			$sql['price'] = '10.00';
			$sql['tags']  = $this->request->data['tags'][$i];
			$sql['language_code']  = $this->request->data['language_code'][$i];
			$sql['user_id']=$this->Session->read('User.id');
			$sql['ip']=$_SERVER['REMOTE_ADDR'];
			$sql['display_order']  = '100';
			$sql['content_type']  = 'V';
			$sql['duration']=$duration;
			$sql['director_name']  = $this->request->data['director_name'][$i];
			$sql['production_house']  = $this->request->data['production_house'][$i];
			$sql['description']  = $this->request->data['description'][$i];
			$sql['meta_title']  = $this->request->data['meta_title'][$i];
			$sql['meta_key']  = $this->request->data['meta_key'][$i];
			$sql['meta_description']  = $this->request->data['meta_description'][$i];
			$sql['premium_type']  = 'free';
			$this->Fcappcontent->create();		
		    $this->Fcappcontent->save($sql);
			}
			$this->Session->setFlash('Content Added Successfully');
		    $this->redirect('addcontent');
		}
	}
}
?>