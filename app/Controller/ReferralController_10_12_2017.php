<?php
App::uses('AppController', 'Controller');
class ReferralController extends AppController{
	public $name = 'Referral';
	public $uses = array('Userprofile', 'Fcusers');
	public $components = array('Paginator');
	public function beforeFilter() {
		parent::beforeFilter();
	}
		
	public function countreport(){
		
		if(!empty($_GET['dataFor'])){
			$dataFor = $_GET['dataFor'];
		} else {
			$dataFor = "registerUser";
		}
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate']. " 00:00:00";
		} else {
			$startDate = date('Y-m-d'). " 00:00:00";
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate']. " 23:59:59";
		} else {
			$endDate = date('Y-m-d'). " 23:59:59";
		}
		
		if($dataFor == "registerUser"){//fetch register user data
		   
			$registerData = $this->Userprofile->query("select count(id) as countRec from user_profile where status=1 and project='firstcut' and operating_system='android' and created>='".$startDate."' and created<='".$endDate."'");
			$this->set(compact('registerData','registerData'));
			
		}
		
		if($dataFor == "view"){//fetch user view user data
		    
			$userViewData = $this->Fcusers->query("select t1.msisdn, count(t2.id) as countRec from user_profile as t1 inner join users_view as t2 on t2.user_id = t1.id where t1.msisdn !='' and t1.msisdn !='NA' and  t1.status=1 and t1.project='firstcut' and t1.operating_system='android' and t2.user_id>0 and t2.project='firstcut' and t2.operating_system='android' and t2.created>='".$startDate."' and t2.created<='".$endDate."' group by  t1.msisdn order by count(t2.id) desc limit 0,100");
			$this->set(compact('userViewData','userViewData'));
			
		}
		
		if($dataFor == "referral"){//fetch user referal data...
		
			$referralData = $this->Userprofile->query("select t1.msisdn as msisdn, count(t2.referredBy) as referredBy from user_profile as t1 join user_profile as t2 on t2.referredBy=t1.referralId where t1.msisdn !='' and t1.msisdn !='NA' and t1.status=1 and t1.project='firstcut' and t1.operating_system='android' and t2.msisdn !='' and t2.msisdn !='NA' and t2.status=1 and t2.created>='".$startDate."' and t2.created<='".$endDate."' group by t1.referredBy order by count(t2.referredBy) desc");
			$this->set(compact('referralData','referralData'));
			
		}
		
		if($dataFor == "subscriber"){//fetch user subscriber data...
		    //paging section start here...
			$pageNum=1;
			$recPerPage = RECORDPERPAGE;
		    $recPerPage = 1;
			if(isset($this->request->query['page']) && $this->request->query['page']!=0)
			{
				$pageNum = $this->request->query['page'];
			}
			$offSet = ($pageNum - 1) * $recPerPage;
			//paging section end here...
		
			$subscriberData = $this->Userprofile->query("select t1.msisdn as msisdn, t1.id as user_id  from user_profile as t1 join ch_subscribed_user_details as t2 on t2.app_user_id=t1.id where t1.msisdn !='' and t1.msisdn !='NA' and t1.status=1 and t1.project='firstcut' and t1.operating_system='android' and t2.app_user_id !='' and t2.app_user_id !='NA' and t2.status=1 and t2.created>='".$startDate."' and t2.created<='".$endDate."' group by t1.msisdn order by t2 .id desc limit $offSet, $recPerPage");
			
			$subscriberCountData = $this->Userprofile->query("select count(t1.msisdn) as countRec from user_profile as t1 join ch_subscribed_user_details as t2 on t2.app_user_id=t1.id where t1.msisdn !='' and t1.msisdn !='NA' and t1.status=1 and t1.project='firstcut' and t1.operating_system='android' and t2.app_user_id !='' and t2.app_user_id !='NA' and t2.status=1 and t2.created>='".$startDate."' and t2.created<='".$endDate."'");
			$totalRec = $subscriberCountData[0][0]["countRec"];
			$numOfPage = ceil($totalRec / $recPerPage);
		
			$this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
			$this->set(compact('subscriberData','subscriberData'));
			//echo "<pre>";
			//print_r($subscriberData);
			//die;
		}
	}
	
	public function detailcountreport(){
		$startDate = $_GET['startDate']. " 00:00:00";
		$endDate = $_GET['endDate']. " 23:59:59";
		$userDetailArr = array();
		$userViewData = $this->Fcusers->query("select count(id) as countRec from  users_view where user_id='".$_GET["user_id"]."' and created>='".$startDate."' and created<='".$endDate."'");
		
		$referralId = $this->getReferalId($_GET["user_id"]);
		$registerData = 0;
		if(!empty($referralId)){
			$registerDataRec = $this->Userprofile->query("select count(id) as countRec from user_profile where referredBy='".$referralId[0]["user_profile"]["referralId"]."' and status=1 and project='firstcut' and operating_system='android' and created>='".$startDate."' and created<='".$endDate."'");
			$registerData = $registerDataRec[0][0]["countRec"];
			$registerMsisdn = $referralId[0]["user_profile"]["msisdn"];
			
		}
		
		$userDetailArr["userView"] = $userViewData[0][0]["countRec"];
		$userDetailArr["registerUser"] = $registerData;	
		$userDetailArr["msisdn"] = $registerMsisdn;	
		$this->set(compact('userDetailArr','userDetailArr'));
       
	}
	public function getReferalId($userId){
		
		$referralId = $this->Userprofile->query("select msisdn, referralId from user_profile where id='".$userId."'");
		return $referralId;
		
	}
	
}
?>