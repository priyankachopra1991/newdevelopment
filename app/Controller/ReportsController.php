<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'S3.php');
App::import('Vendor', 'sdk/sdk.class.php');
App::import('Vendor', 'AmazonS3.php');
ini_set('error_log','/home/newdevelopment/logs/report.log');
class ReportsController extends AppController{
	public $name = 'Reports';
	public $uses = array('Contentusedreport', 'User', 'Channel', 'Transaction', 'UserHit', 'Fcuserlogin', 'Fcusercpcontentviewfull','Fcusers', 'Fcuserviewreport', 'Fcusercpcontentview', 'Operator', 'Project', 'Bppack77', 'UserHit77', 'Transaction77', 'Vodafonecallback77', 'Vodafoneupdate77', 'Ideacallback77', 'Bppack99bhojpuri', 'Bppack99comics', 'UserHit99bhojpuri', 'Transaction99bhojpuri', 'Billingnotification99bhojpuri', 'UserHit99v3mobi', 'Transaction99v3mobi', 'Tatalog99v3mobi', 'Vodafonecallback99bhojpuri', 'Subscribeuser77', 'Cprevenue99v3mobi', 'Cprevenue99v3mobiairtel', 'Fcappcontent', 'BpDownload77', 'Publishercontentview', 'PublisherOrderLine', 'PublisherOrder');
	
	public $components = array('Paginator');
	public function beforeFilter() {
	parent::beforeFilter();
	
	}
	
	public function contentusedreport(){
		
		//Find all user id...
		$searchCond[]="1=1";
		$userIdCheck = "";
		$endDate = date('Y-m-d', strtotime("-1 day"));
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="FIND_IN_SET (cp_id, '$finalUserArr')";
			$userIdCheck = " and FIND_IN_SET (cp_id, '$finalUserArr')";
		}
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		$searchCond[]="date(t1.created)>= '".$startDate."' and date(t1.created)<= '".$endDate."'";
		$searchCondStr=implode(" AND ", $searchCond);
		//$contentData = $this->Contentusedreport->query("select t1.content_id as content_id, t1.created as created, t1.cp_id as cpId,t2.channel_id as channel_id, count(t1.id) as countRec, t2.name as contentName, t2.country_code as country  from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where $searchCondStr group by date(t1.created), content_id order by date(t1.created) asc");
		
		
		$contentData = $this->Contentusedreport->query("select t1.content_id as content_id, t1.created as created, t1.cp_id as cpId,t2.channel_id as channel_id, count(t1.id) as countRec, t2.name as contentName, t2.country_code as country  from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where $searchCondStr group by date(t1.created), content_id order by t2.content_id desc");
		
		$this->set('contentData', $contentData);
		//echo "<pre>";
		//print_r($contentData);
		//die;
		
		//bulk revenue...
		
		$last7days = date('Y-m-d', strtotime("-7 day"));
		$last30days = date('Y-m-d', strtotime("-30 day"));
		$endDate = date('Y-m-d', strtotime("-1 day"));
		//last 7 days revenue...
		
		$contentData7 = $this->Contentusedreport->query("select  count(t1.id) as countRec from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where t1.created >= '".$last7days." 00:00:00' and t1.created <= '".$endDate." 23:59:59' $userIdCheck");
		$this->set('contentData7', $contentData7);
		//last 30 days revenue...
		$contentData30 = $this->Contentusedreport->query("select  count(t1.id) as countRec from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where t1.created >= '".$last30days." 00:00:00' and t1.created <= '".$endDate." 23:59:59' $userIdCheck");
		$this->set('contentData30', $contentData30);
		//last year revenue...
		$contentDataYear = $this->Contentusedreport->query("select  count(t1.id) as countRec from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where year(t1.created)='".date('Y')."'  and date(t1.created)<='".$endDate."' $userIdCheck");
		$this->set('contentDataYear', $contentDataYear);
	}
	
	function contentusedreportexcel()
	{
		$this->autoRender = false;
		$startDate = $_GET['startDate'];
		$endDate = $_GET['endDate'];
		$searchCond[]="1=1";
		if(($this->Session->read('User.user_type') =="CPA") || ($this->Session->read('User.user_type') =="CPU"))
		{
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 
			$finalUserArr = implode(",", $finalUser);
			$searchCond[]="FIND_IN_SET (cp_id, '$finalUserArr')";
		}
		$searchCond[]="date(t1.created)>= '".$startDate."' and date(t1.created)<= '".$endDate."'";
		$searchCondStr=implode(" AND ", $searchCond);

		$contentData = $this->Contentusedreport->query("select t1.content_id as content_id, t1.created as created, t1.cp_id as cpId,t2.channel_id as channel_id, count(t1.id) as countRec, t2.name as contentName from user_cp_content_view as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where $searchCondStr group by date(t1.created), content_id order by date(t1.created) asc");
		$date = date('Y-m-d');
		$filename = "ContentUsedReports_".$date.".csv";
		$csv_file = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		$header_row = array("content id","Content Name","Channel Name","Date","Number of Users","Revenue");
		fputcsv($csv_file,$header_row,',','"');
		foreach($contentData as $contentData=>$obj)
		{
			$contentPriceVal = $this->User->query("select content_pricing from cm_users where id='".$obj['t1']['cpId']."'");
			$channelName = $this->Channel->query("select c_name from ch_channel where id='".$obj['t2']['channel_id']."'");
			//$contentWiseRevenue = $obj['t1']['cpId']*$contentPriceVal[0]['cm_users']['content_pricing']; 
			//print_r($channelName);
			//die;


			$row = array(
			$obj['t1']['content_id'],
			$obj['t2']['contentName'],
			$channelName['0']['ch_channel']['c_name'],
			$obj['t1']['created'],
			$obj['0']['countRec'],
			$obj['0']['countRec']*$contentPriceVal[0]['cm_users']['content_pricing']

			);
			fputcsv($csv_file,$row,',','"');
		}
		fclose($csv_file);
	}
	
	public function dailyusedreport(){
	
		
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher']; 
			$biller_id = $_GET['biller_id']; 
			$arr=array();
			$userid=$this->Session->read('User.id');
			if(($this->Session->read('User.user_type') =="V3MOA"))
			{
				 /* Total User Hits with msisdn*/
			$totalhits = $this->UserHit->query("select count(*) as count,date(date) as date from ch_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
			foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
				}	
				
           /* Billing Status */
			
			$billing_done = $this->Transaction->query("select count(t1.content_id) as count from ch_transaction as t1 join cm_contents as t2 on t1.content_id = t2.content_id and t1.biller_id='".$_GET['biller_id']."' and t1.publisher = '".$publisher."' and t1.transaction_time>='".$stDate."' and t1.transaction_time<='".$edDate."' and t1.billing_status='DONE'");
			
			foreach($billing_done as $key=>$val)
			{
					$resArr[$date]['billing_done']=$val[0]['count'];
			}
			
			/* Total Revenue */
			
			$total_revenue = $this->Transaction->query("select sum(t1.price_deducted) as sum from ch_transaction as t1 join cm_contents as t2 on t1.content_id = t2.content_id and t1.biller_id='".$_GET['biller_id']."' and t1.publisher = '".$publisher."' and t1.transaction_time>='".$stDate."' and t1.transaction_time<='".$edDate."' and t1.billing_status='DONE'");
			
			
			foreach($total_revenue as $key=>$val)
			{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
			} 
			/* Callback Status */
			
		    /*$callback_status=$this->Transaction->query("select count(t1.transaction_id) as count  from ch_transaction  as t1 join cm_contents as t2 on t1.content_id = t2.content_id and t1.biller_id='".$_GET['biller_id']."' and t1.publisher = '".$publisher."' and t1.transaction_time>='".$stDate."' and t1.transaction_time<='".$edDate."' and t1.billing_status='DONE' and t1.callback_status='1'");
			
			foreach($callback_status as $key=>$val)
			{
					$resArr[$date]['callback_status']=$val[0]['count'];
			}*/
			$this->set(compact('totalhits','resArr'));
			
			}
			else
			{
				/* Total User Hits with msisdn*/
			$totalhits = $this->UserHit->query("select count(*) as count,date(date) as date from ch_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
			foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
				}	

			/* Billing Status */
			
			//$billing_done = $this->Transaction->query("select count(*) as count from ch_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
			
			$billing_done = $this->Transaction->query("select count(t1.content_id) as count from ch_transaction as t1 join cm_contents as t2 on t1.content_id = t2.content_id and t1.biller_id='".$_GET['biller_id']."' and t1.publisher = '".$publisher."' and t1.transaction_time>='".$stDate."' and t1.transaction_time<='".$edDate."' and t1.billing_status='DONE' and t2.user_id = '".$userid."'");
			
			foreach($billing_done as $key=>$val)
			{
					$resArr[$date]['billing_done']=$val[0]['count'];
			}
			
			/* Total Revenue */
			
			$total_revenue = $this->Transaction->query("select sum(t1.price_deducted) as sum from ch_transaction as t1 join cm_contents as t2 on t1.content_id = t2.content_id and t1.biller_id='".$_GET['biller_id']."' and t1.publisher = '".$publisher."' and t1.transaction_time>='".$stDate."' and t1.transaction_time<='".$edDate."' and t1.billing_status='DONE' and t2.user_id = '".$userid."'");
			
			
			foreach($total_revenue as $key=>$val)
			{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
			} 
			/* Callback Status */
			
		    /*$callback_status=$this->Transaction->query("select count(t1.transaction_id) as count  from ch_transaction  as t1 join cm_contents as t2 on t1.content_id = t2.content_id and t1.biller_id='".$_GET['biller_id']."' and t1.publisher = '".$publisher."' and t1.transaction_time>='".$stDate."' and t1.transaction_time<='".$edDate."' and t1.billing_status='DONE' and t1.callback_status='1' and t2.user_id = '".$userid."'");
			
			foreach($callback_status as $key=>$val)
			{
					$resArr[$date]['callback_status']=$val[0]['count'];
			}*/
			$this->set(compact('totalhits','resArr'));
		
			
			
			}
			
		}	
		
	}
	
	function report_user_data()
	{
		if(empty($_GET['startDate'])){
			$_GET['startDate'] = date('Y-m-d', strtotime("-1 day"));
		}
		if((!empty($_GET['startDate'])))
		{
			$datenew = $_GET['startDate'];
			//$datenew = date('Y-m-d', strtotime("-1 day", strtotime($date)));
			$sql1 = $this->Fcuserviewreport->query("SELECT  count(*) as count1 FROM user_profile where  created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'wap'");
			$this->set('sql1', $sql1);
			$sql2 = $this->Fcuserviewreport->query("SELECT  count(*) as count2 FROM user_profile where  created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'android'");
			$this->set('sql2', $sql2);
			//$sql3 = $this->Fcusers->query("select count(*) as count3 from users_view where date(users_view.created) = '".$datenew."'");
			$user_query = "select count(*) as count3 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59'";
			//error_log("User View Query | Query=".$user_query."");
			$sql3 = $this->Fcusers->query("select count(*) as count3 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59'");
		
			error_log("User View Query | Query=".$user_query." | countrecord =".$sql3[0][0]['count3']."");
			$this->set('sql3', $sql3);
			
			//$sql4 = $this->Fcusercpcontentview->query("select count(*) as count4 from user_cp_content_view where date(created) = '".$datenew."' and operating_system = 'wap'");
			
			//echo "select count(*) as count4 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'wap' and time_of_play >= 300 ";
			//die;
			$sql4 = $this->Fcusercpcontentview->query("select count(*) as count4 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'wap' and (time_of_play >= 30 or is_full=1)");
			$this->set('sql4', $sql4);
			
			//echo "select count(*) as count5 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'android' and time_of_play >= 300";
			//die;
			
			$sql5 = $this->Fcusercpcontentview->query("select count(*) as count5 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'android' and (time_of_play >= 30 or is_full=1)");
			$this->set('sql5', $sql5);
			
			//echo "select count(*) as count6 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'wap' and is_full=1";
			//die;
			
			$sql6 = $this->Fcusercpcontentviewfull->query("select count(*) as count6 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'wap' and is_full=1");
			$this->set('sql6', $sql6);
			
			$sql7 = $this->Fcusercpcontentviewfull->query("select count(*) as count7 from users_view where created >= '".$datenew." 00:00:00' and created <= '".$datenew." 23:59:59' and operating_system = 'android' and is_full=1");
			$this->set('sql7', $sql7);
			
			$sql8 = $this->Fcuserlogin->query("select count(*) as count8 from users INNER JOIN user_profile on users.user_id = user_profile.id and users.created >= '".$datenew." 00:00:00' and users.created <= '".$datenew." 23:59:59' and  user_profile.operating_system = 'wap'");
			$this->set('sql8', $sql8);
			
			$sql9 = $this->Fcuserlogin->query("select count(*) as count9 from users INNER JOIN user_profile on users.user_id = user_profile.id and users.created >= '".$datenew." 00:00:00' and users.created <= '".$datenew." 23:59:59' and  user_profile.operating_system = 'android'");
			$this->set('sql9', $sql9);
			
					
		}
	}
	
	public function dailyreport(){
		$resArr = "";
		$operator = $this->Operator->query("select * from cm_operators as Operator");
		$this->set('operator', $operator);
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		
		if((!empty($_GET['biller_id'])) && (!empty($_GET['publisher'])) && (!empty($_GET['startDate'])))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			
			//$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher']; 
			$arr=array();
			
			//Check condition to connect 77 bhopuri_conpor for vodafone and idea...
			//22-06-2016....
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmywap")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="miniwap"))){
				
				$pack = $this->Bppack77->query("select product_id from bp_packs where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' group by product_id");
				
				$packRec = array();
				foreach($pack as $keyPack=>$valPack){
					$packRec[] = $valPack["bp_packs"]["product_id"];
				}
				$packRecImp = implode(",", $packRec);
				
				
				//echo "select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'";
				//die;
				
				//data for user hit...
				$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
				
				foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
				}	
				
				//data for cgok...
				if($_GET['biller_id'] == 'vf'|| $_GET['biller_id'] == 'ia'){ 
					$str= " and cg_response_id!='0'";
				} else {
					$str='';
				}
				
				$cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str ");
				
				foreach($cgok as $key=>$val)
				{	
					$resArr[$date]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				
				$billing_done = $this->Transaction77->query("select count(*) as count from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE'");
				foreach($billing_done as $key=>$val)
				{
					$resArr[$date]['billing_done']=$val[0]['count'];
				}	
				
				//data for total revenue...
				$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
				foreach($total_revenue as $key=>$val)
				{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
				}
					
				//data for callback...
				$callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and callback_status='1' ");
				foreach($callback_status as $key=>$val)
				{
					$resArr[$date]['callback_status']=$val[0]['count'];
				}
				
				//data for find total revenue, parking, deactivate for vodafone...
				if($_GET['biller_id']=='vf')
				{
					$callback=$this->Vodafonecallback77->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp') and isblock=0 group by charging_mode,TXNID");
					
					$actual_price_deduct=0; 
					$count=0;
						
					foreach($callback as $key=>$val)
					{
						$actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
						$count++;
					}
					//echo '<pre>'; print_r($resArr);die;
					$resArr[$date]['count_revenue']=$count;
					$resArr[$date]['callback_revenue'] = $actual_price_deduct;
					
					$parking=$this->Vodafonecallback77->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp') and isblock=0   group by charging_mode,TXNID,DATE(CREATED)");
					
					$count=0; 
					$null=0; 
					$suspend=0;
					foreach($parking as $key=>$val)
					{
						if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
						{
						$count++;
						}
						if($val['vodafone_callback_notifications']['charging_mode']=='null')
						{
						  $null++;
						}
						 if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
						{
						  $suspend++;
						}
					}
					'Count: '.$count;
					$deactivate=$null+$suspend;'deactivate: '.$deactivate;
					$resArr[$date]['parking']=$count;
					$resArr[$date]['deactivate'] = $deactivate;
					
					$update=$this->Vodafoneupdate77->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp')");
					
					$sum1=$update['0']['0']['sum1'];
					$sum=$update['0']['0']['count'];
					$resArr[$date]['renewalTotal_revenue']=$sum1;
					$resArr[$date]['Recount'] = $sum;
				}
				//data for find total revenue, parking, deactivate for idea...
				if($_GET['biller_id']=='ia' && $_GET['publisher']=='filmy')
				{
					$totalrenewal = $this->Ideacallback77->query("SELECT count( * ) as count , sum( price ) as sum ,date(created) as date, ACTION , `status` FROM  bp_idea_callback_notifications as idea WHERE date(created) = '$date' AND STATUS = 'SUCCESS' AND FIND_IN_SET (srvkey, '$packRecImp') GROUP BY ACTION");	
					foreach($totalrenewal as $key=>$val){
						if($val['idea']['ACTION']=='ACT'){
							$resArr[$val[0]['date']]['count_revenue']=$val[0]['count'];
							$resArr[$val[0]['date']]['callback_revenue']=$val[0]['sum'];		
						}
						
						if($val['idea']['ACTION']=='DCT'){
							$resArr[$val[0]['date']]['deactivate']=$val[0]['count'];
							//$data['dactivationsum']=$val[0]['sum'];		
						}
						
						
						if($val['idea']['ACTION']=='REN'){
							$resArr[$val[0]['date']]['Recount']=$val[0]['count'];
							$resArr[$val[0]['date']]['renewalTotal_revenue']=$val[0]['sum'];		
						}
						
						$parking = $this->Ideacallback77->query("SELECT count(*) as count FROM  bp_idea_callback_notifications as idea WHERE date(created) = '$date' AND action ='GRACE' and  STATUS = 'BAL-LOW' AND FIND_IN_SET (srvkey, '$packRecImp')");
						$resArr[$date]['parking']=$parking[0][0]["count"];
						
					}
				}
				
			}
			
			//Check condition to connect 99 bhopuri_conpor for airtel...
			if((($_GET['biller_id'] =="at") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="filmywap")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics"))){
				
				if($_GET['publisher'] =="bnama"){
					$pack = $this->Bppack99bhojpuri->query("select product_id from bp_packs where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' group by product_id");
					$packRec = array();
					foreach($pack as $keyPack=>$valPack){
						$packRec[] = $valPack["bp_packs"]["product_id"];
					}
					$packRecImp = implode(",", $packRec);
				} else if($_GET['publisher'] =="atcomics"){
					$pack = $this->Bppack99comics->query("select tata_product_id from packs where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' group by tata_product_id");
					$packRec = array();
					foreach($pack as $keyPack=>$valPack){
						$packRec[] = $valPack["packs"]["tata_product_id"];
					}
					$packRecImp = implode(",", $packRec);
					
				}
				
				
				//data for total hits...
				$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
				foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
					//echo '<pre>'; print_r($resArr);
				}
				
				//data for CG ok...
				if($_GET['biller_id'] == 'at'){ 
					$str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
				} else {
					$str='';
				}
				
				$cgok = $this->Transaction99bhojpuri->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str");
				
				foreach($cgok as $key=>$val)
				{	
					$resArr[$date]['cg_ok']=$val[0]['count'];
					$resArr[$date]['date']=$date;
				}
				
				//data for billing done...
				
				$billing_done=$this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 ");
				foreach($billing_done as $key=>$val)
				{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
					$resArr[$date]['billing_done']=$val[0]['count'];
					$resArr[$date]['date']=$date;
				}
				
				//data for parking...
				
				$parking=$this->Transaction99bhojpuri->query("select count(*) as count from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted='0' ");
				foreach($parking as $key=>$val)
				{
					$resArr[$date]['count_sub2']=$val[0]['count'];
					$resArr[$date]['date']=$date;
				}
				
				//data for callback status...
				
				$callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0");
				foreach($callback_status as $key=>$val)
				{
					$resArr[$date]['callback_status']=$val[0]['count'];
					$resArr[$date]['date']=$date; 
				}
				
				//data for find total revenue, parking, deactivate for airtel...
				if($_GET['biller_id']=='at'){
					if($publisher=='bnama')
					{
						
						$countrevenue=$this->Billingnotification99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and FIND_IN_SET (product_id, '$packRecImp')");
					}
					else
					{
						$countrevenue=$this->Billingnotification99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and FIND_IN_SET (product_id, '$packRecImp')"); 
					}
					$sum=0;
					foreach($countrevenue as $key=>$val)
					{
						$sum=$val[0]['activation_sum'];
						$resArr[$date]['total_revenue_ac']=0;
						$resArr[$date]['count_revenue_ac']=$val[0]['activation_count'];
						
						if(!empty($sum)){
							$resArr[$date]['total_revenue_ac']=$sum;
						}
								
					}
					
					if($publisher=='bnama')
					{
						$airtel_renewal=$this->Billingnotification99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."' and message='Success' and FIND_IN_SET (product_id, '$packRecImp')");
					} else {
						$airtel_renewal=$this->Billingnotification99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."' and message='Success' and FIND_IN_SET (product_id, '$packRecImp')");
					}
					$sum=0;
					foreach($airtel_renewal as $key=>$val)
					{
						$sum=$val[0]['renewal_sum'];
						$resArr[$date]['total_renewal_ac']=0;
						$resArr[$date]['count_renewal_ac']=$val[0]['renewal_count'];
						if(!empty($sum))
						{
							$resArr[$date]['total_renewal_ac']=$val[0]['renewal_sum'];
						}
					}
					
					if($publisher=='bnama')
					{
						$deactivateat=$this->Billingnotification99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."'  and response_code='1001' and FIND_IN_SET (product_id, '$packRecImp')");
					} else {
						
						$deactivateat=$this->Billingnotification99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."'  and response_code='1001'  and FIND_IN_SET (product_id, '$packRecImp')");
					}
					foreach($deactivateat as $key=>$val)
					{
						$resArr[$date]['count_unsub']=$val[0]['count'];
					}
					
				
				}

			}
			
			//Check condition to connect 99 v3mobi for tata...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons"))){
				
				//data for total hits...
				
				$totalhits = $this->UserHit99v3mobi->query("select date(date) as date, count(*) as count from USERS_HIT where date>='".$stDate."' and date<='".$edDate."' group by date(date)");
				
				foreach($totalhits as $key=>$val)
				{
					$date=$val['0']['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					//$resArr[$date]['date']=$val[0]['date'];
					//echo '<pre>'; print_r($resArr);
				}
				
				//data for mobile not found...
				
				$mobile_notfound= $this->UserHit99v3mobi->query("select date(date) as date, COUNT(*) as count from USERS_HIT where publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and mobile=''");
          
				foreach($mobile_notfound as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['mobile_notfound']=$val[0]['count'];
					//$resArr[$date]['date']=$val[0]['date'];
					//array_push($resArr,$resArr[$date);
				}
				
				//data for CG ok...
				
				$cgok = $this->Transaction99v3mobi->query("select date(transaction_time) as date, COUNT(id) as count from transactions where publisher='".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and ccg_response_id !=0");
				
				foreach($cgok as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['cgok']=$val[0]['count'];
				}
				
				//data for billing done...
							
				$billing_done = $this->Transaction99v3mobi->query("select date(transaction_time) as date, COUNT(*) as billing_done,
                SUM(final_price_deducted)/100 as total_revenue from transactions where publisher='".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE'");
				
				foreach($billing_done as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['billing_done']=$val[0]['billing_done'];
					$resArr[$date]['total_revenue']=$val[0]['total_revenue'];
					
				}
				
				//data for callback status...
				
				$callback_status = $this->Transaction99v3mobi->query("select date(transaction_time) as date,COUNT(*) as count from transactions where publisher='".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and callback_status='1'");
				
				foreach($callback_status as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['callback_status']=$val[0]['count'];
				} 
				
				//data for subscriber and total revenue...
				
				$logData = $this->Tatalog99v3mobi->query("select date(created) as date,COUNT(*) as count, SUM(PRICE)/100 as total_log_revenue from tata_subscription_logs where publisher='".$publisher."' and created>='".$stDate."' and created<='".$edDate."' and status='SUCCESS' and SERVICE='SUBSCRIPTION'");
				
				foreach($logData as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['total_subscribe_user']=$val[0]['count'];
					$resArr[$date]['total_subscribe_revenue']=$val[0]['total_log_revenue'];
				}
				
				//data for unsubscriber ...
				
				$unsubscribe_detail = $this->Tatalog99v3mobi->query("select date(created) as date,COUNT(*) as count from tata_subscription_logs where publisher='".$publisher."' and created>='".$stDate."' and created<='".$edDate."' and status='SUCCESS' and SERVICE='UNSUBSCRIPTION' ");
				
				foreach($unsubscribe_detail as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['unsubscribe_detail']=$val[0]['count'];
				}
				
				//data for renewal count...
				
				$renewal_detail = $this->Tatalog99v3mobi->query("select date(created) as date, COUNT(*) as count, SUM(PRICE)/100 as total_log_renewal from tata_subscription_logs where publisher='".$publisher."' and created>='".$stDate."' and created<='".$edDate."' and status='SUCCESS' and SERVICE='AUTORENEWAL'");
				
				foreach($renewal_detail as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['total_renewal_count']=$val[0]['count'];
					$resArr[$date]['total_renewal_revenue']=$val[0]['total_log_renewal'];
				}
				
			}
			
			
			//Check condition to connect 99 v3mobi for tata...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmywap"))){
				
				$pack = $this->Bppack77->query("select product_id from bp_packs where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' group by product_id");
				
				$packRec = array();
				foreach($pack as $keyPack=>$valPack){
					$packRec[] = $valPack["bp_packs"]["product_id"];
				}
				$packRecImp = implode(",", $packRec);
				
				//data for user hit...
				$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
				
				foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
				}	
				
				$mobile_notfound= $this->UserHit77->query("select COUNT(*) as count from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and msisdn=''");
          
				foreach($mobile_notfound as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['mobile_notfound']=$val[0]['count'];
					//$resArr[$date]['date']=$val[0]['date'];
					//array_push($resArr,$resArr[$date);
				}
				
				//data for cgok...
				if($_GET['biller_id'] == 'tt'){ 
					$str= " and cg_response_id!='0'";
				} else {
					$str='';
				}
				
				$cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str ");
				
				foreach($cgok as $key=>$val)
				{	
					$resArr[$date]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				
				$billing_done = $this->Transaction77->query("select count(*) as count, sum(price_deducted) as sum  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE'");
				foreach($billing_done as $key=>$val)
				{
					$resArr[$date]['billing_done']=$val[0]['count'];
					$resArr[$date]['total_revenue']=$val[0]['sum'];
				}	
				
				//data for total revenue...
				/*$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
				foreach($total_revenue as $key=>$val)
				{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
				}
				*/	
				//data for callback...
				$callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and callback_status='1' ");
				foreach($callback_status as $key=>$val)
				{
					$resArr[$date]['callback_status']=$val[0]['count'];
				}
				
				//data for subscriber and total revenue...
				
				$logData = $this->Tatalog99v3mobi->query("select date(created) as date,COUNT(*) as count, SUM(PRICE)/100 as total_log_revenue from tata_subscription_logs where publisher='".$publisher."' and created>='".$stDate."' and created<='".$edDate."' and status='SUCCESS' and SERVICE='SUBSCRIPTION'");
				
				foreach($logData as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['total_subscribe_user']=$val[0]['count'];
					$resArr[$date]['total_subscribe_revenue']=$val[0]['total_log_revenue'];
				}
				
				//data for unsubscriber ...
				
				$unsubscribe_detail = $this->Tatalog99v3mobi->query("select date(created) as date,COUNT(*) as count from tata_subscription_logs where publisher='".$publisher."' and created>='".$stDate."' and created<='".$edDate."' and status='SUCCESS' and SERVICE='UNSUBSCRIPTION' ");
				
				foreach($unsubscribe_detail as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['unsubscribe_detail']=$val[0]['count'];
				}
				
				//data for renewal count...
				
				$renewal_detail = $this->Tatalog99v3mobi->query("select date(created) as date, COUNT(*) as count, SUM(PRICE)/100 as total_log_renewal from tata_subscription_logs where publisher='".$publisher."' and created>='".$stDate."' and created<='".$edDate."' and status='SUCCESS' and SERVICE='AUTORENEWAL'");
				
				foreach($renewal_detail as $key=>$val){
					//$date=$val['0']['date'];
					$resArr[$date]['total_renewal_count']=$val[0]['count'];
					$resArr[$date]['total_renewal_revenue']=$val[0]['total_log_renewal'];
				}
				
				
			} 

			//Check condition to connect 99 v3mobi for tata...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="vfcomics"))){
				
				$pack = $this->Bppack99comics->query("select tata_product_id from packs where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' group by tata_product_id");
					$packRec = array();
					foreach($pack as $keyPack=>$valPack){
						$packRec[] = $valPack["packs"]["tata_product_id"];
					}
					$packRecImp = implode(",", $packRec);
				
				//data for user hit...
				
				$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count, date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
				
				foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
				}	
				 
				//data for cgok...
				if($_GET['biller_id'] == 'vf'|| $_GET['biller_id'] == 'ia'){ 
					$str= " and cg_response_id!='0'";
				} else {
					$str='';
				}
				
				$cgok = $this->Transaction99bhojpuri->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str ");
				
				foreach($cgok as $key=>$val)
				{	
					$resArr[$date]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				
				$billing_done = $this->Transaction99bhojpuri->query("select count(*) as count from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE'");
				foreach($billing_done as $key=>$val)
				{
					$resArr[$date]['billing_done']=$val[0]['count'];
				}	
				
				//data for total revenue...
				$total_revenue = $this->Transaction99bhojpuri->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
				foreach($total_revenue as $key=>$val)
				{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
				}
					
				//data for callback...
				$callback_status=$this->Transaction99bhojpuri->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and callback_status='1' ");
				foreach($callback_status as $key=>$val)
				{
					$resArr[$date]['callback_status']=$val[0]['count'];
				}
				
				//data for find total revenue, parking, deactivate for vodafone...
				if($_GET['biller_id']=='vf')
				{
					$callback=$this->Vodafonecallback99bhojpuri->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp') group by charging_mode,TXNID");
					
					$actual_price_deduct=0; 
					$count=0;
						
					foreach($callback as $key=>$val)
					{
						$actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
						$count++;
					}
					//echo '<pre>'; print_r($resArr);die;
					$resArr[$date]['count_revenue']=$count;
					$resArr[$date]['callback_revenue'] = $actual_price_deduct;
					
					$parking=$this->Vodafonecallback99bhojpuri->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp')  group by charging_mode,TXNID,DATE(CREATED)");
					
					$count=0; 
					$null=0; 
					$suspend=0;
					foreach($parking as $key=>$val)
					{
						if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
						{
						$count++;
						}
						if($val['vodafone_callback_notifications']['charging_mode']=='null')
						{
						  $null++;
						}
						 if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
						{
						  $suspend++;
						}
					}
					'Count: '.$count;
					$deactivate=$null+$suspend;'deactivate: '.$deactivate;
					$resArr[$date]['parking']=$count;
					$resArr[$date]['deactivate'] = $deactivate;
					
					$update=$this->Vodafoneupdate99bhojpuri->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp')");
					
					$sum1=$update['0']['0']['sum1'];
					$sum=$update['0']['0']['count'];
					$resArr[$date]['renewalTotal_revenue']=$sum1;
					$resArr[$date]['Recount'] = $sum;
				}
				
			}

			
			$this->set('resArr', $resArr);
			
		} else {
		
			$date = date('Y-m-d',strtotime('yesterday'));
			$_GET['startDate'] = date('Y-m-d',strtotime('yesterday'));
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = "bnama"; 
			$_GET["publisher"] = "bnama";
			$_GET["biller_id"] = "vf";
			$biller_id = "vf";			
			$arr=array();
			$pack = $this->Bppack77->query("select product_id from bp_packs where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' group by product_id");
				
				$packRec = array();
				foreach($pack as $keyPack=>$valPack){
					$packRec[] = $valPack["bp_packs"]["product_id"];
				}
				$packRecImp = implode(",", $packRec);
				
				
				//data for user hit...
				$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
				
				foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
				}	
				
				//data for cgok...
				if($_GET['biller_id'] == 'vf'|| $_GET['biller_id'] == 'ia'){ 
					$str= " and cg_response_id!='0'";
				} else {
					$str='';
				}
				
				$cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str ");
				
				foreach($cgok as $key=>$val)
				{	
					$resArr[$date]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				
				$billing_done = $this->Transaction77->query("select count(*) as count from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE'");
				foreach($billing_done as $key=>$val)
				{
					$resArr[$date]['billing_done']=$val[0]['count'];
				}	
				
				//data for total revenue...
				$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
				foreach($total_revenue as $key=>$val)
				{
					$resArr[$date]['total_revenue']=$val[0]['sum'];
				}
					
				//data for callback...
				$callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and callback_status='1' ");
				foreach($callback_status as $key=>$val)
				{
					$resArr[$date]['callback_status']=$val[0]['count'];
				}
				
				//data for find total revenue, parking, deactivate for vodafone...
				if($_GET['biller_id']=='vf')
				{
					$callback=$this->Vodafonecallback77->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp') group by charging_mode,TXNID");
					
					$actual_price_deduct=0; 
					$count=0;
						
					foreach($callback as $key=>$val)
					{
						$actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
						$count++;
					}
					//echo '<pre>'; print_r($resArr);die;
					$resArr[$date]['count_revenue']=$count;
					$resArr[$date]['callback_revenue'] = $actual_price_deduct;
					
					$parking=$this->Vodafonecallback77->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp')  group by charging_mode,TXNID,DATE(CREATED)");
					
					$count=0; 
					$null=0; 
					$suspend=0;
					foreach($parking as $key=>$val)
					{
						if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
						{
						$count++;
						}
						if($val['vodafone_callback_notifications']['charging_mode']=='null')
						{
						  $null++;
						}
						 if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
						{
						  $suspend++;
						}
					}
					'Count: '.$count;
					$deactivate=$null+$suspend;'deactivate: '.$deactivate;
					$resArr[$date]['parking']=$count;
					$resArr[$date]['deactivate'] = $deactivate;
					
					$update=$this->Vodafoneupdate77->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$stDate."' and CREATED<='".$edDate."' AND FIND_IN_SET (service_id, '$packRecImp')");
					
					$sum1=$update['0']['0']['sum1'];
					$sum=$update['0']['0']['count'];
					$resArr[$date]['renewalTotal_revenue']=$sum1;
					$resArr[$date]['Recount'] = $sum;
				}
			
			$this->set('resArr', $resArr);
		}
		
	}
	
	public function dailyreportdetail()
	{
		if((!empty($_GET['biller_id'])) && (!empty($_GET['publisher'])) && (!empty($_GET['startDate'])))
		{
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher']; 
			$resArr = array();
			//Check condition to connect 77 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmy"))){
				
				//data for total hits...
				$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' group by interface order by count(*) desc");
				foreach($totalhits as $key=>$val)
				{
					$date=$val[0]['date'];
					$interface=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['total_hit']=$val[0]['count'];
				}
				
				//data for cg ok...
				if($_GET['biller_id'] == 'vf' || $_GET['biller_id'] == 'ia'){ $str= " and cg_response_id!='0'";
				}elseif($_GET['biller_id'] == 'tt'){ $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
				}elseif($_GET['biller_id'] == 'ac' || $_GET['biller_id'] =='at' ||$_GET['biller_id'] =='rl'){ $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
				}else{$str='';}
				
				$cgok = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str group by interface ");
				foreach($cgok as $key=>$val)
				{	
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				$billing_done = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE'  group by interface");
				
				foreach($billing_done as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['billing_done']=$val[0]['count'];
				}
				
				//data for total revenue...
				$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE'  group by interface ");
				foreach($total_revenue as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
				}
				
				//data for mobile not found...
				$mobile_notfound=$this->UserHit77->query("select count(*) as count, date(date) as date,interface from bp_user_hits where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and msisdn='' group by interface");
				foreach($mobile_notfound as $key=>$val)
				{
					$interface=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
				}
				
				//data for callback status...
				$callback_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and callback_status='1'  group by interface");
			
				foreach($callback_status as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['callback_status']=$val[0]['count'];
				}
				
				//data for consent...
				$consent=$this->Transaction77->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and consent='1'  group by interface");
			
				foreach($consent as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['consent']=$val[0]['count'];
				} 
				
				//data for churn status...
				$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and churn_status='1'  group by interface");
			
				foreach($churn_status as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['churn_status']=$val[0]['count'];
				}
				
				//data for total revenue churn...
				$total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and churn_status='1'   group by interface ");
				
				foreach($total_revenue_churn as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
				}
			}
			
			//Check condition to connect 99 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="vfcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="bnama"))){
				//data for total hits...
				$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count, date(date) as date, interface from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' group by interface order by count(*) desc");
				foreach($totalhits as $key=>$val)
				{
					$date = $val[0]['date'];
					$interface=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['total_hit']=$val[0]['count'];
				}	
				
				//data for CG ok...
				if($_GET['biller_id'] == 'vf' ){ $str= " and cg_response_id!='0'";
				}elseif($_GET['biller_id'] == 'at'){ $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
				}elseif($_GET['biller_id'] == 'ac'){ $str= " and cg_response_id!='0' AND cg_response_id!='NULL'";
				}else{$str='';}
				
				 $cgok = $this->Transaction99bhojpuri->query("select count(*) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str group by interface ");
				
				foreach($cgok as $key=>$val)
				{	
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				$billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 group by interface");
				
				foreach($billing_done as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];	
					$resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
					$resArr[$date][$interface]['billing_done']=$val[0]['count'];
				}
				
				//data for mobile not found...
				
				$mobile_notfound=$this->UserHit99bhojpuri->query("select count(*) as count, interface from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and (msisdn='' OR msisdn='NA' OR msisdn='1234') group by interface");
				foreach($mobile_notfound as $key=>$val)
				{
					$interface=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
					$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
				}
				
				//data for call back status...
				$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0 group by interface");
				
				foreach($callback_status as $key=>$val)
				{
					$interface=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
					$resArr[$date][$interface]['callback_status']=$val[0]['count'];
				}

			}
			
			//Check condition to connect 99 v3mobi...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons"))){
				//data for total hits...
				
				$tatatotalhits = $this->UserHit99v3mobi->query("select count(*) as count,interface,packs.name,t.pack_id, date(date) as date from USERS_HIT t INNER JOIN packs on packs.id=t.pack_id and t.date>='".$stDate."' and t.date<='".$edDate."' and t.publisher='".$publisher."' group by interface order by count(*) desc");
				foreach($tatatotalhits as $key=>$val){
					$interface=$val['t']['interface'];
					$packid=$val['t']['pack_id'];
					$packname=$val['packs']['name'];
					$date = $val[0]['date'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['total_hit']=$val[0]['count'];
					$resArr[$date][$interface]['pack_name']=$packname;
					$resArr[$date][$interface]['pack_id']=$packid;
					
				}	
				
				//data for mobile not found...
				$tatamobile_notfound = $this->UserHit99v3mobi->query("select count(*) as count,interface from USERS_HIT where date>='".$stDate."' and date<='".$edDate."' and mobile='' and publisher='".$publisher."' group by interface");
				foreach($tatamobile_notfound as $key=>$val){
					$interface=$val['USERS_HIT']['interface'];
					$resArr[$date][$interface]['interface']=$val['USERS_HIT']['interface'];
					$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
				}		
				
				//data for CG ok...
				$tatacgok = $this->Transaction99v3mobi->query("select count(*) as count,interface from transactions where transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and ccg_response_id !='0' and publisher='".$publisher."' group by interface");
				foreach($tatacgok as $key=>$val){	
					$interface=$val['transactions']['interface'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				}
				
				//data for billing done...
				$tatabilling_done = $this->Transaction99v3mobi->query("select count(*) as count,sum(final_price_deducted)/100 as sum,interface from transactions where transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and publisher='".$publisher."' and publisher='".$publisher."' group by interface");
				foreach($tatabilling_done as $key=>$val){
					$interface=$val['transactions']['interface'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['billing_done']=$val[0]['count'];
					$resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
				}
				
				//data for Callback status...
				$tatacallback_status=$this->Transaction99v3mobi->query("select count(*) as count ,interface from transactions where  transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and callback_status='1' and publisher='".$publisher."' group by interface");
				foreach($tatacallback_status as $key=>$val){
					$interface=$val['transactions']['interface'];
					$resArr[$date][$interface]['interface']=$interface;
					$resArr[$date][$interface]['callback_status']=$val[0]['count'];
				}
				//data for consent report...
				$consent_report=$this->Transaction99v3mobi->query("select count(consent) as count ,interface from transactions  where  transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and consent='1' and publisher='".$publisher."' group by interface");
				foreach($consent_report as $key=>$val)
				{
					$interface=$val['transactions']['interface'];
					$resArr[$date][$interface]['consent_report']=$val[0]['count'];
				}
				
			}
			
			$this->set('resArr', $resArr);
			//$this->set('date1', $date1);
			
		} else {
			$this->Session->setFlash('Something wrong with url. Please try again.');
			$this->redirect('dailyreportrec');
		}
		
	}
	
	public function hourlyreport(){
		$resultSet = "";
		$input = array();
		$operator = $this->Operator->query("select * from cm_operators as Operator");
		$this->set('operator', $operator);
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		
		if((!empty($_GET['biller_id'])) && (!empty($_GET['publisher'])))
		{
			$biller_id = $_GET['biller_id'];
		    $service = $_GET['publisher'];
			
			date_default_timezone_set("Asia/Calcutta");
			$time = date("H:i:s");
			$Time_One_Hour = strtotime("-360 minutes", strtotime($time));
			$Time_Gap =date('H:i:s', $Time_One_Hour);
			
			//Check condition to connect 77 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmy"))){
			
				$getBetweenDate=$this->Transaction77->query("select Record_date from daily_report_newserver as get_date	where biller_id='".$biller_id."' and Record_date>= DATE_SUB( CURDATE( ) , INTERVAL 3  DAY ) order by Record_date ASC");
					
				$result=array();
				foreach($getBetweenDate as $res){
					array_push($result,$res['get_date']['Record_date']);
					$input=array_unique($result);
				}
				$resultSet=array();
				foreach($input as $back_date){
					$report = $this->Transaction77->query("select Record_date,Record_hour from daily_report_newserver where Record_date='".$back_date."' and 
					Record_hour>=  '".$Time_Gap."' and Record_hour <= '".$time."' and biller_id='".$biller_id."' and service_name='".$service."'
					group by biller_id,Record_hour, Record_date order by Record_hour desc");

					foreach($report as $result){
						$report1=$this->Transaction77->query("select sum(total_revenue) as total_revenue1,
						sum(subscription_count) as subscription_count1 ,sum(subscription_sum) as subscription_sum1,
						sum(renewal_count) as renewal_count1,
						sum(renewal_sum) as renewal_sum1 ,Record_date,
						Record_hour from daily_report_newserver where Record_date='".$result['daily_report_newserver']['Record_date']."' and 
						`Record_hour` >='00:00:00' and Record_hour<='".$result['daily_report_newserver']['Record_hour']."' and biller_id='".$biller_id."'
						and service_name='".$service."' group by biller_id,Record_date");

						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['subscription_count1']=$report1[0][0]['subscription_count1'];		
						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['renewal_sum1']=$report1[0][0]['renewal_sum1'];
						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['renewal_count1']=$report1[0][0]['renewal_count1'];
						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['subscription_sum1']=$report1[0][0]['subscription_sum1'];
					}
				}
			}
			
			//Check condition to connect 99 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="vfcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="bnama"))){
				
				if($_GET['publisher'] =="bnama"){
					$service = "Bhojpurinama";
				}
				
				$getBetweenDate=$this->Transaction99bhojpuri->query("select Record_date from daily_report as get_date where biller_id='".$biller_id."' and Record_date>= DATE_SUB( CURDATE( ) , INTERVAL 3  DAY ) order by Record_date ASC");
					
				$result=array();
				
				//print_r($getBetweenDate);
				foreach($getBetweenDate as $res){
					array_push($result,$res['get_date']['Record_date']);
					$input=array_unique($result);
				}
				
				$resultSet=array();
				foreach($input as $back_date){
				
					$report=$this->Transaction99bhojpuri->query("select Record_date,Record_hour from daily_report as daily_report_new where Record_date='".$back_date."' and 
					Record_hour>=  '".$Time_Gap."' and Record_hour <= '".$time."' and biller_id='".$biller_id."' and service_name='".$service."'
					group by biller_id,Record_hour, Record_date order by Record_hour desc");

					foreach($report as $result){
						$report1=$this->Transaction99bhojpuri->query("select sum(total_revenue) as total_revenue1,
						sum(subscription_count) as subscription_count1 ,sum(subscription_sum) as subscription_sum1,
						sum(renewal_count) as renewal_count1,
						sum(renewal_sum) as renewal_sum1 ,Record_date,
						Record_hour from daily_report as daily_report_new where Record_date='".$result['daily_report_new']['Record_date']."' and 
						`Record_hour` >='00:00:00' and Record_hour<='".$result['daily_report_new']['Record_hour']."' and biller_id='".$biller_id."'
						and service_name='".$service."' group by biller_id,Record_date");

						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['subscription_count1']=$report1[0][0]['subscription_count1'];		
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['renewal_sum1']=$report1[0][0]['renewal_sum1'];
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['renewal_count1']=$report1[0][0]['renewal_count1'];
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['subscription_sum1']=$report1[0][0]['subscription_sum1'];
					}
				}
				
			}
			
			//Check condition to connect 99 v3mobi...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons"))){
				
				$getBetweenDate=$this->Transaction99bhojpuri->query("select Record_date from daily_report_new as get_date	where biller_id='tt' and Record_date>= DATE_SUB( CURDATE( ) , INTERVAL 3  DAY ) order by Record_date ASC");
			
				$result=array();
				foreach($getBetweenDate as $res){
					array_push($result,$res['get_date']['Record_date']);
					$input=array_unique($result);
				}
				$resultSet=array();
				foreach($input as $back_date){
					$report=$this->Transaction99bhojpuri->query("select Record_date,Record_hour from daily_report_new where Record_date='".$back_date."' and Record_hour>=  '".$Time_Gap."' and Record_hour <= '".$time."' and biller_id='".$biller_id."'	group by biller_id,Record_hour, Record_date order by Record_hour desc");
					foreach($report as $result){
						$report1 = $this->Transaction99bhojpuri->query("select sum(total_revenue) as total_revenue1, 		sum(subscription_count) as subscription_count1 ,sum(subscription_sum) as subscription_sum1,	sum(renewal_count) as renewal_count1, sum(renewal_sum) as renewal_sum1 ,Record_date, Record_hour from daily_report_new where Record_date='".$result['daily_report_new']['Record_date']."' and `Record_hour` >='00:00:00' and Record_hour<='".$result['daily_report_new']['Record_hour']."' and biller_id='".$biller_id."' group by biller_id,Record_date");
						
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['subscription_count1']=$report1[0][0]['subscription_count1'];		
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['renewal_sum1']=$report1[0][0]['renewal_sum1'];
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['renewal_count1']=$report1[0][0]['renewal_count1'];
						$resultSet[$result['daily_report_new']['Record_hour']][$result['daily_report_new']['Record_date']]['subscription_sum1']=$report1[0][0]['subscription_sum1'];
					}
				}
				
			}
			//print_r($resultSet);
			$this->set('newHourReport',$resultSet);
			$this->set('date',$input);
		} else {
			
			$biller_id = "vf";
		    $_GET['biller_id']= "vf";
			$_GET['publisher'] = "bnama";
		    $service = "bnama";
			
			date_default_timezone_set("Asia/Calcutta");
			$time = date("H:i:s");
			$Time_One_Hour = strtotime("-360 minutes", strtotime($time));
			$Time_Gap =date('H:i:s', $Time_One_Hour);
			
				$getBetweenDate=$this->Transaction77->query("select Record_date from daily_report_newserver as get_date	where biller_id='".$biller_id."' and Record_date>= DATE_SUB( CURDATE( ) , INTERVAL 3  DAY ) order by Record_date ASC");
					
				$result=array();
				foreach($getBetweenDate as $res){
					array_push($result,$res['get_date']['Record_date']);
					$input=array_unique($result);
				}
				$resultSet=array();
				foreach(@$input as $back_date){
					$report = $this->Transaction77->query("select Record_date,Record_hour from daily_report_newserver where Record_date='".$back_date."' and 
					Record_hour>=  '".$Time_Gap."' and Record_hour <= '".$time."' and biller_id='".$biller_id."' and service_name='".$service."'
					group by biller_id,Record_hour, Record_date order by Record_hour desc");

					foreach($report as $result){
						$report1=$this->Transaction77->query("select sum(total_revenue) as total_revenue1,
						sum(subscription_count) as subscription_count1 ,sum(subscription_sum) as subscription_sum1,
						sum(renewal_count) as renewal_count1,
						sum(renewal_sum) as renewal_sum1 ,Record_date,
						Record_hour from daily_report_newserver where Record_date='".$result['daily_report_newserver']['Record_date']."' and 
						`Record_hour` >='00:00:00' and Record_hour<='".$result['daily_report_newserver']['Record_hour']."' and biller_id='".$biller_id."'
						and service_name='".$service."' group by biller_id,Record_date");

						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['subscription_count1']=$report1[0][0]['subscription_count1'];		
						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['renewal_sum1']=$report1[0][0]['renewal_sum1'];
						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['renewal_count1']=$report1[0][0]['renewal_count1'];
						$resultSet[$result['daily_report_newserver']['Record_hour']][$result['daily_report_newserver']['Record_date']]['subscription_sum1']=$report1[0][0]['subscription_sum1'];
					}
				}
			$this->set('newHourReport',$resultSet);
			$this->set('date',$input);	
			
		}	
		
	}
	
	public function contentusesreport($page=0){
		$pageNum=0;
		$NumofPage=0;
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate']." 00:00:00";
		} else {
			$startDate = date('Y-m-d',strtotime('yesterday'))." 00:00:00";
			$_GET['startDate'] = date('Y-m-d',strtotime('yesterday'));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate']." 23:59:59";
		} else {
			$endDate = date('Y-m-d',strtotime('yesterday'))." 23:59:59";
			$_GET['endDate'] = date('Y-m-d',strtotime('yesterday'));
		}
		
		if(!empty($_GET['biller_id'])){
			$biller_id = $_GET['biller_id'];
		} else {
			$biller_id = "vf";
			$_GET['biller_id'] = "vf";
		}
		
		if(!empty($_GET['publisher'])){
			$biller_id = $_GET['biller_id'];
		} else {
			$publisher = "bnama";
			$_GET['publisher'] = "bnama";
		}
		
		
		$operator = $this->Operator->query("select * from cm_operators as Operator");
		$this->set('operator', $operator);
		
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		//if((!empty($_GET['biller_id'])) && (!empty($_GET['publisher']))){
		//Check condition to connect 77 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmy"))){
			
			//echo "select msisdn, product_id , subscription_date, unsubscription_date, renewal_date, total_revenue, renewal_cycle, download_limit, download_count from bp_subscribed_users_details as Subscribeuser  where  DATE(subscription_date) >= '".$startDate."' and DATE(subscription_date) <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' limit $offset,$RowofPerpage";
			
			
			$downloadsRow = $this->Subscribeuser77->query("select msisdn, product_id , subscription_date, unsubscription_date, renewal_date, total_revenue, renewal_cycle, download_limit, download_count from bp_subscribed_users_details as Subscribeuser  where  subscription_date >= '".$startDate."' and subscription_date <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' limit $offSet, $recPerPage");
			
			
			
			//$downloadsRow = $this->Subscribeuser77->query("select msisdn, product_id , subscription_date, unsubscription_date, renewal_date, total_revenue, renewal_cycle, download_limit, download_count from bp_subscribed_users_details as Subscribeuser  where  DATE(subscription_date) >= '".$startDate."' and DATE(subscription_date) <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' limit $offSet, $recPerPage");
			
			$cntRec = $this->Subscribeuser77->query("select count(detail_id) as count from bp_subscribed_users_details as Subscribeuser  where  subscription_date >= '".$startDate."' and subscription_date <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' ");
			
			//$cntRec = $this->Subscribeuser77->query("select count(detail_id) as count from bp_subscribed_users_details as Subscribeuser  where  DATE(subscription_date) >= '".$startDate."' and DATE(subscription_date) <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' ");
		}
		
		//Check condition to connect 99 bhopuri_conpor...
		if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="vfcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="bnama")))
		{
			
			$downloadsRow = $this->Subscribeuser99bhojpuri->query("select msisdn, product_id , subscription_date, unsubscription_date, renewal_date, total_revenue, renewal_cycle, download_limit, download_count from bp_subscribed_users_details as Subscribeuser  where  subscription_date >= '".$startDate."' and subscription_date <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' limit $offSet, $recPerPage");
			
			//$downloadsRow = $this->Subscribeuser99bhojpuri->query("select msisdn, product_id , subscription_date, unsubscription_date, renewal_date, total_revenue, renewal_cycle, download_limit, download_count from bp_subscribed_users_details as Subscribeuser  where  DATE(subscription_date) >= '".$startDate."' and DATE(subscription_date) <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' limit $offSet, $recPerPage");
			
			//$cntRec = $this->Subscribeuser99bhojpuri->query("select count(detail_id) as count from bp_subscribed_users_details as Subscribeuser  where  DATE(subscription_date) >= '".$startDate."' and DATE(subscription_date) <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."'");
			$cntRec = $this->Subscribeuser99bhojpuri->query("select count(detail_id) as count from bp_subscribed_users_details as Subscribeuser  where subscription_date >= '".$startDate."' and subscription_date <= '".$endDate."' and biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."'");
			
		}
		//Check condition to connect 99 v3mobi...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons"))){
			
						
			$downloadsRow = $this->Subscribeuser99v3mobi->query("select msisdn, product_id , subscription_date, unsubscription_date, renewal_date, total_revenue, renewal_cycle from subscribed_users_packs as Subscribeuser  where  subscription_date >= '".$startDate."' and subscription_date <= '".$endDate."' limit $offSet, $recPerPage");
			
			
			$cntRec = $this->Subscribeuser99v3mobi->query("select count(id) as count from subscribed_users_packs as Subscribeuser  where  subscription_date >= '".$startDate."' and subscription_date <= '".$endDate."'");
			
		   }	
		//}
		
		if(!empty($cntRec)){
			$num = $cntRec[0][0]['count'];
			$NumofPage=ceil($num/$recPerPage);
		} else {
			$num = 0;
			$NumofPage= 0;
			$downloadsRow="";
			
		}
		
		$this->set('downloadsRow', $downloadsRow);
		$this->set('numOfPage', $NumofPage);
		$this->set('pageNum', $pageNum);
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		
	}
	
	public function tatarevenuereport(){
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(($this->Session->read('User.user_type') =="CPA")){
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
					
				} else{
				
					$userid = $this->User->query("select id from cm_users where id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
			}	
		} else {
			
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
				
				} else {
					$userid = $this->User->query("select id from cm_users where parent_id='".$_GET['user_id']."' or id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
			}	
		}	
		$arr = array();
		foreach($userid as $key=>$val){
			
			$arr[] = $val["cm_users"]["id"];
		}
		//print_r($arr);
		
		$finalUserArr = implode(",", $arr);
			
		
		
		//$userId = $this->Session->read('User.id');
		
			
		//$tatareport = $this->Cprevenue99v3mobi->query("select * from cp_revenue_detail where cp_id='$userId' and type='S' AND operator='tata' AND date>='".$startDate."'  AND  date<='".$endDate."'");
	
		$tatareport = $this->Cprevenue99v3mobi->query("select * from cp_revenue_detail where FIND_IN_SET (cp_id, '$finalUserArr') and type='S' AND operator='tata' AND date>='".$startDate."'  AND  date<='".$endDate."' order by renewal_count desc");
		//echo "<pre>";
		//print_r($tatareport);
		//die;
		
		$this->set('tatareport',$tatareport);
		
		//find user for drop downlist...
		if($this->Session->read('User.user_type') == "CPA"){
			$userRec = $this->User->query("select * from cm_users as User where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
		} else {
			$userRec = $this->User->query("select * from cm_users as User where parent_id='0' and user_type='CPA'");
		}
		
		$this->set('userRec', $userRec);
		
	}
	
	public function vodafonerevenuereport(){
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		if(($this->Session->read('User.user_type') =="CPA")){
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
					
				} else{
				
					$userid = $this->User->query("select id from cm_users where id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
			}	
		} else {
			
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
				
				} else {
					$userid = $this->User->query("select id from cm_users where parent_id='".$_GET['user_id']."' or id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
			}	
		}	
		$arr = array();
		foreach($userid as $key=>$val){
			
			$arr[] = $val["cm_users"]["id"];
		}
		//print_r($arr);
		
		$finalUserArr = implode(",", $arr);
		
			
		$vodafonereport = $this->Cprevenue99v3mobi->query("select * from cp_revenue_detail where FIND_IN_SET (cp_id, '$finalUserArr')  and type='S' AND operator='vf' AND date>='".$startDate."'  AND  date<='".$endDate."'");
		$this->set('vodafonereport', $vodafonereport);
		
		//find user for drop downlist...
		if($this->Session->read('User.user_type') == "CPA"){
			$userRec = $this->User->query("select * from cm_users as User where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
		} else {
			$userRec = $this->User->query("select * from cm_users as User where parent_id='0' and user_type='CPA'");
		}
		
		$this->set('userRec', $userRec);
	}
	
	public function idearevenuereport(){
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		if(($this->Session->read('User.user_type') =="CPA")){
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
					
				} else{
				
					$userid = $this->User->query("select id from cm_users where id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
			}	
		} else {
			
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
				
				} else {
					$userid = $this->User->query("select id from cm_users where parent_id='".$_GET['user_id']."' or id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
			}	
		}	
		$arr = array();
		foreach($userid as $key=>$val){
			
			$arr[] = $val["cm_users"]["id"];
		}
		//print_r($arr);
		
		$finalUserArr = implode(",", $arr);
		
		$ideareport = $this->Cprevenue99v3mobi->query("select * from cp_revenue_detail where FIND_IN_SET (cp_id, '$finalUserArr') and type='S' AND operator='ia' AND date>='".$startDate."'  AND  date<='".$endDate."'");
		$this->set('ideareport', $ideareport);
		
		
		//find user for drop downlist...
		if($this->Session->read('User.user_type') == "CPA"){
			$userRec = $this->User->query("select * from cm_users as User where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
		} else {
			$userRec = $this->User->query("select * from cm_users as User where parent_id='0' and user_type='CPA'");
		}
		
		$this->set('userRec', $userRec);
		
	}
	
	public function airtelrevenuereport(){
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		if(($this->Session->read('User.user_type') =="CPA")){
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
					
				} else{
				
					$userid = $this->User->query("select id from cm_users where id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
			}	
		} else {
			
			if(!empty($_GET['user_id'])){
				if($_GET['user_id'] == "all"){
					$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
				
				} else {
					$userid = $this->User->query("select id from cm_users where parent_id='".$_GET['user_id']."' or id='".$_GET['user_id']."'");
				}
			} else {
				$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
			}	
		}	
		$arr = array();
		foreach($userid as $key=>$val){
			
			$arr[] = $val["cm_users"]["id"];
		}
		//print_r($arr);
		
		$finalUserArr = implode(",", $arr);
		
			
		$airtelreport = $this->Cprevenue99v3mobiairtel->query("select * from cp_revenue_detail_airtel as cp_revenue_detail where FIND_IN_SET (cp_id, '$finalUserArr')  and type='S' AND operator='at' AND date>='".$startDate."'  AND  date<='".$endDate."'");
		
		$this->set('airtelreport', $airtelreport);
		
		//find user for drop downlist...
		if($this->Session->read('User.user_type') == "CPA"){
			$userRec = $this->User->query("select * from cm_users as User where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
		} else {
			$userRec = $this->User->query("select * from cm_users as User where parent_id='0' and user_type='CPA'");
		}
		
		$this->set('userRec', $userRec);
		
		
		
	}
	
	public function downloadreport($page=0){
		
		
		$downloadRec = array();
		$contentname = array();
		$countdownloadRec = array();
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...		
		$operator = $this->Operator->query("select * from cm_operators as Operator");
		$this->set('operator', $operator);
		
		$project = $this->Project->query("select * from cm_projects as Project where status='A' order by display_order");
		$this->set('project', $project);
		
		//find user for drop downlist...
		if($this->Session->read('User.user_type') == "CPA"){
			$userRec = $this->User->query("select * from cm_users as User where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
		} else {
			$userRec = $this->User->query("select * from cm_users as User where parent_id='0' and user_type='CPA'");
		}
		
		$this->set('userRec', $userRec);
		
		if((!empty($_GET['biller_id'])) && (!empty($_GET['publisher'])) && (!empty($_GET['startDate'])) && (!empty($_GET['endDate'])))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['endDate'].' 23:59:59';
			$publisher = $_GET['publisher']; 
			
			if(($this->Session->read('User.user_type') =="CPA")){
				if(!empty($_GET['user_id'])){
					if($_GET['user_id'] == "all"){
						$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
						
					} else{
					
						$userid = $this->User->query("select id from cm_users where id='".$_GET['user_id']."'");
					}
				} else {
					$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
				}	
			} else {
				
				if(!empty($_GET['user_id'])){
					if($_GET['user_id'] == "all"){
						$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
					
					} else {
						$userid = $this->User->query("select id from cm_users where parent_id='".$_GET['user_id']."' or id='".$_GET['user_id']."'");
					}
				} else {
					$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
				}	
			}	
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			//print_r($arr);
			
			$finalUserArr = implode(",", $arr);
			$searchCond[]="FIND_IN_SET (t2.user_id, '$finalUserArr')";
			
			$searchCondStr=implode(" AND ",$searchCond);
			
			//select content id's behalf of userid... 
			//echo "select content_id, name from cm_contents where user_id in($finalUserArr)";//
			$contents = $this->Fcappcontent->query("select content_id, name from cm_contents where user_id in($finalUserArr)");
			
			//echo "<pre>";
			//echo $len=sizeof($contents);
			//print_r($contents);
			// die;
			
			
		    //Check condition to connect 77 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmy"))){
				
				$contentids =  array();
				foreach($contents as $key=>$val){
					$contentids[] = $val["cm_contents"]["content_id"];
					$contentname[$val["cm_contents"]["content_id"]] = $val["cm_contents"]["name"];
				}
				if(!empty($contentids)){
					$finalContentId = implode(",", $contentids);
					
					
					//$downloadRec = $this->BpDownload77->query("select t1.*, t2.name from bp_downloads as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id where t1.biller_id='".$_GET['biller_id']."' and t1.publisher='".$_GET['publisher']."' and date(t1.created_date) >= '".$stDate."' and date(t1.created_date) <= '".$edDate."' order by t1.download_id desc limit $offset,$RowofPerpage");
				
					//echo "select * from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId) order by download_id desc limit $offset,$RowofPerpage";
					
					
					$downloadRec = $this->BpDownload77->query("select content_id,download_status,count(subscribed_user_id) UserCount,count(distinct(subscribed_user_id)) UniqueUserCount, count(content_id) ContentCount,count(distinct(content_id)) UniqueContentCount from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId)  group by `content_id`,`download_status` order by download_id desc limit $offSet, $recPerPage");

					
					//$downloadRec = $this->BpDownload77->query("select * from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId) order by download_id desc limit $offset,$RowofPerpage");
					
					
					
					$countdownloadRec = $this->BpDownload77->query("select count(*) as count from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id`,`download_status` order by download_id desc");
					
					//$countdownloadRec = $this->BpDownload77->query("select count(*) as count from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId) order by download_id desc");
					
					
					//$downloadRec = $this->BpDownload77->query("select t1.content_id, t2.name from bp_downloads as t1 inner join cm_contents as t2 on t2.content_id=t1.content_id 	inner join bp_transaction as t3 on t3.subscribed_user_id=t1.subscribed_user_id inner join bp_adnetworks as t4 on t4.interface=t3.interface where t1.biller_id='".$_GET['biller_id']."' and t1.publisher='".$_GET['publisher']."' and date(t1.created_date) >= '".$stDate."' and date(t1.created_date) <= '".$edDate."'");
				}
				
				
				
			}
			
			//check for airtel comics(mtoons)...
			if(($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics")){
				
				$contentRec = $this->Content99comics->query("select id from contents as cm_contents where status=1");
				
				$contentids =  array();
				foreach($contents as $key=>$val){
					$contentids[] = $val["cm_contents"]["content_id"];
					$contentname[$val["cm_contents"]["content_id"]] = $val["cm_contents"]["name"];
				}
                if(!empty($contentids)){
					$finalContentId = implode(",", $contentids);
				}
			}
			
			
			
			//Check condition to connect 99 bhopuri_conpor...
			if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="vfcomics")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons"))){
				
				/*echo "select o.subscribed_users_id,o.content_id,o.status,count(o.subscribed_users_id) UserCount,count(distinct(o.subscribed_users_id)) 
UniqueUserCount, count(o.content_id) ContentCount,count(distinct(o.content_id)) UniqueContentCount from orders as o left join subscribed_users as s 
on o.subscribed_users_id=s.id
where s.biller_id='".$_GET['biller_id']."' and 
date(o.created) >= '".$stDate."' and date(o.created) <= '".$edDate."'
group by o.subscribed_users_id,o.content_id,o.status order by o.id desc limit 0,25"; die;*/
				$contentRec = $this->Content99comics->query("select id,title from contents as cm_contents where status=1 and interface='mobile'");
				$contentids =  array();
				
				foreach($contentRec as $key=>$val){
					
					$contentids[] = $val["cm_contents"]["id"];
					$contentname[$val["cm_contents"]["id"]] = $val["cm_contents"]["title"];
				}

				if(!empty($contentids)){
					$finalContentId = implode(",", $contentids);
					
					
					
					
					$downloadRec = $this->Content99comics->query("select Downloads.content_id,Downloads.status,count(Downloads.subscribed_users_id) UserCount,count(distinct(Downloads.subscribed_users_id)) 
	UniqueUserCount, count(Downloads.content_id) ContentCount,count(distinct(Downloads.content_id)) UniqueContentCount from orders as Downloads left join subscribed_users as s 
	on Downloads.subscribed_users_id=s.id
	where s.biller_id='".$_GET['biller_id']."' and 
	date(Downloads.created) >= '".$stDate."' and date(Downloads.created) <= '".$edDate."'
	group by Downloads.content_id,Downloads.status order by Downloads.id desc limit $offSet, $recPerPage");
					
					
					
					
					
			$countdownloadRec = $this->Content99comics->query("select count(Downloads.id) as count from orders as Downloads left join subscribed_users as s 
	on Downloads.subscribed_users_id=s.id
	where s.biller_id='".$_GET['biller_id']."' and 
	date(Downloads.created) >= '".$stDate."' and date(Downloads.created) <= '".$edDate."'
	group by Downloads.subscribed_users_id,Downloads.content_id,Downloads.status");
					}
				
			}
			//Check condition to connect 99 v3mobi...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama"))){
				
				$contentsTata = $this->BpDownload99v3mobi->query("select ID, METADATA_PROPERTY_VALUE from CMS_CONTENT_DETAILS as cm_contents where METADATA_PROPERTY_NAME='Name'");
				
				$contentids =  array();
				foreach($contentsTata as $key=>$val){
					$contentids[] = $val["cm_contents"]["ID"];
					$contentname[$val["cm_contents"]["ID"]] = $val["cm_contents"]["METADATA_PROPERTY_VALUE"];
				}
				if(!empty($contentids)){
					$finalContentId = implode(",", $contentids);
					
					$downloadRec = $this->BpDownload99v3mobi->query("select subscribed_user_id,content_id, count(subscribed_user_id) UserCount,count(distinct(subscribed_user_id)) UniqueUserCount, count(content_id) ContentCount,count(distinct(content_id)) UniqueContentCount from downloads as Downloads where date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id` order by id desc limit $offSet, $recPerPage");
					
					//$downloadRec = $this->BpDownload99v3mobi->query("select * from downloads as Downloads where  date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId) order by id desc limit $offset,$RowofPerpage");
					
					$countdownloadRec = $this->BpDownload99v3mobi->query("select count(*) as count from downloads as Downloads where date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id` order by id desc");
					
					//$countdownloadRec = $this->BpDownload99v3mobi->query("select count(*) as count from downloads as Downloads where  date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId) order by id desc");
				}
			}
			
			//Check condition to connect 77 server to download table...
			if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty"))){
				
				
				$contentids =  array();
				foreach($contents as $key=>$val){
					$contentids[] = $val["cm_contents"]["content_id"];
					$contentname[$val["cm_contents"]["content_id"]] = $val["cm_contents"]["name"];
				}
				
				if(!empty($contentids)){
					$finalContentId = implode(",", $contentids);
					
				
					
					$downloadRec = $this->BpDownload77->query("select subscribed_user_id,content_id, count(subscribed_user_id) UserCount,count(distinct(subscribed_user_id)) UniqueUserCount, count(content_id) ContentCount,count(distinct(content_id)) UniqueContentCount from downloads as Downloads where  publisher='".$_GET['publisher']."' and date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id` order by id desc limit $offSet, $recPerPage");
					
					//$downloadRec = $this->BpDownload77TATA->query("select * from downloads as Downloads where  date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId) order by id desc limit $offset,$RowofPerpage");
					
					//$countdownloadRec = $this->BpDownload77TATA->query("select count(*) as count from downloads as Downloads where  date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId) order by id desc");
					
					$countdownloadRec = $this->BpDownload77->query("select count(*) as count from downloads as Downloads where publisher='".$_GET['publisher']."' and date(created) >= '".$stDate."' and date(created) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id` order by id desc");
					
				}
				
			}
			
			$this->set('downloadRec', $downloadRec);
			$this->set('contentname', $contentname);
			
			
			if(!empty($countdownloadRec)){
				$num = $countdownloadRec[0][0]['count'];
			} else {
				$num=0;
			}
			$NumofPage=ceil($num/$recPerPage);
			$this->set('numOfPage', $NumofPage);
			$this->set('pageNum', $pageNum);
		
		
		
		} else {
			
			$date = date('Y-m-d',strtotime('yesterday'));
			
			$_GET['startDate'] = date('Y-m-d',strtotime('yesterday'));
			$_GET['endDate'] = date('Y-m-d',strtotime('yesterday'));
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate = $_GET['endDate'].' 23:59:59';
			$publisher = 'bnama';
			$_GET['biller_id'] = 'vf';
			$_GET['publisher'] = 'bnama';
			$_GET['user_id'] = "all";
			
			if(($this->Session->read('User.user_type') =="CPA")){
				if(!empty($_GET['user_id'])){
					if($_GET['user_id'] == "all"){
						$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
						
					} else{
					
						$userid = $this->User->query("select id from cm_users where id='".$_GET['user_id']."'");
					}
				} else {
					$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."' OR id = '".$this->Session->read('User.id')."'");
				}	
			} else {
				
				if(!empty($_GET['user_id'])){
					if($_GET['user_id'] == "all"){
						$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
					
					} else {
						$userid = $this->User->query("select id from cm_users where parent_id='".$_GET['user_id']."' or id='".$_GET['user_id']."'");
					}
				} else {
					$userid = $this->User->query("select id from cm_users where user_type='CPA' or user_type='CPU'");
				}	
			}	
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			//print_r($arr);
			
			$finalUserArr = implode(",", $arr);
			$searchCond[]="FIND_IN_SET (t2.user_id, '$finalUserArr')";
			
			$searchCondStr=implode(" AND ",$searchCond);
			
			//select content id's behalf of userid... 
			$contents = $this->Fcappcontent->query("select content_id, name from cm_contents where user_id in($finalUserArr)");
			
			$contentids = array();
			foreach($contents as $key=>$val){
				$contentids[] = $val["cm_contents"]["content_id"];
				$contentname[$val["cm_contents"]["content_id"]] = $val["cm_contents"]["name"];
			}
			if(!empty($contentids)){
				$finalContentId = @implode(",", $contentids);
				
				$downloadRec = $this->BpDownload77->query("select subscribed_user_id,content_id,download_status,count(subscribed_user_id) UserCount,count(distinct(subscribed_user_id)) UniqueUserCount, count(content_id) ContentCount,count(distinct(content_id)) UniqueContentCount from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id`,`download_status` order by download_id desc limit $offSet, $recPerPage");
				
				$countdownloadRec = $this->BpDownload77->query("select count(*) as count from bp_downloads as Downloads where biller_id='".$_GET['biller_id']."' and publisher='".$_GET['publisher']."' and date(created_date) >= '".$stDate."' and date(created_date) <= '".$edDate."' and content_id in($finalContentId)  group by `subscribed_user_id`,`content_id`,`download_status` order by download_id desc");
			}
			
			$this->set('downloadRec', $downloadRec);
			$this->set('contentname', $contentname);
			
			$num = @$countdownloadRec[0][0]['count'];
			$NumofPage=ceil($num/$recPerPage);
			$this->set('numOfPage', $NumofPage);
			$this->set('pageNum', $pageNum);

		}
		
	}
	
	function publishertrafficreport(){
		
		$countRec = array();
		$PageNum=1;
		$RowofPerpage=25;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$PageNum=$this->request->query['page'];
		}
		$offset=($PageNum - 1) * $RowofPerpage;
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['searchType'])){
			$searchType = $_GET['searchType'];
		} else {
			$searchType = "allContent";
		}
		
		
		if($searchType == "contentWise"){
			
			echo $query = "select count(t1.id) as countHits, t1.created as createdDate ,t4.name as contentName from publisher_content_view as t1 inner join cm_contents as t4 on t1.content_id=t4.content_id where date(t1.created)>='".$startDate."' and date(t1.created)<='".$endDate."' group by t1.content_id, t1.created order by t1.created desc limit $offset,$RowofPerpage";
			$recordArr = $this->Publishercontentview->query($query);
			$countRec = $this->Publishercontentview->query("select count(id) as count from publisher_content_view where date(created)>='".$startDate."' and date(created)<='".$endDate."'  group by content_id, created");
			
		} else {
			$query = "select count(id) as countHits, created from publisher_content_view where date(created)>='".$startDate."' and date(created)<='".$endDate."' group by date(created) order by date(created) desc limit $offset,$RowofPerpage";
			$recordArr = $this->Publishercontentview->query($query);
			
			$countRec = $this->Publishercontentview->query("select count(id) as count from publisher_content_view where date(created)>='".$startDate."' and date(created)<='".$endDate."' group by date(created)");
		}	

        $this->set('recordArr', $recordArr);
		if(!empty($countRec)){
			$num = $countRec[0][0]['count'];
		} else {
			$num = 0;
		}
		$NumofPage=ceil($num/$RowofPerpage);
		$this->set('NumofPage', $NumofPage);
		$this->set('PageNum', $PageNum);
		$this->set('num', $num);
		
	}
	
	function publisherpurchasetrafficreport(){
		
		$countRec= array();
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		
		if(!empty($_GET['startDate'])){
			$startDate = $_GET['startDate'];
		} else {
			$startDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		if(!empty($_GET['endDate'])){
			$endDate = $_GET['endDate'];
		} else {
			$endDate = date('Y-m-d', strtotime("-1 day"));
		}
		
		$query = "select count(t1.id) as countHits ,max(t1.created) as lastHit, t3.created as purchaseDate, t4.name as contentName from publisher_content_view as t1 inner join publisher_order_line as t2 on t1.content_id=t2.content_id inner join publisher_order as t3 on t2.order_id=t3.id inner join cm_contents as t4 on t1.content_id=t4.content_id where date(t1.created)>='".$startDate."' and date(t1.created)<='".$endDate."' group by t1.content_id order by t1.id asc limit $offSet, $recPerPage";
		$recordArr = $this->Publishercontentview->query($query);
		
		$countRec = $this->Publishercontentview->query("select count(id) as count from publisher_content_view where date(created)>='".$startDate."' and date(created)<='".$endDate."' group by content_id");

        $this->set('recordArr', $recordArr);
		if(!empty($countRec)){
			$num = $countRec[0][0]['count'];
		} else {
			$num = 0;
		}
		$numOfPage=ceil($num/$recPerPage);
		$this->set(compact('users', @$users));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);		
	}
	
	public function dailyreport_old_cms77()
	{
		 //echo $this->Session->read('User.id');
		 //die;
		$user_id = $this->Session->read('User.id');
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			
			
			
			
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			
			$publisher = $_GET['publisher']; 
			$arr=array();
			
			error_log("dailyreport_old_cms77_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Total User Hits with msisdn*/
		
			
			$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits_reports where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
			
			foreach($totalhits as $key=>$val)
			{
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
			}	//echo '<pre>'; print_r($resArr);
			
			/* CG OK CODE */
			
			if($_GET['biller_id'] == 'vf'|| $_GET['biller_id'] == 'ia' || $_GET['biller_id'] =='at' ||$_GET['biller_id'] =='rl')
		    { 
			    $str= " and cg_response_id!='0'";
			}
			elseif($_GET['biller_id'] == 'tt')
			{ 
				$str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
			}
			elseif($_GET['biller_id'] == 'ac')
			{ 
				$str= " and cg_response_id!='0' and cg_response_id!='NULL'";
			}
		    else
			{
				$str='';
			}
			
			$cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str");
			
			foreach($cgok as $key=>$val)
			{	
				$resArr[$date]['cg_ok']=$val[0]['count'];
			}
			
			/* Billing Done Count */
			
			$billing_done = $this->Transaction77->query("select count(*) as count from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
			
			foreach($billing_done as $key=>$val)
			{
				$resArr[$date]['billing_done']=$val[0]['count'];
			}
			
			/* Total Revenue */
			
			$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
			
			foreach($total_revenue as $key=>$val)
			{
			   $resArr[$date]['total_revenue']=$val[0]['sum'];
		    } 
			
			/* callback Count */
			
			$callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and callback_status='1' ");
			
			foreach($callback_status as $key=>$val)
			{
					$resArr[$date]['callback_status']=$val[0]['count'];
			}
			
			/* Biller Id Vodafone */
			
            if($_GET['biller_id']=='vf')
			{
				if($publisher=='mini')
				{
				   $service_id='V3_MMOVI';
				}
				elseif($publisher=='firstcut')	
				{
				   $service_id='V3_FCUT';
				}
				elseif($publisher=='vfcomics')	
				{
					$service_id='V3_VCOM';
				}
				else
				{
				    $service_id='V3_BHOJ';
				}
				
				/* callback revenue */
				
				$callback = $this->Transaction77->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications_reports` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$stDate."' and CREATED<='".$edDate."' AND service_id LIKE '%$service_id%' and isblock=0  group by charging_mode,TXNID");
				
				$actual_price_deduct=0; $count=0;
				
				foreach($callback as $key=>$val)
			    {
				    $actual_price_deduct = $actual_price_deduct + $val['vodafone_callback_notifications_reports']['actual_price_deduct'];
					$count++;
			    }
				$resArr[$date]['count_revenue']=$count;
				$resArr[$date]['callback_revenue'] = $actual_price_deduct;
				
				/* parking */
				
				$parking=$this->Transaction77->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications_reports` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."' AND service_id LIKE '%$service_id%'  and isblock=0 group by charging_mode,TXNID,DATE(CREATED)");
				
				$count=0; $null=0; $null=0; $suspend=0;
				foreach($parking as $key=>$val)
				{
					if($val['vodafone_callback_notifications_reports']['charging_mode']=='PARKING')
					{
					    $count++;
					}
					if($val['vodafone_callback_notifications_reports']['charging_mode']=='null')
					{
					    $null++;
					}
				    if($val['vodafone_callback_notifications_reports']['charging_mode']=='SUSPEND')
				    {
					    $suspend++;
					}
				}
				'Count: '.$count;
				$deactivate = $null + $suspend;
				'deactivate: '.$deactivate;
				$resArr[$date]['parking'] = $count;
				$resArr[$date]['deactivate'] = $deactivate;
				
				$update = $this->Transaction77->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$stDate."' and CREATED<='".$edDate."' AND service_id LIKE '%$service_id%'");
				
				$sum1=$update['0']['0']['sum1'];
				$sum=$update['0']['0']['count'];
				
				$resArr[$date]['renewalTotal_revenue']=$sum1;
				$resArr[$date]['Recount'] = $sum;
				 
			}
			/* Biller Id Vodafone End */
			
			/* Biller Id Idea start */
			
		    if($_GET['biller_id']=='ia' && $_GET['publisher']=='filmy')
            {
				$totalrenewal = $this->UserHit77->query("SELECT count( * ) as count , sum( price ) as sum ,date(created) as date, ACTION , `status` FROM  bp_idea_callback_notifications_report as idea WHERE date(created) = '$date' AND STATUS = 'SUCCESS' GROUP BY ACTION");
				
				foreach($totalrenewal as $key=>$val)
		        {
		            if($val['idea']['ACTION']=='ACT')
					{
		              $resArr[$val[0]['date']]['count_revenue'] = $val[0]['count'];
                      $resArr[$val[0]['date']]['callback_revenue']=$val[0]['sum'];
					}
					if($val['idea']['ACTION']=='DCT')
					{
		              $resArr[$val[0]['date']]['deactivate']=$val[0]['count'];		
		            }
		            if($val['idea']['ACTION']=='REN')
					{
		              $resArr[$val[0]['date']]['Recount']=$val[0]['count'];
                      $resArr[$val[0]['date']]['renewalTotal_revenue']=$val[0]['sum'];		
					}
				}
				
			$parking = $this->UserHit77->query("SELECT count(*) as count FROM  bp_idea_callback_notifications_report as idea WHERE date(created) = '$date' AND action ='GRACE' and  STATUS = 'BAL-LOW'");
			
		    $resArr[$date]['parking']=$parking[0][0]["count"];
		    }
			
			/* Biller Id Idea End */
			
			/* Biller Id Tata Start */
			
			if($_GET['biller_id']=='tt')
		    {
				 $tata_subscriptioncount = $this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs_reports where  date(created)='".$_GET['startDate']."'  and service='SUBSCRIPTION' and status='SUCCESS'");
				 
				foreach($tata_subscriptioncount as $key=>$val)
				{
				    $date=$val[0]['date'];
                    $resArr[$date]['count_revenue']=$val[0]['count'];
                    $resArr[$date]['date']=$val[0]['date'];
			    }
				
				$tata_subscriptionsum = $this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs_reports where  date(created)='".$_GET['startDate']."'  and service='SUBSCRIPTION' and status='SUCCESS' ");
				
				foreach($tata_subscriptionsum as $key=>$val)
				{
					$date=$val[0]['date'];
                    $resArr[$date]['callback_revenue']=round($val[0]['sum'],2);
				}
				
				$tata_renewalcount = $this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs_reports where  date(created)='".$_GET['startDate']."'  and service='AUTORENEWAL' and status='SUCCESS'");
				
				foreach($tata_renewalcount as $key=>$val)
				{
				    $date=$val[0]['date'];
                    $resArr[$date]['Recount']=$val[0]['count'];
				}
				
				$tata_renewalsum = $this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs_reports where  date(created)='".$_GET['startDate']."'  and service='AUTORENEWAL' and status='SUCCESS'");
				
				foreach($tata_renewalsum as $key=>$val)
			    {
						$date=$val[0]['date'];
                        $resArr[$date]['renewalTotal_revenue']=round($val[0]['sum'],2);
				}
				
				$tata_deactivate = $this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs_reports where  date(created)='".$_GET['startDate']."' and service='UNSUBSCRIPTION' and status='SUCCESS' ");
				foreach($tata_deactivate as $key=>$val)
				{
						$date=$val[0]['date'];
                        $resArr[$date]['deactivate']=$val[0]['count'];
                        $resArr[$date]['date']=$val[0]['date'];
				}
				
		    }
			error_log("dailyreport_old_cms77_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Biller Id Tata End */
			
			$this->set(compact('totalhits','resArr'));
			//error_log("dailyreport_old_cms77_end | User_Id=".$user_id."");
			
		}
		/*else
		{
			$this->Session->setFlash('Select operator');
			$this->redirect(array('controller'=>'reports','action'=>'dailyreport_old_cms77'));	
		}*/
		
		
	}
	
	public function dailyreportdetail_old_cms_77()
	{
		$user_id = $this->Session->read('User.id');
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date1 = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			//if($_GET['biller_id'] == 'tt'){ $_GET['publisher'] = 'filmy';}
			$publisher = $_GET['publisher']; 
		}
		error_log("dailyreportdetail_old_cms_77_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		
		$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits_reports where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' group by interface order by count(*) desc");
		
		foreach($totalhits as $key=>$val)
		{
				$date=$val[0]['date'];
				$interface=$val['bp_user_hits_reports']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['total_hit']=$val[0]['count'];
		}	//echo '<pre>'; print_r($resArr);	
		if($_GET['biller_id'] == 'vf' || $_GET['biller_id'] == 'ia')
		{ 
	            $str= " and cg_response_id!='0'";
		}
		elseif($_GET['biller_id'] == 'tt')
		{ 
		        $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
		}
		elseif($_GET['biller_id'] == 'ac' || $_GET['biller_id'] =='at' ||$_GET['biller_id'] =='rl')
		{     
		        $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
		}
		else
		{
			$str='';
		}
		
		$cgok = $this->Transaction77->query("select count(*) as count,interface, pack_id  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str group by interface ");
		
		
		
		foreach($cgok as $key=>$val)
		{	
		       $campaignname = $this->Transaction77->query("select campaign_name from bp_packs where pack_id ='".$val['bp_transaction_report']['pack_id']."'");
			   //echo "<pre>";
			   //print_r($campaignname). "<br/>";
			   //echo $campaignname[0]['bp_packs']['campaign_name'];
			   //die;
				
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$interface;
				$resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				$resArr[$date][$interface]['pack_id']=$val['bp_transaction_report']['pack_id'];
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
				//$resArr[$date][$interface]['campaign_name']=$val['bp_transaction_report']['campaign_name'];
		}//echo '<pre>'; print_r($resArr);die;
		
		$billing_done = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE'  group by interface");
		
		foreach($billing_done as $key=>$val)
		{
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$interface;
				$resArr[$date][$interface]['billing_done']=$val[0]['count'];
	    }
		
		$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE'  group by interface ");
		
		foreach($total_revenue as $key=>$val)
		{
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
	    } //echo '<pre>'; print_r($resArr);	
		
		$mobile_notfound=$this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits_reports where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and msisdn='' group by interface");
		
		foreach($mobile_notfound as $key=>$val)
		{
				$interface=$val['bp_user_hits_reports']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
		}
		
		$callback_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction_report where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and callback_status='1' group by interface");
		
		foreach($callback_status as $key=>$val)
		{
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['callback_status']=$val[0]['count'];
		}
		
		$consent=$this->Transaction77->query("select count(transaction_id) as count,interface  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and consent='1'  group by interface");
		
		foreach($consent as $key=>$val)
		{
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['consent']=$val[0]['count'];
		} //echo '<pre>';print_r($resArr);die;

		$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction_report where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and churn_status='1'  group by interface");
		
		foreach($churn_status as $key=>$val)
		{
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['churn_status']=$val[0]['count'];
		}
		
		$total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and churn_status='1'   group by interface ");
	    
		foreach($total_revenue_churn as $key=>$val)
		{
				$interface=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction_report']['interface'];
				$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
		} 
		
		error_log("dailyreportdetail_old_cms_77_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		
		$this->set(compact('totalhits','resArr','date1'));
		
	}
	
	public function churndetail_old_cms_77()
	{
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date1 = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			//if($_GET['biller_id'] == 'tt'){ $_GET['publisher'] = 'filmy';}
			$publisher = $_GET['publisher']; 
		}
		
		$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(daily_churn_time) as date,interface  from bp_transaction where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and daily_churn_time>='".$stDate."' and daily_churn_time<='".$edDate."' and billing_status='DONE' and same_day_churn='1' group by interface order by count desc");

        foreach($churn_status as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['churn_status']=$val[0]['count'];
		}	
		
        $total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(daily_churn_time) as date,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and daily_churn_time>='".$stDate."' and daily_churn_time<='".$edDate."'  and billing_status='DONE' and same_day_churn='1'   group by interface");	
		
        foreach($total_revenue_churn as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
	    } 
		
		$this->set(compact('totalhits','resArr','date1'));
         		
	}
	public function dailyreport_cms77()
	{
		$user_id = $this->Session->read('User.id');
	    if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			error_log("dailyreport_cms77 LOG");
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			
			$publisher = $_GET['publisher']; 
			$arr=array();
			
			error_log("dailyreport_cms77_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Total User Hits with msisdn*/
			
			$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
			
			foreach($totalhits as $key=>$val)
			{
					
					$date=$val[0]['date'];
					$resArr[$date]['total_hit']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
			}	//echo '<pre>'; print_r($resArr);
			
			/* CG OK CODE */
			
			if($_GET['biller_id'] == 'vf'|| $_GET['biller_id'] == 'ia' || $_GET['biller_id'] =='at' ||$_GET['biller_id'] =='rl'  )
			{ 
		        $str= " and cg_response_id!='0'";
			}
			elseif($_GET['biller_id'] == 'tt')
			{ 
			    $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
			}
			elseif($_GET['biller_id'] == 'ac')
			{ 
			    $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
			}
			else
			{
				$str='';
			}
			
			$cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str ");
			
			foreach($cgok as $key=>$val)
			{	
				$resArr[$date]['cg_ok']=$val[0]['count'];
			}
			
			/* Billing Done Count */
			
			$billing_done = $this->Transaction77->query("select count(*) as count from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' ");
			
			foreach($billing_done as $key=>$val)
			{
				$resArr[$date]['billing_done']=$val[0]['count'];
			}
			
			/* Total Revenue */
			
			$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE'");
			
			foreach($total_revenue as $key=>$val)
			{
				$resArr[$date]['total_revenue']=$val[0]['sum'];
			} 
			
			/* Callback Count */
			
			$callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and callback_status='1' ");
			
			foreach($callback_status as $key=>$val)
			{
				$resArr[$date]['callback_status']=$val[0]['count'];
			}
			
			/* Biller Id Vodafone Start*/
			
			if($_GET['biller_id']=='vf')
			{
				if($publisher=='mini')
				{
					$service_id='V3_MMOVI';
				}
				elseif($publisher=='vfcomics')	
				{
					$service_id='V3_VCOM';
				}
				elseif($publisher=='firstcut')	
				{
					$service_id='V3_FCUT';
				}
				else
				{
					$service_id='V3_BHOJ';
			    }
				
				/* Callback Revenue */
				
				$callback=$this->Transaction77->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$stDate."' and CREATED<='".$edDate."' AND service_id LIKE '%$service_id%' and isblock=0  group by charging_mode,TXNID");
				
				$actual_price_deduct=0; $count=0;
				
				foreach($callback as $key=>$val)
				{
				    $actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
					$count++;
				}//echo '<pre>'; print_r($resArr);die;
				
				$resArr[$date]['count_revenue']=$count;
				$resArr[$date]['callback_revenue'] = $actual_price_deduct;
				
				/* Parking Count */
				
				$parking=$this->Transaction77->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."'  AND service_id LIKE '%$service_id%'  and isblock=0 group by charging_mode,TXNID,DATE(CREATED)");
				
				$count=0; $null=0; $null=0; $suspend=0;
				
				foreach($parking as $key=>$val)
				{
					if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
					{
					    $count++;
					}
					if($val['vodafone_callback_notifications']['charging_mode']=='null')
					{
					    $null++;
					}
					if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
					{
					    $suspend++;
					}
				}
				'Count: '.$count;
				$deactivate = $null + $suspend;
				'deactivate: '.$deactivate;
				$resArr[$date]['parking']=$count;
				$resArr[$date]['deactivate'] = $deactivate;
				
				$update=$this->Transaction77->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$stDate."' and CREATED<='".$edDate."' AND service_id LIKE '%$service_id%'");
				
				$sum1=$update['0']['0']['sum1'];
				$sum=$update['0']['0']['count'];
				
				$resArr[$date]['renewalTotal_revenue']=$sum1;
				$resArr[$date]['Recount'] = $sum;
				
			}
			
			/* Biller Id Vodafone End*/
			
			/* Biller Id Idea Start*/
			
			if($_GET['biller_id']=='ia' && $_GET['publisher']=='filmy')
			{
				$totalrenewal = $this->UserHit77->query("SELECT count( * ) as count , sum( price ) as sum ,date(created) as date, ACTION , `status` FROM  bp_idea_callback_notifications as idea WHERE date(created) = '$date' AND STATUS = 'SUCCESS' GROUP BY ACTION");
                
                foreach($totalrenewal as $key=>$val)
				{
		            if($val['idea']['ACTION']=='ACT')
					{
		              $resArr[$val[0]['date']]['count_revenue']=$val[0]['count'];
                      $resArr[$val[0]['date']]['callback_revenue']=$val[0]['sum'];		
					}
		            if($val['idea']['ACTION']=='DCT')
					{
		              $resArr[$val[0]['date']]['deactivate']=$val[0]['count'];	
		            }
		            if($val['idea']['ACTION']=='REN')
					{
		              $resArr[$val[0]['date']]['Recount']=$val[0]['count'];
                      $resArr[$val[0]['date']]['renewalTotal_revenue']=$val[0]['sum'];		
					}
				}	
                
				/* Parking Count */
				
                $parking = $this->UserHit77->query("SELECT count(*) as count FROM  bp_idea_callback_notifications as idea WHERE date(created) = '$date' AND action ='GRACE' and  STATUS = 'BAL-LOW'");
				
		        $resArr[$date]['parking']=$parking[0][0]["count"];				
			}
			
			/* Biller Id Idea End */
			
			/* Biller Id Tata Start */
			
			if($_GET['biller_id']=='tt')
		    {
				$tata_subscriptioncount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  date(created)='".$_GET['startDate']."' and service='SUBSCRIPTION' and status='SUCCESS'");
				
				foreach($tata_subscriptioncount as $key=>$val)
				{
					$date=$val[0]['date'];
                    $resArr[$date]['count_revenue']=$val[0]['count'];
                    $resArr[$date]['date']=$val[0]['date'];
				}
				
				$tata_subscriptionsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  date(created)='".$_GET['startDate']."'  and service='SUBSCRIPTION' and status='SUCCESS'");
				
				foreach($tata_subscriptionsum as $key=>$val)
				{
						$date=$val[0]['date'];
                        $resArr[$date]['callback_revenue']=round($val[0]['sum'],2);
				}
				
				$tata_renewalcount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  date(created)='".$_GET['startDate']."'  and service='AUTORENEWAL' and status='SUCCESS' $productId");
				
				foreach($tata_renewalcount as $key=>$val)
				{
						$date=$val[0]['date'];
                        $resArr[$date]['Recount']=$val[0]['count'];
				}
				
				$tata_renewalsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  date(created)='".$_GET['startDate']."'  and service='AUTORENEWAL' and status='SUCCESS'");
				
				foreach($tata_renewalsum as $key=>$val)
				{
						$date=$val[0]['date'];
                        $resArr[$date]['renewalTotal_revenue']=round($val[0]['sum'],2);
			    }
				$tata_deactivate=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  date(created)='".$_GET['startDate']."' and service='UNSUBSCRIPTION' and status='SUCCESS'");
				
				foreach($tata_deactivate as $key=>$val)
				{
						$date=$val[0]['date'];
                        $resArr[$date]['deactivate']=$val[0]['count'];
                        $resArr[$date]['date']=$val[0]['date'];
				}
				
		    }
			error_log("dailyreport_cms77_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Biller Id Tata End */
			
			$this->set(compact('totalhits','resArr'));
			
		}
	}
	public function dailyreportdetail_cms_77()
	{
		$user_id = $this->Session->read('User.id');
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date1 = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			
			$publisher = $_GET['publisher']; 
		}
		
		error_log("dailyreportdetail_cms_77_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		
		$totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' group by interface order by count(*) desc");
		
		foreach($totalhits as $key=>$val)
		{
				$date=$val[0]['date'];
				$interface=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['total_hit']=$val[0]['count'];
	    }	//echo '<pre>'; print_r($resArr);	   
		
		if($_GET['biller_id'] == 'vf' || $_GET['biller_id'] == 'ia')
		{  
	         $str= " and cg_response_id!='0'";
		}
		elseif($_GET['biller_id'] == 'tt')
		{ 
		     $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
		}
		elseif($_GET['biller_id'] == 'ac' || $_GET['biller_id'] =='at' ||$_GET['biller_id'] =='rl')
		{ 
		     $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
		}
		else
		{
			$str='';
		}
		
		$cgok = $this->Transaction77->query("select count(*) as count,interface,pack_id  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str group by interface ");
		
		foreach($cgok as $key=>$val)
		{	
		    $campaignname = $this->Transaction77->query("select campaign_name from bp_packs where pack_id ='".$val['bp_transaction']['pack_id']."'");
			
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$interface;
				$resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				$resArr[$date][$interface]['pack_id']=$val['bp_transaction']['pack_id'];
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
				
		}//echo '<pre>'; print_r($resArr);die;
		
		$billing_done = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE'  group by interface");
	    foreach($billing_done as $key=>$val)
		{
			   
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$interface;
				$resArr[$date][$interface]['billing_done']=$val[0]['count'];
	    }
		
		$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE'  group by interface ");
		
		foreach($total_revenue as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
		} 
		
		$mobile_notfound=$this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and msisdn='' group by interface");
		
		foreach($mobile_notfound as $key=>$val)
		{
				$interface=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
		}
		$callback_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and callback_status='1'  group by interface");
		
		foreach($callback_status as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['callback_status']=$val[0]['count'];
		}
		$consent=$this->Transaction77->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and consent='1'  group by interface");
		
		foreach($consent as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['consent']=$val[0]['count'];
		} //echo '<pre>';print_r($resArr);die;
		
		$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_status='DONE' and churn_status='1'  group by interface");
		
		foreach($churn_status as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['churn_status']=$val[0]['count'];
		}
		
		$total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."'  and billing_status='DONE' and churn_status='1'   group by interface ");
		
		foreach($total_revenue_churn as $key=>$val)
		{
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
		}
		error_log("dailyreportdetail_cms_77_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		
		$this->set(compact('totalhits','resArr','date1'));
		
	}
	public function hourly_churn_cms77()
	{
		$date=$_GET['startDate'];
		$tDate=date('Y-m-d');
		$biller_id=$_GET['biller_id']; 
		$currentDate=date('Y-m-d H:i:s'); 
		$thirtymin=date("Y-m-d H:i:s", strtotime("-30 minutes"));
		$oneHoure=date("Y-m-d H:i:s", strtotime("-60 minutes"));
		$twoHoure=date("Y-m-d H:i:s", strtotime("-120 minutes"));
		$threeHoure=date("Y-m-d H:i:s", strtotime("-180 minutes"));
		$daily=$date.' 00:00:00'; 
		$churn=0;
		$sub=0;
		$x=0;
		/* Biller Id Vodafone Start*/
		if($biller_id=='vf')
		{
			if($date==$tDate)
			{
				$sub=0;
				/* data for 30 minutes */
				$subsData=$this->UserHit77->query("select msisdn from vodafone_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$thirtymin."' and isblock=0");
				
				foreach($subsData as $row)
			    {
					$msisdn = $row['vodafone_callback_notifications']['msisdn']; 
					
					$churnData=$this->UserHit77->query("select * from vodafone_callback_notifications where action='DCT' and status='SUCCESS' and (charging_mode='null' OR charging_mode='DEACTIVATE')  and date(created)='".$date."' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$thirtymin."'");
					
					if(!empty($churnData))
				    {
					    $churn++;
				    }
					$sub++;
				    $arr['30min']['churn']=$churn;
				    $arr['30min']['sub']=$sub;
			    }
				
				/* data for 30 minute End */
				
				/* data for one hour Start */
				
				$churn1=0;
	            $sub1=0;
				
				$subsData=$this->UserHit77->query("select msisdn from vodafone_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$oneHoure."' and isblock=0");
				
				foreach($subsData as $row)
			    {
				    $msisdn=$row['vodafone_callback_notifications']['msisdn'];
				
				    $churnData=$this->UserHit77->query("select * from vodafone_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and (charging_mode='null' OR charging_mode='DEACTIVATE')  and created<='".$currentDate."' and created>='".$oneHoure."'");
				    
					if(!empty($churnData))
				    {
					    $churn1++;
					}
				    
					$sub1++;
				    $arr['1houre']['sub']=$sub1;
				    $arr['1houre']['churn']=$churn1;
				}
				
				/* data for one hour End */
				
				/* data for two hour Start*/
				
				$churn2=0;
	            $sub2=0;
				
				$subsData=$this->UserHit77->query("select msisdn from vodafone_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."'  and created<='".$currentDate."' and created>='".$twoHoure."' and isblock=0");
				
				foreach($subsData as $row)
			    {
				    $msisdn=$row['vodafone_callback_notifications']['msisdn'];
				
				    $churnData=$this->UserHit77->query("select * from vodafone_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$twoHoure."'");
				  
				    if(!empty($churnData))
				    { 
					 $churn2++;
				    }
				    
					$sub2++;
				    $arr['2houre']['churn']=$churn2;
				    $arr['2houre']['sub']=$sub2;
			    }
				
				/* data for two hour End*/
				
				/* data for three hour start*/
				
				$churn3=0;
				$sub3=0;
				
				$subsData=$this->UserHit77->query("select msisdn from vodafone_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$threeHoure."' and isblock=0");
				
				foreach($subsData as $row)
			    {
				    $msisdn=$row['vodafone_callback_notifications']['msisdn'];
				
				    $churnData=$this->UserHit77->query("select * from vodafone_callback_notifications where action='DCT' and status='SUCCESS' and 				   date(created)='".$date."' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$threeHoure."' and (charging_mode='null' OR charging_mode='DEACTIVATE') ");
				
				    if(!empty($churnData))
				    {
					    $churn3++;
				    }
				    
					$sub3++;
				    $arr['3houre']['churn']=$churn3;
				    $arr['3houre']['sub']=$sub3;
				}
				/* data for three hour End*/
				
				/* data for daily hour start*/
				
				$churn4=0;
			    $sub4=0;
				
				$subsData=$this->UserHit77->query("select msisdn,created from vodafone_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$daily."' and isblock=0 order by id desc");
				
				foreach($subsData as $row)
			    {
				    $msisdn=$row['vodafone_callback_notifications']['msisdn'];
				    $subCallbackCreated=$row['vodafone_callback_notifications']['created'];
				
				    $churnData=$this->UserHit77->query("select * from vodafone_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and (charging_mode='null' OR charging_mode='DEACTIVATE')  and created<='".$currentDate."' and created>='".$subCallbackCreated."' ");
				
				    if(!empty($churnData))
				    {
					   $churn4++;
				    }
				    
					$sub4++;
				  	$arr['daily']['churn']=$churn4;
                    $arr['daily']['sub']=$sub4;
				}
				/* data for daily hour End*/
				
				$this->set('arr', $arr);
			}
			else
			{
				
			}
			$churn5=0;
		    $sub5=0;
			
			if($date==$tDate)
		    {
		       $churntable='vodafone_callback_notifications';
			}
		    else
		    {
			   $churntable='vodafone_callback_notifications_reports';
		    }
			
			$subsData=$this->UserHit77->query("select msisdn,created from ".$churntable." as vodafone_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and isblock=0");
			
			foreach($subsData as $row)
			{
				 $msisdn=$row['vodafone_callback_notifications']['msisdn'];
				 $subCallbackCreated=$row['vodafone_callback_notifications']['created'];
				 
				 $churnData=$this->UserHit77->query("select * from ".$churntable." as vodafone_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and created>='".$subCallbackCreated."' and (charging_mode='null' OR charging_mode='DEACTIVATE') ");
				
				 if(!empty($churnData))
				 {
					 $churn5++;
				 }
				  $sub5++;
				  $arr['daily']['churn']=$churn5;
				  $arr['daily']['sub']=$sub5;
			}
            
			$this->set('arr', $arr);			
			
		}
		/* Biller Id Vodafone End*/
		elseif($biller_id=='iaqqqqq')
		{
			$date=$_GET['startDate'];
			$cDate=date('Y-m-d');
			if($date==$cDate)
			{
		  		$churntable='bp_idea_callback_notifications';
			}
		    else
			{
				$churntable='bp_idea_callback_notifications_report';
			}
			$subsData=$this->UserHit77->query("select msisdn,created from ".$churntable." as bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and date(created)='".$date."'");
			$x=0;$y=0;$z=0;$m=0;$d=0;
			foreach($subsData as $data)
			{
				$msisdn=$data['bp_idea_callback_notifications']['msisdn'];
				$createdDate=$data['bp_idea_callback_notifications']['created'];;
				$currentDate=date('Y-m-d H:i:s'); 
				$thirtymin=date("Y-m-d H:i:s", strtotime("-30 minutes"));
				$oneHoure=date("Y-m-d H:i:s", strtotime("-60 minutes"));
				$twoHoure=date("Y-m-d H:i:s", strtotime("-120 minutes"));
				$threeHoure=date("Y-m-d H:i:s", strtotime("-180 minutes"));			
				if($createdDate>=$thirtymin && $createdDate<=$currentDate)
				{
				    $arr['30min'][$x]=$data['bp_idea_callback_notifications']['msisdn'];
				    //$arr['ideaCreate'][$x]=$data['bp_idea_callback_notifications']['createdDate'];
					$x++;
				}
			    if($createdDate>=$oneHoure && $createdDate<=$currentDate)
				{ 
					$arr['1houre'][$y]=$data['bp_idea_callback_notifications']['msisdn'];
				    //$arr['ideaCreate'][$y]=$data['bp_idea_callback_notifications']['createdDate'];
					$y++;
				}
			    if($createdDate>=$twoHoure && $createdDate<=$currentDate)
				{ 
					$arr['2houre'][$z]=$data['bp_idea_callback_notifications']['msisdn'];
					//$arr['ideaCreate'][$z]=$data['bp_idea_callback_notifications']['createdDate'];
				    $z++;
				}
				if($createdDate>=$threeHoure && $createdDate<=$currentDate)
				{ 
					$arr['3houre'][$m]=$data['bp_idea_callback_notifications']['msisdn'];
				    //$arr['ideaCreate'][$m]=$data['bp_idea_callback_notifications']['createdDate'];
					$m++;
				}
				$arr['daily'][$d]=$data['bp_idea_callback_notifications']['msisdn'];
				//$arr['ideaCreate'][$d]=$createdDate;
				$d++;
			}
			$sub1=0;$churn1=0;$sub2=0;$churn2=0;$sub3=0;$churn3=0;$sub4=0;$churn4=0;$sub5=0;$churn5=0;$msi=0;
			$arr1=array();
			foreach($arr as $key=>$data1)
			{
				foreach($data1 as $msisdn)
				{
				    //$msisdn=$data1[$msi];
					if($date==$cdate)
					{
						if($key=='30min')
						{
							$dctDate=$thirtymin;
					    }
						elseif($key=='1houre')
					    {
						    $dctDate=$oneHoure;
						}
						elseif($key=='2houre')
						{
						    $dctDate=$twoHoure;
						}
					    elseif($key=='3houre')
						{
						    $dctDate=$threeHoure;
						}
					    else
						{
							$dctDate=$date.' 00:00:00';
						    $dctDate=$createdDate;
						}	
					}
					else
					{
						$currentDate=$date.' 23:59:59';
					    $dctDate=$date.' 00:00:00';
					}
					
					$churnData=$this->UserHit77->query("select * from ".$churntable." as bp_idea_callback_notifications where action='DCT' and status='SUCCESS' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$dctDate."'");
							
				    if($key=='30min')
					{
						if(!empty($churnData))
						{
							$churn1++;
						}
						$sub1++;
						$arr1[$key]['churn']=$churn1;
						$arr1[$key]['sub']=$sub1;
					}
					elseif($key=='1houre')
					{
						if(!empty($churnData))
						{
							$churn2++;
						}
						$sub2++;
						$arr1[$key]['churn']=$churn2;
						$arr1[$key]['sub']=$sub2;
					}
					elseif($key=='2houre')
					{
						if(!empty($churnData))
						{
							$churn3++;
						}
						$sub3++;
						$arr1[$key]['churn']=$churn3;
						$arr1[$key]['sub']=$sub3;
					}
					elseif($key=='3houre')
					{
						if(!empty($churnData))
						{
							$churn4++;
						}
						$sub4++;
						$arr1[$key]['churn']=$churn4;
						$arr1[$key]['sub']=$sub4;
					}
					else
					{
						if(!empty($churnData))
						{
						    $churn5++;  
							CakeLog::info('[hcchurn daily '.$date.']| msisdn='.$msisdn,'debug');
						}
						$sub5++;
						$arr1[$key]['churn']=$churn5;
						$arr1[$key]['sub']=$sub5;
					}
					    $msi++;
				}
			}
			$this->set('arr', $arr1);	
		}
		elseif($biller_id=='tt')
		{
			$date=$_GET['startDate'];
			$cDate=date('Y-m-d');
		    $daily=$date.' 00:00:00';
			$endDaily=$date.' 23:59:59';
			if($date==$cDate)
		    {
		        $churntable='tata_subscription_logs';
			}
		    else
		    {
			    $churntable='tata_subscription_logs_reports';
		    }
			$subsData=$this->UserHit77->query("select MDN,created from ".$churntable." as tata_subscription_logs where created>='".$daily."' and created<='".$endDaily."' and SERVICE='SUBSCRIPTION' and STATUS='SUCCESS'");
			$x=0;$y=0;$z=0;$m=0;$d=0;
			foreach($subsData as $data)
			{
			    $msisdn=$data['tata_subscription_logs']['MDN'];
				$createdDate=$data['tata_subscription_logs']['created'];
				$currentDate=date('Y-m-d H:i:s'); 
				$thirtymin=date("Y-m-d H:i:s", strtotime("-30 minutes"));
				$oneHoure=date("Y-m-d H:i:s", strtotime("-60 minutes"));
				$twoHoure=date("Y-m-d H:i:s", strtotime("-120 minutes"));
				$threeHoure=date("Y-m-d H:i:s", strtotime("-180 minutes"));			
				if($createdDate>=$thirtymin && $createdDate<=$currentDate)
				{
					$arr['30min'][$x]=$data['tata_subscription_logs']['MDN'];
					$x++;
				}
				if($createdDate>=$oneHoure && $createdDate<=$currentDate)
				{ 
					$arr['1houre'][$y]=$data['tata_subscription_logs']['MDN'];
					$y++;
				}
				if($createdDate>=$twoHoure && $createdDate<=$currentDate)
				{ 
					$arr['2houre'][$z]=$data['tata_subscription_logs']['MDN'];
					$z++;
				}
				if($createdDate>=$threeHoure && $createdDate<=$currentDate)
				{ 
					$arr['3houre'][$m]=$data['tata_subscription_logs']['MDN'];
					$m++;
				}
				$arr['daily'][$d]=$data['tata_subscription_logs']['MDN'];
				$d++;
		    }
			$sub1=0;$churn1=0;$sub2=0;$churn2=0;$sub3=0;$churn3=0;$sub4=0;$churn4=0;$sub5=0;$churn5=0;$msi=0;
		    $arr1=array();
			foreach($arr as $key=>$data1)
			{
				foreach($data1 as $msisdn)
				{
				    //$msisdn=$data1[$msi];
					if($date==$cDate)
					{
						if($key=='30min')
						{
						    $dctDate=$thirtymin;
						}
						elseif($key=='1houre')
						{
						    $dctDate=$oneHoure;
						}
						elseif($key=='2houre')
						{
						    $dctDate=$twoHoure;
						}
						elseif($key=='3houre')
						{
						    $dctDate=$threeHoure;
						}
						else
						{
							$dctDate=$date.' 00:00:00';
						}	
					}
				    else
					{
						$currentDate=$date.' 23:59:59';
						$dctDate=$date.' 00:00:00';
					}
					//echo "select * from tata_subscription_logs where created<='".$currentDate."' and created>='".$dctDate."' and  SERVICE='UNSUBSCRIPTION' and STATUS='SUCCESS' and MDN='".$msisdn."'"; die;
					
					$churnData=$this->UserHit77->query("select * from ".$churntable." as tata_subscription_logs where created<='".$currentDate."' and created>='".$dctDate."' and  SERVICE='UNSUBSCRIPTION' and STATUS='SUCCESS' and MDN='".$msisdn."'");
							
					if($key=='30min')
					{
						if(!empty($churnData))
						{
							$churn1++;
						}
						$sub1++;
						$arr1[$key]['churn']=$churn1;
						$arr1[$key]['sub']=$sub1;
					}
					elseif($key=='1houre')
					{
						if(!empty($churnData))
						{
							$churn2++;
						}
						$sub2++;
						$arr1[$key]['churn']=$churn2;
						$arr1[$key]['sub']=$sub2;
					}
					elseif($key=='2houre')
					{
						if(!empty($churnData))
						{
							$churn3++;
						}
						$sub3++;
						$arr1[$key]['churn']=$churn3;
						$arr1[$key]['sub']=$sub3;
					}
				    elseif($key=='3houre')
					{
					    if(!empty($churnData))
						{
							$churn4++;
						}
						$sub4++;
						$arr1[$key]['churn']=$churn4;
						$arr1[$key]['sub']=$sub4;
					}
					else
					{
						//echo 2222;
					    if(!empty($churnData))
						{
							//echo 11123;die;
							$churn5++;  
						    // CakeLog::info('[hcchurn daily '.$date.']| msisdn='.$msisdn,'debug');
						}
						$sub5++;
						$arr1[$key]['churn']=$churn5;
						$arr1[$key]['sub']=$sub5;
					}
				}
		    }
			$this->set('arr', $arr1);
		}
		elseif($biller_id=='ia')
		{
			
			if($date==$tDate)
			{
				$sub=0;
				/* Data for 30 min Start*/
				
				$subsData=$this->UserHit77->query("select msisdn from bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$thirtymin."'");
				foreach($subsData as $row)
			    {
				    $msisdn=$row['bp_idea_callback_notifications']['msisdn']; 
				
				    $churnData=$this->UserHit77->query("select * from bp_idea_callback_notifications where action='DCT' and status='SUCCESS'  and date(created)='".$date."' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$thirtymin."'");
				    
					if(!empty($churnData))
				    {
					   $churn++;
				    }
				    $sub++;
				    $arr['30min']['churn']=$churn;
				    $arr['30min']['sub']=$sub;
			    }
				/* Data for 30 min End*/
				
				/* Data for 1 hour Start*/
				$churn1=0;$sub1=0;
				
				$subsData=$this->UserHit77->query("select msisdn from bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$oneHoure."'");
				
				foreach($subsData as $row)
			    {
				 $msisdn=$row['bp_idea_callback_notifications']['msisdn'];
				
				 $churnData=$this->UserHit77->query("select * from bp_idea_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."'   and created<='".$currentDate."' and created>='".$oneHoure."' ");
				
				 if(!empty($churnData))
				 {
					 $churn1++;
				 }
				 $sub1++;
				 $arr['1houre']['sub']=$sub1;
				 $arr['1houre']['churn']=$churn1;
				}
				/* Data for 1 hour End*/
				
				/* Data for 2 hour Start*/
				$churn2=0;$sub2=0;
				$subsData=$this->UserHit77->query("select msisdn from bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."'  and created<='".$currentDate."' and created>='".$twoHoure."'");
				
				foreach($subsData as $row)
			    {
				 $msisdn=$row['bp_idea_callback_notifications']['msisdn'];
				
				 $churnData=$this->UserHit77->query("select * from bp_idea_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$twoHoure."' ");
				
				 if(!empty($churnData))
				 { 
					 $churn2++;
				 }
				  $sub2++;
				  $arr['2houre']['churn']=$churn2;
				  $arr['2houre']['sub']=$sub2;
			    }
				/* Data for 2 hour End*/
				
				/* Data for 3 hour Start*/
				$churn3=0;$sub3=0;
				$subsData=$this->UserHit77->query("select msisdn from bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$threeHoure."'");
				
				foreach($subsData as $row)
			    {
				    $msisdn=$row['bp_idea_callback_notifications']['msisdn'];
				    
					$churnData=$this->UserHit77->query("select * from bp_idea_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and created<='".$currentDate."' and created>='".$threeHoure."' ");
				    
					if(!empty($churnData))
				    {
					  $churn3++;
				    }
				    
					$sub3++;
				    $arr['3houre']['churn']=$churn3;
				    $arr['3houre']['sub']=$sub3;
				}
				/* Data for 3 hour End*/
				
				/* Data for daily hour start*/
				$churn4=0;
			    $sub4=0;
				
				$subsData=$this->UserHit77->query("select msisdn,created from bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."' and created<='".$currentDate."' and created>='".$daily."'  order by id desc");
				
			    foreach($subsData as $row)
			    {
				    $msisdn=$row['bp_idea_callback_notifications']['msisdn'];
				    $subCallbackCreated=$row['bp_idea_callback_notifications']['created'];
				
				    $churnData=$this->UserHit77->query("select * from bp_idea_callback_notifications where action='DCT' and status='SUCCESS' and 				   date(created)='".$date."' and msisdn='".$msisdn."'  and created<='".$currentDate."' and created>='".$subCallbackCreated."' ");
					
				    if(!empty($churnData))
				    {
					   $churn4++;
				    }
				    $sub4++;
				  	$arr['daily']['churn']=$churn4;
                    $arr['daily']['sub']=$sub4;
				}
				/* Data for daily hour End*/
				$this->set('arr', $arr);
				
			}
			else
			{
				
			}
			$churn5=0;$sub5=0;
			if($date==$tDate)
		    {
		       $churntable='bp_idea_callback_notifications';
		    }
		    else
		    {
			   $churntable='bp_idea_callback_notifications_report';
		    }
			$subsData=$this->UserHit77->query("select msisdn,created from ".$churntable." as bp_idea_callback_notifications where action='ACT' and status='SUCCESS' and  date(created)='".$date."'  ");
			foreach($subsData as $row)
			{
				 $msisdn=$row['bp_idea_callback_notifications']['msisdn'];
				 $subCallbackCreated=$row['bp_idea_callback_notifications']['created'];
				 
				 $churnData=$this->UserHit77->query("select * from ".$churntable." as bp_idea_callback_notifications where action='DCT' and status='SUCCESS' and date(created)='".$date."' and msisdn='".$msisdn."' and created>='".$subCallbackCreated."'  ");
				
				 if(!empty($churnData))
				 {
					 $churn5++;

					 //CakeLog::info('MDNNNN = '.$msisdn,'debug');
				 }
				  $sub5++;
				  $arr['daily']['churn']=$churn5;
				  $arr['daily']['sub']=$sub5;
			}	
			$this->set('arr', $arr);
			
		}
		
	}
	
	public function dailyreport_cms99()
	{
		$user_id = $this->Session->read('User.id');
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher']; 
			
			error_log("dailyreport_cms99_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Total User Hits with msisdn*/
			
			$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
		    
		    foreach($totalhits as $key=>$val)
		    {
			   $resArr[$date]['total_hit']=$val[0]['count'];
			   $resArr[$date]['date']=$date;
			}
            
			/* Total User Hits without msisdn*/		
			
			$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and (msisdn='' or msisdn='NA')");
			
			foreach($totalhits as $key=>$val)
		    {
			$resArr[$date]['mobilenotfound_hit']=$val[0]['count'];
			$resArr[$date]['date']=$date;
		    }	
			/* CG OK CODE */
			if($_GET['biller_id'] == 'vf' )
			{ 
		       $str= " and cg_response_id!='0'";
			}
			elseif($_GET['biller_id'] == 'at')
			{ 
			   $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
			}
			elseif($_GET['biller_id'] == 'ac')
			{ 
			   $str= " and cg_response_id!='0' AND cg_response_id!='NULL'";
			}
			else
			{
			   $str='';
			}
			$cgok = $this->Transaction99bhojpuri->query("select count(*) as count  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str");
			foreach($cgok as $key=>$val)
		    {	
			$resArr[$date]['cg_ok']=$val[0]['count'];
			$resArr[$date]['date']=$date;
		    }
			if($_GET['biller_id']=='rl')
		    {
			 $billing_done=$this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and billing_response!='subscription already opened.' and price_deducted>0");
	         
			 foreach($billing_done as $key=>$val)
	         {
		        $resArr[$date]['total_revenue']=$val[0]['sum'];
			    $resArr[$date]['billing_done']=$val[0]['count'];
			    $resArr[$date]['date']=$date;
			 }
			}
			else
		    {
		      
			  $billing_done=$this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 ");
	          
			  foreach($billing_done as $key=>$val)
	          {
		         $resArr[$date]['total_revenue']=$val[0]['sum'];
			     $resArr[$date]['billing_done']=$val[0]['count'];
			     $resArr[$date]['date']=$date;
			  }
		     
			  $parking=$this->Transaction99bhojpuri->query("select count(*) as count from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted='0' ");
              
			  foreach($parking as $key=>$val)
	          {
			      $resArr[$date]['count_sub2']=$val[0]['count'];
			      $resArr[$date]['date']=$date;
			  }
		    }
			if($_GET['biller_id']=='rl')
		    {
		        
				$callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1'");
		        foreach($callback_status as $key=>$val)
		        {
			      $resArr[$date]['callback_status']=$val[0]['count'];
			      $resArr[$date]['date']=$date; 
				}
			}
			 else
		    {
			 $callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0");
		     foreach($callback_status as $key=>$val)
		     {
			     $resArr[$date]['callback_status']=$val[0]['count'];
			     $resArr[$date]['date']=$date; 
		     }
			}
			if($_GET['biller_id']=='vf')
		    {
		       $callback=$this->Transaction99bhojpuri->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."' and `ACTION` = 'act' and `STATUS` = 'success'   group by charging_mode,TXNID,DATE(CREATED)");
		       
			   $actual_price_deduct=0; $count=0;
                foreach($callback as $key=>$val)
                {                   
			        $actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
                    $date=$val[0]['date'];
                    $count++;
                }
                $date=$val[0]['date'];
                $resArr[$date]['count_revenue']=$count;
                $resArr[$date]['callback_revenue'] = $actual_price_deduct;
	            
				$parking=$this->Transaction99bhojpuri->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE  CREATED>='".$stDate."' and CREATED<='".$edDate."' group by charging_mode,TXNID,DATE(CREATED)");
                
				$count=0; $null=0; $null=0; $suspend=0;
                foreach($parking as $key=>$val)
                {
                        if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
                        {
                        $count++;
                        }
                        if($val['vodafone_callback_notifications']['charging_mode']=='null')
                        {
                          $null++;
                        }
                         if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
                        {
                          $suspend++;
                        }
                }
                'Count: '.$count;
                $deactivate=$null+$suspend;'deactivate: '.$deactivate;
                $date=$val[0]['date'];
                $resArr[$date]['parking']=$count;
                $resArr[$date]['deactivate'] = $deactivate;
                //echo'<pre>';print_r($resArr);die;
		
		        $update=$this->Transaction99bhojpuri->query("SELECT count(*),charging_mode,TXNID FROM `vodafone_update_notifications` WHERE  CREATED>='".$stDate."' and CREATED<='".$edDate."' group by charging_mode,TXNID,DATE(CREATED)");
                $count=0;$daily=0;$monthly=0;$weekly=0;$days=0;$daily1=0;$daily2=0;$daily3=0;$daily5=0;$days3=0;$grace=0;
                foreach($update as $key=>$val)
                {
                        $fifteen_days= $val['vodafone_update_notifications']['charging_mode'];
                        if($val['vodafone_update_notifications']['charging_mode']=='15DAYS')
                        {
                        $count++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY')
                        {
                         $daily++;

                        }elseif($val['vodafone_update_notifications']['charging_mode']=='WEEKLY')
                        {
                        $weekly++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='MONTHLY')
                        {
                        $monthly++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='3DAYS')
                        {
                        $days3++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='5DAYS')
                        {
                        $days++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY1')
                        {
                        $daily1++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY2')
                        {
                        $daily2++;
						}elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY3')
                        {
                        $daily3++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY5')
                        {
                        $daily5++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='GRACE')
                        {
                        $grace++;
                        }
	
                }
                'Count: '.$count; 'Value:' .$mul=$count*30;
				'Count Daily: '.$daily; 'Value:' .$mul1=$daily*5;
				'Count weekly: '.$weekly; 'Value:' .$mul2=$weekly*15;
				'Count monthly: '.$monthly; 'Value:' .$mul3=$monthly*50;
				'Count days: '.$days; 'Value:' .$mul4=$days*10;
			    'Count days3: '.$days3; 'Value:' .$mul9=$days3*7;
				'Count daily1: '.$daily1; 'Value:' .$mul5=$daily1*1;
				'Count daily2: '.$daily2; 'Value:' .$mul6=$daily2*2;
				'Count daily3: '.$daily3; 'Value:' .$mul7=$daily3*3;
				'Count daily5: '.$daily5; 'Value:' .$mul8=$daily5*5;
                
				$sum=$count+$daily+$weekly+$monthly+$days+$daily1+$daily2+$daily3+$daily5+$days3;
                'Count:'.$sum;
                $sum1=$mul+$mul1+$mul2+$mul3+$mul4+$mul5+$mul6+$mul7+$mul8+$mul9;
                'Sum:'.$sum1;
                'grace: '.$grace;
                $date=$val[0]['date'];
                $resArr[$date]['renewalTotal_revenue']=$sum1;
                $resArr[$date]['Recount'] = $sum;
                //$resArr[$date]['grace'] = $grace;
                //echo '<pre>';print_r($resArr);die;
			}
			/* Airtel Code Starts */
			if($_GET['biller_id']=='at')
			{
				if($publisher=='bnama')
			    {
					$countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and product_id not in(540845,540846,540847,540848) ");	
				}
				else
			    {
				 $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and product_id  in(540845,540846,540847,540848) "); 
			    }
				$sum=0;
				foreach($countrevenue as $key=>$val)
				{
                    $sum=$val[0]['activation_sum'];
				    $resArr[$date]['total_revenue_ac']=0;
                    $resArr[$date]['count_revenue_ac']=$val[0]['activation_count'];
				    
					if(!empty($sum))
					{
                       $resArr[$date]['total_revenue_ac']=$sum;
					}
				}
				if($publisher=='bnama')
			    {
		           
				   $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."' and message='Success' and product_id not in(540845,540846,540847,540848) ");
				}
				else
			    {
				   $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."' and message='Success' and product_id in(540845,540846,540847,540848) ");
			   }
			   $sum=0;
			   foreach($airtel_renewal as $key=>$val)
		       {
			        $sum=$val[0]['renewal_sum'];
			        $resArr[$date]['total_renewal_ac']=0;
			        $resArr[$date]['count_renewal_ac']=$val[0]['renewal_count'];
			        if(!empty($sum))
			        {
			           $resArr[$date]['total_renewal_ac']=$val[0]['renewal_sum'];
					}
				}
				if($publisher=='bnama')
		        {
		           $deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."'  and response_code='1001' and product_id not in(540845,540846,540847,540848) ");
		        }
				else
		        {
				   $deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$stDate."' and date<='".$edDate."'  and response_code='1001'  and product_id in(540845,540846,540847,540848) ");
		        }
				 foreach($deactivateat as $key=>$val)
                {
                        $resArr[$date]['count_unsub']=$val[0]['count'];
                }
			}
			/* Airtel Code End */
			/* Reliance Code Start */
			if($_GET['biller_id']=='rl')
			{
				$countrevenue=$this->Transaction99bhojpuri->query("Select  count(`charged_price`) as activation_count, sum(`charged_price`) as activation_sum from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and charged_price>0 and activity='subscribed' and message!='subscription already opened.' and status='success'");
				
				foreach($countrevenue as $key=>$val)
				{
                            $sum=$val[0]['activation_sum'];
						$resArr[$date]['total_revenue_ac']=0;
                        $resArr[$date]['count_revenue_ac']=$val[0]['activation_count'];
						
						if(!empty($sum)){
                        $resArr[$date]['total_revenue_ac']=$sum;
						}
						
				}
				$relience_renewal=$this->Transaction99bhojpuri->query("Select  count(`charged_price`) as renewal_count, sum(`charged_price`) as renewal_sum  from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and activity='renewal' and status='success' ");
				$sum=0;
		        foreach($relience_renewal as $key=>$val)
		        {
			        $sum=$val[0]['renewal_sum'];
			        $resArr[$date]['total_renewal_ac']=0;
			        $resArr[$date]['count_renewal_ac']=$val[0]['renewal_count'];
			        if(!empty($sum))
			        {
			          $resArr[$date]['total_renewal_ac']=$val[0]['renewal_sum'];
			        }
		        }
				$parking=$this->Transaction99bhojpuri->query("Select  count(*) as count from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and activity='subscribed' and (status='pending' or status='failed')  ");
				foreach($parking as $key=>$val)
                {
                        $resArr[$date]['count_sub2']=$val[0]['count'];
                }
				$deactivateat=$this->Transaction99bhojpuri->query("Select  count(*) as count from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and activity='unsubscription' and status='success'  ");
				foreach($deactivateat as $key=>$val)
                {
                        $resArr[$date]['count_unsub']=$val[0]['count'];
                }
				
			}
			error_log("dailyreport_cms99_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Reliance Code End */
			$this->set(compact('totalhits','resArr'));
		}
	}
	public function dailyreportdetail_cms_99()
	{
		$user_id = $this->Session->read('User.id');
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher'];
			
			error_log("dailyreportdetail_cms_99_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Total User Hits with msisdn*/
			$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,interface,pack_id from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' group by interface order by count(*) desc");
			foreach($totalhits as $key=>$val)
		    {
			 $campaignname = $this->UserHit99bhojpuri->query("select campaign_name from bp_packs where pack_id ='".$val['bp_user_hits']['pack_id']."'");
			
			$interface=$val['bp_user_hits']['interface'];
			$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
			$resArr[$date][$interface]['total_hit']=$val[0]['count'];
			$resArr[$date][$interface]['pack_id']=$val['bp_user_hits']['pack_id'];
			$resArr[$date][$interface]['campaign_name'] = $campaignname[0]['bp_packs']['campaign_name'];
		    }	
			/* CG OK CODE */
		    if($_GET['biller_id'] == 'vf' )
			{ 
		        $str= " and cg_response_id!='0'";
			}
			elseif($_GET['biller_id'] == 'at')
			{
				$str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
			}
			elseif($_GET['biller_id'] == 'ac')
			{ 
			   $str= " and cg_response_id!='0' AND cg_response_id!='NULL'";
		    }
			else
			{
				$str='';
			}
		    if($_GET['biller_id'] != 'rl')
			{
               
			   $cgok = $this->Transaction99bhojpuri->query("select count(*) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str group by interface ");
               foreach($cgok as $key=>$val)
		       {	
			      $interface=$val['bp_transaction']['interface'];
			      $resArr[$date][$interface]['interface']=$interface;
			      $resArr[$date][$interface]['cg_ok']=$val[0]['count'];
			   } 
		    }
			 /* Consent */
		    $consent=$this->Transaction99bhojpuri->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and consent='1'  group by interface");
			
			foreach($consent as $key=>$val)
			 {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['consent']=$val[0]['count'];
			 } //echo '<pre>';print_r($resArr);die;
			 
			 /* Billing Done Count and Total Revenue */
			if($_GET['biller_id'] == 'rl')
		    {
			
			 $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0  and billing_response!='subscription already opened.' group by interface");
	        }
			else
		    {
			 $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 group by interface");
	        }
			foreach($billing_done as $key=>$val)
	        {
			$interface=$val['bp_transaction']['interface'];	
		    $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			$resArr[$date][$interface]['billing_done']=$val[0]['count'];
		    }
			
			$mobile_notfound=$this->UserHit99bhojpuri->query("select count(*) as count, interface from bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and (msisdn='' OR msisdn='NA' OR msisdn='1234') group by interface");
			
			foreach($mobile_notfound as $key=>$val)
            {
						$interface=$val['bp_user_hits']['interface'];
                        $resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
                        $resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
            }
			if($_GET['biller_id'] == 'rl')
		    {
			  $callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and  transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' group by interface");
		     }
		     else
		     {
			  $callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0 group by interface");
		     }
			 foreach($callback_status as $key=>$val)
		     {
						$interface=$val['bp_transaction']['interface'];
                        $resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
                        $resArr[$date][$interface]['callback_status']=$val[0]['count'];
		     }
			 error_log("dailyreportdetail_cms_99_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			 
			 $this->set(compact('totalhits','resArr'));
			
		}
	}
	public function dailyreport_old_cms99()
	{
		$user_id = $this->Session->read('User.id');
		//$_GET['publisher'] = 'bnama';
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher']; 
        
		error_log("dailyreport_old_cms99_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		
		/* Total User Hits with msisdn*/
		$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count from bp_user_hits_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."'");
		foreach($totalhits as $key=>$val)
		{
			$resArr[$date]['total_hit']=$val[0]['count'];
			$resArr[$date]['date']=$date;

		}		

		/* Total User Hits without msisdn*/
		$totalhits = $this->UserHit99bhojpuri->query("select count(*) as count from bp_user_hits_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and (msisdn='' or msisdn='NA')");
		foreach($totalhits as $key=>$val)
		{
			$resArr[$date]['mobilenotfound_hit']=$val[0]['count'];
			$resArr[$date]['date']=$date;

		}	
		/* CG OK CODE */
			if($_GET['biller_id'] == 'vf' ){ $str= " and cg_response_id!='0'";
			}elseif($_GET['biller_id'] == 'at'){ $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
			}elseif($_GET['biller_id'] == 'ac'){ $str= " and cg_response_id!='0' AND cg_response_id!='NULL'";
			}else{$str='';}
	    $cgok = $this->Transaction99bhojpuri->query("select count(*) as count  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str");
        foreach($cgok as $key=>$val)
		{	
			$resArr[$date]['cg_ok']=$val[0]['count'];
			$resArr[$date]['date']=$date;
		}
		/* Billing Done Count and Total Revenue */
		 if($_GET['biller_id']=='rl')
		 {
			 $billing_done=$this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and billing_response!='subscription already opened.' and price_deducted>0");
	     foreach($billing_done as $key=>$val)
	     {
		    $resArr[$date]['total_revenue']=$val[0]['sum'];
			$resArr[$date]['billing_done']=$val[0]['count'];
			$resArr[$date]['date']=$date;
		  }
		 }
		 else
		 {
		// echo "select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 "; die;
		 $billing_done=$this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 ");
	     foreach($billing_done as $key=>$val)
	     {
		    $resArr[$date]['total_revenue']=$val[0]['sum'];
			$resArr[$date]['billing_done']=$val[0]['count'];
			$resArr[$date]['date']=$date;
		  }
		  $parking=$this->Transaction99bhojpuri->query("select count(*) as count from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted='0' ");
foreach($parking as $key=>$val)
	     {
			$resArr[$date]['count_sub2']=$val[0]['count'];
			$resArr[$date]['date']=$date;
		 }
		 }
		 if($_GET['biller_id']=='rl')
		 {
		$callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1'");
		 foreach($callback_status as $key=>$val)
		 {
			$resArr[$date]['callback_status']=$val[0]['count'];
			$resArr[$date]['date']=$date; 
		 }
		 }
		 else
		 {
			 $callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction_report where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0");
		 foreach($callback_status as $key=>$val)
		 {
			$resArr[$date]['callback_status']=$val[0]['count'];
			$resArr[$date]['date']=$date; 
		 }
		 }
			if($_GET['biller_id']=='vf')
		{
		 $callback=$this->Transaction99bhojpuri->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE CREATED>='".$stDate."' and CREATED<='".$edDate."' and `ACTION` = 'act' and `STATUS` = 'success'   group by charging_mode,TXNID,DATE(CREATED)");
		$actual_price_deduct=0; $count=0;
                foreach($callback as $key=>$val)
                {                   
			  $actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
                    $date=$val[0]['date'];
                        $count++;

                }
 $date=$val[0]['date'];
$resArr[$date]['count_revenue']=$count;
$resArr[$date]['callback_revenue'] = $actual_price_deduct;
	$parking=$this->Transaction99bhojpuri->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE  CREATED>='".$stDate."' and CREATED<='".$edDate."' group by charging_mode,TXNID,DATE(CREATED)");
                         $count=0; $null=0; $null=0; $suspend=0;
        foreach($parking as $key=>$val)
                {
                        if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
                        {
                        $count++;
                        }
                        if($val['vodafone_callback_notifications']['charging_mode']=='null')
                        {
                          $null++;
                        }
                         if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
                        {
                          $suspend++;
                        }
                }
 'Count: '.$count;
 $deactivate=$null+$suspend;'deactivate: '.$deactivate;
 $date=$val[0]['date'];
$resArr[$date]['parking']=$count;
$resArr[$date]['deactivate'] = $deactivate;
//echo'<pre>';print_r($resArr);die;
		
		$update=$this->Transaction99bhojpuri->query("SELECT count(*),charging_mode,TXNID FROM `vodafone_update_notifications` WHERE  CREATED>='".$stDate."' and CREATED<='".$edDate."' group by charging_mode,TXNID,DATE(CREATED)");
                  $count=0;$daily=0;$monthly=0;$weekly=0;$days=0;$daily1=0;$daily2=0;$daily3=0;$daily5=0;$days3=0;$grace=0;
                foreach($update as $key=>$val)
                {
                          $fifteen_days= $val['vodafone_update_notifications']['charging_mode'];


                        if($val['vodafone_update_notifications']['charging_mode']=='15DAYS')
                        {
                        $count++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY')
                        {
                         $daily++;

                        }elseif($val['vodafone_update_notifications']['charging_mode']=='WEEKLY')
                        {
                        $weekly++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='MONTHLY')
                        {
                        $monthly++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='3DAYS')
                        {
                        $days3++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='5DAYS')
                        {
                        $days++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY1')
                        {
                        $daily1++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY2')
                        {
                        $daily2++;
						}elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY3')
                        {
                        $daily3++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='DAILY5')
                        {
                        $daily5++;
                        }elseif($val['vodafone_update_notifications']['charging_mode']=='GRACE')
                        {
                        $grace++;
                        }
	
                }

					  'Count: '.$count; 'Value:' .$mul=$count*30;
					  'Count Daily: '.$daily; 'Value:' .$mul1=$daily*5;
					 'Count weekly: '.$weekly; 'Value:' .$mul2=$weekly*15;
					  'Count monthly: '.$monthly; 'Value:' .$mul3=$monthly*50;
					  'Count days: '.$days; 'Value:' .$mul4=$days*10;
					 'Count days3: '.$days3; 'Value:' .$mul9=$days3*7;
					  'Count daily1: '.$daily1; 'Value:' .$mul5=$daily1*1;
					 'Count daily2: '.$daily2; 'Value:' .$mul6=$daily2*2;
					 'Count daily3: '.$daily3; 'Value:' .$mul7=$daily3*3;
					 'Count daily5: '.$daily5; 'Value:' .$mul8=$daily5*5;

$sum=$count+$daily+$weekly+$monthly+$days+$daily1+$daily2+$daily3+$daily5+$days3;
 'Count:'.$sum;
$sum1=$mul+$mul1+$mul2+$mul3+$mul4+$mul5+$mul6+$mul7+$mul8+$mul9;
 'Sum:'.$sum1;
'grace: '.$grace;
 $date=$val[0]['date'];
$resArr[$date]['renewalTotal_revenue']=$sum1;
$resArr[$date]['Recount'] = $sum;
//$resArr[$date]['grace'] = $grace;
//echo '<pre>';print_r($resArr);die;
}
		if($_GET['biller_id']=='ac')
         {
		  $countrevenue=$this->Transaction99bhojpuri->query("Select count(*) as count,sum(amountcharged)  as sum,notifytype from  aircel_sdp_notifications where created>='".$stDate."' and created<='".$edDate."' and (notifytype='SUB-1' or notifytype='RENEW-1' or  notifytype='UNSUB-1' or notifytype='SUB-2') group by  notifytype");
			//print_r($countrevenue);
			foreach($countrevenue as $val)
             	{
                    
					if($val['aircel_sdp_notifications']['notifytype'] == 'RENEW-1'){

						$resArr[$date]['count_renewal_ac']=$val[0]['count'];
						$resArr[$date]['total_renewal_ac']=$val[0]['sum'];

					}elseif($val['aircel_sdp_notifications']['notifytype'] == 'SUB-1'){
						$resArr[$date]['count_revenue_ac']=$val[0]['count'];
						$resArr[$date]['total_revenue_ac']=$val[0]['sum'];

					}elseif($val['aircel_sdp_notifications']['notifytype'] == 'SUB-2'){
						$resArr[$date]['count_sub2']= $val[0]['count'];
					}elseif($val['aircel_sdp_notifications']['notifytype'] == 'UNSUB-1'){
						$resArr[$date]['count_unsub']= $val[0]['count'];
					}
					                    
                       
                  }

			//print_r($countrevenue);die;
		  
		}
		if($_GET['biller_id']=='at'){
			 if($publisher=='bnama')
			 {
				 //echo "Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and product_id not in(540845,540846,540847,540848)";
				//echo "Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and product_id not in(540845,540846,540847,540848) "; die; 
			 $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications_report where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and product_id not in(540845,540846,540847,540848) ");
			 }
			 else
			 {
				 $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications_report where  date>='".$stDate."' and date<='".$edDate."' and message='AOC: Success' and amount_charged>0 and product_id  in(540845,540846,540847,540848) "); 
			 }
			 $sum=0;
			foreach($countrevenue as $key=>$val)
					{
                            $sum=$val[0]['activation_sum'];
						    $resArr[$date]['total_revenue_ac']=0;
                        $resArr[$date]['count_revenue_ac']=$val[0]['activation_count'];
						
						if(!empty($sum)){
                        $resArr[$date]['total_revenue_ac']=$sum;
						}
						
			}
			if($publisher=='bnama')
			{
		 $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications_report where date>='".$stDate."' and date<='".$edDate."' and message='Success' and product_id not in(540845,540846,540847,540848) ");
			}
			else
			{
				$airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications_report where date>='".$stDate."' and date<='".$edDate."' and message='Success' and product_id in(540845,540846,540847,540848) ");
			}
		$sum=0;
		 foreach($airtel_renewal as $key=>$val)
		{
			  $sum=$val[0]['renewal_sum'];
			  $resArr[$date]['total_renewal_ac']=0;
			$resArr[$date]['count_renewal_ac']=$val[0]['renewal_count'];
			if(!empty($sum))
			{
			$resArr[$date]['total_renewal_ac']=$val[0]['renewal_sum'];
			}
		}
		if($publisher=='bnama')
		{
		$deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications_report where date>='".$stDate."' and date<='".$edDate."'  and response_code='1001' and product_id not in(540845,540846,540847,540848) ");
		}
		else
		{
			
				$deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications_report where date>='".$stDate."' and date<='".$edDate."'  and response_code='1001'  and product_id in(540845,540846,540847,540848) ");
		}
                 foreach($deactivateat as $key=>$val)
                {
                        $resArr[$date]['count_unsub']=$val[0]['count'];
                }
		
		}
if($_GET['biller_id']=='rl'){

	//"Select  count(`charged_price`) as activation_count, sum(`charged_price`) as activation_sum from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and activity='subscribed' and message!='subscription already opened.' and status='success'"
			 $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`charged_price`) as activation_count, sum(`charged_price`) as activation_sum from adpay_notification_report where date>='".$stDate."' and date<='".$edDate."' and charged_price>0 and activity='subscribed' and message!='subscription already opened.' and status='success'"); 
			 //$sum=0;
			foreach($countrevenue as $key=>$val)
					{
                            $sum=$val[0]['activation_sum'];
						$resArr[$date]['total_revenue_ac']=0;
                        $resArr[$date]['count_revenue_ac']=$val[0]['activation_count'];
						
						if(!empty($sum)){
                        $resArr[$date]['total_revenue_ac']=$sum;
						}
						
					}
					
			//echo "Select  count(`charged_price`) as renewal_count, sum(`charged_price`) as renewal_sum  from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and activity='renewal' and status='Success' ";die;
		 $relience_renewal=$this->Transaction99bhojpuri->query("Select  count(`charged_price`) as renewal_count, sum(`charged_price`) as renewal_sum  from adpay_notification_report where date>='".$stDate."' and date<='".$edDate."' and activity='renewal' and status='success' ");
		$sum=0;
		 foreach($relience_renewal as $key=>$val)
		{
			  $sum=$val[0]['renewal_sum'];
			  $resArr[$date]['total_renewal_ac']=0;
			$resArr[$date]['count_renewal_ac']=$val[0]['renewal_count'];
			if(!empty($sum))
			{
			$resArr[$date]['total_renewal_ac']=$val[0]['renewal_sum'];
			}
		}
		//echo "Select  count(*) as count from adpay_notification where date>='".$stDate."' and date<='".$edDate."' and activity='subscribed' and (status='pending' or status='failed')";die;
		$parking=$this->Transaction99bhojpuri->query("Select  count(*) as count from adpay_notification_report where date>='".$stDate."' and date<='".$edDate."' and activity='subscribed' and (status='pending' or status='failed')  ");
                 foreach($parking as $key=>$val)
                {
                        $resArr[$date]['count_sub2']=$val[0]['count'];
                }
		$deactivateat=$this->Transaction99bhojpuri->query("Select  count(*) as count from adpay_notification_report where date>='".$stDate."' and date<='".$edDate."' and activity='unsubscription' and status='success'  ");
                 foreach($deactivateat as $key=>$val)
                {
                        $resArr[$date]['count_unsub']=$val[0]['count'];
                }
		
}
error_log("dailyreport_old_cms99_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		$this->set(compact('totalhits','resArr'));
            //$this->set('res',$resArr);		
        }
	}
	
	public function dailyreportdetail_old_cms_99()
	{
		//$_GET['publisher'] = 'bnama';
		$user_id = $this->Session->read('User.id');
		if(!empty($_GET['biller_id']) && !empty($_GET['publisher']))
		{
			$date = $_GET['startDate'];
			$stDate = $_GET['startDate'].' 00:00:00';
			$edDate =  $_GET['startDate'].' 23:59:59';
			$publisher = $_GET['publisher'];

            error_log("dailyreportdetail_old_cms_99_start | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
			
			/* Total User Hits with msisdn*/
			 $totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,interface,pack_id from bp_user_hits_report as bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' group by interface order by count(*) desc");
		foreach($totalhits as $key=>$val)
		{
			$campaignname = $this->UserHit99bhojpuri->query("select campaign_name from bp_packs where pack_id ='".$val['bp_user_hits_report']['pack_id']."'"); 
			
			$interface=$val['bp_user_hits']['interface'];
			$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
			$resArr[$date][$interface]['total_hit']=$val[0]['count'];
			$resArr[$date][$interface]['pack_id']=$val['bp_user_hits_report']['pack_id'];
			$resArr[$date][$interface]['campaign_name'] = $campaignname[0]['bp_packs']['campaign_name'];
		}	

		/* CG OK CODE */
		if($_GET['biller_id'] == 'vf' ){ $str= " and cg_response_id!='0'";
			}elseif($_GET['biller_id'] == 'at'){ $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
			}elseif($_GET['biller_id'] == 'ac'){ $str= " and cg_response_id!='0' AND cg_response_id!='NULL'";
		    }else{$str='';}
		if($_GET['biller_id'] != 'rl'){
            $cgok = $this->Transaction99bhojpuri->query("select count(*) as count,interface  from bp_transaction_report as bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str group by interface ");
        foreach($cgok as $key=>$val)
		{	
			$interface=$val['bp_transaction']['interface'];
			$resArr[$date][$interface]['interface']=$interface;
			$resArr[$date][$interface]['cg_ok']=$val[0]['count'];
		}
		}

		/* Billing Done Count and Total Revenue */
		 if($_GET['biller_id'] == 'rl')
		 {
			
			 $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction_report as bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0  and billing_response!='subscription already opened.' group by interface");
	   
		 }
		 else
		 {
			 $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction_report as bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and price_deducted>0 group by interface");
	    
		 }
		 foreach($billing_done as $key=>$val)
	     {
			$interface=$val['bp_transaction']['interface'];	
		    $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			$resArr[$date][$interface]['billing_done']=$val[0]['count'];
		  }
			$mobile_notfound=$this->UserHit99bhojpuri->query("select count(*) as count, interface from bp_user_hits_report as bp_user_hits where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and date>='".$stDate."' and date<='".$edDate."' and (msisdn='' OR msisdn='NA' OR msisdn='1234') group by interface");
		 foreach($mobile_notfound as $key=>$val)
             {
						$interface=$val['bp_user_hits']['interface'];
                        $resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
                        $resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
                 }
             if($_GET['biller_id'] == 'rl')
		 {
			  $callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction_report as bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and  transaction_time>='".$stDate."' and transaction_time<='".$edDate."' and billing_response_time>='".$stDate."' and billing_response_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' group by interface");
		
		 }
		 else
		 {
			  $callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction_report as bp_transaction where biller_id='".$_GET['biller_id']."' and publisher = '".$publisher."' and transaction_time>='".$stDate."' and transaction_time<='".$edDate."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0 group by interface");
		
		 }
		
		 foreach($callback_status as $key=>$val)
		 {
						$interface=$val['bp_transaction']['interface'];
                        $resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
                        $resArr[$date][$interface]['callback_status']=$val[0]['count'];
		 }
		 error_log("dailyreportdetail_old_cms_99_end | Biller_Id =".$_GET['biller_id']." | publisher=".$publisher." | stDate =".$stDate." | edDate=".$edDate." | User_Id=".$user_id."");
		 
		 $this->set(compact('totalhits','resArr'));
		}//closing of if 
	}
	public function paytm_user_report($page=0)
	{
		  
		
	    //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		//paging section end here...
		
		$prevDate = date('Y-m-d', strtotime(' -1 day'));
		if (isset($_GET['search']) && $_GET['search'] == 'search')
		{	
	       $startDate = $_GET['startDate'] . " " ."00:00:00";
           $endDate = $_GET['endDate'] . " " . "23:59:59";
		   $searchCondStr .=" where created <= '".$endDate."' and created >= '".$startDate."'";
		}
		else if(isset($_GET['download']) && $_GET['download'] == 'download')
		{
			$startDate = $_GET['startDate'] . " " ."00:00:00";
            $endDate = $_GET['endDate'] . " " . "23:59:59";
			$this->layout = null;
	        $this->autoLayout = false;
	        $this->autoRender = false;
	        
	
			$userdetails = $this->Contentusedreport->query("select * from user_paytm_report where created >='".$startDate."' and created<='".$endDate."' order by signup asc");
	        
			$date = date('Y-m-d h:i:s');
	        $filename = "paytmreport".$date.".csv";
	        $csv_file = fopen('php://output', 'w');
	        header('Content-type: application/csv');
	        header('Content-Disposition: attachment; filename="'.$filename.'"');
	        
			$header_row = array("User ID","Cell","Email","Location","Sex","Signup Date","Videos Views","Payment Page","Subscription","Subscription Value","Last login");
	        
			fputcsv($csv_file,$header_row,',','"');
	        foreach($userdetails as $userdetails=>$obj)
	        {
				if(!empty($obj['user_paytm_report']['user_id']))
				{
					$User_Id = $obj['user_paytm_report']['user_id'];
			    }
				else
				{
					$User_Id = $obj['user_paytm_report']['id'];
				}
				if(!empty($obj['user_paytm_report']['msisdn']))
				{
					$msisdn = $obj['user_paytm_report']['msisdn'];
				}
				else
				{
				    $msisdn = "NA";
				}
				if(!empty($obj['user_paytm_report']['email']))
				{
					$email = $obj['user_paytm_report']['email'];
				}
				else
				{
				    $email = "NA";
				}
				if(!empty($obj['user_paytm_report']['location']))
				{
					$location = $obj['user_paytm_report']['location'];
				}
				else
				{
				    $location = "NA";
				}
				if(!empty($obj['user_paytm_report']['sex']))
				{
					$sex = $obj['user_paytm_report']['sex'];
				}
				else
				{
				    $sex = "NA";
				}
				if(!empty($obj['user_paytm_report']['Signup']))
				{
					$Signup = $obj['user_paytm_report']['Signup'];
				}
				else
				{
				    $Signup = "NA";
				}
				if($obj['user_paytm_report']['paytm_page']== 1)
				{
					$paytm_page = "Yes";
				}
				else
				{
				   $paytm_page = "No";
				}
				if($obj['user_paytm_report']['subscription']== 1)
				{
					$subscription = "Yes";
				}
				else
				{
				   $subscription = "No";
				}
				if(!empty($obj['user_paytm_report']['last_login']))
				{
					$last_login = $obj['user_paytm_report']['last_login'];
				}
				else
				{
				   $last_login = "NA";
				}
		            $row = array(
								$obj['user_paytm_report']['id'],
								$msisdn,
								$email,
								$location,
								$sex,
								$Signup,
								$obj['user_paytm_report']['video_views'],
								$paytm_page,
								$subscription,
								$obj['user_paytm_report']['subscription_exp_value'],
								$last_login
								);								
                    fputcsv($csv_file,$row,',','"');
			}
	        fclose($csv_file);
		}
		else 
		{
			$startDate = $prevDate . " " ."00:00:00";
            $endDate = $prevDate . " " . "23:59:59";
			$searchCondStr .=" where created <= '".$endDate."' and created >= '".$startDate."'";
		}
		
		//echo "select * from user_paytm_report $searchCondStr order by id desc limit $offSet, $recPerPage";
		//die;
		
		$categories = $this->Contentusedreport->query("select * from user_paytm_report $searchCondStr order by id desc limit $offSet, $recPerPage");
		
		$cntCategories = $this->Contentusedreport->query("select count(*) as countRec from user_paytm_report $searchCondStr");
		
			
		$totalRec = @$cntCategories[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		$this->set(compact('categories','categories'));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	}
	public function paytm_revenue_report($page=0)
	{
	    //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		//paging section end here...
		
		$prevDate = date('Y-m-d', strtotime(' -1 day'));
		if (isset($_GET['search']) && $_GET['search'] == 'search')
		{	
	       
	       $startDate = $_GET['startDate'] . " " ."00:00:00";
           $endDate = $_GET['endDate'] . " " . "23:59:59";
		   $searchCondStr .=" where renewal_date <= '".$endDate."' and renewal_date >= '".$startDate."' and paytm_page=1 and subscription=1";
		   
		}
		else if(isset($_GET['download']) && $_GET['download'] == 'download')
		{
			$startDate = $_GET['startDate'] . " " ."00:00:00";
            $endDate = $_GET['endDate'] . " " . "23:59:59";
			$this->layout = null;
	        $this->autoLayout = false;
	        $this->autoRender = false;
	        
	        
			$userdetails = $this->Contentusedreport->query("select * from user_paytm_report where renewal_date >='".$startDate."' and renewal_date<='".$endDate."' and paytm_page=1 and subscription=1 order by signup asc");
	        
			$date = date('Y-m-d h:i:s');
	        $filename = "paytmrevenuereport".$date.".csv";
	        $csv_file = fopen('php://output', 'w');
	        header('Content-type: application/csv');
	        header('Content-Disposition: attachment; filename="'.$filename.'"');
	        
			$header_row = array("User ID","Cell","Email","Location","Sex","Signup Date","Videos Views","Payment Page","Subscription","Subscription Value","Cancellation","Cancellation Date","Renewal","Payment Mode","Last login");
	        
			fputcsv($csv_file,$header_row,',','"');
	        foreach($userdetails as $userdetails=>$obj)
	        {
				if(!empty($obj['user_paytm_report']['user_id']))
				{
					$User_Id = $obj['user_paytm_report']['user_id'];
			    }
				else
				{
					$User_Id = $obj['user_paytm_report']['id'];
				}
				if(!empty($obj['user_paytm_report']['msisdn']))
				{
					$msisdn = $obj['user_paytm_report']['msisdn'];
				}
				else
				{
				    $msisdn = "NA";
				}
				if(!empty($obj['user_paytm_report']['email']))
				{
					$email = $obj['user_paytm_report']['email'];
				}
				else
				{
				    $email = "NA";
				}
				if(!empty($obj['user_paytm_report']['location']))
				{
					$location = $obj['user_paytm_report']['location'];
				}
				else
				{
				    $location = "NA";
				}
				if(!empty($obj['user_paytm_report']['sex']))
				{
					$sex = $obj['user_paytm_report']['sex'];
				}
				else
				{
				    $sex = "NA";
				}
				if(!empty($obj['user_paytm_report']['Signup']))
				{
					$Signup = $obj['user_paytm_report']['Signup'];
				}
				else
				{
				    $Signup = "NA";
				}
				if($obj['user_paytm_report']['paytm_page']== 1)
				{
					$paytm_page = "Yes";
				}
				else
				{
				   $paytm_page = "No";
				}
				if($obj['user_paytm_report']['subscription']== 1)
				{
					$subscription = "Yes";
				}
				else
				{
				   $subscription = "No";
				}
				if($obj['user_paytm_report']['cancel_status']== 1)
				{
					$Cancellation = "Yes";
				}
				else
				{
				   $Cancellation = "No";
				}
				if(!empty($obj['user_paytm_report']['cancellation_date']))
				{
					$cancellation_date = $obj['user_paytm_report']['cancellation_date'];
				}
				else
				{
				   $cancellation_date = "NA";
				}
				if($obj['user_paytm_report']['renewal']== 1)
				{
					$renewal = "Yes";
				}
				else
				{
				   $renewal = "No";
				}
				if(!empty($obj['user_paytm_report']['last_login']))
				{
					$last_login = $obj['user_paytm_report']['last_login'];
				}
				else
				{
				   $last_login = "NA";
				}
		            $row = array(
								$obj['user_paytm_report']['id'],
								$msisdn,
								$email,
								$location,
								$sex,
								$Signup,
								$obj['user_paytm_report']['video_views'],
								$paytm_page,
								$subscription,
								$obj['user_paytm_report']['subscription_act_value'],
								$Cancellation,
								$cancellation_date,
								$renewal,
								$obj['user_paytm_report']['payment_mode'],
								$last_login
								);								
                    fputcsv($csv_file,$row,',','"');
			}
	        fclose($csv_file);
		}
		else 
		{
			$startDate = $prevDate . " " ."00:00:00";
            $endDate = $prevDate . " " . "23:59:59";
			$searchCondStr .=" where renewal_date <= '".$endDate."' and renewal_date >= '".$startDate."' and paytm_page=1 and subscription=1";
		}
		
	
		$categories = $this->Contentusedreport->query("select * from user_paytm_report $searchCondStr order by signup asc limit $offSet, $recPerPage");
		
		$cntCategories = $this->Contentusedreport->query("select count(*) as countRec from user_paytm_report $searchCondStr");
		
			
		$totalRec = @$cntCategories[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		$this->set(compact('categories','categories'));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	}
		public function vas_summary_report($page=0)
	{
		 //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		//paging section end here...
		
		$biller_id = $_GET['biller_id'];
		$publisher = $_GET['publisher'];
		
		$prevDate = date('Y-m-d', strtotime(' -1 day'));
		if (isset($_GET['search']) && $_GET['search'] == 'search')
		{	
	       //$startDate = $_GET['startDate'] . " " ."00:00:00";
           //$endDate = $_GET['endDate'] . " " . "23:59:59";
		   $startDate = $_GET['startDate'];
           $endDate = $_GET['endDate'];
		   
		   $searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and biller_id='".$biller_id."' and publisher = '".$publisher."'";
		}
		else if(isset($_GET['download']) && $_GET['download'] == 'download')
		{
			//$startDate = $_GET['startDate'] . " " ."00:00:00";
            //$endDate = $_GET['endDate'] . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$this->layout = null;
	        $this->autoLayout = false;
	        $this->autoRender = false;
	         
		    $userdetails = $this->Contentusedreport->query("select * from vas_summary_report where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and biller_id='".$biller_id."' and publisher = '".$publisher."'");			
			
			$date = date('Y-m-d h:i:s');
	        $filename = "summaryreport".$date.".csv";
	        $csv_file = fopen('php://output', 'w');
	        header('Content-type: application/csv');
	        header('Content-Disposition: attachment; filename="'.$filename.'"');
			
	        
			$header_row = array("Date","Operator","Service","Clicks","CG OK","Activation","Activation Revenue","Call Back","Parking","Parking Revenue","Renewal","Renewal Revenue","Total Revenue","Parking in Queue","Deactivation");
	        
			fputcsv($csv_file,$header_row,',','"');
	        foreach($userdetails as $userdetails=>$obj)
	        {
		            $row = array(
								$obj['vas_summary_report']['reportdate'],
								$obj['vas_summary_report']['biller_id'],
								$obj['vas_summary_report']['publisher'],
								$obj['vas_summary_report']['Clicks'],
								$obj['vas_summary_report']['cgok'],
								$obj['vas_summary_report']['Activation'],
								$obj['vas_summary_report']['activationrevenue'],
								$obj['vas_summary_report']['callback'],
								$obj['vas_summary_report']['parking'],
								$obj['vas_summary_report']['parkingrevenue'],
								$obj['vas_summary_report']['renewal'],
								$obj['vas_summary_report']['renewalrevenue'],
								$obj['vas_summary_report']['totalrevenue'],
								$obj['vas_summary_report']['parkinginqueue'],
								$obj['vas_summary_report']['deactivate']
								);								
                    fputcsv($csv_file,$row,',','"');
			}
	        fclose($csv_file);
		}
		else 
		{
			//$startDate = $prevDate . " " ."00:00:00";
            //$endDate = $prevDate . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and biller_id='".$biller_id."' and publisher = '".$publisher."'";
		}
		
		
		$categories = $this->Contentusedreport->query("select * from vas_summary_report $searchCondStr order by id desc limit $offSet, $recPerPage");
		
		$cntCategories = $this->Contentusedreport->query("select count(*) as countRec from vas_summary_report $searchCondStr");
		
			
		$totalRec = @$cntCategories[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		$this->set(compact('categories','categories'));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	}
	public function vas_summary_report_new($page=0)
	{
		 //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		//paging section end here...
		
		$biller_id = $_GET['biller_id'];
		$publisher = $_GET['publisher'];
		
		$prevDate = date('Y-m-d', strtotime(' -1 day'));
		$currDate = date('Y-m-d');
		if (isset($_GET['search']) && $_GET['search'] == 'search')
		{	
		   $startDate = $_GET['startDate'];
           $endDate = $_GET['endDate'];
		   if(($startDate == $currDate) || ($endDate == $currDate))
		   {
			   
			   $startDate1 = $currDate . " " ."00:00:00";
               $endDate1 = $currDate . " " . "23:59:59";
			   
			   /* Total User Hits with msisdn*/
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."'");
		    
		           foreach($totalhits as $key=>$val)
		           {
			          $date = $val[0]['date'];					 
					  $resArr[$date]['Clicks']=$val[0]['count'];
			          $resArr[$date]['date']=$date;
			       }
			   }
			   else
			   {
				   $totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."'");
			   
			       foreach($totalhits as $key=>$val)
			       {
					$date = $val[0]['date'];
					$resArr[$date]['Clicks']=$val[0]['count'];
					$resArr[$date]['date']=$date;
			       }
			   }
			   
			   /* CG OK CODE */
			  
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   if($biller_id =='at')
			       {
				 $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
			      }
				  else
			   {
				$str='';
			   }
				 $cgok = $this->Transaction99bhojpuri->query("select count(*) as count  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str");
				 foreach($cgok as $key=>$val)
			   {	
				$resArr[$date]['cg_ok']=$val[0]['count'];
			   }
			   }
			   else 
			   {
				   if($biller_id == 'vf'|| $biller_id == 'ia')
			   { 
		         $str= " and cg_response_id!='0'";
			   }
			   
			   elseif($biller_id == 'tt')
			   { 
			    $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
			   }
			    elseif($biller_id == 'bl')
			   { 
			    $str= " and cg_response_id!='0' and cg_response_id!='FAIL'";
			   }
			   else
			   {
				$str='';
			   }
				 
				 $cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str ");
				 
				 foreach($cgok as $key=>$val)
			   {	
				$resArr[$date]['cg_ok']=$val[0]['count'];
			   }
			   }
			   
			   
			   
			   /* Billing Done Count */
			   
			   if($biller_id == 'at')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0");
				   foreach($billing_done as $key=>$val)
			       {
					    $resArr[$date]['Activation_revenue']=$val[0]['sum'];
			            $resArr[$date]['Activation']=$val[0]['count'];
			            $resArr[$date]['date']=$date;
				   }
			   }
			   else if($biller_id == 'rl')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and billing_response!='subscription already opened.' and price_deducted>0");
				   foreach($billing_done as $key=>$val)
			       {
					    $resArr[$date]['Activation_revenue']=$val[0]['sum'];
			            $resArr[$date]['Activation']=$val[0]['count'];
			            $resArr[$date]['date']=$date;
				   }
				
			   }
			   else
			   {
				   $billing_done = $this->Transaction77->query("select count(*) as count from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' ");
				   foreach($billing_done as $key=>$val)
			       {
				     $resArr[$date]['Activation']=$val[0]['count'];
			       }
				   
				   /* Total Revenue */
			
			       $total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE'");
			   
			       foreach($total_revenue as $key=>$val)
			       {
				    $resArr[$date]['Activation_revenue']=$val[0]['sum'];
			       }
			   }
			   
			   /* Callback Count */
			   if($biller_id == 'at')
			   {
				   $callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0");
			   }
			   else if($biller_id == 'rl')
			   {
				    $callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1'");
			   }
			   else
			   {
				   $callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE' and callback_status='1' ");
			   }
			  
			   foreach($callback_status as $key=>$val)
			   {
				$resArr[$date]['Callback']=$val[0]['count'];
			   }
			   
			   /* Biller Id Vodafone Start*/
			   if($biller_id =='vf')
			   {
				  if($publisher=='mini')
				  {
					$service_id='V3_MMOVI';
				  }
				  elseif($publisher=='vfcomics')	
				  {
					$service_id='V3_VCOM';
				  }
				  elseif($publisher=='firstcut')	
				  {
					$service_id='V3_FCUT';
				  }
				  else
				  {
					$service_id='V3_BHOJ';
			      }
				
				/* Callback Revenue */
				
				$callback=$this->Transaction77->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$startDate1."' and CREATED<='".$endDate1."' AND service_id LIKE '%$service_id%' and isblock=0  group by charging_mode,TXNID");
				
				$actual_price_deduct=0; $count=0;
				
				foreach($callback as $key=>$val)
				{
				    $actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
					$count++;
				}//echo '<pre>'; print_r($resArr);die;
				
				$resArr[$date]['total_activation']= $count;
				$resArr[$date]['total_activation_revenue'] = $actual_price_deduct;
				
				/* Parking In Queue Count */
				
				$parkinginqueue = $this->Transaction77->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE CREATED>='".$startDate1."' and CREATED<='".$endDate1."'  AND service_id LIKE '%$service_id%'  and isblock=0 group by charging_mode,TXNID,DATE(CREATED)");
				
				$count=0; $null=0; $null=0; $suspend=0;
				
				foreach($parkinginqueue as $key=>$val)
				{
					if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
					{
					    $count++;
					}
					if($val['vodafone_callback_notifications']['charging_mode']=='null')
					{
					    $null++;
					}
					if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
					{
					    $suspend++;
					}
				}
				'Count: '.$count;
				$deactivate = $null + $suspend;
				'deactivate: '.$deactivate;
				$resArr[$date]['parkinginqueue']=$count;
				$resArr[$date]['deactivate'] = $deactivate;
				
				$update=$this->Transaction77->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$startDate1."' and CREATED<='".$endDate1."' AND service_id LIKE '%$service_id%'");
				
				$sum1=$update['0']['0']['sum1'];
				$sum=$update['0']['0']['count'];
				
				$resArr[$date]['Renewal_revenue']=$sum1;
				$resArr[$date]['Renewal'] = $sum;
				
				$resArr[$date]['parking']=  $resArr[$date]['total_activation'] -  $resArr[$date]['Activation'];
				
				$resArr[$date]['parkingrevenue']=  $resArr[$date]['total_activation_revenue'] -  $resArr[$date]['Activation_revenue'];
				
				$resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				}
				
				/* Biller Id Idea Start*/
			
			    if($biller_id =='ia' && $publisher =='filmy')
			    {
				 $totalrenewal = $this->UserHit77->query("SELECT count( * ) as count , sum( price ) as sum ,date(created) as date, ACTION , `status` FROM  bp_idea_callback_notifications as idea WHERE created>='".$startDate1."' and created<='".$endDate1."' AND STATUS = 'SUCCESS' GROUP BY ACTION");
                
				 foreach($totalrenewal as $key=>$val)
				 {
		            if($val['idea']['ACTION']=='ACT')
					{
		              $resArr[$val[0]['date']]['total_activation']=$val[0]['count'];
                      $resArr[$val[0]['date']]['total_activation_revenue']=$val[0]['sum'];		
					}
		            if($val['idea']['ACTION']=='DCT')
					{
		              $resArr[$val[0]['date']]['deactivate']=$val[0]['count'];	
		            }
		            if($val['idea']['ACTION']=='REN')
					{
		              $resArr[$val[0]['date']]['Renewal']=$val[0]['count'];
                      $resArr[$val[0]['date']]['Renewal_revenue']=$val[0]['sum'];		
					}
				}	
                
				/* Parking in queue Count */
				
                $parkinginqueue = $this->UserHit77->query("SELECT count(*) as count FROM  bp_idea_callback_notifications as idea WHERE created>='".$startDate1."' and created<='".$endDate1."' AND action ='GRACE' and  STATUS = 'BAL-LOW'");
				
		        $resArr[$date]['parkinginqueue'] = $parkinginqueue[0][0]["count"];

                /* Parking */	
				 $resArr[$date]['parking'] = $resArr[$date]['total_activation'] - $resArr[$date]['Activation'];
				 
				 /* Parking Revenue */
				 $resArr[$date]['parkingrevenue'] = $resArr[$date]['total_activation_revenue'] - $resArr[$date]['Activation_revenue'];
				 
				/* Total Revenue */
				  
				$resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				 
			    }
				
				if($biller_id=='tt')
		        {
					if($publisher == 'mini')
					{
	                  $product = 'and PRODUCTID IN(0145037900,0145038100,0145038300,0145038500)';
	                }
	                elseif($publisher =='filmy')	
	                {
		              $product = 'and PRODUCTID IN(0143389800,0145021100,0145021300,0145021500,0145021700,0145021900)';
	                }
	                elseif($publisher == 'firstcut')	
	                {
		
		            $product = 'and PRODUCTID NOT IN(0145037900,0145038100,0145038300,0145038500,0143389800,0145021100,0145021300,0145021500,0145021700,0145021900)';
	                }
	                else
                    {
		               $product = '';
	                }
				   // $tata_subscriptioncount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='SUBSCRIPTION' and status='SUCCESS' $product");
					
					$tata_subscriptioncount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  date(created)='".$startDate1."' and service='SUBSCRIPTION' and status='SUCCESS'");
				   
				    foreach($tata_subscriptioncount as $key=>$val)
				   {
					$date=$val[0]['date'];
                    $resArr[$date]['total_activation'] = $val[0]['count'];
                    $resArr[$date]['date']=$val[0]['date'];
				   }
				
				   //$tata_subscriptionsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='SUBSCRIPTION' and status='SUCCESS' $product");
				   
				   $tata_subscriptionsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  date(created)='".$startDate1."'  and service='SUBSCRIPTION' and status='SUCCESS'");
				
				   foreach($tata_subscriptionsum as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['total_activation_revenue']=round($val[0]['sum'],2);
				   }
				
				   $tata_renewalcount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."'  and service='AUTORENEWAL' and status='SUCCESS' $product");
				
				   foreach($tata_renewalcount as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['Renewal']=$val[0]['count'];
				   }
				
				   $tata_renewalsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='AUTORENEWAL' and status='SUCCESS' $product");
				
				   foreach($tata_renewalsum as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['Renewal_revenue']=round($val[0]['sum'],2);
			       }
				   
				   $tata_deactivate=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='UNSUBSCRIPTION' and status='SUCCESS' $product");
				   
				   foreach($tata_deactivate as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['deactivate']=$val[0]['count'];
                        $resArr[$date]['date']=$val[0]['date'];
				   }
				   
				   $resArr[$date]['parking'] =  $resArr[$date]['total_activation'] -  $resArr[$date]['Activation']; 
				   
				   $resArr[$date]['parkingrevenue'] =  $resArr[$date]['total_activation_revenue'] -  $resArr[$date]['Activation_revenue']; 
				   
				   $resArr[$date]['parkinginqueue'] = $resArr[$date]['cg_ok'] - $resArr[$date]['Activation'];
				   
				   $resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				
		        }
				// bsnl code
				if($biller_id=='bl')
		        {
					
				  
					$bsnl_subscriptioncount=$this->UserHit77->query("select count(*) as count,sum(price) as sum ,date(created) as date from bsnl_callback_notification where  publisher = '".$publisher."' and created>='".$startDate1."' and created<='".$endDate1."' and reqtype='ACT' and price>0");
				   
				    foreach($bsnl_subscriptioncount as $key=>$val)
				   {
					//$date=$val[0]['date'];
					//echo $date; die;
                    $resArr[$date]['total_activation'] = $val[0]['count'];
                    //$resArr[$date]['date']=$val[0]['date'];
					$resArr[$date]['total_activation_revenue']=round($val[0]['sum'],2);
				   }
								   
				
				   $bsnl_renewalcount=$this->UserHit77->query("select count(*) as count,sum(price) as sum,date(created) as date from bsnl_callback_notification where  publisher = '".$publisher."' and created>='".$startDate1."' and created<='".$endDate1."' and (reqtype='REACT' OR reqtype='REN')");
				
				   foreach($bsnl_renewalcount as $key=>$val)
				   {					   
						//$date=$val[0]['date'];						
                        $resArr[$date]['Renewal']=$val[0]['count'];
						$resArr[$date]['Renewal_revenue']=round($val[0]['sum'],2);
				   }				
				   
				   $bsnl_deactivate=$this->UserHit77->query("select count(*) as count,date(created) as date from bsnl_callback_notification where  publisher = '".$publisher."' and created>='".$startDate1."' and created<='".$endDate1."' and (reqtype='STOP' OR reqtype='DEACT')");
				   
				   foreach($bsnl_deactivate as $key=>$val)
				   {
						//$date=$val[0]['date'];
                        $resArr[$date]['deactivate']=$val[0]['count'];
                        //$resArr[$date]['date']=$val[0]['date'];
				   }
				   
				   $resArr[$date]['parking'] =  $resArr[$date]['total_activation'] -  $resArr[$date]['Activation']; 
				   
				   $resArr[$date]['parkingrevenue'] =  $resArr[$date]['total_activation_revenue'] -  $resArr[$date]['Activation_revenue']; 
				   
				   $resArr[$date]['parkinginqueue'] = $resArr[$date]['cg_ok'] - $resArr[$date]['Activation'];
				   
				   $resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				
		        }
				if($biller_id=='at')
		        {
					if($publisher=='bnama')
			        {
					   $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$startDate1."' and date<='".$endDate1."' and message='AOC: Success' and amount_charged>0 and product_id not in(540845,540846,540847,540848) ");	
				    }
				    else
			        {
				      $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$startDate1."' and date<='".$endDate1."' and message='AOC: Success' and amount_charged>0 and product_id  in(540845,540846,540847,540848) "); 
			        }
				    $sum=0;
				    foreach($countrevenue as $key=>$val)
				    {
                      $sum=$val[0]['activation_sum'];
				      $resArr[$date]['total_activation_revenue']=0;
                      $resArr[$date]['total_activation']=$val[0]['activation_count'];
				      if(!empty($sum))
					  {
                       $resArr[$date]['total_activation_revenue']=$sum;
					  }
				    }
				    if($publisher=='bnama')
			        {
		           
				      $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."' and message='Success' and product_id not in(540845,540846,540847,540848) ");
				    }
				    else
			        {
				      $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."' and message='Success' and product_id in(540845,540846,540847,540848) ");
			        }
			        $sum=0;
			        foreach($airtel_renewal as $key=>$val)
		            {
			           $sum=$val[0]['renewal_sum'];
			           $resArr[$date]['Renewal_revenue']=0;
			           $resArr[$date]['Renewal']=$val[0]['renewal_count'];
			           if(!empty($sum))
			           {
			           $resArr[$date]['Renewal_revenue']=$val[0]['renewal_sum'];
					   }
				    }
				    if($publisher=='bnama')
		            {
		             $deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."'  and response_code='1001' and product_id not in(540845,540846,540847,540848) ");
		            }
				    else
		            {
				      $deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."'  and response_code='1001'  and product_id in(540845,540846,540847,540848) ");
		            }
				    foreach($deactivateat as $key=>$val)
                    {
                        $resArr[$date]['deactivate']=$val[0]['count'];
                    }
				   $resArr[$date]['parking'] = $resArr[$date]['total_activation'] - $resArr[$date]['Activation'];

                   $resArr[$date]['parkingrevenue'] = $resArr[$date]['total_activation_revenue'] - $resArr[$date]['Activation_revenue'];		
                   
                   $resArr[$date]['parkinginqueue'] = $resArr[$date]['cg_ok'] - $resArr[$date]['Activation'];

                    $resArr[$date]['totalrevenue'] = $resArr[$date]['Activation_revenue'] + $resArr[$date]['parkingrevenue'] + $resArr[$date]['Renewal_revenue'];				   
				}
				//$this->set(compact('resArr','categories1'));
				//echo "<pre>";
			    //print_r($resArr);
			    //die;
				$this->set('categories1', $resArr);
			   // echo "<pre>";
			   // print_r($resArr);
			   // die;
		   }
		   else
		   {			  
			 $searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and biller_id='".$biller_id."' and publisher = '".$publisher."'";
			 $categories = $this->Contentusedreport->query("select * from vas_summary_report $searchCondStr order by id desc");
		     
			 $cntCategories = $this->Contentusedreport->query("select count(*) as countRec from vas_summary_report $searchCondStr");
	       
		    $totalRec = @$cntCategories[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
		    $this->set(compact('categories','categories'));
		    $this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
		   }
		}
		else if(isset($_GET['download']) && $_GET['download'] == 'download')
		{
			
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$this->layout = null;
	        $this->autoLayout = false;
	        $this->autoRender = false;
			
			if(($startDate == $currDate) || ($endDate == $currDate))
            {
                $startDate1 = $currDate . " " ."00:00:00";
                $endDate1 = $currDate . " " . "23:59:59";
				/* Total User Hits with msisdn*/
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."'");
		    
		           foreach($totalhits as $key=>$val)
		           {
			          $date = $val[0]['date'];
					  $resArr[$date]['Clicks']=$val[0]['count'];
			          $resArr[$date]['date']=$date;
			       }
			   }
			   else
			   {
				   $totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."'");
			   
			       foreach($totalhits as $key=>$val)
			       {
					$date = $val[0]['date'];
					$resArr[$date]['Clicks']=$val[0]['count'];
					$resArr[$date]['date']=$val[0]['date'];
			       }
			   }
			   
			   /* CG OK CODE */
			   
			   if($biller_id == 'vf'|| $biller_id == 'ia')
			   { 
		         $str= " and cg_response_id!='0'";
			   }
			   if($biller_id =='at')
			   {
				 $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
			   }
			   elseif($biller_id == 'tt')
			   { 
			    $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
			   }
			   else
			   {
				$str='';
			   }
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				 $cgok = $this->Transaction99bhojpuri->query("select count(*) as count  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str");
			   }
			   else 
			   {
				 $cgok = $this->Transaction77->query("select count(*) as count  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str ");
			   }
			   
			   foreach($cgok as $key=>$val)
			   {	
				$resArr[$date]['cg_ok']=$val[0]['count'];
			   }
			   
			   /* Billing Done Count */
			   
			   if($biller_id == 'at')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0");
				   foreach($billing_done as $key=>$val)
			       {
					    $resArr[$date]['Activation_revenue']=$val[0]['sum'];
			            $resArr[$date]['Activation']=$val[0]['count'];
			            $resArr[$date]['date']=$date;
				   }
			   }
			   else if($biller_id == 'rl')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and billing_response!='subscription already opened.' and price_deducted>0");
				   foreach($billing_done as $key=>$val)
			       {
					    $resArr[$date]['Activation_revenue']=$val[0]['sum'];
			            $resArr[$date]['Activation']=$val[0]['count'];
			            $resArr[$date]['date']=$date;
				   }
				
			   }
			   else
			   {
				   $billing_done = $this->Transaction77->query("select count(*) as count from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' ");
				   foreach($billing_done as $key=>$val)
			       {
				     $resArr[$date]['Activation']=$val[0]['count'];
			       }
				   
				   /* Total Revenue */
			
			       $total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE'");
			   
			       foreach($total_revenue as $key=>$val)
			       {
				    $resArr[$date]['Activation_revenue']=$val[0]['sum'];
			       }
			   }
			   
			   /* Callback Count */
			   if($biller_id == 'at')
			   {
				   $callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0");
			   }
			   else if($biller_id == 'rl')
			   {
				    $callback_status=$this->Transaction99bhojpuri->query("select count(*) as count ,date(transaction_time) as date  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1'");
			   }
			   else
			   {
				   $callback_status=$this->Transaction77->query("select count(transaction_id) as count  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE' and callback_status='1' ");
			   }
			  
			   foreach($callback_status as $key=>$val)
			   {
				$resArr[$date]['Callback']=$val[0]['count'];
			   }
			   
			   /* Biller Id Vodafone Start*/
			   if($biller_id =='vf')
			   {
				  if($publisher=='mini')
				  {
					$service_id='V3_MMOVI';
				  }
				  elseif($publisher=='vfcomics')	
				  {
					$service_id='V3_VCOM';
				  }
				  elseif($publisher=='firstcut')	
				  {
					$service_id='V3_FCUT';
				  }
				  else
				  {
					$service_id='V3_BHOJ';
			      }
				
				/* Callback Revenue */
				
				$callback=$this->Transaction77->query("SELECT count(*) as count_revenue,actual_price_deduct, charging_mode,TXNID,date(CREATED) as date FROM `vodafone_callback_notifications` WHERE `ACTION` = 'act' AND `STATUS` = 'success' AND CREATED>='".$startDate1."' and CREATED<='".$endDate1."' AND service_id LIKE '%$service_id%' and isblock=0  group by charging_mode,TXNID");
				
				$actual_price_deduct=0; $count=0;
				
				foreach($callback as $key=>$val)
				{
				    $actual_price_deduct=$actual_price_deduct+$val['vodafone_callback_notifications']['actual_price_deduct'];
					$count++;
				}//echo '<pre>'; print_r($resArr);die;
				
				$resArr[$date]['total_activation']= $count;
				$resArr[$date]['total_activation_revenue'] = $actual_price_deduct;
				
				/* Parking In Queue Count */
				
				$parkinginqueue = $this->Transaction77->query("SELECT count(*) as count,actual_price_deduct, charging_mode,TXNID,DATE(CREATED) as date  FROM `vodafone_callback_notifications` WHERE CREATED>='".$startDate1."' and CREATED<='".$endDate1."'  AND service_id LIKE '%$service_id%'  and isblock=0 group by charging_mode,TXNID,DATE(CREATED)");
				
				$count=0; $null=0; $null=0; $suspend=0;
				
				foreach($parkinginqueue as $key=>$val)
				{
					if($val['vodafone_callback_notifications']['charging_mode']=='PARKING')
					{
					    $count++;
					}
					if($val['vodafone_callback_notifications']['charging_mode']=='null')
					{
					    $null++;
					}
					if($val['vodafone_callback_notifications']['charging_mode']=='SUSPEND')
					{
					    $suspend++;
					}
				}
				'Count: '.$count;
				$deactivate = $null + $suspend;
				'deactivate: '.$deactivate;
				$resArr[$date]['parkinginqueue']=$count;
				$resArr[$date]['deactivate'] = $deactivate;
				
				$update=$this->Transaction77->query("SELECT sum(`actual_price_deduct`) as sum1,count(*) as count FROM `vodafone_update_notifications` WHERE `actual_price_deduct`>'0' and  CREATED>='".$startDate1."' and CREATED<='".$endDate1."' AND service_id LIKE '%$service_id%'");
				
				$sum1=$update['0']['0']['sum1'];
				$sum=$update['0']['0']['count'];
				
				$resArr[$date]['Renewal_revenue']=$sum1;
				$resArr[$date]['Renewal'] = $sum;
				
				$resArr[$date]['parking']=  $resArr[$date]['total_activation'] -  $resArr[$date]['Activation'];
				
				$resArr[$date]['parkingrevenue']=  $resArr[$date]['total_activation_revenue'] -  $resArr[$date]['Activation_revenue'];
				
				$resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				}
				
				/* Biller Id Idea Start*/
			
			    if($biller_id =='ia' && $publisher =='filmy')
			    {
				 $totalrenewal = $this->UserHit77->query("SELECT count( * ) as count , sum( price ) as sum ,date(created) as date, ACTION , `status` FROM  bp_idea_callback_notifications as idea WHERE created>='".$startDate1."' and created<='".$endDate1."' AND STATUS = 'SUCCESS' GROUP BY ACTION");
                
				 foreach($totalrenewal as $key=>$val)
				 {
		            if($val['idea']['ACTION']=='ACT')
					{
		              $resArr[$val[0]['date']]['total_activation']=$val[0]['count'];
                      $resArr[$val[0]['date']]['total_activation_revenue']=$val[0]['sum'];		
					}
		            if($val['idea']['ACTION']=='DCT')
					{
		              $resArr[$val[0]['date']]['deactivate']=$val[0]['count'];	
		            }
		            if($val['idea']['ACTION']=='REN')
					{
		              $resArr[$val[0]['date']]['Renewal']=$val[0]['count'];
                      $resArr[$val[0]['date']]['Renewal_revenue']=$val[0]['sum'];		
					}
				}	
                
				/* Parking in queue Count */
				
                $parkinginqueue = $this->UserHit77->query("SELECT count(*) as count FROM  bp_idea_callback_notifications as idea WHERE created>='".$startDate1."' and created<='".$endDate1."' AND action ='GRACE' and  STATUS = 'BAL-LOW'");
				
		        $resArr[$date]['parkinginqueue'] = $parkinginqueue[0][0]["count"];

                /* Parking */	
				 $resArr[$date]['parking'] = $resArr[$date]['total_activation'] - $resArr[$date]['Activation'];
				 
				 /* Parking Revenue */
				 $resArr[$date]['parkingrevenue'] = $resArr[$date]['total_activation_revenue'] - $resArr[$date]['Activation_revenue'];
				 
				/* Total Revenue */
				  
				$resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				 
			    }
				
				if($biller_id=='tt')
		        {
					if($publisher == 'mini')
					{
	                  $product = 'and PRODUCTID IN(0145037900,0145038100,0145038300,0145038500)';
	                }
	                elseif($publisher =='filmy')	
	                {
		              $product = 'and PRODUCTID IN(0143389800,0145021100,0145021300,0145021500,0145021700,0145021900)';
	                }
	                elseif($publisher == 'firstcut')	
	                {
		
		            $product = 'and PRODUCTID NOT IN(0145037900,0145038100,0145038300,0145038500,0143389800,0145021100,0145021300,0145021500,0145021700,0145021900)';
	                }
	                else
                    {
		               $product = '';
	                }
				    $tata_subscriptioncount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='SUBSCRIPTION' and status='SUCCESS' $product");
				   
				    foreach($tata_subscriptioncount as $key=>$val)
				   {
					$date=$val[0]['date'];
                    $resArr[$date]['total_activation']=$val[0]['count'];
                    $resArr[$date]['date']=$val[0]['date'];
				   }
				
				   $tata_subscriptionsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='SUBSCRIPTION' and status='SUCCESS' $product");
				
				   foreach($tata_subscriptionsum as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['total_activation_revenue']=round($val[0]['sum'],2);
				   }
				
				   $tata_renewalcount=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."'  and service='AUTORENEWAL' and status='SUCCESS' $product");
				
				   foreach($tata_renewalcount as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['Renewal']=$val[0]['count'];
				   }
				
				   $tata_renewalsum=$this->UserHit77->query("select sum(PRICE)/100 as sum,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='AUTORENEWAL' and status='SUCCESS' $product");
				
				   foreach($tata_renewalsum as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['Renewal_revenue']=round($val[0]['sum'],2);
			       }
				   
				   $tata_deactivate=$this->UserHit77->query("select count(*) as count,date(created) as date from  tata_subscription_logs where  created>='".$startDate1."' and created<='".$endDate1."' and service='UNSUBSCRIPTION' and status='SUCCESS' $product");
				   
				   foreach($tata_deactivate as $key=>$val)
				   {
						$date=$val[0]['date'];
                        $resArr[$date]['deactivate']=$val[0]['count'];
                        $resArr[$date]['date']=$val[0]['date'];
				   }
				   
				   $resArr[$date]['parking'] =  $resArr[$date]['total_activation'] -  $resArr[$date]['Activation']; 
				   
				   $resArr[$date]['parkingrevenue'] =  $resArr[$date]['total_activation_revenue'] -  $resArr[$date]['Activation_revenue']; 
				   
				   $resArr[$date]['parkinginqueue'] = $resArr[$date]['cg_ok'] - $resArr[$date]['Activation'];
				   
				   $resArr[$date]['totalrevenue']=$resArr[$date]['Activation_revenue'] +  $resArr[$date]['parkingrevenue'] +  $resArr[$date]['Renewal_revenue'];
				
		        }
				if($biller_id=='at')
		        {
					if($publisher=='bnama')
			        {
					   $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$startDate1."' and date<='".$endDate1."' and message='AOC: Success' and amount_charged>0 and product_id not in(540845,540846,540847,540848) ");	
				    }
				    else
			        {
				      $countrevenue=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as activation_count, sum(`amount_charged`) as activation_sum from bp_billing_notifications where  date>='".$startDate1."' and date<='".$endDate1."' and message='AOC: Success' and amount_charged>0 and product_id  in(540845,540846,540847,540848) "); 
			        }
				    $sum=0;
				    foreach($countrevenue as $key=>$val)
				    {
                      $sum=$val[0]['activation_sum'];
				      $resArr[$date]['total_activation_revenue']=0;
                      $resArr[$date]['total_activation']=$val[0]['activation_count'];
				      if(!empty($sum))
					  {
                       $resArr[$date]['total_activation_revenue']=$sum;
					  }
				    }
				    if($publisher=='bnama')
			        {
		           
				      $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."' and message='Success' and product_id not in(540845,540846,540847,540848) ");
				    }
				    else
			        {
				      $airtel_renewal=$this->Transaction99bhojpuri->query("Select  count(`amount_charged`) as renewal_count, sum(`amount_charged`) as renewal_sum from bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."' and message='Success' and product_id in(540845,540846,540847,540848) ");
			        }
			        $sum=0;
			        foreach($airtel_renewal as $key=>$val)
		            {
			           $sum=$val[0]['renewal_sum'];
			           $resArr[$date]['Renewal_revenue']=0;
			           $resArr[$date]['Renewal']=$val[0]['renewal_count'];
			           if(!empty($sum))
			           {
			           $resArr[$date]['Renewal_revenue']=$val[0]['renewal_sum'];
					   }
				    }
				    if($publisher=='bnama')
		            {
		             $deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."'  and response_code='1001' and product_id not in(540845,540846,540847,540848) ");
		            }
				    else
		            {
				      $deactivateat=$this->Transaction99bhojpuri->query("Select count(*) as count from  bp_billing_notifications where date>='".$startDate1."' and date<='".$endDate1."'  and response_code='1001'  and product_id in(540845,540846,540847,540848) ");
		            }
				    foreach($deactivateat as $key=>$val)
                    {
                        $resArr[$date]['deactivate']=$val[0]['count'];
                    }
				   $resArr[$date]['parking'] = $resArr[$date]['total_activation'] - $resArr[$date]['Activation'];

                   $resArr[$date]['parkingrevenue'] = $resArr[$date]['total_activation_revenue'] - $resArr[$date]['Activation_revenue'];		
                   
                   $resArr[$date]['parkinginqueue'] = $resArr[$date]['cg_ok'] - $resArr[$date]['Activation'];

                   $resArr[$date]['totalrevenue'] = $resArr[$date]['Activation_revenue'] + $resArr[$date]['parkingrevenue'] + $resArr[$date]['Renewal_revenue'];				   
				}
				//$this->set(compact('resArr','categories1'));
				//$this->set('categories1', $resArr);
				$date = date('Y-m-d h:i:s');
	            $filename = "summaryreport".$date.".csv";
	            $csv_file = fopen('php://output', 'w');
	            header('Content-type: application/csv');
	            header('Content-Disposition: attachment; filename="'.$filename.'"');
				$header_row = array("Date","Operator","Service","Clicks","CG OK","Activation","Activation Revenue","Call Back","Parking","Parking Revenue","Renewal","Renewal Revenue","Total Revenue","Parking in Queue","Deactivation");
	        
			    fputcsv($csv_file,$header_row,',','"');
				 foreach($resArr as $resArr=>$obj)
	            {
					//echo "<pre>";
					//print_r($obj);
					//die;
					if(!empty($obj['Activation_revenue']))
					{
						$Activation_revenue = $obj['Activation_revenue'];
					}
					else 
					{
						$Activation_revenue = 0;
					}
		            $row = array(
								$obj['date'],
								$biller_id,
								$publisher,
								$obj['Clicks'],
								$obj['cg_ok'],
								$obj['Activation'],
								$Activation_revenue,
								$obj['Callback'],
								$obj['parking'],
								$obj['parkingrevenue'],
								$obj['Renewal'],
								$obj['Renewal_revenue'],
								$obj['totalrevenue'],
								$obj['parkinginqueue'],
								$obj['deactivate']
								);								
                    fputcsv($csv_file,$row,',','"');
				}
	            fclose($csv_file);
            }
			else 
			{
				$userdetails = $this->Contentusedreport->query("select * from vas_summary_report where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and biller_id='".$biller_id."' and publisher = '".$publisher."'");			
			
			    $date = date('Y-m-d h:i:s');
	            $filename = "summaryreport".$date.".csv";
	            $csv_file = fopen('php://output', 'w');
	            header('Content-type: application/csv');
	            header('Content-Disposition: attachment; filename="'.$filename.'"');
				
			    $header_row = array("Date","Operator","Service","Clicks","CG OK","Activation","Activation Revenue","Call Back","Parking","Parking Revenue","Renewal","Renewal Revenue","Total Revenue","Parking in Queue","Deactivation");
	        
			    fputcsv($csv_file,$header_row,',','"');
	            foreach($userdetails as $userdetails=>$obj)
	            {
		            $row = array(
								$obj['vas_summary_report']['reportdate'],
								$obj['vas_summary_report']['biller_id'],
								$obj['vas_summary_report']['publisher'],
								$obj['vas_summary_report']['Clicks'],
								$obj['vas_summary_report']['cgok'],
								$obj['vas_summary_report']['Activation'],
								$obj['vas_summary_report']['activationrevenue'],
								$obj['vas_summary_report']['callback'],
								$obj['vas_summary_report']['parking'],
								$obj['vas_summary_report']['parkingrevenue'],
								$obj['vas_summary_report']['renewal'],
								$obj['vas_summary_report']['renewalrevenue'],
								$obj['vas_summary_report']['totalrevenue'],
								$obj['vas_summary_report']['parkinginqueue'],
								$obj['vas_summary_report']['deactivate']
								);								
                    fputcsv($csv_file,$row,',','"');
				}
	            fclose($csv_file);
			}
	         
		}
		else 
		{
			//$startDate = $prevDate . " " ."00:00:00";
            //$endDate = $prevDate . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and biller_id='".$biller_id."' and publisher = '".$publisher."'";
			$categories = $this->Contentusedreport->query("select * from vas_summary_report $searchCondStr order by id desc");
		
		    $cntCategories = $this->Contentusedreport->query("select count(*) as countRec from vas_summary_report $searchCondStr");
	       
		    $totalRec = @$cntCategories[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
		    $this->set(compact('categories','categories'));
		    $this->set('numOfPage', $numOfPage);
		    $this->set('pageNum', $pageNum);
		}
		
		
		
		
	}
	public function vas_detail_report($page=0)
	{
		 //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		//paging section end here...
		
		$biller_id = $_GET['biller_id'];
		$publisher = $_GET['publisher'];
		
		//$prevDate = date('Y-m-d', strtotime(' -1 day'));
		if (isset($_GET['search']) && $_GET['search'] == 'search')
		{	
	       //$startDate = $_GET['startDate'] . " " ."00:00:00";
           //$endDate = $_GET['endDate'] . " " . "23:59:59";
		   $startDate = $_GET['startDate'];
           $endDate = $_GET['endDate'];
		   $searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and operator='".$biller_id."' and service = '".$publisher."'";
		}
		else if(isset($_GET['download']) && $_GET['download'] == 'download')
		{
			//$startDate = $_GET['startDate'] . " " ."00:00:00";
            //$endDate = $_GET['endDate'] . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$this->layout = null;
	        $this->autoLayout = false;
	        $this->autoRender = false;
	         
		    $userdetails = $this->Contentusedreport->query("select * from vas_detail_report where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and operator='".$biller_id."' and service = '".$publisher."'");			
			
			$date = date('Y-m-d h:i:s');
	        $filename = "detailreport".$date.".csv";
	        $csv_file = fopen('php://output', 'w');
	        header('Content-type: application/csv');
	        header('Content-Disposition: attachment; filename="'.$filename.'"');
			
	        
			$header_row = array("Date","Clicks","Mobile not found","CG OK","Activation","Activation Revenue","Call Back","Consent","Campaign Name","Pack Id","Operator","Service","Interface","Publisher");
	        
			fputcsv($csv_file,$header_row,',','"');
	        foreach($userdetails as $userdetails=>$obj)
	        {
				$pub = explode("_", $obj['vas_detail_report']['interface']);
		
		            $row = array(
								$obj['vas_detail_report']['reportdate'],
								$obj['vas_detail_report']['Clicks'],
								$obj['vas_detail_report']['mobile_not_found'],
								$obj['vas_detail_report']['cgok'],
								$obj['vas_detail_report']['Activation'],
								$obj['vas_detail_report']['activationrevenue'],
								$obj['vas_detail_report']['callback'],
								$obj['vas_detail_report']['consent'],
								$obj['vas_detail_report']['campaign_name'],
								$obj['vas_detail_report']['pack_id'],
								$obj['vas_detail_report']['operator'],
								$obj['vas_detail_report']['service'],
								$obj['vas_detail_report']['interface'],
								$pub[0]
							
								);								
                    fputcsv($csv_file,$row,',','"');
			}
	        fclose($csv_file);
		}
		else 
		{
			//$startDate = $prevDate . " " ."00:00:00";
            //$endDate = $prevDate . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and operator ='".$biller_id."' and service = '".$publisher."'";
		}
		
		
		$categories = $this->Contentusedreport->query("select * from vas_detail_report $searchCondStr order by clicks desc ");
		
		//$cntCategories = $this->Contentusedreport->query("select count(*) as countRec from vas_detail_report $searchCondStr");
		
			
		$totalRec = @$cntCategories[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		$this->set(compact('categories','categories'));
		//$this->set('numOfPage', $numOfPage);
		//$this->set('pageNum', $pageNum);
	}
	public function vas_detail_report_new($page=0)
	{
		
		 //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		
		//paging section end here...
		
		$biller_id = $_GET['biller_id'];
		$publisher = $_GET['publisher'];
		
		//$prevDate = date('Y-m-d', strtotime(' -1 day'));
		$currDate = date('Y-m-d');
		
		if (isset($_GET['search']) && $_GET['search'] == 'search')
		{
		   $startDate = $_GET['startDate'];
           $endDate = $_GET['endDate'];
		   if(($startDate == $currDate) || ($endDate == $currDate))
		   {
			   
			   $startDate1 = $currDate . " " ."00:00:00";
               $endDate1 = $currDate . " " . "23:59:59";
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,interface,pack_id from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' group by interface order by count(*) desc");
				   foreach($totalhits as $key=>$val)
		           {
				
				$date = $val[0]['date'];
				$interface = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface'] = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['pack_id'] = $val['bp_user_hits']['pack_id'];
				$resArr[$date][$interface]['total_hit'] = $val[0]['count'];
				$campaignname = $this->UserHit99bhojpuri->query("select campaign_name from bp_packs where pack_id ='".$resArr[$date][$interface]['pack_id']."'");
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
	           }
			   }
			   else
			   {
				   $totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date,interface, pack_id from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' group by interface order by count(*) desc");
				   foreach($totalhits as $key=>$val)
		       {
				
				$date = $val[0]['date'];
				$interface = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface'] = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['pack_id'] = $val['bp_user_hits']['pack_id'];
				$resArr[$date][$interface]['total_hit'] = $val[0]['count'];
				$campaignname = $this->Transaction77->query("select campaign_name from bp_packs where pack_id ='".$resArr[$date][$interface]['pack_id']."'");
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
	           }
			   }
			  
			   if($biller_id == 'vf' || $biller_id == 'ia')
		       {  
	              $str= " and cg_response_id!='0'";
		       }
		       elseif($biller_id == 'tt')
		       { 
		          $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
		       }
		       elseif($biller_id =='at')
		       { 
		         // $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
				  $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
		       }
		       else
		       {
			      $str='';
		       }
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $cgok = $this->Transaction99bhojpuri->query("select count(*) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str group by interface ");
			   }
			   else
			   {
				   $cgok = $this->Transaction77->query("select count(*) as count,interface,pack_id  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str group by interface ");
			   }
			   
			   
			   foreach($cgok as $key=>$val)
		       {	
		         
			
				 $interface=$val['bp_transaction']['interface'];
				 $resArr[$date][$interface]['interface']=$interface;
				 $resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				 //$resArr[$date][$interface]['pack_id']=$val['bp_transaction']['pack_id'];
				 
			    }//echo '<pre>'; print_r($resArr);die;
				
				
			   if($biller_id == 'at')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0 group by interface");
				   foreach($billing_done as $key=>$val)
	               {
			           $interface=$val['bp_transaction']['interface'];	
		               $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			           $resArr[$date][$interface]['billing_done']=$val[0]['count'];
		           }
			   }
			   else if($biller_id == 'rl')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0  and billing_response!='subscription already opened.' group by interface");
				   foreach($billing_done as $key=>$val)
	               {
			           $interface=$val['bp_transaction']['interface'];	
		               $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			           $resArr[$date][$interface]['billing_done']=$val[0]['count'];
		           }
			   }
			   else
			   {
				   $billing_done = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE'  group by interface");
				   foreach($billing_done as $key=>$val)
		           {
			         $interface=$val['bp_transaction']['interface'];
				     $resArr[$date][$interface]['interface']=$interface;
				     $resArr[$date][$interface]['billing_done']=$val[0]['count'];
	                }
					$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE'  group by interface ");
			    
				    foreach($total_revenue as $key=>$val)
		            {
				      $interface=$val['bp_transaction']['interface'];
				      $resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				      $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
		             } 
			   }
				
				
				
				if($biller_id == 'at' || $biller_id == 'rl')
			    {
					$mobile_notfound=$this->UserHit99bhojpuri->query("select count(*) as count, interface from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' and (msisdn='' OR msisdn='NA' OR msisdn='1234') group by interface");
			    }
				else
				{
					$mobile_notfound=$this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits where  biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' and msisdn='' group by interface");
				}
				
				foreach($mobile_notfound as $key=>$val)
		        {
				$interface=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
		        }
				
				if($biller_id == 'at')
			    {
					$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0 group by interface");
				}
				else if($biller_id == 'rl')
				{
					$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and  transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' group by interface");
				}
				else
				{
				   	$callback_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' and callback_status='1'  group by interface");
				}
			
				foreach($callback_status as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['callback_status']=$val[0]['count'];
		        }
				if($biller_id == 'at' || $biller_id == 'rl')
			    {
					 $consent=$this->Transaction99bhojpuri->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and consent='1'  group by interface");
				}
				else
				{
					$consent=$this->Transaction77->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and consent='1'  group by interface");
				}
				
				foreach($consent as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['consent']=$val[0]['count'];
		        } //echo '<pre>';print_r($resArr);die;
				
				$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' and churn_status='1'  group by interface");
		        
				foreach($churn_status as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['churn_status']=$val[0]['count'];
		        }
				$total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE' and churn_status='1'   group by interface ");
				
				foreach($total_revenue_churn as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
		        }
				//echo "<pre>";
				//print_r($resArr);
				//die;
				
				$this->set(compact('totalhits','resArr','date1'));
				
				
		   }
		   else 
		   {
			$searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and operator='".$biller_id."' and service = '".$publisher."'"; 
            $categories = $this->Contentusedreport->query("select * from vas_detail_report $searchCondStr order by clicks desc ");
	
		    $totalRec = @$cntCategories[0][0]["countRec"];
		    $numOfPage = ceil($totalRec / $recPerPage);
		    $this->set(compact('categories','categories'));		
		   }
		   
		}
		else if(isset($_GET['download']) && $_GET['download'] == 'download')
		{
			
			//$startDate = $_GET['startDate'] . " " ."00:00:00";
            //$endDate = $_GET['endDate'] . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			$this->layout = null;
	        $this->autoLayout = false;
	        $this->autoRender = false;
	         
		    if(($startDate == $currDate) || ($endDate == $currDate))
		   {
			   
			   $startDate1 = $currDate . " " ."00:00:00";
               $endDate1 = $currDate . " " . "23:59:59";
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,interface,date(date) as date,pack_id from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' group by interface order by count(*) desc");
				   foreach($totalhits as $key=>$val)
		           {
					   
				
				$date = $val[0]['date'];
				$interface = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface'] = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['pack_id'] = $val['bp_user_hits']['pack_id'];
				$resArr[$date][$interface]['total_hit'] = $val[0]['count'];
				$campaignname = $this->UserHit99bhojpuri->query("select campaign_name from bp_packs where pack_id ='".$resArr[$date][$interface]['pack_id']."'");
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
	           }
			   }
			   else
			   {
				   $totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date,interface, pack_id from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' group by interface order by count(*) desc");
				   foreach($totalhits as $key=>$val)
		       {
				
				$date = $val[0]['date'];
				$interface = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface'] = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['pack_id'] = $val['bp_user_hits']['pack_id'];
				$resArr[$date][$interface]['total_hit'] = $val[0]['count'];
				$campaignname = $this->Transaction77->query("select campaign_name from bp_packs where pack_id ='".$resArr[$date][$interface]['pack_id']."'");
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
	           }
			   }
			  
			   if($biller_id == 'vf' || $biller_id == 'ia')
		       {  
	              $str= " and cg_response_id!='0'";
		       }
		       elseif($biller_id == 'tt')
		       { 
		          $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
		       }
		       elseif($biller_id =='at')
		       { 
		         // $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
				  $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
		       }
		       else
		       {
			      $str='';
		       }
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $cgok = $this->Transaction99bhojpuri->query("select count(*) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str group by interface ");
			   }
			   else
			   {
				   $cgok = $this->Transaction77->query("select count(*) as count,interface,pack_id  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str group by interface ");
			   }
			   
			   
			   foreach($cgok as $key=>$val)
		       {	
		         
			
				 $interface=$val['bp_transaction']['interface'];
				 $resArr[$date][$interface]['interface']=$interface;
				 $resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				 //$resArr[$date][$interface]['pack_id']=$val['bp_transaction']['pack_id'];
				 
			    }//echo '<pre>'; print_r($resArr);die;
				
				
			   if($biller_id == 'at')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0 group by interface");
				   foreach($billing_done as $key=>$val)
	               {
			           $interface=$val['bp_transaction']['interface'];	
		               $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			           $resArr[$date][$interface]['billing_done']=$val[0]['count'];
		           }
			   }
			   else if($biller_id == 'rl')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0  and billing_response!='subscription already opened.' group by interface");
				   foreach($billing_done as $key=>$val)
	               {
			           $interface=$val['bp_transaction']['interface'];	
		               $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			           $resArr[$date][$interface]['billing_done']=$val[0]['count'];
		           }
			   }
			   else
			   {
				   $billing_done = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE'  group by interface");
				   foreach($billing_done as $key=>$val)
		           {
			         $interface=$val['bp_transaction']['interface'];
				     $resArr[$date][$interface]['interface']=$interface;
				     $resArr[$date][$interface]['billing_done']=$val[0]['count'];
	                }
					$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE'  group by interface ");
			    
				    foreach($total_revenue as $key=>$val)
		            {
				      $interface=$val['bp_transaction']['interface'];
				      $resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				      $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
		             } 
			   }
				
				
				
				if($biller_id == 'at' || $biller_id == 'rl')
			    {
					$mobile_notfound=$this->UserHit99bhojpuri->query("select count(*) as count, interface from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' and (msisdn='' OR msisdn='NA' OR msisdn='1234') group by interface");
			    }
				else
				{
					$mobile_notfound=$this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits where  biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' and msisdn='' group by interface");
				}
				
				foreach($mobile_notfound as $key=>$val)
		        {
				$interface=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
		        }
				
				if($biller_id == 'at')
			    {
					$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0 group by interface");
				}
				else if($biller_id == 'rl')
				{
					$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and  transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' group by interface");
				}
				else
				{
				   	$callback_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' and callback_status='1'  group by interface");
				}
			
				foreach($callback_status as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['callback_status']=$val[0]['count'];
		        }
				if($biller_id == 'at' || $biller_id == 'rl')
			    {
					 $consent=$this->Transaction99bhojpuri->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and consent='1'  group by interface");
				}
				else
				{
					$consent=$this->Transaction77->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and consent='1'  group by interface");
				}
				
				foreach($consent as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['consent']=$val[0]['count'];
		        } //echo '<pre>';print_r($resArr);die;
				
				$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' and churn_status='1'  group by interface");
		        
				foreach($churn_status as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['churn_status']=$val[0]['count'];
		        }
				$total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE' and churn_status='1'   group by interface ");
				
				foreach($total_revenue_churn as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
		        }
				/*echo "<pre>";
				print_r($resArr);
				die;*/
				
				$this->set(compact('totalhits','resArr','date1'));
				$date = date('Y-m-d h:i:s');
	            $filename = "detailreport".$date.".csv";
	            $csv_file = fopen('php://output', 'w');
	            header('Content-type: application/csv');
	            header('Content-Disposition: attachment; filename="'.$filename.'"');
		        
				$header_row = array("Date","Clicks","Mobile not found","CG OK","Activation","Activation Revenue","Call Back","Consent","Campaign Name","Pack Id","Operator","Service","Interface","Publisher");
	        
			     fputcsv($csv_file,$header_row,',','"');
				 
				 if(!empty($resArr))
				 {
					 foreach($resArr[$currDate] as $key=>$res)
	                {
						/*echo "<pre>";
						print_r($res);
						die;*/
						if(!empty($res['billing_done']))
						{
							$billing_done = $res['billing_done'];
						}
						else
						{
							$billing_done = 0;
						}
						if(!empty($res['total_revenue']))
						{
							$total_revenue = $res['total_revenue'];
						}
						else
						{
							$total_revenue = 0;
						}
						if(!empty($res['callback_status']))
						{
							$callback_status = $res['callback_status'];
						}
						else
						{
							$callback_status = 0;
						}
						if(!empty($res['consent']))
						{
							$consent = $res['consent'];
						}
						else
						{
							$consent = 0;
						}
						if(!empty($res['cg_ok']))
						{
							$cg_ok = $res['cg_ok'];
						}
						else
						{
							$cg_ok = 0;
						}
						if(!empty($res['mobile_notfound']))
						{
							$mobile_notfound = $res['mobile_notfound'];
						}
						else
						{
							$mobile_notfound = 0;
						}
						if(!empty($res['total_hit']))
						{
							$total_hit = $res['total_hit'];
						}
						else
						{
							$total_hit = 0;
						}
						$pub = explode('_',$res['interface']);
                            $row = array(
							    $currDate,
								$total_hit,
								$mobile_notfound,
								$cg_ok,
								$billing_done,
								$total_revenue,
								$callback_status,
								$consent,
								$res['campaign_name'],
								$res['pack_id'],
								$biller_id,
								$publisher,
								$res['interface'],
								$pub[0]
								);	
						fputcsv($csv_file,$row,',','"');
                           								
				    }
					 
						 
				 }
				 
				 fclose($csv_file);
				
		   }
			else
			{
				$userdetails = $this->Contentusedreport->query("select * from vas_detail_report where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and operator='".$biller_id."' and service = '".$publisher."'");			
			
			$date = date('Y-m-d h:i:s');
	        $filename = "detailreport".$date.".csv";
	        $csv_file = fopen('php://output', 'w');
	        header('Content-type: application/csv');
	        header('Content-Disposition: attachment; filename="'.$filename.'"');
			
	        
			$header_row = array("Date","Clicks","Mobile not found","CG OK","Activation","Activation Revenue","Call Back","Consent","Campaign Name","Pack Id","Operator","Service","Interface","Publisher");
	        
			fputcsv($csv_file,$header_row,',','"');
	        foreach($userdetails as $userdetails=>$obj)
	        {
				$pub = explode("_", $obj['vas_detail_report']['interface']);
		
		            $row = array(
								$obj['vas_detail_report']['reportdate'],
								$obj['vas_detail_report']['Clicks'],
								$obj['vas_detail_report']['mobile_not_found'],
								$obj['vas_detail_report']['cgok'],
								$obj['vas_detail_report']['Activation'],
								$obj['vas_detail_report']['activationrevenue'],
								$obj['vas_detail_report']['callback'],
								$obj['vas_detail_report']['consent'],
								$obj['vas_detail_report']['campaign_name'],
								$obj['vas_detail_report']['pack_id'],
								$obj['vas_detail_report']['operator'],
								$obj['vas_detail_report']['service'],
								$obj['vas_detail_report']['interface'],
								$pub[0]
							
								);								
                    fputcsv($csv_file,$row,',','"');
			}
	        fclose($csv_file);
			}
		    
		}
		else 
		{
			//$startDate = $prevDate . " " ."00:00:00";
            //$endDate = $prevDate . " " . "23:59:59";
			$startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
			
			if(($startDate == $currDate) || ($endDate == $currDate))
			{
				$startDate1 = $currDate . " " ."00:00:00";
               $endDate1 = $currDate . " " . "23:59:59";
			    if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $totalhits = $this->UserHit99bhojpuri->query("select count(*) as count,interface,pack_id from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' group by interface order by count(*) desc");
				   foreach($totalhits as $key=>$val)
		           {
				
				$date = $val[0]['date'];
				$interface = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface'] = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['pack_id'] = $val['bp_user_hits']['pack_id'];
				$resArr[$date][$interface]['total_hit'] = $val[0]['count'];
				$campaignname = $this->UserHit99bhojpuri->query("select campaign_name from bp_packs where pack_id ='".$resArr[$date][$interface]['pack_id']."'");
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
	           }
			   }
			   else
			   {
				   $totalhits = $this->UserHit77->query("select count(*) as count,date(date) as date,interface, pack_id from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' group by interface order by count(*) desc");
				   foreach($totalhits as $key=>$val)
		       {
				
				$date = $val[0]['date'];
				$interface = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface'] = $val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['pack_id'] = $val['bp_user_hits']['pack_id'];
				$resArr[$date][$interface]['total_hit'] = $val[0]['count'];
				$campaignname = $this->Transaction77->query("select campaign_name from bp_packs where pack_id ='".$resArr[$date][$interface]['pack_id']."'");
				$resArr[$date][$interface]['campaign_name']=$campaignname[0]['bp_packs']['campaign_name'];
	           }
			   }
			  
			   
			   if($biller_id == 'vf' || $biller_id == 'ia')
		       {  
	              $str= " and cg_response_id!='0'";
		       }
		       elseif($biller_id == 'tt')
		       { 
		          $str= " and cg_response_id!='0' and cg_response_id!='FAIL' and cg_response_id!='TIMEOUT'";
		       }
		       elseif($biller_id =='at')
		       { 
		         // $str= " and cg_response_id!='0' and cg_response_id!='NULL'";
				  $str= " and cg_response_id!='0' AND `cg_response_id` != 'access_denied|User Declined'";
		       }
		       else
		       {
			      $str='';
		       }
			   
			   if($biller_id == 'at' || $biller_id == 'rl')
			   {
				   $cgok = $this->Transaction99bhojpuri->query("select count(*) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str group by interface ");
			   }
			   else
			   {
				   $cgok = $this->Transaction77->query("select count(*) as count,interface,pack_id  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str group by interface ");
			   }
			   
			   
			   foreach($cgok as $key=>$val)
		       {	
		         
			
				 $interface=$val['bp_transaction']['interface'];
				 $resArr[$date][$interface]['interface']=$interface;
				 $resArr[$date][$interface]['cg_ok']=$val[0]['count'];
				 //$resArr[$date][$interface]['pack_id']=$val['bp_transaction']['pack_id'];
				 
			    }//echo '<pre>'; print_r($resArr);die;
				
				
			   if($biller_id == 'at')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0 group by interface");
				   foreach($billing_done as $key=>$val)
	               {
			           $interface=$val['bp_transaction']['interface'];	
		               $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			           $resArr[$date][$interface]['billing_done']=$val[0]['count'];
		           }
			   }
			   else if($biller_id == 'rl')
			   {
				   $billing_done = $this->Transaction99bhojpuri->query("select count(*) as count,sum(price_deducted) as sum,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and price_deducted>0  and billing_response!='subscription already opened.' group by interface");
				   foreach($billing_done as $key=>$val)
	               {
			           $interface=$val['bp_transaction']['interface'];	
		               $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
			           $resArr[$date][$interface]['billing_done']=$val[0]['count'];
		           }
			   }
			   else
			   {
				   $billing_done = $this->Transaction77->query("select count(*) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE'  group by interface");
				   foreach($billing_done as $key=>$val)
		           {
			         $interface=$val['bp_transaction']['interface'];
				     $resArr[$date][$interface]['interface']=$interface;
				     $resArr[$date][$interface]['billing_done']=$val[0]['count'];
	                }
					$total_revenue = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE'  group by interface ");
			    
				    foreach($total_revenue as $key=>$val)
		            {
				      $interface=$val['bp_transaction']['interface'];
				      $resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				      $resArr[$date][$interface]['total_revenue']=$val[0]['sum'];
		             } 
			   }
				
				
				
				if($biller_id == 'at' || $biller_id == 'rl')
			    {
					$mobile_notfound=$this->UserHit99bhojpuri->query("select count(*) as count, interface from bp_user_hits where biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' and (msisdn='' OR msisdn='NA' OR msisdn='1234') group by interface");
			    }
				else
				{
					$mobile_notfound=$this->UserHit77->query("select count(*) as count,date(date) as date,interface from bp_user_hits where  biller_id='".$biller_id."' and publisher = '".$publisher."' and date>='".$startDate1."' and date<='".$endDate1."' and msisdn='' group by interface");
				}
				
				foreach($mobile_notfound as $key=>$val)
		        {
				$interface=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_user_hits']['interface'];
				$resArr[$date][$interface]['mobile_notfound']=$val[0]['count'];
		        }
				
				if($biller_id == 'at')
			    {
					$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' and price_deducted>0 group by interface");
				}
				else if($biller_id == 'rl')
				{
					$callback_status=$this->Transaction99bhojpuri->query("select count(callback_status) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and  transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_response_time>='".$startDate1."' and billing_response_time<='".$endDate1."' $str and billing_status='DONE' and callback_status='1' group by interface");
				}
				else
				{
				   	$callback_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' and callback_status='1'  group by interface");
				}
			
				foreach($callback_status as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['callback_status']=$val[0]['count'];
		        }
				if($biller_id == 'at' || $biller_id == 'rl')
			    {
					 $consent=$this->Transaction99bhojpuri->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and consent='1'  group by interface");
				}
				else
				{
					$consent=$this->Transaction77->query("select count(transaction_id) as count,interface  from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and consent='1'  group by interface");
				}
				
				foreach($consent as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['consent']=$val[0]['count'];
		        } //echo '<pre>';print_r($resArr);die;
				
				$churn_status=$this->Transaction77->query("select count(transaction_id) as count ,date(transaction_time) as date,interface  from bp_transaction where  biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."' and billing_status='DONE' and churn_status='1'  group by interface");
		        
				foreach($churn_status as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['churn_status']=$val[0]['count'];
		        }
				$total_revenue_churn = $this->Transaction77->query("select sum(price_deducted) as sum,date(transaction_time) as date,interface from bp_transaction where biller_id='".$biller_id."' and publisher = '".$publisher."' and transaction_time>='".$startDate1."' and transaction_time<='".$endDate1."'  and billing_status='DONE' and churn_status='1'   group by interface ");
				
				foreach($total_revenue_churn as $key=>$val)
		        {
				$interface=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['interface']=$val['bp_transaction']['interface'];
				$resArr[$date][$interface]['total_revenue_churn']=$val[0]['sum'];
		        }
				//echo "<pre>";
				//print_r($resArr);
				//die;
				
				$this->set(compact('totalhits','resArr','date1'));
			   
			}
			else 
			{
				$searchCondStr .=" where reportdate <= '".$endDate."' and reportdate >= '".$startDate."' and operator ='".$biller_id."' and service = '".$publisher."'";
			    
				$categories = $this->Contentusedreport->query("select * from vas_detail_report $searchCondStr order by clicks desc ");
	
		        $totalRec = @$cntCategories[0][0]["countRec"];
		        $numOfPage = ceil($totalRec / $recPerPage);
		        $this->set(compact('categories','categories'));
			}
			
		}
	}
	
	//=============================@Vijay Pandey=========================
	public function cat_user_data()
	{
		
		if(empty($_GET['startDate']))
		{
			$_GET['startDate'] = date('Y-m-d', strtotime("-1 day"));
		}
		if((!empty($_GET['startDate'])))
		{
			$datenew = $_GET['startDate'];
			$resArr = array();
			$sql1 = $this->Fcuserviewreport->query("SELECT t3.name,count(t2.content_id) as total,count(t1.user_id) as totalview FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' and t3.status='A' and (time_of_play >= 30 or is_full=1)  group by t3.category_id");
			
			
			$i=0;
		
			foreach($sql1 as $key=>$val)
		    {
			//echo '<pre>';print_r($sql1);
			
				$resArr[$i]['name']			=  $val['t3']['name'];
				
				$resArr[$i]['total']	 	=  $val[0]['totalview'];
				
				$resArr[$i]['totalview']    =  $val[0]['totalview'];
				
				
				$i++;
		    } 
			
			
			$sql2 = $this->Fcuserviewreport->query("SELECT count(t1.user_id) as totalview2 FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' and t3.status='A' and t1.operating_system = 'android' and (time_of_play >= 30 or is_full=1) group by t3.category_id");
			
			//echo '<pre>';print_r($sql2);die;
			$j = 0;
			foreach($sql2 as $key1=>$val1)
		    {
				$resArr[$j]['totalview2'] = $val1[0]['totalview2'];
				//$resArr = array_sum(array_column($resArr,'totalview2'));
				$j++;
		    } 
			
			
			$sql3 = $this->Fcusercpcontentview->query("SELECT count(t1.user_id) as count3  FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' 
			and t3.status='A' and t2.status='A' and t2.approved= 1 and operating_system = 'wap' and is_full=1 group by t3.category_id");
			
			//echo "SELECT count(t1.user_id) as count3  FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' and t3.status='A' and t2.status='A' and t2.approved= 1 and operating_system = 'wap' and is_full=1 group by t3.category_id";die;
			$k=0;
			foreach($sql3 as $key3=>$val3)
		    {
				$resArr[$k]['count3'] = $val3[0]['count3'];
				//$resArr = array_sum(array_column($resArr,'count3'));
				$k++;
		    } 
			
			$sql4 = $this->Fcusercpcontentview->query("SELECT count(t1.user_id) as count4  FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' 
			and t3.status='A' and t2.status='A' and t2.approved= 1 and operating_system = 'android' and is_full=1 group by t3.category_id");
			
			
			$m=0;
			foreach($sql4 as $key4=>$val4)
		    {
				$resArr[$m]['count4'] = $val4[0]['count4'];
				//$resArr = array_sum(array_column($resArr,'count4'));
				$m++;
		    }
			
			$this->set('resArr', $resArr);
			
			/*$sql5 = $this->Fcusercpcontentviewfull->query("SELECT count(*) as count5  FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' and t3.status='A'");
			
			$resArrCount = $sql5[0][0]["count5"];*/
			

			//$this->set('resArrCount', $resArrCount);
			//$this->set('sql6', $sql6);
			/*
			$sql7 = $this->Fcusercpcontentviewfull->query("SELECT count(t1.user_id) as count7  FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew." 00:00:00' and t1.created <= '".$datenew." 23:59:59' and  t3.project='firstcut' 
			and t3.status='A' and t2.status='A' and t2.approved= 1 and operating_system = 'android' and is_full=1 group by t3.category_id");
			$this->set('sql7', $sql7); */
			
			/* $sql8 = $this->Fcuserlogin->query("SELECT count(*) as count8 FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew1." 00:00:00' and t1.created <= '".$datenew1." 23:59:59' and t3.project='firstcut' and t3.status='A' and t1.operating_system='wap'");
			$this->set('sql8', $sql8);
			
			$sql9 = $this->Fcuserlogin->query("SELECT count(*) as count9 FROM users_view as t1 left join cm_contents as t2 on t1.content_id = t2.content_id left join cm_categories as t3 on t2.category_id = t3.category_id where  t1.created >= '".$datenew1." 00:00:00' and t1.created <= '".$datenew1." 23:59:59' and t3.project='firstcut' and t3.status='A' and t1.operating_system='android'");
			$this->set('sql9', $sql9); */
			
					
		}
	}
}
?>