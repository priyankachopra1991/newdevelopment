<?php
//App::uses('HttpSocket', 'Network/Http');
App::import('Vendor', 'S3.php');
App::import('Vendor', 'sdk/sdk.class.php');
App::import('Vendor', 'AmazonS3.php');

class FcappcategoriesController extends AppController {
	public $uses = array('Fcappcategories');

	public function add() {	
	
		if($this->request->isPost())
		{   $sql = array(); 
			$data = $this->request->data;
			//$this->Session->write('Appname.slug', $data["app_slug"]);
		    
			$sql["name"] = $data["name"];
			$sql["slug"] = $data["slug"];
			$sql["parent_id"] = $data["category_id"];
			$sql["description"] = $data["description"];
			$sql["alttag"] = $data["alttag"];
			$sql["display_order"] = $data["display_order"];
			$sql["status"] = $data["status"];
			$sql["app_flag"] = $data["app_flag"];
			$sql["heading_back_color"] = $data["heading_back_color"];
			$sql["project"] = $data["app_slug"];
			
			if(!empty($data["cat_icon"]["name"])){//Category icon image upload...
				$file1 = $data['cat_icon']; 
				$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType1) != "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif")){
					$this->Session->setFlash('Unacceptable file type for category icon.');
					$this->redirect('add');
					
				} else {
				
					//move_uploaded_file($file1['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_icon_".time()."_".$file1['name']);
					move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'catimg/' . "c_icon_".time().".".$imageFileType1);
					//$sql["cat_icon"] = "c_icon_".time()."_".$file1['name'];
					$sql["cat_icon"] = "c_icon_".time().".".$imageFileType1;
					
					//images move on bucket code start here...
					$bannerImage1 = UPLOADPATH . 'catimg/'. $sql["cat_icon"];
					/*if(file_exists($bannerImage1)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage1);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage1);
					}*/
				}
			}
			
			if(!empty($data["portal_cat_icon"]["name"])){//Portal category icon image upload...
				$file2 = $data['portal_cat_icon']; 
				$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType2) != "jpg") && (strtolower($imageFileType2) !="png") && (strtolower($imageFileType2) !="jpeg") && (strtolower($imageFileType2) !="gif")){
					$this->Session->setFlash('Unacceptable file type for portal category icon.');
					$this->redirect('add');
					
				} else {
					//move_uploaded_file($file2['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "p_c_icon_".time()."_".$file2['name']);
					move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'catimg/' . "p_c_icon_".time().".".$imageFileType2);
					//$sql["portal_cat_icon"] = "p_c_icon_".time()."_".$file2['name'];
					$sql["portal_cat_icon"] = "p_c_icon_".time().".".$imageFileType2;
					
					//images move on bucket code start here...
					$bannerImage2 = UPLOADPATH . 'catimg/'. $sql["portal_cat_icon"];
					/*if(file_exists($bannerImage2)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage2);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage2); 
					}*/
				}
			}
			
			if(!empty($data["category_banner"]["name"])){//Category banner image upload...
				$file3 = $data['category_banner']; 
				$imageFileType3 = pathinfo($file3['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType3) != "jpg") && (strtolower($imageFileType3) !="png") && (strtolower($imageFileType3) !="jpeg") && (strtolower($imageFileType3) !="gif")){
					$this->Session->setFlash('Unacceptable file type for category banner icon.');
					$this->redirect('add');
					
				} else {
					//move_uploaded_file($file3['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_ban_".time()."_".$file3['name']);
					move_uploaded_file($file3['tmp_name'], UPLOADPATH . 'catimg/' . "c_ban_".time().".".$imageFileType3);
					//$sql["category_banner"] = "c_ban_".time()."_".$file3['name'];
					$sql["category_banner"] = "c_ban_".time().".".$imageFileType3;
					
					//images move on bucket code start here...
					$bannerImage3 = UPLOADPATH . 'catimg/'. $sql["category_banner"];
					/*if(file_exists($bannerImage3)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage3);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage3);
					}*/
				}
			}
			
			if(!empty($data["category_page_icon"]["name"])){//Category page icon image upload...
				$file4 = $data['category_page_icon']; 
				$imageFileType4 = pathinfo($file4['name'], PATHINFO_EXTENSION);
				if((strtolower($imageFileType4) != "jpg") && (strtolower($imageFileType4) !="png") && (strtolower($imageFileType4) !="jpeg") && ($imageFileType4 !="gif")){
					$this->Session->setFlash('Unacceptable file type for portal category icon.');
					$this->redirect('add');
					
				} else {
					//move_uploaded_file($file4['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_p_icon_".time()."_".$file4['name']);
					move_uploaded_file($file4['tmp_name'], UPLOADPATH . 'catimg/' . "c_p_icon_".time().".".$imageFileType4);
					//$sql["category_page_icon"] = "c_p_icon_".time()."_".$file4['name'];
					$sql["category_page_icon"] = "c_p_icon_".time().".".$imageFileType4;
					
					//images move on bucket code start here...
					$bannerImage4 = UPLOADPATH . 'catimg/'. $sql["category_page_icon"];
					/*if(file_exists($bannerImage4)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage4);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage4);
					}*/
				}	
			}
			
			//connect to image server
			$resConnection = ssh2_connect(FCIMAGESERVER,  FCIMAGESERVERPORT);
			if(ssh2_auth_password($resConnection, FCIMAGESERVERUSERNAME, FCIMAGESERVERPASSWORD)){
				$resSFTP = ssh2_sftp($resConnection);
				if(isset($bannerImage1) && !empty($bannerImage1) && file_exists($bannerImage1)){//cat icon code on new server start here...
					$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$sql["cat_icon"], 'w');
					$srcFile = fopen($bannerImage1, 'r');
					$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
					fclose($resFile);
					fclose($srcFile);
					unlink($bannerImage1);
				}
				
				if(isset($bannerImage2) && !empty($bannerImage2) &&  file_exists($bannerImage2)){//portal cat icon move code start here...
					$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$sql["portal_cat_icon"], 'w');
					$srcFile = fopen($bannerImage2, 'r');
					$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
					fclose($resFile);
					fclose($srcFile);
					unlink($bannerImage2);
				}
				
				if(isset($bannerImage3) && !empty($bannerImage3) &&  file_exists($bannerImage3)){//category banner move code start here...
					$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$sql["category_banner"], 'w');
					$srcFile = fopen($bannerImage3, 'r');
					$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
					fclose($resFile);
					fclose($srcFile);
					unlink($bannerImage3);
				}
				
				if(isset($bannerImage4) && !empty($bannerImage4) &&  file_exists($bannerImage4)){//category page icon move code start here...
					$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$sql["category_page_icon"], 'w');
					$srcFile = fopen($bannerImage4, 'r');
					$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
					fclose($resFile);
					fclose($srcFile);
					unlink($bannerImage4);
				}
				
				ssh2_exec($resConnection, 'exit');
				unset($resConnection);
			}
			
			$this->Fcappcategories->save($sql);
			$lastInsertId =  $this->Fcappcategories->id;
			$this->Session->setFlash('Category added successfully.');
			$this->redirect('view');
		}
		
		
		
	}

	
	public function view($page=0) {
	    //paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		$searchCond = array();
		
		if(isset($_GET['searchBy']) && !empty($_GET['searchBy'])){
			 
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "name")){
				
				$searchCond[]='and child.name like "%'.$_GET["searchString"].'%"' ;
			}
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "ct_id")){
				$searchCond[]='and child.category_id="'.$_GET["searchString"].'"' ;
			}
		}	  
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && !empty($_GET['filterBy'])){
			$searchCondStr .=" and child.status = '".$_GET["filterBy"]."'" ;
		}
		$orderBy = "";
		$orderBy = "order by child.category_id desc";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by child.category_id ".$_GET["sortBy"]."" ;			
		}
			
		$searchCondStr = @implode(" ",$searchCond);	
		
		$categories = $this->Fcappcategories->query("select child.*, parent.name as parent_cat_name from cm_categories as child left join cm_categories as parent on child.parent_id = parent.category_id where (child.project='firstcut' or parent.project='firstcut') $searchCondStr $searchCondStr $orderBy limit $offSet, $recPerPage");
			
		$cntCategories = $this->Fcappcategories->query("select count(child.category_id) as countRec from cm_categories as child left join cm_categories as parent on child.parent_id = parent.category_id where (child.project='firstcut' or parent.project='firstcut') $searchCondStr $searchCondStr");
		
			
		$totalRec = @$cntCategories[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		$this->set(compact('categories','categories'));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	}
	
	public function edit($id=null) 
	{
		$categories = $this->Fcappcategories->query("select * from cm_categories where category_id='".$id."'");
		$this->set('categories', $categories);
		
		$data = $this->request->data;
		if(!empty($data)){
			$sql = "update cm_categories set ";
			$sql = $sql . " name = '" .  $data["name"] . "'";
			//$sql = $sql . ", parent_id = '" . $data["category_id"] . "'";
			$sql = $sql . ", slug = '" . $data["slug"] . "'";
			$sql = $sql . ", description = '" . $data["description"] . "'";
			$sql = $sql . ", display_order = '" . $data["display_order"] . "'";
			$sql = $sql . ", status = '" . $data["status"] . "'";
			$sql = $sql . ", app_flag = '" . $data["app_flag"] . "'";
			$sql = $sql . ", heading_back_color = '" . $data["heading_back_color"] . "'";
			$sql = $sql . ", alttag = '" . $data["alttag"] . "'";
	
			if(!empty($data["cat_icon"]["name"])){//Category icon image upload...
			
				$file1 = $data['cat_icon']; 
				
				$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType1) != "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif")){
						$this->Session->setFlash('Unacceptable file type for category icon.');
						$this->redirect('edit/'.$data["id"]);
					
				} else {
					
					if(file_exists(UPLOADPATH . 'catimg/'.$data['hiddenimage1'])){
						unlink(UPLOADPATH . 'catimg/'.$data['hiddenimage1']);								
					}
					
					//move_uploaded_file($file1['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_icon_".time()."_".$file1['name']);
					move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'catimg/' . "c_icon_".time().".".$imageFileType1);
					//$cat_icon = "c_icon_".time()."_".$file1['name'];
					$cat_icon = "c_icon_".time().".".$imageFileType1;
					$sql = $sql . ", cat_icon = '" . $cat_icon . "'";	
					
					//images move on bucket code start here...
					$bannerImage1 = UPLOADPATH . 'catimg/'. $cat_icon;
					/*if(file_exists($bannerImage1)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage1);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage1);
					}*/
					
				}
			}
			
			if(!empty($data["portal_cat_icon"]["name"])){//Portal category icon image upload...
				$file2 = $data['portal_cat_icon']; 
				$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType2) != "jpg") && (strtolower($imageFileType2) !="png") && (strtolower($imageFileType2) !="jpeg") && (strtolower($imageFileType2) !="gif")){
					$this->Session->setFlash('Unacceptable file type for portal category icon.');
					$this->redirect('edit/'.$data["id"]);
					
				} else {
					
					if(file_exists(UPLOADPATH . 'catimg/'.$data['hiddenimage2'])){
						unlink(UPLOADPATH . 'catimg/'.$data['hiddenimage2']);								
					}
					
					//move_uploaded_file($file2['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "p_c_icon_".time()."_".$file2['name']);
					move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'catimg/' . "p_c_icon_".time().".".$imageFileType2);
					//$portal_cat_icon = "p_c_icon_".time()."_".$file2['name'];
					$portal_cat_icon = "p_c_icon_".time().".".$imageFileType2;
					$sql = $sql . ", portal_cat_icon = '" . $portal_cat_icon . "'";
					
					//images move on bucket code start here...
					$bannerImage2 = UPLOADPATH . 'catimg/'. $portal_cat_icon;
					/*if(file_exists($bannerImage2)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage2);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage2);
					}*/
				}
			}
			
			if(!empty($data["category_banner"]["name"])){//Category banner image upload...
				$file3 = $data['category_banner'];
				$imageFileType3 = pathinfo($file3['name'], PATHINFO_EXTENSION);
				
				if((strtolower($imageFileType3) != "jpg") && (strtolower($imageFileType3) !="png") && (strtolower($imageFileType3) !="jpeg") && (strtolower($imageFileType3) !="gif")){
					$this->Session->setFlash('Unacceptable file type for category banner icon.');
					$this->redirect('edit/'.$data["id"]);
					
				} else {
					if(file_exists(UPLOADPATH . 'catimg/'.$data['hiddenimage3'])){
						unlink(UPLOADPATH . 'catimg/'.$data['hiddenimage3']);								
					}
					 
					//move_uploaded_file($file3['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_ban_".time()."_".$file3['name']);
					move_uploaded_file($file3['tmp_name'], UPLOADPATH . 'catimg/' . "c_ban_".time().".".$imageFileType3);
					//$category_banner = "c_ban_".time()."_".$file3['name'];
					$category_banner = "c_ban_".time().".".$imageFileType3;
					$sql = $sql . ", category_banner = '" . $category_banner . "'";
					
					//images move on bucket code start here...
					$bannerImage3 = UPLOADPATH . 'catimg/'. $category_banner;
					/*if(file_exists($bannerImage3)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage3);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage3); 
					}*/
				}
			}
			
			if(!empty($data["category_page_icon"]["name"])){//Category page icon image upload...
				$file4 = $data['category_page_icon']; 
				$imageFileType4 = pathinfo($file4['name'], PATHINFO_EXTENSION);
				if((strtolower($imageFileType4) != "jpg") && (strtolower($imageFileType4) !="png") && (strtolower($imageFileType4) !="jpeg") && (strtolower($imageFileType4) !="gif")){
					$this->Session->setFlash('Unacceptable file type for portal category icon.');
					$this->redirect('edit/'.$data["id"]);
					
				} else {
					if(file_exists(UPLOADPATH . 'catimg/'.$data['hiddenimage4'])){
						unlink(UPLOADPATH . 'catimg/'.$data['hiddenimage4']);
						
						
					}
					
					//move_uploaded_file($file4['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_p_icon_".time()."_".$file4['name']);
					move_uploaded_file($file4['tmp_name'], UPLOADPATH . 'catimg/' . "c_p_icon_".time().".".$imageFileType4);
					//$category_page_icon = "c_p_icon_".time()."_".$file4['name'];
					$category_page_icon = "c_p_icon_".time().".".$imageFileType4;
					$sql = $sql . ", category_page_icon = '" . $category_page_icon . "'";
					
					//images move on bucket code start here...
					$bannerImage4 = UPLOADPATH . 'catimg/'. $category_page_icon;
					/*if(file_exists($bannerImage4)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						
						$files_arr = explode("\n", $bannerImage4);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($bannerImage4);
					}*/
					//connect to image server
					$resConnection = ssh2_connect(FCIMAGESERVER,  FCIMAGESERVERPORT);
					if(ssh2_auth_password($resConnection, FCIMAGESERVERUSERNAME, FCIMAGESERVERPASSWORD)){
						$resSFTP = ssh2_sftp($resConnection);
						if(isset($bannerImage1) && !empty($bannerImage1) && file_exists($bannerImage1)){//cat icon code on new server start here...
							$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$cat_icon, 'w');
							$srcFile = fopen($bannerImage1, 'r');
							$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
							fclose($resFile);
							fclose($srcFile);
							unlink($bannerImage1);
						}
						
						if(isset($bannerImage2) && !empty($bannerImage2) &&  file_exists($bannerImage2)){//portal cat icon move code start here...
							$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$portal_cat_icon, 'w');
							$srcFile = fopen($bannerImage2, 'r');
							$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
							fclose($resFile);
							fclose($srcFile);
							unlink($bannerImage2);
						}
						
						if(isset($bannerImage3) && !empty($bannerImage3) &&  file_exists($bannerImage3)){//category banner move code start here...
							$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$category_banner, 'w');
							$srcFile = fopen($bannerImage3, 'r');
							$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
							fclose($resFile);
							fclose($srcFile);
							unlink($bannerImage3);
						}
						
						if(isset($bannerImage4) && !empty($bannerImage4) &&  file_exists($bannerImage4)){//category page icon move code start here...
							$resFile = fopen("ssh2.sftp://{$resSFTP}/".FCIMAGESERVERIMAGEPATH.$category_page_icon, 'w');
							$srcFile = fopen($bannerImage4, 'r');
							$writtenBytes = stream_copy_to_stream($srcFile, $resFile);
							fclose($resFile);
							fclose($srcFile);
							unlink($bannerImage4);
						}
						
						ssh2_exec($resConnection, 'exit');
						unset($resConnection);
					}
			
				}
			}
			$sql = $sql . " where 	category_id = '". $data["id"] ."'";
			
			$update = $this->Fcappcategories->query($sql);
			
			$this->Session->setFlash('Category edit successfully.');
			$this->redirect('view');
		}
	
	}
	
	
}
?>
