<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'S3.php');
App::import('Vendor', 'sdk/sdk.class.php');
App::import('Vendor', 'AmazonS3.php');
App::import('Component', 'Common');

class SeriesController extends AppController{
	public $name = 'Series';
	public $uses = array('Series', 'Channel', 'User', 'Content', 'Contentnotification');
	public $components = array('Paginator');
	public function beforeFilter() {
	parent::beforeFilter();
	
	}
	
	public function addseries(){
		$searchCondStr = "status = 1";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		$channels = $this->Channel->query("select id, c_name from ch_channel where $searchCondStr");
		$this->set('channels', $channels);
		//echo "<pre>";
		//print_r($channels);
		//die;
		if($this->request->isPost())
		{	
			$data = $this->request->data;
			
			$sql["s_name"] = $data["s_name"];
			$sql["channel_id"] = $data["channel_id"];
			$sql["s_aboutus"] = $data["s_aboutus"];			
			$sql["status"] = 0;
			$sql["created"] = CURRDATE;
			$sql["modified"] = CURRDATE;
			$sql["user_id"] = $this->Session->read('User.id');
			
			$time = time();
			if(!empty($data["s_banner"]["name"]))
			{//series banner image upload...
				$file1 = $data['s_banner']; 
				$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
				/*if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage1'])){
					//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
					//die;
					unlink(UPLOADPATH . 'channel/'.$data['hiddenimage1']);	
				}*/
				
				move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'series/' . "s_b_".$time.".".$imageFileType1);
				$s_banner = "s_b_".$time.".".$imageFileType1;
				$sql["s_banner"] = $s_banner;
				$sbannerImage = UPLOADPATH . 'series/'. $s_banner;
				chmod($sbannerImage, 0777);
				
				if(file_exists($sbannerImage)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
					$files_arr = explode("\n", $sbannerImage);
					$s3 = new S3(ACCESS_KEY,SECRET_KEY);
					
					foreach($files_arr as $file1){
						if(!empty($file1)){
						
							$bucketname = FIRSTPOSTERBUCKET;
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
							$main_arr = array_splice($file1_arr, 6);
							$bucket_file = implode("/",$main_arr); 
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);
							$cache = array('CacheControl' =>'public, max-age=31536000');
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
							
							//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
							if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
								//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
							}
						}
					}
					
					unlink($sbannerImage);
					 
				}
			}
			
			if(!empty($data["s_s_banner"]["name"]))
			{//series small banner image upload...
				$file2 = $data['s_s_banner']; 
				$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
				/*if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage1'])){
					//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
					//die;
					unlink(UPLOADPATH . 'channel/'.$data['hiddenimage1']);	
				}*/
				
				move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'series/' . "s_s_".$time.".".$imageFileType2);
				$s_s_banner = "s_s_".$time.".".$imageFileType2;
				$sql["s_s_banner"] = $s_s_banner;
				$ssbannerImage = UPLOADPATH . 'series/'. $s_s_banner;
				chmod($ssbannerImage, 0777);
				
				if(file_exists($ssbannerImage)){//small banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
					$files_arr = explode("\n", $ssbannerImage);
					$s3 = new S3(ACCESS_KEY,SECRET_KEY);
					
					foreach($files_arr as $file1){
						if(!empty($file1)){
						
							$bucketname = FIRSTPOSTERBUCKET;
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
							$main_arr = array_splice($file1_arr, 6);
							$bucket_file = implode("/",$main_arr); 
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);
							$cache = array('CacheControl' =>'public, max-age=31536000');
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
							
							//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
							if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
								//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
							}
						}
					}
					
					unlink($ssbannerImage);
					 
				}
			}
			
			if(!empty($data["s_c_banner"]["name"]))
			{//series cover banner image upload...
				$file3 = $data['s_c_banner']; 
				$imageFileType3 = pathinfo($file3['name'], PATHINFO_EXTENSION);
				/*if(file_exists(UPLOADPATH . 'channel/'.$data['hiddenimage1'])){
					//echo UPLOADPATH . 'channel/'.$data['hiddenimage1'];
					//die;
					unlink(UPLOADPATH . 'channel/'.$data['hiddenimage1']);	
				}*/
				
				move_uploaded_file($file3['tmp_name'], UPLOADPATH . 'series/' . "s_c_".$time.".".$imageFileType3);
				$s_c_banner = "s_c_".$time.".".$imageFileType3;
				$sql["s_c_banner"] = $s_c_banner;
				$scbannerImage = UPLOADPATH . 'series/'. $s_c_banner;
				chmod($scbannerImage, 0777);
				
				if(file_exists($scbannerImage)){//banner move code start here...
						//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
					$files_arr = explode("\n", $scbannerImage);
					$s3 = new S3(ACCESS_KEY,SECRET_KEY);
					
					foreach($files_arr as $file1){
						if(!empty($file1)){
						
							$bucketname = FIRSTPOSTERBUCKET;
							$file1_arr = explode("/",$file1);
							$filename1 = end($file1_arr);
							$main_arr = array_splice($file1_arr, 6);
							$bucket_file = implode("/",$main_arr); 
							$image_type = image_type_to_mime_type(exif_imagetype($file1));
							$header = array('Content-Type' =>$image_type);
							$cache = array('CacheControl' =>'public, max-age=31536000');
							$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
							
							//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
							if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
								//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
							}
						}
					}
					unlink($scbannerImage);
				}
			}
			
			$this->Series->save($sql);
			$this->Session->setFlash('Series Added Successfully.');
			$this->redirect('viewseries');

		}
		
	}
	
	public function viewseries($page=0){
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
			$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		$orderBy = "";
		//$countryCode = "IN";
		$countryCode = "";
		$searchCondStr = " t1.status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (t2.user_id, '$finalUserArr')";
		}
		//data by filter...
		
		if(isset($_GET['filterBy']) && $_GET['filterBy'] != ""){
			$searchCondStr .=" and t1.status = '".$_GET["filterBy"]."'" ;
		}
		
		$orderBy = "";
		$orderBy = " order by t1.id desc";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by t1.id ".$_GET["sortBy"]."" ;			
		}
		$series = $this->Series->query("select t1.*, t2.c_name from ch_series as t1 inner join ch_channel as t2 on t1.channel_id = t2.id where $searchCondStr $orderBy limit $offSet, $recPerPage");
		
		
		foreach($series as $key=>$val){
			$seriesVideoCount = $this->contentDetailSeries($series[$key]["t1"]["id"], $countryCode);
			$series[$key]["t1"]["videoCount"] = $seriesVideoCount[0][0]["videoCount"];
			$series[$key]["t1"]["videoViewCount"] = $seriesVideoCount[0][0]["videoViewCount"];
		}
		
		$seriesCount = $this->Series->query("select count(t1.id) as countRec from ch_series as t1 inner join ch_channel as t2 on t1.channel_id = t2.id where $searchCondStr");
		
		$totalRec = $seriesCount[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		
		$this->set(compact('series', $series));
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
		
	}
	
	public function editseries($id=null){
		$searchCondStr = "t1.id = '".$id."'";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (t2.user_id, '$finalUserArr')";
		}
		$searchCondStr .= " and t2.status=1";
		$series = $this->Series->query("select t1.* from ch_series as t1 inner join ch_channel as t2 on t1.channel_id = t2.id where $searchCondStr");
		
		if(count($series) <= 0 ){
			$this->Session->setFlash('You have no permission to edit this series.');
			$this->redirect('viewseries');
		} else {	
		    
			$this->set(compact('series', $series));
			//echo "<pre>";
			//print_r($series);
			//die;
			$channels = $this->Channel->query("select id, c_name from ch_channel  where status=1 and FIND_IN_SET (user_id, '$finalUserArr')");
			$this->set(compact('channels', $channels));
			
			if($this->request->isPost())
			{	
				$data = $this->request->data;
				$sql["id"] = $data["id"];
				$sql["s_name"] = $data["s_name"];
				$sql["channel_id"] = $data["channel_id"];
				$sql["s_aboutus"] = $data["s_aboutus"];			
				if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO"))
				{
					$sql["status"] = $data["status"];
					$sql["approved_by"] = $this->Session->read('User.id');
					//insert data in user notification...
					if($data["status"] != $data["oldstatus"]){
						$sql1 = array();
						$sql1["approved"] = $data["status"];
						$sql1["created"] = CURRDATE;
						$sql1["approved_by"] = $this->Session->read('User.id');
						$sql1["c_type"] = 3;
						$sql1["c_name"] = $data["s_name"];
						$sql1["c_id"] = $data["id"];
						$sql1["user_id"] = $data["user_id"];
						$this->Contentnotification->save($sql1);

					}
				}
				//$sql["created"] = CURRDATE;
				$sql["modified"] = CURRDATE;
				
				$time = time();
				if(!empty($data["s_banner"]["name"]))
				{//series banner image upload...
					$file1 = $data['s_banner']; 
					$imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);
					if(file_exists(UPLOADPATH . 'series/'.$data['hiddenimage1'])){
						
						unlink(UPLOADPATH . 'series/'.$data['hiddenimage1']);	
					}
					
					move_uploaded_file($file1['tmp_name'], UPLOADPATH . 'series/' . "s_b_".$time.".".$imageFileType1);
					$s_banner = "s_b_".$time.".".$imageFileType1;
					$sql["s_banner"] = $s_banner;
					$sbannerImage = UPLOADPATH . 'series/'. $s_banner;
					chmod($sbannerImage, 0777);
					
					if(file_exists($sbannerImage)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $sbannerImage);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$cache = array('CacheControl' =>'public, max-age=31536000');
								$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						
						unlink($sbannerImage);
						 
					}
				}
				
				if(!empty($data["s_s_banner"]["name"]))
				{//series small banner image upload...
					$file2 = $data['s_s_banner']; 
					$imageFileType2 = pathinfo($file2['name'], PATHINFO_EXTENSION);
					if(file_exists(UPLOADPATH . 'series/'.$data['hiddenimage2'])){
						unlink(UPLOADPATH . 'series/'.$data['hiddenimage2']);	
					}
					
					move_uploaded_file($file2['tmp_name'], UPLOADPATH . 'series/' . "s_s_".$time.".".$imageFileType2);
					$s_s_banner = "s_s_".$time.".".$imageFileType2;
					$sql["s_s_banner"] = $s_s_banner;
					$ssbannerImage = UPLOADPATH . 'series/'. $s_s_banner;
					chmod($ssbannerImage, 0777);
					
					if(file_exists($ssbannerImage)){//small banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $ssbannerImage);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$cache = array('CacheControl' =>'public, max-age=31536000');
								$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						
						unlink($ssbannerImage);
						 
					}
				}
				
				if(!empty($data["s_c_banner"]["name"]))
				{//series cover banner image upload...
					$file3 = $data['s_c_banner']; 
					$imageFileType3 = pathinfo($file3['name'], PATHINFO_EXTENSION);
					if(file_exists(UPLOADPATH . 'series/'.$data['hiddenimage3'])){
						unlink(UPLOADPATH . 'series/'.$data['hiddenimage3']);	
					}
					
					move_uploaded_file($file3['tmp_name'], UPLOADPATH . 'series/' . "s_c_".$time.".".$imageFileType3);
					$s_c_banner = "s_c_".$time.".".$imageFileType3;
					$sql["s_c_banner"] = $s_c_banner;
					$scbannerImage = UPLOADPATH . 'series/'. $s_c_banner;
					chmod($scbannerImage, 0777);
					
					if(file_exists($scbannerImage)){//banner move code start here...
							//$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
						$files_arr = explode("\n", $scbannerImage);
						$s3 = new S3(ACCESS_KEY,SECRET_KEY);
						
						foreach($files_arr as $file1){
							if(!empty($file1)){
							
								$bucketname = FIRSTPOSTERBUCKET;
								$file1_arr = explode("/",$file1);
								$filename1 = end($file1_arr);
								$main_arr = array_splice($file1_arr, 6);
								$bucket_file = implode("/",$main_arr); 
								$image_type = image_type_to_mime_type(exif_imagetype($file1));
								$header = array('Content-Type' =>$image_type);
								$cache = array('CacheControl' =>'public, max-age=31536000');
								$s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header);
								
								//CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
								if($s3->putObjectFile($file1, $bucketname, $bucket_file,$cache,S3::ACL_PUBLIC_READ,array(),$header)){
									//unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
								}
							}
						}
						unlink($scbannerImage);
					}
				}
				
				$this->Series->save($sql);
				$this->Session->setFlash('Series Updated Successfully.');
				$this->redirect('viewseries');
			}
			
			
		}
		
		
	}
	
	public function detailseries($id=null){
		$searchCondStr = "t1.id='".$id."' and t1.status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (t2.user_id, '$finalUserArr')";
		}
		
		$series = $this->Series->query("select t1.*, t2.c_name from ch_series as t1 inner join ch_channel as t2 on t1.channel_id = t2.id where $searchCondStr");
		
		if(count($series) <= 0 ){
			$this->Session->setFlash('You have no permission to view this series.');
			$this->redirect('viewseries');
		} else {
			$this->set(compact('series', $series));
		}
		
	}
	
	public function addcontentseries(){
		$searchCondStr = "status !=4";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (user_id, '$finalUserArr')";
		}
		
		$series = $this->Series->query("select id, s_name from ch_series where $searchCondStr");
		$this->set(compact('series', $series));
		
		//$country = $this->Country->find('all', array('conditions'=>array('Country.status'=>'1')));
		//$this->set('country', $country);
		
	}
	
	public function deleteseries($id=null){
		$searchCondStr = "t1.id='".$id."'";
		//Find all user id...
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " and FIND_IN_SET (t2.user_id, '$finalUserArr')";
		}
		
		$series = $this->Series->query("select t1.id, t2.c_name from ch_series as t1 inner join ch_channel as t2 on t1.channel_id = t2.id where $searchCondStr");
		if(count($series) <= 0 ){
			$this->Session->setFlash('You have no permission to delete this series.');
			$this->redirect('viewseries');
		} else {
			
			$seriesUpdate = $this->Series->query("update ch_series set status='4', modified='".CURRDATE."' where id='".$id."'");
			$contentUpdate = $this->Series->query("update ch_series_content set status='4', modified='".CURRDATE."' where series_id='".$id."'");
			$this->Session->setFlash('Series deleted successfully.');
			$this->redirect('viewseries');
		}
		
	}
	
	
	
	public function contentDetailSeries($seriesId, $countryCode=null){
		$countryCond = "";
		if(!empty($countryCode)){
			$countryCond = "and t2.country_code='".$countryCode."'";
		}
		//$contentCount = $this->Content->query("select count(t2.content_id) as videoCount, sum(t2.view) as videoViewCount from ch_series_content as t1 inner join cm_contents as t2 on t2.content_id = t1.content_id inner join cm_content_lang as t3 on t3.content_id = t2.content_id where t2.status='A' and t2.approved=1 $countryCond and t1.series_id = '".$seriesId."' and t1.status=1 and t2.category_id in (select category_id from cm_categories where status='A' and project='firstcut')");
		
		$contentCount = $this->Content->query("select count(t2.content_id) as videoCount, sum(t2.view) as videoViewCount from ch_series_content as t1 inner join cm_contents as t2 on t2.content_id = t1.content_id where t2.status='A' and t2.approved=1 $countryCond and t1.series_id = '".$seriesId."' and t1.status=1");
		
		return $contentCount;
	}
	
}
?>