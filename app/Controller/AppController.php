<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::import('Vendor', 'S3');
App::import('Vendor', 'AmazonS3', array('file' => 'sdk'.DS.'sdk.class.php'));
//App::uses('AuthComponent', 'Controller/Component');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
session_start();  
class AppController extends Controller {
	public $components = array( 'Cookie', 'Session', 'Common');
	
	public function beforeFilter(){
		//if((!$this->Session->read('User.id')) && ($this->params['action']!='login') && ($this->params["action"] != "register")){
		if((!$this->Session->check('User.user_id')) && ($this->params['action']!='login') && ($this->params["action"] != "register")){
			$this->redirect('/users/login');
		}
	
	}
	
	
	//Creating Images Thumbnail Function Start here....
	public function createThumbnail($img, $tnWidth, $tnHeight, $canvasSize, $saveImgPath) {
		$image = $this->open_image($img);
		if ($image === false) { return 'ERROR: Unable to open image'; }
			
		// Get original width and height
		$width_orig = imagesx($image);
		$height_orig = imagesy($image);

		// Set a maximum height and width
		$new_width = $tnWidth;
		$new_height = $tnHeight;
		
		if(($new_height == 0) && ($new_width == 0)) {
			$new_width = $canvasSize;
			$new_height = $canvasSize;
		}

		if($new_height == 0) {
			$new_height = ceil(($new_width/$width_orig)*$height_orig);
		}

		if($new_width == 0) {
			$new_width = ceil(($new_height/$height_orig)*$width_orig);
		}

		if($canvasSize == 0) {
			$thumbsize_W = $new_width;
			$thumbsize_H = $new_height;
		}
		else {
			$thumbsize_W = $canvasSize;
			$thumbsize_H = $canvasSize;
		}

		$ratio_orig = $width_orig/$height_orig;

		if ($new_width/$new_height > $ratio_orig) {
		   $new_width = $new_height*$ratio_orig;
		} else {
		   $new_height = $new_width/$ratio_orig;
		}

		//If Image Size is less than the calculated New Size
		if( ($new_width > $width_orig) && ($new_height > $height_orig) ) {
			$new_width = $width_orig;
			$new_height = $height_orig;
		}	
		
		// Resample
		$image_resized = imagecreatetruecolor($thumbsize_W, $thumbsize_H);
		$white = imagecolorallocate($image_resized, 255, 255, 255 );
		
		//fill the background with white (not sure why it has to be in this order)
		imagefill( $image_resized, 0, 0, $white);
	
		//fastimagecopyresampled (&$dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h, $quality = 3)
		$this->fastimagecopyresampled($image_resized, $image, -($new_width/2) + ($thumbsize_W/2), -($new_height/2) + ($thumbsize_H/2), 0, 0, $new_width, $new_height, $width_orig, $height_orig, 3);
	
		//Save Image to the folder
		imagejpeg($image_resized, $saveImgPath, 80);
		
		return "";
	}
	
	//Opens Images for Thumbnailing.
	public function open_image($file) {
		# JPEG:
		$im = @imagecreatefromjpeg($file);
		if ($im !== false) { return $im; }
	
		# GIF:
		$im = @imagecreatefromgif($file);
		if ($im !== false) { return $im; }
	
		# PNG:
		$im = @imagecreatefrompng($file);
		if ($im !== false) { return $im; }
	
		# GD File:
		$im = @imagecreatefromgd($file);
		if ($im !== false) { return $im; }
	
		# GD2 File:
		$im = @imagecreatefromgd2($file);
		if ($im !== false) { return $im; }
	
		# WBMP:
		$im = @imagecreatefromwbmp($file);
		if ($im !== false) { return $im; }
	
		# XBM:
		$im = @imagecreatefromxbm($file);
		if ($im !== false) { return $im; }
	
		# XPM:
		$im = @imagecreatefromxpm($file);
		if ($im !== false) { return $im; }
	
		# Try and load from string:
		$im = @imagecreatefromstring(file_get_contents($file));
		if ($im !== false) { return $im; }
	
		return false;
	}
	
	public function fastimagecopyresampled (&$dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h, $quality = 3) {
	  // Plug-and-Play fastimagecopyresampled function replaces much slower imagecopyresampled.
	  // Just include this function and change all "imagecopyresampled" references to "fastimagecopyresampled".
	  // Typically from 30 to 60 times faster when reducing high resolution images down to thumbnail size using the default quality setting.
	  // Author: Tim Eckel - Date: 09/07/07 - Version: 1.1 - Project: FreeRingers.net - Freely distributable - These comments must remain.
	  //
	  // Optional "quality" parameter (defaults is 3). Fractional values are allowed, for example 1.5. Must be greater than zero.
	  // Between 0 and 1 = Fast, but mosaic results, closer to 0 increases the mosaic effect.
	  // 1 = Up to 350 times faster. Poor results, looks very similar to imagecopyresized.
	  // 2 = Up to 95 times faster.  Images appear a little sharp, some prefer this over a quality of 3.
	  // 3 = Up to 60 times faster.  Will give high quality smooth results very close to imagecopyresampled, just faster.
	  // 4 = Up to 25 times faster.  Almost identical to imagecopyresampled for most images.
	  // 5 = No speedup. Just uses imagecopyresampled, no advantage over imagecopyresampled.
	
	  if (empty($src_image) || empty($dst_image) || $quality <= 0) { return false; }
	  if ($quality < 5 && (($dst_w * $quality) < $src_w || ($dst_h * $quality) < $src_h)) {
		$temp = imagecreatetruecolor ($dst_w * $quality + 1, $dst_h * $quality + 1);
		imagecopyresized ($temp, $src_image, 0, 0, $src_x, $src_y, $dst_w * $quality + 1, $dst_h * $quality + 1, $src_w, $src_h);
		imagecopyresampled ($dst_image, $temp, $dst_x, $dst_y, 0, 0, $dst_w, $dst_h, $dst_w * $quality, $dst_h * $quality);
		imagedestroy ($temp);
	  } else imagecopyresampled ($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
	  return true;
	}
	
	public function userNotification(){
		$this->loadModel('Contentnotification');
		$this->loadModel('User');
		//Find all user id...
		$searchCondStr = "";
		if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){
			$userid = $this->User->query("select id from cm_users where parent_id='".$this->Session->read('User.id')."'");
			$arr = array();
			foreach($userid as $key=>$val){
				
				$arr[] = $val["cm_users"]["id"];
			}
			$finalUser = array_merge(array($this->Session->read('User.id')), $arr); 

			$finalUserArr = implode(",", $finalUser);
			$searchCondStr .= " where FIND_IN_SET (_id, '$finalUserArr')";
		}
		$notificationLists = $this->Contentnotification->query("select * from cm_content_notification $searchCondStr order by id limit 0,10");
		return $notificationLists;
		//$this->set(compact('notificationLists', $notificationLists));
	}
}
