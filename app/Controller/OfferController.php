<?php
App::uses('HttpSocket', 'Network/Http');
class OfferController extends AppController
{
	public $name = 'Offer';
	public $uses = array('Appnamelist','Fcappcontent','FcappBulkupload','FcappContentPrice','Fcappcategories','Channel','Country', 'User', 'Language', 'ContentNotification','Fccgimage');
	public $components = array('Paginator');
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function view()
	{
		//paging section start here...
		$pageNum=1;
		$recPerPage = RECORDPERPAGE;
		if(isset($this->request->query['page']) && $this->request->query['page']!=0)
		{
				$pageNum = $this->request->query['page'];
		}
		$offSet = ($pageNum - 1) * $recPerPage;
		//paging section end here...
		
		//find all user data...
		$searchCondStr = "id != '".$this->Session->read('User.id')."'";
		if($this->Session->read('User.user_type') == 'CPA' || $this->Session->read('User.user_type') == 'CPU')
		{  
	       //check condition for user...
		   $searchCondStr .= " and user_type='CPU'";
		   $searchCondStr .= " and parent_id='".$this->Session->read('User.id')."'";
		}
		
		//data by search...
		
		if(isset($_GET['searchBy']) && !empty($_GET['searchBy']))
		{
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "name")){
				$searchCondStr .=" and name like '%".$_GET["searchString"]."%'" ;
			}
			if(isset($_GET['searchString']) && !empty($_GET['searchString']) && ($_GET['searchBy'] == "email")){
				$searchCondStr .=" and email like '%".$_GET["searchString"]."%'" ;
			}
		}
		
		//data by filter...
		
		if(isset($_GET['filterBy']) && !empty($_GET['filterBy']))
		{
			$searchCondStr .=" and status = '".$_GET["filterBy"]."'" ;
		}
		$orderBy = "";
		$orderBy = "order by id desc";
		if(isset($_GET['sortBy']) && !empty($_GET['sortBy'])){
			$orderBy = " order by id ".$_GET["sortBy"]."" ;			
		}
		
		$users = $this->User->query("SELECT * from offerlist as User where status = '1'");
		$cntUser = $this->User->query("SELECT count(*) as countRec from cm_users where $searchCondStr");
		
		$totalRec = $cntUser[0][0]["countRec"];
		$numOfPage = ceil($totalRec / $recPerPage);
		
		//$this->set(compact('users', $users));
		$this->set('users', $users);
		$this->set('numOfPage', $numOfPage);
		$this->set('pageNum', $pageNum);
	}
	public function addoffer()
	{
		
	}
	

}
?>