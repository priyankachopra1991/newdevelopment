<?php
App::uses('AppController', 'Controller');
//App::uses('HttpSocket', 'Network/Http');
//chmod("/home/cmsadmin", 0755);
//chmod("/home/cmsadmin/public_html", 0755);
//chmod("/home/cmsadmin/public_html/bulkupload", 0777);
//chmod("/home/cmsadmin/public_html/tmpcontents", 0777);
//chmod("/home/cmsadmin/public_html/contents", 0777);
//ini_set('memory_limit', '4024M');
//ini_set('max_execution_time', 3000);
//ini_set('post_max_size', '500M');
//ini_set('upload_max_filesize', '500M');

class ContentsuploadController extends AppController
{


  public $name='Contentsupload';

  public $helpers = array('Js');

	public $uses = array('Appnamelist','Content','Contentsupload','Category','ContentPrice','Bulkupload', 'Channelcountry','User','Casttype','Socialurl', 'Project');
	
public function addcontent() 
	{
	     
         $castType = $this->Casttype->query("select id,name from ch_cast_type");
         $this->set('castType', $castType); 
      
        $country = $this->Channelcountry->query("select id,country_name,country_code from cm_country_code where status=1");
        $this->set('country', $country);
        
    if($this->request->isPost())
	   {       
         $data = $this->request->data; 

        

        $value=$this->Casttype->query("select name from ch_cast_type where id='".$data["role"]."'");

         $sql = array(); 
         $sql1=array();
         $sql["name"] = $data["name"];
         $sql["decription"] = $data["description"];
         $sql["dob"] = $data["dob"];
        // $sql["coverImg"] = $data["coverimg"]["name"];
        // $sql["userImg"] = $data["userimg"]["name"];
         $sql["tagline"] = $data["tagline"];
         $sql["country_code"]=$data["country_code"];
         $sql["roleId"]=$data["role"];
         $sql["role"] = $value['0']['ch_cast_type']['name'];
         $sql["status"]=0;      
        
         $time = time();

       if(!empty($data["coverimg"]["name"]))
	   {//Category icon image upload...
                $file1 = $data['coverimg']; 
                $imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);

                if((strtolower($imageFileType1) != "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif")){
                    $this->Session->setFlash('Unacceptable file type for cast image.');
                    $this->redirect('addcontent');
                } else {
                    //move_uploaded_file($file1['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_icon_".time()."_".$file1['name']);
                    move_uploaded_file($file1['tmp_name'], CHANNEL_PUBLICHTML_PATH . 'artistimg/' . "c1_".$time.".".$imageFileType1);
                    //$sql["cat_icon"] = "c_icon_".time()."_".$file1['name'];
                    $sql["coverImg"] = "c1_".$time.".".$imageFileType1;
                }
            }

            $artistImage = CHANNEL_PUBLICHTML_PATH . 'artistimg/'. $sql["coverImg"];

          
              if(file_exists($artistImage))
			  {//banner move code start here...
                //$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
                $files_arr = explode("\n", $artistImage);
                $s3 = new S3(ACCESS_KEY,SECRET_KEY);
                
                foreach($files_arr as $file1){
                    if(!empty($file1)){
                    
                        $bucketname = FIRSTPOSTERBUCKET;
                        $file1_arr = explode("/",$file1);
                        $filename1 = end($file1_arr);
                        $main_arr = array_splice($file1_arr, 5);
                        $bucket_file = implode("/",$main_arr); 
                        $image_type = image_type_to_mime_type(exif_imagetype($file1));
                        $header = array('Content-Type' =>$image_type);
                        $s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
                        
                        //CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
                        if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
                            //unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
                        }
                    }
                }
                 
            }

           if(!empty($data["userimg"]["name"]))
		   {//Category icon image upload...
                $file1 = $data['userimg']; 
                $imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);

                if((strtolower($imageFileType1) != "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif")){
                    $this->Session->setFlash('Unacceptable file type for cast image.');
                    $this->redirect('addcontent');
                } else {
                    //move_uploaded_file($file1['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_icon_".time()."_".$file1['name']);
                    move_uploaded_file($file1['tmp_name'], CHANNEL_PUBLICHTML_PATH . 'artistimg/' . "c2_".$time.".".$imageFileType1);
                    //$sql["cat_icon"] = "c_icon_".time()."_".$file1['name'];
                    $sql["userImg"] = "c2_".$time.".".$imageFileType1;
                }
            }

           
            $userImage = CHANNEL_PUBLICHTML_PATH . 'artistimg/'. $sql["userImg"];
          
          
              if(file_exists($userImage)){//banner move code start here...
                //$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
                $files_arr = explode("\n", $userImage);
                $s3 = new S3(ACCESS_KEY,SECRET_KEY);
                
                foreach($files_arr as $file1)
				{
                    if(!empty($file1)){
                    
                        $bucketname = FIRSTPOSTERBUCKET;
                        $file1_arr = explode("/",$file1);
                        $filename1 = end($file1_arr);
                        $main_arr = array_splice($file1_arr, 5);
                        $bucket_file = implode("/",$main_arr); 
                        $image_type = image_type_to_mime_type(exif_imagetype($file1));
                        $header = array('Content-Type' =>$image_type);
                        $s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
                        
                        //CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
                        if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
                            //unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
                        }
                    }
                }
                 
            }
            //print_r($sql); die;

          
           if(!empty($data["facebookId"]))
		   {
			    $total=$this->Socialurl->query("SELECT id FROM `ar_artists_socialInfo` where facaebookId='".$data["facebookId"]."'");
				 if(empty($total))
		         {
                   $this->Contentsupload->save($sql);
                   $lastInsertId =  $this->Contentsupload->id;
                 }
		         else
		         {
         
                 $this->Session->setFlash('Artist Record already exist');
                 }

		   }
           else
		   {
			  
           $this->Contentsupload->save($sql);

           $lastInsertId =  $this->Contentsupload->id; 
		   }
		   //echo "SELECT id FROM `ar_artists_socialInfo` where facaebookId='".$data["facebookId"]."'";
		   //die;
          
         
        if($lastInsertId){

            if(!empty($data["facebookId"])){

                 $sql1["facaebookId"]=$data["facebookId"];
                 $sql1["facebookPageId"]=$data["publicId"]; 
                $sql1["facebookUrl"]="http://www.facebook.com/{CLICK_ID}";
                 

            }

          if(!empty($data["googleId"])){

            $sql1["googleId"]=$data["googleId"];
            $sql1["googleUrl"]="https://plus.google.com/u/1/{CLICK_ID}";
           
          }
         
          if($data["twitterId"]){

           $sql1["tweeterId"]=$data["twitterId"];        
           $sql1["tweeterUrl"]="https://twitter.com/https://twitter.com/{CLICK_ID}";

          }   
                    
           $sql1["artistId"]=$lastInsertId; 
           //print_r($sql1); die;
          if(empty($total)){
             $this->Socialurl->save($sql1);
            $this->Session->setFlash('Artist added successfully, Artist will be displayed after approval.');
           }

         //$this->redirect('contentview ');

       }
       
       $this->redirect($_SERVER['HTTP_REFERER']); 
    }

 }

  public function contentview($page=0)
    {


       $country = $this->Channelcountry->query("select id,country_name,country_code from cm_country_code where status=1");

       $this->set('country', $country);


        $PageNum=1;
        $RowofPerpage=10;
        if(isset($this->request->query['page']) && $this->request->query['page']!=0)
        {
                $PageNum=$this->request->query['page'];
        }
        $offset=($PageNum - 1) * $RowofPerpage;
        
        $searchCategories = array();
        $searchCond = array();
        $casts = array();
        $cntCasts = array();
        $num = 0;
        $NumofPage = 0;
         
         
        if(!empty($this->request->data))  
        {         
          

            $data=$this->request->data;     
   
             $name=trim($data['searchStr']);
             $role=trim($data['searchIn']);
             $country_code=$data['country_code'];
            
            if((!empty($name) &&  !empty($role) && !empty($country_code)))
            {
               

             $contentdetail = $this->Contentsupload->query("select * from  ar_artist where country_code='$country_code' and name like '%$name%' and role like '%$role%' order by id desc limit $offset, $RowofPerpage");
             
                
               
            }else if(!empty($role)&& !empty($name))
            {
               
                $contentdetail = $this->Contentsupload->query("select * from ar_artist where name='$name' and  role like '%$role%' order by id desc limit $offset, $RowofPerpage");

            }else if(!empty($name) && !empty($country_code)) {
                          
              $contentdetail = $this->Contentsupload->query("select * from ar_artist where country_code='$country_code' and name  like '%$name%'  order by id desc limit $offset, $RowofPerpage");

            }
            else{ 
                   
                 
            	 $contentdetail =$this->Contentsupload->query("select * from ar_artist order by id desc limit $offset, $RowofPerpage");
              }

           }else{
             
          $contentdetail =$this->Contentsupload->query("select * from ar_artist order by id desc limit $offset, $RowofPerpage");
        } 

         
        // echo "anuj  <pre>"; print_r($contentdetail); die;

        $num= $this->Contentsupload->find('count');
    
        //$cntCasts[0][0]["countRec"];
        //$num = @$cntCasts[0][0]["countRec"];
        $NumofPage=ceil($num/$RowofPerpage);
        $this->set(compact('contentdetail', $contentdetail));   
        $this->set('NumofPage', $NumofPage);
        $this->set('PageNum', $PageNum);
        $this->set('num', $num);
      
          
    }	



    public function contentedit($id=null){

    $castType = $this->Casttype->query("select * from ch_cast_type");
    
    $this->set('castType', $castType); 

    $content = $this->Contentsupload->query("select * from ar_artist where id='".$id."'");

    $this->set('content',  $content );
         
    $social=$this->Socialurl->query("select * from ar_artists_socialInfo where artistId='".$id."'");
    $this->set('social',  $social);


    $country = $this->Channelcountry->query("select id,country_name,country_code from cm_country_code where status=1");
    $this->set('country', $country);
    
   
    $data = $this->request->data;

   // print_r($data); die;
     $value=$this->Casttype->query("select name from ch_cast_type where id='".$data["role"]."'");

      if(!empty($data)){

      	   $sql['id']=$data['id'];
           $sql['name']=$data['name'];
           $sql['decription'] = $data['description'];
           $sql['dob'] = $data['dob'];
           $sql['tagline']  = $data['tagline']; 
           $sql["roleId"]=$data["role"];
           $sql["role"] = $value['0']['ch_cast_type']['name'];
           if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO"))
		   {
			   $sql['approved_status']=$data['status'];
		   }
		   else
		   {
			   $sql['approved_status']='0';
		   }
           
           $time = time();
           
        if(!empty($data["coverimg"]["name"])){//Category icon image upload...
                $file1 = $data['coverimg']; 
                $imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);

                if((strtolower($imageFileType1) != "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif")){
                    $this->Session->setFlash('Unacceptable file type for cast image.');
                    $this->redirect('addcontent');
                } else {
                    //move_uploaded_file($file1['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_icon_".time()."_".$file1['name']);
                    move_uploaded_file($file1['tmp_name'], CHANNEL_PUBLICHTML_PATH . 'artistimg/' . "c1_".$time.".".$imageFileType1);
                    //$sql["cat_icon"] = "c_icon_".time()."_".$file1['name'];
                    $sql["coverImg"] = "c1_".$time.".".$imageFileType1;
                }
            }
           $artistImage = CHANNEL_PUBLICHTML_PATH . 'artistimg/'. $sql["coverImg"];
          
              if(file_exists($artistImage)){//banner move code start here...
                //$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
				$files_arr = explode("\n", $artistImage);
                $s3 = new S3(ACCESS_KEY,SECRET_KEY);
                
                foreach($files_arr as $file1){
                    if(!empty($file1)){
                    
                        $bucketname = FIRSTPOSTERBUCKET;
                        $file1_arr = explode("/",$file1);
                        $filename1 = end($file1_arr);
                        $main_arr = array_splice($file1_arr, 5);
                        $bucket_file = implode("/",$main_arr); 
                        $image_type = image_type_to_mime_type(exif_imagetype($file1));
                        $header = array('Content-Type' =>$image_type);
                        $s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
                        
                        //CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
                        if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
                            //unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
						}
                    }
                }
                 
            }
         
          if(!empty($data["userimg"]["name"])){//Category icon image upload...
                $file1 = $data['userimg']; 
                $imageFileType1 = pathinfo($file1['name'], PATHINFO_EXTENSION);

                if((strtolower($imageFileType1) != "jpg") && (strtolower($imageFileType1) !="png") && (strtolower($imageFileType1) !="jpeg") && (strtolower($imageFileType1) !="gif")){
                    $this->Session->setFlash('Unacceptable file type for cast image.');
                    $this->redirect('addcontent');
                } else {
                    //move_uploaded_file($file1['tmp_name'], CAT_PUBLICHTML_PATH . 'catimg/' . "c_icon_".time()."_".$file1['name']);
                    move_uploaded_file($file1['tmp_name'], CHANNEL_PUBLICHTML_PATH . 'artistimg/' . "c2_".$time.".".$imageFileType1);
                    //$sql["cat_icon"] = "c_icon_".time()."_".$file1['name'];
                    $sql["userImg"] = "c2_".$time.".".$imageFileType1;
                }
            }

           $userImage = CHANNEL_PUBLICHTML_PATH . 'artistimg/'. $sql["userImg"];
          
              if(file_exists($userImage)){//banner move code start here...
                //$get_all_files = shell_exec("find  ".$bannerImage." -name *.jpeg -print");
                $files_arr = explode("\n", $userImage);
                $s3 = new S3(ACCESS_KEY,SECRET_KEY);
                
                foreach($files_arr as $file1){
                    if(!empty($file1)){
                    
                        $bucketname = FIRSTPOSTERBUCKET;
                        $file1_arr = explode("/",$file1);
                        $filename1 = end($file1_arr);
                        $main_arr = array_splice($file1_arr, 5);
                        $bucket_file = implode("/",$main_arr); 
                        $image_type = image_type_to_mime_type(exif_imagetype($file1));
                        $header = array('Content-Type' =>$image_type);
                        $s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header);
                        
                        //CakeLog::info('AWS ICON FILE1='.$file1.' | BUCKET='.$bucketname.'  | bucket_file='.$bucket_file,'debug');
                        if($s3->putObjectFile($file1, $bucketname, $bucket_file, S3::ACL_PUBLIC_READ,array(),$header)){
                            //unlink(CHANNEL_PUBLICHTML_PATH . 'channelimg/'. $sql["c_banner"]);
                        }
                    }
                }
                 
            }
               //print_r($data); die;
           
           $this->Contentsupload->save($sql); 
           /* if(!empty($data['social_id'])){
            	$sql1['id'] = $data['social_id'];
			}
            $sql1["facaebookId"]=$data["facebookId"];
            $sql1["facebookPageId"]=$data["facebookUrl"];
            $sql1["googleId"]=$data["googleId"];
            $sql1["tweeterId"]=$data["tweeterId"];   */
            //$sql1=$sql1 . " where artistId = '". $data["artistId"] ."'";
           /*echo "<pre>";
		   print_r($sql1);
		   die;
		   $update= $this->Socialurl->save($sql1);*/
		  if(empty($data['social_id'])){ 
			  $sql1 = "insert into ar_artists_socialInfo set ";
			  $sql1 = $sql1 . " facaebookId = '" . $data["facebookId"] . "'";
			  $sql1 = $sql1 . ", facebookPageId = '" . $data["facebookUrl"] . "'";
			  $sql1 = $sql1 . ", googleId = '" . $data["googleId"] . "'";
			  $sql1 = $sql1 . ", tweeterId = '" . $data["tweeterId"] . "'";
			  $sql1 = $sql1 . ", artistId = '" . $data["id"] . "'";
		  } else {
		  	 $sql1 = "update ar_artists_socialInfo set ";
			  $sql1 = $sql1 . " facaebookId = '" . $data["facebookId"] . "'";
			  $sql1 = $sql1 . ", facebookPageId = '" . $data["facebookUrl"] . "'";
			  $sql1 = $sql1 . ", googleId = '" . $data["googleId"] . "'";
			  $sql1 = $sql1 . ", tweeterId = '" . $data["tweeterId"] . "'";
			  $sql1 = $sql1 . ", artistId = '" . $data["id"] . "'";
			  $sql1 = $sql1 . " where id = '". $data['social_id'] ."'";
		  }	  
		 
		
		  $update = $this->Socialurl->query($sql1);

           //if($update) {
           	$this->redirect('contentview');
           //}
         
              

      }

    }


}

?>
