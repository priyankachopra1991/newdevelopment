<?php echo $this->Html->css('defaultcss/jquery-ui.css'); ?>
<?php echo $this->Html->script('defaultjs/jquery.min.js'); ?>
<?php echo $this->Html->script('defaultjs/jquery-ui.js'); ?>

<script>
$(function() {
	$( "#datepicker" ).datepicker( { "dateFormat":"yy-mm-dd" } );
});

$(function() {
	$( "#datepicker1" ).datepicker({ "dateFormat":"yy-mm-dd" } );
});  
</script>

<section class="content-header">
	<h1>Upload Content</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
			<i class="fa fa-pencil-square-o"></i>
			Content Management
			</a>
		</li>
		<li class="active">Upload Content</li>
	</ol>
</section>

<section class="content">

	<form class="vuhu-form" name="addContent" id="addContent" method="post" action="add" enctype="multipart/form-data">
	
		<div class="form-group">
			<label for="name">Category*</label>
			<select name="data[Content][parent_id]" id='category_id' onchange="getsubcategory(this.value);" class="form-control" required="required">
				<option value="" >Select category:</option>
			<?php foreach($categoryList as $categoryLists){?> 
				<option value="<?php echo $categoryLists["Category"]["category_id"]; ?>"><?php echo $categoryLists["Category"]["name"]; ?>
					<?php if($categoryLists["Category"]["project"] == "bnama"){ echo "(Bhojpurinama)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "filmy"){ echo "(Filmytales)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "pretty"){ echo "(Prettymodel)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "mini"){ echo "(Minymovies)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "miniwap"){ echo "(Minywap)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "filmywap"){ echo "(Filmywap)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "mastram"){ echo "(Mastram)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "watchearn"){ echo "(Watchearn)"; } ?>
				</option>

			<?php } ?>
			</select>
		</div>
		<div class="form-group">
			<label for="name">Sub Category*</label>
			<select name="data[Content][subcategory_id]" id="subcategory_id" class="form-control" required>
				<option value="" >Select</option>
			</select>
		</div>
		
		<div class="form-group">
			<label for="name">Name*</label>
			<?php echo $this->Form->input('name', array('type'=>'text', 'label'=>'', 'class'=>'form-control','required'=>true));?>
		</div>
		<div class="form-group">
			<label for="name">Price*</label>
			<?php 
			echo $this->Form->input('price', array('type'=>'text', 'label'=>'','class'=>'form-control','value'=>'', 'onkeypress'=>'return isFloatNumber(this,event)', 'required'=>true));?>
		</div>
		<div class="form-group">
			<label for="name">Vodafone Price:</label>
			<select name="data[Content][vodafoneprice]" class="form-control">
				<option value="1.00">1.00</option>
				<option value="2.00">2.00</option>
				<option value="5.00">5.00</option>
				<option value="10.00">10.00</option>
				<option value="15.00">15.00</option>
				<option value="20.00">20.00</option>
			</select>
		</div>
		<div class="form-group">
			<label for="name">Content Type*</label>
			<select name="data[Content][content_type]" id="content_type_id" class="form-control">
				<option value="V">Video</option>
				<option value="W">Wallpaper</option>
				<option value="F">Fulltrack</option>
			</select>
		</div>
		<div class="form-group" id="content_video_dev">
			<label for="name">Content Video*</label>
			<?php echo $this->Form->input('content', array('type'=>'file','label'=>'', 'id'=>'content','required'=>true, 'class'=>'form-control'));?>
			
			<div id="targetLayer"></div>			
		</div>
		
		
			
		<div class="form-group" id="posterdiv">
			<label for="name">Poster*</label>
			<?php echo $this->Form->input('poster', array('type'=>'file','label'=>'','id'=>'poster', 'required'=>true, 'class'=>'form-control'));?>
		</div>
		<div class="form-group">
			<label for="name">Language</label>
			<select name="data[Content][language_code]" id="lang_id" class="form-control">
				<option value="" >Select Language</option>
				<?php foreach($lang as $language):?>
				<option value="<?php echo $language['Language']['language_code'];?>"><?php echo $language['Language']['language'];?> </option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group" style="display:none;">
			<label for="name">Display Order</label>
			<?php echo $this->Form->input('display_order', array('type'=>'text', 'label'=>'', 'class'=>'form-control','value'=>'', 'onkeypress'=>'return isNumber(event)'));?>
		</div>
		<div class="form-group">
			<label for="name">Release Date</label>
			<input type="text" id="datepicker" name="data[Content][release_date]" autocomplete="off" value="<?php echo date('Y-m-d');?>"  class="form-control">
		</div>
		<div class="form-group">
			<label for="name">Expiry Date</label>
			<input type="text" name="data[Content][expiry_date]" id="datepicker1" autocomplete="off"  class="form-control">
		</div>
		<div class="form-group">
			<label for="name">Director Name</label>
			<input type="text" id="datepicker" name="data[Content][director_name]" autocomplete="off" value=""  class="form-control">
		</div>
		<div class="form-group">
			<label for="name">Production House</label>
			<input type="text" id="datepicker" name="data[Content][production_house]" autocomplete="off" value=""  class="form-control">
		</div>
		<div class="form-group">
			<label for="Decription">Description</label>
			<textarea class="form-control" name="data[Content][description]" rows="6" cols="30">
			</textarea>
		</div>
		
		<div class="form-button">
			<button type="submit" id="submit" value="Submit">Submit</button>
			<button type="reset" id="reset" value="Reset">Reset</button>
		</div>   
	</form>
</section>
        
<script>
	function getcategory(project)	
	{
		
		$.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/categories/get_category',
				'data':'project='+project,
				'success':function(resp){
					 
					$('#category_id').html(resp);
				}
		});
	}

function getsubcategory(category_id)	
{
	
	$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/categories/get_sub_category',
			'data':'category_id='+category_id,
			'success':function(resp){
				//alert(resp);
				$('#subcategory_id').html(resp);
			}
	});
}

$(document).ready(function () {
$("#sortpicture").on("change", function(){
	var file_data = $("#sortpicture")[0].files[0];
	var form_data = new FormData(); 
	var n=1;                 // Creating object of FormData class
	//alert(file_data);
	form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
	form_data.append("user_id", 123)                 // Adding extra parameters to form_data
	$("#targetLayer").html('<img src="http://208.109.106.99/cmsadmin/txt/loading.gif"> Loading...').css({"margin-left":"84px", "display":"inline"});
	$("#submit").html('Please wait loading video...');
	$.ajax({
                url: "<?php echo BASE_URL;?>/contents/createThumb",
                data: n,                         // Setting the data attribute of ajax with file_data
                type: 'post',
		processData: false,
		contentType: false,
		success:function(data){
                        if(data) {
                                $("#targetLayer").html("");
                                $("#images_tmp").html(data);
								$("#submit").html('Submit');
                        }
		}
       });
 });

     $('#images_tmp').on('change', function() {
		 //alert("imagetmp");
		var val = $("#images_tmp").prop("value");
		 //alert("s"+val+"e");
		setTimeout(function() {
			if(val!="") {
				$('#posterdiv').hide('slow');
				$('#poster').removeAttr( "required" );
			    
			} else {
				$('#posterdiv').show('slow');
				
			}
		}, 500);
			
     });
	 $('#poster').on('change', function() {
		 //alert("shiva");
		 if($(this).prop("value")) {
			    $('#images_tmp').hide('slow');
				 //$('#targetLayer').hide('slow');
			} else {
				$('#clickme').show('slow');
			}
	 });
	 $('#content_type_id').on('change', function() {
		 var content_type=$(this).prop("value");
		 if(content_type=='V' || content_type=='F') {
				$('#content_video_dev').show('slow');
			} else {
			    $('#content_video_dev').hide('slow');
			}
	 });
});




</script>
           
<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>		 

<script>
function isFloatNumber(item,evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode==46)
        {
            var regex = new RegExp(/\./g)
            var count = $(item).val().match(regex).length;
            if (count > 1)
            {
                return false;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

</script>  
 <script type="text/javascript">
    $(function () {
        $("#content_type_id").change(function () {
            if ($(this).val() == "W") {
                 $('#content').prop('required',false);
            } else {
                 $('#content').prop('required',true);
				
            }
        });
    });
</script>


<?php $errorMsgContent = $this->Session->flash(); ?>
<!-- Modal content start-->
<div id="myModalUserAdd" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">			
			<button type="button" class="close" data-dismiss="modal">&times;</button>			
			<div class="modal-body">
			 <?php echo $errorMsgContent; ?>
			</div>
		</div>
	</div>
</div>



<!-- Modal content end-->	


<?php if($errorMsgContent !=""){ ?>
<script type="text/javascript"> 
jQuery(document).ready(function () {
	$('#myModalUserAdd').modal('show');
});	
</script>	
<?php } ?>

