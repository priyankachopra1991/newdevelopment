<?php echo $this->Html->css('defaultcss/jquery-ui.css'); ?>
<?php echo $this->Html->script('defaultjs/jquery.min.js'); ?>
<?php echo $this->Html->script('defaultjs/jquery-ui.js'); ?>

<script>
$(function() {
	$( "#datepicker" ).datepicker( { "dateFormat":"yy-mm-dd" } );
});

$(function() {
	$( "#datepicker1" ).datepicker({ "dateFormat":"yy-mm-dd" } );
});  
</script>

<section class="content-header">
	<h1>Edit Content</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
			<i class="fa fa-pencil-square-o"></i>
			Content Management
			</a>
		</li>
		<li class="active">Edit Content</li>
	</ol>
</section>

<section class="content">

	<!--<form class="vuhu-form" name="editContent" id="editContent" method="post" action="edit" enctype="multipart/form-data">-->
	<?php echo $this->Form->create('Content', array('novalidate'=>false,'action'=>'edit/'.$content['Content']['content_id'],'enctype'=>'multipart/form-data', 'class'=>'vuhu-form'  ,array('id' => 'content_id')));?>
	
	<?php echo $this->Form->input('content_id',array('type'=>'hidden','name'=>'content_id','value'=>$content['Content']['content_id'],'class'=>'form-control'));?>
		
		<div class="form-group">
			<label for="name">CP Name*</label>
			<input type="text" name="cpname" class="form-control" value="<?php echo $content['Users']['username']; ?>" required="required" readonly="readonly" />
		</div>
		<div class="form-group">
			<label for="name">Category*</label>
			<select id="category_id" name="category_id"  class="form-control"  onchange="getsubcategory(this.value);" required>

			<?php foreach($categoryList as $categoryLists){ ?>
				<option value="<?php echo $categoryLists["Category"]["category_id"]; ?>" <?php if(@$content['Subcategory']['category_id'] == $categoryLists["Category"]["category_id"]){ echo "selected"; }?>><?php echo $categoryLists["Category"]["name"]; ?>
					<?php if($categoryLists["Category"]["project"] == "bnama"){ echo "(Bhojpurinama)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "filmy"){ echo "(Filmytales)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "pretty"){ echo "(Prettymodel)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "mini"){ echo "(Minymovies)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "miniwap"){ echo "(Minywap)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "filmywap"){ echo "(Filmywap)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "mastram"){ echo "(Mastram)"; } ?>
					<?php if($categoryLists["Category"]["project"] == "watchearn"){ echo "(Watchearn)"; } ?>
				
				</option>
			<?php } ?>

			</select>
		</div>
		<div class="form-group">
			<label for="name">Sub Category*</label>
			<select name="subcategory_id" id="subcategory_id"  class="form-control"  required>
			<?php foreach($sub_categories as $key => $value): 
			if($content['Subcategory']['category_id']==$value['Category']['category_id'])
			echo '<option value="'.$value['Category']['category_id'].'" selected>'.$value['Category']['name'].'</option>';
			else
			echo '<option value="'.$value['Category']['category_id'].'">'.$value['Category']['name'].'</option>';
			endforeach;  ?>
			</select>
		</div>
		<div class="form-group">
			
			<?php echo $this->Form->input('name',array('type'=>'text','name'=>'name','value'=>$content['Content']['name'],'class'=>'form-control','required'=>true, 'label'=>'Name*'));?>
		</div>
		<div class="form-group">
			<label for="name">Price*</label>
			<input type="text" class="form-control" name="price" value="<?php echo $content['Content']['price']; ?>" onkeypress="return isFloatNumber(this,event);" />
		</div>
		<div class="form-group">
			<?php $vodafone_price_id='';$vodafone_price=0;
			if(!empty($vodafone_price_arr)){
			$vodafone_price=$vodafone_price_arr['ContentPrice']['price'];
			$vodafone_price_id=$vodafone_price_arr['ContentPrice']['content_price_id'];
			} ?>
			<label for="name">Vodafone Price:</label>
			<select name="vodafoneprice" class="form-control">											
				<option value="1.00" <?php if($vodafone_price==1.00) echo 'selected';?>>1.00</option>
				<option value="2.00" <?php if($vodafone_price==2.00) echo 'selected';?>>2.00</option>
				<option value="5.00" <?php if($vodafone_price==5.00) echo 'selected';?>>5.00</option>
				<option value="10.00" <?php if($vodafone_price==10.00) echo 'selected';?>>10.00</option>
				<option value="15.00" <?php if($vodafone_price==15.00) echo 'selected';?>>15.00</option>
				<option value="20.00" <?php if($vodafone_price==20.00) echo 'selected';?>>20.00</option>
			</select>
		</div>
		
		<div class="form-group">
			<label for="name">Content Type*</label>
			<?php
			if($content['Content']['content_type']=='V'){
				$contentType = "Video"; 
            } else if($content['Content']['content_type']=='F'){
				$contentType = "Fulltrack"; 
			} else if($content['Content']['content_type']=='W'){
				$contentType = "Wallpaper"; 
			} 
			?>
			<input type="text" name="contentType" class="form-control" value="<?php echo $contentType; ?>" required="required" readonly="readonly" />
		</div>
		<?php if($content['Content']['content_type']!='W'){ ?>
		
		<div class="form-group">
			<?php echo $this->Form->input('content', array('type'=>'file','label'=>'Content Video*','id'=>'sortpicture1', 'id'=>'content','required'=>false));
			echo $this->Form->input('old_content', array('type'=>'hidden','label'=>'Content Video*', 'value'=>$content['Content']['content']));
			?>		
			<a href="<?php echo CONTENT_BASE_PATH ?>/contents/<?php echo $content['Content']['content']; ?>" target="blank">View Video</a>
		</div>
		<?php } ?>
		<div class="form-group">
			<?php
			
			echo $this->Form->input('poster', array('type'=>'file','label'=>'Poster', 'required'=>false));
			if(strpos($content['Content']['poster'],'THUMB')===false){
			echo '<img src="'.CONTENT_BASE_PATH.'/contents/'.$content['Content']['poster'].'" width="150px" height="150px" />';
			}else{
			$flag=0;
			$poster_arr=explode('_',$content['Content']['poster']);
			$extention = pathinfo($content['Content']['poster'],PATHINFO_EXTENSION);
			for($i=1;$i<=4;$i++){
			$poster_name=$poster_arr[0].'_THUMB'.$i.'.'.$extention;
			if(file_exists(PUBLICHTML_PATH.'contents/'.$poster_name)){
			if($poster_name==$content['Content']['poster'])$selected='checked'; else $selected='';
			echo '<input type ="radio" name="thumb_poster" value="'.$poster_name.'" '.$selected.'>
			<img src="'.CONTENT_BASE_PATH.'/contents/'.$poster_name.'" width="50px" height="50px" />';
			$flag=1;
			}
			}
			if($flag==0)
			echo '<img src="'.CONTENT_BASE_PATH.'/contents/'.$content['Content']['poster'].'" width="150px" height="150px" />';
			}

			echo $this->Form->input('old_poster', array('type'=>'hidden','label'=>'Poster', 'value'=>$content['Content']['poster']));
			
			?>
		</div>
		<div class="form-group">
			<label for="name">Language</label>
			<select id="lang-id" name="language_code" class="form-control">

				<?php foreach($langs as $key => $value):
				if($value['Language']['language_code']== $content['Content']['language_code'])
				echo '<option value="'.$value['Language']['language_code'].'" selected="selected">'.$value['Language']['language'].'</option>';
				else
				echo '<option value="'.$value['Language']['language_code'].'">'.$value['Language']['language'].'</option>';
				endforeach;?>

			</select>

		</div>
		<?php if(($this->Session->read('User.user_type') =="V3MO") || ($this->Session->read('User.user_type') =="V3MOA")){ ?>
		<div class="form-group" >
			
			<?php echo $this->Form->input('display_order', array('type'=>'text', 'label'=>'Display Order', 'class'=>'form-control', 'name'=>'display_order','value'=>$content['Content']['display_order'],  'onkeypress'=>'return isNumber(event)'));?>
		</div>
		<?php } ?>
		<?php if(($this->Session->read('User.user_type') =="V3MO") || ($this->Session->read('User.user_type') =="V3MOA")){ ?>
		<div class="form-group">
			<label for="name">Status</label>
			<input type="radio" name="status" id="optionsRadiosInline1" value="A" <?php echo (($content['Content']["status"] == "A") ? "checked" : ""); ?>>Active
			<input type="radio" name="status" id="optionsRadiosInline2" value="D" <?php echo (($content['Content']["status"] == "D") ? "checked" : ""); ?>>Deactive
		</div>
		<?php } else { ?>
			<input type="hidden" name="status" value="<?php echo $content['Content']["status"]; ?>" />
		<?php } ?>
		<?php if(($this->Session->read('User.user_type') =="V3MO") || ($this->Session->read('User.user_type') =="V3MOA")){ ?>
		<div class="form-group">
			<label for="name">Approval Status</label>
			<input type="radio" name="approved" id="optionsRadiosInline3" value="0" <?php echo (($content['Content']["approved"] == 0) ? "checked=''" : ""); ?>>Un approved
			<input type="radio" name="approved" id="optionsRadiosInline4" value="1" <?php echo (($content['Content']["approved"] == 1) ? "checked" : ""); ?>>Approved
			<input type="radio" name="approved" id="optionsRadiosInline5" value="2" <?php echo (($content['Content']["approved"] == 2) ? "checked" : ""); ?>>Rejected
		</div>	
		<?php } else { ?>
			<input type="hidden" name="approved" value="<?php echo $content['Content']["approved"]; ?>" />
		<?php } ?>	
		
		<div class="form-group" id="remarkBlock" <?php if($content['Content']["approved"] != 2){?>style="display:none;"<?php } ?>>
			<label for="name">Remark*</label>
			<textarea name="remark" id="remark" class="form-control" rows="6" cols="30" ><?php echo $content['Content']["remark"]; ?></textarea> 
			<?php /*if(($this->Session->read('User.user_type') !="V3MO") || ($this->Session->read('User.user_type') !="V3MOA")){ ?>readonly="readonly" <?php }*/ ?>
		</div>
		<div class="form-group">
			<label for="name">Release Date</label>
			<input type="text" id="datepicker" name="release_date" autocomplete="off" value="<?php echo $content['Content']['release_date'];?>" class="form-control" >
		</div>
		<div class="form-group">
			<label for="name">Expiry Date</label>
			<input type="text" name="expiry_date" id="datepicker1" autocomplete="off" value="<?php echo $content['Content']['expiry_date'];?>" class="form-control" >
		</div>
		<div class="form-group">
			
			<?php echo $this->Form->input('Director Name',array('type'=>'text','name'=>'director_name','value'=>$content['Content']['director_name'],'class'=>'form-control','required'=>false, 'label'=>'Director Name'));?>		
		</div>
		<div class="form-group">
			
			<?php echo $this->Form->input('Production House',array('type'=>'text','name'=>'production_house','value'=>$content['Content']['production_house'],'class'=>'form-control','required'=>false, 'label'=>'Production House'));?>
		</div>
		<div class="form-group">
			<?php echo $this->Form->input('description',array('type'=>'textarea','name'=>'description','value'=>$content['Content']['description'],'class'=>'form-control','required'=>true));?>	
		</div>
		<div class="form-button">
			<button type="submit" id="submit" value="Submit">Submit</button>
			<button type="reset" id="reset" value="Reset">Reset</button>
		</div>   
	</form>
</section>
















<script>
 function getcategory(project)
        {
                $.ajax({
                                'type':'post',
                                'url':'<?php echo BASE_URL;?>/categories/get_category',
                                'data':'project='+project,
                                'success':function(resp){
                                        $('#category_id').html(resp);
                                }
                });
        }

	function getsubcategory(category_id)	
	{
	//alert(category_id);
		$.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/categories/get_sub_category',
				'data':'category_id='+category_id,
				'success':function(resp){
					//alert(resp);
					$('#subcategory_id').html(resp);
				}
		});
	}
</script>

<script>
function myfun(val)
{
	
	//salert(val);die;
	if(val=="2") {
			    $('#makeit').hide('slow');
				$('#Rem').show('slow');
				$("#rem2").attr("required", true);
				$("#make-live").hide("slow");
			} else if (val=="1"){
				$('#Rem').hide('slow');
				$('#makeit').show('slow');
				$("#make-live").show("slow");
				$("#rem2").attr("required", false);
			}
}
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>	

<script>
function isFloatNumber(item,evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode==46)
        {
            var regex = new RegExp(/\./g)
            var count = $(item).val().match(regex).length;
            if (count > 1)
            {
                return false;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

</script>

 <script type="text/javascript">
    
$(document).ready(function() {
    $("input[name$='approved']").click(function() {
		var testVal = $(this).val();
		//alert(testVal);
		if(testVal ==2)
		{
			$("#remarkBlock").css('display', 'block');
			$('#remark').prop('required',true);
		} else {
			$("#remarkBlock").css('display', 'none');
			$('#remark').prop('required',false);
		}
    });
});
</script>
<?php $errorMsgContent = $this->Session->flash(); ?>
<!-- Modal content start-->
<div id="myModalUserAdd" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">			
			<button type="button" class="close" data-dismiss="modal">&times;</button>			
			<div class="modal-body">
			 <?php echo $errorMsgContent; ?>
			</div>
		</div>
	</div>
</div>

<!-- Modal content end-->	
<?php if($errorMsgContent !=""){ ?>
<script type="text/javascript"> 
jQuery(document).ready(function () {
	$('#myModalUserAdd').modal('show');
});	
</script>	
<?php } ?>




