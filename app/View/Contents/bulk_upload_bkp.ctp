<?php echo $this->Html->script('defaultjs/jquery.min.js'); ?>
<section class="content-header">
	<h1>Bulk Upload</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
			<i class="fa fa-pencil-square-o"></i>
			Content Management
			</a>
		</li>
		<li class="active">Bulk Upload</li>
	</ol>
</section>

<section class="content">

	<form class="vuhu-form" name="addContent" id="addContent" method="post" action="bulk_upload" enctype="multipart/form-data">
	<input type="hidden" name="project" value="bnama" />
		
		<div class="form-group">
			<label for="name">Category*</label>
			<select name='category_id' id='category_id' onchange="getsubcategory(this.value);"  class="form-control" required>
				<option value="">Select category</option>
				<?php foreach($categoryList as $categoryLists){?> 
				<option value="<?php echo $categoryLists["Category"]["category_id"]; ?>"><?php echo $categoryLists["Category"]["name"]; ?></option>
				
			<?php } ?>
			</select>
		</div>
		<div class="form-group">
			<label for="name">Sub Category*</label>
			<select name='subcategory_id' id='subcategory_id' class="form-control"  required>
			<option value="" >Select Subcategory</option>
			</select>
		</div>
		<div class="form-group">
			<label for="name">Content Type*</label>
			<select name="contenttype" id="contenttype"  class="form-control"  required>
				<option value="V">Video</option>
				<option value="W">Wallpaper</option>
				<option value="F">Fulltrack</option>
			</select>	
		</div>
		<div class="form-group">
			<label for="name">File Upload(CSV File)*</label>
			<?php echo $this->Form->input('file',array( 'type' => 'file','name'=>'csvfile','label'=>'','required'=>true, 'class'=>"form-control")); ?>
			<a href="<?php echo BASE_PATH.'/bulkupload/format/bulk_upload_format.csv';?>">Download Format</a>
		</div>
		<div class="form-button">
			<button type="submit" id="submit" value="Submit">Submit</button>
			<button type="reset" id="reset" value="Reset">Reset</button>
		</div>   
	</form>
</section>





<script>
	function getsubcategory(category_id)	
	{
		$.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/categories/get_sub_category',
				'data':'category_id='+category_id,
				'success':function(resp){
					$('#subcategory_id').html(resp);
				}
		});
	}
</script>

<script>
	function getcategory(project)	
	{
		
		$.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/categories/get_category',
				'data':'project='+project,
				'success':function(resp){
					 
					$('#category_id').html(resp);
				}
		});
	}
</script>


<?php $errorMsgContent = $this->Session->flash(); ?>
<!-- Modal content start-->
<div id="myModalUserAdd" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">			
			<button type="button" class="close" data-dismiss="modal">&times;</button>			
			<div class="modal-body">
			 <?php echo $errorMsgContent; ?>
			</div>
		</div>
	</div>
</div>



<!-- Modal content end-->	


<?php if($errorMsgContent !=""){ ?>
<script type="text/javascript"> 
jQuery(document).ready(function () {
	$('#myModalUserAdd').modal('show');
});	
</script>	
<?php } ?>



