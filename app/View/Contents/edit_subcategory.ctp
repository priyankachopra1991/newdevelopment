<div class="row">
	 <!-- page header -->
	<div class="col-lg-12">
		<h3 class="page-header">Edit Sub Category</h3>
	</div>
	<!--end page header -->
</div>
<div class="row">
	<div class="col-lg-12">
		<!-- Form Elements -->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">
					<?php echo $this->Form->create('Category', array('novalidate'=>false,'action'=>'edit' ,array('id' => 'formregister')));?>
					   <!-- <form role="form" >-->
						   
							<div class="form-group">
								<label>Please Enter Sub Category</label>
								<?php echo $this->Form->input('',array('type'=>'text','name'=>'name','value'=>$value['Category']['name'],'class'=>'form-control'));
									//echo $this->Form->input('',array('type'=>'hidden','name'=>'id','value'=>$value['Category']['id']));
								?>											
							</div>
							
							<div class="form-group">
								<label>Select Status</label>
								<label class="radio-inline">
									<input type="radio" name="status" id="optionsRadiosInline1" value="A" <?php if($value['Category']['status']=="A") echo "checked"; ?>>Active
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" id="optionsRadiosInline2" value="D" <?php if($value['Category']['status']=="D") echo "checked"; ?>>Dactive
								</label>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					   
					</div>
				</div>
			</div>
		</div>
		 <!-- End Form Elements -->
	</div>
</div>
</div>
<!-- end page-wrapper -->

</div>
<!-- end wrapper -->