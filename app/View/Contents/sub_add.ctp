<div class="row">
	 <!-- page header -->
	<div class="col-lg-12">
		<h3 class="page-header">Add Sub Category</h3>
	</div>
	<!--end page header -->
</div>
<div class="row">
	<div class="col-lg-12">
		<!-- Form Elements -->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">
					<?php echo $this->Form->create('Category', array('novalidate'=>false,'action'=>'sub_add' ,array('id' => 'formregister')));?>
					   <!-- <form role="form" >-->
						   
							<div class="form-group">
								<label>Please Enter Sub Category</label>
								<!--<input class="form-control" placeholder="Enter Category" required>-->
								<?php echo $this->Form->input('', array('type'=>'text', 'name'=>'name','class'=>'form-control','required'=>true));?>
								
								<?php echo $this->Form->input('',array('type'=>'hidden','name'=>'parent_id','value'=>$value));?>
							</div>
							
							<div class="form-group">
							   <label>Select Status</label>
								<label class="radio-inline">
									<input type="radio" name="status" id="optionsRadiosInline1" value="a">Active
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" id="optionsRadiosInline2" value="i">Dactive
								</label>
								
							</div>
						   
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="reset" class="btn btn-success">Reset</button>
						</form>
				   
					   
					</div>
				</div>
			</div>
		</div>
		 <!-- End Form Elements -->
	</div>
</div>
</div>
