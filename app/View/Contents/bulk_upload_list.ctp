
<div class="row">
	<!--  page header -->
	<div class="col-lg-12">
		<h3 class="page-header">Content List
        <div  align="right" style="margin-top:-20px;">   <a href="<?php echo BASE_URL;?>/contents/bulk_upload" class="btn btn-success" style="margin-right:22px;">Add CSV file</a> </div>
        </h3>
	</div>
	<!-- end  page header -->
</div>
<div class="row">
	<div class="col-lg-12">
		<!-- Advanced Tables -->
		<div class="panel-body">

		<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
                          
								<th>BulkId</th>
								<th>CSV File</th>
								<th>CSV Report</th>
								<th>Date</th>
								<th>CP Id</th>
							</tr>
						</thead>
                         
					<?php 
					$y=1;
					foreach($uploadall as $obj){
						
						
						?>
             
						<tr>
						<td><?php echo $obj['Bulkupload']['bulk_id']; ?></td>
                        
						<td><a href="<?php echo BASE_PATH;?>/bulkupload/<?php echo $obj['Bulkupload']['csv_file'] ?>"><?php echo $obj['Bulkupload']['csv_file'];?></a></td>
						<td><a href="<?php echo BASE_PATH;?>/bulkupload/<?php echo $obj['Bulkupload']['error_file'] ?>"><?php echo $obj['Bulkupload']['error_file']; ?></a></td>
						<td><?php echo $obj['Bulkupload']['created'];?></td>
                        <td><?php echo $obj['Bulkupload']['user_id'];?></td>
					</tr>
          
					<?php  } ?>
          </table>          
				</div>



				
			</div>
</div>



</div>
			<?php /*?><div class='pagination'>
				<ul>
                   <li><?php echo $this->Paginator->prev("« previous", array('escape' => false), null,array('class' => 'prevnext disablelink')) .' ';?>
                  <?php echo $this->Paginator->numbers(array('separator' => false, 'modulus' => 2, 'last' => 1,'ellipsis'=>'...', 'currentClass' => 'currentpage'));?>
                   <?php echo $this->Paginator->next("next »", array('escape' => false), null, array('class' => 'prevnext disablelink')); ?></li>
                <ul>
		    </div><?php */?>
<!-- end page-wrapper -->

<!-- </div>-->
<!-- end wrapper -->
