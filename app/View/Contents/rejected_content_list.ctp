<?php echo $this->Html->script('publisherjs/jquery.min.js'); ?>
<div class="row">
<section class="content-header">
	<h1>Rejected Content List</h1>
	<ol class="breadcrumb">
		<li>
			<a href="#">
			<i class="fa fa-pencil-square-o"></i>
			Content Management
			</a>
		</li>
		<li class="active">Rejected Content List</li>
	</ol>
</section>
<section class="table-list">
	<div class="report-view">
		<?php if(isset($_GET["category_id"])){ 
		?>
		<div id="filter-panel" class="filter-panel collapse in" aria-expanded="true" style="">
		<?php } else { ?>
		<div id="filter-panel" class="collapse filter-panel">
		<?php } ?>
			<div class="panel panel-default">
				<div class="panel-body">
					<form class="form-inline" role="form" method="get">
						<div class="form-group">
							<label class="filter-col" style="margin-right:0;" for="pref-perpage">Category:</label>
							<select name='category_id' id='category_id' onchange="getsubcategory(this.value);" class="form-control">
								<option value="">Select</option>
								<?php foreach($categoryList as $categoryLists){ ?>
								<option value="<?php echo $categoryLists["Category"]["category_id"]; ?>" <?php if(@$_GET["category_id"] == $categoryLists["Category"]["category_id"]){ echo "selected"; }?>><?php echo ucfirst(strtolower(trim($categoryLists["Category"]["name"]))); ?>
									<?php if($categoryLists["Category"]["project"] == "bnama"){ echo "(Bhojpurinama)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "filmy"){ echo "(Filmytales)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "pretty"){ echo "(Prettymodel)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "mini"){ echo "(Minymovies)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "miniwap"){ echo "(Minywap)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "filmywap"){ echo "(Filmywap)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "mastram"){ echo "(Mastram)"; } ?>
									<?php if($categoryLists["Category"]["project"] == "watchearn"){ echo "(Watchearn)"; } ?>
								</option>
							<?php } ?>
							</select>                                
						</div> <!-- form group [rows] -->

						<div class="form-group">
							<label class="filter-col" style="margin-right:0;" for="pref-orderby">Sub Category:</label>
							<select name="subcategory_id" id="subcategory_id" class="form-control">
								<option value="" >Select</option>
								<?php if(!empty($searchSubCategories)){
									foreach($searchSubCategories as $key2=> $value2):
									if($value2['Category']['category_id']== $this->request->query["subcategory_id"])
									echo '<option value="'.$value2['Category']['category_id'].'" selected="selected">'.ucfirst(strtolower(trim($value2['Category']['name']))).'</option>';
									else
									echo '<option value="'.$value2['Category']['category_id'].'">'.ucfirst(strtolower(trim($value2['Category']['name']))).'</option>';
									endforeach;
								}?>
							</select>
						</div> <!-- form group [order by] --> 
						<div class="form-group">
							<input type="hidden" name="searchIn" value="name" />
							<label class="filter-col" style="margin-right:0;" for="pref-search">Search:</label>
							<input type="text" name="searchStr" value="<?php echo @$this->request->query["searchStr"];?>" class="form-control input-sm" />
							
							
						</div><!-- form group [search] -->
						<div class="form-group">    
							<button type="submit" class="btn btn-default filter-col" name="searchForm" >
							 Search
							</button>  
						</div>
					</form>
				</div>
			</div>
		</div>    
		<button type="button" class="btn btn-primary data-search" data-toggle="collapse" data-target="#filter-panel">
		<span class="glyphicon glyphicon-cog"></span> Advanced Search
		</button>
		         
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Category</th>
					<th>Subcategory</th>
					<th>Price</th>
					<th>Poster</th>
					<th>Rejected By</th>
					<?php if($this->Session->read('User.user_type') != "V3MBA"){ ?>
						<th>Action</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<form action="" method="post">
				<?php if(empty($contents)){ ?>
					<tr>
						<td colspan="14" class="got-error">
						 Sorry!! no record found.
						</td>
					</tr>
				<?php } else {
					$y=1;
					foreach($contents as $contentsRec){
				?>
					<tr>
						<td class="title-name"><?php echo ucfirst(strtolower(trim($contentsRec['Content']['name'])));?></td>
						<td><?php echo ucfirst(strtolower($contentsRec['Category']['name']));?>
							<br/>
								<?php if($contentsRec['Subcategory']['project'] == "bnama"){ echo "(Bhojpurinama)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "filmy"){ echo "(Filmytales)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "pretty"){ echo "(Prettymodel)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "mini"){ echo "(Minymovies)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "miniwap"){ echo "(Minywap)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "filmywap"){ echo "(Filmywap)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "mastram"){ echo "(Mastram)"; } ?>
								<?php if($contentsRec['Subcategory']["project"] == "watchearn"){ echo "(Watchearn)"; } ?>
						
						</td>
						<td class="data-left"><?php echo ucfirst(strtolower($contentsRec['Subcategory']['name']));?></td>
						<td class="data-right"><?php echo $contentsRec['Content']['price']; ?></td>
						<td> 
						<a href="#"  data-toggle="modal" data-target="#myModal<?php echo $contentsRec['Content']['content_id']; ?>" class="vuhu-view">View</a>
						</td>
						<td class="data-left"><?php echo $contentsRec['Users']['username'];?></td>
						<?php if($this->Session->read('User.user_type') != "V3MBA"){ ?>
						<td colspan="2">
						<a href="<?php echo BASE_URL.'/'.'contents/edit/'.$contentsRec['Content']['content_id']?>" class="vuhu-edit">Edit</a>
						</td>
						<?php } ?>	

					</tr>
					<!-- Modal content start-->

					<div id="myModal<?php echo $contentsRec['Content']['content_id']; ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"><?php echo ucfirst(strtolower(trim($contentsRec['Content']['name'])));?></h4>
								</div>
								<div class="modal-body">
									<p><img src="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contentsRec['Content']['poster'] ?>" width="560px" height="200px" /></p>
									<p>
									<a href="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contentsRec['Content']['poster'] ?>" onclick="javascript:void window.open('<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contentsRec['Content']['poster'] ?>','1436938826415','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" style="text-decoration:underline;">
									Download Image</a>
									<?php if(!empty($contentsRec['Content']['content']))  { ?>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contentsRec['Content']['content'];?>" target="blank"> Download Video</a>
									<?php } ?>

									</p>
								</div>
								
							</div>

						</div>
					</div>
					<!-- Modal content end-->
				
					<?php } ?>
				<?php } ?>
				</form>
			</tbody>
		</table>
	</div>

<?php if((!empty($contents)) && ($NumofPage>1)){?>
	<?php
	
	$Nav="";
	$category_id = "";
	$searchStr = "";
	
	if(@$_GET["category_id"] !=""){
	$category_id = @$_GET["category_id"];
	} else {
	$category_id = @$_POST["category_id"];
	}

	if(@$_GET["subcategory_id"] !=""){
	$subcategory_id = @$_GET["subcategory_id"];
	} else {
	$subcategory_id = @$_POST["subcategory_id"];
	}
	if(@$_GET["searchIn"] !=""){
	$searchIn = @$_GET["searchIn"];
	} else {
	$searchIn = @$_POST["searchIn"];
	}

	if(@$_GET["searchStr"] !=""){
	$searchStr = @$_GET["searchStr"];
	} else {
	$searchStr = @$_POST["searchStr"];
	}
	if(!isset($_GET["page"]))
	{
	$_GET["page"]=1;

	}
	$PageName=BASE_URL."/contents/rejected_content_list";
	?>
	<nav aria-label="Page navigation">
		<ul class="vuhu-pagination pagination">
		<?php
		$page=$PageNum-1;
		?>
		<li>
			<?php if($PageNum > 1) {?>
				<a href="<?php echo $PageName.'/?page='.$page.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&searchIn='.@$searchIn.'&searchStr='.@$searchStr.'&searchForm=Search'; ?> aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			<?php } else { ?>
				<span aria-hidden="true">&laquo;</span>
			<?php } ?>
		</li>
		<?php 
		if($NumofPage >3){
			if($_GET["page"] >=$NumofPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $NumofPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{?>
			<li <?php if($i==@$_GET["page"]){ ?>class="active"<?php } ?>>
				<a href="<?php echo $PageName.'/?page='.$i.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&searchIn='.@$searchIn.'&searchStr='.@$searchStr.'&searchForm=Search'; ?>"><?php echo $i; ?></a>
			</li>
		<?php } ?>
		<?php 
		$page=$PageNum+1;
		?>
			<li>
			<?php if($NumofPage == $PageNum) {?>
				<span aria-hidden="true">&raquo;</span>

			<?php } else {	?>
				<a href="<?php echo $PageName.'/?page='.$page.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&searchIn='.@$searchIn.'&searchStr='.@$searchStr.'&searchForm=Search'; ?>"  aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			<?php } ?>
			</li>
		</ul>
	</nav>
<?php } ?>
</section>
</div>
<script>

function getsubcategory(category_id)	
{
	
	$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/categories/get_sub_category',
			'data':'category_id='+category_id,
			'success':function(data){
				data = data.replace(/\n/g,"").replace(/\r/g,"");
				//alert(resp);
				$('#subcategory_id').html(data);
			}
	});
}

</script>
