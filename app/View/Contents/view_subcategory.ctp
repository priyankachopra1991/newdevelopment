<div class="row">
	<!--  page header -->
	<div class="col-lg-12">
		<h3 class="page-header">Sub Category List</h3>
	</div>
	<!-- end  page header -->
</div>
<div class="row">
	<div class="col-lg-12">
		<!-- Advanced Tables -->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>ID</th>
								<th>NAME</th>
								<th>STATUS</th>
								<th>ACTION</th>
								
							</tr>
						</thead>
					<?php foreach($value as $obj):?>
						<tr>
						<td><?php echo $obj['Category']['category_id']; ?></td>
						<td><?php echo $obj['Category']['name'];?></td>
						<td><?php echo $obj['Category']['status']; ?></td>
						<td colspan="2">
						<?php echo $this->Form->postButton('Edit', array('controller'=>'categories','action'=>'edit_subcategory',$obj['Category']['category_id'])); ?>
						</td>
						</tr>
					<?php endforeach; ?>
					<?php echo "</tbody>" ?>
				</div>
				
			</div>
		</div>
		<!--End Advanced Tables -->
	</div>
</div>



</div>
<!-- end page-wrapper -->

</div>
<!-- end wrapper -->