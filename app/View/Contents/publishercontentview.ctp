<?php echo $this->Html->script('publisherjs/jquery.min.js'); ?>
<div class="row">
	<section class="content-header">
		<h1>Content View</h1>
		<ol class="breadcrumb">
			<li>
				<a href="#">
				<i class="fa fa-shopping-cart"></i>
				Content View
				</a>
			</li>
			<li class="active">Content View</li>
		</ol>
	</section>
		<section class="table-list">
			<div class="report-view">
				<?php if(isset($_GET["category_id"])){ 
				?>
				<div id="filter-panel" class="filter-panel collapse in" aria-expanded="true" style="">
				<?php } else { ?>
				<div id="filter-panel" class="collapse filter-panel">
				<?php } ?>
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-inline" role="form" method="get">
							<div class="form-group">
								<label class="filter-col" style="margin-right:0;" for="pref-perpage">Category:</label>
								<select name='category_id' id='category_id' onchange="getsubcategory(this.value);" class="form-control">
									<option value="">Select</option>
									<?php foreach($topCategory as $topCategory){ ?>
										<option value="<?php echo $topCategory["Category"]["category_id"]; ?>" <?php if($topCategory["Category"]["category_id"]==@$this->request->query["category_id"]){ echo "selected"; } ?>><?php echo $topCategory["Category"]["name"]; ?></option>
									<?php } ?>
								</select>                                
							</div> <!-- form group [rows] -->
							
							<div class="form-group">
								<label class="filter-col" style="margin-right:0;" for="pref-orderby">Sub Category:</label>
								<select name="subcategory_id" id="subcategory_id" class="form-control">
								<option value="" >Select</option>
									<?php if(!empty($searchSubCategories)){
										foreach($searchSubCategories as $key2=> $value2):
									if($value2['Category']['category_id']== $this->request->query["subcategory_id"])
									echo '<option value="'.$value2['Category']['category_id'].'" selected="selected">'.$value2['Category']['name'].'</option>';
									else
											echo '<option value="'.$value2['Category']['category_id'].'">'.$value2['Category']['name'].'</option>';
										endforeach;
									}?>
							</select>

								
							</div> <!-- form group [order by] --> 
							<div class="form-group">
								<label class="filter-col" style="margin-right:0;" for="pref-search">Search:</label>
								<input type="text" class="form-control input-sm" name="content_name" id="content_name" value="<?php echo @$this->request->query["content_name"];?>">
							</div><!-- form group [search] -->
							<div class="form-group">    
								
								<button type="submit" class="btn btn-default filter-col" name="searchForm" >
									 Search
								</button>  
							</div>
						</form>
					</div>
				</div>
			</div>    
			<button type="button" class="btn btn-primary data-search" data-toggle="collapse" data-target="#filter-panel">
				<span class="glyphicon glyphicon-cog"></span> Advanced Search
			</button>
				<h4>Content View</h4>            
				<table>
					<thead>
						<tr>
							<th>Name</th>
							<th>Sub Category</th>
							<th>Price</th>
							<th>Created</th>
							<!--<th>Modified</th>-->
							<th>Preview</th>
												 
						</tr>
					</thead>
					<tbody>
						<?php if(empty($contents)){ ?>
							<tr>
								<td colspan="8" class="got-error">Sorry!! No Record Found.</td>
							</tr>
						<?php } else { ?>
						<?php foreach($contents as $contents){ ?>
							<tr>
							<td class="title-name"><?php echo ucfirst(strtolower(trim($contents["t1"]["name"]))); ?></td>
								<td><?php echo $contents["t2"]["category_name"]; ?></td>
								<td class="price"><?php echo $contents["t1"]["price"]; ?></td>
								<td><?php echo date('d-m-Y', strtotime($contents["t1"]["created"])); ?></td>
								<!--<td><?php echo date('d-m-Y', strtotime($contents["t1"]["modified"])); ?></td>-->
								<td><a href="#"  data-toggle="modal" data-target="#myModal<?php echo $contents["t1"]['content_id']; ?>"><i class="fa fa-play" aria-hidden="true"></i></a></td>
								
							</tr>
							<!-- Modal content start-->
									
								<div id="myModal<?php echo $contents["t1"]["content_id"]; ?>" class="modal fade" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title"><?php echo ucfirst(strtolower(trim($contents["t1"]["name"]))); ?></h4>
												<p class="pre-time"><strong>Preview:</strong> 30sec</p>
											</div>
											<div class="modal-body">
												<?php if($contents["t1"]['content_type'] == "W"){ ?>
												<img src="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contents['t1']['poster'] ?>" style="width:200px; height200px;" />
												<?php } else if($contents["t1"]['content_type'] == "V"){ ?>	
											 
												<video id="myVideo_<?php echo $contents["t1"]["content_id"]; ?>" width="450" height="300"  controls poster="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contents['t1']['poster'] ?>" preload="none">
												<source src="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contents['t1']['content'] ?>#0,10" type="video/mp4">
												</video>
												   
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<!-- Modal content end-->
								<script type="text/javascript">
									$(document).ready(function(){
											$('#myVideo_<?php echo $contents["t1"]["content_id"]; ?>').on('timeupdate', function(){
												if(this.currentTime >= 1 * 30) {
													this.pause();
												}
											});
									});

								</script>
								<script>
								$('#myModal<?php echo $contents["t1"]["content_id"]; ?>').on('hidden.bs.modal', function (e) {
								  // do something...
								  $('#myModal<?php echo $contents["t1"]["content_id"]; ?> video').removeAttr("src");
								});
								</script>

								
						<?php } ?>	
						<?php } ?>
					</tbody>
				</table>                
			</div>
			

	<?php if((!empty($contents)) && ($NumofPage>1)){?>
		<?php
		$Nav="";
		$project = "";
		$category_id = "";
		$searchStr = "";
		if(@$_GET["project"] !=""){
			$project = @$_GET["project"];
		} else {
			$project = @$_POST["project"];
		}
		
		if(@$_GET["category_id"] !=""){
			$category_id = @$_GET["category_id"];
		} else {
			$category_id = @$_POST["category_id"];
		}
		
		if(@$_GET["subcategory_id"] !=""){
			$subcategory_id = @$_GET["subcategory_id"];
		} else {
			$subcategory_id = @$_POST["subcategory_id"];
		}
		
		if(@$_GET["content_name"] !=""){
			$content_name = @$_GET["content_name"];
		} else {
			$content_name = @$_POST["content_name"];
		}
		
		
		if(!isset($_GET["page"]))
		{
		$_GET["page"]=1;

		}
		$PageName=BASE_URL."/contents/publishercontentview";
		?>
		<nav aria-label="Page navigation">
			<ul class="vuhu-pagination pagination">
			<?php
			$page=$PageNum-1;
			?>
			<li>
				<?php if($PageNum > 1) {?>
					<a href="<?php echo $PageName.'/?page='.$page.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&content_name='.@$content_name.'&searchForm=Search'; ?> aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				<?php } else { ?>
					<span aria-hidden="true">&laquo;</span>
				<?php } ?>
			</li>
			<?php 
			if($NumofPage >3){
				if($_GET["page"] >=$NumofPage){
					$minVal = $_GET["page"]-2;
					$maxVal = $_GET["page"];
				} else {
					$minVal = $_GET["page"];
					$maxVal = $_GET["page"]+2;
				}
			} else {
				$minVal = 1;
				$maxVal = $NumofPage;
			}
			
			for($i= $minVal; $i<=$maxVal; $i++)
			{?>
				<li <?php if($i==@$_GET["page"]){ ?>class="active"<?php } ?>>
					<a href="<?php echo $PageName.'/?page='.$i.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&content_name='.@$content_name.'&searchForm=Search'; ?>"><?php echo $i; ?></a>
				</li>
			<?php } ?>
			<?php 
			$page=$PageNum+1;
			?>
				<li>
				<?php if($NumofPage == $PageNum) {?>
					<span aria-hidden="true">&raquo;</span>

				<?php } else {	?>
					<a href="<?php echo $PageName.'/?page='.$page.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&content_name='.@$content_name.'&searchForm=Search'; ?>"  aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				<?php } ?>
				</li>
			</ul>
		</nav>
	<?php } ?>
	</section>
</div>

<?php $errorMsgPublisherContent = $this->Session->flash(); ?>
<!-- Modal content start-->
<div id="myModalUserAdd" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">			
			<button type="button" class="close" data-dismiss="modal">&times;</button>			
			<div class="modal-body">
			 <?php echo $errorMsgPublisherContent; ?>
			</div>
		</div>
	</div>
</div>

<!-- Modal content end-->	

<?php if($errorMsgPublisherContent !=""){ ?>
<script type="text/javascript"> 
jQuery(document).ready(function () {
	$('#myModalUserAdd').modal('show');
});	
</script>	
<?php } ?>
	
<script type="text/javascript">
function getsubcategory(category_id)	
{
	
	$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/categories/get_sub_category',
			'data':'category_id='+category_id,
			'success':function(data){
				data = data.replace(/\n/g,"").replace(/\r/g,"");
				//alert(resp);
				$('#subcategory_id').html(data);
			}
	});
}

</script>

