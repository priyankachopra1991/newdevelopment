<?php echo $this->Html->script('publisherjs/jquery.min.js'); ?>

	<section class="table-list">
		<div class="report-view">
		    <?php if(isset($_GET["category_id"])){ 
			?>
		    <div id="filter-panel" class="filter-panel collapse in" aria-expanded="true" style="">
			<?php } else { ?>
			<div id="filter-panel" class="collapse filter-panel">
			<?php } ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form" method="get">
                        <div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-perpage">Category:</label>
                            <select name='category_id' id='category_id' onchange="getsubcategory(this.value);" class="form-control">
								<option value="">Select</option>
								<option value="0" <?php if(@$this->request->query["category_id"] == "0"){ echo "selected"; } ?>>Select All</option>
								<?php foreach($topCategory as $topCategory){ ?>
									<option value="<?php echo $topCategory["Category"]["category_id"]; ?>" <?php if($topCategory["Category"]["category_id"]==@$this->request->query["category_id"]){ echo "selected"; } ?>><?php echo $topCategory["Category"]["name"]; ?></option>
								<?php } ?>
                            </select>                                
                        </div> <!-- form group [rows] -->
                        
                        <div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-orderby">Sub Category:</label>
                            <select name="subcategory_id" id="subcategory_id" class="form-control">
							<option value="" >Select</option>
								<?php if(!empty($searchSubCategories)){
									foreach($searchSubCategories as $key2=> $value2):
								if($value2['Category']['category_id']== $this->request->query["subcategory_id"])
								echo '<option value="'.$value2['Category']['category_id'].'" selected="selected">'.$value2['Category']['name'].'</option>';
								else
										echo '<option value="'.$value2['Category']['category_id'].'">'.$value2['Category']['name'].'</option>';
									endforeach;
								}?>
						</select>

                            
                        </div> <!-- form group [order by] --> 
						<div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Search:</label>
                            <input type="text" class="form-control input-sm" name="content_name" id="content_name" value="<?php echo @$this->request->query["content_name"];?>">
                        </div><!-- form group [search] -->
                        <div class="form-group">    
                            
                            <button type="submit" class="btn btn-default filter-col" name="searchForm" >
                                <span class="glyphicon glyphicon-record"></span> Search
                            </button>  
                        </div>
                    </form>
                </div>
            </div>
        </div>    
        <button type="button" class="btn btn-primary data-search" data-toggle="collapse" data-target="#filter-panel">
            <span class="glyphicon glyphicon-cog"></span> Advanced Search
        </button>
			<h4>Purchase Content</h4>
			<form name="contentPurchase" id="contentPurchase" method="post">
			<input type="hidden" name="selectpage" id="selectpage" value="" />
			<table>
				<thead>
					<tr>
						<th><input type="checkbox" name="select_all" id="select_all" /></th>
						<th>Name</th>
						<th>Sub Category</th>
						<th>Price</th>
						<th>Created</th>
						<!--<th>Modified</th>-->
						<th>Preview</th>
						                     
					</tr>
				</thead>
				<tbody>
					<?php if(empty($contents)){ ?>
						<tr>
							<td colspan="8" class="got-error">Sorry!! No Record Found.</td>
						</tr>
					<?php } else { ?>
					<?php foreach($contents as $contents){ ?>
						<tr>
							<td>
							<?php if(in_array($contents["t1"]["content_id"], $finalcontentArray)){ ?>
								<input type="checkbox" name="contentId[]" id="contentId[]" value="<?php echo $contents["t1"]["content_id"]; ?>"  disabled="disabled" />
							<?php } else { ?>
								<input type="checkbox" name="contentId[]" id="contentId[]" class="case" value="<?php echo $contents["t1"]["content_id"]; ?>" />
							<?php } ?>
							</td>
							<td class="title-name"><?php echo ucfirst(strtolower(trim($contents["t1"]["name"]))); ?></td>
							<td><?php echo $contents["t2"]["category_name"]; ?></td>
							<td class="price"><?php echo $contents["t1"]["price"]; ?></td>
							<td><?php echo date('d-m-Y', strtotime($contents["t1"]["created"])); ?></td>
							<!--<td><?php echo date('d-m-Y', strtotime($contents["t1"]["modified"])); ?></td>-->
							<td><a href="#"  data-toggle="modal" data-target="#myModal<?php echo $contents["t1"]['content_id']; ?>"><i class="fa fa-play" aria-hidden="true"></i></a></td>
							
						</tr>
						<!-- Modal content start-->
								
							<div id="myModal<?php echo $contents["t1"]["content_id"]; ?>" class="modal fade" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title"><?php echo ucfirst(strtolower(trim($contents["t1"]["name"]))); ?></h4>
											<p class="pre-time"><strong>Preview:</strong> 30sec</p>
										</div>
										<div class="modal-body">
											<?php if($contents["t1"]['content_type'] == "W"){ ?>
											<img src="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contents['t1']['poster'] ?>" style="width:200px; height200px;" />
											<?php } else if($contents["t1"]['content_type'] == "V"){ ?>	
										 
											<video id="myVideo_<?php echo $contents["t1"]["content_id"]; ?>" width="450" height="300"  controls poster="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contents['t1']['poster'] ?>" preload="none">
											<source src="<?php echo CONTENT_BASE_PATH;?>/contents/<?php echo $contents['t1']['content'] ?>#0,10" type="video/mp4">
											</video>
											   
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<!-- Modal content end-->
							<script type="text/javascript">
								$(document).ready(function(){
										$('#myVideo_<?php echo $contents["t1"]["content_id"]; ?>').on('timeupdate', function(){
											if(this.currentTime >= 1 * 30) {
												this.pause();
											}
										});
								});

							</script>
							<script>
							$('#myModal<?php echo $contents["t1"]["content_id"]; ?>').on('hidden.bs.modal', function (e) {
							  // do something...
							  $('#myModal<?php echo $contents["t1"]["content_id"]; ?> video').removeAttr("src");
							});
							</script>

							
					<?php } ?>	
					<?php } ?>
				</tbody>
				<tfoot>
				<tr>
					<td colspan="6">
					<input type="submit" name="purchaseSubmit" id="purchaseSubmit" value="Submit" class="my-button" />
					</td>
				
				</tr>				
				</tfoot>
			</table>
			
			</form>			
		</div>
	</section>
	<?php if(!empty($contents)){?>
	<?php
	$Nav="";
	$project = "";
	$category_id = "";
	$searchStr = "";
	if(@$_GET["project"] !=""){
		$project = @$_GET["project"];
	} else {
		$project = @$_POST["project"];
	}
	
	if(@$_GET["category_id"] !=""){
		$category_id = @$_GET["category_id"];
	} else {
		$category_id = @$_POST["category_id"];
	}
	
	if(@$_GET["subcategory_id"] !=""){
		$subcategory_id = @$_GET["subcategory_id"];
	} else {
		$subcategory_id = @$_POST["subcategory_id"];
	}
	
	if(@$_GET["content_name"] !=""){
		$content_name = @$_GET["content_name"];
	} else {
		$content_name = @$_POST["content_name"];
	}
	
	$PageName=BASE_URL."/contents/publisherpurchaseview";
	for($i= $PageNum-1; $i<=$PageNum; $i++)
	{
		if($PageNum==$i){
			
			$Nav .=$i."&nbsp;";
			}
		else{
			/*$Nav.='<a href="'.$PageName.'/?page='.$i.'&selectvalue='.@$selectvalue.'&searchStr='.@$searchStr.'&searchForm=Search">'.$i.'</a>&nbsp;';*/
			}
	}
	if($PageNum>1)
	{
		$page=$PageNum-1;
	   
		$Prev='<a href="'.$PageName.'/?page='.$page.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&content_name='.@$content_name.'&searchForm=Search">[Previous]</a>&nbsp;';
	}
	else
	{

		$First="&nbsp;";
		$Prev="&nbsp;";
	}

	if($PageNum<$NumofPage)
	{

		$page=$PageNum+1;
		
		$Next='<a href="'.$PageName.'/?page='.$page.'&category_id='.@$category_id.'&subcategory_id='.@$subcategory_id.'&content_name='.@$content_name.'&searchForm=Search">[Next]</a>&nbsp;';
	}
	else
	{   
		$Last="&nbsp;";
		$Next="&nbsp;";
	   
	}
   ?>			

<div class='pagination'>
	<ul>
		 <li><?php echo "$Prev $Nav $Next"; ?></li>
	</ul>
</div>
<?php } ?>


<script type="text/javascript">
function getsubcategory(category_id)	
{
	
	$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/categories/get_sub_category',
			'data':'category_id='+category_id,
			'success':function(data){
				data = data.replace(/\n/g,"").replace(/\r/g,"");
				//alert(resp);
				$('#subcategory_id').html(data);
			}
	});
}

</script>


<script type="text/javascript">
$(function () {
    $("#select_all").click(function () {
		
        if ($("#select_all").is(':checked')) {
			$('#myModalConfirm').modal('show');
            $(".case").prop("checked", true);
        } else {
			$("#selectpage").val('');
            $(".case").prop("checked", false);
        }
    });
});

function selectValue(valuetype){
	var fieldValue = valuetype;
	$("#selectpage").val(fieldValue);
	$('#myModalConfirm').modal('hide');
}

</script>

<!-- Modal content start-->	
						
<div id="myModalConfirm" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation Message</h4>

			</div>
			<div class="modal-body">
				<p>Would you like to select the content?
				<a href="#" onclick="selectValue('all')">All Pages</a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="selectValue('current')">Only Current Page</a>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- Modal content end-->

