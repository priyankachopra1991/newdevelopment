<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" name="editChannel" id="editChannel" method="post" action="<?php echo BASE_URL."/channels/editchannel/".$channel["Channel"]["id"]; ?>" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit Channel</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<input type="hidden" name="user_id" value="<?php echo $channel["Channel"]["user_id"]; ?>" />
							<div class = "col s8">
								<div class = "row">
									<div class="input-field col s12">
										<input placeholder="Channel Name" id="c_name" name="c_name" value="<?php echo $channel["Channel"]["c_name"]; ?>" type="text" class="validate">
									</div>
								<div class="input-field col s12">
									<input placeholder="Channel Id" id="id" name="id" value="<?php echo $channel["Channel"]["id"]; ?>"  type="text" readonly="readonly" class="validate">
								</div>
								
								
								
								
								<div class="input-field series-file file_icon col s12">
									<label for="channel">Choose Channel Banner</label>
									<i class = "material-icons">perm_media</i>
									<!--<input type="file" id="c_banner" name="c_banner">-->
									    <?php $styleBanner = 'background-image: url('.AMAZONIMAGEURL.$channel['Channel']['c_banner'].'), opacity:1;'; ?>
										<?php //echo $this->Form->input('c_banner', array('type'=>'file','label'=>'','id'=>'c_banner', 'src'=>AMAZONIMAGEURL.$channel['Channel']['c_banner']));?>	
										<?php echo $this->Form->input('c_banner', array('type'=>'file','label'=>'','id'=>'c_banner', 'style'=>$styleBanner));?>	
										
									<br clear="all" />
									<br clear="all" />
									
									<img src="<?php echo AMAZONIMAGEURL.$channel["Channel"]["c_banner"]; ?>" height="100" width="100" />
									<?php
									echo $this->Form->input('hiddenimage1',array('type'=>'hidden','value'=>$channel["Channel"]["c_banner"]));
									?>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Description" id="c_description" name="c_description" class="materialize-textarea" ><?php echo $channel["Channel"]["c_description"]; ?></textarea>
								</div>
								
								<div class="input-field col s12">
								
								<select name="country_code" id="country_code" onchange="getLanguage(this.value);">
									<option value="">Country</option>
									<?php foreach($country as $countryRec){ ?>
										<option value="<?php echo $countryRec["Country"]["country_code"]; ?>" <?php if($countryRec["Country"]["country_code"] == $channel["Channel"]["country_code"]){ echo "selected"; } ?>><?php echo $countryRec["Country"]["country_name"]; ?></option>
									<?php } ?>
									
								</select>
							</div>
							
							<!--<div class="input-field col s12">
								<select name='c_lang' id='c_lang' required  class="dropdown">
									<option value="">Select</option>
								</select>
							</div>-->
							
							<div class="input-field col s12">
								<select name='c_lang' id='c_lang'>
									<option value="">Select</option>
								</select>
							</div>
							<div class="input-field col s12">
								<input placeholder="Facebook Connect URL" id="facebook_connect_url" name="facebook_connect_url" value="<?php echo $channel["Channel"]["facebook_connect_url"]; ?>" type="text" class="validate">
							</div>
							
							<div class="input-field col s12">
								<input placeholder="Youtube Connect URL" id="youtube_connect_url" name="youtube_connect_url" value="<?php echo $channel["Channel"]["youtube_connect_url"]; ?>" type="text" class="validate">
							</div>
							
							</div>                    
						</div>
						<div class = "col s4">
							<div class = "row">                                                    
							<div class="input-field series-icon file_icon col s12">                             
								<i class = "material-icons">perm_media</i>
								<!--<input type="file" name="c_icon" id="c_icon">-->
								<?php echo $this->Form->input('c_icon', array('type'=>'file','label'=>'','id'=>'c_icon'));?>								
								<label for="channel_icon">Choose Channel Icon</label> 
								<img src="<?php echo AMAZONIMAGEURL.$channel["Channel"]["c_icon"]; ?>" height="100" width="100" />
                                <?php
								echo $this->Form->input('hiddenimage2',array('type'=>'hidden','value'=>$channel["Channel"]["c_icon"]));
								?>
							</div>
							<div class="input-field col s12">
								<select id = "c_legaldoc_type" name="c_legaldoc_type">
									<option value="">Legal Document Type</option>
									<option value="Treadmark" <?php if($channel["Channel"]["c_legaldoc_type"] == "Treadmark"){ echo "selected"; } ?>>Treadmark</option>
									<option value="Licence" <?php if($channel["Channel"]["c_legaldoc_type"] == "Licence"){ echo "selected"; } ?>>Licence</option>
								</select>
							</div>
							<div class="input-field series-icon file_icon col s12">                             
								<i class = "material-icons">perm_media</i>
								<!--<input type="file" name="c_legaldoc" id="c_legaldoc">-->
								<?php echo $this->Form->input('c_legaldoc', array('type'=>'file','label'=>'','id'=>'c_legaldoc'));?>								
								<label for="channel_icon">Choose Licence</label>
								
                                <?php
								echo $this->Form->input('hiddenimage3',array('type'=>'hidden','value'=>$channel["Channel"]["c_legaldoc"]));
								?>
							</div>
							<div class="input-field col s12">
							<?php if(!empty($channel["Channel"]["c_legaldoc"])){ ?>	
		
									<a href="<?php echo AMAZONIMAGEURL.$channel["Channel"]["c_legaldoc"]; ?>">View Licence</a>
								<?php } ?>
							</div>
							<?php if($this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
							    <input type="hidden" name="oldstatus" value="<?php echo $channel["Channel"]["status"]; ?>" />
								<div class="input-field col s12">
									<select name ="status">
										<option value="">Status</option>
										<option value="1" <?php if($channel["Channel"]["status"] == "1"){ echo "selected"; } ?>>Approved </option>
										<option value="2" <?php if($channel["Channel"]["status"] == "2"){ echo "selected"; } ?>>Rejected</option>
										<option value="0" <?php if($channel["Channel"]["status"] == "0"){ echo "selected"; } ?>>Wating for Approval</option>
									</select>
								</div>
							<?php } ?>
							<div class="input-field col s12">
								<input placeholder="Channel By" id="user_name" name="user_name" value="<?php echo $user["User"]["name"]; ?>" type="text"  readonly="readonly" class="validate">
							</div>
						</div>                          
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $("#editChannel")[0].reset();
	})
	$("#submitForm").click(function(){
	    var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var c_name = $.trim($('#c_name').val());
		var c_legaldoc = $.trim($('#c_legaldoc').val());
		var c_banner = $('#c_banner').val().split('.').pop().toLowerCase();
		var c_icon = $('#c_icon').val().split('.').pop().toLowerCase();
		var c_legaldoc = $('#c_legaldoc').val().split('.').pop().toLowerCase();
		var userType = '<?php echo $this->Session->read('User.user_type'); ?>';
		
		if(c_name ==""){
		    $('#c_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(c_name)){
			$('#c_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if( $('#country_code').val() == "") {
		    $('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if( $('#c_lang').val() == "") {
		    $('#c_lang').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select language.");
			return false;
		}
		
		if(c_banner !="" && $.inArray(c_banner, ['png','jpg','jpeg']) == -1){
			$('#c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel banner as png, jpg, jpeg.");
			return false;
		}
		
		if(c_icon !="" && $.inArray(c_icon, ['png','jpg','jpeg']) == -1){
			$('#c_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel icon as png, jpg, jpeg.");
			return false;
		}
		
		if(c_legaldoc !="" && $.inArray(c_legaldoc, ['pdf','doc', 'docx']) == -1){
			$('#c_legaldoc').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel legal document as  pdf, doc.");
			return false;
		}
		
		if(c_legaldoc != "" && $('#c_legaldoc_type').val() == "" ){
			$('#c_legaldoc_type').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select legal documement type.");
			return false;
		}
		
		if(userType == 'V3MO' || userType == 'V3MOA'){
			if( $('#status').val() == "") {
				$('#status').focus() ;
				$(".errorMsgFrm").css("display", "block");
				$('.errorMsgFrm').html("Please select status.");
				return false;
			}
		}
		document.editChannel.submit();
	})
	
	$('select').material_select();
	//fetch language data onload...
	var countryCode = '<?php echo $channel["Channel"]["country_code"]; ?>';
	var languageCode = '<?php echo $channel["Channel"]["c_lang"]; ?>';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#c_lang').material_select('destroy');
			//$('#c_lang').html(resp);
			var $selectDropdown = 
			  $("#c_lang")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$selectDropdown.trigger('contentChanged');
		}
	});
})

function getLanguage(countryCode){
	$('select').material_select();
    var languageCode = '<?php echo $channel["Channel"]["c_lang"]; ?>';
	//alert(countryCode);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#c_lang').material_select('destroy');
			//$('#c_lang').html(resp);
			var $selectDropdown = 
			  $("#c_lang")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$selectDropdown.trigger('contentChanged');
		}
	});
}

$('select').on('contentChanged', function() {
	// re-initialize (update)
	$(this).material_select();
});

</script>	