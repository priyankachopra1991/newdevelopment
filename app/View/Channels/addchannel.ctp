<div class = "table-format-data">
	<div class = "row">
		<div class = "col s8 offset-s2">                     
			<div class = "add-channel-form">
				<div class = "channel-header">
					<h2>Add Channel</h2> 
					<p>This information will let us know more about your channel</p>
				</div>
				<div class="row">
					<div class="col s12">
						<ul class="tabs">
							<li class="tab col s3"><a class="active" href="#basicinfo" id="tabberDisableBasic">Basic Info</a></li>
							<li class="tab col s3"><a href="#identity" id="tabberDisableIdentity">Identity</a></li>
							<li class="tab col s3 "><a href="#socialConnect" id="tabberDisableSocialConnect">Social Connect</a></li>
							<li class="tab col s3"><a href="#legalInfo" id="tabberDisableLegalInfo">Legal Information</a></li>
						</ul>
					</div>
					<form name="addChannel" id="addChannel" action="<?php echo BASE_URL; ?>/channels/addchannel" method="post"  enctype="multipart/form-data">
						<div id="basicinfo" class="col s12 tab-content">
							
							<div class = "row">
							<div class="errorMsgFrm" style="display:none;"></div>
								<div class="input-field col s6">
									<i class="material-icons prefix">live_tv</i>
									<input name="c_name" id="c_name" type="text" class="validate">
									<label for="icon_prefix">Channel Name</label>
								</div>
									  
								<div class="input-field col s6">
									<select name="country_code" id="country_code" onchange="getLanguage(this.value);">
									<option value="">Country</option>
									<?php foreach($country as $countryRec){ ?>
										<option value="<?php echo $countryRec["Country"]["country_code"]; ?>"><?php echo $countryRec["Country"]["country_name"]; ?></option>
									<?php } ?>
									
								</select>
								</div>
								<div class="input-field col s6">
									<i class="material-icons prefix">assignment</i> 
									<!--<input type="text" name="c_description" id="c_description" class="validate">
									<label for="icon_prefix">Channel Description</label>--->
									<textarea placeholder = "Channel Description" id="c_description" name="c_description" class="materialize-textarea"> </textarea>
								</div>  
								<div class="input-field col s6">
									<select name='c_lang' id='c_lang'>
										<option value="Language">Language</option>
									</select>
								</div>								
							</div>                             
							
							<div class="input-field col s12">
								<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityTab">Next</a>
							</div>
						</div>
						<div id="identity" class="col s12 tab-content">
							<div class = "row  upload-data">
								<div class="errorMsgFrm" style="display:none;"></div>
								<div class = "col s6">
									<!---<div class="file_icon">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('c_icon', array('type'=>'file','label'=>'','id'=>'c_icon'));?>
										<label>Choose Channel's Icon
									</div>---> 
									<label style="font-size:18px;">Choose Channel's Icon</label>
								    <div class="file-field input-field">
                                          <div class="btn right red darken-1">
                                            <span>Browse</span>
                                            <input type="file" name = "data[c_icon]" id="c_icon" />
                                          </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text">
                                        </div> 
                                        <span class = "spacer"></span>                                   
                                    </div>
								</div>
								<div class = "col s6">
									<!---<div class="file_icon banner-image">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('c_banner', array('type'=>'file','label'=>'','id'=>'c_banner'));?>
										<label>Choose Channel's Banner
									</div>--->
									<label style="font-size:18px;">Choose Channel's Banner</label>
								    <div class="file-field input-field">
                                          <div class="btn right red darken-1">
                                            <span>Browse</span>
                                            <input type="file" name = "data[c_banner]" id="c_banner"/>
                                          </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text">
                                        </div> 
                                        <span class = "spacer"></span>                                   
                                    </div>
								</div>
								
								<div class="input-field col s12">
									<a class="waves-effect waves-light btn left red darken-1" href ="javascript:void(0)" id="identityPrevTab">Previous</a>
									<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityNextTab">Next</a>
								</div>
							</div>
						</div>
						<div id="socialConnect" class="col s12 tab-content">
							<div class = "social-container">
								<div class = "facebook-container">
									<h2>Facebook</h2>
									<img src = "<?php echo BASE_URL; ?>/img/facebook.png" class = "left" alt = "" />
									<span class = "left">Connect your facebook page/profile</span>  
									<!--<a class="waves-effect waves-light btn right red darken-1" href = "#">Connect</a>-->
									<input name="facebook_connect_url" id="facebook_connect_url" type="text" class="validate">	
								</div>
								<span class = "spacer"></span>
								<div class = "youtube-container">
									<h2>Youtube</h2>
									<img src = "<?php echo BASE_URL; ?>/img/youtube.png" class = "left" alt = "" />  
									<span class = "left">Connect your youtube Channel</span>
									<!--<a class="waves-effect waves-light btn right red darken-1" href = "#">Connect</a>-->
									<input name="youtube_connect_url" id="youtube_connect_url" type="text" class="validate">									
								</div>
								<span class = "spacer"></span>
								<div class="input-field col s12">
									<a class="waves-effect waves-light btn left red darken-1" href = "javascript:void(0)" id="socialConnectPrevTab">Previous</a>
									<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="socialConnectNextTab">Next</a>
								</div>
							</div>
						</div>
						<div id="legalInfo" class="col s12 tab-content">
						<div class="errorMsgFrm" style="display:none;"></div>
							<div class = "lagel-info-container">                              
								<div class = "youtube-container">
									<h2>Upload Document</h2>                                      
									<div class="file-field input-field">
										<select id = "c_legaldoc_type" name="c_legaldoc_type">
											<option value="">Legal Document Type</option>
											<option value="Treadmark" >Treadmark</option>
											<option value="Licence">Licence</option>
										</select>
										<div class="btn right red darken-1">
											<span>Choose File</span>
											<?php echo $this->Form->input('c_legaldoc', array('type'=>'file','label'=>'','id'=>'c_legaldoc'));?>
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text" style="width:50%;float:right;">
										</div> 
										<span class = "spacer"></span> 
										<div class="input-field col s12">
											<a class="waves-effect waves-light btn left red darken-1" href = "javascript:void(0)" id="legalInfoPrevTab">Previous</a>
											<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="submitForm">Finish</a>
										</div>                                   
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>  
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    //$('select').material_select();
	$("#c_lang").append('<option value="option6">option6</option>');
	$('#tabberDisableBasic').click(function(){ return false});
	$('#tabberDisableIdentity').click(function(){ return false});
	$('#tabberDisableSocialConnect').click(function(){ return false});
	$('#tabberDisableLegalInfo').click(function(){ return false});
	$("#identityTab").click(function(){
	    
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var c_name = $.trim($('#c_name').val());
		if(c_name ==""){
		    $('#c_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		/*if(!nameRegex.test(c_name)){
			$('#c_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}*/
		
		if( $('#country_code').val() == "") {
		    $('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if( $('#c_lang').val() == "") {
		    $('#c_lang').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select language.");
			return false;
		}
		
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 160px;");
		return true;
		
	})
	
	$("#identityPrevTab").click(function(){
		$("#identity").css("display", "none");
		$("#basicinfo").css("display", "block");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableBasic").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 0px;");
	})
	$("#identityNextTab").click(function(){
		
		var c_banner = $('#c_banner').val().split('.').pop().toLowerCase();
		var c_icon = $('#c_icon').val().split('.').pop().toLowerCase();
		
		if(c_icon ==""){
			$('#c_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel icon.");
			return false;
		}
		
		if(c_icon !="" && $.inArray(c_icon, ['png','jpg','jpeg']) == -1){
			$('#c_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel icon as png, jpg, jpeg.");
			return false;
		}
		
		if(c_banner ==""){
			$('#c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel banner.");
			return false;
		}
		
		if(c_banner !="" && $.inArray(c_banner, ['png','jpg','jpeg']) == -1){
			$('#c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel banner as png, jpg, jpeg.");
			return false;
		}
		
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "none");
		$("#socialConnect").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableSocialConnect").addClass("active");
		$(".indicator").attr("style", "right: 161px; left: 320px;");
		return true;
		
	})
	
	$("#socialConnectPrevTab").click(function(){
		$("#basicinfo").css("display", "none");
		$("#socialConnect").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableSocialConnect").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 160px;");
	})
	
	$("#socialConnectNextTab").click(function(){
		$("#basicinfo").css("display", "none");
		$("#socialConnect").css("display", "none");
		$("#identity").css("display", "none");
		$("#legalInfo").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableSocialConnect").removeClass("active");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableLegalInfo").addClass("active");
		$(".indicator").attr("style", "right: 1px; left: 480px;");
	})
	
	$("#legalInfoPrevTab").click(function(){
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "none");
		$("#legalInfo").css("display", "none");
		$("#socialConnect").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableLegalInfo").removeClass("active");
		$("#tabberDisableSocialConnect").addClass("active");
		$(".indicator").attr("style", "right: 161px; left: 320px;");
	})
	
	$("#submitForm").click(function(){
		var c_legaldoc = $('#c_legaldoc').val().split('.').pop().toLowerCase();
		/*if(c_legaldoc ==""){
			$('#c_legaldoc').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel legal document as  pdf, doc.");
			return false;
		}
		if(c_legaldoc !="" && $.inArray(c_legaldoc, ['pdf','doc', 'docx']) == -1){
			$('#c_legaldoc').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload channel legal document as  pdf, doc.");
			return false;
		}
		
		if(c_legaldoc != "" && $('#c_legaldoc_type').val() == "" ){
			$('#c_legaldoc_type').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select legal documement type.");
			return false;
		}*/
		document.addChannel.submit();
	})
	
	//fetch language data onload...
	var countryCode = '';
	var languageCode = '';
	$('select').material_select();
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    var $selectDropdown = 
			  $("#c_lang")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$selectDropdown.trigger('contentChanged');
		}
	});
})
function getLanguage(countryCode){
    var languageCode = '';
	//alert(countryCode);
	$('select').material_select();
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    
			var $selectDropdown = 
			  $("#c_lang")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$selectDropdown.trigger('contentChanged');
		}
	});
	
}
 $('select').on('contentChanged', function() {
    // re-initialize (update)
    $(this).material_select();
  });

</script>

<script type = "text/javascript" >
history.pushState(null, null, 'addchannel');
window.addEventListener('popstate', function(event) {
history.pushState(null, null, 'addchannel');
});
</script>