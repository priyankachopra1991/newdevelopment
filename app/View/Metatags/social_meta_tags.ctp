
<?php if(isset($_GET['subcategory_id']) && !empty($_GET['subcategory_id'])){ ?>
<script type="text/javascript">
$(document).ready(function(){
	var app_slug = 'firstcut';
	var subcategory_id = '<?php echo $_GET['subcategory_id']; ?>';
	alert(subcategory_id);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/viewcontentcategorylist.php',
		data : {'app_slug': app_slug, 'category_id': subcategory_id},
		'success':function(resp){
			$('#subcategory_id').material_select('destroy');
		    $('#subcategory_id').html(resp);
		}
	});
});
</script>
<?php } else { ?>
<script type="text/javascript">
$(document).ready(function () {
	var app_slug = 'firstcut';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			$('#subcategory_id').material_select('destroy');
			$('#subcategory_id').html(resp);
			//$('.subcategory_id').html(resp);
		}
	});
});	
</script>
<?php } ?>
<div class = "row all-channel-container">
	 <?php if(!empty($metatags)){ 
	 ?>
		<div class = "col s12 listView" style="display:none;">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th>Title</th>
						<th>Category Id</th>
						<th>No of type</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($metatags as $obj){ 
					
					
					?>
						<tr>
						    <td><?php echo $obj["t1"]["content_id"]; ?></td>
							<td><?php echo empty($obj["t1"]["name"]) ? 'NA' : $obj["t1"]["name"]; ?></td>
							<td>
							<?php if($obj['t1']['approved'] == 0){ ?>
								Submitted for approval
						   <?php } else if($obj['t1']['approved'] == 1){ ?>
								Approved
						   <?php } else { ?>
							    Rejected
						   <?php } ?>
							</td>
							<td><?php echo $obj['t1']['view']; ?></td>
							<td><a href = "<?php echo BASE_URL."/fcappcontents/detailcontent/".$obj['t1']['content_id']; ?>" class = "btn red lighten-1">View</a>
							</td>

						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView" style="display:none;">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!-list view section end here-->
	</form>
</div>
<?php if((!empty($value)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/fcappcontents/approvedcontent";
	
	if(@$_GET["country_code"] !=""){
		$country_code = @$_GET["country_code"];
	} else {
		$country_code = "";
	}
	if(@$_GET["status"] !=""){
		$status = @$_GET["status"];
	} else {
		$status = "";
	}
	if(@$_GET["channel_id"] !=""){
		$channel_id = @$_GET["channel_id"];
	} else {
		$channel_id = "";
	}
	if(@$_GET["subcategory_id"] !=""){
		$subcategory_id = @$_GET["subcategory_id"];
	} else {
		$subcategory_id = "";
	}
	if(@$_GET["name"] !=""){
		$name = @$_GET["name"];
	} else {
		$name = "";
	}
	
	if(@$_GET["sortBy"] !=""){
		$sortBy = @$_GET["sortBy"];
	} else {
		$sortBy = "";
	}
	
	if(@$_GET["filterBy"] !=""){
		$filterBy = @$_GET["filterBy"];
	} else {
		$filterBy = "";
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&country_code='.@$country_code.'&status='.@$status.'&channel_id='.@$channel_id.'&subcategory_id='.@$subcategory_id.'&name='.@$name.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&country_code='.@$country_code.'&status='.@$status.'&channel_id='.@$channel_id.'&subcategory_id='.@$subcategory_id.'&name='.@$name.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&country_code='.@$country_code.'&status='.@$status.'&channel_id='.@$channel_id.'&subcategory_id='.@$subcategory_id.'&name='.@$name.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    //alert($.cookie("userView"));
	if($.cookie("userView") == "listView"){
		$(".gridView").css("display","none");
		$(".listView").css("display","block");
		$("#listViewModule").addClass("active");
		$("#gridViewModule").removeClass("active");
	} else {
		$(".listView").css("display","none");
		$(".gridView").css("display","block");
		$("#gridViewModule").addClass("active");
		$("#listViewModule").removeClass("active");
	}
	$("#listViewModule").click(function(){
		$.cookie("userView", "listView");
		$(".gridView").css("display","none");
		$(".listView").css("display","block");
		$("#listViewModule").addClass("active");
		$("#gridViewModule").removeClass("active");
	})
	$("#gridViewModule").click(function(){
		$.cookie("userView", "gridView");
		$(".listView").css("display","none");
		$(".gridView").css("display","block");
		$("#gridViewModule").addClass("active");
		$("#listViewModule").removeClass("active");
	})
	
	$("#searchRec").click(function(){
		document.searchContent.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortContent.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortContent.submit();
	
	})
	
})

</script>
