<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchCategory" id="searchCategory" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
					<ul class = "select-list right">                           
							<li>
								<select name="searchBy" id="searchBy">
									<option value="">Search By</option>
									<option value="name" >Name</option>
									<option value="u_id" >Id</option>
								</select>
							</li>
							
							<li>
								<div class="input-field">
									<input placeholder="Search By String" id="searchString" name="searchString" type="text" value="" class="validate">
								</div>
							</li>
							<li>
								<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<!--<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>-->
		</div>
		<form name="sortCategory" id="sortCategory" action="" method="get">
			<div class = "col s6 small-filteration">
				<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="" >Sort By</option>
							<option value="desc"  >Id Desc</option>
							<option value="asc" >Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="1" >Active</option>
						<option value="0" >Inactive</option>
					</select>
					</li>
				</ul>
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    
	<!--List view seection strat here-->
     <?php if(!empty($userdata)){ ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th>User Id</th>
						<th>Name</th>
						<th>Link</th>
						<th>Type</th>
						<th>Status</th>
						<th>Image</th>
						<th>Order</th>
						<th>Edit</th>
					
					</tr>
				</thead>
				<tbody>
					<?php foreach($userdata as $udata){ ?>
						<tr>
							<td><?php echo $udata["cm_promotional_content"]["id"];?></td>
							<td><?php echo $udata["cm_promotional_content"]["name"];?></td>
							<td><?php echo $udata["cm_promotional_content"]["link"];?></td>
							<td><?php 
										
										if($udata["cm_promotional_content"]["type"]==1 || $udata["cm_promotional_content"]["type"]=="1"){
										
											echo "Play Video";
										}
										if($udata["cm_promotional_content"]["type"]==2 || $udata["cm_promotional_content"]["type"]=="2"){
										
											echo "App";
										}
										 if($udata["cm_promotional_content"]["type"]==3 || $udata["cm_promotional_content"]["type"]=="3"){
										
											echo "Web";
										}
										 if($udata["cm_promotional_content"]["type"]==4 || $udata["cm_promotional_content"]["type"]=="4"){
										
											echo "Pro Banner";
										}
										
										
							?></td>
							<td><?php 
							
									
									if($udata["cm_promotional_content"]["status"]==1 || $udata["cm_promotional_content"]["status"]=="1"){
										
											echo "Active";
										}
										else{
											echo "Deactive";
										}
									
							?></td>
							<td><img src="<?php echo AMAZONIMAGEURL.$udata["cm_promotional_content"]["image"]; ?>" style="width:70px; height:50px;" /></td>
							<td><?php echo $udata["cm_promotional_content"]["display_order"]; ?></td>
							<td><a href = "<?php echo BASE_URL."/promotion/edit/".$udata["cm_promotional_content"]["id"]; ?>" class = "btn red lighten-1">Edit</a></td>
							
						
						</tr>
						<?php }?>
					
				</tbody>
			</table>
		</div>
        <?php }else{?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php }?>
	<!--list view section end here-->
</div>
	
	<?php

		$sort="";
		$filter="";
		$pgnum=1;
		if(!empty($_GET['sortBy'])){
			$sort=$_GET['sortBy'];
		}
		if(!empty($_GET['filterBy'])){
			$filter=$_GET['filterBy'];
		}
		if(!empty($_GET['page'])){
			$pgnum=$_GET['page'];
		}
	
		if(!empty($total_num)){
		
		$pageName = BASE_URL."/promotion/view";
		
			$recode_per_page=10;
			
			$num_of_pages=ceil($total_num/$recode_per_page);
			
		?>
		  <ul class="pagination">
		  <?php if($pgnum>1){ ?>
    <li ><a href="<?php echo $pageName?>/?page=<?php echo $pgnum-1; ?>&sortBy=<?php echo $sort; ?>&filterBy=<?php echo $filter;?>"><i class="material-icons">chevron_left</i></a></li>
			<?php 
			}else{
	
			?>
				<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
			<?php } ?>
	<?php for($per_page=1;$per_page<=$num_of_pages;$per_page++){?>
	
	<?php if($per_page==$pgnum){?>
    <li class="active" ><a href="<?php echo $pageName?>/?page=<?php echo $per_page; ?>&sortBy=<?php echo $sort; ?>&filterBy=<?php echo $filter;?>"><?php echo $per_page;?></a></li>
	<?php }else{?>
		<li ><a href="<?php echo $pageName?>/?page=<?php echo $per_page; ?>&sortBy=<?php echo $sort; ?>&filterBy=<?php echo $filter;?>"><?php echo $per_page;?></a></li>
		<?php }?>
	<?php }?>
    <li class="waves-effect"><a href="<?php echo $pageName?>/?page=<?php echo $per_page-1; ?>&sortBy=<?php echo $sort; ?>&filterBy=<?php echo $filter;?>"><i class="material-icons">chevron_right</i></a></li>
  </ul>
  
  <?php }?>
	
	
	

<?php echo $this->element('flashmessage'); ?>

<script type="text/javascript">
$(document).ready(function(){

	$("#searchRec").click(function(){
		document.searchCategory.submit();
	
	})
    
	$("#sortBy").change(function(){
		document.sortCategory.submit();
	
	});
	$("#filterBy").change(function(){
		document.sortCategory.submit();
	
	});
	
})


</script>

