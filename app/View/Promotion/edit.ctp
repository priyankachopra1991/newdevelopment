<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" name="edituser" id="edituser" method="post" action="<?php echo BASE_URL."/promotion/edit/". $proData["Promotion"]["id"]; ?>" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit Promotion</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s8">
								<input id="id" name="id" value="<?php echo $proData["Promotion"]["id"];?>" type="hidden" class="validate">
								<div class = "row">
									<div class="input-field col s12">
										<input placeholder="User Name"  id="name" name="name"  value="<?php echo $proData["Promotion"]["name"];?>" type="text" class="validate">
									</div>
								<div class="input-field col s12">
									<input placeholder="Link" id="url" name="url_link" value="<?php echo $proData["Promotion"]["link"];?>"  type="text" class="validate">
								</div>
								
								<div class="input-field col s12">
									<input id="display_order" name="display_order" type="text"  value="<?php echo $proData["Promotion"]["display_order"];?>" class="validate" onkeypress="return isNumber(event)">
											<label for="display_order">Order</label>	
								</div> 
								<div class="input-field col s12">
								<label for="name">Status</label>
								<br/>
								<br/>
									<input type="radio" name="status" id="optionsRadiosInline1" value="1" <?php echo (($proData["Promotion"]["status"] == "1") ? "checked" : ""); ?> class="space">Active&nbsp;&nbsp;
														
									<input type="radio" name="status" id="optionsRadiosInline2" value="0" <?php echo (($proData["Promotion"]["status"] == "0") ? "checked" : ""); ?> class="space">Deactive
								</div>
								
								<div class="input-field col s12">	
									<label for="name">Type</label>
									<br/>
									<br/>
									<input type="radio" name="type" id="optionsRadiosInline3" value="1" class="space"  checked="checked"  <?php echo (($proData["Promotion"]["type"] == "1") ? "checked" : ""); ?>>Play Video&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline4" value="2" class="space" <?php echo (($proData["Promotion"]["type"] == "2") ? "checked" : ""); ?>>In-App&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline5" value="3" class="space" <?php echo (($proData["Promotion"]["type"] == "3") ? "checked" : ""); ?>>Out-App&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline6" value="4" class="space" <?php echo (($proData["Promotion"]["type"] == "4") ? "checked" : ""); ?>>Pro Banner&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline7" value="5" class="space" <?php echo (($proData["Promotion"]["type"] == "5") ? "checked" : ""); ?>>View-Pack
									
								</div>
								
							</div>                    
						</div>
						<div class = "col s4">
							<div class = "row"><div class="input-field series-icon file_icon col s12">          
							<label for="channel">Promotional Image</label>                   
								<i class = "material-icons">perm_media</i>
								
									<?php echo $this->Form->input('imgfile', array('type'=>'file','label'=>'','id'=>'imgfile'));?>	
									<img src="<?php echo AMAZONIMAGEURL.$proData["Promotion"]["image"]; ?>" height="100" width="100" />
									 <?php
									echo $this->Form->input('hiddenimage2',array('type'=>'hidden','value'=>$proData["Promotion"]["image"]));
								?>
									
							</div>
					 </form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>

<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $(".errorMsgFrm").css("display", "none");
	    $("#edituser")[0].reset();
	})
	$("#submitForm").click(function(){
		var imgfile = $('#imgfile').val().split('.').pop().toLowerCase();
		
		if( $('#name').val() == "") {
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		/*if(imgfile ==""){
			$('#imgfile').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload promotional banner.");
			return false;
		}*/
		
		if(imgfile !="" && $.inArray(imgfile, ['png','jpg','jpeg']) == -1){
			$('#imgfile').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload promotional banner as png, jpg, jpeg.");
			return false;
		}
		document.edituser.submit();
	})
})
</script>	
<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>		
		