<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class="edit-form" method="post" action="<?php echo BASE_URL; ?>/promotion/add" id="add" name="add" enctype="multipart/form-data" method="post">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Promotion</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn red darken-1" id="submitForm" >Submit</a>
									<!--<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>-->
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
							
								<div class = "row">
									<div class="input-field col s12">
									
									<input id="name" name="name" type="text" class="validate">
									<label for="icon_prefix">Title</label>
								</div>
								<div class="input-field col s12">
									<input id="url" name="url_link" type="text" class="validate">
											<label for="first_name">Link</label>	
								</div> 
								<div class="input-field col s12">
									<input id="display_order" name="display_order" type="text" class="validate" onkeypress="return isNumber(event)">
											<label for="display_order">Order</label>	
								</div> 
								<div class="input-field col s12">
								<label for="name">Status</label>
								<br/>
								<br/>
									<input type="radio" name="status" id="optionsRadiosInline1" value="1"  class="space"  checked="checked">Active&nbsp;&nbsp;
														
									<input type="radio" name="status" id="optionsRadiosInline2" value="0"  class="space">Deactive
								</div>
								<div class="input-field col s12">	
									<label for="name">Type</label>
									<br/>
									<br/>
									<input type="radio" name="type" id="optionsRadiosInline3" value="1" class="space"  checked="checked">Play Video&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline4" value="2" class="space">In-App&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline5" value="3" class="space">Out-App&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline6" value="4" class="space">Pro Banner&nbsp;&nbsp;
									<input type="radio" name="type" id="optionsRadiosInline7" value="5" class="space">View-Pack
									
								</div>	
									
								</div> 
									</div>                              
									
									<div>
									<!--<a href="">Download Format</a>-->
									</div>
								</div>                    
							</div>
							<div class = "col s6">
							<div class="input-field series-file file_icon col s12">
										<label for="channel">Choose File</label>
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('imgfile', array('type'=>'file','label'=>'','id'=>'imgfile'));?>								
									</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>

<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $(".errorMsgFrm").css("display", "none");
	    $("#add")[0].reset();
	})
	$("#submitForm").click(function(){
		var imgfile = $('#imgfile').val().split('.').pop().toLowerCase();
		
		if( $('#name').val() == "") {
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		if(imgfile ==""){
			$('#imgfile').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload promotional banner.");
			return false;
		}
		
		if(imgfile !="" && $.inArray(imgfile, ['png','jpg','jpeg']) == -1){
			$('#imgfile').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload promotional banner as png, jpg, jpeg.");
			return false;
		}
		document.add.submit();
	})
})
</script>	

<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>		
		