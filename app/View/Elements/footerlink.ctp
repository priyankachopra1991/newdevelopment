<footer class="page-footer">
	<div class="footer-copyright">          
		<nav>             
		  <div class="nav-wrapper">                
			<ul id="nav-mobile" class="left hide-on-med-and-down">                  
			  <li><a href="#">About FirstCut</a></li>
			  <li><a href="#">Contact Us</a></li>
			</ul>
			<ul id="nav-mobile" class="right hide-on-med-and-down">                  
			  <li>&copy <?php echo date("Y"); ?></li>
			  <li><a href="#"><b>FirstCut</b></a></li>
			  <li>Shaping Your Watching Experience</li>
			</ul>
		  </div>          
		</nav>
	</div>          
</footer>