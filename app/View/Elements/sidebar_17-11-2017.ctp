<div class = "sidebar-container">
	<ul id="slide-out" class="side-nav fixed red darken-1">
		<li>
			<div class = "logo-sidebar">
			<a href = "<?php echo BASE_URL; ?>/users/dashboard" class = "logo-text">FirstCut</a>
			<img class = "fc-icon" src = "<?php echo BASE_URL; ?>/img/logo.png" alt = "" />
			</div>
		</li>
		<li>
			<ul class="collapsible collapsible-accordion">
				<li>
					<a class="collapsible-header user-profile">
						<i class = "material-icons large profile">person</i>
						<?php echo $this->Session->read('User.name'); ?>
						<i class="material-icons pull-right">arrow_drop_down</i>
					</a>
					<div class="collapsible-body child-list">
						<ul>
							<li>
								<a href="<?php echo BASE_URL."/users/myprofile/".$this->Session->read('User.id'); ?>">
								<i class = "material-icons small profile"></i> My Profile
								</a>
							</li>                                                   <li>
								<a href="<?php echo BASE_URL."/users/logout"; ?>">
								<i class = "material-icons small profile"></i> Logout
								</a>
							</li>                  
						</ul>
					</div>
				</li>
			</ul>
		</li>
		<li class = "dashboard">
			<a class="collapsible-header user-profile" href = "<?php echo BASE_URL; ?>/users/dashboard">
			<i class = "material-icons large profile">dashboard</i>
			Dashboard                 
			</a>
		</li>
		<li>
			<ul class="collapsible collapsible-accordion">
		<li>
		<a class="collapsible-header user-profile">
			<i class = "material-icons large profile">assignment</i>
			Reports
			<i class="material-icons pull-right">arrow_drop_down</i>
		</a>
		<div class="collapsible-body child-list">
			<ul>
			    <li>
					<a href="<?php echo BASE_URL; ?>/reports/contentusedreport">
					<i class = "material-icons profile"></i> Content Used Report
					</a>
				</li>
				<li>
					<a href="<?php echo BASE_URL; ?>/reports/dailyusedreport">
					<i class = "material-icons profile"></i> Daily Revenue Report
					</a>
				</li>
			
				<li>
					<a href="daily-report.html">
					<i class = "material-icons profile"></i> Daily Report
					</a>
				</li>
				<li>
					<a href="daily-user-report.html">
						<i class = "material-icons profile"></i> Daily User Report
					</a>
				</li>
				<li>
					<a href="hourly-report.html">
					<i class = "material-icons profile"></i> Hourly Report
					</a>
				</li>   
				<li>
					<a href="content-usage-report.html">
					<i class = "material-icons profile"></i> Content Usage Report
					</a>
				</li>
				<li>
					<a href="tata-revenue-report.html">
					<i class = "material-icons profile"></i> Tata Revenue Report
					</a>
				</li>
				<li>
					<a href="vodafone-revenue-report.html">
					<i class = "material-icons profile"></i> Vodafone Revenue Report
					</a>
				</li>   
				<li>
					<a href="idea-revenue-report.html">
					<i class = "material-icons profile"></i> Idea Revenue Report
					</a>
				</li>
				<li>
					<a href="airtel-revenue-report.html">
					<i class = "material-icons profile"></i> Airtel Revenue Report
					</a>
				</li>
				<li>
					<a href="download-report.html">
					<i class = "material-icons profile"></i> Download Report
					</a>
				</li>                                            
			</ul>
		</div>
	</ul>
	             
	<li>
		<ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header user-profile">
					<i class = "material-icons large profile">library_books</i>
					Content Management
					<i class="material-icons pull-right">arrow_drop_down</i>
				</a>
				<div class="collapsible-body child-list">
					<ul>
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/viewcontent">
							<i class = "material-icons small profile"></i> Content View
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/bulkupload">
							<i class = "material-icons small profile"></i> Bulk Upload
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/direct_upload_to_bucket">
							<i class = "material-icons small profile"></i> Direct Image Upload
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/direct_upload_to_bucket_video">
							<i class = "material-icons small profile"></i> Direct Video Upload
							</a>
						</li>						
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/approvedcontent"> 
							<i class = "material-icons small profile"></i> Approved 
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/rejectedcontent"> 
							<i class = "material-icons small profile"></i> Rejected
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/bulk_upload_without_content">
							<i class = "material-icons small profile"></i> Bulk Upload Without Content
							</a>
						</li> 
						<li>
							<a href="<?php echo BASE_URL; ?>/fcappcontents/addyoutube">
							<i class = "material-icons small profile"></i> Add Youtube Content
							</a>
						</li> 						
					</ul>
				</div>
			</li>
		</ul>
	</li>  
	<li>
		<ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header user-profile">
				<i class = "material-icons large profile">live_tv</i>
				User Management 
				<i class="material-icons pull-right">arrow_drop_down</i>
				</a>
				<div class="collapsible-body child-list">
					<ul>
						<li>
							<a href="<?php echo BASE_URL; ?>/users/adduser">
							<i class = "material-icons small profile"></i> Add User 
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/users/viewuser">
							<i class = "material-icons small profile"></i> View User 
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/users/changepassword">
							<i class = "material-icons small profile"></i> Change Password 
							</a>
						</li>                                  
					</ul>
				</div>
			</li>
		</ul>
	</li>
	<li>
		<ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header user-profile">
				<i class = "material-icons large profile">live_tv</i>
				Channel Management
				<i class="material-icons pull-right">arrow_drop_down</i>
				</a>
				<div class="collapsible-body child-list">
					<ul>
						<li>
							<a href="<?php echo BASE_URL; ?>/channels/addchannel">
							<i class = "material-icons small profile"></i> Add Channel 
							</a>
						</li> 
						<li>
							<a href="<?php echo BASE_URL; ?>/channels/viewchannel">
							<i class = "material-icons small profile"></i> View Channel 
							</a>
						</li>                                          
						
					</ul>
				</div>
			</li>
		</ul>
	</li>
	<li>
		<ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header user-profile">
					<i class = "material-icons large profile">live_tv</i>
					Series 
					<i class="material-icons pull-right">arrow_drop_down</i>
				</a>
				<div class="collapsible-body child-list">
					<ul>
						<li>
							<a href="<?php echo BASE_URL; ?>/series/addseries">
							<i class = "material-icons small profile"></i> Add Series 
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/series/viewseries">
							<i class = "material-icons small profile"></i> View Series 
							</a>
						</li>    
						<li>
							<a href="<?php echo BASE_URL; ?>/series/addcontentseries">
							<i class = "material-icons small profile"></i> Add Content 
							</a>
						</li>                             
					</ul>
				</div>
			</li>
		</ul>
	</li>  
	<li>
		<ul class="collapsible collapsible-accordion">
			<li>
				<a class="collapsible-header user-profile">
					<i class = "material-icons large profile">live_tv</i>
					Artist 
					<i class="material-icons pull-right">arrow_drop_down</i>
				</a>
				<div class="collapsible-body child-list">
					<ul>
						<li>
							<a href="<?php echo BASE_URL; ?>/artist/addartist">
							<i class = "material-icons small profile"></i> Add Artist 
							</a>
						</li>
						<li>
							<a href="<?php echo BASE_URL; ?>/artist/viewartist">
							<i class = "material-icons small profile"></i> View Artist 
							</a>
						</li>       
						<li>
							<a href="<?php echo BASE_URL; ?>/artist/addcontentartist">
							<i class = "material-icons small profile"></i> Add Content 
							</a>
						</li>                           
					</ul>
				</div>
			</li>
		</ul>
	</li>  
	<li class = "dashboard">
		<a class="collapsible-header user-profile" href = "add-category.html">
		<i class = "material-icons large profile">add </i>
		Add Catogory                 
		</a>
	</li>

	</ul>
	<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
</div>
<!-- end of sidebar container -->