<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<?php echo $this->Html->css("materialize.min.css", array("media"=>"screen,projection")); ?>
	<?php echo $this->Html->css("style.css", array("media"=>"screen,projection")); ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<?php echo $this->Html->css('jquery-ui.css'); ?>
	
	
    <?php echo $this->Html->script("Chart.min.js"); ?>
	
	<?php echo $this->Html->script("jquery-3.2.1.min.js"); ?>
	<?php echo $this->Html->script('jquery-ui.js'); ?>
	<?php echo $this->Html->script("jquery.cookie.js"); ?>
	<?php echo $this->Html->css("custom.css"); ?>