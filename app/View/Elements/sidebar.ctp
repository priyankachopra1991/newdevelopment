<?php $allNavigation = $this->requestAction('/users/sidebar/');
?>
<div class = "sidebar-container">
	<ul id="slide-out" class="side-nav fixed red darken-1" >
		<li>
			<div class = "logo-sidebar">
			<a href = "<?php echo BASE_URL; ?>/users/dashboard" class = "logo-text">FirstCut</a>
			<img class = "fc-icon" src = "<?php echo BASE_URL; ?>/img/logo.png" alt = "" />
			</div>
		</li>

		<li>
			<ul class="collapsible collapsible-accordion" >
				<li>
					<a class="collapsible-header user-profile">
						<i class = "material-icons large profile">person</i>
						<?php echo substr($this->Session->read('User.name'), 0,20); ?>
						<i class="material-icons pull-right">arrow_drop_down</i>
					</a>
					<div class="collapsible-body child-list">
						<ul>
							<li>
								<a href="<?php echo BASE_URL."/users/myprofile/".$this->Session->read('User.id'); ?>">
								<i class = "material-icons small profile"></i> My Profile
								</a>
							</li><li>
								<a href="<?php echo BASE_URL."/users/changepassword/".$this->Session->read('User.id'); ?>">
								<i class = "material-icons small profile"></i> Change Password
								</a>
							</li>                                                   <li>
								<a href="<?php echo BASE_URL."/users/logout"; ?>">
								<i class = "material-icons small profile"></i> Logout
								</a>
							</li>                  
						</ul>
					</div>
				</li>
			</ul>
		</li>
		
		<?php foreach($allNavigation as $key=>$val){ ?>
		
		<li>
			<ul class="collapsible collapsible-accordion">
				<li>
					<a class="collapsible-header user-profile" href="<?php if(($val["parent"]["controller"] !="") && ($val["parent"]["action"] !="")) { echo BASE_URL.'/'.$val["parent"]["controller"].'/'.$val["parent"]["action"]; } else { ?>javascript:void(0)<?php } ?>">
						<i class = "<?php echo $val["parent"]["class"]; ?>"><?php echo $val["parent"]["menu_icon_name"]; ?></i>
						<?php echo $val["parent"]["menu_name"]; ?>
						<?php if(!empty($val["firstchild"])){ ?>
						<i class="material-icons pull-right">arrow_drop_down</i>
						<?php } ?>
					</a>
					<div class="collapsible-body child-list">
						<ul>
						<?php if(!empty($val["firstchild"])){ 
							foreach($val["firstchild"] as $key1=>$val1){
							?>
							<li>
								<a href="<?php if(($val1["controller"] !="") && ($val1["action"] !="")) { echo BASE_URL.'/'.$val1['controller'].'/'.$val1['action']; } ?>" ondblclick="return false;">
								<i class = "material-icons small profile"></i> <?php echo $val1["menu_name"]; ?>
								</a>
							</li>
							<?php } ?>	
							<?php } ?>								
						</ul>
					</div>
				</li>
			</ul>
		</li>
		
		<?php } ?>
		
		
	</ul>
	<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
</div>
<!-- end of sidebar container -->