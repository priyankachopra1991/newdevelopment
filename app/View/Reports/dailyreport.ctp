
<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> assignment </i>
                        </div>
                        <div class = "card-content">
                            <h3>Daily Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9">
			    <form name="dailyReport" id="dailyReport" method="get" action="<?php echo BASE_URL; ?>/reports/dailyreport">
                  <ul class = "select-list right">
                      <li>
                       <input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>" />
                       <!-- <i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                        <select name="biller_id" id="biller_id">
                            
                            <?php foreach($operator as $operatorRec){ ?>
								<option value="<?php echo $operatorRec["Operator"]["slug"]; ?>" <?php if((@$_POST['biller_id'] == $operatorRec["Operator"]["slug"]) ||(@$_GET['biller_id'] == $operatorRec["Operator"]["slug"])){ echo "selected"; } ?>><?php echo $operatorRec["Operator"]["name"] ?></option>
							<?php } ?>
                        </select>
                      </li>
                      <li>
                        <select name="publisher" id="publisher">
							<!--<option value="">Select</option>-->
							<?php foreach($project as $projectRec){ ?>
								<option value="<?php echo $projectRec["Project"]["slug"]; ?>" <?php if((@$_POST['publisher'] == $projectRec["Project"]["slug"]) ||(@$_GET['publisher'] == $projectRec["Project"]["slug"])){ echo "selected"; } ?>><?php echo $projectRec["Project"]["name"] ?></option>
							<?php } ?>
                        </select>
                      </li>
                      <li>
                        <a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
                        <?php if((!isset($_GET["startDate"])) && (!empty($resArr))){ ?>
							<a href="<?php echo BASE_URL; ?>/reports/dailyreportdetail?startDate=<?php echo date('Y-m-d',strtotime('yesterday')); ?>&biller_id=<?php echo "vf"; ?>&publisher=<?php echo "bnama"; ?> " class="waves-effect waves-light btn red darken-1">Get Details</a>
						<?php } else if(!empty($resArr)){ ?>
							<a href="<?php echo BASE_URL; ?>/reports/dailyreportdetail?startDate=<?php echo $_GET["startDate"];?>&biller_id=<?php echo $_GET["biller_id"]; ?>&publisher=<?php echo $_GET["publisher"]; ?> " class="waves-effect waves-light btn red darken-1">Get Details</a>
						<?php } ?>
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <div class = "row">
                  <div class = "col s6">
                      <!--<ul class = "view-in-listing">
                        <li>
                          <a href="daily-report.html">
                            <i class = "material-icons grey-text">format_list_bulleted</i>
                          </a>
                        </li>
                        <li>
                          <a href="view-graph.html" class = "active">
                            <i class = "material-icons grey-text">view_module</i>
                          </a>
                        </li>                       
                      </ul>-->
                  </div>
                  <div class = "col s6 small-filteration">
                      <!--<ul class = "right">                       
                        <li>                                          
                          <select>
                            <option value="" disabled selected>Sort By</option>
                            <option value="1">View</option>
                            <option value="2">Performance</option>
                            <option value="3">Option 3</option>
                          </select> 
                        </li>
                        <li>                                         
                          <select>
                            <option value="" disabled selected>Filter By</option>
                            <option value="1">Submitted For Approval</option>
                            <option value="2">Rejected</option>
                            <option value="3">Option 3</option>
                          </select>
                        </li>
                      </ul>-->
                  </div>
              </div>
              
              <table class = "bordered centered">                   
                  <thead>                    
                    
                    <?php if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons"))){ ?>
					<tr class ="red lighten-5">
						<th>Total Hits</th>
						<th>Mobile Not Found</th>
						<th>CG OK</th>
						<th>New Activation Count</th>
						<th>New Activation Revenue</th>	
						<th>CallBack Count</th>
						<th>Total Activation Count</th>
						<th>Total Activation Revenue</th>
						<th>Renewal Count</th>
						<th>Renewal Revenue</th>
						<th>Total Revenue </th>
						<th>Deactivate</th>
					</tr>
				<?php } else { ?>
					<tr>
						<th>Total Hits</th>
						<th>CG OK</th>
						<th>New Activation Count</th>
						<th>New Activation Revenue</th>	
						<th>CallBack Count</th>
						<th>Total Activation Count</th>
						<th>Total Activation Revenue</th>
						<th>Renewal Count</th>
						<th>Renewal Revenue</th>
						<th>Total Revenue </th>
						<th>Parking</th>
						<th>Deactivate</th>
					</tr>
				<?php } ?>
                    </tr>
                  </thead>

                  <tbody>
                    <?php if(empty($resArr)){ ?>
					<tr>
						<td colspan="14" class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				<?php } else {?>
					<?php if((($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="pretty")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mtoons")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="tt") && ($_GET['publisher'] =="filmywap"))){ ?>
								<?php  
					 $totalHits=0;
					 $mobileNotfound=0;
					 $Cgok=0;
					 $Billing_done=0;
					 $Total_Revenue=0;
					 $CallbackStatus=0;
					 $TotalSubscribeUser=0;
					 $TotalSubscribeRevenue=0;
					 $TotalRenewalCount=0;
					 $TotalRenewalRevenue=0;
					 $TotalRenewalRevenue2=0;
					 $unsubscribeDetail=0;
					 
					 foreach($resArr as $key=>$val){
					 
					
					 
					 ?>     
					       
					<td class="data-right"><?php if(isset($val['total_hit'])){echo $val['total_hit']; $totalHits +=$val['total_hit']; } else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['mobile_notfound'])){echo $val['mobile_notfound']; $mobileNotfound +=$val['mobile_notfound'];} else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['cgok'])){echo $val['cgok']; $Cgok +=$val['cgok'];} else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['billing_done'])){echo $val['billing_done']; $Billing_done+=$val['billing_done']; } else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['total_revenue'])){echo round($val['total_revenue'],2);  $Total_Revenue+=$val['total_revenue'];} else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['callback_status'])){echo $val['callback_status']; $CallbackStatus+=$val['callback_status'];} else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['total_subscribe_user'])){ echo $val['total_subscribe_user']; $TotalSubscribeUser+=$val['total_subscribe_user'];} else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['total_subscribe_revenue'])){echo round($val['total_subscribe_revenue'],2); $TotalSubscribeRevenue+=$val['total_subscribe_revenue']; } else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['total_renewal_count'])){echo $val['total_renewal_count']; $TotalRenewalCount+=$val['total_renewal_count']; } else echo 0;  ?></td>
					<td class="data-right"><?php if(isset($val['total_renewal_revenue'])){echo round($val['total_renewal_revenue'],2); $TotalRenewalRevenue+=$val['total_renewal_revenue']; } else echo 0;  ?></td>
				   <td class="data-right"><?php if(isset($val['total_renewal_revenue'])){ echo round(@$val['total_subscribe_revenue']+$val['total_renewal_revenue'],2); $TotalRenewalRevenue2+=(@$val['total_subscribe_revenue']+$val['total_renewal_revenue']); } else {echo 0 ; }  ?></td>

					<td class="data-right"><?php if(isset($val['unsubscribe_detail'])){echo $val['unsubscribe_detail']; $unsubscribeDetail+=$val['unsubscribe_detail'];} else echo 0;  ?></td>
				
				</tr>
				<?php } ?>
					<?php } else if((($_GET['biller_id'] =="at") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="filmywap")) || (($_GET['biller_id'] =="at") && ($_GET['publisher'] =="atcomics"))){ ?>
					<?php foreach($resArr as $date=>$res){ ?>
					
						<tr>
						
						<td class="data-right"><?php if(isset($res['total_hit'])){echo $res['total_hit'];} else echo 0;  ?></td>
						<td class="data-right"><?php if($_GET['biller_id']=='ac' || $_GET['biller_id']=='at' || $_GET['biller_id']=='vf')
						{
								if(isset($res['cg_ok'])){echo $res['cg_ok'];} else echo 0; 
						} else{  echo '-';} ?></td>
						<td class="data-right"><?php if(isset($res['billing_done'])){echo $res['billing_done'];} else echo 0; ?></td>
						<td class="data-right"><?php if(isset($res['total_revenue'])){echo $res['total_revenue'];} else echo 0; ?></td>		
						<td class="data-right"><?php if(isset($res['callback_status'])){echo $res['callback_status'];} else echo 0; ?></td>
							<?php if($_GET['biller_id']=='ac' || $_GET['biller_id']=='at' || $_GET['biller_id']=='rl')
							{	 ?>
						  <td class="data-right"><?php if(isset($res['count_revenue_ac'])){echo $res['count_revenue_ac'];}else echo 0;?></td>
						  <td class="data-right"><?php if(isset($res['total_revenue_ac'])){echo $res['total_revenue_ac'];}else echo 0;?></td>
						  <td class="data-right"><?php if(isset($res['count_renewal_ac'])){ echo $res['count_renewal_ac'];}else echo 0;?></td>
						  <td class="data-right"><?php if(isset($res['total_renewal_ac'])){ echo $res['total_renewal_ac'];}else echo 0;?></td>
						  <td class="data-right"><?php echo $res['total_revenue_ac']+$res['total_renewal_ac']; ?></td>
						  <td class="data-right"><?php if(isset($res['count_sub2'])){ echo $res['count_sub2'];}else echo 0; ?></td>
						  <td class="data-right"><?php if(isset($res['count_unsub'])){ echo $res['count_unsub'];}else echo 0;  ?></td>
							<?php  } else {?>
							 <td class="data-right"><?php if(isset($res['count_revenue'])){echo $res['count_revenue'];}else echo 0;?></td>
							 <td class="data-right"><?php if(isset($res['callback_revenue'])){echo $res['callback_revenue'];}else echo 0;?></td>
							 <td class="data-right"><?php if(isset($res['Recount'])){ echo $res['Recount'];}else echo 0;?></td>
							<td class="data-right"><?php if(isset($res['renewalTotal_revenue'])){ echo $res['renewalTotal_revenue'];}else echo 0;?></td>
							<td class="data-right"><?php if(isset($res['callback_revenue'])){echo $res['callback_revenue']+$res['renewalTotal_revenue'];}else echo 0; ?></td>
							<td class="data-right"><?php if(isset($res['parking'])){ echo $res['parking'];}else echo 0; ?></td>
							<td class="data-right"><?php if(isset($res['deactivate'])){ echo $res['deactivate'];}else echo 0;  ?></td>
						<?php } ?>
						</tr>
					<?php } ?>	
					<?php } else if((($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="bnama")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="mini")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmywap")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="filmy")) || (($_GET['biller_id'] =="ia") && ($_GET['publisher'] =="miniwap")) || (($_GET['biller_id'] =="vf") && ($_GET['publisher'] =="vfcomics"))){ ?>
					<?php foreach($resArr as $date=>$res){ ?>
						<tr>
							<td class="data-right"><?php if(isset($res['total_hit'])){ echo $res['total_hit']; } else { echo "0"; } ?></td>
							<td class="data-right"><?php if(isset($res['cg_ok'])){ echo $res['cg_ok']; } else { echo "0"; } ?></td>
							<td class="data-right"><?php if(isset($res['billing_done'])){ echo $res['billing_done']; } else { echo "0"; } ?></td>
							<td class="data-right"><?php if(isset($res['total_revenue'])){ echo $res['total_revenue']; } else { echo "0"; } ?></td>
							<td class="data-right"><?php if(isset($res['callback_status'])){ echo $res['callback_status']; } else { echo "0" ; } ?></td>	
							
								<td class="data-right"><?php if(isset($res['count_revenue'])){ echo $res['count_revenue']; } else { echo "0"; } ?></td>
								<td class="data-right"><?php if(isset($res['callback_revenue'])){ echo $res['callback_revenue']; }else { echo "0" ; }?></td>
								<td class="data-right"><?php if(isset($res['Recount'])){ echo $res['Recount']; } else { echo "0"; }?></td>
								<td class="data-right"><?php if(isset($res['renewalTotal_revenue'])){ echo $res['renewalTotal_revenue']; } else { echo "0"; } ?></td>
								<td class="data-right"><?php if(isset($res['callback_revenue'])){ echo $res['callback_revenue']+$res['renewalTotal_revenue']; } else { echo "0"; } ?></td>
								<td class="data-right"><?php if(isset($res['parking'])){ echo $res['parking']; } else { echo "0"; } ?></td>
								<td class="data-right"><?php if(isset($res['deactivate'])){ echo $res['deactivate']; } else { echo "0"; }  ?></td>
						
						</tr>
						<?php } ?>
					<?php } ?>
				<?php } ?>                    
                  </tbody>
              </table>
               
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.dailyReport.submit();
	})
})
</script>