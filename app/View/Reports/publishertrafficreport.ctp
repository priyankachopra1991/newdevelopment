<script>
	$(function() {
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		$("#datepicker1").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
	});
</script>

<div class = "table-format-data white-bg">
	<div class = "row">
		<div class = "col s3">
			<div class = "detail-title-section">
				<div class = "card card-stats">
					<div class = "card-header" data-background-color = "red">
						<i class = "material-icons"> data_usage </i>
					</div>
					<div class = "card-content">
						<h3>Publisher Purchase Report</h3>
					</div>                        
				</div>
			</div>
		</div>
		<div class = "col s9 right">
		<form name="contentReport" id="contentReport" method="get">
		
			<ul class = "select-list right">
			<div class="errorMsgFrm" style="display:none;"></div>
				<li>
					<!--<input placeholder = "From: " type="date" class="datepicker" place-holder = "dd/mm/yyyy">-->
					
					<input type="text" place-holder="Start Date" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>" />                   
				</li>
				<li>
					<!--<input placeholder = "To: " type="date" class="datepicker" place-holder = "dd/mm/yyyy">-->
					
					<input type="text" id="datepicker1" name="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>"  />
				</li> 
				<li>
					<a class="waves-effect waves-light btn red darken-1" id="searchRecord">Search</a>
                </li>				
				<li>
				<?php 
					if(!empty($_GET["startDate"]) && !empty($_GET["endDate"]))
					{ 
						$dwnDate = $_GET["startDate"]; 
						$endDate = $_GET["endDate"];
					} else { 
						$dwnDate = date('Y-m-d', strtotime("-1 day"));
						$endDate = date('Y-m-d', strtotime("-1 day"));						
					}
				?>
					
				</li>
			</ul>
			</form>
		</div>
	</div>
	<?php if(!empty($contentData)){ ?>
	<table class = "bordered centered striped">                   
		<thead>                    
			<tr class = "red lighten-5">
				<th>Content Id</th>
				<th>Content Name</th>
				<th>Country</th>
				<th>Channel Name</th>
				<th>Date</th>
				<th>Number Of Users</th>
				<th>Revenue</th>                        
			</tr>
		</thead>
		<tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td colspan="2"><b>Total</b></td>
			<td id="totRevenueBack"></td>
		</tr>
		<?php
			$totalRevenue = "";
			//$this->InstallTracking = ClassRegistry::init('InstallTracking');
			//$this->User = ClassRegistry::init('User');
			foreach($contentData as $key=>$val){ 
			$contentPriceVal = $this->User->query("select content_pricing from cm_users where id='".$val['t1']['cpId']."'");

			$channelName = $this->Fcappcontent->query("select c_name from ch_channel where id='".$val['t2']['channel_id']."'");
			
		?>
			<tr>
				<td><?php echo $val['t1']['content_id']; ?></td>
				<td><?php echo $val['t2']['contentName']; ?></td>
				<td><?php 
						if($val['t2']['country'] == 'ID')
						{
							echo "Indonesia"; 
						}
						else
						{
							echo "India"; 
						}
						?>
				</td>
				<td><?php echo $channelName[0]['ch_channel']['c_name'] ?></td>
				<td><?php echo date('Y-m-d', strtotime($val['t1']['created'])); ?></td>
				<td><?php echo $val[0]['countRec']; ?></td>
				<td><?php $contentWiseRevenu = ($val[0]['countRec']*$contentPriceVal[0]['cm_users']['content_pricing']); ?>
						<?php echo number_format($contentWiseRevenu, 2); ?></td>
				                     
			</tr>
		<?php $totalRevenue = $totalRevenue + $contentWiseRevenu; ?>
		<?php } ?>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td colspan="2"><b>Total</b></td>
			<td><div id="totRevenue"><?php echo number_format($totalRevenue, 2); ?></div></td>
		</tr>
		</tbody>
	</table>
	<?php } else { ?>
		   <label class = "no-record red-text"><i class = "material-icons red-text">error</i><span> No Record Found </span></label>
	<?php } ?>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var totRevenue = $("#totRevenue").html();
	//alert(totRevenue);
	 $("#totRevenueBack").html(totRevenue);
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#searchRecord").click(function(){
		var datepicker = $.trim($('#datepicker').val());
		var datepicker1 = $.trim($('#datepicker1').val());
		
		if(datepicker ==""){
		    $('#datepicker').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the from date.");
			return false;
		}
		
		if(datepicker1 ==""){
		    $('#datepicker1').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the to date.");
			return false;
		}
		document.contentReport.submit();
	})
})
</script>