
<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		$("#datepicker1").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
	});

</script>
<?php //echo "<pre>";
//print_r($tatareport);
$operator = 'tata';
?>
<div class = "table-format-data white-bg">
              <div class = "row">
			  <form class="form-inline" name="tatarevenueReport" id="tatarevenueReport" role="form" method="get" action="<?php echo BASE_URL; ?>/reports/tatarevenuereport">
                <div class = "col s4">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> attach_money </i>
                        </div>
                        <div class = "card-content">
                            <h3>Tata Revenue Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s8">
                  <ul class = "select-list right">
                      <li>
                       
					   <input placeholder = "From: " type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>"  />
                       <!--<i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                       
					   <input placeholder = "To: " type="text" id="datepicker1" name="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>">
                       <!-- <i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                        <select name="user_id" id="user_id">
							<option value="all">All</option>
							<?php foreach($userRec as $userRecs){ ?>
								<option value="<?php echo $userRecs["User"]["id"]; ?>" <?php if((@$_POST['user_id'] == $userRecs["User"]["id"]) ||(@$_GET['user_id'] == $userRecs["User"]["id"])){ echo "selected"; } ?>><?php echo ucfirst(strtolower(trim($userRecs["User"]["name"]))); ?></option>
							<?php } ?>
							</select>
                      </li>                      
                      <li>
                        <a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
                      </li>
                  </ul>
               </div>
			   </form>
              </div>
              <table class = "bordered centered striped">                   
                  <thead>                    
                    <tr class ="red lighten-5">
                        
                        <th class ="red lighten-4 centered">Pack Name</th>
						<th>Hits</th>
						<th>CG OK</th>
						<th>Charged User Count</th> 
						<th>Failed User Count</th> 
						<th>New Sub Revenue</th> 
						<th>Renewal Count</th>
						<th>Renewal Revenue</th>
						<th>Total Revenue</th>
						<th>V3mobi Rev</th>
						<th>CP Rev</th>
						<th>Date</th>                  
                    </tr>
                  </thead>
                  <tbody>
                    
				<?php if(empty($tatareport)){ ?>
					<tr>
						<td colspan="14" class="noRecordMsg">
						 Sorry!! no record found.
						</td>
					</tr>
				<?php } else {
					$totalhits=0; 
					$cg_ok=0;
					$charged_count=0;
					$failed_count=0;
					$new_sub=0;
					$renewal_count=0;
					$renewal_rev=0;
					$total_rev=0;
					$v3mobi_rev=0;
					$cp_rev=0;
					
					
					foreach($tatareport as $val){
					$factor = $val['cp_revenue_detail']['factor'];	

				?>
					<tr>
						
						<td class="data-left"><?php echo $val['cp_revenue_detail']['pack_name']; ?></td>
						<td class="data-right"><?php 
						if($operator=='tata')
						echo $thit= round($val['cp_revenue_detail']['total_hits'],0); 
						else
						echo $thit= round(($val['cp_revenue_detail']['total_hits']*$factor)/100,0); 

						?></td> 
						<?php $totalhits=$totalhits+$thit;   ?>
						<td class="data-right"><?php 
						$cg=round(($val['cp_revenue_detail']['cg_ok']*$factor)/100,0);
						echo $cg;  ?></td>
						<?php $cg_ok = $cg_ok+$cg;   ?>
						<td class="data-right"><?php if($cg == '0'){ $charged=0;}else{$charged=round(($val['cp_revenue_detail']['charged_user_count']*$factor)/100,0);}	
						echo $charged; ?></td> <?php    $charged_count=$charged_count+$charged;   ?>
						<td class="data-right"><?php 
						$failed= $cg-$charged;
						//$failed= round(($val['cp_revenue_detail']['failed_user_count']*$factor)/100,0); 
						echo  $failed;  ?></td> <?php  $failed_count=$failed+$failed_count;  ?>
						<td class="data-right"><?php if($charged == '0'){ $new_sub_rev=0;}else{$new_sub_rev=number_format(($val['cp_revenue_detail']['new_sub_rev']*$factor)/100,2, '.', '');}
						echo $new_sub_rev; ?></td> <?php $new_sub=$new_sub_rev+$new_sub; ?>
						<td class="data-right"><?php $renewal=round(($val['cp_revenue_detail']['renewal_count']*$factor)/100,0); 
						echo $renewal; ?></td> <?php $renewal_count=$renewal+$renewal_count; ?>

						<td class="data-right"><?php /*if($renewal == '0'){ $renewalrev=0;}else{*/$renewalrev= number_format(($val['cp_revenue_detail']['renewal_rev']*$factor)/100, 2, '.', ''); //}
						echo $renewalrev; ?></td> <?php $renewal_rev= $renewal_rev+$renewalrev;?>

						<td class="data-right"><?php if($new_sub_rev =='0' && $renewalrev =='0'){$totalrev=0;}else{$totalrev=number_format(($val['cp_revenue_detail']['total_rev']*$factor)/100,2, '.', ''); }
						echo $totalrev; ?></td> <?php $total_rev=$total_rev+$totalrev; ?>

						<td class="data-right"><?php  if($new_sub_rev =='0' && $renewalrev =='0'){$v3mobirev=0; }else{$v3mobirev=number_format(($val['cp_revenue_detail']['v3mobi_rev']*$factor)/100,2, '.', '');} 
						echo $v3mobirev; ?></td> <?php $v3mobi_rev=$v3mobirev+$v3mobi_rev; ?>

						<td class="data-right"><?php if($new_sub_rev =='0' && $renewalrev =='0'){ $cprev=0;}else{$cprev=number_format(($val['cp_revenue_detail']['cp_rev']*$factor)/100,2, '.', ''); }
						echo $cprev; ?></td> <?php $cp_rev=$cprev+$cp_rev;  ?>

						<td><?php echo date('d-m-Y', strtotime($val['cp_revenue_detail']['date'])); ?></td>
							
					</tr>
				<?php  } ?>                   
                  </tbody>
                  <tfoot class ="red lighten-5">
				  <tr>
                      <td>Total</td> 
						<td class="data-right"><?php echo $totalhits; ?></td>
						<td class="data-right"><?php echo $cg_ok; ?></td>
						<td class="data-right"><?php echo $charged_count;?></td>
						<td class="data-right"><?php echo $failed_count; ?></td>
						<td class="data-right"><?php echo $new_sub; ?></td>
						<td class="data-right"><?php echo $renewal_count; ?></td>
						<td class="data-right"><?php echo $renewal_rev; ?></td>
						<td class="data-right"><?php echo $total_rev; ?></td>
						<td class="data-right"><?php echo $v3mobi_rev;?></td>
						<td class="data-right"><?php echo $cp_rev; ?></td>
						<td></td>
					</tr>
				<?php } ?>
                  </tfoot>
              </table>
                
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.tatarevenueReport.submit();
	})
})
</script>		