<script>
	$(function() {
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "table-format-data white-bg">
	<div class = "row">
		<div class = "col s3">
			<div class = "detail-title-section">
				<div class = "card card-stats">
					<div class = "card-header" data-background-color = "red">
						<i class = "material-icons"> person </i>
					</div>
					<div class = "card-content">
						<h3>Daily User Report</h3>
					</div>                        
				</div>
			</div>
		</div>
			<div class = "col s9">
			   <form name="reportUserData" id="reportUserData" method="get" action="<?php echo BASE_URL; ?>/reports/report_user_data">
				<ul class = "select-list right">
					<li>                                             
						<input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>">
						<i class = "material-icons">event_note</i>
					</li>                      
					<li>
						<a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
						<!--<a class="waves-effect waves-light btn red darken-1">Get Report</a>-->
					</li>
				</ul>
				</form>
			</div>
		</div>
		<table class = "bordered centered">                   
			<thead>                    
			<tr class = "red lighten-5">
				<th class = "red lighten-4"></th>
				<th>Daily No of registered Users</th>
				<th>Daily No of Logged in Users</th>
				<th>Daily No of video views (30 sec)</th>
				<th>Daily No of video viewed completely</th></tr>
			</thead>
			<tbody>
				<tr>
					<th class = "red lighten-4">Web</th>                      
					<td><?php echo $sql1[0][0]["count1"]; ?></td>
					<td><?php echo $sql8[0][0]["count8"]; ?></td>
					<td><?php echo $sql4[0][0]["count4"]; ?></td>
					<td><?php echo $sql6[0][0]["count6"]; ?></td>                      
				</tr>
				<tr>
					<th class = "red lighten-4">App</th>
					<td><?php echo $sql2[0][0]["count2"]; ?></td>
					<td><?php echo $sql9[0][0]["count9"]; ?></td>
					<td><?php echo $sql5[0][0]["count5"]; ?></td>
					<td><?php echo $sql7[0][0]["count7"]; ?></td>                      
				</tr>                    
			</tbody>
			<tfoot class = "red lighten-5 centered">
				<tr>
					<th class = "red lighten-4">Daily No of video views</th>
					<td><?php echo $sql3[0][0]["count3"]; ?></td> 
					<td></td>
					<td></td>
					<td></td>            
				</tr>
			</tfoot>
		</table>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.reportUserData.submit();
	})
})
</script>
          