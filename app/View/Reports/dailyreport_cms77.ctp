
<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-0d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> assignment </i>
                        </div>
                        <div class = "card-content">
                            <h3>
							<?php 
							if(!empty($_GET["biller_id"]))
							{
							   if($_GET["biller_id"] == 'vf')
							   {
							       echo "Vodafone Daily Report";
							   }
							   else if($_GET["biller_id"] == 'tt')
							   {
							       echo "Tata Daily Report";
							   }
							   else if($_GET["biller_id"] == 'ia')
							   {
							       echo "Idea Daily Report";
							   }
							   else
							   {
							      echo "Daily Report";
							   }
							}
							else
							{
							   echo "Daily Report";
 							}
							?>
							</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9">
			    <form name="dailyReport" id="dailyreport_old_cms77" method="get" action="<?php echo BASE_URL; ?>/reports/dailyreport_cms77">
                  <ul class = "select-list right">
                      <li>
                       <input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d'); } ?>" />
                       <!-- <i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                        <select name="biller_id" id="biller_id">
						<option value="" >Select Operator</option>
                        <option value="vf">Vodafone </option>
				        <option value="tt">Tata </option>
				        <option value="ia">Idea </option>
                        </select>
                      </li>
                      <li>
                        <select name="publisher" id="publisher">
							<option value="" >Select Service</option>
				            <option value="bnama">Bhojpurinama</option>
				            <option value="filmy">filmy</option>
                            <option value="mini">mini</option>
				            <option value="vfcomics">vfcomics</option>
				            <option value="mtoons">mtoons</option>
				            <option value="firstcut">Firstcut</option>
                        </select>
                      </li>
                      <li>
                        <!---<a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>--->
						<input type="submit" style="padding:10px 10px !important;" value="Search" class="waves-effect waves-light btn red darken-1"/>
						<?php $url="dailyreportdetail_cms_77?startDate=".date('Y-m-d',strtotime($_GET['startDate']))."&biller_id=".$_GET['biller_id']."&publisher=".$_GET['publisher']; ?>
						<a class="waves-effect waves-light btn red darken-1" href="<?php echo $url; ?>">Details</a>
						
						<?php if($_GET['biller_id'] =='vf'){ 
						$url1="churndetail_old_cms_77?startDate=".date('Y-m-d',strtotime($_GET['startDate']))."&biller_id=".$_GET['biller_id']."&publisher=".$_GET['publisher'];?>	
						<a class="waves-effect waves-light btn red darken-1" href="<?php echo $url1; ?>">Churn</a>
						<?php }?>
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <div class = "row">
                  <div class = "col s6">
                      <!--<ul class = "view-in-listing">
                        <li>
                          <a href="daily-report.html">
                            <i class = "material-icons grey-text">format_list_bulleted</i>
                          </a>
                        </li>
                        <li>
                          <a href="view-graph.html" class = "active">
                            <i class = "material-icons grey-text">view_module</i>
                          </a>
                        </li>                       
                      </ul>-->
                  </div>
                  <div class = "col s6 small-filteration">
                      <!--<ul class = "right">                       
                        <li>                                          
                          <select>
                            <option value="" disabled selected>Sort By</option>
                            <option value="1">View</option>
                            <option value="2">Performance</option>
                            <option value="3">Option 3</option>
                          </select> 
                        </li>
                        <li>                                         
                          <select>
                            <option value="" disabled selected>Filter By</option>
                            <option value="1">Submitted For Approval</option>
                            <option value="2">Rejected</option>
                            <option value="3">Option 3</option>
                          </select>
                        </li>
                      </ul>-->
                  </div>
              </div>
              
              <table class = "bordered centered">                   
                  <thead>                    
                  <tr>
					<th>Date</th>
					<th>Total Hits</th>
					<th>CG OK</th>
					<th>New Activation Count</th>
					<th>New Activation Revenue</th>	
					<th>CallBack Count</th>
					<th>Total Activation Count</th>
					<th>Total Activation Revenue</th>
				    <th>Renewal Count</th>
					<th>Renewal Revenue</th>
					<th>Total Revenue </th>
					<th>Parking</th>
					<th>Deactivate</th>
				  </tr>
                  </thead>
                  <tbody>
                  <?php  //echo '<pre>';print_r($resArr);die;
								if(!empty($resArr))
								{
									
								foreach($resArr as $date=>$res): 
								
								 if(!isset($res['callback_revenue']))
								 {
								   $res['callback_revenue']=0;
								 }
								//foreach($res1 as $interface=>$res): ?>
						<tr>
							<td><?php echo $date; ?></td>
							<td><?php if(isset($res['total_hit'])){echo $res['total_hit'];} else echo 0;  ?></td>
							<td><?php if(isset($res['cg_ok'])){echo $res['cg_ok'];} else echo 0; ?></td>
							<td><?php if(isset($res['billing_done'])){echo $res['billing_done'];} else echo 0; ?></td>
							<td><?php if(isset($res['total_revenue'])){echo $res['total_revenue'];} else echo 0; ?></td>		
							<td><?php if(isset($res['callback_status'])){echo $res['callback_status'];} else echo 0; ?></td>
								<?php if($_GET['biller_id']=='ac')
								{	 ?>
							 <td><?php if(isset($res['count_revenue_ac'])){echo $res['count_revenue_ac'];}else echo 0;?></td>
                                                         <td><?php if(isset($res['total_revenue_ac'])){echo $res['total_revenue_ac'];}else echo 0;?></td>
                                                         <td><?php if(isset($res['count_renewal_ac'])){ echo $res['count_renewal_ac'];}else echo 0;?></td>
                                                        <td><?php if(isset($res['total_renewal_ac'])){ echo $res['total_renewal_ac'];}else echo 0;?></td>
                                                        <td><?php if(isset($res['total_renewal_ac'])){echo $res['total_revenue_ac']+$res['total_renewal_ac'];}else echo 0; ?></td>
                                                        <td><?php if(isset($res['count_sub2'])){ echo $res['count_sub2'];}else echo 0; ?></td>
                                                        <td><?php if(isset($res['count_unsub'])){ echo $res['count_unsub'];}else echo 0;  ?></td>

								<?php }
								else {?>
							 <td><?php if(isset($res['count_revenue'])){echo $res['count_revenue'];}else echo 0;?></td>
							 <td><?php if(isset($res['callback_revenue'])){echo $res['callback_revenue'];}else echo 0;?></td>
							 <td><?php if(isset($res['Recount'])){ echo $res['Recount'];}else echo 0;?></td>
							<td><?php if(isset($res['renewalTotal_revenue'])){ echo $res['renewalTotal_revenue'];}else echo 0;?></td>
							<td><?php echo $res['callback_revenue']+$res['renewalTotal_revenue'];  ?></td>
							<td><?php if(isset($res['parking'])){ echo $res['parking'];}else echo 0; ?></td>
							<td><?php if(isset($res['deactivate'])){ echo $res['deactivate'];}else echo 0;  ?></td>
							<?php } ?>
							</tr>
						<?php endforeach; 
						 //endforeach; 
						 }
						else{?>
						<tr><td colspan="13">No Record Found</td></tr>
						<?php
						}?>
                  </tbody>
              </table>
               
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.dailyReport.submit();
	})
})
</script>