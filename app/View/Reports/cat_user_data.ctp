
<script>
	$(function() {
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "table-format-data white-bg">
	<div class = "row">
		<div class = "col s3">
			<div class = "detail-title-section">
				<div class = "card card-stats">
					<div class = "card-header" data-background-color = "red">
						<i class = "material-icons"> person </i>
					</div>
					<div class = "card-content">
						<h3>Daily User Report</h3>
					</div>                        
				</div>
			</div>
		</div>
			<div class = "col s9">
			   <form name="catUserData" id="catUserData" method="get" action="<?php echo BASE_URL; ?>/reports/cat_user_data">
				<ul class = "select-list right">
					<li>                                             
						<input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>">
						<i class = "material-icons">event_note</i>
					</li>                      	
					<li>
								<!---<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>--->
								<input type="submit" name="search" class="waves-effect waves-light btn red darken-1" value="search" style="padding:10px 10px !important;">
							</li>
							<!--li>
							<input type="submit" name="download" class="waves-effect waves-light btn red darken-1" value="download" style="padding:10px 10px !important;">
							</li-->
				</ul>
				</form>
			</div>
		</div>
		<table class = "bordered centered">                   
			<thead>                    
			<tr class = "red lighten-5">
				<th class = "red lighten-4"></th>
				<th>Category Name</th>
				<th>Total Content</th>
				<th colspan = 2>Daily No of video views (30 sec)</th>
				<th colspan = 2>Daily No of video viewed completely</th></tr>
			</thead>
			<tbody>
			<tr>
			
			<th></th>
			<th></th>
			<th></th>
			<th>WEB</th>
			<th>APP</th>
			<th>WEB</th>
			<th>APP</th>
			</tr>
			
			<?php
			if(!empty($resArr)){
			$total =0;$totalview=0;$totalview2=0;$count3=0;$count4=0;
			  foreach($resArr as $resArr1)
			  {
			 $total = $total + $resArr1['total'];
			 $totalview = $totalview + $resArr1['totalview'];
			 $totalview2 = $totalview2 + $resArr1['totalview2'];
			 $count3 = $count3 + $resArr1['count3'];
			 $count4 = $count4 + $resArr1['count4'];
			 
			  ?>
			  <tr>
				<td></td>
				<td><?php echo $resArr1['name'];?></td>
				<td><?php echo $resArr1['total'];?></td>
				<td>
					<?php if(!empty($resArr1['totalview'])) {
						echo $resArr1['totalview'];
						} else {
						echo "0";
						}?>
				</td>	
				<td>
					<?php if(!empty($resArr1['totalview2'])) {
						echo $resArr1['totalview2'];
					}else { echo "0"; }?>
				</td>
				
				<td>
					<?php if(!empty($resArr1['count3'])) {
						echo $resArr1['count3'];
					}else { echo "0"; }?>
				</td>
				<td>
					<?php if(!empty($resArr1['count4'])) {
						echo $resArr1['count4'];
					}else { echo "0"; }?>
				</td>
				
							  
			  </tr>
			  <?php }

			

			  } else { ?>
			  <tr><td colspan="13" style="color:red; font-size: 20px">!!! No Record Found</td></tr>
						<?php
						}?>
				
			</tbody>
			
			<tfoot class = "red lighten-5 centered">
				<tr>
					<th class = "red lighten-4">Total Record</th>
						
					<td></td>		
					<td><?php echo $total; ?></td> 
					<td><?php echo $totalview; ?></td>
					<td><?php echo $totalview2; ?></td>
					<td><?php echo $count3; ?></td> 
					<td><?php echo $count4; ?></td>
				</tr>
			</tfoot>
		</table>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.catUserData.submit();
	})
})
</script>
          
