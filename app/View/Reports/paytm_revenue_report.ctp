<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		$("#datepicker2").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchCategory" id="searchCategory" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list right">  
                            <li>Start Date</li>						
                            <li>
							<?php $prevDate = date('Y-m-d', strtotime(' -1 day')); ?>
							<input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo $prevDate; } ?>" />
							<!--<input placeholder="startDate" id="searchString" name="startDate" type="date" class="validate" max="<?php echo $prevDate;?>" required>--->
							</li>
							 <li>End Date</li>	
                            <li>
							<input type="text" id="datepicker2" name="endDate" id="startDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo $prevDate; } ?>" />
							<!--<input placeholder="endDate" id="searchString" name="endDate" type="date" class="validate" max="<?php echo $prevDate;?>" required>--->
							</li>								
							<li>
								<!---<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>--->
								<input type="submit" name="search" class="waves-effect waves-light btn red darken-1" value="search" style="padding:10px 10px !important;">
							</li>
							<li>
							<input type="submit" name="download" class="waves-effect waves-light btn red darken-1" value="download" style="padding:10px 10px !important;">
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<!--<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>-->
		</div>
		<form name="sortCategory" id="sortCategory" action="" method="get">
			<div class = "col s6 small-filteration">
				<!---<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Active</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>
				</ul>--->
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    
	<!--List view seection strat here-->
	<?php if(!empty($categories)){
	?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th>User ID</th>
						<th>Cell</th>
						<th>Email</th>
						<th>Location</th>
						<th>Sex</th>
						<th>Signup Date</th>
						<th>Videos Views</th>
						<th>Payment Page</th>
						<th>Subscription</th>
						<th>Subscription Value</th>
						<th>Cancellation</th>
						<th>Cancellation Date</th>
						<th>Renewal</th>
						<th>Payment Mode</th>
						<th>Last login </th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($categories as $categoriesRec)
					{ 				?>
						<tr>
						<td>
						<?php echo $categoriesRec["user_paytm_report"]["id"]; ?>
						</td>
						<td><?php if(!empty($categoriesRec["user_paytm_report"]["msisdn"]))
						{
						    echo $categoriesRec["user_paytm_report"]["msisdn"];
						}
						else
						{
						    echo "NA";
						}
						?></td>
						<td><?php if(!empty($categoriesRec["user_paytm_report"]["email"]))
						{
						    echo $categoriesRec["user_paytm_report"]["email"];
						}
						else
						{
						    echo "NA";
						}
						?></td>
						<td><?php if(!empty($categoriesRec["user_paytm_report"]["location"]))
						{
						    echo $categoriesRec["user_paytm_report"]["location"];
						}
						else
						{
						    echo "NA";
						}
						?></td>
						<td><?php if(!empty($categoriesRec["user_paytm_report"]["sex"]))
						{
						    echo $categoriesRec["user_paytm_report"]["sex"];
						}
						else
						{
						    echo "NA";
						}
						?></td>
						<td><?php if(!empty($categoriesRec["user_paytm_report"]["Signup"]))
						{
						    echo $categoriesRec["user_paytm_report"]["Signup"];
						}
						else
						{
						    echo "NA";
						}
						?></td>
					    <td><?php echo $categoriesRec["user_paytm_report"]["video_views"]; ?></td>
						<td><?php 
						if($categoriesRec["user_paytm_report"]["paytm_page"] == 1)
						{
						   echo "Yes"; 
						}
						else
						{
						   echo "No"; 
						}
						?></td>
						<td><?php 
						if($categoriesRec["user_paytm_report"]["subscription"] == 1)
						{
						   echo "Yes"; 
						}
						else
						{
						   echo "No"; 
						}
						?></td>
						<td><?php echo $categoriesRec["user_paytm_report"]["subscription_act_value"]; ?></td>
						<td>
						<?php if($categoriesRec["user_paytm_report"]["cancel_status"] == 1)
						{
						    echo "Yes";
						}
						else
						{
						     echo "No";
						}
						?>
						</td>
						<td>
						<?php if(!empty($categoriesRec["user_paytm_report"]["cancellation_date"]))
						{
						    echo $categoriesRec["user_paytm_report"]["cancellation_date"];
						}
						else
						{
						    echo "NA";
						}
						?>
						</td>
						<td>
						<?php if($categoriesRec["user_paytm_report"]["renewal"] == 1)
						{
						    echo "Yes";
						}
						else
						{
						     echo "No";
						}
						?>
						</td>
						<td><?php echo $categoriesRec["user_paytm_report"]["payment_mode"]; ?></td>
						<td>
						<?php if(!empty($categoriesRec["user_paytm_report"]["last_login"]))
						{
						    echo $categoriesRec["user_paytm_report"]["last_login"];
						}
						else
						{
						    echo "NA";
						}
						?>
						</td>
						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
</div>
<?php if((!empty($categories)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/reports/paytm_revenue_report";
	
	if(@$_GET["startDate"] !=""){
		$startDate = @$_GET["startDate"];
	} else {
		$startDate = "";
	}
	if(@$_GET["endDate"] !=""){
		$endDate = @$_GET["endDate"];
	} else {
		$endDate = "";
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&search=search'; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&startDate='.@$startDate.'&endDate='.@$endDate.'&search=search'; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&search=search'; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    
	$("#searchRec").click(function(){
		document.searchCategory.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortCategory.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortCategory.submit();
	
	})
	
})
</script>

