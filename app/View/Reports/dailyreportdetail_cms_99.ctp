<?php 
$biller_id = $_GET['biller_id'];
$publisher=$_GET['publisher'];
?>
<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s12">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> assignment </i>
                        </div>
                        <div class = "card-content">
                            <!--<h3>Daily Report Detail</h3>-->
							 <?php if($biller_id=='tt'){?>
							<h3><?php echo "Tata Daily Report"; ?> (<?php echo date('d-m-Y', strtotime($_GET['startDate'])); ?>)</h3>
						<?php } ?>
						<?php if($biller_id=='vf'){?>
							<h3><?php echo "Vodafone Daily Report"; ?> (<?php echo date('d-m-Y', strtotime($_GET['startDate'])); ?>)</h3>
						<?php } ?>
						<?php if($biller_id=='ia'){?>
							<h3><?php echo "Idea Daily Report"; ?> (<?php echo date('d-m-Y', strtotime($_GET['startDate'])); ?>)</h3>
						<?php } ?>
						<?php if($biller_id=='at'){?>
							<h3><?php echo "Airtel Daily Report"; ?> (<?php echo date('d-m-Y', strtotime($_GET['startDate'])); ?>)</h3>
						<?php } ?>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9">
                  <!--<ul class = "select-list right">
                      <li>
                       <input placeholder = "Today" type="date" class="datepicker" place-holder = "dd/mm/yyyy">
                       <!-- <i class = "material-icons">event_note</i> -->
                      <!--</li>
                      <li>
                        <select>
                            <option value="" disabled selected>Operator</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                      </li>
                      <li>
                        <select>
                            <option value="" disabled selected>Service</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                      </li>
                      <li>
                        <a class="waves-effect waves-light btn red darken-1">Search</a>
                        <a class="waves-effect waves-light btn red darken-1">Get Report</a>
                      </li>
                  </ul>-->
               </div>
              </div>
              <div class = "row">
                  <div class = "col s6">
                      <!--<ul class = "view-in-listing">
                        <li>
                          <a href="daily-report.html">
                            <i class = "material-icons grey-text">format_list_bulleted</i>
                          </a>
                        </li>
                        <li>
                          <a href="view-graph.html" class = "active">
                            <i class = "material-icons grey-text">view_module</i>
                          </a>
                        </li>                       
                      </ul>-->
                  </div>
                  
              </div>
              
              <table class = "bordered centered">                   
                  <thead>                    
                    <tr class ="red lighten-5">
                       <th>Date</th>
					   <th>Interface</th>
					   <th>Campaign Name</th>
					   <th>Total Hits</th>
					   <th>Mobile NotFound</th>
					   <th>Consent</th>
					   <th>CG OK</th>
					   <th>Billing Done</th>
					   <th>Total Revenue</th>	
					   <th>CallBack_status</th>
                    </tr>
                  </thead>

                  <tbody>
                  <?php  //echo '<pre>';print_r($resArr);die;
								if(!empty($resArr))
								{
								foreach($resArr as $date=>$res1): 
								foreach($res1 as $interface=>$res): ?>
						<tr>
							<td><?php echo $date; ?></td>
							<td><?php echo $interface; ?></td-->
							<td><?php if(isset($res['campaign_name'])){echo $res['campaign_name'];} else echo "NA";  ?></td>
							<td><?php if(isset($res['total_hit'])){echo $res['total_hit'];} else echo 0;  ?></td>
							<td><?php if(isset($res['mobile_notfound'])){ echo $res['mobile_notfound'];} else echo 0; ?></td>
							<td><?php if(isset($res['consent'])){ echo $res['consent'];} else echo 0; ?></td>
							<td><?php if(isset($res['cg_ok'])){echo $res['cg_ok'];} else echo 0; ?></td>
							<td><?php if(isset($res['billing_done'])){echo $res['billing_done'];} else echo 0; ?></td>
							<td><?php if(isset($res['total_revenue'])){echo $res['total_revenue'];} else echo 0; ?></td>		
							<td><?php if(isset($res['callback_status'])){echo $res['callback_status'];} else echo 0; ?></td>
						</tr>
						<?php endforeach; 
						 endforeach; 
						 }
						else{?>
						<tr><td colspan="9">No Record Found</td></tr>
						<?php
						}?>
                  </tbody>
              </table>
                 
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>