<script>
	$(function() {
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
	});
</script>

<div class = "table-format-data white-bg">
	<div class = "row">
		<div class = "col s3">
			<div class = "detail-title-section">
				<div class = "card card-stats">
					<div class = "card-header" data-background-color = "red">
						<i class = "material-icons"> data_usage </i>
					</div>
					<div class = "card-content">
						<h3>Daily Usage Report</h3>
					</div>                        
				</div>
			</div>
		</div>
		<div class = "col s9 right">
		<form name="dalyUsesReport" id="dalyUsesReport" method="get">
			<ul class = "select-list right">
			<div class="errorMsgFrm" style="display:none;"></div>
				<li>
					<!--<input placeholder = "From: " type="date" class="datepicker" place-holder = "dd/mm/yyyy">-->
					<input type="text" id="datepicker" name="startDate"  value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>" autocomplete="off"  />
				</li>
				<li>
					<select name="biller_id" id="biller_id">
						<option value="">Select Operator</option>
						<option value="ip">Ipay</option>
					</select>
                </li> 
				<li>
					<select name="publisher" id="publisher">
						<option value="">Select Publisher</option>
						<option value="mini">Mini</option>
					</select>
                </li> 
				<li>
					<a class="waves-effect waves-light btn red darken-1" id="searchRecord">Search</a>
                </li>				
				
			</ul>
			</form>
		</div>
	</div>
	<?php if(!empty($resArr)){ ?>
	<table class = "bordered centered striped">                   
		<thead>                    
			<tr class = "red lighten-5">
				<th>Date</th>
				<th>New Activation Count</th>
				<th>New Activation Revenue</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($resArr as $date=>$res){ ?>
			<tr>
				<td><?php if(!empty($date)){ echo $date; } else { echo $_GET["startDate"]; } ?></td>
				<td><?php if(isset($res['billing_done'])){echo $res['billing_done'];} else echo 0; ?></td>
				<td><?php if(isset($res['total_revenue'])){echo $res['total_revenue'];} else echo 0; ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php } else { ?>
		   <label class = "no-record red-text"><i class = "material-icons red-text">error</i><span> No Record Found </span></label>
	<?php } ?>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var totRevenue = $("#totRevenue").html();
	//alert(totRevenue);
	 $("#totRevenueBack").html(totRevenue);
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#searchRecord").click(function(){
		var datepicker = $.trim($('#datepicker').val());
		
		if(datepicker ==""){
		    $('#datepicker').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the from date.");
			return false;
		}
		
		if( $('#biller_id').val() == "") {
		    $('#biller_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select biller.");
			return false;
		}
		
		if( $('#publisher').val() == "") {
		    $('#publisher').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select publisher.");
			return false;
		}
		document.dalyUsesReport.submit();
	})
})
</script>