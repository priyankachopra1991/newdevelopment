<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		$("#datepicker1").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
	});

</script>
 <div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> data_usage </i>
                        </div>
                        <div class = "card-content">
                            <h3>Content Usage Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9 right">
			   <form name="contentusesReport" id="contentusesReport" method="get" action="<?php echo BASE_URL; ?>/reports/contentusesreport">
                  <ul class = "select-list right">
                      <li>
                       <input placeholder = "From: " type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>"  />
					   
                       <!--<i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                       
					   <input placeholder = "To: " type="text" id="datepicker1" name="endDate" id="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>">
                       <!-- <i class = "material-icons">event_note</i> -->
                      </li>                      
                      <li>
                       <select name="biller_id" id="biller_id">
							<!--option value="">Select</option>-->
							<?php foreach($operator as $operatorRec){ ?>
								<option value="<?php echo $operatorRec["Operator"]["slug"]; ?>" <?php if((@$_POST['biller_id'] == $operatorRec["Operator"]["slug"]) ||(@$_GET['biller_id'] == $operatorRec["Operator"]["slug"])){ echo "selected"; } ?>><?php echo $operatorRec["Operator"]["name"] ?></option>
							<?php } ?>
							</select>
                      </li>
                       
                      <li>
                        <select name="publisher" id="publisher">
							<!--<option value="">Select</option>-->
							<?php foreach($project as $projectRec){ ?>
								<option value="<?php echo $projectRec["Project"]["slug"]; ?>" <?php if((@$_POST['publisher'] == $projectRec["Project"]["slug"]) ||(@$_GET['publisher'] == $projectRec["Project"]["slug"])){ echo "selected"; } ?>><?php echo $projectRec["Project"]["name"] ?></option>
							<?php } ?>
						</select>	
                      </li>
                      <li>
                        <a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <table class = "bordered centered striped">                   
                  <thead>                    
                    <tr class = "red lighten-5">
                        <th>Mobile</th>
						<th>Product Name</th>
						<th>Subscription Date</th>
						<th>UnSubscription Date</th>
						<th>Renewal Count</th>
						<th>Last Renewal Date</th>
						<th>Revenue</th>  
						<th>Downloads Limit</th>
						<th>Downloads Used</th>                       
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($downloadsRow)){ ?>
					<tr>
						<td colspan="14" class="noRecordMsg">
						 Sorry!! no record found.
						</td>
					</tr>
				<?php } else {
					foreach($downloadsRow as $key=>$val){ 
				?>
					<tr>
						<?php if(isset($val['Subscribeuser']['msisdn'])){ ?>
							<td class="data-right"><?php echo $val['Subscribeuser']['msisdn'];?></td>
						<?php } else {?>
							<td class="data-right">0</td>
						<?php }?>
						<?php if(isset($val['Subscribeuser']['product_id'])){ ?>
							<td class="data-left"><?php echo $val['Subscribeuser']['product_id'];?></td>
						<?php } else {?>
							<td class="data-right">0</td>
						<?php }?>
						<?php if(isset($val['Subscribeuser']['subscription_date'])){ ?>
							<td><?php echo date('d-m-Y', strtotime($val['Subscribeuser']['subscription_date']));?></td>
						<?php } else {?>
							<td class="data-left">Log Not Found </td>
						<?php }?>
						<?php if(isset($val['Subscribeuser']['unsubscription_date'])){ ?>
							<td><?php echo date('d-m-Y', strtotime($val['Subscribeuser']['unsubscription_date']));?></td>
						<?php } else {?>
							<td  class="data-right">0</td>
						<?php }?>
						<?php if(isset($val['Subscribeuser']['renewal_cycle'])){ ?>
							<td class="data-right"><?php echo $val['Subscribeuser']['renewal_cycle'];?></td>
						<?php } else {?>
							<td class="data-right">0</td>
						<?php } ?>
						<?php if(isset($val['Subscribeuser']['renewal_date'])){ ?>
							<td ><?php echo date('d-m-Y', strtotime($val['Subscribeuser']['renewal_date']));?></td>
						<?php } else {?>
							<td class="data-right">0</td>
						<?php }?>

						<?php if(isset ($val['Subscribeuser']['total_revenue'])){ ?>
							<td class="data-right"><?php echo number_format($val['Subscribeuser']['total_revenue'],2);?></td>
						<?php } else {?>
							<td class="data-right">0</td>
						<?php } ?>
						<?php if(isset($val['Subscribeuser']['download_limit'])) {?>
							<td class="data-right"><?php echo $val['Subscribeuser']['download_limit'];?></td>	
						<?php } else {?>
							<td class="data-right">0</td>
						<?php } ?>
						<?php if(isset($val['Subscribeuser']['download_count'])) {?>
							<td class="data-right"><?php echo $val['Subscribeuser']['download_count'];?></td>	
						<?php } else {?>
							<td class="data-right">0</td>
						<?php  } ?>
							
					</tr>
					<?php } ?>
				<?php } ?>
                  </tbody>
              </table>
                  <?php if((!empty($downloadsRow)) && ($numOfPage>1)){ 
?>
    <?php
	$pageName = BASE_URL."/reports/contentusesreport";
	$Nav="";
	$startDate = "";
	$endDate = "";
	$biller_id = "";
	$publisher = "";
	if(@$_GET["biller_id"] !=""){
		$biller_id = @$_GET["biller_id"];
	} else {
		$biller_id = @$_POST["biller_id"];
	}
	
	if(@$_GET["startDate"] !=""){
		$startDate = @$_GET["startDate"];
	} else {
		$startDate = @$_POST["startDate"];
	}
	
	if(@$_GET["endDate"] !=""){
		$endDate = @$_GET["endDate"];
	} else {
		$endDate = @$_POST["endDate"];
	}
	
	if(@$_GET["publisher"] !=""){
		$publisher = @$_GET["publisher"];
	} else {
		$publisher = @$_POST["publisher"];
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"]=1;
	}

	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&biller_id='.@$biller_id.'&publisher='.@$publisher; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&startDate='.@$startDate.'&endDate='.@$endDate.'&biller_id='.@$biller_id.'&publisher='.@$publisher; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&biller_id='.@$biller_id.'&publisher='.@$publisher; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>	
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>

	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.contentusesReport.submit();
	})
})
</script>