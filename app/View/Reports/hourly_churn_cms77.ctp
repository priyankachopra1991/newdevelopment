
<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> assignment </i>
                        </div>
                        <div class = "card-content">
                            <h3>
							<?php 
							if(!empty($_GET["biller_id"]))
							{
							   if($_GET["biller_id"] == 'vf')
							   {
							       echo "Vodafone Churn Report";
							   }
							   else if($_GET["biller_id"] == 'tt')
							   {
							       echo "Tata Churn Report";
							   }
							   else if($_GET["biller_id"] == 'ia')
							   {
							       echo "Idea Churn Report";
							   }
							   else
							   {
							      echo "Hourly Churn Report";
							   }
							}
							else
							{
							   echo "Hourly Churn Report";
 							}
							?>
							</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9">
			    <form name="dailyReport" id="dailyreport_old_cms77" method="get" action="<?php echo BASE_URL; ?>/reports/hourly_churn_cms77">
                  <ul class = "select-list right">
                      <li>
                       <input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d'); } ?>" />
                       <!-- <i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                        <select name="biller_id" id="biller_id" required>
						<option value="" >Select Operator</option>
                        <option value="vf">Vodafone </option>
				        <option value="tt">Tata </option>
				        <option value="ia">Idea </option>
                        </select>
                      </li>
                      <li>
                        <!---<a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>--->
						<input type="submit" style="padding:10px 10px !important;" value="Search" class="waves-effect waves-light btn red darken-1"/>
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <div class = "row">
                  <div class = "col s6">
                      <!--<ul class = "view-in-listing">
                        <li>
                          <a href="daily-report.html">
                            <i class = "material-icons grey-text">format_list_bulleted</i>
                          </a>
                        </li>
                        <li>
                          <a href="view-graph.html" class = "active">
                            <i class = "material-icons grey-text">view_module</i>
                          </a>
                        </li>                       
                      </ul>-->
                  </div>
                  <div class = "col s6 small-filteration">
                      <!--<ul class = "right">                       
                        <li>                                          
                          <select>
                            <option value="" disabled selected>Sort By</option>
                            <option value="1">View</option>
                            <option value="2">Performance</option>
                            <option value="3">Option 3</option>
                          </select> 
                        </li>
                        <li>                                         
                          <select>
                            <option value="" disabled selected>Filter By</option>
                            <option value="1">Submitted For Approval</option>
                            <option value="2">Rejected</option>
                            <option value="3">Option 3</option>
                          </select>
                        </li>
                      </ul>-->
                  </div>
              </div>
              
              <table class = "bordered centered">                   
                  <thead>                    
                  <tr>
								<th>date</th>
								<th colspan="2">Deactivation last 30 min</th>
								<th colspan="2">Deacativation last 1 hour</th>
								<th colspan="2">Deactivation last 2 hour</th>
								<th colspan="2">Deactivation last 3 hour</th>
								<th colspan="2">Same day churn</th>						
							</tr>
							<tr>
								<th></th>
								<th scope="col">SUB</th>
								<th scope="col">Churn</th>
								<th scope="col">SUB</th>
								<th scope="col">Churn</th>
								<th scope="col">SUB</th>
								<th scope="col">Churn</th>
								<th scope="col">SUB</th>
								<th scope="col">Churn</th>
								<th scope="col">SUB</th>
								<th scope="col">Churn</th>
								<th scope="col">ChurnPercentage</th>

							</tr>
                  </thead>
                  <tbody>
                   <?php // echo '<pre>';print_r($arr);die;
					  			$totalSub=0;
								$totalChurn=0;
								$totalPer=0;
					  			
								if(!empty($arr))
								{
								//echo $res['30min']['churn']; die;
								$churnPer=100*$arr['daily']['churn']/$arr['daily']['sub'];
								?>
						<tr>
							<td><?php echo $_GET['startDate']; ?></td>
							 
							<td><?php if(isset($arr['30min']['sub'])){echo $arr['30min']['sub'];} else echo 0;  ?>
							
							<td><a href="hchurn_detail?startDate=<?php echo $_GET['startDate']; ?>&min=30min&biller_id=<?php echo $_GET['biller_id']; ?>"><?php if(isset($arr['30min']['churn'])){echo $arr['30min']['churn'];} else echo 0;  ?></a>
							</td>
								<td><?php if(isset($arr['1houre']['sub'])){echo $arr['1houre']['sub'];} else echo 0;  ?>
							<td><a href="hchurn_detail?startDate=<?php echo $_GET['startDate']; ?>&min=1houre&biller_id=<?php echo $_GET['biller_id']; ?>"><?php if(isset($arr['1houre'])){echo $arr['1houre']['churn'];} else echo 0;  ?></a></td>
							
								<td><?php if(isset($arr['2houre']['sub'])){echo $arr['2houre']['sub'];} else echo 0;  ?>
							<td><a href="hchurn_detail?startDate=<?php echo $_GET['startDate']; ?>&min=2houre&biller_id=<?php echo $_GET['biller_id']; ?>"><?php if(isset($arr['2houre'])){echo $arr['2houre']['churn'];} else echo 0;  ?></a></td>
							<td><?php if(isset($arr['3houre']['sub'])){echo $arr['3houre']['sub'];} else echo 0;  ?>

							<td><a href="hchurn_detail?startDate=<?php echo $_GET['startDate']; ?>&min=3houre&biller_id=<?php echo $_GET['biller_id']; ?>"><?php if(isset($arr['3houre'])){echo $arr['3houre']['churn'];} else echo 0;  ?></a></td>
							<td>
							<?php if(isset($arr['daily']['sub'])){echo $arr['daily']['sub'];} else echo 0;  ?>

							<td>
							<a href="hchurn_detail?startDate=<?php echo $_GET['startDate']; ?>&min=daily&biller_id=<?php echo $_GET['biller_id']; ?>"><?php if(isset($arr['daily'])){echo $arr['daily']['churn'];} else echo 0;  ?></a>
							<td>
							<?php if(isset($churnPer)){echo $churnPer;} else echo 0;  ?>
							</td>
								
							
							</tr>
						<?php }  else{?>
						<tr><td colspan="12">No Record Found</td></tr>
						<?php
						}?>
                  </tbody>
              </table>
               
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.dailyReport.submit();
	})
})
</script>