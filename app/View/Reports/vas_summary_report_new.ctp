<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "0d", "dateFormat":"yy-mm-dd"});
		$("#datepicker2").datepicker({maxDate: "0d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchCategory" id="searchCategory" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list left">  
                            <li>Start</li>						
                            <li>
							<?php $prevDate = date('Y-m-d', strtotime(' -1 day')); ?>
							<input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo $prevDate; } ?>" />
							<!---<input placeholder="startDate" id="searchString" name="startDate" type="date" class="validate" max="<?php echo $prevDate;?>" required>-->
							</li>
							<li>End</li>	
                            <li>
							<input type="text" id="datepicker2" name="endDate" id="startDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo $prevDate; } ?>" />
							<!--<input placeholder="endDate" id="searchString" name="endDate" type="date" class="validate" max="<?php echo $prevDate;?>" required>--->
							</li>	
                            <li>
                            <select name="biller_id" id="biller_id">
						    <option value="" >Select Operator</option>
                            <option value="vf" <?php if($_GET["biller_id"] == "vf"){ echo "selected"; } ?>>Vodafone </option>
				            <option value="tt" <?php if($_GET["biller_id"] == "tt"){ echo "selected"; } ?>>Tata </option>
				            <option value="ia" <?php if($_GET["biller_id"] == "ia"){ echo "selected"; } ?>>Idea </option>
							<option value="at" <?php if($_GET["biller_id"] == "at"){ echo "selected"; } ?>>Airtel </option>
				            <option value="rl" <?php if($_GET["biller_id"] == "rl"){ echo "selected"; } ?>>Reliance </option>
							<option value="bl" <?php if($_GET["biller_id"] == "bl"){ echo "selected"; } ?>>BSNL </option>
                            </select>
                            </li>
                            <li>
                            <select name="publisher" id="publisher">
							<option value="" >Select Service</option>
				            <option value="bnama" <?php if($_GET["publisher"] == "bnama"){ echo "selected"; } ?>>Bhojpurinama</option>
				            <option value="filmy" <?php if($_GET["publisher"] == "filmy"){ echo "selected"; } ?>>filmy</option>
                            <option value="mini" <?php if($_GET["publisher"] == "mini"){ echo "selected"; } ?>>mini</option>
				            <option value="vfcomics" <?php if($_GET["publisher"] == "vfcomics"){ echo "selected"; } ?>>vfcomics</option>
				            <option value="mtoons" <?php if($_GET["publisher"] == "mtoons"){ echo "selected"; } ?>>mtoons</option>
				            <option value="firstcut" <?php if($_GET["publisher"] == "firstcut"){ echo "selected"; } ?>>Firstcut</option>
							<option value="atcomics" <?php if($_GET["publisher"] == "firstcut"){ echo "selected"; } ?>>atcomics</option>
                            </select>
                            </li>
                            <br/>							
							<li>
								<!---<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>--->
								<input type="submit" name="search" class="waves-effect waves-light btn red darken-1" value="search" style="padding:10px 10px !important;">
							</li>
							<li>
							<input type="submit" name="download" class="waves-effect waves-light btn red darken-1" value="download" style="padding:10px 10px !important;">
							</li>
							<li>
							<?php $url="vas_detail_report_new?startDate=".date('Y-m-d',strtotime($_GET['startDate']))."&endDate=".date('Y-m-d',strtotime($_GET['endDate']))."&biller_id=".$_GET['biller_id']."&publisher=".$_GET['publisher']; ?>
							<a class="waves-effect waves-light btn red darken-1" style="margin-top:-30px;" href="<?php echo $url; ?>">DETAILS</a>
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<!--<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>-->
		</div>
		<form name="sortCategory" id="sortCategory" action="" method="get">
			<div class = "col s6 small-filteration">
				<!---<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Active</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>
				</ul>--->
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    
	<!--List view seection strat here-->
	<?php 
	$currDate = date('Y-m-d');
	$startDate = $_GET['startDate'];
	$endDate = $_GET['endDate'];
	if($startDate == $currDate || $endDate == $currDate)
	{ 
		   //echo "<pre>";
		   //print_r($categories1);
		   //die;
	   if(!empty($categories1))
	   {
	       
	      ?>
		  <div class = "col s12 listView">
	   <table class = "bordered centered striped">                   
	   <thead> 
	   <tr class ="red lighten-5">
				   <th colspan = "13">
				   <?php 
				   if(!empty($_GET["biller_id"]))
	               {
	                  if($_GET["biller_id"] == 'vf')
		              {
					     echo "Vodafone Summary Report";				       
		              }
					  else if($_GET["biller_id"] == 'tt')
					  {
					     echo "Tata Summary Report";
					  }
					  else if($_GET["biller_id"] == 'ia')
					  {
					     echo "Idea Summary Report";
					  }
					  else if($_GET["biller_id"] == 'at')
					  {
					     echo "Airtel Summary Report";
					  }
					  else if($_GET["biller_id"] == 'rl')
					  {
					     echo "Reliance Summary Report";
					  }
					  else if($_GET["biller_id"] == 'bl')
					  {
					     echo "BSNL Summary Report";
					  }
		              else 
		              {
		                 echo "Summary Report";
		              }
	               }
				   ?>
				   </th>
                </tr>	
                <tr class ="red lighten-5">
						<th>Date</th>
						<th>Clicks</th>
						<th>CG OK</th>
						<th>Activation</th>
						<th>Activation Revenue</th>
						<th>Call Back</th>
						<th>Parking</th>
						<th>Parking Revenue</th>
						<th>Renewal</th>
						<th>Renewal Revenue</th>
						<th>Total Revenue</th>
						<th>Parking in Queue</th>
						<th>Deactvation</th>
					</tr>				
                    </thead>
	   				<tbody>
					<?php foreach($categories1 as $categoriesRec)
					{						
					?>
						<tr>
						<td><?php echo $categoriesRec["date"];?></td>
						<td><?php if(isset($categoriesRec["Clicks"])){echo $categoriesRec["Clicks"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["cg_ok"])){echo $categoriesRec["cg_ok"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["Activation"])){echo $categoriesRec["Activation"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["Activation_revenue"])){echo $categoriesRec["Activation_revenue"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["Callback"])){echo $categoriesRec["Callback"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["parking"])){echo $categoriesRec["parking"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["parkingrevenue"])){echo $categoriesRec["parkingrevenue"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["Renewal"])){echo $categoriesRec["Renewal"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["Renewal_revenue"])){echo $categoriesRec["Renewal_revenue"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["totalrevenue"])){echo $categoriesRec["totalrevenue"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["parkinginqueue"])){echo $categoriesRec["parkinginqueue"];} else echo 0;?></td>
						<td><?php if(isset($categoriesRec["deactivate"])){echo $categoriesRec["deactivate"];} else echo 0;?></td>
						</tr>
						
					<?php } ?>
				</tbody>
       </table>	   
	</div>
		  <?
	   }
	}
	else
	{
	
	if(!empty($categories)){
	//echo "<pre>";
	//print_r($categories);
	//die;
	?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped">                   
				<thead>  
                <tr class ="red lighten-5">
				   <th colspan = "13">
				   <?php 
				   if(!empty($_GET["biller_id"]))
	               {
	                  if($_GET["biller_id"] == 'vf')
		              {
					     echo "Vodafone Summary Report";				       
		              }
					  else if($_GET["biller_id"] == 'tt')
					  {
					     echo "Tata Summary Report";
					  }
					  else if($_GET["biller_id"] == 'ia')
					  {
					     echo "Idea Summary Report";
					  }
					  else if($_GET["biller_id"] == 'at')
					  {
					     echo "Airtel Summary Report";
					  }
					  else if($_GET["biller_id"] == 'rl')
					  {
					     echo "Reliance Summary Report";
					  }
					  else if($_GET["biller_id"] == 'bl')
					  {
					     echo "BSNL Summary Report";
					  }
		              else 
		              {
		                 echo "Summary Report";
		              }
	               }
				   ?>
				   </th>
                </tr>				
					<tr class ="red lighten-5">
						<th>Date</th>
						<th>Clicks</th>
						<th>CG OK</th>
						<th>Activation</th>
						<th>Activation Revenue</th>
						<th>Call Back</th>
						<th>Parking</th>
						<th>Parking Revenue</th>
						<th>Renewal</th>
						<th>Renewal Revenue</th>
						<th>Total Revenue</th>
						<th>Parking in Queue</th>
						<th>Deactvation</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($categories as $categoriesRec)
					{ 				?>
						<tr>
						<td><?php echo $categoriesRec["vas_summary_report"]["reportdate"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["Clicks"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["cgok"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["Activation"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["activationrevenue"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["callback"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["parking"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["parkingrevenue"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["renewal"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["renewalrevenue"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["totalrevenue"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["parkinginqueue"];?></td>
						<td><?php echo $categoriesRec["vas_summary_report"]["deactivate"];?></td>
						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } }?>
	<!--list view section end here-->
</div>


<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    
	$("#searchRec").click(function(){
		document.searchCategory.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortCategory.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortCategory.submit();
	
	})
	
})
</script>

