<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		$("#datepicker2").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchCategory" id="searchCategory" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list right">  
                            <li>Start</li>						
                            <li>
							<?php $prevDate = date('Y-m-d', strtotime(' -1 day')); ?>
							<input type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo $prevDate; } ?>" />
							<!---<input placeholder="startDate" id="searchString" name="startDate" type="date" class="validate" max="<?php echo $prevDate;?>" required>-->
							</li>
							<li>End</li>	
                            <li>
							<input type="text" id="datepicker2" name="endDate" id="startDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo $prevDate; } ?>" />
							<!--<input placeholder="endDate" id="searchString" name="endDate" type="date" class="validate" max="<?php echo $prevDate;?>" required>--->
							</li>	
                            <li>
                            <select name="biller_id" id="biller_id">
								<option value="vf" <?php if($_GET["biller_id"] == "vf"){ echo "selected"; } ?>>Vodafone </option>
								<option value="tt" <?php if($_GET["biller_id"] == "tt"){ echo "selected"; } ?>>Tata </option>
								<option value="ia" <?php if($_GET["biller_id"] == "ia"){ echo "selected"; } ?>>Idea </option>
								<option value="at" <?php if($_GET["biller_id"] == "at"){ echo "selected"; } ?>>Airtel </option>
								<option value="rl" <?php if($_GET["biller_id"] == "rl"){ echo "selected"; } ?>>Reliance </option>
                            </select>
                            </li>
                            <li>
                            <select name="publisher" id="publisher">
							<option value="" >Select Service</option>
				            <option value="bnama" <?php if($_GET["publisher"] == "bnama"){ echo "selected"; } ?>>Bhojpurinama</option>
				            <option value="filmy" <?php if($_GET["publisher"] == "filmy"){ echo "selected"; } ?>>filmy</option>
                            <option value="mini" <?php if($_GET["publisher"] == "mini"){ echo "selected"; } ?>>mini</option>
				            <option value="vfcomics" <?php if($_GET["publisher"] == "vfcomics"){ echo "selected"; } ?>>vfcomics</option>
				            <option value="mtoons" <?php if($_GET["publisher"] == "mtoons"){ echo "selected"; } ?>>mtoons</option>
				            <option value="firstcut" <?php if($_GET["publisher"] == "firstcut"){ echo "selected"; } ?>>Firstcut</option>
                            
                            </select>
                            </li>							
							<li>
								<!---<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>--->
								<input type="submit" name="search" class="waves-effect waves-light btn red darken-1" value="search" style="padding:10px 10px !important;">
							</li>
							<li>
							<input type="submit" name="download" class="waves-effect waves-light btn red darken-1" value="download" style="padding:10px 10px !important;">
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<!--<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>-->
		</div>
		<form name="sortCategory" id="sortCategory" action="" method="get">
			<div class = "col s6 small-filteration">
				<!---<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Active</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>
				</ul>--->
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    
	<!--List view seection strat here-->
	<?php if(!empty($categories)){
	//echo "<pre>";
	//print_r($categories);
	//die;
	?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped">                   
				<thead>  
                <tr class ="red lighten-5">
				   <th colspan = "14">
				   <?php 
				   if(!empty($_GET["biller_id"]))
	               {
	                  if($_GET["biller_id"] == 'vf')
		              {
					     echo "Vodafone Detail Report";				       
		              }
					  else if($_GET["biller_id"] == 'tt')
					  {
					     echo "Tata Detail Report";
					  }
					  else if($_GET["biller_id"] == 'ia')
					  {
					     echo "Idea Detail Report";
					  }
					  else if($_GET["biller_id"] == 'at')
					  {
					     echo "Airtel Detail Report";
					  }
					  else if($_GET["biller_id"] == 'rl')
					  {
					     echo "Reliance Detail Report";
					  }
		              else 
		              {
		                 echo "Detail Report";
		              }
	               }
				   ?>
				   </th>
                </tr>				
					<tr class ="red lighten-5">
						<th>Date</th>
						<th>Clicks</th>
						<th>Mobile Not Found</th>
						<th>CG OK</th>
						<th>Activation</th>
						<th>Activation Revenue</th>
						<th>Call Back</th>
						<th>Consent</th>
						<th>Campaign Name</th>
						<th>Pack Id</th>
						<th>Operator</th>
						<th>Service</th>
						<th>Interface</th>
						<th>Publisher</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($categories as $categoriesRec)
					{ 				?>
						<tr>
						<td><?php echo $categoriesRec["vas_detail_report"]["reportdate"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["Clicks"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["mobile_not_found"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["cgok"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["Activation"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["activationrevenue"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["callback"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["consent"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["campaign_name"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["pack_id"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["operator"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["service"];?></td>
						<td><?php echo $categoriesRec["vas_detail_report"]["interface"];?></td>
						<td><?php 
						$pub = explode("_", $categoriesRec["vas_detail_report"]["interface"]);
						echo $pub[0];?></td>
						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
</div>
<?php if((!empty($categories)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/reports/paytm_user_report";
	
	if(@$_GET["startDate"] !=""){
		$startDate = @$_GET["startDate"];
	} else {
		$startDate = "";
	}
	if(@$_GET["endDate"] !=""){
		$endDate = @$_GET["endDate"];
	} else {
		$endDate = "";
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&search=search'; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&startDate='.@$startDate.'&endDate='.@$endDate.'&search=search'; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&search=search'; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    
	$("#searchRec").click(function(){
		document.searchCategory.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortCategory.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortCategory.submit();
	
	})
	
})
</script>

