
<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		
	});

</script>
<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> assignment </i>
                        </div>
                        <div class = "card-content">
                            <h3>Daily Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9">
			    <form name="hourlyReport" id="hourlyReport" method="get" action="<?php echo BASE_URL; ?>/reports/hourlyreport">
                  <ul class = "select-list right">
                     
                      <li>
                        <select name="biller_id" id="biller_id">
                            
                            <?php foreach($operator as $operatorRec){ ?>
								<option value="<?php echo $operatorRec["Operator"]["slug"]; ?>" <?php if((@$_POST['biller_id'] == $operatorRec["Operator"]["slug"]) ||(@$_GET['biller_id'] == $operatorRec["Operator"]["slug"])){ echo "selected"; } ?>><?php echo $operatorRec["Operator"]["name"] ?></option>
							<?php } ?>
                        </select>
                      </li>
                      <li>
                        <select name="publisher" id="publisher">
							<!--<option value="">Select</option>-->
							<?php foreach($project as $projectRec){ ?>
								<option value="<?php echo $projectRec["Project"]["slug"]; ?>" <?php if((@$_POST['publisher'] == $projectRec["Project"]["slug"]) ||(@$_GET['publisher'] == $projectRec["Project"]["slug"])){ echo "selected"; } ?>><?php echo $projectRec["Project"]["name"] ?></option>
							<?php } ?>
                        </select>
                      </li>
                      <li>
                        <a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
                        
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <div class = "row">
                  <div class = "col s6">
                      <!--<ul class = "view-in-listing">
                        <li>
                          <a href="daily-report.html">
                            <i class = "material-icons grey-text">format_list_bulleted</i>
                          </a>
                        </li>
                        <li>
                          <a href="view-graph.html" class = "active">
                            <i class = "material-icons grey-text">view_module</i>
                          </a>
                        </li>                       
                      </ul>-->
                  </div>
                  <div class = "col s6 small-filteration">
                      <!--<ul class = "right">                       
                        <li>                                          
                          <select>
                            <option value="" disabled selected>Sort By</option>
                            <option value="1">View</option>
                            <option value="2">Performance</option>
                            <option value="3">Option 3</option>
                          </select> 
                        </li>
                        <li>                                         
                          <select>
                            <option value="" disabled selected>Filter By</option>
                            <option value="1">Submitted For Approval</option>
                            <option value="2">Rejected</option>
                            <option value="3">Option 3</option>
                          </select>
                        </li>
                      </ul>-->
                  </div>
              </div>
              
              
              <table class = "bordered centered">  
			
			<tbody>
				<?php if(empty($newHourReport)){ ?>
					<tr>
						<td colspan="14" class="noRecordMsg">
						 Sorry!! no record found.
						</td>
					</tr>
				<?php } else {?>
					<thead>
						<tr>
							<th id="par" colspan="5">Subscription Count</th>
						</tr>
					</thead>
					<tr>
						<td>Hour</td>
						<?php foreach ($date as  $val) {?>
							<td><?php echo date('d-m-Y', strtotime($val)); ?></td>
						<?php } ?>
					</tr>
					<tr> 
						<?php
						ksort($newHourReport);
						$i=0;
						foreach($newHourReport as $hour=>$res) { 
						?>
						<td class="data-right"><?php echo $hour; ?></td>
						<?php foreach($date as $val1)
						{ 
						?>

						<td class="data-right"><?php 
						if(isset($newHourReport[$hour][$val1]['subscription_count1'])) 
						{
						echo $subscount1=$newHourReport[$hour][$val1]['subscription_count1']; 
						}
						else{
						echo 0;
						}
						?></td>

						<?php
						$i++;
						}
						?>
					</tr>
					<?php }	 ?>
					<thead>
						<tr>
							<th id="par" colspan="5">Subscription Sum</th>
						</tr>
					</thead>
					<tr>
						<td>Hour</td>
						<?php foreach ($date as  $val) {?>
							<td><?php echo $val; ?></td>
						<?php } ?>
					</tr>
					<tr> 
						<?php	
						$i=0;						
						foreach($newHourReport as $hour=>$res) { 
						?>
						<td class="data-right"><?php echo $hour; ?></td>
						<?php foreach($date as $val1)
						{ 
						?>
						<td class="data-right"><?php 
						if(isset($newHourReport[$hour][$val1]['subscription_sum1'])){ 

						echo round($newHourReport[$hour][$val1]['subscription_sum1'],2); 
						}
						else{
						echo "0";
						}
						?></td>
						<?php
						$i++;
						} ?>
					</tr>
					<?php }  ?>
					<thead>
						<tr>
							<th id="par" colspan="5">Renewal Count</th>
						</tr>
					</thead>
					<tr>
						<td>Hour</td>
						<?php foreach ($date as  $val) {?>
							<td><?php echo $val; ?></td>
						<?php } ?>
					</tr>
					<tr> 
						<?php
									
						foreach($newHourReport as $hour=>$res) { 
						?>
						<td class="data-right"><?php echo $hour; ?></td>
						<?php foreach($date as $val1)
						{   
						?>
						<td class="data-right"><?php if(isset($newHourReport[$hour][$val1]['renewal_count1'])) {

						echo  $newHourReport[$hour][$val1]['renewal_count1'];

						  }								
						else{
						echo "0";
						}								
						?></td>
						<?php

						}
						?>
					</tr>
					<?php } ?>
					<thead>
						<tr>
							<th id="par" colspan="5">Renewal Sum</th>
						</tr>
					</thead>
					<tr>
						<td>Hour</td>
						<?php foreach ($date as  $val) {?>
							<td><?php echo $val; ?></td>
						<?php } ?>
					</tr>
					<tr> 
					<?php	

					foreach($newHourReport as $hour=>$res) { 
					?>
					<td class="data-right"><?php echo $hour; ?></td>
					<?php foreach($date as $val1)
					{

					?>
					<td class="data-right">
					<?php  if(isset($newHourReport[$hour][$val1]['renewal_sum1'])) {  

					echo round($newHourReport[$hour][$val1]['renewal_sum1'],2); 
					}
					else{
					echo '0';

					}
					?></td>
					<?php

					}
					?>
					</tr>
					<?php } ?>
					
				<?php } ?>
			</tbody>
		</table>
	       
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
		<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.hourlyReport.submit();
	})
})
</script>