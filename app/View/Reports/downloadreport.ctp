<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
		$("#datepicker1").datepicker({maxDate: "-1d", "dateFormat":"yy-mm-dd"});
	});

</script>
 <div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> file_download </i>
                        </div>
                        <div class = "card-content">
                            <h3>Download Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9">
			   <form class="form-inline" role="form" name="downloadReport" id="downloadReport" action="<?php echo BASE_URL; ?>/reports/downloadreport" method="get">
                    
                  <ul class = "select-list right download-list">
                      <li>
                       
					   <input placeholder = "From: " type="text" id="datepicker" name="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>" />
                       <!--<i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                       
					   <input placeholder = "To: " type="text" id="datepicker1" name="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-1 day")); } ?>">
                       <!-- <i class = "material-icons">event_note</i> -->
                      </li>
                      <li>
                        <select name="user_id" id="user_id">
                                <option value="all">All</option>
                                <?php foreach($userRec as $userRecs){ ?>
                                    <option value="<?php echo $userRecs["User"]["id"]; ?>" <?php if((@$_POST['user_id'] == $userRecs["User"]["id"]) ||(@$_GET['user_id'] == $userRecs["User"]["id"])){ echo "selected"; } ?>><?php echo ucfirst(strtolower(trim($userRecs["User"]["name"]))); ?></option>
                                <?php } ?>
                                </select>
                      </li>
                      <li>
                        <select name="biller_id" id="biller_id">
                                <!--option value="">Select</option>-->
                                <?php foreach($operator as $operatorRec){ ?>
                                    <option value="<?php echo $operatorRec["Operator"]["slug"]; ?>" <?php if((@$_POST['biller_id'] == $operatorRec["Operator"]["slug"]) ||(@$_GET['biller_id'] == $operatorRec["Operator"]["slug"])){ echo "selected"; } ?>><?php echo $operatorRec["Operator"]["name"] ?></option>
                                <?php } ?>
                                </select>
                      </li>
                       
                      <li>
                        <select name="publisher" id="publisher">
                                <!--<option value="">Select</option>-->
                                <?php foreach($project as $projectRec){ ?>
                                    <option value="<?php echo $projectRec["Project"]["slug"]; ?>" <?php if((@$_POST['publisher'] == $projectRec["Project"]["slug"]) ||(@$_GET['publisher'] == $projectRec["Project"]["slug"])){ echo "selected"; } ?>><?php echo $projectRec["Project"]["name"] ?></option>
                                <?php } ?>
                                </select>
                      </li>
                      <li>
                        <a class="waves-effect waves-light btn red darken-1"  id="submitForm">Download</a>
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <table class = "bordered centered">                   
                  <thead>                    
                    <tr class ="red lighten-5">
                        <th>Content Id</th>
					<th>Content name</th>
					<th>Operator</th>
					<th>Publisher</th>
					<th>Download Status</th>
					<th>Total User Count</th>
					<th>Unique User Count</th>
					<th>Total Content Count</th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php if(empty($downloadRec)){ ?>
					<tr>
						<td colspan="14" class="noRecordMsg" style="text-align:center;">
						 Sorry!! no record found.
						</td>
					</tr>
				<?php } else {
					foreach($downloadRec as $key=>$val){
				?>
					<tr>
						
						<td class="data-right"><?php echo $val["Downloads"]["content_id"]; ?></td>
						<td class="data-left"><?php echo ucfirst(strtolower(trim($contentname[$val["Downloads"]["content_id"]]))); ?></td>
						<td class="data-left"><?php if(isset($val["Downloads"]["biller_id"])) { echo $val["Downloads"]["biller_id"]; } else { echo $_GET["biller_id"]; } ?></td>
						<td class="data-left"><?php if(isset($val["Downloads"]["publisher"])) { echo $val["Downloads"]["publisher"]; } else { echo $_GET["publisher"]; } ?></td>
						<td class="data-left"><?php if(isset($val["Downloads"]["download_status"])) { echo $val["Downloads"]["download_status"]; } else { echo "DONE"; } ?></td>
						<td class="data-right"><?php echo $val[0]["UserCount"]; ?></td>
						<td class="data-right"><?php echo $val[0]["UniqueUserCount"]; ?></td>
						<td class="data-right"><?php echo $val[0]["ContentCount"]; ?></td>
							
					</tr>
				<?php } ?>
				<?php } ?>
                  </tbody>
              </table>
                  <?php if((!empty($downloadRec)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/reports/downloadreport";
	$Nav="";
	$startDate = "";
	$endDate = "";
	$biller_id = "";
	$publisher="";
	if(@$_GET["biller_id"] !=""){
		$biller_id = @$_GET["biller_id"];
	} else {
		$biller_id = "vf";
	}
	
	if(@$_GET["startDate"] !=""){
		$startDate = @$_GET["startDate"];
	} else {
		$startDate = date('Y-m-d',strtotime('yesterday'));
	}
	
	if(@$_GET["endDate"] !=""){
		$endDate = @$_GET["endDate"];
	} else {
		$endDate = date('Y-m-d',strtotime('yesterday'));
	}
	
	
	if(@$_GET["publisher"] !=""){
		$publisher = @$_GET["publisher"];
	} else {
		$publisher = "bnama";
	}

	if(!isset($_GET["page"]))
	{
		$_GET["page"]=1;
	}
	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&biller_id='.@$biller_id.'&publisher='.@$publisher; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&startDate='.@$startDate.'&endDate='.@$endDate.'&biller_id='.@$biller_id.'&publisher='.@$publisher; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&startDate='.@$startDate.'&endDate='.@$endDate.'&biller_id='.@$biller_id.'&publisher='.@$publisher; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>
          </div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
          <!-- end of page wrapper div -->
		  <?php echo $this->element('flashmessage'); ?>
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.downloadReport.submit();
	})
})
</script>			  
		  