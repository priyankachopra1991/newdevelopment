<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
			<div class = "col s12">
			<form class = "edit-form"  method="post" action="<?php echo BASE_URL."/referral/notification"; ?>">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Send Notification</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
						        <input type="submit" class="waves-effect waves-light btn blue darken-1" />
							    <a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class = "row">
									<div class="input-field col s6">
								    <input type="text" name="message" id="message" value="" class="validate" Placeholder="Enter Message" />
									</div>
									<div class="input-field col s6">
										<!---<label style="font-size:18px;">Choose Image</label>--->
								        <div class="file-field input-field">
                                          <div class="btn right red darken-1">
                                            <span>Browse</span>
                                            <input type="file" name = "image" id="image" />
                                          </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text">
                                        </div> 
                                        <span class = "spacer"></span>                                   
                                    </div>
									</div>
								</div>                    
							</div>                       
						</div>
					<!-- end of row div -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$('#channel_id').material_select('destroy');
	$("#resetForm").click(function(){
	    $("#editUser")[0].reset();
	})
	$("#submitForm, #next").click(function(){
	
	    //$(".errorMsgFrm").css("display","block");
		//var emailRegex = /^[a-z_A-Z0-9\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		//var mobileRegex = /^[7-9][0-9]{9}$/;
		//var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var youtube_channel_id = $.trim($('#youtube_channel_id').val());
		var email = $.trim($('#email').val());
		var mobile_number = $.trim($('#mobile_number').val());
		var company = $.trim($('#company').val());
		
		if(youtube_channel_id ==""){
		    $('#youtube_channel_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter youtube channel id.");
			return false;
		}
		
		
		if( $('#country_code').val() == "") {
		    $('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if( $('#subcategory_id').val() == "") {
		    $('#subcategory_id').focus() ;
		    $(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select subcategory.");
			return false;
		}
		
		if( $('#channel_id').val() == "") {
		    $('#channel_id').focus() ;
		    $(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select channel.");
			return false;
		}
		
	   document.youtubeAdd.submit();
	})
})
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function getchannel(country_code)	
{
	$('select').material_select();
    var userId = '<?php echo $this->Session->read('User.id'); ?>';
	$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/channellistbycountry.php',
			'data':'country_code='+country_code+'&userId='+userId,
			'success':function(resp){
				//alert(resp);
				//$('#channel_id').material_select('destroy');
				//$('#channel_id').html(resp);
				var $selectDropdown = 
				$("#channel_id")
				.empty()
				.html('');
					
				$selectDropdown.append(resp);
				$selectDropdown.trigger('contentChanged');
			}
	});
}

$('select').on('contentChanged', function() {
    // re-initialize (update)
    $(this).material_select();
});
  
$(document).ready(function() {
  $('select').material_select();
  var app_slug = 'firstcut';
  $.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
			//$('#subcategory_id').html(resp);
			$selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			 $selectDropdown.append(resp);
			 $('select').material_select();
			
		}
	});
	$("#submitForm").hide();
});
function valueChanged()
{
   if($('#allContent').is(":checked")){   
        $("#next").hide();
		$("#submitForm").show();
	} else {
    	$("#submitForm").hide();
	    $("#next").show();
	}	
}

</script>

