<div class = "table-format-data white-bg">
	<div class = "row">
		<div class = "col s12">
			<div class = "detail-title-section">
				<div class = "card card-stats">
					<div class = "card-header" data-background-color = "red">
						<i class = "material-icons"> data_usage </i>
					</div>
					<div class = "card-content">
						<h3>Referral Count Report</h3>
					</div>                        
				</div>
				<!-- end of card div -->
			</div>
		<!-- end of detail-title-section -->
		</div>
		
	</div>	
	<table class = "bordered centered">                   
		<thead>  
			<tr class ="red lighten-5">
				<th>Msisdn</th>
				<th>View</th>
				<th>Referral</th>
			</tr>
		</thead>
		<tbody>
		<?php if(empty($userDetailArr)){ ?>
			<tr>
				<td colspan="3" class="noRecordMsg">
					Sorry!! No record found.
				</td>
			</tr>
		<?php } else {?>
			<?php //foreach($userDetailArr as $userDetailArrRec){ ?>
				<tr>
					<td><?php echo $userDetailArr["msisdn"]; ?></td>
					<td><?php echo $userDetailArr["userView"]; ?></td>
					<td><?php echo $userDetailArr["registerUser"]; ?></td>
				</tr>
			<?php //} ?>
		<?php } ?>
		</tbody>
	</table>
</div>