<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-0d", "dateFormat":"yy-mm-dd"});
		$("#datepicker1").datepicker({maxDate: "-0d", "dateFormat":"yy-mm-dd"});
	});

</script>

<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> data_usage </i>
                        </div>
                        <div class = "card-content">
                            <h3>Referral Count Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div>
               <div class = "col s9 right">
			   <form name="countReport" id="countReport" method="get" action="<?php echo BASE_URL; ?>/referral/countreport">
                  <ul class = "select-list right">
				      <li>
                        <select name="dataFor" id="dataFor">
							<option value="registerUser" <?php if(@$_GET['dataFor'] == "registerUser"){ echo "selected"; } ?>>Register User</option>
							<option value="view" <?php if(@$_GET['dataFor'] == "view"){ echo "selected"; } ?>>View</option>
							<option value="referral" <?php if(@$_GET['dataFor'] == "referral"){ echo "selected"; } ?>>Referral</option>
							<option value="subscriber" <?php if(@$_GET['dataFor'] == "subscriber"){ echo "selected"; } ?>>Subscriber</option>
						</select>	
                      </li>
                      <li>
						   <input placeholder = "From: " type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-0 day")); } ?>"  />
						   
						   <i class = "material-icons">event_note</i>
                      </li>
                      <li>
							<input placeholder = "To: " type="text" id="datepicker1" name="endDate" id="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-0 day")); } ?>">
							<i class = "material-icons">event_note</i>
                      </li>                      
                      <li>
                        <a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
                      </li>
                  </ul>
				  </form>
               </div>
              </div>
              <?php if(!isset($_GET["dataFor"]) || ((!empty($_GET["dataFor"])) && ($_GET["dataFor"] == "registerUser"))){ ?>
				<table class = "bordered centered">        
					<!--<thead>                    
						<tr class = "red lighten-5">
							<th class = "red lighten-4"></th>
							<th>Count</th>
						</tr>
					</thead>-->
					<tbody>
						<tr>
						    <td class = "red lighten-4">Register User Count</th>   
							<td><?php echo empty($registerData[0][0]["countRec"]) ? '0' : $registerData[0][0]["countRec"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
					</tbody>
					
				</table>
			  <?php } ?>
			  
			  <?php if((!empty($_GET["dataFor"])) && ($_GET["dataFor"] == "view")){ ?>
				<table class = "bordered centered">                   
					<thead>  
						<tr class ="red lighten-5">
							<th>Msisdn</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
                    <?php if(empty($userViewData)){ ?>
						<tr>
							<td colspan="3" class="noRecordMsg">
								Sorry!! No record found.
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($userViewData as $userViewDataRec){ ?>
							<tr>
								<td><?php echo $userViewDataRec["t1"]["msisdn"]; ?></td>
								<td><?php echo $userViewDataRec[0]["countRec"]; ?></td>
							</tr>
						<?php } ?>
					<?php } ?>
					</tbody>
				</table>
			  <?php } ?>
			  
			   <?php if((!empty($_GET["dataFor"])) && ($_GET["dataFor"] == "referral")){ ?>
					<table class = "bordered centered">                   
					<thead>  
						<tr class ="red lighten-5">
							<th>Msisdn</th>
							<th>Referral Count</th>
						</tr>
					</thead>
					<tbody>
                    <?php if(empty($referralData)){ ?>
						<tr>
							<td colspan="3" class="noRecordMsg">
								Sorry!! No record found.
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($referralData as $referralDataRec){ ?>
							<tr>
								<td><?php echo $referralDataRec["t1"]["msisdn"]; ?></td>
								<td><?php echo $referralDataRec[0]["referredBy"]; ?></td>
							</tr>
						<?php } ?>
					<?php } ?>
					</tbody>
				</table>
			  <?php } ?>
			  
			   <?php if((!empty($_GET["dataFor"])) && ($_GET["dataFor"] == "subscriber")){ ?>
			    <div class = "col s12 listView">
					<table class = "bordered centered">                   
					<thead>  
						<tr class ="red lighten-5">
							<th>Msisdn</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php if(empty($subscriberData)){ ?>
						<tr>
							<td colspan="3" class="noRecordMsg">
								Sorry!! No record found.
							</td>
						</tr>
					<?php } else {?>
						<?php foreach($subscriberData as $subscriberDataRec){ ?>
							<tr>
								<td><?php echo $subscriberDataRec["t1"]["msisdn"]; ?></td>
								<td>
									<a href = "<?php echo BASE_URL."/referral/detailcountreport"; ?>?user_id=<?php echo  $subscriberDataRec['t1']['user_id']; ?>&startDate=<?php echo  $_GET["startDate"]; ?>&endDate=<?php echo  $_GET["endDate"]; ?>" class = "btn red lighten-1">View Detail</a>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
					</tbody>
				</table>
				
				<?php 
					if((!empty($subscriberData)) && ($numOfPage > 1)){ 
					?>
						<?php
						$pageName = BASE_URL."/referral/countreport";
						
						if(@$_GET["dataFor"] !=""){
							$dataFor = @$_GET["dataFor"];
						} else {
							$dataFor = "";
						}
						if(@$_GET["startDate"] !=""){
							$startDate = @$_GET["startDate"];
						} else {
							$startDate = "";
						}
						if(@$_GET["endDate"] !=""){
							$endDate = @$_GET["endDate"];
						} else {
							$endDate = "";
						}
						
						if(!isset($_GET["page"]))
						{
							$_GET["page"] = 1;
						}
						$page = $pageNum - 1;
						?>
						<ul class="pagination">
							<?php if($pageNum > 1) {?>
								<li><a href="<?php echo $pageName.'/?page='.$page.'&dataFor='.@$dataFor.'&startDate='.@$startDate.'&endDate='.@$endDate; ?>"><i class="material-icons">chevron_left</i></a></li>
							<?php } else { ?>
								<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
							<?php } ?>
							
							<?php 
							if($numOfPage >3){
								if($_GET["page"] >=$numOfPage){
									$minVal = $_GET["page"]-2;
									$maxVal = $_GET["page"];
								} else {
									$minVal = $_GET["page"];
									$maxVal = $_GET["page"]+2;
								}
							} else {
								$minVal = 1;
								$maxVal = $numOfPage;
							}
							
							for($i= $minVal; $i<=$maxVal; $i++)
							{
							?>
								<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&dataFor='.@$dataFor.'&startDate='.@$startDate.'&endDate='.@$endDate; ?>"><?php echo $i; ?></a></li>
							<?php } 
							$page = $pageNum + 1;
							?>
							<?php if($numOfPage == $pageNum){ ?>
								<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
							<?php } else { ?>
								<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&dataFor='.@$dataFor.'&startDate='.@$startDate.'&endDate='.@$endDate; ?>"><i class="material-icons">chevron_right</i></a></li>
							<?php } ?>
						</ul>
					<?php } ?>
				 
				</div>
			  <?php } ?>
			</div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
		
	</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.countReport.submit();
	})
})
</script>