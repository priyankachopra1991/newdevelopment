<script>
	$(function() {
		
		$("#datepicker").datepicker({maxDate: "-0d", "dateFormat":"yy-mm-dd"});
		$("#datepicker1").datepicker({maxDate: "-0d", "dateFormat":"yy-mm-dd"});
	});

</script>

<div class = "table-format-data white-bg">
              <div class = "row">
                <div class = "col s3">
                    <div class = "detail-title-section">
                      <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> data_usage </i>
                        </div>
                        <div class = "card-content">
                            <h3>Referral Count Report</h3>
                        </div>                        
                      </div>
                      <!-- end of card div -->
                  </div>
                    <!-- end of detail-title-section -->
                </div><br/>
               <div class = "col s12">
			   <form name="countReport" id="countReport" method="get" action="<?php echo BASE_URL; ?>/referral/countreportdata">
			   
                  <ul class = "select-list right">
					  <!--<li>
                        <select name="dataFor" id="dataFor">
							<option value="registerUser" <?php if(@$_GET['dataFor'] == "registerUser"){ echo "selected"; } ?>>Register User</option>
							<option value="view" <?php if(@$_GET['dataFor'] == "view"){ echo "selected"; } ?>>View</option>
							<option value="referral" <?php if(@$_GET['dataFor'] == "referral"){ echo "selected"; } ?>>Referral</option>
							<option value="subscriber" <?php if(@$_GET['dataFor'] == "subscriber"){ echo "selected"; } ?>>Subscriber</option>
						</select>	
                      </li>-->
					  <li>
						<input required type="checkbox" id="terms"  class="filled-in" name="register" style="display:block" value="1" <?php if(@$_GET["register"] == 1){ echo "checked=checked"; } ?> /> 
					  </li>
					  <li>
						Register
					  </li>
					  <li>
						<input required type="checkbox" id="terms"  class="filled-in" name="subscriber" value="1" style="display:block" <?php if(@$_GET["subscriber"] == 1){ echo "checked=checked"; } ?> /> 
					  </li>
					  <li>
						Subscriber
					  </li>
					  <li>
							<input required type="checkbox" id="terms"  class="filled-in" name="view" value="1" style="display:block" <?php if(@$_GET["view"] == 1){ echo "checked=checked"; } ?> /> 
					  </li>
					  <li>
						View
					  </li>
					  <li>
						<input required type="checkbox" id="terms"  class="filled-in" name="referral" value="1"  style="display:block" <?php if(@$_GET["referral"] == 1){ echo "checked=checked"; } ?> /> 
					  </li>
					   <li>
						Referral
					  </li>
					  <li>
						   <input placeholder = "From: " type="text" id="datepicker" name="startDate" id="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-0 day")); } ?>"  />
						   
						   <i class = "material-icons">event_note</i>
                      </li>
                      <li>
							<input placeholder = "To: " type="text" id="datepicker1" name="endDate" id="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-0 day")); } ?>">
							<i class = "material-icons">event_note</i>
                      </li> 
					 
					  
                      <li>
                        <a class="waves-effect waves-light btn red darken-1" id="submitForm">Search</a>
					</li>
                  </ul>
				  </form>
               </div>
			   <?php if(!empty($userData)){ ?>
			    <div class = "col s9 right">
					<form name="countReport1" id="countReport1" method="post" action="<?php echo BASE_URL; ?>/referral/givenUserPointByAdmin">
						<input type="hidden" name="subscriber" value="<?php if(!empty($_GET["subscriber"])){ echo $_GET["subscriber"]; } ?>" />
						
						<input type="hidden" name="view" value="<?php if(!empty($_GET["view"])){ echo $_GET["view"]; } ?>" />
						
						<input type="hidden" name="referral" value="<?php if(!empty($_GET["referral"])){ echo $_GET["referral"]; } ?>" />
						
						<input type="hidden" name="startDate" value="<?php if(!empty($_GET["startDate"])){ echo $_GET["startDate"]; } else { echo date('Y-m-d', strtotime("-0 day")); } ?>" />
						<input type="hidden" name="endDate" value="<?php if(!empty($_GET["endDate"])){ echo $_GET["endDate"]; } else { echo date('Y-m-d', strtotime("-0 day")); } ?>" />
						<ul class = "select-list right">
							<div class="errorMsgFrm" style="display:none;"></div>
							<li>
								<input type="text" name="no_of_user" id="no_of_user" placeholder="No. Of Users" onkeypress="return isNumber(event)" />
							</li> 
							<li>
								<input type="text" name="points_per_user" id="points_per_user" placeholder="Points Per User" onkeypress="return isNumber(event)" />
							</li>
							<li>
								<a class="waves-effect waves-light btn red darken-1" id="submitPoints">Submit</a>
							</li>
						</ul>
					</form>
			    </div>
				<?php } ?>
              </div>
            
			  <table class = "bordered centered">                   
					<thead>  
						<tr class ="red lighten-5">
							<th>Msisdn</th>
							<th>Referral Count</th>
							<th>View Count</th>
						</tr>
					</thead>
					<tbody>
                    <?php if(empty($userData)){ ?>
						<tr>
							<td colspan="4" class="noRecordMsg">
								Sorry!! No record found.
							</td>
						</tr>
					<?php } else { ?>
						<?php foreach($userData as $userDataRec){ ?>
							<tr>
								<td><?php echo $userDataRec["t1"]["msisdn"]; ?></td>
								<td><?php echo empty($userDataRec["t1"]["referral_count"]) ? '0' : $userDataRec["t1"]["referral_count"]; ?></td>
								<td><?php echo empty($userDataRec["t1"]["view_count"]) ? '0' : $userDataRec["t1"]["view_count"]; ?></td>
							</tr>
						<?php } ?>
					<?php } ?>
					</tbody>
				</table>
			</div>
  
          </div>
            <!-- end of content wrapper div -->
        </div>
		
	</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#submitForm").click(function(){
		document.countReport.submit();
	})
})
</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#submitPoints").click(function(){
	
	    var no_of_user = $.trim($('#no_of_user').val());
	    var points_per_user = $.trim($('#points_per_user').val());
		if(no_of_user ==""){
		    $('#no_of_user').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the number of user.");
			return false;
		}

		if(no_of_user <=0){
		    $('#no_of_user').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the number of user greater than 0.");
			return false;
		}
		
		if(points_per_user ==""){
		    $('#points_per_user').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the points per user.");
			return false;
		}
		
		if(points_per_user <= 0){
		    $('#points_per_user').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the points per user greater than 0.");
			return false;
		}
		document.countReport1.submit();
	})
})
</script>



<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
