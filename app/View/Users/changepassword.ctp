<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form change-password" name="changePassword" id="changePassword" method="post" action="<?php echo BASE_URL."/users/changepassword/"; ?>">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Change Password</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
							<div class="errorMsgFrm" style="display:none;"></div>
								<div class = "row">
									<div class="input-field col s6">
										<input placeholder="Email" id="email" name="email" type="text" class="validate" value="<?php echo $this->Session->read('User.email'); ?>" readonly>
									</div>
									<div class="input-field col s6">
										<input placeholder="Old Password" id="oldpassword" name="oldpassword" type="password" class="validate">
									</div>
									<div class="input-field col s6">
										<input placeholder="New Password" id="newpassword" name="newpassword"  type="password" class="validate">
									</div>
									<div class="input-field col s6">
										<input placeholder="Confirm Password" id="cnewpassword" name="cnewpassword"  type="password" class="validate">
									</div>
								</div>                    
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $("#changePassword")[0].reset();
	})
	
	$("#submitForm").click(function(){
		var emailRegex = /^[a-z_A-Z0-9\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var email = $.trim($('#email').val());
		var oldpassword = $.trim($('#oldpassword').val());
		var newpassword = $.trim($('#newpassword').val());
		var cnewpassword = $.trim($('#cnewpassword').val());
		if(email ==""){
		    $('#email').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the email.");
			return false;
		}
		if(!emailRegex.test(email)){
			$('#email').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the valid email.");
			return false;
		}
		if(oldpassword ==""){
		    $('#oldpassword').focus();
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the old password.");
			return false;
		}
		if(newpassword ==""){
		    $('#newpassword').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the new password.");
			return false;
		}
		if(cnewpassword ==""){
		    $('#cnewpassword').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the confirm new password.");
			return false;
		}
		
		if(cnewpassword != newpassword){
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("New password and confirm password not match.");
			return false;
		}
		//return true;
		document.changePassword.submit();
	})
})
</script>	