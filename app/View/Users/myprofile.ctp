<div class = "profile-section">
  <div class = "row">
	  <div class = "col s4">
		 <div class="card">
			<div class="card-image">
			  <img src="<?php echo BASE_URL; ?>/img/profile-bg.jpg">                          
			  <div class = "profile-img"><img src = "<?php echo BASE_URL; ?>/img/user-icon.png" alt = "" /></div>
			</div>
			<div class="card-content">
			  <ul class = "profile-info-list">
				<li>
				  <label>User Id</label>
				  <p><?php echo $user["User"]["id"]; ?></p>
				</li>
				<li>
				  <label>Name</label>
				  <p><?php 
				          if(strlen($user["User"]["name"])>20 && !empty($user["User"]["name"]))
				           {
						      echo substr($user["User"]["name"],0,10);
						   }
						   elseif(!empty($user["User"]["name"]))
						   {
						      echo $user["User"]["name"];
						   }
						   else
						   {
						      echo 'NA';
						   }
					   //echo empty($user["User"]["name"]) ? 'NA' : $user["User"]["name"];
				         ?></p>
				</li>
				<li>
				  <label>Email</label>
				  <p>
				  <?php echo empty($user["User"]["email"]) ? 'NA' : $user["User"]["email"]; ?>
				  </p>
				</li>
				<li>
				  <label>Phone No.</label>
				  <p>
				  <?php echo empty($user["User"]["phone_number"]) ? 'NA' : "+91 ".$user["User"]["phone_number"]; ?>
				  </p>
				</li>
				<li>
				  <label>User Type</label>
				  <p><?php echo empty($user["User"]["user_type"]) ? 'NA' : $user["User"]["user_type"]; ?></p>
				</li>
				<li>
				  <label>Company</label>
				  <p><?php echo empty($user["User"]["company"]) ? 'NA' : $user["User"]["company"]; ?></p>
				</li>
				<li>
				  <label>Status</label>
				  <p><?php echo ($user["User"]["status"] != 'A') ? 'Inactive' : 'Active'; ?></p>
				</li>
			  </ul>
			</div>
		  </div>
	  </div>
		<!-- end of col s4 div -->
	  <div class = "col s8">
	    <form class="edit-profile-form white-bg" name="updateProfile" id="updateProfile" method="post" action="<?php echo BASE_URL; ?>/users/myprofile/<?php echo $user["User"]["id"]; ?>">
		 <div id="errorMsgFrm" style="display:none;"></div>	
			<div class="row">
			  <div class="input-field col s6">
				<input placeholder = "Name" id="name" name="name" type="text" value="<?php echo $user["User"]["name"]; ?>"  class="validate">
			  </div>
			  <div class="input-field col s6">
				<input placeholder = "Email" id="email" name="email" type="text" value="<?php echo $user["User"]["email"]; ?>"  readonly="readonly" class="validate">
			  </div>
			  <div class="input-field col s6">
				<input placeholder = "Phone Number" id="phone_number" name="phone_number" type="text" value="<?php echo $user["User"]["phone_number"]; ?>" onkeypress="return isNumber(event)" class="validate">
			  </div>
			  <div class="input-field col s6">
				<input placeholder = "Company" id="company" name="company" type="text" value="<?php echo $user["User"]["company"]; ?>" "class="validate">                            
			  </div>   
				<div class = "submit-form-container col s6">                      
				  <button class="btn waves-effect waves-light red darken-1" type="submit" name="action" id="submitMyProfile">Save
				  </button>
				  <!--<button class="btn waves-effect waves-light red darken-1" type="submit" name="action">Delete
				  </button>-->
				</div>
			  
			</div>

	    </form>
	  </div>
		<!-- end of col s8 div -->
  </div>
</div>  
<!-- end of profile-section div -->
<?php echo $this->element('flashmessage'); ?>	

<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>		

<script type="text/javascript">
$(document).ready(function(){
	$("#errorMsgFrm").css("display", "none");
	$("#submitMyProfile").click(function(){
		
		var mobileRegex = /^[7-9][0-9]{9}$/;
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var phone_number = $.trim($('#phone_number').val());
		var company = $.trim($('#company').val());
		
		if(name ==""){
		    $('#name').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the name.");
			return false;
		}
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if(phone_number ==""){
		    $('#phone_number').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the phone.");
			return false;
		}
		if(!mobileRegex.test(sphone)){
			$('#phone_number').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the valid mobile number.");
			return false;
		}
		return true;
	})
})
</script>  