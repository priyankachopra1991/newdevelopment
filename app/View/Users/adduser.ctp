<div class = "table-format-data">
	<div class = "row">
		<div class = "col s8 offset-s2">                     
			<div class = "add-channel-form">
				<div class = "channel-header">
					<h2>Add User</h2> 
					<p>This information will let us know more about your channel</p>
				</div>
				<div class="row">
				
					<div class="col s12">
						<ul class="tabs series-tabs">
							<li class="tab col s6">
							<a class="active" href="#basicinfo" id="tabberDisableBasic">Basic Info</a>
							</li>
							<li class="tab col s6">
							<a class="" href="#identity" id="tabberDisablePermission">Permissions</a>
							</li>
						</ul>
					</div>
					<form name="addUser" id="addUser" action="<?php echo BASE_URL; ?>/users/adduser" method="post" enctype="multipart/form-data">
					
					
					<div id="basicinfo" class="col s12 tab-content">
					
							<div class = "row">
							<div class="errorMsgFrm" style="display:none;"></div>
							<div class="input-field col s6">
							<i class="material-icons prefix">face</i>
							<input id="fname" type="text" name="fname"  class="validate">
							<label for="icon_prefix">First Name</label>
							</div>
							<div class="input-field col s6">
							<i class="material-icons prefix"></i>
							<input  id="lname" type="text" name="lname" class="validate">
							<label for="icon_prefix">Last Name</label>
							</div>
							<div class="input-field col s6">
							<i class="material-icons prefix">email</i> 
							<input id="email" type="text"  name="email" class="validate">
							<label for="icon_prefix">Email</label>
							</div> 
							<div class="input-field col s6">
							<i class="material-icons prefix">call</i> 
							<input id="mobile_number" type="text" name="mobile_number"  onkeypress="return isNumber(event)" class="validate">
							<label for="icon_prefix">Phone</label>
							</div>                                
							</div>                             
						
						<div class="input-field col s12 tabs series-tabs" >
							<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityTab">Next</a>
						</div>
					</div>
					<div id="identity" class="col s12 tab-content">
						<div class = "row">
							 <div class="errorMsgFrm" style="display:none;"></div>
							 <div class="permission-form1">
							    <?php foreach($qryMenuRecs as $qryMenuRec){ ?>
									<p class="col s6">
										<input type="checkbox" class="filled-in checkboxChecked main" id="<?php echo $qryMenuRec["Menu"]["menu_name"]; ?>" name="menuIdArr[]" value="<?php echo $qryMenuRec["Menu"]["menu_id"]; ?>"  />
										<label for="<?php echo $qryMenuRec["Menu"]["menu_name"]; ?>"><?php echo $qryMenuRec["Menu"]["menu_name"]; ?></label>
									</p>
									
								<?php } ?>
								    
							  </div>
							<hr class = "space" /> 
							<div class="input-field col s12">
								<a class="waves-effect waves-light btn left red darken-1" href = "javascript:void(0)" id="identityPrevTab">Previous</a>
								<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="submitForm">Finish</a>
							</div>
						</div>
					</div>
					</form>
				</div>          
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$('#tabberDisablePermission').click(function(){ return false});
	$('#tabberDisableBasic').click(function(){ return false});
	$("#identityTab").click(function(){
		$(".errorMsgFrm").css("display","block");
		var emailRegex = /^[a-z_A-Z0-9\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var mobileRegex = /^[7-9][0-9]{9}$/;
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var fname = $.trim($('#fname').val());
		var lname = $.trim($('#lname').val());
		var email = $.trim($('#email').val());
		var mobile_number = $.trim($('#mobile_number').val());
		
		//alert(email);
		if(fname ==""){
		    $('#fname').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the first name.");
			return false;
		}
		if(!nameRegex.test(fname)){
			$('#fname').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in first name.");
			return false;
		}
		
		if(lname !="" && !nameRegex.test(lname)){
			$('#lname').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in last name.");
			return false;
		}
		if(email ==""){
		    $('#email').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the email.");
			return false;
		}
		if(!emailRegex.test(email)){
			$('#email').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the valid email.");
			return false;
		}
		if(mobile_number ==""){
		    $('#mobile_number').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the phone.");
			return false;
		}
		if(!mobileRegex.test(mobile_number)){
			$('#mobile_number').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the valid mobile number.");
			return false;
		}
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisablePermission").addClass("active");
		$(".indicator").attr("style", "right: 1px; left: 320px;");
		return true;
	
	})

	$("#identityPrevTab").click(function()
	{
		$("#identity").css("display", "none");
		$("#basicinfo").css("display", "block");
		$("#tabberDisablePermission").removeClass("active");
		$("#tabberDisableBasic").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 0px;");
	})
	
	$("#submitForm").click(function(){
	    if(($('input[name="menuIdArr[]"]:checked').length) < 1) {
		    $(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select permission.");
			return false;
		}
		document.addUser.submit();
	})
})


</script>

<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

<script type = "text/javascript" >
history.pushState(null, null, 'adduser');
window.addEventListener('popstate', function(event) {
history.pushState(null, null, 'adduser');
});
</script>
