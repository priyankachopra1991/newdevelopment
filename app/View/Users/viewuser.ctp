<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchUser" id="searchUser" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list right">                           
							<li>
								<select name="searchBy" id="searchBy">
									<option value="">Search By</option>
									<option value="name" <?php if(@$_GET['searchBy'] == "name"){ echo "selected"; } ?>>Name</option>
									<option value="email" <?php if(@$_GET['searchBy'] == "email"){ echo "selected"; } ?>>Email</option>
								</select>
							</li>
							
							<li>
								<div class="input-field">
									<input placeholder="Search" id="searchString" name="searchString" type="text" value="<?php echo @$_GET["searchString"]; ?>" class="validate">
								</div>
							</li>
							<li>
								<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>
		</div>
		<form name="sortUser" id="sortUser" action="" method="get">
			<div class = "col s6 small-filteration">
				<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Approved</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Wating Email Verify</option>
						<option value="B" <?php if(@$_GET['filterBy'] == "B"){ echo "selected"; } ?>>Unapproved</option>
						<option value="C" <?php if(@$_GET['filterBy'] == "C"){ echo "selected"; } ?>>Rejected</option>
						<option value="I" <?php if(@$_GET['filterBy'] == "I"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>
				</ul>
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    <!--Grid view section start here--> 
    <?php if(!empty($users)){ ?>
		<?php foreach($users as $user){ ?>
			<div class = "col s4 gridView" style="display:none;">
				<div class="row">
					<div class="col s12 m12">
						<div class="card view-user-card">
							<div class="card-image">
								<img src="<?php echo BASE_URL; ?>/img/profile-bg.jpg">                          
									<div class = "profile-img"><img src = "<?php echo BASE_URL; ?>/img/user-icon.png" alt = "" />
								</div>
							</div>
							<div class="card-content">
								<ul class = "profile-info-list">
									<li>
										<label>User Id</label>
										<p><?php echo $user["User"]["id"]; ?></p>
									</li>
									<li>
										<label>Name</label>
										<p><?php 		
				                        if(strlen($user["User"]["name"])>20 && !empty($user["User"]["name"]))
				                        {
						                  echo substr($user["User"]["name"],0,10);
										}
						                elseif(!empty($user["User"]["name"]))
						                {
						                  echo $user["User"]["name"];
						                }
										else
						                {
						                  echo 'NA';
										}
										
										//echo empty($user["User"]["name"]) ? 'NA' : $user["User"]["name"]; 
										?></p>
									</li>
									
									<li>
										<label>Email</label>
										<p>
										<?php 		
				                       if(strlen($user["User"]["email"])>20 && !empty($user["User"]["email"]))
				                       {
						                 echo substr($user["User"]["email"],0,10);
						               }
						               elseif(!empty($user["User"]["email"]))
						               {
						                echo $user["User"]["email"];
						               }
						               else
						               {
						                 echo 'NA';
						               }
										//echo empty($user["User"]["email"]) ? 'NA' : $user["User"]["email"]; 
										
										?></p>
									</li>
								
									<li>
										<label>Phone No.</label>
										<p><?php echo empty($user["User"]["phone_number"]) ? 'NA' : "+91 ".$user["User"]["phone_number"]; ?></p>
									</li>
									<li>
										<label>User Type</label>
										<p><?php echo empty($user["User"]["user_type"]) ? 'NA' : $user["User"]["user_type"]; ?></p>
									</li>
									<li>
										<label>Company</label>
										<p><?php 
										if(strlen($user["User"]["company"])>20 && !empty($user["User"]["company"]))
				                        {
						                  echo substr($user["User"]["company"],0,10);
										}
						                elseif(!empty($user["User"]["company"]))
						                {
						                  echo $user["User"]["company"];
						                }
										else
						                {
						                  echo 'NA';
										}
										//echo empty($user["User"]["company"]) ? 'NA' : $user["User"]["company"]; ?></p>
									</li>
									
									<li>
										<label>Status</label>
										<p><?php echo ($user["User"]["status"] != 'A') ? 'Inactive' : 'Active'; ?></p>
									</li>
								</ul>
							</div>
							<?php if($this->Session->read('User.user_type') == 'CPA' || $this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
								<div class="card-action">
									<a class = "orange edit-user" href="<?php echo BASE_URL."/users/edituser/".$user["User"]["id"]; ?>">Edit</a>                           
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				
				<!-- end of card div -->
			</div>
		<?php } ?>
	<?php } else { ?>
		<div class = "col s12 gridView" style="display:none;">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--Grid view section end here-->
	<!--List view seection strat here-->
	<?php if(!empty($users)){ ?>
		<div class = "col s12 listView" style="display:none;">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th>User Id</th>
						<th>Name</th>
						<th>Email</th>
						<th>Phone No.</th>
						<th>User Type</th>
						<th>Status</th>
						<?php if($this->Session->read('User.user_type') == 'CPA' || $this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
							<th>Action</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach($users as $user){ ?>
						<tr>
							<td><?php echo $user["User"]["id"]; ?></td>
							<td><?php echo empty($user["User"]["name"]) ? 'NA' : $user["User"]["name"]; ?></td>
							<td><?php echo empty($user["User"]["email"]) ? 'NA' : $user["User"]["email"]; ?></td>
							<td><?php echo empty($user["User"]["phone_number"]) ? 'NA' : $user["User"]["phone_number"]; ?></td>
							<td><?php echo empty($user["User"]["user_type"]) ? 'NA' : $user["User"]["user_type"]; ?></td>
							<td><?php echo ($user["User"]["status"] != 'A') ? 'Inactive' : 'Active'; ?></td>
							<?php if($this->Session->read('User.user_type') == 'CPA' || $this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
								<td><a href = "<?php echo BASE_URL."/users/edituser/".$user["User"]["id"]; ?>" class = "btn red lighten-1">Edit</a></td>

							<?php } ?>
						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView" style="display:none;">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!-list view section end here-->
</div>
<?php if((!empty($users)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/users/viewuser";
	
	if(@$_GET["searchBy"] !=""){
		$searchBy = @$_GET["searchBy"];
	} else {
		$searchBy = "";
	}
	if(@$_GET["searchString"] !=""){
		$searchString = @$_GET["searchString"];
	} else {
		$searchString = "";
	}
	if(@$_GET["sortBy"] !=""){
		$sortBy = @$_GET["sortBy"];
	} else {
		$sortBy = "";
	}
	
	if(@$_GET["filterBy"] !=""){
		$filterBy = @$_GET["filterBy"];
	} else {
		$filterBy = "";
	}
	
	
	if(@$_GET["selectvalue"] !=""){
		$selectvalue = @$_GET["selectvalue"];
	} else {
		$selectvalue = @$_POST["selectvalue"];
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    //alert($.cookie("userView"));
	if($.cookie("userView") == "listView"){
		$(".gridView").css("display","none");
		$(".listView").css("display","block");
		$("#listViewModule").addClass("iconActive");
		$("#gridViewModule").removeClass("iconActive");
	} else {
		$(".listView").css("display","none");
		$(".gridView").css("display","block");
		$("#gridViewModule").addClass("iconActive");
		$("#listViewModule").removeClass("iconActive");
	}
	$("#listViewModule").click(function(){
		$.cookie("userView", "listView");
		$(".gridView").css("display","none");
		$(".listView").css("display","block");
		$("#listViewModule").addClass("iconActive");
		$("#gridViewModule").removeClass("iconActive");
	})
	$("#gridViewModule").click(function(){
		$.cookie("userView", "gridView");
		$(".listView").css("display","none");
		$(".gridView").css("display","block");
		$("#gridViewModule").addClass("iconActive");
		$("#listViewModule").removeClass("iconActive");
	})
	
	$("#searchRec").click(function(){
		document.searchUser.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortUser.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortUser.submit();
	
	})
	
})
</script>

