<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" name="editUser" id="editUser" method="post" action="<?php echo BASE_URL."/users/edituser/".$user["User"]["id"]; ?>">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit User Detail</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
							<div class="errorMsgFrm" style="display:none;"></div>
								<div class = "row">
									<div class="input-field col s6">
										<input placeholder="Name" id="name" name="name" value="<?php echo $user["User"]["name"]; ?>" type="text" class="validate">
									</div>
									
									<div class="input-field col s6">
										<input placeholder="Email" id="email" type="text" name="email" value="<?php echo $user["User"]["email"]; ?>" readonly="readonly" class="validate">
									</div>
									<div class="input-field col s6">
										<input placeholder="Number" id="mobile_number" name="mobile_number"  value="<?php echo $user["User"]["phone_number"]; ?>" type="text" class="validate"  onkeypress="return isNumber(event)">
									</div>
									<div class="input-field col s6">
										<input placeholder="Company" id="company" name="company"  value="<?php echo $user["User"]["company"]; ?>" type="text" class="validate">
									</div>
									<div class="input-field col s6">
										<select name="status" id="status">
											<option value="">Status</option>
											<option value="A" <?php if($user["User"]["status"] == "A"){ echo "selected"; } ?>>Active</option>
											<option value="I" <?php if($user["User"]["status"] == "I"){ echo "selected"; } ?>>Inctive</option>
											<?php if( $this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
												<option value="D" <?php if($user["User"]["status"] == "D"){ echo "selected"; } ?>>Wating Email Verify</option>
												<option value="B" <?php if($user["User"]["status"] == "B"){ echo "selected"; } ?>>Wating Admin Approval</option>
												<option value="C" <?php if($user["User"]["status"] == "C"){ echo "selected"; } ?>>Rejected</option>
											<?php } ?>
										</select>
									</div>
									<?php $explodeMenuId = @explode(",", $user["User"]["menu_id"]);
									?>
									<!--<div class="input-field col s6">
								    <select name="menuIdArr[]" id="menuIdArr" multiple="multiple">
									<option disabled selected>Permission For</option>
								    <?php foreach($qryMenuRecs as $qryMenuRec){?>
									<option value="<?php echo $qryMenuRec["Menu"]["menu_id"]; ?>" <?php echo (in_array($qryMenuRec["Menu"]["menu_id"], $explodeMenuId ) ? "selected=selected" : ""); ?>>
									<?php echo $qryMenuRec["Menu"]["menu_name"]; ?>
									</option>
									<?php } ?>
									</select>
									</div>-->
									<div class="input-field col s6">
										<select name="menuIdArr[]" id="menuIdArr" multiple="multiple">
										<option value="" disabled selected>Permission For</option>
										<?php foreach($qryMenuRecs as $qryMenuRec){?> 
											<option value="<?php echo $qryMenuRec["Menu"]["menu_id"]; ?>" <?php echo (in_array($qryMenuRec["Menu"]["menu_id"], $explodeMenuId ) ? "selected=selected" : ""); ?>><?php echo $qryMenuRec["Menu"]["menu_name"]; ?></option>
										<?php } ?>
										</select>  
									</div>
								</div>                    
							</div>                       
						</div>
					<!-- end of row div -->
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $("#editUser")[0].reset();
	})
	$("#submitForm").click(function(){
	    //$(".errorMsgFrm").css("display","block");
		var emailRegex = /^[a-z_A-Z0-9\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var mobileRegex = /^[7-9][0-9]{9}$/;
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var email = $.trim($('#email').val());
		var mobile_number = $.trim($('#mobile_number').val());
		var company = $.trim($('#company').val());
		
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the first name.");
			return false;
		}
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if(email ==""){
		    $('#email').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the email.");
			return false;
		}
		if(!emailRegex.test(email)){
			$('#email').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the valid email.");
			return false;
		}
		/*if(mobile_number ==""){
		    $('#mobile_number').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the phone.");
			return false;
		}
		if(!mobileRegex.test(mobile_number)){
			$('#mobile_number').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the valid mobile number.");
			return false;
		}*/
		if(company ==""){
		    $('#company').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the company.");
			return false;
		}
       
		if( $('#status').val() == "") {
		    $('#status').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select status.");
			return false;
		}
		
		if( $('#menuIdArr').val() == "") {
		    $('#menuIdArr').focus() ;
		    $(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select permission.");
			return false;
		}
	   document.editUser.submit();
	})
})
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
