<?php 

if(($this->Session->read('User.user_type') == "CPA") || ($this->Session->read('User.user_type') == "CPU")){ 
$theComponent = new CommonComponent();
//$theComponent->changeNumberStyle();
?>
<?php
$this->InstallTracking = ClassRegistry::init('InstallTracking');
$this->User = ClassRegistry::init('User');
$this->Fcappcontent = ClassRegistry::init('Fcappcontent');

$contentPriceVal = $this->User->query("select content_pricing from cm_users where id='".$this->Session->read('User.id')."'");


?>
<!-- Home-States-->
<div class = "home-stats">
	<div class = "row">
	<div class = "col s3">
		<div class = "card card-stats">
			<div class = "card-header" data-background-color = "orange">
				<!---<i class = "material-icons"> attach_money </i>--->
		<img src = "<?php echo BASE_URL; ?>/img/rupee-indian.png"  alt = "" align="center" style="padding:35px;"/>
			</div>
			<div class = "card-content">
				<p>Total Earning</p>
				<br/>
				<?php 
				
				$totalRevenue7 = ($contentData7[0][0]["countRec"] * $contentPriceVal[0]['cm_users']['content_pricing']);
				
				//echo number_format($totalRevenue7, 2);
				//$totalRevenue7 = "10";
				?>
				<h3>		
				<?php echo (empty($totalRevenue7) || $totalRevenue7==""  || $totalRevenue7 == 0) ? '0' : $theComponent->changeNumberStyle($totalRevenue7); 
				
				?>
				</h3>
			</div>
			<div class = "card-footer">
				<i class = "material-icons tiny">date_range</i>
				<span>Last 7 Days</span>
			</div>
		</div>
	<!-- end of card div -->
	</div>
	<div class = "col s3">
	<div class = "card card-stats">
	<div class = "card-header" data-background-color = "orange">
	<!----<i class = "material-icons"> attach_money </i>--->
	<img src = "<?php echo BASE_URL; ?>/img/rupee-indian.png"  alt = "" align="center" style="padding:35px;"/>
	</div>
	<div class = "card-content">
	<p>Total Earning</p>
	<br/>
	<?php 
	$totalRevenue30 = $contentData30[0][0]["countRec"] * $contentPriceVal[0]['cm_users']['content_pricing'];
	//echo number_format($totalRevenue30, 2);
	
	?>
	<h3><?php echo (empty($totalRevenue30) || $totalRevenue30==""  || $totalRevenue30 == 0) ? '0' : $theComponent->changeNumberStyle($totalRevenue30); ?>
	</div>
	<div class = "card-footer">
	<i class = "material-icons tiny">date_range</i>
	<span>Last 30 Days</span>
	</div>
	</div>
	<!-- end of card div -->
	</div>
	<div class = "col s3">
	<div class = "card card-stats">
	<div class = "card-header" data-background-color = "orange">
	<!-----<i class = "material-icons"> attach_money </i>--->
	<img src = "<?php echo BASE_URL; ?>/img/rupee-indian.png"  alt = "" align="center" style="padding:35px;"/>
	</div>
	<div class = "card-content">
	<p>Total Earning</p>
	<br/>
	<?php 
	$contentDataYear = $contentDataYear[0][0]["countRec"] * $contentPriceVal[0]['cm_users']['content_pricing'];
	//echo number_format($contentDataYear, 2);
	?>
	<h3><?php echo (empty($contentDataYear) || $contentDataYear==""  || $contentDataYear == 0) ? '0' : $theComponent->changeNumberStyle($contentDataYear); ?>
	</div>
	<div class = "card-footer">
	<i class = "material-icons tiny">date_range</i>
	<span>This Year</span>
	</div>
	</div>
	<!-- end of card div -->
	</div>
	<div class = "col s3">
	<div class = "card card-stats">
	<div class = "card-header" data-background-color = "red">
	<i class = "material-icons"> visibility </i>
	</div>
	<div class = "card-content">
	<p>Total Views</p>
	<br/>
	<h3><?php echo (empty($contentView[0][0]["countRec"]) || $contentView[0][0]["countRec"]==""  || $contentView[0][0]["countRec"] == 0) ? '0' : $theComponent->changeNumberStyle($contentView[0][0]["countRec"]); ?></h3>
	</div>
	<div class = "card-footer">
	<i class = "material-icons tiny">date_range</i>
	<span>Last 24 Hours</span>
	</div>
	</div>
	<!-- end of card div -->
	</div>
</div>
<!-- end of home State div -->
<!-- home-graph-container -->
<div class = "graph-container">
  <div class = "row">
	  <div class = "col s6">
		<div class = "card card-stats">
			<div class = "card-header" data-background-color = "orange">
				<i class = "material-icons"> group </i>
			</div>
		   <div class = "input-field col s4 offset-s5">
			  <select name="barChartChange" id="barChartChange">
				<!--<option value = "today" selected>Today</option>-->
				<option value = "yesterday" selected>Yesterday</option>
				<option value = "7days">Last 7 Days</option>
				<option value = "28days">Last 28 Days</option>
				<option value = "thisyear">This Year</option>
			  </select>
		   </div>
			<div class = "graph-content">
				<canvas id="bar-chart1" width="800" height="450"></canvas>
				<canvas id="bar-chart2" width="800" height="450"></canvas>
				<canvas id="bar-chart3" width="800" height="450"></canvas>
				<canvas id="bar-chart4" width="800" height="450"></canvas>
				<canvas id="bar-chart5" width="800" height="450"></canvas>
			</div>
		</div>
	  </div>
	  <div class = "col s6">
		<div class = "card card-stats">
			<div class = "card-header" data-background-color = "green">
				<i class = "material-icons"> group </i>
			</div>
			<div class = "input-field col s4 offset-s5">
			  <select name="lineChartChange" id="lineChartChange">
				<!--<option value = "today" selected>Today</option>-->
				<option value = "yesterday" selected>Yesterday</option>
				<option value = "7days">Last 7 Days</option>
				<option value = "28days">Last 28 Days</option>
				<option value = "thisyear">This Year</option>
			  </select>
		   </div>
			<div class = "graph-content">
				<canvas id="line-chart1" width="800" height="450"></canvas>
				<canvas id="line-chart2" width="800" height="450"></canvas>
				<canvas id="line-chart3" width="800" height="450"></canvas>
				<canvas id="line-chart4" width="800" height="450"></canvas>
				<canvas id="line-chart5" width="800" height="450"></canvas>
				
			</div>
		</div>
	  </div>    
  </div>
</div>  


<!-- end of content wrapper div -->
<?php echo $this->element('flashmessage'); ?>
<?php
//graph calculation revenue this year start here...
if(!empty($userMonthWiseData)){
	$arrRevenueMonth = array();
	$arrRevenueMonthRs = array();
	$arrRevenueMonthColor = array();
	$revenueArrColor = array("#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850");
	foreach($userMonthWiseData as $userMonthWiseDataRec){
		//$arrRevenueMonth[] = $userMonthWiseDataRec[0]["month"];
		$arrRevenueMonth[] = date("F", mktime(0, 0, 0, $userMonthWiseDataRec[0]["month"], 10));
		$arrViewMonthRs[] = $userMonthWiseDataRec[0]["countRec"];
		$arrRevenueMonthRs[] = $userMonthWiseDataRec[0]["countRec"]*$contentPriceVal[0]['cm_users']['content_pricing'];
		$arrRevenueMonthColorKey = array_rand($revenueArrColor);
		$arrRevenueMonthColor[] = $revenueArrColor[$arrRevenueMonthColorKey];
	}
	$imparrRevenueMonth = implode('","', $arrRevenueMonth);
	$imparrRevenueMonth = '"'.$imparrRevenueMonth.'"';
	$imparrRevenueMonthColor = implode('","', $arrRevenueMonthColor);
	$imparrRevenueMonthColor = '"'.$imparrRevenueMonthColor.'"';
	$imparrRevenueMonthRs = implode(",", $arrRevenueMonthRs);
	$imparrViewMonthRs = implode(",", $arrViewMonthRs);
	//line chart variable set here...
	$viewMonthLabel = $imparrRevenueMonth;
	$viewMonthData = $imparrViewMonthRs;
	$viewMonthColor = $revenueArrColor[$arrRevenueMonthColorKey];
	
} else {
	$imparrRevenueMonth = "";
	$imparrRevenueMonthColor = "";
	$imparrRevenueMonthRs = 0;
	$viewMonthLabel = "";
	$viewMonthData = 0;
	$viewMonthColor = "";
}
//graph calculation revenue this year end here...
//graph calculation revenue today start here...

if(!empty($userHourWiseData)){
	$arrRevenueHour = array();
	$arrRevenueHourRs = array();
	$arrRevenueHourColor = array();
	$arrViewHourRs = array();
	$revenueArrColorHour = array("#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850");
	foreach($userHourWiseData as $userHourWiseDataRec){
		//$arrRevenueMonth[] = $userMonthWiseDataRec[0]["month"];
		$arrRevenueHour[] = $userHourWiseDataRec[0]["hour"];
		$arrViewHourRs[] = $userHourWiseDataRec[0]["countRec"];
		$arrRevenueHourRs[] = $userHourWiseDataRec[0]["countRec"]*$contentPriceVal[0]['cm_users']['content_pricing'];
		$arrRevenueHourColorKey = array_rand($revenueArrColorHour);
		$arrRevenueHourColor[] = $revenueArrColorHour[$arrRevenueHourColorKey];
	}
	
	
	$imparrRevenueHour = implode('","', $arrRevenueHour);
	$imparrRevenueHour = '"'.$imparrRevenueHour.'"';
	$imparrRevenueHourColor = implode('","', $arrRevenueHourColor);
	$imparrRevenueHourColor = '"'.$imparrRevenueHourColor.'"';
	$imparrRevenueHourRs = implode(",", $arrRevenueHourRs);
	$imparrViewHourRs = implode(",", $arrViewHourRs);
	
	//line chart variable set here...
	$viewHourLabel = $imparrRevenueHour;
	$viewHourData = $imparrViewHourRs;
	$viewHourColor = $revenueArrColorHour[$arrRevenueHourColorKey];
} else {
	$imparrRevenueHour = "";
	$imparrRevenueHourColor = "";
	$imparrRevenueHourRs = 0;
	$viewHourLabel = "";
	$viewHourData = 0;
	$viewHourColor = "";
}
//graph calculation revenue today end here...

//graph calculation revenue yesterday start here...
//echo "<pre>";
//print_r($userPrevHourWiseData);
//die;
if(!empty($userPrevHourWiseData)){
	$arrRevenuePrevHour = array();
	$arrRevenuePrevHourRs = array();
	$arrRevenuePrevHourColor = array();
	$arrViewPrevHourRs = array();
	$revenueArrColorPrevHour = array("#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850");
	foreach($userPrevHourWiseData as $userPrevHourWiseDataRec){
		//$arrRevenueMonth[] = $userMonthWiseDataRec[0]["month"];
		$arrRevenuePrevHour[] = $userPrevHourWiseDataRec[0]["hour"];
		$arrViewPrevHourRs[] = $userPrevHourWiseDataRec[0]["countRec"];
		$arrRevenuePrevHourRs[] = $userPrevHourWiseDataRec[0]["countRec"]*$contentPriceVal[0]['cm_users']['content_pricing'];
		$arrRevenuePrevHourColorKey = array_rand($revenueArrColorPrevHour);
		$arrRevenuePrevHourColor[] = $revenueArrColorPrevHour[$arrRevenuePrevHourColorKey];
	}
	
	
	$imparrRevenuePrevHour = implode('","', $arrRevenuePrevHour);
	$imparrRevenuePrevHour = '"'.$imparrRevenuePrevHour.'"';
	$imparrRevenuePrevHourColor = implode('","', $arrRevenuePrevHourColor);
	$imparrRevenuePrevHourColor = '"'.$imparrRevenuePrevHourColor.'"';
	$imparrRevenuePrevHourRs = implode(",", $arrRevenuePrevHourRs);
	$imparrViewPrevHourRs = implode(",", $arrViewPrevHourRs);
	
	//line chart variable set here...
	$viewPrevHourLabel = $imparrRevenuePrevHour;
	$viewPrevHourData = $imparrViewPrevHourRs;
	$viewPrevHourColor = $revenueArrColorPrevHour[$arrRevenuePrevHourColorKey];
} else {
	$imparrRevenuePrevHour = "";
	$imparrRevenuePrevHourColor = "";
	$imparrRevenuePrevHourRs = 0;
	$viewPrevHourLabel = "";
	$viewPrevHourData = 0;
	$viewPrevHourColor = "";
}
//graph calculation revenue yesterday end here...
//graph calculation revenue 28 day start here...
//echo "<pre>";
//print_r($userPrev28DaysWiseData);
//die;
if(!empty($userPrev28DaysWiseData)){
	$arrRevenuePrev28Days = array();
	$arrRevenuePrev28DaysRs = array();
	$arrRevenuePrev28DaysColor = array();
	$arrViewPrev28DaysRs = array();
	$revenueArrColorPrev28Days = array("#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850");
	foreach($userPrev28DaysWiseData as $userPrev28DaysWiseDataRec){
		//$arrRevenueMonth[] = $userMonthWiseDataRec[0]["month"];
		$arrRevenuePrev28Days[] = $userPrev28DaysWiseDataRec[0]["created"];
		$arrViewPrev28DaysRs[] = $userPrev28DaysWiseDataRec[0]["countRec"];
		$arrRevenuePrev28DaysRs[] = $userPrev28DaysWiseDataRec[0]["countRec"]*$contentPriceVal[0]['cm_users']['content_pricing'];
		$arrRevenuePrev28DaysColorKey = array_rand($revenueArrColorPrev28Days);
		$arrRevenuePrev28DaysColor[] = $revenueArrColorPrev28Days[$arrRevenuePrev28DaysColorKey];
	}
	
	$imparrRevenuePrev28Days = implode('","', $arrRevenuePrev28Days);
	$imparrRevenuePrev28Days = '"'.$imparrRevenuePrev28Days.'"';
	$imparrRevenuePrev28DaysColor = implode('","', $arrRevenuePrev28DaysColor);
	$imparrRevenuePrev28DaysColor = '"'.$imparrRevenuePrev28DaysColor.'"';
	$imparrRevenuePrev28DaysRs = implode(",", $arrRevenuePrev28DaysRs);
	$imparrViewPrev28DaysRs = implode(",", $arrViewPrev28DaysRs);
	
	//line chart variable set here...
	$viewPrev28DaysLabel = $imparrRevenuePrev28Days;
	$viewPrev28DaysData = $imparrViewPrev28DaysRs;
	$viewPrev28DaysColor = $revenueArrColorPrev28Days[$arrRevenuePrev28DaysColorKey];
} else {
	$imparrRevenuePrev28Days = "";
	$imparrRevenuePrev28DaysColor = "";
	$imparrRevenuePrev28DaysRs = 0;
	
	$viewPrev28DaysLabel = "";
	$viewPrev28DaysData = 0;
	$viewPrev28DaysColor = "";
}
//graph calculation revenue 28 day end here...

//graph calculation revenue 7 day start here...
//echo "<pre>";
//print_r($userPrev28DaysWiseData);
//die;
if(!empty($userPrev7DaysWiseData)){
	$arrRevenuePrev7Days = array();
	$arrRevenuePrev7DaysRs = array();
	$arrRevenuePrev7DaysColor = array();
	$arrViewPrev7DaysRs = array();
	$revenueArrColorPrev7Days = array("#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850");
	foreach($userPrev7DaysWiseData as $userPrev7DaysWiseDataRec){
		//$arrRevenueMonth[] = $userMonthWiseDataRec[0]["month"];
		$arrRevenuePrev7Days[] = $userPrev7DaysWiseDataRec[0]["created"];
		$arrViewPrev7DaysRs[] = $userPrev7DaysWiseDataRec[0]["countRec"];
		$arrRevenuePrev7DaysRs[] = $userPrev7DaysWiseDataRec[0]["countRec"]*$contentPriceVal[0]['cm_users']['content_pricing'];
		$arrRevenuePrev7DaysColorKey = array_rand($revenueArrColorPrev7Days);
		$arrRevenuePrev7DaysColor[] = $revenueArrColorPrev7Days[$arrRevenuePrev7DaysColorKey];
	}
	
	$imparrRevenuePrev7Days = implode('","', $arrRevenuePrev7Days);
	$imparrRevenuePrev7Days = '"'.$imparrRevenuePrev7Days.'"';
	$imparrRevenuePrev7DaysColor = implode('","', $arrRevenuePrev7DaysColor);
	$imparrRevenuePrev7DaysColor = '"'.$imparrRevenuePrev7DaysColor.'"';
	$imparrRevenuePrev7DaysRs = implode(",", $arrRevenuePrev7DaysRs);
	$imparrViewPrev7DaysRs = implode(",", $arrViewPrev7DaysRs);
	
	//line chart variable set here...
	$viewPrev7DaysLabel = $imparrRevenuePrev7Days;
	$viewPrev7DaysData = $imparrViewPrev7DaysRs;
	$viewPrev7DaysColor = $revenueArrColorPrev7Days[$arrRevenuePrev7DaysColorKey];
	
} else {
	$imparrRevenuePrev7Days = "";
	$imparrRevenuePrev7DaysColor = "";
	$imparrRevenuePrev7DaysRs = 0;
	
	$viewPrev7DaysLabel = "";
	$viewPrev7DaysData = 0;
	$viewPrev7DaysColor = "";
}
//graph calculation revenue 7 day end here...

?>
<script type="text/javascript">

$(document).ready(function(){
	var barChartChangeVal = $("#barChartChange").val();
	//alert(barChartChangeVal);
	/*if(barChartChangeVal == "today"){
		$("#bar-chart1").css("display", "block");
		$("#bar-chart2").css("display", "none");
		$("#bar-chart3").css("display", "none");
		$("#bar-chart4").css("display", "none");
		$("#bar-chart5").css("display", "none");
		//Bar chart
		new Chart(document.getElementById("bar-chart1"), {
			type: 'bar',
			data: {
			  labels: [<?php echo $imparrRevenueHour; ?>],
			  datasets: [
				{
				  label: "Revenue",
				  backgroundColor: [<?php echo $imparrRevenueHourColor; ?>],
				  data: [<?php echo $imparrRevenueHourRs; ?>]
				}
			  ]
			},
			options: {
			  legend: { display: false },
			  title: {
				display: true,
				text: 'Today Revenue'
			  }
			}
		});
	}*/
	if(barChartChangeVal == "yesterday"){
			
		$("#bar-chart1").css("display", "none");
		$("#bar-chart2").css("display", "block");
		$("#bar-chart3").css("display", "none");
		$("#bar-chart4").css("display", "none");
		$("#bar-chart5").css("display", "none");
		
		//Bar chart
		new Chart(document.getElementById("bar-chart2"), {
			type: 'bar',
			data: {
			  labels: [<?php echo $imparrRevenuePrevHour; ?>],
			  datasets: [
				{
				  label: "Revenue",
				  backgroundColor: [<?php echo $imparrRevenuePrevHourColor; ?>],
				  data: [<?php echo $imparrRevenuePrevHourRs; ?>]
				}
			  ]
			},
			options: {
			  legend: { display: false },
			  title: {
				display: true,
				text: 'Yesterday Revenue'
			  }
			}
		});
			
	}
	$("#barChartChange").change(function(){
		var barChartChangeVal = $("#barChartChange").val();
		if(barChartChangeVal == "today"){
			$("#bar-chart1").css("display", "block");
			$("#bar-chart2").css("display", "none");
			$("#bar-chart3").css("display", "none");
			$("#bar-chart4").css("display", "none");
			$("#bar-chart5").css("display", "none");
			//Bar chart
			new Chart(document.getElementById("bar-chart1"), {
				type: 'bar',
				data: {
				  labels: [<?php echo $imparrRevenueHour; ?>],
				  datasets: [
					{
					  label: "Revenue",
					  backgroundColor: [<?php echo $imparrRevenueHourColor; ?>],
					  data: [<?php echo $imparrRevenueHourRs; ?>]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: true,
					text: 'Today Revenue'
				  }
				}
			});
				
			
		} else if(barChartChangeVal == "yesterday"){
			
			$("#bar-chart1").css("display", "none");
			$("#bar-chart2").css("display", "block");
			$("#bar-chart3").css("display", "none");
			$("#bar-chart4").css("display", "none");
			$("#bar-chart5").css("display", "none");
			
			//Bar chart
			new Chart(document.getElementById("bar-chart2"), {
				type: 'bar',
				data: {
				  labels: [<?php echo $imparrRevenuePrevHour; ?>],
				  datasets: [
					{
					  label: "Revenue",
					  backgroundColor: [<?php echo $imparrRevenuePrevHourColor; ?>],
					  data: [<?php echo $imparrRevenuePrevHourRs; ?>]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: true,
					text: 'Yesterday Revenue'
				  }
				}
			});
				
		} else if(barChartChangeVal == "7days"){
			$("#bar-chart1").css("display", "none");
			$("#bar-chart2").css("display", "none");
			$("#bar-chart3").css("display", "block");
			$("#bar-chart4").css("display", "none");
			$("#bar-chart5").css("display", "none");
			//Bar chart
			new Chart(document.getElementById("bar-chart3"), {
				type: 'bar',
				data: {
				  labels: [<?php echo $imparrRevenuePrev7Days; ?>],
				  datasets: [
					{
					  label: "Revenue",
					  backgroundColor: [<?php echo $imparrRevenuePrev7DaysColor; ?>],
					  data: [<?php echo $imparrRevenuePrev7DaysRs; ?>]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: true,
					text: 'Last 7 day Revenue'
				  }
				}
			});
			
		} else if(barChartChangeVal == "28days"){
			$("#bar-chart1").css("display", "none");
			$("#bar-chart2").css("display", "none");
			$("#bar-chart3").css("display", "none");
			$("#bar-chart4").css("display", "block");
			$("#bar-chart5").css("display", "none");
			
			new Chart(document.getElementById("bar-chart4"), {
				type: 'bar',
				data: {
				  labels: [<?php echo $imparrRevenuePrev28Days; ?>],
				  datasets: [
					{
					  label: "Revenue",
					  backgroundColor: [<?php echo $imparrRevenuePrev28DaysColor; ?>],
					  data: [<?php echo $imparrRevenuePrev28DaysRs; ?>]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: false,
					text: 'Last 28 day Revenue'
				  }
				}
			});
		} else if(barChartChangeVal == "thisyear"){
			//Bar chart
			$("#bar-chart1").css("display", "none");
			$("#bar-chart2").css("display", "none");
			$("#bar-chart3").css("display", "none");
			$("#bar-chart4").css("display", "none");
			$("#bar-chart5").css("display", "block");
			new Chart(document.getElementById("bar-chart5"), {
				type: 'bar',
				data: {
				  labels: [<?php echo $imparrRevenueMonth; ?>],
				  datasets: [
					{
					  label: "Revenue",
					  backgroundColor: [<?php echo $imparrRevenueMonthColor; ?>],
					  data: [<?php echo $imparrRevenueMonthRs; ?>]
					}
				  ]
				},
				options: {
				  legend: { display: false },
				  title: {
					display: false,
					text: 'Revenue in this year'
				  }
				}
			});
		}
		
	})
})

$(document).ready(function(){
	var lineChartChangeVal = $("#lineChartChange").val();
	/*if(lineChartChangeVal == "today"){
		$("#line-chart1").css("display", "block");
		$("#line-chart2").css("display", "none");
		$("#line-chart3").css("display", "none");
		$("#line-chart4").css("display", "none");
		$("#line-chart5").css("display", "none");
		//Line Chart
		new Chart(document.getElementById("line-chart1"), {
		  type: 'line',
		  data: {
			labels: [<?php echo $viewHourLabel; ?>],
			datasets: [{ 
				data: [<?php echo $viewHourData; ?>],
				label: "Today View",
				borderColor: "<?php echo $viewHourColor; ?>",
				fill: false
			  }
			]
		  },
		  options: {
			title: {
			  display: true,
			  text: ''
			}
		  }
		});
	}*/
	if(lineChartChangeVal == "yesterday"){
		$("#line-chart1").css("display", "none");
		$("#line-chart2").css("display", "block");
		$("#line-chart3").css("display", "none");
		$("#line-chart4").css("display", "none");
		$("#line-chart5").css("display", "none");
		//Line Chart
		new Chart(document.getElementById("line-chart2"), {
		  type: 'line',
		  data: {
			labels: [<?php echo $viewPrevHourLabel; ?>],
			datasets: [{ 
				data: [<?php echo $viewPrevHourData; ?>],
				label: "Yesterday View",
				borderColor: "<?php echo $viewPrevHourColor; ?>",
				fill: false
			  }
			]
		  },
		  options: {
			title: {
			  display: true,
			  text: ''
			}
		  }
		});

	}
	$("#lineChartChange").change(function(){
		var lineChartChangeVal = $("#lineChartChange").val();
		//alert(lineChartChangeVal);
		if(lineChartChangeVal == "today"){
			$("#line-chart1").css("display", "block");
			$("#line-chart2").css("display", "none");
			$("#line-chart3").css("display", "none");
			$("#line-chart4").css("display", "none");
			$("#line-chart5").css("display", "none");
			//Line Chart
			new Chart(document.getElementById("line-chart1"), {
			  type: 'line',
			  data: {
				labels: [<?php echo $viewHourLabel; ?>],
				datasets: [{ 
					data: [<?php echo $viewHourData; ?>],
					label: "Today View",
					borderColor: "<?php echo $viewHourColor; ?>",
					fill: false
				  }
				]
			  },
			  options: {
				title: {
				  display: true,
				  text: ''
				}
			  }
			});
		} else if(lineChartChangeVal == "yesterday"){
			$("#line-chart1").css("display", "none");
			$("#line-chart2").css("display", "block");
			$("#line-chart3").css("display", "none");
			$("#line-chart4").css("display", "none");
			$("#line-chart5").css("display", "none");
			//Line Chart
			new Chart(document.getElementById("line-chart2"), {
			  type: 'line',
			  data: {
				labels: [<?php echo $viewPrevHourLabel; ?>],
				datasets: [{ 
					data: [<?php echo $viewPrevHourData; ?>],
					label: "Yesterday View",
					borderColor: "<?php echo $viewPrevHourColor; ?>",
					fill: false
				  }
				]
			  },
			  options: {
				title: {
				  display: true,
				  text: ''
				}
			  }
			});

		} else if(lineChartChangeVal == "7days"){
			$("#line-chart1").css("display", "none");
			$("#line-chart2").css("display", "none");
			$("#line-chart3").css("display", "block");
			$("#line-chart4").css("display", "none");
			$("#line-chart5").css("display", "none");
			//Line Chart
			new Chart(document.getElementById("line-chart3"), {
			  type: 'line',
			  data: {
				labels: [<?php echo $viewPrev7DaysLabel; ?>],
				datasets: [{ 
					data: [<?php echo $viewPrev7DaysData; ?>],
					label: "last 7 Day View",
					borderColor: "<?php echo $viewPrev7DaysColor; ?>",
					fill: false
				  }
				]
			  },
			  options: {
				title: {
				  display: true,
				  text: ''
				}
			  }
			});
		} else if(lineChartChangeVal == "28days"){
			$("#line-chart1").css("display", "none");
			$("#line-chart2").css("display", "none");
			$("#line-chart3").css("display", "none");
			$("#line-chart4").css("display", "block");
			$("#line-chart5").css("display", "none");
			//Line Chart
			new Chart(document.getElementById("line-chart4"), {
			  type: 'line',
			  data: {
				labels: [<?php echo $viewPrev28DaysLabel; ?>],
				datasets: [{ 
					data: [<?php echo $viewPrev28DaysData; ?>],
					label: "Last 28 Day View",
					borderColor: "<?php echo $viewPrev28DaysColor; ?>",
					fill: false
				  }
				]
			  },
			  options: {
				title: {
				  display: true,
				  text: ''
				}
			  }
			});
		} else if(lineChartChangeVal == "thisyear"){
			$("#line-chart1").css("display", "none");
			$("#line-chart2").css("display", "none");
			$("#line-chart3").css("display", "none");
			$("#line-chart4").css("display", "none");
			$("#line-chart5").css("display", "block");
			//Line Chart
			new Chart(document.getElementById("line-chart5"), {
			  type: 'line',
			  data: {
				labels: [<?php echo $viewMonthLabel; ?>],
				datasets: [{ 
					data: [<?php echo $viewMonthData; ?>],
					label: "This Year View",
					borderColor: "<?php echo $viewMonthColor; ?>",
					fill: false
				  }
				]
			  },
			  options: {
				title: {
				  display: true,
				  text: ''
				}
			  }
			});
		}
	})	
})
</script>
<?php } else { ?>
<div class = "home-stats">
	<div class = "row">
		<div class = "col s12">
			<h3>Welcome <?php echo ucfirst($this->Session->read('User.name')); ?></h3>
		</div>
	</div>
</div>	
<?php } ?>