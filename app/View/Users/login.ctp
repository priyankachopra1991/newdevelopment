<div class="signup-content login-content">
	<div class="row">                                            
		<div class="col s6 x12 offset-s3">
			<div class="card parent-card-container">                           
				<div class="row">                            
					<div class="col s12">
						<div class="row">
							<form class="col s12 login-form"  id="loginFrm" name="loginFrm" method="post" action="<?php echo BASE_URL; ?>/users/login" autocomplete="off">    
								<h2 class="header">Log In</h2>  
								<hr class="space" />    
								<div id="errorMsgFrm" style="display:none;"></div>									
								<div class="input-field">
									<i class="material-icons prefix tiny" data-input-color="grey">email</i>
									<input id="email" name="email" type="email" value="" class="validate" autocomplete="new-email" />
									<label for="telephone">Email</label>
								</div>                                         
								<div class="input-field">
									<i class="material-icons prefix tiny" data-input-color = "grey">lock_outline</i>
									<input id="password" name="password" type="password" value="" class="validate" autocomplete="new-password" />
									<label for="password">Password</label>
								</div>   
								<div class="input-field submit-form">
									<!--<a href="" class="btn red darken-1" id="submitLogin">Let's Go</a>-->
									<button class="btn red darken-1" data-background-color = "red" type="submit" id="submitLogin" name="submitLogin">Let's Go
									<i class="material-icons right tiny">send</i>
									</button>
									
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    //$('#email').val('');
	//$('#password').val('');
	//$('#email').attr("autocomplete", "off");
    //setTimeout('$("#email").val("");', 50); 
	//$('#loginFrm input').val("");
	//$('input').attr('autocomplete', 'off');
	//$("#loginFrm")[0].reset();
	$("#errorMsgFrm").css("display", "none");
	$("#submitLogin").click(function(){
		$("#errorMsgFrm").css("display", "none");
		var emailRegex = /^[a-z_A-Z0-9\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	    
		var email = $.trim($('#email').val());
		var password = $.trim($('#password').val());
		
		if(email ==""){
		    $('#email').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the email.");
			return false;
		}
		if(!emailRegex.test(email)){
			$('#email').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the valid email.");
			return false;
		}
		if(password ==""){
		    $('#password').focus() ;
			$("#errorMsgFrm").css("display", "block");
			$('#errorMsgFrm').html("Please enter the password.");
			return false;
		}
		
		return true;
		
	})
})
</script>
<?php //echo $this->element('flashmessage'); ?>
<?php $errorMsgContent = $this->Session->flash(); 
if(!empty($errorMsgContent) || $errorMsgContent !=""){
?> 
	<script type="text/javascript">
	$(document).ready(function(){
		//$('#errorMsgFrm .notice').remove();
	    //$(".notice").attr("class",""); 
		//var eMsg = $("#errorMsgFrm").val();
		//alert(eMsg);
		var errorMsg = '<?php echo $errorMsgContent; ?>';
		$("#errorMsgFrm").css("display", "block");
		$('#errorMsgFrm').html(errorMsg);
		return false;
		
	})
	</script>
<?php } ?>