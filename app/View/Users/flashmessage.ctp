<?php $errorMsgContent = $this->Session->flash(); ?>
<div id="myModalBox" class="modal">
    <div class="modal-content">
      <h4></h4>
      <p>
	  <?php echo $errorMsgContent;?></p>
    </div>
    <div class="modal-footer">
      <a href="javascript:void(0)" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
	  
    </div>
</div>

<!-- Modal content end-->	


<?php if($errorMsgContent !=""){ ?>
	<script type="text/javascript"> 
	$(document).ready(function () {
		$("#myModalBox").modal('open');
		$('.modal-trigger').leanModal({ dismissible: false});
		$("#myModalBox").find('.modal-close').off();
	});	
	</script>	
<?php } ?>
