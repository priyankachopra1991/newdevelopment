<?php echo $this->element("header"); ?>
<title>FirstCut Pannel</title>
</head>
<body>
    <div class = "page-wrapper">
		<?php echo $this->element("sidebar"); ?>
		<div class = "content-wrapper container">
			<?php $msg = $this->Session->flash(); ?>
			<?php echo $this->element("notifyheader"); ?>
			<?php echo $this->fetch("content"); ?>
			<?php //echo $this->element('sql_dump');?>
		</div>
	</div>
	
<?php //echo $this->element("footerlink"); ?>
<?php echo $this->element("footer"); ?>

