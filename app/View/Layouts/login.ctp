<?php echo $this->element("header"); ?>
<title>Login</title>
</head>
<body class = "login-wrapper" style="background-image: url('<?php echo $this->Html->url('/', true); ?>img/login-bg.jpg'); background-size: cover; background-position: center; background-repeat: no-repeat;" alt = "">

	<nav>             
		<div class="nav-wrapper">                
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="<?php echo $this->html->url('/users/register', true); ?>"><i class = "material-icons tiny left">person_add</i>Register</a></li>
				<li><a href="About.html"><i class = "material-icons tiny left">info_outline</i>About FirstCut</a></li>
				<li><a href="contact.html"><i class = "material-icons tiny left">local_phone</i>Contact Us</a></li>
			</ul>
		</div>          
	</nav>

	<div class = "main-content">
		<div class = "container">
			<?php $msg = $this->Session->flash(); ?>
			<?php echo $this->fetch("content"); ?>
		</div>
	</div>
	
	<?php echo $this->element("footerlink"); ?>
	<?php echo $this->element("footer"); ?>
