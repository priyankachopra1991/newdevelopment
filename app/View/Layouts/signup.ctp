<?php echo $this->element("header"); ?>
<title>Register</title>
</head>
<body class = "login-wrapper" style = "background-image: url('<?php echo $this->Html->url('/', true); ?>img/signup-bg.jpg'); background-size: cover; background-position : center;
    background-repeat: no-repeat;" alt = ""> 
	<nav>             
		<div class="nav-wrapper">                
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="<?php echo $this->html->url('/users/login', true); ?>"><i class = "material-icons tiny left">fingerprint</i>already a member? Login</a></li>
				<li><a href="#"><i class = "material-icons tiny left">info_outline</i>About FirstCut</a></li>
				<li><a href="collapsible.html"><i class = "material-icons tiny left">local_phone</i>Contact Us</a></li>
			</ul>
		</div>          
	</nav>
	
	<div class = "main-content">
		<div class = "container">
			<?php $msg = $this->Session->flash(); ?>
			<?php echo $this->fetch("content"); ?>
		</div>
	</div>
	
	<?php echo $this->element("footerlink"); ?>
	<?php echo $this->element("footer"); ?>