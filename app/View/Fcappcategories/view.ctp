<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchCategory" id="searchCategory" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list right">                           
							<li>
								<select name="searchBy" id="searchBy">
									<option value="">Search By</option>
									<option value="name" <?php if(@$_GET['searchBy'] == "name"){ echo "selected"; } ?>>Name</option>
									<option value="ct_id" <?php if(@$_GET['searchBy'] == "ct_id"){ echo "selected"; } ?>>Category Id</option>
								</select>
							</li>
							
							<li>
								<div class="input-field">
									<input placeholder="Search By String" id="searchString" name="searchString" type="text" value="<?php echo @$_GET["searchString"]; ?>" class="validate">
								</div>
							</li>
							<li>
								<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<!--<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>-->
		</div>
		<form name="sortCategory" id="sortCategory" action="" method="get">
			<div class = "col s6 small-filteration">
				<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Active</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>
				</ul>
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    
	<!--List view seection strat here-->
	<?php if(!empty($categories)){ ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th>Category Id</th>
						<th>Name</th>
						<th>Parent Name</th>
						<th>Status</th>
						<?php if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO")){ ?>
							<th>ACTION</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach($categories as $categoriesRec){ ?>
						<tr>
							<td><?php echo $categoriesRec["child"]["category_id"]; ?></td>
							<td><?php echo $categoriesRec["child"]["name"]; ?></td>
							<td><?php if(empty($categoriesRec["parent"]["parent_cat_name"])){ echo "NA"; } else { echo $categoriesRec["parent"]["parent_cat_name"]; } ?></td>
							<td class="data-left"><?php if($categoriesRec["child"]["status"] =="A"){ echo "Active"; } else { echo "Deactive"; } ?></td>
							
							<?php if($this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
								<td><a href = "<?php echo BASE_URL."/fcappcategories/edit/".$categoriesRec["child"]["category_id"]; ?>" class = "btn red lighten-1">Edit</a></td>

							<?php } ?>
						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
</div>
<?php if((!empty($categories)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/fcappcategories/view";
	
	if(@$_GET["searchBy"] !=""){
		$searchBy = @$_GET["searchBy"];
	} else {
		$searchBy = "";
	}
	if(@$_GET["searchString"] !=""){
		$searchString = @$_GET["searchString"];
	} else {
		$searchString = "";
	}
	if(@$_GET["sortBy"] !=""){
		$sortBy = @$_GET["sortBy"];
	} else {
		$sortBy = "";
	}
	
	if(@$_GET["filterBy"] !=""){
		$filterBy = @$_GET["filterBy"];
	} else {
		$filterBy = "";
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    
	$("#searchRec").click(function(){
		document.searchCategory.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortCategory.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortCategory.submit();
	
	})
	
})
</script>

