<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" name="editCategory" id="editCategory" method="post" action="<?php echo BASE_URL."/fcappcategories/edit/".$categories[0]["cm_categories"]["category_id"]; ?>" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit Category</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s8">
								<input id="id" name="id" value="<?php echo @$categories[0]['cm_categories']["category_id"]; ?>" type="hidden" class="validate">
								<div class = "row">
									<div class="input-field col s12">
										<input placeholder="Category Name" id="name" name="name" value="<?php echo @$categories[0]['cm_categories']["name"]; ?>" type="text" class="validate">
									</div>
								<div class="input-field col s12">
									<input placeholder="Category Slug" id="slug" name="slug" value="<?php echo @$categories[0]['cm_categories']["slug"]; ?>"  type="text" class="validate">
								</div>
								<div class="input-field col s12">
									<input placeholder="Display Order" id="display_order" name="display_order" value="<?php echo @$categories[0]['cm_categories']["display_order"]; ?>"  type="text"  onkeypress="return isNumber(event)" class="validate">
								</div>
								<div class="input-field col s12">
									<input placeholder="Heading Background" id="heading_back_color" name="heading_back_color" value="<?php echo @$categories[0]["cm_categories"]["heading_back_color"]; ?>"  type="text" class="validate">
								</div>
								<div class="input-field col s12">
								<label for="name">Status</label>
								<br/>
								<br/>
									<input type="radio" name="status" id="optionsRadiosInline1" value="A" <?php echo ((@$categories[0]["cm_categories"]["status"] == "A") ? "checked" : ""); ?> class="space">Active&nbsp;&nbsp;
														
									<input type="radio" name="status" id="optionsRadiosInline2" value="D" <?php echo ((@$categories[0]["cm_categories"]["status"] == "D") ? "checked" : ""); ?> class="space">Deactive
								</div>
								<div class="input-field col s12">	
									<label for="name">Display On App</label>
									<br/>
									<br/>
									<input type="radio" name="app_flag" id="optionsRadiosInline3" value="1" <?php echo ((@$categories[0]["cm_categories"]["app_flag"] == 1) ? "checked" : ""); ?> class="space">Yes&nbsp;&nbsp;
									<input type="radio" name="app_flag" id="optionsRadiosInline4" value="0" <?php echo ((@$categories[0]["cm_categories"]["app_flag"] == 0) ? "checked" : ""); ?> class="space">No
								</div>
								<div class="input-field series-file file_icon col s12">
									<label for="channel">Category Banner</label>
									<i class = "material-icons">perm_media</i>
									<!--<input type="file" id="c_banner" name="c_banner">-->
									
									<?php echo $this->Form->input('category_banner', array('type'=>'file','label'=>'','id'=>'category_banner'));?>
								
									<?php if(($categories[0]["cm_categories"]["category_banner"] !="")|| ($categories[0]["cm_categories"]["category_banner"]!=NULL)){
										echo "<br/>";
										echo "<img src='".AMAZONIMAGEURL.$categories[0]["cm_categories"]["category_banner"]."' height='100' width='100' />";
														
										} 
										echo $this->Form->input('hiddenimage3',array('type'=>'hidden','value'=>$categories[0]["cm_categories"]["category_banner"]));
													
									?>
									
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Description" id="description" name="description" class="materialize-textarea" ><?php echo @$categories[0]["cm_categories"]["description"]; ?></textarea>
								</div>
								
								<div class="input-field col s12">
									<textarea placeholder = "Alt Tag" id="alttag" name="alttag" class="materialize-textarea" ><?php echo @$categories[0]["cm_categories"]["alttag"]; ?></textarea>
								</div>
							
							</div>                    
						</div>
						<div class = "col s4">
							<div class = "row"><div class="input-field series-icon file_icon col s12">          
							<label for="channel">Category Icon</label>                   
								<i class = "material-icons">perm_media</i>
								<!--<input type="file" name="c_icon" id="c_icon">-->
									
									<?php echo $this->Form->input('cat_icon', array('type'=>'file','label'=>'','id'=>'cat_icon', 'required'=>false)); ?><?php if(($categories[0]["cm_categories"]["cat_icon"] !="")|| ($categories[0]["cm_categories"]["cat_icon"]!=NULL)){
										echo "<br/>";
										echo "<img src='".AMAZONIMAGEURL.$categories[0]["cm_categories"]["cat_icon"]."' height='100' width='100' />";
														
									}
									echo $this->Form->input('hiddenimage1',array('type'=>'hidden','value'=>$categories[0]["cm_categories"]["cat_icon"]));
						
									?>
							</div>
							
							<div class = "row"><div class="input-field series-icon file_icon col s12">               <label for="channel">Portal Category Icon</label>              
								<i class = "material-icons">perm_media</i>
								<?php echo $this->Form->input('portal_cat_icon', array('type'=>'file','label'=>'','id'=>'portal_cat_icon', 'required'=>false));?>
								
								<?php if(($categories[0]["cm_categories"]["portal_cat_icon"] !="")|| ($categories[0]["cm_categories"]["portal_cat_icon"]!=NULL)){
									echo "<br/>";
									echo "<img src='".AMAZONIMAGEURL.$categories[0]["cm_categories"]["portal_cat_icon"]."' height='100' width='100' />";
																		
								} 
								echo $this->Form->input('hiddenimage2',array('type'=>'hidden','value'=>$categories[0]["cm_categories"]["portal_cat_icon"]));
								?>
							</div>
							

							<div class = "row"><div class="input-field series-icon file_icon col s12">                  <label for="channel">Category Page Icon</label>           
								<i class = "material-icons">perm_media</i>
								<?php echo $this->Form->input('category_page_icon', array('type'=>'file','label'=>'','id'=>'category_page_icon', 'class'=>'form-control'));?>
								<?php if(($categories[0]["cm_categories"]["category_page_icon"] !="")|| ($categories[0]["cm_categories"]["category_page_icon"]!=NULL)){
									echo "<br/>";
									echo "<img src='".AMAZONIMAGEURL.$categories[0]["cm_categories"]["category_page_icon"]."' height='100' width='100' />";
													
									} 
									echo $this->Form->input('hiddenimage4',array('type'=>'hidden','value'=>$categories[0]["cm_categories"]["category_page_icon"]));
								?>
							</div>
						</div>                          
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $("#editCategory")[0].reset();
	})
	$("#submitForm").click(function(){
	    var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var slug = $.trim($('#slug').val());
		var category_banner = $('#category_banner').val().split('.').pop().toLowerCase();
		var cat_icon = $('#cat_icon').val().split('.').pop().toLowerCase();
		var portal_cat_icon = $('#portal_cat_icon').val().split('.').pop().toLowerCase();
		var category_page_icon = $('#category_page_icon').val().split('.').pop().toLowerCase();
	
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if(slug ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the slug.");
			return false;
		}
		
		
		if(category_banner !="" && $.inArray(category_banner, ['png','jpg','jpeg']) == -1){
			$('#category_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload category banner as png, jpg, jpeg.");
			return false;
		}
		
		if(cat_icon !="" && $.inArray(cat_icon, ['png','jpg','jpeg']) == -1){
			$('#cat_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload category icon as png, jpg, jpeg.");
			return false;
		}
		
		if(portal_cat_icon !="" && $.inArray(portal_cat_icon, ['png','jpg','jpeg']) == -1){
			$('#portal_cat_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload portal category icon as png, jpg, jpeg.");
			return false;
		}
		
		if(category_page_icon !="" && $.inArray(category_page_icon, ['png','jpg','jpeg']) == -1){
			$('#c_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload category page icon as png, jpg, jpeg.");
			return false;
		}
		
		document.editCategory.submit();
	})
	
	
})
</script>	
<script type="text/javascript"> 
    function isNumber(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>