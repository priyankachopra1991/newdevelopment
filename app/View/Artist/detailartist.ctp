<div class = "single-view-channel-section "> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<div class = "row">
						<div class = "col s7">
							<div class = "inner-title left">
								<h2><?php echo $artists[0]["ar_artist"]["name"]; ?></h2>
								<p>Artist ID : <?php echo $artists[0]["ar_artist"]["id"]; ?></p>
							</div>
						</div>
						<div class = "col s5">
							<div class = "inner-button right">
								<a class="waves-effect waves-light btn green darken-1" href = "<?php echo BASE_URL."/artist/addcontentartist"; ?>">Add Content</a>
								<a class="waves-effect waves-light btn orange darken-1" href = "<?php echo BASE_URL."/artist/editartist/".$artists[0]["ar_artist"]["id"]; ?>">Edit</a>
								<?php if($this->Session->read('User.user_type') == 'CPA'){ ?>
									<a class="waves-effect waves-light btn red darken-1" href="<?php echo BASE_URL."/artist/deleteartist/".$artists[0]["ar_artist"]["id"]; ?>"  onclick="return ConfirmDelete();">Delete</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class = "col s7">  
					<div class = "inner-container">                          
						<div class = "inner-img">
							<img src = "<?php echo AMAZONIMAGEURL.$artists[0]["ar_artist"]["coverImg"]; ?>" alt = "" />
						</div>
						<div class = "inner-des">
							<h3>Description</h3>
							<p><?php echo empty($artists[0]["ar_artist"]["decription"]) ? 'NA' : $artists[0]["ar_artist"]["decription"]; ?> </p>
						</div>
						<div class = "inner-car">
							<h3>Role</h3>
							<p><?php echo empty($artists[0]["ar_artist"]["role"]) ? 'NA' : $artists[0]["ar_artist"]["role"]; ?> </p>
						</div>
					</div>                     
				</div>
				<!-- end of col s8 div -->
				<div class = "col s5">                     
					<ul class = "approval-list">
						<li class = "list-hreading">Approval Status</li>
						<li>
						<?php if($artists[0]["ar_artist"]["status"] == 1){ ?>
						Approved
						<?php } else if ($artists[0]["ar_artist"]["status"] == 0){?>
						Submitted for approval
						<?php } else { ?>
						Inactive
						<?php } ?>
						
						</li>
					</ul> 
					<ul class = "Date-Created">
						<li class = "list-hreading">Date Created</li>
						<li>
						<?php echo date('d/m/Y', strtotime($artists[0]["ar_artist"]["created"])); ?>
						</li>
					</ul>
					<ul class = "Channel-listed">
						<li class = "list-hreading">Date Of Birth</li>
						<li><?php echo date('d/m/Y', strtotime($artists[0]["ar_artist"]["dob"])); ?></li>
					</ul>                    
				</div>
			<!-- end of col s4 div -->
			</div>
		</div>
	</div>
</div>
<script>
    function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
</script>  