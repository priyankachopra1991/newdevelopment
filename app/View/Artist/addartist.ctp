<script>
$(function() {
	$("#dob").datepicker( { "dateFormat":"yy-mm-dd" } );
});
 
</script>
<div class = "table-format-data">
	<div class = "row">
		<div class = "col s8 offset-s2">                     
			<div class = "add-channel-form">
				<div class = "channel-header">
					<h2>Add Artist</h2> 
                    <p>This information will let us know more about your artist</p>
				</div>
				<div class="row">
					<div class="col s12">
						<ul class="tabs artist-tabs">
							<li class="tab col s4"><a class="active" href="#basicinfo" id="tabberDisableBasic">Basic Info</a></li>
							<li class="tab col s4"><a href="#identity" id="tabberDisableIdentity">Identity</a></li>
							<li class="tab col s4"><a href="#socialConnect" id="tabberDisableSocialConnect">Social Connect</a></li>
							
						</ul>
					</div>
					<form name="addArtist" id="addArtist" action="<?php echo BASE_URL; ?>/artist/addartist" method="post"  enctype="multipart/form-data">
						<div id="basicinfo" class="col s12 tab-content">
							<div class = "row">
							<div class="errorMsgFrm" style="display:none;"></div>
								<div class="input-field col s6">
                                    <i class="material-icons prefix">live_tv</i>
                                    <input type="text" id="name" name="name" class="validate">
                                    <label for="icon_prefix">Name</label>
                                </div>
                                <div class="input-field col s6">
                                  <select name="country_code" id="country_code">
										<option value="">Country</option>
										<?php foreach($country as $country){ ?>
											<option value="<?php echo $country["Country"]["country_code"]; ?>"><?php echo $country["Country"]["country_name"]; ?></option>
										<?php } ?>
										
									</select>
								</div>
                                <div class="input-field col s6">
                                    <select name="roleId" id="roleId">
										<option value="">Role</option>
										<?php foreach($casts as $cast){ ?>
											<option value="<?php echo $cast["Cast"]["id"]."~".$cast["Cast"]["name"]; ?>"><?php echo $cast["Cast"]["name"]; ?></option>
										<?php } ?>
										
									</select>
                                </div>
                                <div class="input-field col s6">
                                  <i class="material-icons prefix">g_translate</i>
                                  <input id="tagline" name="tagline" type="text" class="validate">
                                  <label for="icon_prefix">Tag Line</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">live_tv</i>
                                    <input id="dob" name="dob" type="text" class="validate">
                                    <label for="icon_prefix">Date Of Birth</label>
                                </div>                               
                                <div class="input-field col s12">
                                  <i class="material-icons prefix">assignment</i> 
                                  <input id="decription" name="decription" type="text" class="validate">
                                  <label for="icon_prefix">Artist Description</label>
                                </div>                               
							</div>                             
							
							<div class="input-field col s12">
								<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityTab">Next</a>
							</div>
						</div>
						<div id="identity" class="col s12 tab-content">
							<div class = "row  upload-data">
								<div class="errorMsgFrm" style="display:none;"></div>
								<div class = "col s6">
									<div class="file_icon">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('coverImg', array('type'=>'file','label'=>'','id'=>'coverImg'));?>	
										<label>Choose Cover Image</lable>
									</div>                                 
								</div>
								<div class = "col s6">
									<div class="file_icon banner-image">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('userImg', array('type'=>'file','label'=>'','id'=>'userImg'));?>	
								<label>Choose user image</label>
									</div>
								</div>
								
								<div class="input-field col s12">
									<a class="waves-effect waves-light btn left red darken-1" href ="javascript:void(0)" id="identityPrevTab">Previous</a>
									<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityNextTab">Next</a>
								</div>
							</div>
						</div>
						<div id="socialConnect" class="col s12 tab-content">
							<div class = "social-container">
								<div class = "facebook-container">
									<h2>Facebook</h2>
									<img src = "<?php echo BASE_URL; ?>/img/facebook.png" class = "left" alt = "" />
									<span class = "left">Connect your facebook page/profile</span>  
									<!--<a class="waves-effect waves-light btn right red darken-1" href = "#">Connect</a>-->
									<textarea placeholder = "Facebook Id" id="facaebookId" name="facaebookId" class="materialize-textarea" ></textarea>	
									<textarea placeholder = "Facebook Page Id" id="facebookPageId" name="facebookPageId" class="materialize-textarea" ></textarea>
									
								</div>

								<span class = "spacer"></span>
								<div class = "youtube-container">
									<h2>Twitter</h2>
									<img src = "<?php echo BASE_URL; ?>/img/twitter.png" class = "left" alt = "" />  
									<span class = "left">Connect your twitter Channel</span>
									<!--<a class="waves-effect waves-light btn right red darken-1" href = "#">Connect</a>-->
									<textarea placeholder = "Google Id" id="googleId" name="googleId" class="materialize-textarea"></textarea>	
									<textarea placeholder = "Twitter Id" id="tweeterId" name="tweeterId" class="materialize-textarea" ></textarea>									
								</div>
								<span class = "spacer"></span>
								<div class="input-field col s12">
									<a class="waves-effect waves-light btn left red darken-1" href = "javascript:void(0)" id="legalInfoPrevTab">Previous</a>
									<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="submitForm">Finish</a>
								</div>   
							</div>
						</div>
						
					</form>
				</div>  
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$('#tabberDisableBasic').click(function(){ return false});
	$('#tabberDisableIdentity').click(function(){ return false});
	$('#tabberDisableSocialConnect').click(function(){ return false});
	$("#identityTab").click(function(){
	    
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var dob = $.trim($('#dob').val());
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if( $('#country_code').val() == "") {
		    $('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if( $('#roleId').val() == "") {
		    $('#roleId').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select role.");
			return false;
		}
		
		if(dob ==""){
		    $('#dob').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the date of birth.");
			return false;
		}
		
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 214px; left: 213px;");
		return true;
		
	})
	
	$("#identityPrevTab").click(function(){
		$("#identity").css("display", "none");
		$("#basicinfo").css("display", "block");
		$("#tabberDisableBasic").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 0px;");
	})
	$("#identityNextTab").click(function(){
		
		var coverImg = $('#coverImg').val().split('.').pop().toLowerCase();
		var userImg = $('#userImg').val().split('.').pop().toLowerCase();
		
		if(coverImg ==""){
			$('#coverImg').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload cover image.");
			return false;
		}
		
		if(coverImg !="" && $.inArray(coverImg, ['png','jpg','jpeg']) == -1){
			$('#coverImg').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload cover image as png, jpg, jpeg.");
			return false;
		}
		
		if(userImg ==""){
			$('#userImg').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload user image.");
			return false;
		}
		
		if(userImg !="" && $.inArray(userImg, ['png','jpg','jpeg']) == -1){
			$('#c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload user image as png, jpg, jpeg.");
			return false;
		}
		
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "none");
		$("#socialConnect").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableSocialConnect").addClass("active");
		$(".indicator").attr("style", "right: 1px; left: 427px;");
		return true;
		
	})
	
	$("#legalInfoPrevTab").click(function(){
		$("#basicinfo").css("display", "none");
		$("#socialConnect").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableSocialConnect").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 214px; left: 213px;");
	})
	
	$("#submitForm").click(function(){
		document.addArtist.submit();
	})
	
	
})

</script>

<script type = "text/javascript" >
history.pushState(null, null, 'addartist');
window.addEventListener('popstate', function(event) {
history.pushState(null, null, 'addartist');
});
</script>