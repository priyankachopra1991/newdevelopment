<?php $theComponent = new CommonComponent(new ComponentCollection());  ?>
<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchArtist" id="searchArtist" action="" method="get">
			<div class = "sort-section">
				<div class = "row">                    
					<div class = "col s4">
						<label class = "left">Sort By:</label>                    
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc"  <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select>                   
					</div>
					<div class = "col s4">
						<label left>Filter By:</label>                    
						<select id="filterBy" name="filterBy">
							<option value="">Filter By</option>
							<option value="1" <?php if(@$_GET['filterBy'] == "1"){ echo "selected"; } ?>>Approved</option>
							<option value="0" <?php if(@$_GET['filterBy'] == "0"){ echo "selected"; } ?>>Submitted For Approval</option>
							<option value="2" <?php if(@$_GET['filterBy'] == "2"){ echo "selected"; } ?>>Rejected</option>
							
						</select>  
					</div>
					<div class = "col s4">
						<a href = "<?php echo BASE_URL; ?>/artist/addartist" class="waves-effect waves-light btn red darken-1 right add-channel"><i class="material-icons left">add</i>Add Artist</a>
					</div>
				</div>                 
			</div>
		</form>
	<!-- end of sort section div -->
	</div>

	<?php if(!empty($artists))
	{ ?>
		<?php foreach($artists as $artist){			 
		?>
			<div class = "col s4">
				<div class="row">
					<div class="col s12 m12">
						<div class="card">
							<a href = "<?php echo BASE_URL."/artist/detailartist/".$artist["ar_artist"]["id"]; ?>">
								<div class="card-image">
									<img class = "z-depth-3" src="<?php echo AMAZONIMAGEURL.$artist["ar_artist"]["coverImg"]; ?>">                          
								</div>
							</a>
							<div class="card-content">
								<span class="card-title"><?php echo substr($artist["ar_artist"]["name"], 0,25); ?></span>
								<ul class="channel-overview-list">
									<li><i class = "material-icons tiny">visibility</i> <?php echo (empty($artist["ar_artist"]["videoViewCount"]) || $artist["ar_artist"]["videoViewCount"] <=0) ? '0' : $theComponent->changeNumberStyle($artist["ar_artist"]["videoViewCount"]); ?> views</li>
									<li><i class = "material-icons tiny">video_library</i> <?php echo (empty($artist["ar_artist"]["videoCount"]) || $artist["ar_artist"]["videoCount"] <=0) ? '0' : $theComponent->changeNumberStyle($artist["ar_artist"]["videoCount"]); ?> videos</li>
								</ul>
							</div>
							<div class="card-action">                            
								<?php 
								 
								if($artist['ar_artist']['status'] == "1")
								{ ?>
							     <a class = "green accent-4">Approved</a>  
                                <?php } 
								else if($artist['ar_artist']['status'] == "2")
								{ ?>	
							     <a class = "red accent-4">Rejected</a>
								<?php } 
								else { ?>
									<a class = "amber accent-3">Submitted for approval</a>
								<?php } ?>								
							</div>
							<br/>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php } else { ?>
	
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
</div>

<?php if((!empty($artists)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/artist/viewartist";
	
	/*if(@$_GET["searchBy"] !=""){
		$searchBy = @$_GET["searchBy"];
	} else {
		$searchBy = "";
	}
	if(@$_GET["searchString"] !=""){
		$searchString = @$_GET["searchString"];
	} else {
		$searchString = "";
	}*/
	if(@$_GET["sortBy"] !=""){
		$sortBy = @$_GET["sortBy"];
	} else {
		$sortBy = "";
	}
	
	if(@$_GET["filterBy"] !=""){
		$filterBy = @$_GET["filterBy"];
	} else {
		$filterBy = "";
	}
	
	
	if(@$_GET["selectvalue"] !=""){
		$selectvalue = @$_GET["selectvalue"];
	} else {
		$selectvalue = @$_POST["selectvalue"];
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&searchBy='.@$searchBy.'&searchString='.@$searchString.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>

<script type="text/javascript">
$(document).ready(function(){
	$("#sortBy").change(function(){
		document.searchArtist.submit();
	
	})
	$("#filterBy").change(function(){
		document.searchArtist.submit();
	
	})
})
</script>	
