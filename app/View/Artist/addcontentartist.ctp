<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Add Content</h2>                          
								</div> 
							</div>
							<div class = "col s12">
								<div class = "row">
								<div class="errorMsgFrm" style="display:none;"></div>
								<div class="input-field col s12"> 
									<select name="artist_detail" id="artist_detail">
										<option value="">Artist</option>
										<?php foreach($artists as $artistsRec){ ?>
											<option value="<?php echo $artistsRec["ar_artist"]["id"]; ?>"><?php echo $artistsRec["ar_artist"]["name"]; ?></option>
										<?php } ?>
									</select>
								</div>
									<div class="input-field col s12">                                 
										<!--<div class="chips">
										</div>-->
										<ul id="contentListValue"  style="border-bottom:1px;"></ul>
									</div>
									<div class="input-field col s12">
										<h2>Searching  asynchronously.</h2>
										<input placeholder="To" id="searchKeyword" name="searchKeyword" type="text" class="validate">
										<div id="listContent" style="display:none;">
										<select name="contentOpt[]" id="contentOpt" style="height:200px;" multiple>
										<select>
										</div>
									</div>    
								</div>                    
							</div>                       
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#artist_detail").val('');
	$("#searchKeyword").val('');
	$("#searchKeyword").keyup(function(){
	    
		var searchValue = $("#searchKeyword").val();
		var searchValueCount = $("#searchKeyword").val().length;
		var userId = '<?php echo $this->Session->read('User.id'); ?>';
		var artistVal = $('#artist_detail').val();
		
		//alert(searchValueCount);
		 if( $('#artist_detail').val() == "") {
			$('#artist_detail').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select artist.");
			return false;
		}
		if(searchValue !="" && searchValueCount > 2){
		  $.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/contentartistsearch.php',
			'data':'searchValue='+searchValue+'&userId='+userId+'&artistVal='+artistVal,
				'success':function(resp){
				    //alert(resp);
					if(resp != "")
					{
						$("#contentOpt").css("display", "block");
						$('#contentOpt').material_select('destroy');
						$("#listContent").css("display", "block");
						$('#contentOpt').html(resp);
					}
				}
			});
		}
	})
	$("#contentOpt").click(function(){
	    var artistVal = $('#artist_detail').val();
		var contentVal = $(this).val();
		var searchValue = $("#searchKeyword").val();
		var userId = '<?php echo $this->Session->read('User.id'); ?>';
		$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/addcontentartist.php',
			'data':'artistVal='+artistVal+'&contentVal='+contentVal+'&searchValue='+searchValue,
			'success':function(resp){
				//alert(resp);
				//$("#contentOpt").css("display", "block");
				//$('#contentOpt').material_select('destroy');
				$('#contentListValue').html(resp);
				searchValueRecord(searchValue, userId, artistVal);
				//$(this).val().css("display", "none");
			}
		});
	})
	
	$("#artist_detail").change(function(){
		var artistVal = $('#artist_detail').val();
		if(artistVal !=""){
		   $.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/ajaxdata/contentartistdata.php',
				'data':'artistVal='+artistVal,
				'success':function(resp){
					//alert(resp);
					//$("#contentOpt").css("display", "block");
					//$('#contentOpt').material_select('destroy');
					$('#contentListValue').html(resp);
				}
			});
		}
		
	})
	
	$('#contentListValue').on('click', 'li', function(){
	    var artistVal = $('#artist_detail').val();
	    var artistContentVal = $(this).val();
		//alert(seriesContentVal);
		if(artistContentVal !=""){
		   $.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/ajaxdata/contentartistdelete.php',
				'data':'artistContentVal='+artistContentVal,
				'success':function(resp){
					//alert(resp);
					//$("#contentOpt").css("display", "block");
					//$('#contentOpt').material_select('destroy');
					$('#contentListValue').html(resp);
					searchAddedRecord(artistVal);
				}
			});
		}
	});
})
	
function searchValueRecord(searchValue, userId, artistVal)
{
    //alert("hello raghav");
    $.ajax({
	'type':'post',
	'url':'<?php echo BASE_URL;?>/ajaxdata/contentartistsearch.php',
	'data':'searchValue='+searchValue+'&userId='+userId+'&artistVal='+artistVal,
		'success':function(resp){
			//alert(resp);
			if(resp != "")
			{
				$("#contentOpt").css("display", "block");
				$('#contentOpt').material_select('destroy');
				$('#contentOpt').html(resp);
			}
		}
	});
}

function searchAddedRecord(artistVal)
{
	if(artistVal !=""){
	   $.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/contentartistdata.php',
			'data':'artistVal='+artistVal,
			'success':function(resp){
				//alert(resp);
				//$("#contentOpt").css("display", "block");
				//$('#contentOpt').material_select('destroy');
				$('#contentListValue').html(resp);
			}
		});
	}
}
</script>