<script>
$(function() {
	$("#dob").datepicker( { "dateFormat":"yy-mm-dd" } );
});
 
</script>
<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" name="editArtist" id="editArtist" method="post" action="<?php echo BASE_URL."/artist/editartist/".$artists[0]["ar_artist"]["id"]; ?>" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit Artist</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<input type="hidden" name="user_id" value="<?php echo $artists[0]["ar_artist"]["user_id"]; ?>" />
							<div class = "col s8">
								<div class = "row">
									<div class="input-field col s12">
										<input placeholder="Artist Name" id="name" name="name" value="<?php echo $artists[0]["ar_artist"]["name"]; ?>" type="text" class="validate">
									</div>
								<div class="input-field col s12">
									<input placeholder="Artist Id" id="id" name="id" value="<?php echo $artists[0]["ar_artist"]["id"]; ?>" type="text" readonly="readonly" class="validate">
								</div>
								<div class="input-field series-file file_icon col s12">
									<label for="channel">Choose cover image</label>
									<i class = "material-icons">perm_media</i>
									<!--<input type="file" id="c_banner" name="c_banner">-->
									
									<?php echo $this->Form->input('coverImg', array('type'=>'file','label'=>'','id'=>'coverImg'));?>
									<br clear="all" />
									
									<img src="<?php echo AMAZONIMAGEURL.$artists[0]["ar_artist"]["coverImg"]; ?>" height="100" width="100" />
									<?php
									echo $this->Form->input('hiddenimage1',array('type'=>'hidden','value'=>$artists[0]["ar_artist"]["coverImg"]));
									?>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "decription" id="decription" name="decription" class="materialize-textarea" ><?php echo $artists[0]["ar_artist"]["decription"]; ?></textarea>
								</div>
								
								<div class="input-field col s12">
									
									<select name="roleId" id="roleId">
										<option value="">Role</option>
										<?php foreach($casts as $cast){ ?>
											<option value="<?php echo $cast["Cast"]["id"]."~".$cast["Cast"]["name"]; ?>" <?php if($cast["Cast"]["id"] == $artists[0]["ar_artist"]["roleId"]){ echo "selected"; } ?>><?php echo $cast["Cast"]["name"]; ?></option>
										<?php } ?>
										
									</select>
								</div>
								<div class="input-field col s12">
									
									<select name="country_code" id="country_code">
										<option value="">Country</option>
										<?php foreach($country as $country){ ?>
											<option value="<?php echo $country["Country"]["country_code"]; ?>" <?php if($country["Country"]["country_code"] == $artists[0]["ar_artist"]["country_code"]){ echo "selected"; } ?>><?php echo $country["Country"]["country_name"]; ?></option>
										<?php } ?>
										
									</select>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Tagline" id="tagline" name="tagline" class="materialize-textarea" ><?php echo $artists[0]["ar_artist"]["tagline"]; ?></textarea>
								</div>
								<input type="hidden" name="artistInfoId" id="artistInfoId" value="<?php echo $artistInfo[0]["ar_artists_socialInfo"]["id"]; ?>" />
								<div class="input-field col s12">
									<textarea placeholder = "Facebook Id" id="facaebookId" name="facaebookId" class="materialize-textarea" ><?php echo $artistInfo[0]["ar_artists_socialInfo"]["facaebookId"]; ?></textarea>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Facebook Page Id" id="facebookPageId" name="facebookPageId" class="materialize-textarea" ><?php echo $artistInfo[0]["ar_artists_socialInfo"]["facebookPageId"]; ?></textarea>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Google Id" id="googleId" name="googleId" class="materialize-textarea" ><?php echo $artistInfo[0]["ar_artists_socialInfo"]["googleId"]; ?></textarea>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Twitter Id" id="tweeterId" name="tweeterId" class="materialize-textarea" ><?php echo $artistInfo[0]["ar_artists_socialInfo"]["tweeterId"]; ?></textarea>
								</div>
							</div>                    
						</div>
						<div class = "col s4">
							<div class = "row">                                                    
							<div class="input-field series-icon file_icon col s12">                             
								<i class = "material-icons">perm_media</i>
								<?php echo $this->Form->input('userImg', array('type'=>'file','label'=>'','id'=>'userImg'));?>								
								<label for="channel_icon">Choose user image</label> 
								<img src="<?php echo AMAZONIMAGEURL.$artists[0]["ar_artist"]["userImg"]; ?>" height="100" width="100" />
                                <?php
								echo $this->Form->input('hiddenimage2',array('type'=>'hidden','value'=>$artists[0]["ar_artist"]["userImg"]));
								?>
							</div>
							<div class="input-field col s12">
								<input placeholder="Date of birth" id="dob" name="dob" value="<?php echo $artists[0]["ar_artist"]["dob"]; ?>" type="text" class="validate">
							</div>
							<?php if($this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
							 <input type="hidden" name="oldstatus" value="<?php echo $artists[0]["ar_artist"]["status"]; ?>" />
								<div class="input-field col s12">
									<select>
										<option value="">Status</option>
										<option value="1" <?php if($artists[0]["ar_artist"]["status"] == "1"){ echo "selected"; } ?>>Approval</option>
										<option value="2" <?php if($artists[0]["ar_artist"]["status"] == "2"){ echo "selected"; } ?>>Rejected</option>
										<option value="0" <?php if($artists[0]["ar_artist"]["status"] == "0"){ echo "selected"; } ?>>Wating for Approval</option>
									</select>
								</div>
							<?php } ?>
							
						</div>                          
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $("#editArtist")[0].reset();
	})
	$("#submitForm").click(function(){
	    var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var coverImg = $('#coverImg').val().split('.').pop().toLowerCase();
		var userImg = $('#userImg').val().split('.').pop().toLowerCase();
		var dob = $.trim($('#dob').val());
		var userType = '<?php echo $this->Session->read('User.user_type'); ?>';
		
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if(coverImg !="" && $.inArray(coverImg, ['png','jpg','jpeg']) == -1){
			$('#coverImg').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload cover image as png, jpg, jpeg.");
			return false;
		}
		
		if( $('#roleId').val() == "") {
			$('#roleId').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select role.");
			return false;
		}
		
		if( $('#country_id').val() == "") {
			$('#country_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if(userImg !="" && $.inArray(userImg, ['png','jpg','jpeg']) == -1){
			$('#userImg').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload user image as png, jpg, jpeg.");
			return false;
		}
		
		if(dob ==""){
		    $('#dob').focus();
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the date of birth.");
			return false;
		}
		
		if(userType == 'V3MO' || userType == 'V3MOA'){
			if( $('#status').val() == "") {
				$('#status').focus() ;
				$(".errorMsgFrm").css("display", "block");
				$('.errorMsgFrm').html("Please select status.");
				return false;
			}
		}
		document.editArtist.submit();
	})
	
})

</script>	