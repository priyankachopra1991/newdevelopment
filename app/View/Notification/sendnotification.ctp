<script>
$(function() {
	$("#datepicker").datepicker( { "dateFormat":"yy-mm-dd" } );
	$("#datepicker1").datepicker( { "dateFormat":"yy-mm-dd" } );
});
 function uploadFile1(contentId){
    //alert("hello raghav");
    var splitVal = contentId.split('_');
    var dynamicId = splitVal[1];
	var file = document.getElementById(contentId).files[0];
	var formdata = new FormData();
	formdata.append("contentId", file);
	var ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress",  function (event) {
	    //alert(event);
		//document.getElementById("loaded_n_total_" + dynamicId).innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
	    document.getElementById("progressBar_" + dynamicId).value = Math.round(percent);
		//document.getElementById("status_" + dynamicId).innerHTML = Math.round(percent)+"% uploaded... please wait";
	});

	ajax.addEventListener("load",  function (event) {
		//document.getElementById("status_" + dynamicId).innerHTML = event.target.responseText;
		//document.getElementById("progressBar_" + dynamicId).value = 0;
	});
	
	ajax.addEventListener("error",  function (event) {
		document.getElementById("status_" + dynamicId).innerHTML = "Upload Failed";
	});
	ajax.addEventListener("abort",  function (event) {
		document.getElementById("status_" + dynamicId).innerHTML = "Upload Aborted";
	});
	
	ajax.open("POST", "");
	ajax.send(formdata);
}

function uploadFile2(contentId){
    var splitVal = contentId.split('_');
    var dynamicId = splitVal[1];
	var file = document.getElementById(contentId).files[0];
	var formdata = new FormData();
	formdata.append("contentId", file);
	var ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress",  function (event) {
		//document.getElementById("loaded_n_total2_" + dynamicId).innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
	    document.getElementById("progressBar2_" + dynamicId).value = Math.round(percent);
	    //document.getElementById("status2_" + dynamicId).innerHTML = Math.round(percent)+"% uploaded... please wait";
	});

	ajax.addEventListener("load",  function (event) {
		//document.getElementById("status2_" + dynamicId).innerHTML = event.target.responseText;
		//document.getElementById("progressBar2_" + dynamicId).value = 0;
	});
	
	ajax.addEventListener("error",  function (event) {
		document.getElementById("status2_" + dynamicId).innerHTML = "Upload Failed";
	});
	ajax.addEventListener("abort",  function (event) {
		document.getElementById("status2_" + dynamicId).innerHTML = "Upload Aborted";
	});
	
	ajax.open("POST", "");
	ajax.send(formdata);
}

function uploadFile3(contentId){
    var splitVal = contentId.split('_');
    var dynamicId = splitVal[1];
	var file = document.getElementById(contentId).files[0];
	var formdata = new FormData();
	formdata.append("contentId", file);
	var ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress",  function (event) {
		//document.getElementById("loaded_n_total3_" + dynamicId).innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
	    document.getElementById("progressBar3_" + dynamicId).value = Math.round(percent);
	    //document.getElementById("status3_" + dynamicId).innerHTML = Math.round(percent)+"% uploaded... please wait";
	});

	ajax.addEventListener("load",  function (event) {
		//document.getElementById("status3_" + dynamicId).innerHTML = event.target.responseText;
		//document.getElementById("progressBar3_" + dynamicId).value = 0;
	});
	
	ajax.addEventListener("error",  function (event) {
		document.getElementById("status3_" + dynamicId).innerHTML = "Upload Failed";
	});
	ajax.addEventListener("abort",  function (event) {
		document.getElementById("status3_" + dynamicId).innerHTML = "Upload Aborted";
	});
	
	ajax.open("POST", "");
	ajax.send(formdata);
}
</script>
<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class="edit-form" method="post" action="<?php echo BASE_URL."/notification/sendnotification"; ?>" id="sendnotification" name="sendnotification" enctype="multipart/form-data" method="post">
					    <div id="sections">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Send Notification</h2>
								</div>
								
								<div class = "input-field inner-button col s12 custom-setting">
								<input type="submit" value= "Send" style="padding:10px 10px !important" class="waves-effect waves-light btn blue darken-1"/>
								<a href= "<?php echo BASE_URL."/users/dashboard"; ?>" class="waves-effect waves-light btn red darken-1" id="">Cancel</a>
									<!----<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Send</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>-->
								</div>
								
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
								<div class = "row">
									<div class="input-field col s12">
										<select name="topic" id="topic">
											<option value="">Select To</option>
											<option value="/topics/news">News</option>
						                    <option value="/topics/dev">Dev</option>
										</select>
									</div>                              
									<div class="input-field col s12">
									<input type="text" id="title" name="title" class="validate" placeholder="Title" required>				
									</div>
									
									<div class="input-field col s12">
										<input type="text" id="tags" name="tags" placeholder="Tags" required>
									</div>
									
									<div class="input-field series-file file_icon col s12">
									<label for="channel">Upload Image*(format - png,jpeg)</label>
									<i class = "material-icons">perm_media</i>
									<?php echo $this->Form->input('image_link', array('type'=>'file','label'=>'','id'=>'image_link', 'required'=>true, 'class'=>'form-control'));?>								
									</div>
								</div>                    
							</div>
							<div class = "col s6">
							<div class = "row">         
								    <div class="input-field col s12">
										<select name="app" id="app">
											<option value="">Select For</option>
											<option value="Firstcut">Firstcut</option>
										</select>
									</div>                                   
								<div class="input-field col s12">
									<input type="text" id="deep_link" name="deep_link" class="validate" placeholder="deep_link" required>				
								</div>
				                <div class="input-field col s12">
									<input id="message" name="message" type="text" placeholder="Message" required>
								</div>
							</div>
                            							
						</div>
						</div>
					</form> 
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	

<script type="text/javascript">
$(document).ready(function () {
    $('select').material_select();
    var app_slug = 'firstcut';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
			//$("#subcategory_id").css("display", "block");
			//$('#subcategory_id').html(resp);
			//$('.subcategory_id').html(resp);
			$selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#subcategory_id').material_select();
		}
	});
	
	//fetch language data onload...
	var countryCode = '';
	var languageCode = '';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp);
			$selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#language_code').material_select();
		}
	});
});	

function getLanguage(countryCode){
	$('select').material_select();
	var languageCode = '';
	//alert(countryCode);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp
			$selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#language_code').material_select();
		}
	});
}
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


</script>

