<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Add Content</h2>                          
								</div> 
							</div>
							<div class = "col s12">
								<div class = "row">
								<div class="errorMsgFrm" style="display:none;"></div>
								<div class="input-field col s12"> 
									<select name="series_detail" id="series_detail">
										<option value="">Series</option>
										<?php foreach($series as $seriesRec){ ?>
											<option value="<?php echo $seriesRec["ch_series"]["id"]; ?>"><?php echo $seriesRec["ch_series"]["s_name"]; ?></option>
										<?php } ?>
									</select>
								</div>
									<div class="input-field col s12">                                 
										<!--<div class="chips">
										</div>-->
										<ul id="contentListValue"  style="border-bottom:1px;"></ul>
									</div>
									<div class="input-field col s12">
										<h2>Searching  asynchronously.</h2>
										<input placeholder="To" id="searchKeyword" name="searchKeyword" type="text" class="validate">
										<div id="listContent" style="display:none;">
										<select name="contentOpt[]" id="contentOpt" style="height:200px;" multiple>
										<select>
										</div>
									</div>    
								</div>                    
							</div>                       
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#series_detail").val('');
	$("#searchKeyword").val('');
	$("#searchKeyword").keyup(function(){
		var searchValue = $("#searchKeyword").val();
		var searchValueCount = $("#searchKeyword").val().length;
		var userId = '<?php echo $this->Session->read('User.id'); ?>';
		var seriesVal = $('#series_detail').val();
		
		//alert(searchValueCount);
		 if( $('#series_detail').val() == "") {
			$('#series_detail').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select series.");
			return false;
		}
		if(searchValue !="" && searchValueCount > 2){
		  $.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/contentsearch.php',
			'data':'searchValue='+searchValue+'&userId='+userId+'&seriesVal='+seriesVal,
				'success':function(resp){
				    //alert(resp);
					if(resp != "")
					{
						$("#contentOpt").css("display", "block");
						$('#contentOpt').material_select('destroy');
						$("#listContent").css("display", "block");
						$('#contentOpt').html(resp);
					}
				}
			});
		}
	})
	$("#contentOpt").click(function(){
	    var seriesVal = $('#series_detail').val();
		var contentVal = $(this).val();
		var searchValue = $("#searchKeyword").val();
		var userId = '<?php echo $this->Session->read('User.id'); ?>';
		$.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/addcontentseries.php',
			'data':'seriesVal='+seriesVal+'&contentVal='+contentVal+'&searchValue='+searchValue,
			'success':function(resp){
				//alert(resp);
				//$("#contentOpt").css("display", "block");
				//$('#contentOpt').material_select('destroy');
				$('#contentListValue').html(resp);
				searchValueRecord(searchValue, userId, seriesVal);
				//$(this).val().css("display", "none");
			}
		});
	})
	
	$("#series_detail").change(function(){
		var seriesVal = $('#series_detail').val();
		if(seriesVal !=""){
		   $.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/ajaxdata/contentseriesdata.php',
				'data':'seriesVal='+seriesVal,
				'success':function(resp){
					//alert(resp);
					//$("#contentOpt").css("display", "block");
					//$('#contentOpt').material_select('destroy');
					$('#contentListValue').html(resp);
				}
			});
		}
		
	})
	
	$('#contentListValue').on('click', 'li', function(){
	    var seriesVal = $('#series_detail').val();
	    var seriesContentVal = $(this).val();
		//alert(seriesContentVal);
		if(seriesContentVal !=""){
		   $.ajax({
				'type':'post',
				'url':'<?php echo BASE_URL;?>/ajaxdata/contentseriesdelete.php',
				'data':'seriesContentVal='+seriesContentVal,
				'success':function(resp){
					//alert(resp);
					//$("#contentOpt").css("display", "block");
					//$('#contentOpt').material_select('destroy');
					$('#contentListValue').html(resp);
					searchAddedRecord(seriesVal);
				}
			});
		}
	});
})
	
function searchValueRecord(searchValue, userId, seriesVal)
{
    //alert("hello raghav");
    $.ajax({
	'type':'post',
	'url':'<?php echo BASE_URL;?>/ajaxdata/contentsearch.php',
	'data':'searchValue='+searchValue+'&userId='+userId+'&seriesVal='+seriesVal,
		'success':function(resp){
			//alert(resp);
			if(resp != "")
			{
				$("#contentOpt").css("display", "block");
				$('#contentOpt').material_select('destroy');
				$('#contentOpt').html(resp);
			}
		}
	});
}

function searchAddedRecord(seriesVal)
{
	if(seriesVal !=""){
	   $.ajax({
			'type':'post',
			'url':'<?php echo BASE_URL;?>/ajaxdata/contentseriesdata.php',
			'data':'seriesVal='+seriesVal,
			'success':function(resp){
				//alert(resp);
				//$("#contentOpt").css("display", "block");
				//$('#contentOpt').material_select('destroy');
				$('#contentListValue').html(resp);
			}
		});
	}
}
</script>