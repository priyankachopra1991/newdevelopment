<div class = "table-format-data">
	<div class = "row">
		<div class = "col s8 offset-s2">                     
			<div class = "add-channel-form">
				<div class = "channel-header">
					<h2>Add Series</h2> 
					<p>This information will let us know more about your channel</p>
				</div>
				<div class="row">
					<div class="col s12">
						<ul class="tabs series-tabs">
							<li class="tab col s6"><a class="active" href="#basicinfo" id="tabberDisableBasic">Basic Info</a></li>
							<li class="tab col s6"><a href="#identity" id="tabberDisableIdentity">Appearance</a></li>
						</ul>
					</div>
					<form name="addSeries" id="addSeries" action="<?php echo BASE_URL; ?>/series/addseries" method="post" enctype="multipart/form-data">
						<div id="basicinfo" class="col s12 tab-content">
						
							<div class = "row">
							<div class="errorMsgFrm" style="display:none;"></div>
								<div class="input-field col s6">
									<i class="material-icons prefix">live_tv</i>
									<input type="text" name="s_name" id="s_name" class="validate">
									<label for="icon_prefix">Name</label>
								</div>
								<div class="input-field col s6">
									<select name="channel_id" id="channel_id">
										<option value="">Choose Channel</option>
										<?php foreach($channels as $channel){ ?>
											<option value="<?php echo $channel["ch_channel"]["id"]; ?>"><?php echo $channel["ch_channel"]["c_name"]; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">assignment</i> 
									<input type="text" name="s_aboutus" id="s_aboutus" class="validate">
									<label for="icon_prefix">Description</label>
								</div>                                
							</div>                             
							<div class="input-field col s12">
								<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityTab">Next</a>
							</div>
						</div>
						<div id="identity" class="col s12 tab-content">
							<div class = "row upload-data">
							 <div class="errorMsgFrm" style="display:none;"></div>
								<div class = "col s6">
									<div class="file_icon">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('s_banner', array('type'=>'file','label'=>'','id'=>'s_banner'));?>
										<label>Series Banner</label>
									</div>                                 
								</div>
								<div class = "col s6">
									<div class="file_icon banner-image">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('s_s_banner', array('type'=>'file','label'=>'','id'=>'s_s_banner'));?>
										<label>Series Small Banner</label>
									</div>
								</div>
								<div class = "col s6">
									<div class="file_icon banner-image">
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('s_c_banner', array('type'=>'file','label'=>'','id'=>'s_c_banner'));?>
										<label>Series Cover Banner</label>
									</div>
								</div>
								<div class="input-field col s12">
									<a class="waves-effect waves-light btn left red darken-1" href = "javascript:void(0)" id="identityPrevTab">Previous</a>
									<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="submitForm">Finish</a>
								</div>
							</div>
						</div>
					</form>
				</div>          
			</div>
		</div>
	</div>	
</div>
<?php echo $this->element('flashmessage'); ?>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabberDisableBasic').click(function(){ return false});
	$('#tabberDisableIdentity').click(function(){ return false});
	$("#identityTab").click(function(){
	    var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var s_name = $.trim($('#s_name').val());
		
		if(s_name ==""){
		    $('#s_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(s_name)){
			$('#s_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if( $('#channel_id').val() == "") {
		    $('#channel_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select channel.");
			return false;
		}
		
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 1px; left: 320px;");
		return true;
		
	})
	
	$("#identityPrevTab").click(function(){
		$("#identity").css("display", "none");
		$("#basicinfo").css("display", "block");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableBasic").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 0px;");
	})
	
	$("#submitForm").click(function(){
		var s_banner = $('#s_banner').val().split('.').pop().toLowerCase();
		var s_s_banner = $('#s_s_banner').val().split('.').pop().toLowerCase();
		var s_c_banner = $('#s_c_banner').val().split('.').pop().toLowerCase();
		
		if(s_banner ==""){
			$('#s_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload series banner.");
			return false;
		}
		
		if(s_banner !="" && $.inArray(s_banner, ['png','jpg','jpeg']) == -1){
			$('#s_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload series banner as png, jpg, jpeg.");
			return false;
		}
		
		if(s_s_banner ==""){
			$('#s_s_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload series small banner.");
			return false;
		}
		
		if(s_s_banner !="" && $.inArray(s_s_banner, ['png','jpg','jpeg']) == -1){
			$('#s_s_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload series small banner as png, jpg, jpeg.");
			return false;
		}
		
		if(s_c_banner ==""){
			$('#s_c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload series cover banner.");
			return false;
		}
		
		if(s_c_banner !="" && $.inArray(s_c_banner, ['png','jpg','jpeg']) == -1){
			$('#s_c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload series cover banner as png, jpg, jpeg.");
			return false;
		}
		
		document.addSeries.submit();
	})
})
</script>

<script type = "text/javascript" >
history.pushState(null, null, 'addseries');
window.addEventListener('popstate', function(event) {
history.pushState(null, null, 'addseries');
});
</script>
