<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" name="editSeries" id="editSeries" method="post" action="<?php echo BASE_URL."/series/editseries/".$series[0]["t1"]["id"]; ?>" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit Series</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<input type="hidden" name="user_id" value="<?php echo $series[0]["t1"]["user_id"]; ?>" />
							<div class = "col s6">
								<div class = "row">
									<div class="input-field col s12">
										<input placeholder="Series Name" id="s_name" name="s_name" value="<?php echo $series[0]["t1"]["s_name"]; ?>" type="text" class="validate">
									</div>
								<div class="input-field col s12">
									<input placeholder="Series Id" id="id" name="id" value="<?php echo $series[0]["t1"]["id"]; ?>" type="text" readonly="readonly" class="validate">
								</div>
								<div class="input-field series-file file_icon col s12">
									<label for="channel">Choose Series Banner</label>
									<i class = "material-icons">perm_media</i>
									<!--<input type="file" id="c_banner" name="c_banner">-->
									
									<?php echo $this->Form->input('s_banner', array('type'=>'file','label'=>'','id'=>'s_banner'));?>
									<br clear="all" />
									
									<img src="<?php echo AMAZONIMAGEURL.$series[0]["t1"]["s_banner"]; ?>" height="100" width="100" />
									<?php
									echo $this->Form->input('hiddenimage1',array('type'=>'hidden','value'=>$series[0]["t1"]["s_banner"]));
									?>
								</div>
								<div class="input-field col s12">
									<textarea placeholder = "Description" id="s_aboutus" name="s_aboutus" class="materialize-textarea" ><?php echo $series[0]["t1"]["s_aboutus"]; ?></textarea>
								</div>
								
								<div class="input-field col s12">
								
								<select name="channel_id" id="channel_id">
									<option value="">Channel</option>
									<?php foreach($channels as $channel){ ?>
										<option value="<?php echo $channel["ch_channel"]["id"]; ?>" <?php if($channel["ch_channel"]["id"] == $series[0]["t1"]["channel_id"]){ echo "selected"; } ?>><?php echo $channel["ch_channel"]["c_name"]; ?></option>
									<?php } ?>
									
								</select>
							</div>
							</div>                    
						</div>
						<div class = "col s6">
							<div class = "row">                                                    
							<div class="input-field series-icon file_icon col s12">
                                    <ul style="display:inline;">
									<li>
									 <div class="file-field input-field">
                                          <div class="btn right red darken-1">
                                            <span>Browse</span>
                                            <input type="file" name = "data[s_s_banner]" id="s_s_banner" required>
                                          </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text">
                                        </div> 
                                        <span class = "spacer"></span>                                   
                                    </div>
									</li>
									<li>
									<img src="<?php echo AMAZONIMAGEURL.$series[0]["t1"]["s_s_banner"]; ?>" height="100" width="100" />
                                <?php
								echo $this->Form->input('hiddenimage2',array('type'=>'hidden','value'=>$series[0]["t1"]["s_s_banner"]));
								?>
									</li>
                                    </ul>									
							</div>
							<div class="input-field series-icon file_icon col s12">                             
								<i class = "material-icons">perm_media</i>
								<?php echo $this->Form->input('s_c_banner', array('type'=>'file','label'=>'','id'=>'s_c_banner'));?>								
								<label for="channel_icon">Choose Cover Banner</label>
								<img src="<?php echo AMAZONIMAGEURL.$series[0]["t1"]["s_c_banner"]; ?>" height="100" width="100" />
                                <?php
								echo $this->Form->input('hiddenimage3',array('type'=>'hidden','value'=>$series[0]["t1"]["s_c_banner"]));
								?>
							</div>
							
							<?php if($this->Session->read('User.user_type') == 'V3MO' || $this->Session->read('User.user_type') == 'V3MOA'){ ?>
							    <input type="hidden" name="oldstatus" value="<?php echo $series[0]["t1"]["status"]; ?>" />
								<div class="input-field col s12">
									<select name="status">
										<option value="">Status</option>
										<option value="1" <?php if($series[0]["t1"]["status"] == "1"){ echo "selected"; } ?>>Approved</option>
										<option value="2" <?php if($series[0]["t1"]["status"] == "2"){ echo "selected"; } ?>>Rejected</option>
										<option value="0" <?php if($series[0]["t1"]["status"] == "0"){ echo "selected"; } ?>>Wating for Approval</option>
									</select>
								</div>
							<?php } ?>
							
						</div>                          
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $("#editChannel")[0].reset();
	})
	$("#submitForm").click(function(){
	
	    var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var s_name = $.trim($('#s_name').val());
		var s_banner = $('#s_banner').val().split('.').pop().toLowerCase();
		var s_s_banner = $('#s_s_banner').val().split('.').pop().toLowerCase();
		var s_c_banner = $('#s_c_banner').val().split('.').pop().toLowerCase();
	
		var userType = '<?php echo $this->Session->read('User.user_type'); ?>';
		
		if(s_name ==""){
		    $('#s_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(s_name)){
			$('#s_name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		
		if( $('#channel_id').val() == "") {
		    $('#channel_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select channel.");
			return false;
		}
		
		if(s_banner !="" && $.inArray(s_banner, ['png','jpg','jpeg']) == -1){
			$('#s_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload banner as png, jpg, jpeg.");
			return false;
		}
		
		if(s_s_banner !="" && $.inArray(s_s_banner, ['png','jpg','jpeg']) == -1){
			$('#s_s_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload small banner as png, jpg, jpeg.");
			return false;
		}
		
		if(s_c_banner !="" && $.inArray(s_c_banner, ['png','jpg','jpeg']) == -1){
			$('#s_c_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload cover banner as png, jpg, jpeg.");
			return false;
		}
		
		if(userType == 'V3MO' || userType == 'V3MOA'){
			if( $('#status').val() == "") {
				$('#status').focus() ;
				$(".errorMsgFrm").css("display", "block");
				$('.errorMsgFrm').html("Please select status.");
				return false;
			}
		}
		
		document.editSeries.submit();
	})
	
})

</script>	