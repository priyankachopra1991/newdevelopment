<div class = "single-view-channel-section "> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<div class = "row">
						<div class = "col s7">
							<div class = "inner-title left">
								<h2><?php echo $series[0]["t1"]["s_name"]; ?></h2>
								<p>Series ID : <?php echo $series[0]["t1"]["id"]; ?></p>
							</div>
						</div>
						<div class = "col s5">
							<div class = "inner-button right">
								<a class="waves-effect waves-light btn green darken-1" href = "<?php echo BASE_URL."/series/addcontentseries"; ?>">Add Content</a>
								<a class="waves-effect waves-light btn orange darken-1" href = "<?php echo BASE_URL."/series/editseries/".$series[0]["t1"]["id"]; ?>">Edit</a>
								<?php if($this->Session->read('User.user_type') == 'CPA'){ ?>
									<a class="waves-effect waves-light btn red darken-1" href="<?php echo BASE_URL."/series/deleteseries/".$series[0]["t1"]["id"]; ?>" onclick="return ConfirmDelete();">Delete</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class = "col s7">  
					<div class = "inner-container">                          
						<div class = "inner-img">
							<img src = "<?php echo AMAZONIMAGEURL.$series[0]["t1"]["s_banner"]; ?>" alt = "" />
						</div>
						<div class = "inner-des">
							<h3>Description</h3>
							<p><?php echo empty($series[0]["t1"]["s_aboutus"]) ? 'NA' : $series[0]["t1"]["s_aboutus"]; ?> </p>
						</div>
						<div class = "inner-car">
							<h3>Channel</h3>
							<p><?php echo empty($series[0]["t2"]["c_name"]) ? 'NA' : $series[0]["t2"]["c_name"]; ?> </p>
						</div>
					</div>                     
				</div>
				<!-- end of col s8 div -->
				<div class = "col s5">                     
					<ul class = "approval-list">
						<li class = "list-hreading">Approval Status</li>
						<li>
						<?php if($series[0]["t1"]["status"] == 1){ ?>
						Approved
						<?php } else if ($series[0]["t1"]["status"] == 0){?>
						Submitted for approval
						<?php } else { ?>
						Inactive
						<?php } ?>
						
						</li>
					</ul> 
					<ul class = "Date-Created">
						<li class = "list-hreading">Date Created</li>
						<li>
						<?php echo date('d/m/Y', strtotime($series[0]["t1"]["created"])); ?>
						</li>
					</ul>
					<ul class = "Channel-listed">
						<li class = "list-hreading">Channel</li>
						<li><?php echo $series[0]["t2"]["c_name"]; ?></li>
					</ul>                    
				</div>
			<!-- end of col s4 div -->
			</div>
		</div>
	</div>
</div>

<script>
    function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
</script>   