<script>
$(function() {
	$("#datepicker").datepicker( { "dateFormat":"yy-mm-dd" } );
	$("#datepicker1").datepicker( { "dateFormat":"yy-mm-dd" } );
});
 
</script>
<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class="edit-form" method="post" action="<?php echo BASE_URL."/fcappcontents/addyoutube"; ?>" id="addyoutube" name="addyoutube" enctype="multipart/form-data" method="post">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Add Youtube Content</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
							<input type="hidden" name="app_slug" value="firstcut" />
								<div class = "row">
									<div class="input-field col s12">
										<select name="country_code" id="country_code" onchange="getLanguage(this.value);">
											<option value="">Country</option>
											<?php foreach($country as $countrylist){?>
												<option value="<?php echo $countrylist['cm_country_code']['country_code']; ?>"><?php echo $countrylist['cm_country_code']['country_name']; ?></option> 
											<?php } ?>
										</select>
									</div>                              
									<div class="input-field col s12">
										<input type="text" id="name" name="name" class="validate" placeholder="Name">
										 					
									</div>
									
									<div class="input-field col s12">
										<input type="text" id="content" name="content" class="validate"  placeholder="Content">
										<input type="hidden" name="time" vlue="<?php echo time();?>">	
									</div>
									<div class="input-field col s12">
										<input type="text" id="tags" name="tags" placeholder="Tags">
									</div>
									
									<?php if($user_type == "V3MO" || $user_type == "V3MOA" ){ ?>
										<div class="input-field col s12">
											<input type="text" id="display_order" name="display_order"  'onkeypress'='return isNumber(event)' placeholder="Display Order">
									</div>
									<?php } else { ?>
										<input type="text" id="display_order" name="display_order" style="display:none;">
									<?php } ?>
									<div class="input-field col s12">
										<input type="text" id="datepicker" name="release_date" value="<?php echo date('Y-m-d'); ?>" placeholder="Release Date">
										
									</div>
									<div class="input-field col s12">
										<input type="text" id="datepicker1" name="expiry_date" value=""  placeholder="Expiry Date">
											
									</div>
									<div class="input-field col s12">
										<input id="description" name="description" type="text" placeholder="Description">
									</div>
								</div>                    
							</div>
							<input type="hidden" name="price" value="10" />	
							<input type="hidden" name="tataprice" value="10" />	
							<input type="hidden" name="vodafoneprice" value="10" />	
							<input type="hidden" name="mtnlprice" value="10" />	
							<input type="hidden" name="aircelprice" value="10" />
							<div class = "col s6">
							<div class = "row">         
								<div class="input-field col s12">
									<select name="channel_id" id="channel_id">
										<option value="">Channel</option>
											<?php foreach($channelList as $channelList){?>
												<option value="<?php echo $channelList["ch_channel"]["id"]; ?>"><?php echo ucfirst(strtolower(trim($channelList["ch_channel"]["c_name"]))); ?></option>
											<?php } ?>
									</select>
								</div>                                           
								<div class="input-field col s12">                      
									<select name='subcategory_id' id='subcategory_id'>
										<option value="">Category</option>
									</select>                     
								</div>
								<div class="input-field col s12">
									<select name="content_type" id="content_type">
										<option value="">Content Type</option>
										<option value="V">Video</option>
									</select> 
								</div>
								
								<div class="input-field col s12">
									<select name='language_code' id='language_code'>
										<option value="">Language</option>
									</select>
								</div>
								<div class="input-field col s12">
									<input id="meta_title" name="meta_title" type="text" placeholder="Meta Title">
								</div>
								<div class="input-field col s12">
									<input id="meta_key" name="meta_key" type="text" placeholder="Meta Keyword">
									
								</div>
								<div class="input-field col s12">
									<input id="meta_description" name="meta_description" type="text"  placeholder="Meta Description">
								</div>
							</div>                          
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $(".errorMsgFrm").css("display", "none");
	    $("#addyoutube")[0].reset();
	})
	$("#submitForm").click(function(){
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var content = $.trim($('#content').val());
	    if( $('#country_code').val() == "") {
			$('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		/*if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}*/
		
		if(content ==""){
		    $('#content').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the content.");
			return false;
		}
		
		if( $('#channel_id').val() == "") {
			$('#channel_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select channel.");
			return false;
		}
		
		if( $('#subcategory_id').val() == "") {
			$('#subcategory_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select category.");
			return false;
		}
		
		if( $('#content_type').val() == "") {
			$('#content_type').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select content type.");
			return false;
		}
		//alert("hello raghav");
		document.addyoutube.submit();
	})
})
</script>	
<script type="text/javascript">
$(document).ready(function () {
    $('select').material_select();
    var app_slug = 'firstcut';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
			//$("#subcategory_id").css("display", "block");
			//$('#subcategory_id').html(resp);
			//$('.subcategory_id').html(resp);
			$selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#subcategory_id').material_select();
		}
	});
	
	//fetch language data onload...
	var countryCode = '';
	var languageCode = '';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp);
			$selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#language_code').material_select();
		}
	});
});	

function getLanguage(countryCode){
	$('select').material_select();
	var languageCode = '';
	//alert(countryCode);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp
			$selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#language_code').material_select();
		}
	});
}
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>