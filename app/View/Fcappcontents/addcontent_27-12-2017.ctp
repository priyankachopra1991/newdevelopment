<script>
$(function() {
	$("#datepicker").datepicker( { "dateFormat":"yy-mm-dd" } );
	$("#datepicker1").datepicker( { "dateFormat":"yy-mm-dd" } );
});
 
</script>
<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class="edit-form" method="post" action="<?php echo BASE_URL."/fcappcontents/addcontent"; ?>" id="addcontent" name="addcontent" enctype="multipart/form-data" method="post">
					    <div id="sections">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Content Upload</h2>
								</div>
								
								<div class = "input-field inner-button col s12 custom-setting">
								<input type="submit" style="padding:10px 10px !important" class="waves-effect waves-light btn blue darken-1"/>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
									<!----<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>-->
								</div>
								
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
							<input type="hidden" name="app_slug[]" value="firstcut" />
								<div class = "row">
									<div class="input-field col s12">
										<select name="country_code[]" id="country_code" onchange="getLanguage(this.value);">
											<option value="">Country</option>
											<?php foreach($country as $countrylist){?>
												<option value="<?php echo $countrylist['cm_country_code']['country_code']; ?>"><?php echo $countrylist['cm_country_code']['country_name']; ?></option> 
											<?php } ?>
										</select>
									</div>                              
									<div class="input-field col s12">
									<input type="text" id="name" name="name[]" class="validate" placeholder="Name" required>
										 					
									</div>
									
									<div class="input-field col s12">
										<input type="text" id="tags" name="tags[]" placeholder="Tags" required>
									</div>
									
									<?php if($user_type == "V3MO" || $user_type == "V3MOA" )
									{ ?>
										<div class="input-field col s12">
											<input type="text" id="display_order" name="display_order[]"  'onkeypress'='return isNumber(event)' placeholder="Display Order">
									</div>
									<?php } else { ?>
										<input type="text" id="display_order" name="display_order[]" style="display:none;">
									<?php } ?>
									<div class="input-field series-file file_icon col s12">
									<label for="channel">Upload Content Video*</label>
									<i class = "material-icons">perm_media</i>
									<input type="file" name="data[content][]" id="file" required/>										
									</div>
									<div class="input-field col s12">
									<input id="description" name="description[]" type="text" placeholder="Description" required>
									</div>
									<div class="input-field col s12">
										<input id="Production House" name="production_house[]" type="text" placeholder="Production House">
									</div>
									<div class="input-field col s12">
										<input id="Director Name" name="director_name[]" type="text" placeholder="Director Name">
									</div>
									<div class="input-field col s12">
									<input id="meta_key" name="meta_key[]" type="text" placeholder="Meta Keyword">
								</div>
								</div>                    
							</div>
							<input type="hidden" name="price" value="10" />	
							<input type="hidden" name="tataprice" value="10" />	
							<input type="hidden" name="vodafoneprice" value="10" />	
							<input type="hidden" name="mtnlprice" value="10" />	
							<input type="hidden" name="aircelprice" value="10" />
							<div class = "col s6">
							<div class = "row">         
								<div class="input-field col s12">
									<select name="channel_id[]" id="channel_id">
										<option value="">Channel</option>
											<?php foreach($channelList as $channelList){?>
												<option value="<?php echo $channelList["ch_channel"]["id"]; ?>"><?php echo ucfirst(strtolower(trim($channelList["ch_channel"]["c_name"]))); ?></option>
											<?php } ?>
									</select>
								</div>                                           
								<div class="input-field col s12">                      
									<select name='subcategory_id[]' id='subcategory_id'>
										<option value="">Category</option>
									</select>                     
								</div>
								
								<div class="input-field col s12">
									<select name='language_code[]' id='language_code'>
										<option value="">Language</option>
									</select>
								</div>
								<div class="input-field col s12">
									<input id="meta_title" name="meta_title[]" type="text" placeholder="Meta Title">
								</div>
								<div class="input-field series-file file_icon col s12">
								<label for="channel">Upload Trailer Video</label>
								<i class = "material-icons">perm_media</i>
								<input type="file" name="data[trialcontent][]" id="file"/>										
								</div>
								<div class="input-field series-file file_icon col s12">
								<label for="channel">Upload Poster*</label>
								<i class = "material-icons">perm_media</i>
								<input type="file" name="data[poster][]" id="file" required/>										
								</div>
								<div class="input-field col s12">
									<input id="meta_description" name="meta_description[]" type="text"  placeholder="Meta Description">
								</div>
							</div>
                            							
						</div>
						</div>
					</form> 
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	

<script type="text/javascript">
$(document).ready(function () {
    $('select').material_select();
    var app_slug = 'firstcut';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
			//$("#subcategory_id").css("display", "block");
			//$('#subcategory_id').html(resp);
			//$('.subcategory_id').html(resp);
			$selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#subcategory_id').material_select();
		}
	});
	
	//fetch language data onload...
	var countryCode = '';
	var languageCode = '';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp);
			$selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#language_code').material_select();
		}
	});
});	

function getLanguage(countryCode){
	$('select').material_select();
	var languageCode = '';
	//alert(countryCode);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp
			$selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#language_code').material_select();
		}
	});
}
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


</script>

