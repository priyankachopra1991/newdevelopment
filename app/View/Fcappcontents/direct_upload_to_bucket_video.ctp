<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class = "edit-form" action="https://s3-ap-southeast-1.amazonaws.com/firstcut/" method="post" name="uploadContent" id="uploadContent" enctype="multipart/form-data">
					<input type="hidden" name="key" value="${filename}" />
					<input type="hidden" name="acl" value="public-read" />
					<input type="hidden" name="X-Amz-Credential" value="<?= $access_key; ?>/<?= $short_date; ?>/<?= $region; ?>/s3/aws4_request" />
					<input type="hidden" name="X-Amz-Algorithm" value="AWS4-HMAC-SHA256" />
					<input type="hidden" name="X-Amz-Date" value="<?=$iso_date ; ?>" />
					<input type="hidden" name="Policy" value="<?=base64_encode($policy); ?>" />
					<input type="hidden" name="X-Amz-Signature" value="<?=$signature ?>" />
					<input type="hidden" name="success_action_redirect" value="<?= $success_redirect ?>" /> 
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Direct Video Upload</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
								<div class = "row">
									<div class="input-field series-file file_icon col s12">
										<label for="channel">Choose File</label>
										<i class = "material-icons">perm_media</i>
										<input type="file" name="file" id="file"/>										
									</div>
								</div>                    
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $(".errorMsgFrm").css("display", "none");
	    $("#bulkupload")[0].reset();
	})
	$("#submitForm").click(function(){
		var file = $('#file').val().split('.').pop().toLowerCase();
		if(file ==""){
			$('#file').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload file.");
			return false;
		}
		
		if(file !="" && $.inArray(file, ['mp4']) == -1){
			$('#file').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload file as mp4.");
			return false;
		}
		document.uploadContent.submit();
	})
})	
</script>