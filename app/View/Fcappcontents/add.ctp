<div class = "table-format-data">
	<div class = "row">
		<div class = "col s8 offset-s2">                     
			<div class = "add-channel-form">
				<div class = "channel-header">
					<h2>Add Category</h2> 
					<p>This information will let us know more about your channel</p>
				</div>
				<div class="row">
					<div class="col s12">
						<ul class="tabs artist-tabs">
							<li class="tab col s4"><a class="active" href="#basicinfo" id="tabberDisableBasic">Basic Info</a></li>
							<li class="tab col s4"><a href="#identity" id="tabberDisableIdentity">Identity</a></li>
							<li class="tab col s4 "><a href="#otherInfo" id="tabberDisableSocialConnect">Other Info</a></li>                         
						</ul>
					</div>
					<form name="addCategory" id="addCategory" action="<?php echo BASE_URL; ?>/fcappcategories/add" method="post"  enctype="multipart/form-data">
					<div id="basicinfo" class="col s12 tab-content">
						
							<div class = "row">
							<div class="errorMsgFrm" style="display:none;"></div>
							
							<input type="hidden" name="app_slug" id="app_slug" value="firstcut" />
								<div class="input-field col s12">
									<i class="material-icons prefix">face</i>
									<input id="name" name="name" type="text" class="validate">
									<label for="icon_prefix">Name</label>
								</div>
								<div class="col s6">
									<i class="material-icons prefix left"></i>
									<select name="category_id" id="category_id" class="dropdown">
										<option value="">Category</option>
									</select>
								</div>
								<div class="input-field col s6">
									<i class="material-icons prefix"></i>
									<input id="slug" name="slug" type="text" class="validate">
									<label for="icon_prefix">Category Slug</label>
								</div>                                                               
								<div class="input-field col s12">
									<i class="material-icons prefix">assignment</i> 
									<input id="description" name="description" type="text" class="validate">
									<label for="icon_prefix">Description</label>
								</div>                                
							</div>                             
						<div class="input-field col s12">
							<a class="waves-effect waves-light btn right red darken-1" href ="javascript:void(0)" id="identityTab">Next</a>
						</div>
					</div>
				<div id="identity" class="col s12 tab-content">
					<div class = "row  upload-data">
					    <div class="errorMsgFrm" style="display:none;"></div>
							<div class = "col s6">
								<div class="file_icon">
									<i class = "material-icons">perm_media</i>
									<?php echo $this->Form->input('cat_icon', array('type'=>'file','label'=>'','id'=>'cat_icon'));?>
									<label>Choose Category Icon
								</div>                                 
							</div>
							<div class = "col s6">
								<div class="file_icon banner-image">
									<i class = "material-icons">perm_media</i>
									<?php echo $this->Form->input('category_banner', array('type'=>'file','label'=>'','id'=>'category_banner', 'value'=>''));?>
									<label>Choose Category Banner
								</div>
							</div>
							<div class = "clearfix" style = "clear: both;"></div>
							<div class = "col s6">
								<div class="file_icon">
									<i class = "material-icons">perm_media</i>
									<?php echo $this->Form->input('portal_cat_icon', array('type'=>'file','label'=>'','id'=>'portal_cat_icon'));?>
									<label>Portal Category's Icon
								</div>                                 
							</div>
							<div class = "col s6">
								<div class="file_icon banner-image">
									<i class = "material-icons">perm_media</i>
									<?php echo $this->Form->input('category_page_icon', array('type'=>'file','label'=>'','id'=>'category_page_icon', 'value'=>''));?>
									<label>Category Page Icon
								</div>
							</div>
						
						<div class="input-field col s12">
							<a class="waves-effect waves-light btn left red darken-1" href ="javascript:void(0)" id="identityPrevTab">Previous</a>
							<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="identityNextTab">Next</a>
						</div>
					</div>
				</div>
				<div id="otherInfo" class="col s12 tab-content">
					<div class = "social-container">
						
							<div class = "row">
							<div class="errorMsgFrm" style="display:none;"></div>
								<div class="input-field col s12">    
									<input id="display_order" name="display_order" type="text" class="validate"  onkeypress="return isNumber(event)">
									<label for="icon_prefix">Display Order</label>
								</div>
								<div class="input-field col s6"> 
									<input id="heading_back_color" name="heading_back_color" type="text" class="validate">
									<label for="icon_prefix">Heading Background</label>
								</div>
								<div class="input-field col s6">
									<input id="alttag" name="alttag" type="text" class="validate">
									<label for="icon_prefix">Alt Tag</label>
								</div>  
						        <div class="input-field col s6">
									<label>Status</label>
									<input type="radio" name="status" id="optionsRadiosInline1" value="A" checked>Active
									<input type="radio" name="status" id="optionsRadiosInline2" value="D">Deactive
								</div>
								<div class="input-field col s6">
									<label for="name">Display On App</label>
									<input type="radio" name="app_flag" id="optionsRadiosInline3" value="1" checked>Yes
									<input type="radio" name="app_flag" id="optionsRadiosInline4" value="0">No
								</div>								
							</div>                             
						
						<div class="input-field col s12">
							<a class="waves-effect waves-light btn left red darken-1" href = "javascript:void(0)" id="socialConnectPrevTab">Previous</a>
							<a class="waves-effect waves-light btn right red darken-1" href = "javascript:void(0)" id="submitForm">Finish</a>
						</div>
					</div>
				</div>
				</form>
			</div>          
		</div>
	</div>
</div>
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$('#tabberDisableBasic').click(function(){ return false});
	$('#tabberDisableIdentity').click(function(){ return false});
	$('#tabberDisableSocialConnect').click(function(){ return false});
	$("#identityTab").click(function(){
	    var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var slug = $.trim($('#slug').val());
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}
		if( $('#category_id').val() == "") {
		    $('#category_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select Category.");
			return false;
		}
		if(slug ==""){
		    $('#slug').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the slug.");
			return false;
		}
		
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 214px; left: 213px;");
		return true;
	})
	$("#identityPrevTab").click(function(){
		$("#identity").css("display", "none");
		$("#basicinfo").css("display", "block");
		$("#tabberDisableBasic").addClass("active");
		$(".indicator").attr("style", "right: 321px; left: 0px;");
	})
	
	$("#identityNextTab").click(function(){
		var category_banner = $('#category_banner').val().split('.').pop().toLowerCase();
		var cat_icon = $('#cat_icon').val().split('.').pop().toLowerCase();
		var portal_cat_icon = $('#portal_cat_icon').val().split('.').pop().toLowerCase();
		var category_page_icon = $('#category_page_icon').val().split('.').pop().toLowerCase();
		if(cat_icon !="" && $.inArray(cat_icon, ['png','jpg','jpeg']) == -1){
			$('#cat_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload category icon as png, jpg, jpeg.");
			return false;
		}
		
		if(category_banner !="" && $.inArray(category_banner, ['png','jpg','jpeg']) == -1){
			$('#category_banner').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload category banner as png, jpg, jpeg.");
			return false;
		}
		
		if(portal_cat_icon !="" && $.inArray(portal_cat_icon, ['png','jpg','jpeg']) == -1){
			$('#portal_cat_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload portal category icon as png, jpg, jpeg.");
			return false;
		}
		
		if(category_page_icon !="" && $.inArray(category_page_icon, ['png','jpg','jpeg']) == -1){
			$('#c_icon').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload category page icon as png, jpg, jpeg.");
			return false;
		}
		$(".errorMsgFrm").css("display", "none");
		$("#basicinfo").css("display", "none");
		$("#identity").css("display", "none");
		$("#otherInfo").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableIdentity").removeClass("active");
		$("#tabberDisableSocialConnect").addClass("active");
		$(".indicator").attr("style", "right: 1px; left: 440px;");
		return true;
	
	})
	$("#socialConnectPrevTab").click(function(){
		$("#basicinfo").css("display", "none");
		$("#otherInfo").css("display", "none");
		$("#identity").css("display", "block");
		$("#tabberDisableBasic").removeClass("active");
		$("#tabberDisableSocialConnect").removeClass("active");
		$("#tabberDisableIdentity").addClass("active");
		$(".indicator").attr("style", "right: 214px; left: 213px;");
	})
	$("#submitForm").click(function(){
		document.addCategory.submit();
	})
	var app_slug = "firstcut";
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/categorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//alert(resp);
			$('#category_id').material_select('destroy');
			$('#category_id').html(resp);
		}
	});
})
</script>
<script type="text/javascript"> 
    function isNumber(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

<script type = "text/javascript" >
history.pushState(null, null, 'add');
window.addEventListener('popstate', function(event) {
history.pushState(null, null, 'add');
});
</script>