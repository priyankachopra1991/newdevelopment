<div class = "single-view-channel-section "> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<div class = "row">
						<div class = "col s7">
							<div class = "inner-title left">
								<h2><?php echo $contentRec[0]["t1"]["name"]; ?></h2>
								<p>Content ID : <?php echo $contentRec[0]["t1"]["content_id"]; ?></p>
							</div>
						</div>
					<div class = "col s5">
						<div class = "inner-button right">
						<?php if($this->Session->read('User.user_type') == 'CPA' || $this->Session->read('User.user_type') == 'V3MO' ||  $this->Session->read('User.user_type') == 'V3MOA'){ ?>
							<a class="waves-effect waves-light btn green darken-1" href = "<?php echo BASE_URL."/fcappcontents/bulkupload/"; ?>">Add Content</a>
							<?php if($contentRec[0]["t1"]["fcut_content_type"] == "utube"){ ?>
								<a class="waves-effect waves-light btn orange darken-1" href = "<?php echo BASE_URL."/fcappcontents/edityoutubecontent/".$contentRec[0]["t1"]["content_id"]; ?>">Edit</a>
							<?php } else { ?>
								<a class="waves-effect waves-light btn orange darken-1" href = "<?php echo BASE_URL."/fcappcontents/editcontent/".$contentRec[0]["t1"]["content_id"]; ?>">Edit</a>
							<?php } ?>
							<a class="waves-effect waves-light btn red darken-1" href = "<?php echo BASE_URL."/fcappcontents/deletecontent/".$contentRec[0]["t1"]["content_id"]; ?>"  onclick="return ConfirmDelete();">Delete</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class = "col s7">  
				<div class = "inner-container">                          
					<div class = "inner-img">
					    <?php if($contentRec[0]["t1"]["fcut_content_type"] == "utube"){ ?>
							<img src = "<?php echo $contentRec[0]['t1']['poster']; ?>" alt = "" />
						<?php } else { ?>
						    <?php $imgExp = explode(".", $contentRec[0]['t1']['poster']); ?>
							<img src = "<?php echo AMAZONIMAGEURL.$imgExp[0]."_650_376.jpeg"; ?>" alt = "" />
						<?php } ?>
						
					</div>
					<div class = "inner-des">
						<h3>Description</h3>
						<p><?php echo empty($contentRec[0]["t1"]["description"]) ? 'NA' : $contentRec[0]["t1"]["description"]; ?></p>
					</div>
					<div class = "inner-car">
						<h3>Category</h3>
						<p><?php echo $contentRec[0]["t2"]["name"]; ?></p>
					</div>
				</div>                     
			</div>
			<div class = "col s5">      
                <div class = "inner-img">
				<video width="400" controls>
                <source src="https://s3-ap-southeast-1.amazonaws.com/firstcut/<?php echo $contentRec[0]["t1"]["content"]?>" type="video/mp4">
                </video>
			    </div>		
				<ul class = "approval-list">
					<li class = "list-hreading">Approval Status</li>
					<li><?php if($contentRec[0]["t1"]["approved"] == 1){ ?>
							Approved
						<?php } else if ($contentRec[0]["t1"]["approved"] == 0){?>
							Submitted for approval
						<?php } else { ?>
							Rejected
						<?php } ?>
					</li>
				</ul> 
				<ul class = "Date-Created">
					<li class = "list-hreading">Date Created</li>
					<li><?php echo date('d/m/Y', strtotime($contentRec[0]["t1"]["created"])); ?></li>
				</ul>
				<ul class = "Channel-listed">
					<li class = "list-hreading">Channel</li>
					<li><?php echo $channel["Channel"]["c_name"]; ?></li>
				</ul>                    
			</div>
		</div>
	</div>  
</div>
<script>
    function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
</script>  