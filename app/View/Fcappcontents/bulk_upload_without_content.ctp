<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class="edit-form" method="post" action="<?php echo BASE_URL."/fcappcontents/bulk_upload_without_content"; ?>" id="bulkupload" name="bulkupload" enctype="multipart/form-data" method="post">
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Bulk Upload Without Content</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
							<input type="hidden" name="app_slug"  id="app_slug" value="firstcut" />
								<div class = "row">
									<div class="input-field col s12">
										<select name="country_code" id="country_code">
											<option value="">Country</option>
											<?php foreach($country as $countrylist){?>
												<option value="<?php echo $countrylist['cm_country_code']['country_code']; ?>"><?php echo $countrylist['cm_country_code']['country_name']; ?></option> 
											<?php } ?>
										</select>
									</div>                              
									<div class="input-field series-file file_icon col s12">
										<label for="channel">Choose File</label>
										<i class = "material-icons">perm_media</i>
										<?php echo $this->Form->input('file',array('type' => 'file','name'=>'csvfile','label'=>'', 'id'=>'csvfile')); ?>
																				
									</div>
									<div>
									<a href="<?php echo BASE_URL.'/bulkupload/format/bulk_upload_format.csv';?>">Download Format</a>
									</div>
								</div>                    
							</div>
							<div class = "col s6">
							<div class = "row">         
								<div class="input-field col s12">
									<select name="channel_id" id="channel_id">
										<option value="">Channel</option>
											<?php foreach($channelList as $channelList){?>
												<option value="<?php echo $channelList["ch_channel"]["id"]; ?>"><?php echo ucfirst(strtolower(trim($channelList["ch_channel"]["c_name"]))); ?></option>
											<?php } ?>
									</select>
								</div>                                           
								<div class="input-field col s12">                       
									<select name='subcategory_id' id='subcategory_id'>
										<option value="">Category</option>
									</select>                     
								</div>
								<div class="input-field col s12">
									<select name="contenttype" id="contenttype">
										<option value="">Content Type</option>
										<option value="V">Video</option>
									</select> 
								</div>
							</div>                          
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $(".errorMsgFrm").css("display", "none");
	    $("#bulkupload")[0].reset();
	})
	$("#submitForm").click(function(){
		var csvfile = $('#csvfile').val().split('.').pop().toLowerCase();
		
		if( $('#country_code').val() == "") {
			$('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if(csvfile == "") {
			$('#csvfile').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select file.");
			return false;
		}
		
		
		if(csvfile !="" && $.inArray(csvfile, ['csv']) == -1){
			$('#csvfile').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please upload file as csv.");
			return false;
		}
		
		if( $('#channel_id').val() == "") {
			$('#channel_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select channel.");
			return false;
		}
		
		if( $('#subcategory_id').val() == "") {
			$('#subcategory_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select category.");
			return false;
		}
		
		if( $('#contenttype').val() == "") {
			$('#contenttype').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select content type.");
			return false;
		}
		//alert("hello raghav");
		document.bulkupload.submit();
	})
})
</script>	
<script type="text/javascript">
$(document).ready(function () {
    var app_slug = 'firstcut';
	$('select').material_select();
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
			//$("#subcategory_id").css("display", "block");
			//$('#subcategory_id').html(resp);
			//$('.subcategory_id').html(resp);
			$selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$('#subcategory_id').material_select();
		}
	});
});	
</script>