<script>
$(function() {
	$("#datepicker").datepicker( { "dateFormat":"yy-mm-dd" } );
	$("#datepicker1").datepicker( { "dateFormat":"yy-mm-dd" } );
});
</script>
<?php if(isset($content['Content']["category_id"]) && !empty($content['Content']["category_id"])){ ?>
<script type="text/javascript">
$(document).ready(function(){
	$('select').material_select();
	var app_slug = 'firstcut';
	var subcategory_id = '<?php echo $content['Content']["category_id"]; ?>';
	//alert(subcategory_id);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/viewcontentcategorylist.php',
		data : {'app_slug': app_slug, 'category_id': subcategory_id},
		'success':function(resp){
		    //$('#subcategory_id').material_select('destroy');
			//$('#subcategory_id').css("display", "block");
		    //$('#subcategory_id').html(resp);
			$selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$("#subcategory_id").material_select();
		}
	});
});
</script>
<?php } ?>
<?php
/*echo "<pre>";
print_r($content);
die;*/
?>
<div class = "single-view-channel-section edit-it"> 
	<div class = "row">
		<div class = "col s10 offset-s1 wrapper-role white-bg">
			<div class = "row">
				<div class = "col s12">
					<form class="edit-form" method="post" action="<?php echo BASE_URL."/fcappcontents/edityoutubecontent/".$content['Content']['content_id']; ?>" id="edityoutube" name="edityoutube" enctype="multipart/form-data" method="post">
					<input type="hidden" name="content_id" id="content_id" value="<?php echo $content['Content']['content_id']; ?>" />
						<div class = "row">
							<div class = "col s12">
								<div class = "main-heading left">
									<h2>Edit Content</h2>
								</div>
								<div class = "input-field inner-button col s12 custom-setting">
									<a class="waves-effect waves-light btn blue darken-1" id="submitForm">Submit</a>
									<a class="waves-effect waves-light btn red darken-1" id="resetForm">Reset</a>
								</div> 
							</div>
							<div class = "col s12">
								<div class="errorMsgFrm" style="display:none;"></div>
							</div>
							<div class = "col s6">
							<input type="hidden" name="app_slug" value="firstcut" />
								<div class = "row">
									<div class="input-field col s12">
										<select name="country_code" id="country_code" onchange="getLanguage(this.value);">
											<option value="">Country</option>
											<?php foreach($country as $countrylist){?>
												<option value="<?php echo $countrylist['cm_country_code']['country_code']; ?>" <?php if($countrylist['cm_country_code']['country_code'] == $content['Content']["country_code"]){ echo "selected"; } ?>><?php echo $countrylist['cm_country_code']['country_name']; ?></option> 
											<?php } ?>
										</select>
									</div>                              
									<div class="input-field col s12">
										<input type="text" id="name" name="name"  value="<?php echo $content['Content']['name']; ?>" class="validate" placeholder="Name" >
															
									</div>
									
									<div class="input-field col s12">
										<input type="text" id="content" name="content" value="<?php echo $content['Content']['content']; ?>" class="validate" placeholder="Content">
										<input type="hidden" name="time" vlue="<?php echo time();?>">	
									</div>
									<div class="input-field col s12">
										<input type="text" id="tag" name="tag" value="<?php echo $content['Content']['tags']; ?>"  placeholder="Tags">
											
									</div>
									
									<?php if(($this->Session->read('User.user_type') =="V3MO") || ($this->Session->read('User.user_type') =="V3MOA")){ ?>
									
									<div class="input-field col s12">
									    <input type="text" id="display_order" name="display_order" value="<?php echo $content['Content']['display_order']; ?>" placeholder="Display Order"> 
									</div>
									<?php } else{  ?>
										<div class="input-field col s12" style="display:none;">
											<input type="text" id="display_order" name="display_order" value="<?php echo $content['Content']['display_order']; ?>" placeholder="Display Order"> 
										</div>
									<?php } ?>
									<div class="input-field col s12">
										<input type="text" id="datepicker" name="release_date" value="<?php echo $content['Content']['release_date']; ?>" placeholder="Release Date"> 
										
									</div>
									<div class="input-field col s12">
										<input type="text" id="datepicker1" name="expiry_date" value="<?php echo $content['Content']['expiry_date']; ?>" placeholder="Expiry Date">
											
									</div>
									<div class="input-field col s12">
										<input id="description" name="description" type="text" value="<?php echo $content['Content']['description']; ?>" placeholder="Description">
									</div>
								</div>                    
							</div>
							
							<input type="hidden" name="price" value="10" />	
							<input type="hidden" name="tataprice" value="10" />	
							<input type="hidden" name="vodafoneprice" value="10" />	
							<input type="hidden" name="mtnlprice" value="10" />	
							<input type="hidden" name="aircelprice" value="10" />
							<input type="hidden" name="price" value="10" />	
							<input type="hidden" name="tata_price_id" value="10" />	
							<input type="hidden" name="mtnl_price_id" value="10" />	
							<input type="hidden" name="vodafone_price_id" value="10" />	
							<input type="hidden" name="aircel_price_id" value="10" />
							<div class = "col s6">
							<div class = "row">         
								<div class="input-field col s12">
									<select name="channel_id" id="channel_id">
										<option value="">Channel</option>
											<?php foreach($channelList as $channelList){?>
												<option value="<?php echo $channelList["ch_channel"]["id"]; ?>" <?php if($channelList["ch_channel"]["id"] == $content['Content']["channel_id"]){ echo "selected"; } ?>><?php echo ucfirst(strtolower(trim($channelList["ch_channel"]["c_name"]))); ?></option>
											<?php } ?>
									</select>
								</div>                                           
								<div class="input-field col s12">                        
									<select name='subcategory_id' id='subcategory_id'>
										<option value="">Category</option>
									</select>                     
								</div>
								<div class="input-field col s12">
									<select name="content_type" id="content_type">
										<option value="">Content Type</option>
										<option value="V" <?php if($content['Content']["content_type"] == "V"){ echo "selected"; } ?>>Video</option>
									</select> 
								</div>
								
								<div class="input-field col s12">
									<select name='language_code' id='language_code'>
										<option value="">Select</option>
									</select>
								</div>
								<div class="input-field col s12">
									<input id="director_name" name="director_name" type="text" value="<?php echo $content['Content']['director_name']; ?>" placeholder="Director Name">
									
								</div>
								<div class="input-field col s12">
									<input id="production_house" name="production_house" type="text" value="<?php echo $content['Content']['production_house']; ?>" placeholder="Production House">
								</div>
								<div class="input-field col s12">
									<input id="meta_title" name="meta_title" type="text" value="<?php echo $content['Content']['meta_title']; ?>" placeholder="Meta Title">
									
								</div>
								<div class="input-field col s12">
									<input id="meta_key" name="meta_key" type="text" value="<?php echo $content['Content']['meta_key']; ?>" placeholder="Meta Keywords">
							
								</div>
								<div class="input-field col s12">
									<input id="duration" name="duration" type="text" value="<?php echo $content['Content']['duration']; ?>" placeholder="Duration">
							
								</div>
								<div class="input-field col s12">
									<input id="meta_description" name="meta_description" type="text" value="<?php echo $content['Content']['meta_description']; ?>" placeholder="Meta Description">
									
								</div>
								<input type="hidden" name="old_status" value="<?php echo $content['Content']["status"]; ?>" />
									
								<input type="hidden" name="old_approved" value="<?php echo $content['Content']["approved"]; ?>" />
								
								<?php if(($this->Session->read('User.user_type') =="V3MO") || ($this->Session->read('User.user_type') =="V3MOA")){ ?>
								
									<div class="input-field col s12">
										<label for="name">Status</label>
										<br/>
										<br/>
										<input type="radio" name="status" id="optionsRadiosInline1" value="A" <?php echo (($content['Content']["status"] == "A") ? "checked" : ""); ?>  class="space">Active&nbsp;&nbsp;
										<input type="radio" name="status" id="optionsRadiosInline2" value="D" <?php echo (($content['Content']["status"] == "D") ? "checked" : ""); ?> class="space">Deactive
									</div>
									<?php } else { ?>
										<input type="hidden" name="status" value="<?php echo $content['Content']["status"]; ?>" />
									<?php } ?>
									<?php if(($this->Session->read('User.user_type') =="V3MO") || ($this->Session->read('User.user_type') =="V3MOA")){ ?>
									<div class="input-field col s12">
										<label for="name">Approval Status</label>
										<br/>
										<br/>
										<input type="radio" name="approved" id="optionsRadiosInline3" value="0" <?php echo (($content['Content']["approved"] == 0) ? "checked=''" : ""); ?> class="space">Un approved&nbsp;&nbsp;
										<input type="radio" name="approved" id="optionsRadiosInline4" value="1" <?php echo (($content['Content']["approved"] == 1) ? "checked" : ""); ?>  class="space">Approved&nbsp;&nbsp;
										<input type="radio" name="approved" id="optionsRadiosInline5" value="2" <?php echo (($content['Content']["approved"] == 2) ? "checked" : ""); ?>  class="space">Rejected
									</div>	
									<div class="input-field col s12">
										<label for="name">Content Type</label>
										<br/>
										<br/>
										<input type="radio" name="premium_type" id="optionsRadiosInline3" value="premium" <?php echo (($content['Content']["premium_type"] == 'premium') ? "checked=''" : ""); ?> class="space">Premium&nbsp;&nbsp;
										<input type="radio" name="premium_type" id="optionsRadiosInline4" value="free" <?php echo (($content['Content']["premium_type"] == 'free') ? "checked" : ""); ?> class="space">Free&nbsp;&nbsp;
									</div>	
									<?php } else { ?>
										<input type="hidden" name="approved" value="<?php echo $content['Content']["approved"]; ?>" />
									<?php } ?>	
									<input type="hidden" id="approved1" name="approved1" value="<?php echo $content['Content']["approved"]; ?>"> 
									<div class="input-field col s12" id="remarkBlock" <?php if($content['Content']["approved"] != 2){?>style="display:none;"<?php } ?>>
										<label for="name">Remark*</label>
										<textarea name="remark" id="remark"  rows="6" cols="30" ><?php echo $content['Content']["remark"]; ?></textarea> 
										<?php /*if(($this->Session->read('User.user_type') !="V3MO") || ($this->Session->read('User.user_type') !="V3MOA")){ ?>readonly="readonly" <?php }*/ ?>
									</div>
							</div>                          
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div> 
<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
	$("#resetForm").click(function(){
	    $(".errorMsgFrm").css("display", "none");
	    $("#addyoutube")[0].reset();
	})
	$("#submitForm").click(function(){
		var nameRegex = /^[a-zA-Z 0-9&._-]+$/;
		var name = $.trim($('#name').val());
		var content = $.trim($('#content').val());
	    if( $('#country_code').val() == "") {
			$('#country_code').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select country.");
			return false;
		}
		
		if(name ==""){
		    $('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the name.");
			return false;
		}
		
		/*if(!nameRegex.test(name)){
			$('#name').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Special characters are not allowed in name.");
			return false;
		}*/
		
		if(content ==""){
		    $('#content').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please enter the content.");
			return false;
		}
		
		if( $('#channel_id').val() == "") {
			$('#channel_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select channel.");
			return false;
		}
		
		if( $('#subcategory_id').val() == "") {
			$('#subcategory_id').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select category.");
			return false;
		}
		
		if( $('#content_type').val() == "") {
			$('#content_type').focus() ;
			$(".errorMsgFrm").css("display", "block");
			$('.errorMsgFrm').html("Please select content type.");
			return false;
		}
		//alert("hello raghav");
		document.edityoutube.submit();
	})
})
</script>	
<script type="text/javascript">
$(document).ready(function () {
    $('select').material_select();
   //fetch language data onload...
	var countryCode = '<?php echo $content['Content']["country_code"]; ?>';
	var languageCode = '<?php echo $content['Content']["language_code"]; ?>';
	//alert(languageCode);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp);
			var $selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$selectDropdown.trigger('contentChanged');
		}
	});
});	

function getLanguage(countryCode){
	$('select').material_select();
	var languageCode = '<?php echo $content['Content']["language_code"]; ?>';
	//alert(countryCode);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/language.php',
		'data':'countryCode='+countryCode+'&languageCode='+languageCode,
		'success':function(resp){
		    //$('#language_code').material_select('destroy');
			//$("#language_code").css("display", "block");
			//$('#language_code').html(resp);
			var $selectDropdown = 
			  $("#language_code")
				.empty()
				.html('');
					
			$selectDropdown.append(resp);
			$selectDropdown.trigger('contentChanged');
		}
	});
}

$('select').on('contentChanged', function() {
	// re-initialize (update)
	$(this).material_select();
});

</script>
<script type="text/javascript"> 
    function isNumber(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>

 <script type="text/javascript">
    
$(document).ready(function() {
    $("input[name$='approved']").click(function() {
		var testVal = $(this).val();
		//alert(testVal);
		if(testVal ==2)
		{
			$("#remarkBlock").css('display', 'block');
			$('#remark').prop('required',true);
		} else {
			$("#remarkBlock").css('display', 'none');
			$('#remark').prop('required',false);
		}
    });
});
</script>