<?php $theComponent = new CommonComponent(new ComponentCollection());  ?>
<?php if(isset($_GET['subcategory_id']) && !empty($_GET['subcategory_id'])){ ?>
<script type="text/javascript">
$(document).ready(function(){
	$('select').material_select();
	var app_slug = 'firstcut';
	var subcategory_id = '<?php echo $_GET['subcategory_id']; ?>';
	//alert(subcategory_id);
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/viewcontentcategorylist.php',
		data : {'app_slug': app_slug, 'category_id': subcategory_id},
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
		    //$('#subcategory_id').html(resp);
			  $selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			 $selectDropdown.append(resp);
			 $('select').material_select();
		}
	});
});
</script>
<?php } else { ?>
<script type="text/javascript">
$(document).ready(function () {
	$('select').material_select();
	var app_slug = 'firstcut';
	$.ajax({
		'type':'post',
		'url':'<?php echo BASE_URL;?>/ajaxdata/contentcategorylist.php',
		'data':'app_slug='+app_slug,
		'success':function(resp){
			//$('#subcategory_id').material_select('destroy');
			//$('#subcategory_id').html(resp);
			//$('.subcategory_id').html(resp);
			 $selectDropdown = 
			  $("#subcategory_id")
				.empty()
				.html('');
					
			 $selectDropdown.append(resp);
			 $('select').material_select();
			//$selectDropdown.trigger('contentChanged');
		}
	});
});	


</script>
<?php } ?>
<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchContent" id="searchContent" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list right">                           
							<li>
								<select name="country_code" id="country_code">
									<option value="">Country Name</option>
									<?php foreach($country as $countrylist){?>
									<option value="<?php echo $countrylist['cm_country_code']['country_code']; ?>" <?php if($countrylist['cm_country_code']['country_code']==@$this->request->query["country_code"]){ echo "selected"; } ?>><?php echo $countrylist['cm_country_code']['country_name']; ?></option> 
									<?php } ?> 
								</select>
							</li>
							<li>
     							<select name="channel_id" id="channel_id">
								<option value="">Channel</option>
								<?php
								 foreach($channelList as $channelList)
								 {
									if(!empty($channelList))
									{
								 ?>
									 <option value="<?php echo $channelList["ch_channel"]["id"]; ?>" <?php if($channelList['ch_channel']['id']==@$this->request->query["channel_id"]){ echo "selected"; } ?>><?php echo ucfirst(strtolower(trim($channelList["ch_channel"]["c_name"]))); ?></option>
									 <?
									}
								 else
								 {
								 ?>
								 <option value="">Add Channel</option>
								 <?
								 }
								 }
								 ?>
								 </select>	
								
							</li>
							<li>
								<select name="status" id="status">
									<option value="">Status</option>
									<option value="1" <?php if(@$_GET['status'] == "1"){ echo "selected"; } ?>>Approved</option>
									<option value="0" <?php if(@$_GET['status'] == "0"){ echo "selected"; } ?>>Unapproved</option>
									<option value="2" <?php if(@$_GET['status'] == "2"){ echo "selected"; } ?>>Rejected</option>
								</select>	
							</li>
							
							<li>
								<select name='subcategory_id' id='subcategory_id'>
									<option value="">Select</option>
								</select>
							</li>
							<li>
								<input type="text" name="name" value="<?php echo @$this->request->query['name'];?>" placeholder="Search By Name" class="validate" />
							</li>
							<li>
								<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule" >
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)" id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>
		</div>
		<form name="sortContent" id="sortContent" action="" method="get">
			<div class = "col s6 small-filteration">
				<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Active</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>
				</ul>
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    <!--Grid view section start here--> 
	<form action="" name="updateStatus" id="updateStatus" method="post">
    <?php if(!empty($value)){ ?>
	<?php if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO")){ ?>
					<div class="col s6">
					<ul class="select-list">
                    <!---<label>Change Status:</label>-->
					<li>
							<select name="statusn">
							<option value="">Change Status</option>
							<option value="A">Active</option>
							<option value="D">Deactive</option>
							<option value="1">Approved</option>
							<option value="0">Unapproved</option>
							<option value="2">Rejected</option>
							<option value="T">Trending</option>
							</select>
                    </li>
                    <li>					
				    <input type = 'submit' name ='submit' value ='Submit' class="waves-effect waves-light btn red darken-1" style="padding:5px;position:absolute;margin-top:-16px;">
					</li>
					</ul>
					</div>
                    	<!-- end of col-lg-6 div -->	
						<br clear="all" />
						<br clear="all" />
					<?php } ?>
		<?php foreach($value as $obj){ 
		?>
			<div class = "col s4 gridView" style="display:none;">
			    
				<div class="row">
					<div class="col s12 m12">
						<div class="card view-user-card">
							<div class="card-image">
							
							   <?php if($obj['t1']['fcut_content_type'] == "utube"){ ?>
							   <a href = "<?php echo BASE_URL."/fcappcontents/detailcontent/".$obj['t1']['content_id']; ?>">
									<img class = "z-depth-3" src="<?php echo $obj['t1']['poster']; ?>">
								</a>
							   <?php } else { ?>
							   <a href = "<?php echo BASE_URL."/fcappcontents/detailcontent/".$obj['t1']['content_id']; ?>">
							   <?php $imgExp = explode(".", $obj['t1']['poster']); ?>
									<img class = "z-depth-3" src="<?php echo AMAZONIMAGEURL.$imgExp[0]."_650_376.jpeg"; ?>">
								</a>									
								<?php } ?>								
							</div>
							<div class="card-content">
								<span class="card-title"><?php //echo substr($obj['t1']['name'], 0,25); ?>
								<?php echo mb_substr($obj['t1']['name'], 0, 25, 'UTF-8'); ?></span>
								<ul class="channel-overview-list">
								<?php if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO")){ ?>
						 			<li><i class = "material-icons tiny">visibility</i> <?php echo $theComponent->changeNumberStyle($obj['t1']['view']); ?> views</li>
								<?php }?>    
									<li> <i class = "material-icons tiny">visibility</i> <?php echo $obj['t1']['premium_type']; ?> </li>
								</ul>
                            </div>
                            <div class="card-action">                            
                              <a style="visibility: hidden">
							  <i class = "material-icons red-text darken-1">group</i>
							  </a>
							  <?php if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO")){ ?> 
							  <input type="checkbox" class="checkcontent" name="check[]"  value="<?php echo $obj['t1']['content_id']; ?>" style="display:block;" />
							   <?php } ?>
							   
							  <?php if($obj['t1']['approved'] == 0)
							    { 
							   ?>
						      <a class = "amber accent-3">Submitted for approval</a>
							   <?php } else if($obj['t1']['approved'] == 1){ ?>
									<a class = "green accent-4">Approved</a>
							   <?php } else { ?>
									<a class = "red accent-4">Rejected</a>
							   <?php } ?>
							</div>
                        </div>
					</div>
				</div>
				
				<!-- end of card div -->
			</div>
		<?php } ?>
		
	<?php } else { ?>
		<div class = "col s12 gridView" style="display:none;">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--Grid view section end here-->
	<!--List view seection strat here-->
	 <?php if(!empty($value)){ ?>
		<div class = "col s12 listView" style="display:none;">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
					     <?php if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO")){ ?>
						<th><input type="checkbox" id="selecctall"/></th>
					<?php } ?>
						<th>Id</th>
						<th>Name</th>
						<th>Status</th>
						<!---<th>View</th>-->
						<th>Content Type</th>
						<th>Country</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($value as $obj){ ?>
						<tr>
						     <?php if(($this->Session->read('User.user_type') == "V3MOA") || ($this->Session->read('User.user_type') == "V3MO")){ ?>
							 <td>
								<input type="checkbox" name="check[]"  value="<?php echo $obj['t1']['content_id']; ?>"  style="display:block;" />
								</td>
							   <?php } ?>
							<td><?php echo $obj["t1"]["content_id"]; ?></td>
							<td><?php echo mb_substr($obj['t1']['name'], 0, 25, 'UTF-8'); ?></td>
							<td>
							<?php if($obj['t1']['approved'] == 0){ ?>
								Submitted for approval
						   <?php } else if($obj['t1']['approved'] == 1){ ?>
								Approved
						   <?php } else { ?>
							    Rejected
						   <?php } ?>
							</td>
							<!---<td><?php echo $obj['t1']['view']; ?></td>--->
							<td><?php echo $obj['t1']['premium_type']; ?></td>
							<td><?php 
							if($obj['t1']['country_code'] == 'IN')
							{
								echo "India"; 
							}
							else
							{
								echo "Indonesia";
							}
							?></td>
							<td><a href = "<?php echo BASE_URL."/fcappcontents/detailcontent/".$obj['t1']['content_id']; ?>" class = "btn red lighten-1">View</a>
							</td>

						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView" style="display:none;">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
	</form>
</div>
<?php if((!empty($value)) && ($numOfPage > 1)){ ?>
    <?php
	$pageName = BASE_URL."/fcappcontents/viewcontent";
	
	if(@$_GET["country_code"] !=""){
		$country_code = @$_GET["country_code"];
	} else {
		$country_code = "";
	}
	if(@$_GET["status"] !=""){
		$status = @$_GET["status"];
	} else {
		$status = "";
	}
	if(@$_GET["channel_id"] !=""){
		$channel_id = @$_GET["channel_id"];
	} else {
		$channel_id = "";
	}
	if(@$_GET["subcategory_id"] !=""){
		$subcategory_id = @$_GET["subcategory_id"];
	} else {
		$subcategory_id = "";
	}
	if(@$_GET["name"] !=""){
		$name = @$_GET["name"];
	} else {
		$name = "";
	}
	
	if(@$_GET["sortBy"] !=""){
		$sortBy = @$_GET["sortBy"];
	} else {
		$sortBy = "";
	}
	
	if(@$_GET["filterBy"] !=""){
		$filterBy = @$_GET["filterBy"];
	} else {
		$filterBy = "";
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&country_code='.@$country_code.'&status='.@$status.'&channel_id='.@$channel_id.'&subcategory_id='.@$subcategory_id.'&name='.@$name.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&country_code='.@$country_code.'&status='.@$status.'&channel_id='.@$channel_id.'&subcategory_id='.@$subcategory_id.'&name='.@$name.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&country_code='.@$country_code.'&status='.@$status.'&channel_id='.@$channel_id.'&subcategory_id='.@$subcategory_id.'&name='.@$name.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>
	
<script type="text/javascript">
$(document).ready(function(){
    //alert($.cookie("userView"));
	if($.cookie("userView") == "listView"){
		$(".gridView").css("display","none");
		$(".listView").css("display","block");
		$("#listViewModule").addClass("iconActive");
		$("#gridViewModule").removeClass("iconActive");
		
	} else {
		$(".listView").css("display","none");
		$(".gridView").css("display","block");
		$("#gridViewModule").addClass("iconActive");
		$("#listViewModule").removeClass("iconActive");
	}
	$("#listViewModule").click(function(){
		$.cookie("userView", "listView");
		$(".gridView").css("display","none");
		$(".listView").css("display","block");
		$("#listViewModule").addClass("iconActive");
		$("#gridViewModule").removeClass("iconActive");
		
	})
	$("#gridViewModule").click(function(){
		$.cookie("userView", "gridView");
		$(".listView").css("display","none");
		$(".gridView").css("display","block");
		$("#gridViewModule").addClass("iconActive");
		$("#listViewModule").removeClass("iconActive");
	})
	
	$("#searchRec").click(function(){
		document.searchContent.submit();
	
	})
	$("#sortBy").change(function(){
		document.sortContent.submit();
	
	})
	$("#filterBy").change(function(){
		document.sortContent.submit();
	
	})
	
})

</script>
