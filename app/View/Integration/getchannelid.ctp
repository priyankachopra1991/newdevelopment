<div class = "row all-channel-container">
	<div class = "col s12">
		<div>1) Open <a href="https://www.youtube.com/" target="blank">YouTube.com</a> and click on User icon as display in screenshot.</div>
		<br clear="all" />
		<br clear="all" />
		<div><img src="<?php echo BASE_URL."/img/channelimg/channelYoutube1.jpg"; ?>" /></div>
		<br clear="all" />
		<div>2) Click on Creator Studio button as displaying in screenshot.</div>
		<br clear="all" />
		<br clear="all" />
		<div><img src="<?php echo BASE_URL."/img/channelimg/channelYoutube2.jpg"; ?>" /></div>
		<br clear="all" />
		<div>3) Click on View Channel text link as displaying in screenshot.</div>
		<br clear="all" />
		<br clear="all" />
		<div><img src="<?php echo BASE_URL."/img/channelimg/channelYoutube3.jpg"; ?>" /></div>
		<br clear="all" />
	</div>
</div>	