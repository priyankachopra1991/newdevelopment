<div class = "row all-channel-container">
	<div class = "col s12">
		
   
	
	<!--List view section strat here-->
	<?php if(!empty($youtubeContents)){ ?>
		<div class = "col s12 listView">
		<form name="contentPurchase" id="contentPurchase" method="post">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th><input type="checkbox" name="select_all" id="select_all" style="display:inline-block;" /></th>
						<th>Name</th>
						<th>Content</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($youtubeContents as $key=>$val){ ?>
						<?php foreach($val as $keyRec=>$response){ ?>
							<tr>
								<td><input type="checkbox" name="video_id[]" class="case" value="<?php echo $response['id']['videoId']; ?>"  style="display:inline-block;" />
								<input type="hidden" name="subcategory_id[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $_GET["subcategory_id"]; ?>" />
								<input type="hidden" name="name[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $response['snippet']['title']; ?>" />
								<input type="hidden" name="description[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $response['snippet']['description']; ?>" />
								<input type="hidden" name="channel_id[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $_GET["channel_id"]; ?>" />
								<input type="hidden" name="country_code[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $_GET["country_code"]; ?>" />
								<input type="hidden" name="youtube_channel_name[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $response['snippet']['channelTitle']; ?>" />
								<input type="hidden" name="youtube_channel_id[<?php echo $response['id']['videoId']; ?>]" value="<?php echo $_GET["youtube_channel_id"]; ?>" />
								</td>
								
								<td class="title-name"><?php echo $response['snippet']['title']; ?></td>
								<!--<td><img src="<?php echo  YOUTUBEIMAGEURL. $response['id']['videoId'] . YOUTUBEIMAGEURLBACK; ?>" width="150" height="100" /></td>-->
								<td><a href="<?php echo YOUTUBEVIDEOEMBEDURL. $response['id']['videoId']; ?>" target="blank" class="btn btn-success">View Video</a><!--<iframe width="200" height="100" src="<?php echo YOUTUBEVIDEOEMBEDURL. $response['id']['videoId']; ?>" frameborder="0" allowfullscreen></iframe>--></td>
								
							</tr>
						<?php } ?>
					<?php } ?>	
				
				</tbody>
				<tfoot>
				<tr>
					<td colspan="6">
						<input type="submit" name="purchaseSubmit" id="purchaseSubmit" value="Submit" class="my-button" />
					</td>
				</tr>				
			</tfoot>
			</table>
			</form>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
	
<script type="text/javascript"> 
$(function () {
    $("#select_all").click(function () {
		if ($("#select_all").is(':checked')) {
			$(".case").prop("checked", true);
        } else {
			$("#selectpage").val('');
            $(".case").prop("checked", false);
        }
    });
});
</script>

