<div class = "row all-channel-container">
	<div class = "col s12">
		<form name="searchYoutubeView" id="searchYoutubeView" action="" method="get">
			<div class = "sort-section white-bg content-sorting">
				<div class = "row">    
					<div class = "col s12">
						<ul class = "select-list right">                           
							<li>
								<select name="searchIn" class="form-control" required>
								<option value="">Select</option>
								<option value="youtube_channel_id" <?php if(@$this->request->query["searchIn"]=='youtube_channel_id')echo 'selected';?>>Channel Id</option>
								<option value="youtube_channel_name" <?php if(@$this->request->query["searchIn"]=='youtube_channel_name')echo 'selected';?>>Channel Name</option>
							</select>	
							</li>
							
							<li>
								<div class="input-field">
									<input type="text" name="searchStr" value="<?php echo @$this->request->query["searchStr"];?>" class="validate" />
								</div>
							</li>
							<li>
								<a class="waves-effect waves-light btn red darken-1" id="searchRec">Search</a>
							</li>
						</ul>
					</div>                             
				</div>     
			</div>
		</form>
		<!-- end of sort section div -->
		<div class = "col s6">
			<!--<ul class = "view-in-listing">
				<li>
					<a href="javascript:void(0)" id="listViewModule">
					<i class = "material-icons grey-text">format_list_bulleted</i>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)"  id="gridViewModule">
					<i class = "material-icons grey-text">view_module</i>
					</a>
				</li>                       
			</ul>-->
		</div>
		<form name="searchYoutubeView2" id="searchYoutubeView2" action="" method="get">
			<div class = "col s6 small-filteration">
				<ul class = "right">                       
					<li>                                          
						<select id="sortBy" name="sortBy">
							<option value="">Sort By</option>
							<option value="desc" <?php if(@$_GET['sortBy'] == "desc"){ echo "selected"; } ?>>Id Desc</option>
							<option value="asc" <?php if(@$_GET['sortBy'] == "asc"){ echo "selected"; } ?>>Id Asc</option>
						</select> 
					</li>
					<!--<li>                                        
					<select id="filterBy" name="filterBy">
						<option value="">Filter By</option>
						<option value="A" <?php if(@$_GET['filterBy'] == "A"){ echo "selected"; } ?>>Active</option>
						<option value="D" <?php if(@$_GET['filterBy'] == "D"){ echo "selected"; } ?>>Inactive</option>
					</select>
					</li>-->
				</ul>
			</div>
		</form>
		<!-- end of s6 div -->  
	</div>
    
	<!--List view seection strat here-->
	<?php if(!empty($channel)){ ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
						<th>Channel Name</th>
						<th>Channel ID</th>
						<th>Content Imported</th>
						<th>Last Synced</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($channel as $channelRec){ ?>
						<td class="data-left"><?php echo $channelRec["youtube_channel_content"]["youtube_channel_name"]; ?></td>
						<td class="data-left"><?php echo $channelRec["youtube_channel_content"]["youtube_channel_id"]; ?></td>
						<!--<td class="data-left"><a href="<?php echo BASE_URL."/fcappcontents/view_content"; ?>" class="vuhu-edit">View Content</a></td>-->
						<td class="data-left"><a href="<?php echo BASE_URL."/integration/youtubecontentview?searchIn=youtube_channel_id&searchStr=".$channelRec["youtube_channel_content"]["youtube_channel_id"]."&searchForm=Search"; ?>" class = "btn red lighten-1">View Content</a></td>
						<td class="data-left"><?php echo  date('Y-m-d H:i:s', strtotime($channelRec["youtube_channel_content"]["created"])); ?></td>
						<td><a href="<?php echo BASE_URL."/integration/youtubeDelete/".$channelRec["youtube_channel_content"]["youtube_channel_id"]; ?>" onclick="deleteConfirm()" class = "btn red lighten-1">Delete</a></td>
					</tr>
				<?php } ?>
				
				</tbody>
			</table>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView">
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
</div>
<?php if((!empty($channel)) && ($numOfPage > 1)){ 
?>
    <?php
	$pageName = BASE_URL."/integration/youtubeview";
	
	if(@$_GET["searchIn"] !=""){
		$searchIn = @$_GET["searchIn"];
	} else {
		$searchIn = "";
	}
	if(@$_GET["searchStr"] !=""){
		$searchStr = @$_GET["searchStr"];
	} else {
		$searchStr = "";
	}
	if(@$_GET["sortBy"] !=""){
		$sortBy = @$_GET["sortBy"];
	} else {
		$sortBy = "";
	}
	
	if(@$_GET["filterBy"] !=""){
		$filterBy = @$_GET["filterBy"];
	} else {
		$filterBy = "";
	}
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page.'&searchIn='.@$searchIn.'&searchStr='.@$searchStr.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i.'&searchIn='.@$searchIn.'&searchStr='.@$searchStr.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page.'&searchIn='.@$searchIn.'&searchStr='.@$searchStr.'&sortBy='.@$sortBy.'&filterBy='.@$filterBy; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
		
		
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>	
<script type="text/javascript">
$(document).ready(function(){
    
	$("#searchRec").click(function(){
		document.searchYoutubeView.submit();
	
	})
	$("#sortBy").change(function(){
		document.searchYoutubeView2.submit();
	
	})
	$("#filterBy").change(function(){
		document.searchYoutubeView2.submit();
	
	})
	
})
</script>
<script>
function deleteConfirm() {
    confirm("Are you sure want to Delete your channel? Note :- All Video Contents will be delete from our system.")
}
</script>

