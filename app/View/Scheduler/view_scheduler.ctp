

<div class = "row all-channel-container">
	<div class = "col s12">
	
		<!---form name="searchContent" id="searchContent" action="" method="get"-->
			<div class = "table-format-data white-bg">
				<div class = "row">    
					<div class = "col s3">
					
						<div class = "detail-title-section">
							<div class = "card card-stats">
								<div class = "card-header" data-background-color = "red">
									<i class = "material-icons"> person </i>
								</div>
								<div class = "card-content">
									<h3>View Notification</h3>
								</div>                        
							</div>
						</div>                            
					</div>     
				
		<!--/form-->
		
		<!-- end of s6 div -->  
	
    <!--Grid view section start here--> 
	
	<!--Grid view section end here-->
	<!--List view seection strat here-->
	
	  <?php 
	  if(!empty($value)){ ?>
	   <div class = "col s12 listView" >
			<table class = "bordered centered striped">                   
				<thead>                    
					<tr class ="red lighten-5">
					     
						<th>Id</th>
						<th>Topic</th>
						<th>App</th>
						<th>Title</th>
						<th>Deeplink</th>
						<th>Image</th>
						<!--th>Image link</th-->
						<th>Tag</th>
						<th>Message</th>
						<!--th>Message ID</th-->
						<th>Created</th>
						<th>Scheduled</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					
					
					<?php
					foreach($value as $obj){
						
					?>
						<tr>
						    <td><?php echo $obj["scheduler_data"]['id']; ?></td> 
							<td><?php echo $obj["scheduler_data"]['topic']; ?></td>
							<td><?php echo $obj["scheduler_data"]['app']; ?></td>
							<td><?php echo $obj["scheduler_data"]['title']; ?></td>
							<td><?php echo $obj["scheduler_data"]['deep_link'];//image?>
							<!--td><?php if (file_exists($obj["scheduler_data"]['image_link'])):
								echo $this->Html->image($obj["scheduler_data"]['image_link'], array('alt' => 'First Cut','width'=>'50px','height'=>'50px'));
								endif;		?></td---> 
					
							<td><?php echo $this->Html->image($obj["scheduler_data"]['image_link'], array('alt' => 'First Cut','width'=>'70px','height'=>'70px'));?></td> 
							
							<!--td><?php echo $obj["scheduler_data"]['image_link']; ?></td-->
							<td><?php echo $obj["scheduler_data"]['tags']; ?></td>
							<td><?php echo $obj["scheduler_data"]['message']; ?></td>
							<!--td><?php //echo $obj["pushnotification_data"]['messageid'];?></td-->
							<td><?php echo $obj["scheduler_data"]['created'];?></td>
							<td><?php echo $obj["scheduler_data"]['scheduled'];?></td>
							<td><?php if($obj["scheduler_data"]['flag']=='1' || $obj["scheduler_data"]['flag']==1){ echo "Sent";}else {echo "Queue";}?></td>
							
							
						</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
		</div>
		</div>
    <?php } else { ?>
		<div class = "col s12 listView" >
			<table class = "bordered centered striped"> 
				<tbody>
					<tr>
						<td class="noRecordMsg">
							Sorry!! No record found.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php } ?>
	<!--list view section end here-->
	</form>
</div>

<?php if((!empty($value)) && ($numOfPage > 1)){ 
    
	$pageName = BASE_URL."/scheduler/view_scheduler";
	
	
	if(!isset($_GET["page"]))
	{
		$_GET["page"] = 1;
	}
	$page = $pageNum - 1;
	?>
	<ul class="pagination">
	    <?php if($pageNum > 1) {?>
			<li><a href="<?php echo $pageName.'/?page='.$page ; ?>"><i class="material-icons">chevron_left</i></a></li>
		<?php } else { ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_left</i></a></li>
		<?php } ?>
		
		<?php 
		if($numOfPage >3){
			if($_GET["page"] >=$numOfPage){
				$minVal = $_GET["page"]-2;
				$maxVal = $_GET["page"];
			} else {
				$minVal = $_GET["page"];
				$maxVal = $_GET["page"]+2;
			}
		} else {
			$minVal = 1;
			$maxVal = $numOfPage;
		}
		
		for($i= $minVal; $i<=$maxVal; $i++)
		{
		?>
			<li <?php if($i == @$_GET["page"]){ ?>class="active"<?php } else { ?>class="waves-effect"<?php } ?>><a href="<?php echo $pageName.'/?page='.$i; ?>"><?php echo $i; ?></a></li>
		<?php } 
		$page = $pageNum + 1;
		?>
		<?php if($numOfPage == $pageNum){ ?>
			<li class="disabled"><a href="javascript:void(0)"><i class="material-icons">chevron_right</i></a></li>
		<?php } else { ?>
			<li class="waves-effect"><a href="<?php echo $pageName.'/?page='.$page; ?>"><i class="material-icons">chevron_right</i></a></li>
		<?php } ?>
	</ul>	
<?php } ?>

<?php echo $this->element('flashmessage'); ?>
	

