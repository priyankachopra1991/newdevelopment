 <!-- Header -->
          <div class = "content-wrapper container">
          <div class = "navbar-fixed header-nav">
            <ul class="collection dropdown-content" id="dropdown1">
                <li class="collection-item avatar">
                  <i class="material-icons circle">folder</i>                 
                  <p>First Line
                     Second Line
                  </p>
                </li>
                <li class="collection-item avatar">
                  <i class="material-icons circle green">insert_chart</i>                  
                  <p>First Line
                     Second Line
                  </p>
                </li>
                <li class="collection-item avatar">
                  <i class="material-icons circle red">play_arrow</i>                  
                  <p>First Line 
                     Second Line
                  </p>
                </li>
              </ul>            
              <nav>   
                <div class = "nav-wrapper">
                  <ul id="nav-mobile" class="right hide-on-med-and-down right notification-list">  
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1"> <i class="material-icons">notifications</i><span class="new badge">4</span></a></li>
                  </ul>                  
                    <form>
                    <div class = "input-field">
                      <input id = "search" type = "search" required>
                      <label class = "label-icon" for = "search">
                        <i class = "material-icons">search</i>
                      </label> 
                    </div>
                  </form>
                   
                </div>
              </nav>
          </div>
            <!-- end of Header -->
            <!-- Home-States-->
          <div class = "home-stats">
              <div class = "row">
                  <div class = "col s3">
                    <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "orange">
                            <i class = "material-icons"> attach_money </i>
                        </div>
                        <div class = "card-content">
                          <p>Total Earning</p>
                          <h3>Rs.28976</h3>
                        </div>
                        <div class = "card-footer">
                          <i class = "material-icons tiny">date_range</i>
                          <span>Last 24 Hours</span>
                        </div>
                    </div>
                      <!-- end of card div -->
                  </div>
                  <div class = "col s3">
                    <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "red">
                            <i class = "material-icons"> visibility </i>
                        </div>
                        <div class = "card-content">
                          <p>Total Views</p>
                          <h3>Rs.28976</h3>
                        </div>
                        <div class = "card-footer">
                          <i class = "material-icons tiny">date_range</i>
                          <span>Last 24 Hours</span>
                        </div>
                    </div>
                      <!-- end of card div -->
                  </div>
                  <div class = "col s3">
                    <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "green">
                            <i class = "material-icons"> insert_chart </i>
                        </div>
                        <div class = "card-content">
                          <p>Revenue</p>
                          <h3>Rs.28976</h3>
                        </div>
                        <div class = "card-footer">
                          <i class = "material-icons tiny">date_range</i>
                          <span>Last 24 Hours</span>
                        </div>
                    </div>
                      <!-- end of card div -->
                  </div>
                  <div class = "col s3">
                    <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "blue">
                            <i class = "material-icons"> group </i>
                        </div>
                        <div class = "card-content">
                          <p>Subscribers</p>
                          <h3>Rs.28976</h3>
                        </div>
                        <div class = "card-footer">
                          <i class = "material-icons tiny">date_range</i>
                          <span>Last 24 Hours</span>
                        </div>
                    </div>
                      <!-- end of card div -->
                  </div>
              </div>
                <!-- end of row div -->
          </div>
            <!-- end of home State div -->
          
          <!-- home-graph-container -->
          <div class = "graph-container">
              <div class = "row">
                  <div class = "col s6">
                    <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "orange">
                            <i class = "material-icons"> group </i>
                        </div>
                       <div class = "input-field col s4 offset-s5">
                          <select>
                            <option value = "Today" selected>Today</option>
                            <option value = "Yesterday">Yesterday</option>
                            <option value = "Last 7 Days">Last 7 Days</option>
                            <option value = "Last 28 Days">Last 28 Days</option>
                            <option value = "This Year">This Year</option>
                          </select>
                       </div>
                        <div class = "graph-content">
                            <canvas id="bar-chart" width="800" height="450"></canvas>
                        </div>
                    </div>
                  </div>
                  <div class = "col s6">
                    <div class = "card card-stats">
                        <div class = "card-header" data-background-color = "green">
                            <i class = "material-icons"> group </i>
                        </div>
                        <div class = "input-field col s4 offset-s5">
                          <select>
                            <option value = "Today" selected>Today</option>
                            <option value = "Yesterday">Yesterday</option>
                            <option value = "Last 7 Days">Last 7 Days</option>
                            <option value = "Last 28 Days">Last 28 Days</option>
                            <option value = "This Year">This Year</option>
                          </select>
                       </div>
                        <div class = "graph-content">
                            <canvas id="line-chart" width="800" height="450"></canvas
                        </div>
                    </div>
                  </div>    
              </div>
          </div>  
  
          </div>
            <!-- end of content wrapper div -->