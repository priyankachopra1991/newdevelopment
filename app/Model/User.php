<?php
class User extends AppModel {
	public $useTable='cm_users';
	public $name='User';
	public $useDbConfig = 'multipleApp';
	
	//generate password...
	function randPassword( $length ){

		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return substr(str_shuffle($chars),0,$length);

	}
	
	//Email Function
	function _mail($sender, $senderName, $recepient, $recepientName, $subject, $message, $debug = 0) {
	if($debug == 1) {
		echo "Email Variables:" . "<br />";
		echo "Recepient Name: " . $recepientName . "<br />";
		echo "Recepient Email: " . $recepient . "<br />";
		echo "Sender Name: " . $senderName . "<br />";
		echo "Sender Email: " . $sender . "<br />";
		echo "Subject: " . $subject . "<br />";
		echo "Message: <hr />" . $message . "<hr />";
	}
	
		$recepientName = (($recepientName == "") ? $recepient : $recepientName);
		$senderName = (($senderName == "") ? $sender : $senderName);	
		//$headers = "To: $recepientName<$recepient>\n" . "From: $senderName<$sender>\n" . "MIME-Version: 1.0\n" . "Content-type: text/html; charset=iso-8859-1";
		
		$headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        $headers .= "From: ".$sender."" . "\r\n" . "Reply-To: ".$sender."" . "\r\n" ."X-Mailer: PHP/" . phpversion();
		
		$varHTML = '';
		$varHTML = $varHTML . '<div style="font-family:Verdana; font-size:12px;">';
		$varHTML = $varHTML . '[DATA]';
		$varHTML = $varHTML . '</div>';
		
		$varHTML = str_replace("[DATA]", $message, $varHTML);
		
		if ( mail($recepient, $subject, $varHTML, $headers)  ) {
			$varStatus = "1";
		}
		else {
			$varStatus = "0";
		}
	
		
	return $varStatus;
	}
	
	function getLastNDays($days, $format = 'd/m'){
		$m = date("m"); $de= date("d")-1; $y= date("Y");
		$dateArray = array();
		for($i=0; $i<=$days-1; $i++){
			$dateArray[] = '' . date($format, mktime(0,0,0,$m,($de-$i),$y)) . ''; 
		}
		return array_reverse($dateArray);
	}
}
?>