<?php
class Project extends AppModel {
	public $useTable='cm_projects';
	public $name='Project';
	
	public function createSlug($string) {
	   $string1 = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
	   $newString = preg_replace('/[^A-Za-z0-9\-]/', '', $string1); // Removes special chars.
	   return $newString;
	}
}
?>